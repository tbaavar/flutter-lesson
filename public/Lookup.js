export const lookup = [
    {
        "lookup_type": "Role",
        "lookup_code": "Admin",
        "name": "Админ",
        "sequence": 1
    },
    {
        "lookup_type": "Trade_type",
        "lookup_code": "Term_Sale",
        "name": "Term Sale",
        "sequence": 1
    },
    {
        "lookup_type": "Trade_type",
        "lookup_code": "Term_Purchase",
        "name": "Term Purchase",
        "sequence": 2
    },
    {
        "lookup_type": "Trade_type",
        "lookup_code": "Spot_Sale",
        "name": "Spot Sale",
        "sequence": 3
    },
    {
        "lookup_type": "Trade_type",
        "lookup_code": "Spot_Purchase",
        "name": "Spot Purchase",
        "sequence": 4
    },
    {
        "lookup_type": "Trade_Origin",
        "lookup_code": "Singapore",
        "name": "Singapore",
        "sequence": 1
    },
    {
        "lookup_type": "Trade_Origin",
        "lookup_code": "China",
        "name": "China",
        "sequence": 2
    },
    {
        "lookup_type": "Trade_Origin",
        "lookup_code": "Russia",
        "name": "Russia",
        "sequence": 3
    },
    {
        "lookup_type": "Contract_type",
        "lookup_code": "Buckeye",
        "name": "Buckeye",
        "sequence": 4
    },
    {
        "lookup_type": "Contract_type",
        "lookup_code": "American_Natural_Gas",
        "name": "American Natural Gas",
        "sequence": 1
    },
    {
        "lookup_type": "Contract_type",
        "lookup_code": "Barge_Other",
        "name": "Barge -Other",
        "sequence": 2
    },
    {
        "lookup_type": "Contract_type",
        "lookup_code": "Barge_TTB",
        "name": "Barge -TTB",
        "sequence": 3
    },
    {
        "lookup_type": "Contract_type",
        "lookup_code": "Bunkering",
        "name": "Bunkering",
        "sequence": 5
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "CPT",
        "name": "Carriege Paid To",
        "sequence": 1
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DAF",
        "name": "Delivered at Frontier",
        "sequence": 2
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DAP",
        "name": "Delivered at Place",
        "sequence": 3
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DAT",
        "name": "Deleivered at Terminal",
        "sequence": 4
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DDP",
        "name": "Deleivered Duty Paid",
        "sequence": 5
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DDU",
        "name": "Deleivered Duty Unpaid",
        "sequence": 6
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DEQ",
        "name": "Deleivered Ex Quay",
        "sequence": 7
    },
    {
        "lookup_type": "Terms",
        "lookup_code": "DES",
        "name": "Deleivered Ex Ship",
        "sequence": 8
    },
    {
        "lookup_type": "Delivery_method",
        "lookup_code": "Vessel_Barge",
        "name": "Vessel/ Barge",
        "sequence": 1
    },
    {
        "lookup_type": "GeoArea",
        "lookup_code": "Singapore ",
        "name": "Singapore ",
        "sequence": 3
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "Diesel",
        "name": "Diesel",
        "sequence": 1
    },
    {
        "lookup_type": "GeoArea",
        "lookup_code": "China ",
        "name": "China",
        "sequence": 2
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "Diesel 77",
        "name": "Diesel 77",
        "sequence": 2
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "GO30",
        "name": "GO30",
        "sequence": 3
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "GO50",
        "name": "GO50",
        "sequence": 4
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "Gasoil",
        "name": "Gasoil",
        "sequence": 5
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "Gasoil0.3%",
        "name": "Gasoil0.3%",
        "sequence": 6
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "Gasoil0.35%",
        "name": "Gasoil0.35%",
        "sequence": 7
    },
    {
        "lookup_type": "Grade",
        "lookup_code": "Gasoil1.5%",
        "name": "Gasoil1.5%",
        "sequence": 8
    },
    {
        "lookup_type": "Role",
        "lookup_code": "Operator",
        "name": "Үйл ажиллагаа хариуцсан ажилтан",
        "sequence": 4
    },
    {
        "lookup_type": "Role",
        "lookup_code": "Financer",
        "name": "Санхүү хариуцсан ажилтан",
        "sequence": 3
    },
    {
        "lookup_type": "Role",
        "lookup_code": "Trader",
        "name": "Худалдаа хариуцсан ажилтан",
        "sequence": 2
    },
    {
        "lookup_type": "Role",
        "lookup_code": "ContractAdmin",
        "name": "Гэрээний хариуцсан ажилтан",
        "sequence": 5
    },
    {
        "lookup_type": "GeoArea",
        "lookup_code": "Russia ",
        "name": "Russia",
        "sequence": 1
    },
    {
        "lookup_type": "Product",
        "lookup_code": "GasOil",
        "name": "Gas Oil",
        "sequence": 1
    },
    {
        "lookup_type": "Company",
        "lookup_code": "TRP",
        "name": "TRP, Trafigura Pre Limited",
        "sequence": 1
    },
    {
        "lookup_type": "Product",
        "lookup_code": "Gasoline",
        "name": "Gasoline",
        "sequence": 2
    },
    {
        "lookup_type": "Product",
        "lookup_code": "Heavy_fuel_oil",
        "name": "Heavy Fuel Oil",
        "sequence": 3
    },
    {
        "lookup_type": "Product",
        "lookup_code": "Iron_Ore",
        "name": "Iron Ore",
        "sequence": 4
    },
    {
        "lookup_type": "Product",
        "lookup_code": "Jet",
        "name": "Jet",
        "sequence": 5
    },
    {
        "lookup_type": "Product",
        "lookup_code": "Kerosene",
        "name": "Kerosene",
        "sequence": 6
    },
    {
        "lookup_type": "Product",
        "lookup_code": "LCFS",
        "name": "LCFS",
        "sequence": 7
    },
    {
        "lookup_type": "Product",
        "lookup_code": "Light_crude_oil",
        "name": "Light Crude Oil",
        "sequence": 8
    },
    {
        "lookup_type": "Unit",
        "lookup_code": "CM",
        "name": "Cubic Metres",
        "sequence": 2
    },
    {
        "lookup_type": "Unit",
        "lookup_code": "B",
        "name": "Barrels",
        "sequence": 1
    },
    {
        "lookup_type": "Unit",
        "lookup_code": "G",
        "name": "Gallons",
        "sequence": 3
    },
    {
        "lookup_type": "Unit",
        "lookup_code": "MT",
        "name": "Metric Tonnes",
        "sequence": 4
    },
    {
        "lookup_type": "Trade_Treatment",
        "lookup_code": "NA",
        "name": "NA",
        "sequence": 1
    },
    {
        "lookup_type": "Cover_Type",
        "lookup_code": "Covered",
        "name": "Covered",
        "sequence": 1
    },
    {
        "lookup_type": "Cover_Type",
        "lookup_code": "UnCovered",
        "name": "UnCovered",
        "sequence": 1
    },
    {
        "lookup_type": "Responsible_Party",
        "lookup_code": "Buyer",
        "name": "Buyer's",
        "sequence": 1
    },
    {
        "lookup_type": "Responsible_Party",
        "lookup_code": "Seller",
        "name": "Seller's",
        "sequence": 2
    },
    {
        "lookup_type": "Responsible_Party",
        "lookup_code": "NA ",
        "name": "NA",
        "sequence": 3
    },
    {
        "lookup_type": "Port",
        "lookup_code": "Port_1",
        "name": "Border Russia/Mongolia, Russia",
        "sequence": 1
    },
    {
        "lookup_type": "Port",
        "lookup_code": "Port_2",
        "name": "Border Russia/Mongolia , Mongolia",
        "sequence": 2
    },
    {
        "lookup_type": "Intended_Vessel",
        "lookup_code": "Rail_Car",
        "name": "Rail Car",
        "sequence": 1
    },
    {
        "lookup_type": "Intended_Vessel",
        "lookup_code": "Vessel",
        "name": "Vessel",
        "sequence": 2
    },
    {
        "lookup_type": "Demurage_rate",
        "lookup_code": "AFRA",
        "name": "AFRA",
        "sequence": 1
    },
    {
        "lookup_type": "Transfer_type",
        "lookup_code": "Loading",
        "name": "Loading",
        "sequence": 1
    },
    {
        "lookup_type": "Demurage_rate",
        "lookup_code": "N/A",
        "name": "N/A",
        "sequence": 4
    },
    {
        "lookup_type": "Demurage_rate",
        "lookup_code": "USD",
        "name": "USD",
        "sequence": 6
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_8",
        "name": "Load port- Vessel's composite after loading",
        "sequence": 7
    },
    {
        "lookup_type": "Demurage_rate",
        "lookup_code": "As_Other",
        "name": "As Other",
        "sequence": 2
    },
    {
        "lookup_type": "Demurage_rate",
        "lookup_code": "As_Per_C_P",
        "name": "As per C/P",
        "sequence": 3
    },
    {
        "lookup_type": "Demurage_rate",
        "lookup_code": "T_C_Cost",
        "name": "T/C  and Cost",
        "sequence": 5
    },
    {
        "lookup_type": "Quantity_base",
        "lookup_code": "BL",
        "name": "Bill of landing",
        "sequence": 1
    },
    {
        "lookup_type": "Quantity_base",
        "lookup_code": "OT",
        "name": "Outturn",
        "sequence": 2
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "60",
        "name": "60 days",
        "sequence": 1
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "65",
        "name": "65 days",
        "sequence": 2
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "70",
        "name": "70 days",
        "sequence": 3
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "75",
        "name": "75 days",
        "sequence": 4
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "BL ",
        "name": "Bill of lading\r\n",
        "sequence": 1
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "COD",
        "name": "COD",
        "sequence": 2
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "CCO",
        "name": "Completion of Cargo Operations",
        "sequence": 3
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "CL",
        "name": "Completion of Loading",
        "sequence": 4
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "DAA",
        "name": "Delivery Acceptance Act",
        "sequence": 5
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "FD",
        "name": "Fixed date",
        "sequence": 6
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "ITT",
        "name": "On Tank Trasnsfer",
        "sequence": 7
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "30",
        "name": "30 days",
        "sequence": 5
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "35",
        "name": "35 days",
        "sequence": 6
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "40",
        "name": "40 days",
        "sequence": 7
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "45",
        "name": "45 days",
        "sequence": 8
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "50",
        "name": "50 days",
        "sequence": 9
    },
    {
        "lookup_type": "Notification_timebar",
        "lookup_code": "55",
        "name": "55 days",
        "sequence": 10
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "LDDM",
        "name": "Last Day of Delivery Month",
        "sequence": 8
    },
    {
        "lookup_type": "Law",
        "lookup_code": "ENG",
        "name": "English",
        "sequence": 1
    },
    {
        "lookup_type": "Law",
        "lookup_code": "EST",
        "name": "Estonia",
        "sequence": 2
    },
    {
        "lookup_type": "Law",
        "lookup_code": "French",
        "name": "French",
        "sequence": 3
    },
    {
        "lookup_type": "Law",
        "lookup_code": "Greek",
        "name": "Greek",
        "sequence": 4
    },
    {
        "lookup_type": "Law",
        "lookup_code": "HongKong",
        "name": "Hong Kong",
        "sequence": 5
    },
    {
        "lookup_type": "Law",
        "lookup_code": "Indonesian",
        "name": "Indonesian",
        "sequence": 6
    },
    {
        "lookup_type": "Law_title",
        "lookup_code": "Title_2",
        "name": "Load or upon conclusion of the agreement, whichever is the later",
        "sequence": 2
    },
    {
        "lookup_type": "Law_title",
        "lookup_code": "Title_1",
        "name": "Load port",
        "sequence": 1
    },
    {
        "lookup_type": "Law_title",
        "lookup_code": "Title_3",
        "name": "Load or upon Seller's receipt of Letter credit, whichever is the later",
        "sequence": 3
    },
    {
        "lookup_type": "Law_title",
        "lookup_code": "Title_4",
        "name": "Other",
        "sequence": 4
    },
    {
        "lookup_type": "Law_title",
        "lookup_code": "Title_5",
        "name": "Payment",
        "sequence": 5
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_1",
        "name": "Discharge port- Shoretanks after discharge",
        "sequence": 1
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_2",
        "name": "Discharge port- Vessels composite prior to discharge",
        "sequence": 2
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_3",
        "name": "ITT Location - Buyer's tanks after transfer",
        "sequence": 3
    },
    {
        "lookup_type": "Transfer_type",
        "lookup_code": "Discharge",
        "name": "Discharge",
        "sequence": 1
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_4",
        "name": "ITT Location - Inspector's quality report ",
        "sequence": 4
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_5",
        "name": "ITT Location - Seller's tanks prior to transfer",
        "sequence": 5
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_6",
        "name": "ITT Location - Terminal certificate of quality",
        "sequence": 5
    },
    {
        "lookup_type": "Quality_Test_Location",
        "lookup_code": "Location_7",
        "name": "Load port- Shoretanks after load",
        "sequence": 6
    },
    {
        "lookup_type": "Time_Condition",
        "lookup_code": "After",
        "name": "After",
        "sequence": 1
    },
    {
        "lookup_type": "Delivery_date_type",
        "lookup_code": "INV",
        "name": "Invoicing",
        "sequence": 8
    },
    {
        "lookup_type": "Time_Condition",
        "lookup_code": "Before",
        "name": "Before",
        "sequence": 2
    },
    {
        "lookup_type": "Time_Condition",
        "lookup_code": "From",
        "name": "From",
        "sequence": 3
    },
    {
        "lookup_type": "Day_Calc_Method",
        "lookup_code": "NWD",
        "name": "Next Working Day",
        "sequence": 2
    },
    {
        "lookup_type": "Day_Calc_Method",
        "lookup_code": "IGN",
        "name": "Ignore",
        "sequence": 1
    },
    {
        "lookup_type": "Day_Calc_Method",
        "lookup_code": "PWD",
        "name": "Previous Working Day",
        "sequence": 3
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "LBR",
        "name": "Libor",
        "sequence": 1
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "PRM",
        "name": "Prime",
        "sequence": 2
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "SOFR",
        "name": "Secured Overnight Financing Rate",
        "sequence": 3
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "SOIA",
        "name": "Sterling Overnight Index Avegrage",
        "sequence": 4
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "SARO",
        "name": "Swiss Average Rate Overnight",
        "sequence": 5
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "TOAR",
        "name": "Tokyo Overnight Average Rate",
        "sequence": 6
    },
    {
        "lookup_type": "Interest_Rate_Type",
        "lookup_code": "TTRFR",
        "name": "Tokyo Term Risk Free Rate",
        "sequence": 7
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "PP",
        "name": "Pre-payment",
        "sequence": 1
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "LCS",
        "name": "Letter of Credit (Standby)",
        "sequence": 3
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "LCD",
        "name": "Letter of Credit (documentary)",
        "sequence": 2
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "OA",
        "name": "Open Account",
        "sequence": 4
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "OAPCG",
        "name": "Open Account with PCG",
        "sequence": 5
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "OAPU",
        "name": "Open Account with PU",
        "sequence": 6
    },
    {
        "lookup_type": "Credit_Type",
        "lookup_code": "BG",
        "name": "Bank Guarantee",
        "sequence": 7
    },
    {
        "lookup_type": "Currency",
        "lookup_code": "USD ",
        "name": "USD",
        "sequence": 1
    },
    {
        "lookup_type": "Currency",
        "lookup_code": "UYU",
        "name": "UYU",
        "sequence": 2
    },
    {
        "lookup_type": "Currency",
        "lookup_code": "VND",
        "name": "VND",
        "sequence": 3
    },
    {
        "lookup_type": "Currency",
        "lookup_code": "XAF",
        "name": "XAF",
        "sequence": 4
    },
    {
        "lookup_type": "Currency",
        "lookup_code": "XOF",
        "name": "XOF",
        "sequence": 5
    },
    {
        "lookup_type": "Period",
        "lookup_code": "ON",
        "name": "Overnight",
        "sequence": 1
    },
    {
        "lookup_type": "Period",
        "lookup_code": "OM",
        "name": "One Month",
        "sequence": 2
    },
    {
        "lookup_type": "Period",
        "lookup_code": "OW",
        "name": "One Week",
        "sequence": 3
    },
    {
        "lookup_type": "Period",
        "lookup_code": "TW",
        "name": "Two Week",
        "sequence": 4
    },
    {
        "lookup_type": "Period",
        "lookup_code": "TM",
        "name": "Two Month",
        "sequence": 5
    },
    {
        "lookup_type": "Bank",
        "lookup_code": "Bank_1",
        "name": "Bank_1",
        "sequence": 1
    },
    {
        "lookup_type": "Bank",
        "lookup_code": "Bank_2",
        "name": "Bank_2",
        "sequence": 2
    },
    {
        "lookup_type": "Bank",
        "lookup_code": "Bank_3",
        "name": "Bank_3",
        "sequence": 3
    },
    {
        "lookup_type": "Payment_documentation",
        "lookup_code": "IO",
        "name": "Invoice Only",
        "sequence": 1
    },
    {
        "lookup_type": "Payment_documentation",
        "lookup_code": "ODPDI",
        "name": "Original doc per doc Instruction",
        "sequence": 2
    },
    {
        "lookup_type": "Payment_documentation",
        "lookup_code": "LOI",
        "name": "Letter of indemnity (LOI)",
        "sequence": 3
    },
    {
        "lookup_type": "Payment_documentation",
        "lookup_code": "CLOI",
        "name": "Countersigned LOI (in lieu of missing doc)",
        "sequence": 4
    },
    {
        "lookup_type": "Operation",
        "lookup_code": "Divide",
        "name": "Divide by",
        "sequence": 1
    },
    {
        "lookup_type": "Operation",
        "lookup_code": "Minus",
        "name": "Minus",
        "sequence": 2
    },
    {
        "lookup_type": "Operation",
        "lookup_code": "Multiply",
        "name": "Multiply by",
        "sequence": 3
    },
    {
        "lookup_type": "Operation",
        "lookup_code": "Plus",
        "name": "Plus",
        "sequence": 4
    },
    {
        "lookup_type": "Price_Date_Type",
        "lookup_code": "EFP",
        "name": "EFP",
        "sequence": 1
    },
    {
        "lookup_type": "Price_Date_Type",
        "lookup_code": "FD ",
        "name": "Fixed Dates",
        "sequence": 2
    },
    {
        "lookup_type": "Price_Date_Type",
        "lookup_code": "Formulated",
        "name": "Formulated",
        "sequence": 3
    },
    {
        "lookup_type": "Price_Date_Type",
        "lookup_code": "Other",
        "name": "Other",
        "sequence": 4
    },
    {
        "lookup_type": "Price_Date_Type",
        "lookup_code": "TBA",
        "name": "TBA",
        "sequence": 5
    },
    {
        "lookup_type": "Inequality_Term",
        "lookup_code": ">=",
        "name": ">=",
        "sequence": 4
    },
    {
        "lookup_type": "Inequality_Term",
        "lookup_code": "<=",
        "name": "<=",
        "sequence": 2
    },
    {
        "lookup_type": "Inequality_Term",
        "lookup_code": "<",
        "name": "<",
        "sequence": 1
    },
    {
        "lookup_type": "Inequality_Term",
        "lookup_code": ">",
        "name": ">",
        "sequence": 3
    },
    {
        "lookup_type": "Cost_type",
        "lookup_code": "C1",
        "name": "Cost 1",
        "sequence": 1
    },
    {
        "lookup_type": "Cost_type",
        "lookup_code": "C2",
        "name": "Cost 2",
        "sequence": 2
    },
    {
        "lookup_type": "Cost_method",
        "lookup_code": "AMT",
        "name": "By Amount",
        "sequence": 1
    },
    {
        "lookup_type": "Cost_method",
        "lookup_code": "PER",
        "name": "By Percent",
        "sequence": 2
    },
    {
        "lookup_type": "Escalator_Type",
        "lookup_code": "ET1",
        "name": "Escalator Type 1",
        "sequence": 1
    },
    {
        "lookup_type": "Escalator_Type",
        "lookup_code": "ET2",
        "name": "Escalator Type 2",
        "sequence": 2
    },
    {
        "lookup_type": "Escalator_Spec",
        "lookup_code": "ES1",
        "name": "Escalator Spec 1",
        "sequence": 1
    },
    {
        "lookup_type": "Escalator_Spec",
        "lookup_code": "ES2",
        "name": "Escalator Spec 2",
        "sequence": 2
    }
]
  

