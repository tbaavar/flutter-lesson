export function tokenResponse(req, res) {
  try {
    return Response.json({ message: "Success", result: res }, { status: 200 });
  } catch (error) {
    return Response.json({ message: "Error", result: "" }, { status: 500 });
  }
}
