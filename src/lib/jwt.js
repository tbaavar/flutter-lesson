import { SignJWT, jwtVerify } from "jose";

export const getJwtSecretKey = () => {
  const secret_key = process.env.SECRET_KEY;
  if (!secret_key || secret_key.length === 0) {
    throw new Error("Missing JWT secret key");
  }
  return secret_key;
};

export async function signJwtAccessToken(payload) {
  const iat = Math.floor(Date.now() / 1000);
  const exp = iat + 60 * 60 * 24;
  const expRefresh = iat + 60 * 60 * 24;

  const token = await new SignJWT({ ...payload })
    .setProtectedHeader({ alg: "HS256", typ: "JWT" })
    .setExpirationTime(exp)
    .setIssuedAt(iat)
    .setNotBefore(iat)
    .sign(new TextEncoder().encode(getJwtSecretKey()));
  const refreshToken = await new SignJWT({ ...payload })
    .setProtectedHeader({ alg: "HS256", typ: "JWT" })
    .setExpirationTime(expRefresh)
    .setIssuedAt(iat)
    .setNotBefore(iat)
    .sign(new TextEncoder().encode(getJwtSecretKey()));
  return {
    token,
    refreshToken,
  };
}

export async function verifyJwt(token) {
  try {
    const verified = await jwtVerify(
      token,
      new TextEncoder().encode(getJwtSecretKey())
    );
    return verified.payload;
  } catch (error) {
    throw new Error("Invalid JWT token");
  }
}
