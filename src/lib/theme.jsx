"use client";
import { createTheme, alpha, getContrastRatio } from "@mui/material/styles";
import { Manrope } from "next/font/google";

export const plus = Manrope({
  weight: ["500", "700"],
  subsets: ["latin"],
  display: "swap",
  fallback: ["Helvetica", "Arial", "sans-serif"],
});

const baselightTheme = createTheme({
  direction: "ltr",
  palette: {
    primary: {
      main: "#0B626B",
      light: "#3b8188",
      dark: "#07444a",
      contrastText: "#ffffff",
    },
    secondary: {
      main: "#616D7C",
      light: "#8993A0",
      dark: "#192434",
    },
    success: {
      main: "#158444",
      light: "#439c69",
      dark: "#0e5c2f",
      contrastText: "#ffffff",
    },
    info: {
      main: "#263445",
      light: "#616D7C",
      dark: "#192434",
      contrastText: "#ffffff",
    },
    error: {
      main: "#E21D12",
      light: "#ea6760",
      dark: "#CA150C",
      contrastText: "#ffffff",
    },
    warning: {
      main: "#FFCC00",
      light: "#ffd633",
      dark: "#b28e00",
      contrastText: "#ffffff",
    },
    grey: {
      100: "#DADFE4",
      400: "#C4CBD3",
      500: "#F4F5F7",
    },
    urgent: {
      main: "#fdedec",
      light: "#FDE9E8",
      dark: "#b1a3a2",
      contrastText: "#D43D35",
    },
    high: {
      main: "#fff4cb",
      light: "#FFF2BF",
      dark: "#b2a985",
      contrastText: "#985211",
    },
    medium: {
      main: "#deebf2",
      light: "#D6E6EF",
      dark: "#95a1a7",
      contrastText: "#00A3FF",
    },
    regular: {
      main: "#ecf6f0",
      light: "#E8F4ED",
      dark: "#a2aaa5",
      contrastText: "#158443",
    },
    dark: {
      main: "#11344D",
      light: "#405c70",
      dark: "#0b2435",
      contrastText: "#ffffff",
    },
    text: {
      primary: "#263445",
      secondary: "#616D7C",
      tertiary: "#8993A0",
      header: "#192434",
    },
    blueChip: {
      //default: "#e0e0e0",
      main: "#4285F4",
      contrastText: "#ffffff"
    },
    greenChip: {
      //default: "#e0e0e0",
      main: "#34A853",
      contrastText: "#ffffff"
    },
    background: {
      //default: "#e0e0e0",
      default: "#eeeeee",
    },
    action: {
      disabledBackground: "rgba(73,82,88,0.12)",
      selectedOpacity: 0.2,
    },
    divider: "#C4CBD3",
  },
  typography: {
    fontFamily: plus.style.fontFamily,
    h2: {
      fontWeight: 700,
      fontSize: "32px",
      lineHeight: "41.6px",
      fontFamily: plus.style.fontFamily,
    },

    h5: {
      fontWeight: 700,
      fontSize: "1.125rem",
      lineHeight: "1.6rem",
    },

    h8: {
      fontWeight: 300,
      fontSize: "1.000rem",
      lineHeight: "1.6rem",
    },

    body1: {
      fontSize: "12px",
      fontWeight: 400,
      lineHeight: "1.334rem",
    },
    body2: {
      fontSize: "0.75rem",
      letterSpacing: "0rem",
      fontWeight: 400,
      lineHeight: "1rem",
    },
    body3: {
      fontSize: "0.75rem",
      letterSpacing: "0rem",
      fontWeight: 400,
      lineHeight: "1rem",
    },
    body4: {
      fontSize: "0.75rem",
      letterSpacing: "0rem",
      fontWeight: 400,
      lineHeight: "1rem",
    },
    subtitle1: {
      fontSize: "0.875rem",
      fontWeight: 400,
    },
    subtitle2: {
      fontSize: "14px",
      fontWeight: 500,
      lineHeight: "19.6px",
    },
    subtitle3: {
      fontSize: "14px",
      fontWeight: 500,
      lineHeight: "19.6px",
      color: "text.tertiary",
    },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        ".MuiPaper-elevation9, .MuiPopover-root .MuiPaper-elevation": {
          boxShadow:
            "rgb(145 158 171 / 30%) 0px 0px 2px 0px, rgb(145 158 171 / 12%) 0px 12px 24px -4px !important",
        },
      },
    },
    MuiCardHeader: {
      styleOverrides: {
        root: {
          padding: "16px 24px",
        },
        title: {
          fontSize: "1.125rem",
        },
      },
    },
    MuiCard: {
      styleOverrides: {
        root: {
          borderRadius: "10px",
          padding: "0",
          boxShadow: "0px 7px 30px 0px rgba(90, 114, 123, 0.11)",
          //backgroundColor: "#eeeeee",
          backgroundColor: "#F3F5F7",
        },
      },
    },

    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: "30px",
        },
      },
    },
    MuiBox: {
      styleOverrides: {
        root: {
          position: "relative",
          "&:before": {
            content: '""',
            background: "radial-gradient(#d2f1df, #d3d7fa, #bad8f4)",
            backgroundSize: "400% 400%",
            animation: "gradient 15s ease infinite",
            position: "absolute",
            height: "100%",
            width: "100%",
            opacity: "0.3",
          },
        },
      },
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: {
          color: "#ffffff",
          justifyContent: "center",
        },
      },
    },
    MuiListItemText: {
      styleOverrides: {
        root: {
          color: "#E8EBEE",
          textAlign: "center",
        },
      },
    },
   
    MuiTextField: {
      styleOverrides: {
        root: {
          backgroundColor: "#ffffff",
          borderRadius: "5px",
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        root: {
          backgroundColor: "#ffffff",
          borderRadius: "5px",
        },
      },
    },
  },
});

export { baselightTheme };
