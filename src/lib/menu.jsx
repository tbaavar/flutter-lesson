import {
  IconHome2,
  IconReportMoney,
  IconBusinessplan,
  IconArchive,
  IconTransfer,
  IconWriting,
  IconSettings,
  IconCircleCheck,
  IconChartInfographic,
} from "@tabler/icons-react";

import { uniqueId } from "lodash";

const Menuitems = [
  {
    id: 1,
    title: "Task",
    icon: IconHome2,
    href: "/dashboard",
    role: ["edit"],
    menu: "task"
  },
  {
    id: uniqueId(),
    title: "Trade",
    icon: IconBusinessplan,
    href: "/trade",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },
  {
    id: uniqueId(),
    title: "Operation",
    icon: IconTransfer,
    href: "/operation",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },

  {
    id: uniqueId(),
    title: "Storage",
    icon: IconArchive,
    href: "/storage",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },
  {
    id: uniqueId(),
    title: "Finance",
    icon: IconReportMoney,
    href: "/finance",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },
  {
    id: uniqueId(),
    title: "Contract",
    icon: IconWriting,
    href: "/contract",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },
  {
    id: uniqueId(),
    title: "Deal",
    icon: IconCircleCheck,
    href: "/deal",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },
  {
    id: uniqueId(),
    title: "Report",
    icon: IconChartInfographic,
    href: "/report",
    role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
  },
  {
    id: uniqueId(),
    title: "Setting",
    icon: IconSettings,
    href: "/setting",
    role: ["Admin"],
  },
];

export default Menuitems;
