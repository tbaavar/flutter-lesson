export async function fetchData(url, method, body, token) {
  try {
    const res = await fetch(`${url}`, {
      method,
      headers: {
        Accept: "application.json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(body),
    });

    const data = await res.json();

    return { status: res.status, message: data.message, result: data.result };
  } catch (error) {
    return { status: 500, message: error };
  }
}
