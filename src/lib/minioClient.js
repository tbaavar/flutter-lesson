import * as Minio from "minio";
const minioClient = new Minio.Client({
  endPoint: process.env.MINIO_ENDPOINT,
  port: 9000,
  useSSL: false,
  accessKey: process.env.MINIO_ACCESSKEY,
  secretKey: process.env.MINIO_SECRET_KEY,
});

export async function putObject(
  sourceFile,
  subdirectory,
  bucket,
  filename,
  filetype,
  filelength
) {
  try {
     
    const destinationObject = `${subdirectory}/${filename}`;

    const exists = await minioClient.bucketExists(bucket);
    if (!exists) {
      await minioClient.makeBucket(bucket, "s3");
    }
    const fileBuffer = await sourceFile.arrayBuffer();
    const buffer = Buffer.from(fileBuffer);

    if (filetype === "application/pdf") {
      var metaData = {
        "Content-Type": "application/pdf",
        example: 10,
      };
    } else if (filetype === "image/jpeg") {
      var metaData = {
        "Content-Type": "image/jpeg",
        example: 10,
      };
    } else if (filetype === "image/png") {
      var metaData = {
        "Content-Type": "image/png",
        example: 10,
      };
    } else if (filetype === "image/jpg") {
      var metaData = {
        "Content-Type": "image/jpg",
        example: 10,
      };
    } else {
      return {
        status: 500,
        message: "Error",
        result: "Not supported filetype check your file",
      };
    }
    // if (filelength >= "26214400") {
    //   return {
    //     status: 500,
    //     message: "Error",
    //     result: "File size not available",
    //   };
    // }

    await minioClient.putObject(
      bucket,
      destinationObject,
      buffer,
      metaData
    );
    // await minioClient.fPutObject(
    //   bucket,
    //   destinationObject,
    //   sourceFile,
    //   metaData
    // );

    const fullPath = {
      url: process.env.FILE_URL + bucket + "/" + destinationObject,
    };

    return {
      status: 200,
      message: "Success",
      result: fullPath,
    };
  } catch (error) {
    console.log(`err`, error)
    return {
      status: 500,
      message: "Error",
      result: "File upload error",
    };
  }
}

export async function removeObject(bucket, subdirectory, filename) {
  try {

    const subdirectoryPath = subdirectory + '/' + filename;



    console.log(subdirectoryPath)
    // const aaa = await minioClient.removeObject(bucket, sourceFile);
    const objectsStream = minioClient.listObjects(
      bucket,
      subdirectoryPath,
      true
    );

    console.log(objectsStream)

    // Delete each object in the subdirectory
    objectsStream.on("data", function (obj) {
      minioClient.removeObject(bucket, subdirectoryPath);
    });
    // objectsStream.on("data", function (obj) {
    //   minioClient.removeObject(bucket, obj.name, function (err) {
    //     if (err) {
    //       console.log(`Unable to remove object ${obj.name} from bucket ${bucket}:`, err);
    //     } else {
    //       console.log(`Successfully removed object ${obj.name} from bucket ${bucket}`);
    //     }
    //   });
    // });
    return {
      status: 200,
      message: "Success",
    };
  } catch (error) {
    console.log(`errDel`, error)
    return {
      status: 500,
      message: "Error",
    };
  }
}

export default minioClient;