export const fetcher = async (params) => {
  const [url, token] = params;

  const res = await fetch(url, {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  });
  return await res.json();
};
