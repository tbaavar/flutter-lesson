"use client";
import { createContext, useContext, useEffect, useState } from "react";

export const FinanceFilterContext = createContext();

export default function FinanceFilterProvider({ children }) {
  let storedFilter;
  let pageInfo;
  if (typeof window !== "undefined" && window.localStorage) {
    storedFilter = localStorage.getItem("filterFinance");
    pageInfo = localStorage.getItem("tradeMenu");
  }
  const [searchFilter, setSearchFilter] = useState(() => {
    return storedFilter ? JSON.parse(storedFilter) : {invoice_type: "", payment: "", counter_party: ""} || {};
  });

  // Update local storage whenever count changes
  useEffect(() => {
    localStorage.setItem("filterFinance", JSON.stringify(searchFilter));
  }, [searchFilter]);

  return (
    <FinanceFilterContext.Provider value={{ searchFilter, setSearchFilter }}>
      {children}
    </FinanceFilterContext.Provider>
  );
}

export function useFinanceFilter() {
  const context = useContext(FinanceFilterContext);
  if (!context) {
    throw new Error("useFilter must be used within a SearchFilterProvider");
  }
  return context;
}
