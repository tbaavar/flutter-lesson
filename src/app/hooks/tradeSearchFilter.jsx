"use client";
import { createContext, useContext, useEffect, useState } from "react";

export const TradeFilterContext = createContext();

export default function TradeFilterProvider({ children }) {
  let storedFilter;
  let pageInfo;
  let testFilter = {
    counter_party: "",
    trade_type: "",
    trader_user_id: "",
    product_id: "",
    trade_status: "",
  };
  if (typeof window !== "undefined" && window.localStorage) {
    storedFilter = localStorage.getItem("filterTrade");
    pageInfo = localStorage.getItem("tradeMenu");
  }
  const [searchFilter, setSearchFilter] = useState(() => {
    return storedFilter
      ? JSON.parse(storedFilter)
      : testFilter || {};
  });

  // Update local storage whenever count changes
  useEffect(() => {
    localStorage.setItem("filterTrade", JSON.stringify(searchFilter));
  }, [searchFilter]);

  return (
    <TradeFilterContext.Provider value={{ searchFilter, setSearchFilter }}>
      {children}
    </TradeFilterContext.Provider>
  );
}

export function useTradeFilter() {
  const context = useContext(TradeFilterContext);
  if (!context) {
    throw new Error("useFilter must be used within a SearchFilterProvider");
  }
  return context;
}
