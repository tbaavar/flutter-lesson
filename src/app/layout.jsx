import { ThemeProvider } from "@mui/material/styles";
import { baselightTheme } from "@/lib/theme";
import CssBaseline from "@mui/material/CssBaseline";
import { NextAuthProviders } from "@/components/NextAuthProvider";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth";
import "./global.css";
import FinanceFilterProvider from "./hooks/financeSearchFilter";
import TradeFilterProvider from "./hooks/tradeSearchFilter";

// import { Inter } from 'next/font/google'
// import './globals.css'

// const inter = Inter({ subsets: ['latin'] })

// export const metadata = {
//   title: "Vilson System",
//   description: "Trading system",
// };

export default async function RootLayout({ children }) {
  const session = await getServerSession(authOptions);

  return (
    <html lang="en" suppressHydrationWarning={true}>
      <body>
        <ThemeProvider theme={baselightTheme}>
          <CssBaseline />
          <NextAuthProviders session={session}>
            <FinanceFilterProvider>
              <TradeFilterProvider>{children}</TradeFilterProvider>
            </FinanceFilterProvider>
          </NextAuthProviders>
        </ThemeProvider>
      </body>
    </html>
  );
}
