"use client";
import {
  Box,
  Card,
  Grid,
  Stack,
  Typography,
  TextField,
  Button,
  Alert,
  FormControl,
  FormHelperText,
  InputLabel,
} from "@mui/material";
import Logo from "@/components/Logo";
import { useFormik } from "formik";
import * as yup from "yup";
import { signIn } from "next-auth/react";
import { useState } from "react";
import { useSearchParams, useRouter } from "next/navigation";

const userSchema = yup.object({
  email: yup
    .string()
    .required("Please enter your username"),
  password: yup.string().required("Please enter your password"),
});

export default function Login() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get("callbackUrl") || "/dashboard";

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: userSchema,
    onSubmit: async (values) => {
      try {
        setLoading(true);
        const res = await signIn("credentials", {
          redirect: false,
          username: values.email,
          password: values.password,
          callbackUrl,
        });

        if (!res?.error) {
          localStorage.setItem("tradeMenu", "Task");
          router.push(callbackUrl);
        } else {
          setError("Incorrect username or password!");
        }
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    },
    enableReinitialize: true,
  });

  return (
    <Box
      sx={{
        backgroundImage: "url('back.png')",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center center",
        height: "100%",
        width: "100%",
        position: "fixed",
      }}
    >
      <Grid
        container
        spacing={1}
        justifyContent="center"
        sx={{
          height: "100vh",
        }}
      >
        <Grid
          item
          xs={12}
          sm={12}
          lg={4}
          xl={3}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Card
            elevation={9}
            sx={{
              p: 4,
              zIndex: 1,
              width: "494px",
              maxWidth: "500px",
              position: "center",
              borderRadius: "24px",
              bgcolor: "#FFFFFF",
            }}
          >
            <Stack alignItems="center">
              <Logo />
            </Stack>
            <Typography variant="h2" mt={3} textAlign="center">
              Welcome back
            </Typography>
            <Typography
              variant="subtitle2"
              textAlign="center"
              color="tertiary"
              mb={1}
            >
              Please fill username and password to sign in
            </Typography>
            <form onSubmit={formik.handleSubmit}>
              <Stack spacing={2}>
                <FormControl>
                  <TextField
                    fullWidth
                    label="Username"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText
                    error={formik.touched.email && Boolean(formik.errors.email)}
                  >
                    {formik.touched.email && formik.errors.email}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <TextField
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText
                    error={
                      formik.touched.password && Boolean(formik.errors.password)
                    }
                  >
                    {formik.touched.password && formik.errors.password}
                  </FormHelperText>
                </FormControl>

                {error && (
                  <Alert color="error" severity="info" sx={{ mt: 3 }}>
                    {" "}
                    {error}
                  </Alert>
                )}
                <Button
                  type="submit"
                  fullWidth
                  size="large"
                  variant="contained"
                  sx={{ mt: 3 }}
                  disabled={loading}
                >
                  {loading ? "loading..." : "Log in"}
                </Button>
              </Stack>
            </form>
          </Card>
        </Grid>
      </Grid>
    </Box>
  );
}
