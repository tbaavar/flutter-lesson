"use client";
import React, { useEffect, useState } from "react";
import { Container, Box, styled } from "@mui/material";
// import Header from "@/components/Header";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import Sidebar from "@/components/Sidebar";
import dynamic from "next/dynamic";
import Loading from "@/components/Loading";

const Header = dynamic(() => import("@/components/Header"), { ssr: false });

const PageWrapper = styled("div")(() => ({
  display: "flex",
  flexGrow: 1,
  paddingBottom: "60px",
  flexDirection: "column",
  zIndex: 1,
  backgroundColor: "transparent",
}));
export default function MainLayout({ children }) {
  const { data: session, status } = useSession();
  const router = useRouter();

  const [isSidebarOpen, setSidebarOpen] = useState(true);
  const [isSecSideBarOpen, setSecSideBarOpen] = useState(false);
  const [isMobileSidebarOpen, setMobileSidebarOpen] = useState(false);

  useEffect(() => {
    if (!session) {
      router.push("/");
    }
  }, [session, router]);
  if (status === "loading") {
    return <Loading />;
  } else {
    return (
      <div
        style={{
          display: "flex",
          minHeight: "100vh",
          width: "100%",
        }}
      >
        <Sidebar
          isSidebarOpen={isSidebarOpen}
          isSecSideBarOpen={isSecSideBarOpen}
          isMobileSidebarOpen={isMobileSidebarOpen}
          onSidebarClose={() => setMobileSidebarOpen(false)}
          onSecSideBarClose={() => setSecSideBarOpen(false)}
        />
        <div
          style={{
            display: "flex",
            flexGrow: 1,
            paddingBottom: 60,
            flexDirection: "column",
            zIndex: 1,
            backgroundColor: "transparent",
          }}
        >
          <PageWrapper className="page-wrapper">
            <Header
              toggleMobileSidebar={() => setMobileSidebarOpen(true)}
              toggleSecSideBar={() => setSecSideBarOpen(!isSecSideBarOpen)}
            />
            <Container
              sx={{
                paddingTop: "20px",
                maxWidth: "100% !important",
              }}
            >
              {/* ------------------------------------------- */}
              {/* Page Route */}
              {/* ------------------------------------------- */}
              <Box
                sx={{
                  //minHeight: "calc(100vh-170px)",
                  minHeight: "100vh",
                }}
              >
                {children}
              </Box>
              {/* ------------------------------------------- */}
              {/* End Page */}
              {/* ------------------------------------------- */}
            </Container>
          </PageWrapper>
        </div>
      </div>
    );
  }
}
