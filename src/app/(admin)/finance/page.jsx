"use client";
import {
  Box,
  TextField,
  Button,
  Typography,
  Stack,
  CircularProgress,
  Modal,
  Alert,
  Select,
  InputLabel,
  Chip,
  MenuItem,
  IconButton,
  AlertTitle,
  FormControl,
  Link,
} from "@mui/material";
import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import MyAlert from "@/components/Myalert";
import CustomNoRowsOverlay from "@/components/NoRecord";
import InvoiceReport from "@/components/SalesReport";
import useSWR from "swr";
import { useRouter } from "next/navigation";
import dayjs from "dayjs";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import PurchaseReport from "@/components/PurchaseReport";
import { fetchData } from "@/lib/fetch";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { Clear as ClearIcon } from "@mui/icons-material";
import { useState } from "react";
import FinancePayment from "@/components/FinancePayment";
import exportToExcel from "@/components/ExcelExport";
import { useFinanceFilter } from "@/app/hooks/financeSearchFilter";
import { numberWithCommas } from "@/lib/numberWithCommas";

function FilteredComp() {
  const { data: session } = useSession();
  const { searchFilter, setSearchFilter } = useFinanceFilter();
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [filterText, setFilterText] = useState("");
  const [drawerLoading, setDrawerLoading] = useState(false);
  const [toggleReport, setToggleReport] = useState(false);
  const [toggleReport1, setToggleReport1] = useState(false);
  const [invoiceType, setinvoiceType] = useState("");
  const [open, setOpen] = useState(false);
  const [opens, setOpens] = useState(false);
  const [invoiceNumber, setInvoiceNumber] = useState("");
  const [invoiceId, setInvoiceId] = useState("");
  const [invoiceTypo, setInvoiceTypo] = useState("");
  const [startDate, setStartDate] = useState(
    dayjs(new Date(new Date().getFullYear(), 0, 1))
  );
  const [endDate, setEndDate] = useState(dayjs(new Date()));
  const [invoiceIdModal, setInvoiceIdModal] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [rowsPaymentData, setRowsPaymentData] = useState(null);
  const [paramsModal, setParamsModal] = useState();
  const [clicked, setClicked] = useState(false);

  const handleChangeFilter = (e) => {
    setSearchFilter((prev) => {
      return {
        ...prev,
        [e.target.name]: e.target.value,
      };
    });
  };
  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };
  const handleRowClick = (params) => {
    setInvoiceIdModal(params.row.invoice_id);
    setClicked(true);
  };
  // const handleClickOpen = () => {
  //   setOpen(true);
  // };

  const closeModal = () => {
    setOpenModal(false);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleClearIT = () => {
    setSearchFilter((prev) => ({ ...prev, invoice_type: "" }));
  };
  const handleClearCP = () => {
    setSearchFilter((prev) => ({ ...prev, counter_party: "" }));
  };
  const handleClearPS = () => {
    setSearchFilter((prev) => ({ ...prev, payment: "" }));
  };

  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );
  const { data: counterparty } = useSWR(
    [`/api/legal`, session?.user?.token],
    fetcher
  );

  const { data: invoice_log } = useSWR(
    invoiceIdModal
      ? [`/api/finance/log/invoice/${invoiceIdModal}`, session?.user?.token]
      : null,
    fetcher
  );
  const {
    data: invoices,
    error,
    isLoading,
    mutate,
  } = useSWR(
    [
      `/api/finance/invoice/list/${session?.user.user_id}/${startDate.format(
        "YYYY-MM-DD"
      )}/${endDate.format("YYYY-MM-DD")}`,
      session?.user?.token,
    ],
    fetcher
  );
  const dataRealChangePayment = (newValue) => {
    setRowsPaymentData(newValue);
    setParamsModal(newValue);
  };

  const columns = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 30,
    },
    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice number"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Link
            href={`/finance/invoice/detail/${params.row.invoice_id}`}
            disabled={params.row.invoice_status === "Submit"}
            variant="h7"
          >
            {params.row.invoice_number}
          </Link>
        </Box>
      ),
      width: 120,
    },
    // {
    //   field: "action3",
    //   headerName: <strong>{"Payment"}</strong>,
    //   width: 110,
    //   renderCell: (params) => {
    //     const handlePreview = () => {
    //       const currentRow = params.row;

    //       setRowsPaymentData(null);
    //       setInvoiceTypo(currentRow);
    //       setInvoiceIdModal(currentRow.invoice_id);
    //       setOpenModal(true);
    //     };
    //     if (
    //       session &&
    //       session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
    //         .permission === "Edit"
    //     ) {
    //       return (
    //         <Stack direction="row" spacing={1}>
    //           <Button
    //             startIcon={<AddSharpIcon />}
    //             variant="text"
    //             onClick={handlePreview}
    //             size="small"
    //           >
    //             Payment
    //           </Button>
    //         </Stack>
    //       );
    //     } else {
    //       return null;
    //     }
    //   },
    // },
    {
      field: "parcel_number",
      renderHeader: () => <strong>{"Parcel number"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.parcel_number} </Typography>
        </Box>
      ),
      width: 150,
    },
    {
      field: "payment_status",
      renderHeader: () => <strong>{"Status"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.invoice_payment_status}
          </Typography>
        </Box>
      ),
      width: 60,
    },
    {
      field: "trade_info",
      renderHeader: () => <strong>{"Trade Info"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.trade_info}</Typography>
          <Typography variant="h7">{"T-" + params.row.trade_id}</Typography>
        </Box>
      ),
      width: 250,
    },
    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.counterparty_name}</Typography>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}
        </Box>
      ),
      width: 170,
    },
    {
      field: "invoice_type_name",
      renderHeader: () => <strong>{"Invoice Type"}</strong>,
      alignProperty: "center",
      renderCell: (params) => (
        <Box
          sx={{
            flexDirection: "column",
            display: "flex",
            justifyContent: "center",
          }}
        >
          {" "}
          <Typography variant="h7">{params.row.invoice_state}</Typography>
          {params.row.invoice_type === "PO" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.invoice_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.invoice_type_name}</strong>
            </Typography>
          )}
        </Box>
      ),
      width: 120,
    },
    {
      field: "invoice_date",
      renderHeader: () => <strong>{"Invoice Date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.invoice_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 90,
    },

    {
      field: "invoice_due_date",
      renderHeader: () => <strong>{"Due Date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.invoice_due_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 90,
    },
    {
      field: "est_amount",
      renderHeader: () => <strong>{"Est Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.est_amount)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "invoice_amount",
      renderHeader: () => <strong>{"Invoice Amount"}</strong>,
      renderCell: (params) => {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={numberWithCommas(params.row.invoice_amount)}
            // label={
            //   params.row.invoice_amount <= 0
            //     ? 0 + " " + params.row.currency
            //     : numberWithCommas(params.row.invoice_amount) +
            //       " " +
            //       params.row.currency
            // }
          />
        );
      },
      width: 160,
      align: "right",
    },
    // {
    //   field: "difference",
    //   renderHeader: () => <strong>{"Difference"}</strong>,
    //   renderCell: (params) => (
    //     <Box sx={{ flexDirection: "column", display: "flex" }}>
    //       <Typography variant="h7">
    //         {params.row.difference <= 0
    //           ? 0 + " " + params.row.currency
    //           : numberWithCommas(params.row.difference) +
    //             " " +
    //             params.row.currency}
    //       </Typography>
    //     </Box>
    //   ),
    //   width: 140,
    // },
    {
      field: "paid_amount",
      renderHeader: () => <strong>{"Paid Amount"}</strong>,
      renderCell: (params) => {
        if (
          parseInt(params.row.paid_amount) >= parseInt(params.row.unpaid_amount)
        ) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={numberWithCommas(params.row.paid_amount)}
              // label={
              //   params.row.paid_amount <= 0
              //     ? 0 + " " + params.row.currency
              //     : numberWithCommas(params.row.paid_amount) +
              //       " " +
              //       params.row.currency
              // }
            />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={numberWithCommas(params.row.paid_amount)}
              // label={
              //   params.row.paid_amount <= 0
              //     ? 0 + " " + params.row.currency
              //     : numberWithCommas(params.row.paid_amount) +
              //       " " +
              //       params.row.currency
              // }
            />
          );
      },
      width: 160,
      align: "right",
    },
    {
      field: "unpaid_amount",
      renderHeader: () => <strong>{"Unpaid Amount"}</strong>,

      renderCell: (params) => {
        if (parseInt(params.row.unpaid_amount) <= 0) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={numberWithCommas(params.row.unpaid_amount)}
              // label={
              //   params.row.paid_amount <= 0
              //     ? 0 + " " + params.row.currency
              //     : numberWithCommas(params.row.unpaid_amount) +
              //       " " +
              //       params.row.currency
              // }
            />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={numberWithCommas(params.row.unpaid_amount)}
              // label={
              //   numberWithCommas(params.row.unpaid_amount) +
              //   " " +
              //   params.row.currency
              // }
            />
          );
      },
      width: 160,
      align: "right",
    },
  ];

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  const handleStartDateChange = (newValue) => {
    setStartDate(newValue);
  };

  const handleEndDateChange = (newValue) => {
    setEndDate(newValue);
  };
  const rows =
    invoices?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];

  const filteredRows = rows.filter(
    (row) =>
      (searchFilter.invoice_type === "" ||
        row.invoice_type === searchFilter.invoice_type) &&
      (searchFilter.payment === "" ||
        row.invoice_payment_status === searchFilter.payment) &&
      (searchFilter.counter_party === "" ||
        row.counterparty_id === searchFilter.counter_party) &&
      Object.values(row).some(
        (value) =>
          (typeof value === "string" &&
            value.toLowerCase().includes(filterText.toLowerCase())) ||
          (typeof value === "number" &&
            value.toString().includes(filterText.toLowerCase()))
      )
  );

  function calculateColumnSum(rows, invoice_amount) {
    let sum = 0;
    rows.forEach((row) => {
      sum += row[invoice_amount] || 0;
    });
    return sum;
  }

  const total = calculateColumnSum(filteredRows, "unpaid_amount");

  const delInvoice = async () => {
    const body = {
      invoice_id: invoiceId,
      username: session?.user?.username,
    };

    try {
      setOpens(true);
      const r = await fetch("/api/finance/invoice/create", {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + session?.user?.token,
        },
        body: JSON.stringify(body),
      });

      const res = await r.json();

      if (r.status === 200) {
        mutate();
        toast.success("Амжилттай хадгаллаа.", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpens(false);
      handleClose();
    }
  };

  const savePayment = async (value) => {
    const payment = rowsPaymentData?.map((row, i) => ({
      invoice_id: invoiceIdModal,
      pay_date: new Date(row.pay_date),
      value: row.value,
      payment_id: row.payment_id,
      description: row.description,
      created_by: session?.user?.username,
      last_updated_by: session?.user?.username,
    }));

    const body = {
      invoice_id: invoiceIdModal,
      invoice_mount: invoiceTypo.invoice_amount,
      payment: payment,
    };

    try {
      setLoading(true);
      setOpens(true);
      const res = await fetchData(
        `/api/finance/payment`,
        "POST",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгалалаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        mutate();
      } else {
        toast.error("Алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpens(false);
      closeModal();
    }
  };
  const handleExportExcel = () => {
    const trade = [
      {
        header: "id",
      },
      {
        header: "invoice_number",
      },
      {
        header: "invoice_payment_status",
      },
      {
        header: "trade_info",
      },
      {
        header: "trade_id",
      },
      {
        header: "parcel_number",
      },
      {
        header: "counterparty_name",
      },
      {
        header: "trade_type_name",
      },
      {
        header: "invoice_state",
      },
      {
        header: "invoice_type_name",
      },
      {
        header: "invoice_date",
      },
      {
        header: "invoice_due_date",
      },
      {
        header: "est_amount",
      },

      {
        header: "invoice_amount",
      },

      {
        header: "difference",
      },

      {
        header: "paid_amount",
      },

      {
        header: "unpaid_amount",
      },
      {
        header: "currency",
      },
    ];
    const rows =
      invoices?.result?.map((row, i) => ({
        id: i + 1,
        invoice_number: row.invoice_number,
        invoice_payment_status: row.invoice_payment_status,
        trade_info: row.trade_info,
        trade_id: row.trade_id,
        parcel_number: row.parcel_number,
        counterparty_name: row.counterparty_name,
        trade_type_name: row.trade_type_name,
        invoice_state: row.invoice_state,
        invoice_type_name: row.invoice_type_name,
        invoice_date: row.invoice_date,
        invoice_due_date: row.invoice_due_date,
        est_amount: row.est_amount,
        invoice_amount: row.invoice_amount,
        difference: row.difference,
        paid_amount: row.paid_amount,
        unpaid_amount: row.unpaid_amount,
        currency: row.currency,
      })) || [];

    exportToExcel(
      rows,
      trade,
      `FinanceReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  return (
    <>
      <ToastContainer />
      <Box sx={{ mb: 1, display: "flex", justifyContent: "space-between" }}>
        <Stack
          direction={"row"}
          spacing={1}
          justifyContent={"flex-start"}
          alignItems={"center"}
        >
          <TextField
            variant="outlined"
            size="small"
            placeholder="Search..."
            value={filterText}
            onChange={handleFilterChange}
            sx={{ minWidth: "300px" }}
          />
          <FormControl sx={{ width: "15%" }}>
            <LocalizationProvider
              dateAdapter={AdapterDayjs}
              firstDayOfWeek={0}
              sx={{ ml: -20 }}
            >
              <DatePicker
                label="Эхлэх огноо"
                format="YYYY-MM-DD"
                value={startDate}
                onChange={handleStartDateChange}
                slotProps={{ textField: { size: "small", minwidth: 200 } }}
                showDaysOutsideCurrentMonth
                displayWeekNumber
              />
            </LocalizationProvider>
          </FormControl>
          <FormControl sx={{ ml: 5, width: "15%" }}>
            <LocalizationProvider dateAdapter={AdapterDayjs} firstDayOfWeek={0}>
              <DatePicker
                label="Дуусах огноо"
                format="YYYY-MM-DD"
                value={endDate}
                onChange={handleEndDateChange}
                slotProps={{ textField: { size: "small", minwidth: 200 } }}
                showDaysOutsideCurrentMonth
                displayWeekNumber
              />
            </LocalizationProvider>
          </FormControl>
          <FormControl
            margin="normal"
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <InputLabel sx={{ mt: -0.8 }}>Invoice Type</InputLabel>
            <Stack direction="row">
              <Select
                sx={{ height: "35px", width: "200px" }}
                labelId="invoice_type"
                margin="dense"
                label="Invoice Type"
                name="invoice_type"
                onChange={handleChangeFilter}
                value={searchFilter.invoice_type || ""}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Invoice_type")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {`${item.name}`}
                      </MenuItem>
                    ))}
              </Select>
              {searchFilter.invoice_type && (
                <IconButton
                  aria-label="clear invoiceTypeFilter selection"
                  onClick={handleClearIT}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </Stack>
          </FormControl>
          <FormControl
            margin="normal"
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <InputLabel sx={{ mt: -0.8 }}>Paid/Unpaid</InputLabel>
            <Stack direction="row">
              <Select
                sx={{ height: "35px", width: "200px" }}
                labelId="payment"
                margin="dense"
                label="Paid/Unpaid"
                name="payment"
                onChange={handleChangeFilter}
                value={searchFilter.payment || ""}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Invoice_payment_status")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {`${item.name}`}
                      </MenuItem>
                    ))}
              </Select>
              {searchFilter.payment && (
                <IconButton
                  aria-label="clear paymentStatus selection"
                  onClick={handleClearPS}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </Stack>
          </FormControl>
          <FormControl
            margin="normal"
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <InputLabel sx={{ mt: -0.8 }}>Counterparty</InputLabel>
            <Stack direction="row">
              <Select
                sx={{ height: "35px", width: "200px" }}
                labelId="counter_party"
                margin="dense"
                label="Counterparty"
                name="counter_party"
                onChange={handleChangeFilter}
                value={searchFilter.counter_party || ""}
              >
                {counterparty?.result.length > 0 &&
                  counterparty?.result.map((item, i) => (
                    <MenuItem
                      value={item.legal_entity_id}
                      key={item.legal_entity_id}
                    >
                      {`${item.name}`}
                    </MenuItem>
                  ))}
              </Select>
              {searchFilter.counter_party && (
                <IconButton
                  aria-label="clear counterParty selection"
                  onClick={handleClearCP}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </Stack>
          </FormControl>
        </Stack>

        <Stack
          container
          direction="row"
          justifyContent="flex-end"
          alignItems="center"
        >
          <Box>
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
              .permission === "Edit" ? (
              <Button
                startIcon={<AddSharpIcon />}
                variant="text"
                onClick={() => router.push(`/finance/invoice/detail/new`)}
              >
                Invoice
              </Button>
            ) : null}
          </Box>
        </Stack>
      </Box>

      <Stack
        direction="row"
        sx={{ mb: 1 }}
        spacing={1}
        maxWidth={"92vw"}
        justifyContent={"space-between"}
      >
        <Stack direction="row" spacing={1} alignItems={"center"}>
          {/* <Typography variant="h5">Total Outstanding:</Typography>
          <Typography variant="h6">{total} USD</Typography> */}
        </Stack>

        <Button
          variant="outlined"
          size="small"
          sx={{ maxWidth: 205 }}
          onClick={handleExportExcel}
        >
          Export
        </Button>
      </Stack>

      <DataGrid
        getRowId={(row) => row.id}
        rows={filteredRows}
        columns={columns}
        onRowClick={handleRowClick}
        pageSizeOptions={[50]}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
        initialState={{
          ...filteredRows.initialState,
          pagination: { paginationModel: { pageSize: 20 } },
        }}
        getRowHeight={() => "auto"}
        sx={{
          backgroundColor: "#FFFFFF",
          "& .MuiDataGrid-cell:hover": {
            color: "primary.main",
          },
          "& .MuiDataGrid-cell": {
            borderTop: 1,
            borderTopColor: "#E9ECF0",
          },
          mb: 2,
          height: "auto",
          maxWidth: "92vw",
        }}
      />

      <Modal open={open} onClose={handleClose}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Delete invoice
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{invoiceNumber}</AlertTitle>
                are you sure delete!
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                delInvoice();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              Yes
            </Button>
            <Button onClick={handleClose} variant="outlined">
              No
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal
        open={openModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <FinancePayment
          dataRealChange={dataRealChangePayment}
          invoiceTypo={invoiceTypo}
          onClose={closeModal}
          rows={rowsPaymentData}
          setRows={setRowsPaymentData}
          savePayment={savePayment}
          invoiceId={invoiceIdModal}
        />
      </Modal>
      <React.Fragment>
        <PurchaseReport
          loading={drawerLoading}
          setToggleReport1={setToggleReport1}
          toggleReport1={toggleReport1}
        />
        <InvoiceReport
          loading={drawerLoading}
          setDrawerLoading={setDrawerLoading}
          setToggleReport={setToggleReport}
          toggleReport={toggleReport}
          invoiceType={invoiceType}
        />
      </React.Fragment>
      <Modal open={opens}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
function Finance() {
  return <FilteredComp />;
}
export default Finance;
