"use client";
import {
  Box,
  Radio,
  TextField,
  InputLabel,
  MenuItem,
  FormControl,
  Grid,
  Select,
  Button,
  Typography,
  Stack,
  Tabs,
  Alert,
  Tab,
  IconButton,
  Card,
  CircularProgress,
  FormHelperText,
  RadioGroup,
  FormControlLabel,
  Modal,
  Tooltip,
  Input,
  Divider,
  InputAdornment,
} from "@mui/material";
import * as React from "react";
import MyAlert from "@/components/Myalert";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import CustomNoRowsOverlay from "@/components/NoRecord";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import useSWR from "swr";
import { AntTabs } from "@/components/CustomTabControl";
import { AntTab } from "@/components/CustomTabControl";
import * as yup from "yup";
import { a11yProps } from "@/components/CustomTabControl";
import { CustomTabPanel } from "@/components/CustomTabControl";
import { Formik, useFormik } from "formik";
import dayjs from "dayjs";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import CloseIcon from "@mui/icons-material/Close";
import { useRouter } from "next/navigation";
import InvoiceReport from "@/components/SalesReport";
import PurchaseReport from "@/components/PurchaseReport";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import { fetchData } from "@/lib/fetch";
import DebitsDatagrid from "@/components/DebitsDatagrid";
import CreditDatagrid from "@/components/CreditDatagrid";
import { numberWithCommas } from "@/lib/numberWithCommas";

// const AntTabs = styled(Tabs)({
//   borderBottom: "1px solid #e8e8e8",
//   "& .MuiTabs-indicator": {
//     backgroundColor: "#3b8188",
//   },
// });

// const AntTab = styled((props) => <Tab disableRipple {...props} />)(
//   ({ theme }) => ({
//     textTransform: "none",
//     minWidth: 0,
//     [theme.breakpoints.up("sm")]: {
//       minWidth: 0,
//     },
//     fontWeight: theme.typography.fontWeightRegular,
//     marginRight: theme.spacing(1),
//     color: "rgba(0, 0, 0, 0.85)",
//     fontFamily: [
//       "-apple-system",
//       "BlinkMacSystemFont",
//       '"Segoe UI"',
//       "Roboto",
//       '"Helvetica Neue"',
//       "Arial",
//       "sans-serif",
//       '"Apple Color Emoji"',
//       '"Segoe UI Emoji"',
//       '"Segoe UI Symbol"',
//     ].join(","),
//     "&:hover": {
//       color: "#3b8188",
//       opacity: 1,
//     },
//     "&.Mui-selected": {
//       color: "#3b8188",
//       fontWeight: theme.typography.fontWeightMedium,
//     },
//     "&.Mui-focusVisible": {
//       backgroundColor: "#3b8188",
//     },
//   })
// );

// function CustomTabPanel(props) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`invoice-tabpanel-${index}`}
//       aria-labelledby={`invoice-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box sx={{ mt: 1, mb: 1 }}>
//           <Typography component={"span"}>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// CustomTabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.number.isRequired,
//   value: PropTypes.number.isRequired,
// };

// function a11yProps(index) {
//   return {
//     id: `invoice-tab-${index}`,
//     "aria-controls": `invoice-tabpanel-${index}`,
//   };
// }

export default function InvoiceAddPage({ params }) {
  const { data: session } = useSession();
  const { invoice_id } = params;
  const router = useRouter();

  const [value, setValue] = React.useState(0);
  let valueDebit = 0;
  let valueCredit = 0;
  const id = params.invoice_id;
  const [open, setOpen] = React.useState(false);
  const [opens, setOpens] = React.useState(false);
  const [image, setImage] = React.useState(null);
  const [openAttachment, setOpenAttachment] = React.useState(false);
  const [drawerLoading, setDrawerLoading] = React.useState(false);
  const [toggleReport, setToggleReport] = React.useState(false);
  const [toggleReport1, setToggleReport1] = React.useState(false);
  const [methodattachment, setMethodattachment] = React.useState();
  const [loading, setLoading] = React.useState(false);
  const [rowsDebitData, setRowsDebitData] = React.useState(null);
  const [rowsCreditData, setRowsCreditData] = React.useState(null);
  const [counterPartyId, setCounterpartyId] = React.useState("");
  const [bankId, setBankId] = React.useState("");

  const handleAttachmentOpen = () => {
    setOpenAttachment(true);
  };

  const handleAttachmentClose = () => {
    setOpenAttachment(false);
  };
  const handleCloses = () => {
    setOpens(false);
  };
  const handleClickOpen = () => {
    setOpens(true);
  };
  const handleTabChange = (event, newValue) => {
    setValue(newValue);
  };
  const uploadToClient = (event) => {
    if (event.target.files && event.target.files[0]) {
      const i = event.target.files[0];
      setImage(i);
      formikAttachment.setFieldValue("filename", event.target.files[0].name);
      formikAttachment.setFieldValue("filelength", event.target.files[0].size);
      formikAttachment.setFieldValue("filetype", event.target.files[0].type);
      formikAttachment.setFieldValue("sourcefile", i);
    }
  };

  const colAttachment = [
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 100,
      renderCell: (params) => {
        const handleEdit = () => {
          const currentRow = params.row;

          formikAttachment.setFieldValue(
            "invoice_attachment_id",
            currentRow.invoice_attachment_id
          );

          formikAttachment.setFieldValue(
            "attachment_name",
            currentRow.attachment_name
          );
          formikAttachment.setFieldValue(
            "fileDescription",
            currentRow.description
          );

          setMethodattachment("PUT");
          handleAttachmentOpen();
        };
        const handleDelete = (e) => {
          const currentRow = params.row;

          deleteInvoice(currentRow);
        };
        const handlePreview = () => {
          window.open(`${params.row.attachment_url}`, "_blank");
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleDelete}
              />
            </Tooltip>
            <Tooltip title="Preview">
              <GridActionsCellItem
                icon={<VisibilityIcon />}
                label="Preview"
                color="primary"
                onClick={handlePreview}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
    { field: "id", renderHeader: () => <strong>{"ID"}</strong>, width: 90 },
    {
      field: "attachment_name",
      renderHeader: () => <strong>{"Attachment name"}</strong>,
      width: 150,
    },

    {
      field: "attachment_type",
      renderHeader: () => <strong>{"Attachment type"}</strong>,
      width: 150,
    },
    {
      field: "filename",
      renderHeader: () => <strong>{"Filename"}</strong>,
      width: 150,
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Attachment date"}</strong>,
      width: 150,
      valueGetter: (params) =>
        dayjs(params.row.creation_date).format("YYYY-MM-DD"),
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 250,
    },
  ];
  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );

  const { data: trades } = useSWR(
    [`/api/finance/trade/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );

  const { data: companys } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );

  const { data: counterpartys } = useSWR(
    [`/api/legal`, session?.user?.token],
    fetcher
  );

  const { data: banks } = useSWR(
    [`/api/finance/bank`, session?.user?.token],
    fetcher
  );

  const { data: attachments } = useSWR(
    invoice_id !== "new"
      ? [`/api/finance/invoice/attachments/${invoice_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const {
    data: infoInvoice,
    error,
    isLoading,
    mutate,
  } = useSWR(
    invoice_id !== "new"
      ? [`/api/finance/invoice/${invoice_id}`, session?.user?.token]
      : null,
    fetcher
  );
  const { data: accounts } = useSWR(
    bankId && counterPartyId
      ? [`/api/finance/bank/${counterPartyId}/${bankId}`, session?.user?.token]
      : infoInvoice?.result[0]?.counterparty_id &&
        infoInvoice?.result[0]?.bank_id
      ? [
          `/api/finance/bank/${infoInvoice?.result[0]?.counterparty_id}/${infoInvoice?.result[0]?.bank_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const rowsA = attachments?.result?.map((row, i) => ({ id: i + 1, ...row }));

  const dataRealChange = (newValue) => {
    setRowsDebitData(newValue);
  };

  if (rowsDebitData) {
    if (rowsDebitData.length > 0) {
      for (let i = 0; rowsDebitData.length > i; i++) {
        valueDebit = valueDebit + rowsDebitData[i].value;
      }
    }
  }

  const dataRealChangeCredit = (newValue) => {
    setRowsCreditData(newValue);
  };

  if (rowsCreditData) {
    if (rowsCreditData.length > 0) {
      for (let i = 0; rowsCreditData.length > i; i++) {
        valueCredit = valueCredit + rowsCreditData[i].value;
      }
    }
  }

  const formikAttachment = useFormik({
    initialValues: {
      invoice_attachment_id: "",
      attachment_name: "",
      fileDescription: "",
      attachmentname: "",
      attachmenttype: "",
      description: "",
      filename: "",
      filetype: "",
      attachmenturl: "",
      sourcefile: "",
      method: null,
    },
    validationSchema: yup.object({
      filename: yup.string().required("Please choose File!"),
    }),
    onSubmit: async (values) => {
      saveAttachment(values);
      handleAttachmentClose();
    },
    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const saveInvoice = async (data) => {
    let fullData = null;

    const bodyDebit = rowsDebitData?.map((row, i) => ({
      invoice_detail_id: row.invoice_detail_id ? row.invoice_detail_id : null,
      offset_inv_id: null,
      balance_type: "Debit",
      type: row.type,
      description: row.description,
      cost_code: row.cost,
      value: row.value,
      tran_date: dayjs(row.tran_date).format("YYYY-MM-DD"),
      exp_pay_date: dayjs(row.exp_pay_date).format("YYYY-MM-DD"),
    }));

    const bodyCredit = rowsCreditData?.map((row, i) => ({
      invoice_detail_id: row.invoice_detail_id ? row.invoice_detail_id : null,
      offset_inv_id: row.offset_inv_id ? row.offset_inv_id : null,
      balance_type: "Credit",
      type: row.type,
      description: row.description,
      cost_code: row.cost,
      value: row.value,
      tran_date: dayjs(row.tran_date).format("YYYY-MM-DD"),
      exp_pay_date: dayjs(row.exp_pay_date).format("YYYY-MM-DD"),
    }));

    if (bodyCredit) {
      fullData = bodyDebit.concat(bodyCredit);
    } else {
      fullData = bodyDebit;
    }

    const body = {
      voyage_parcel_id: data.voyage_parcel_id,
      company_id: data.company,
      company_contact: data.contactMail,
      counterparty_id: data.counterParty,
      counterparty_contact: data.counterPartyContact,
      invoice_owner_id: data.invoice_owner_id,
      invoice_number: data.invoiceNo,
      invoice_state: data.invoiceState,
      creditor_invoice_number: data.creditorInvoiceNumber,
      invoice_type: data.invoiceType,
      invoice_date: data.invoiceDate,
      invoice_due_date: data.dueDate,
      bank_id: data.bank,
      account_id: data.account,
      invoice_amount: valueDebit,
      // invoice_amount: valueDebit - valueCredit,
      currency: data.currency,
      invoice_description: data.invoiceDesc,
      invoice_comments: data.comments,
      username: session?.user?.username,
      invoice_id: invoice_id === "new" ? undefined : invoice_id,
      detail: fullData,
    };
    const method = invoice_id === "new" ? "POST" : "PUT";
    try {
      setLoading(true);
      setOpen(true);

      const r = await fetch(`/api/finance/invoice/save`, {
        method: method,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + session?.user?.token,
        },
        body: JSON.stringify(body),
      });

      const res = await r.json();

      if (r.status === 200) {
        mutate;
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });

        if (method === "POST") {
          router.push(`/finance/invoice/detail/${res.result.invoice_id}`);
        }
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
      setOpen(false);
      router.push(`/finance`);
    }
  };
  const submitInvoice = async () => {
    if (invoice_id === "new") {
      return toast.error("Invoice хадгалаагүй байна", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }

    const body = {
      username: session?.user?.username,
      invoice_id: invoice_id,
    };

    try {
      setLoading(true);
      const res = await fetchData(
        `/api/finance/invoice/submit`,
        "PUT",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  const deleteInvoice = async (data) => {
    try {
      setOpen(true);
      const body = {
        invoice_id: invoice_id,
        username: session?.user?.username,
        bucket: "invoice",
        id: invoice_id,
        filename: data.filename,
        invoice_attachment_id: data.invoice_attachment_id,
      };

      const res = await fetchData(
        `/api/finance/invoice/attachments`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        mutate();
        toast.success("Амжилттай устгалаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Устгах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpen(false);
    }
  };

  const saveAttachment = async (data) => {
    if (invoice_id === "new") {
      toast.error("Invoice үүсээгүй байна", {
        position: toast.POSITION.TOP_RIGHT,
      });
      handleAttachmentClose();
    } else {
      setOpen(true);
      setLoading(true);
      const formData = new FormData();
      formData.append("id", invoice_id);
      formData.append("bucket", "invoice");
      formData.append("invoice_id", invoice_id);
      formData.append("attachment_type", data.filetype);
      formData.append("attachment_name", data.attachmentname);
      formData.append("filename", data.filename);
      formData.append("description", data.description);
      formData.append("username", session?.user?.username);
      formData.append("filelength", data.filelength);
      formData.append(
        "invoice_attachment_id",
        methodattachment === "POST" ? undefined : data.invoice_attachment_id
      );
      formData.append("sourcefile", data.sourcefile);
      try {
        const res = await fetch(`/api/finance/invoice/attachments`, {
          method: methodattachment,
          headers: {
            Authorization: `Bearer ${session?.user?.token}`,
          },
          body: formData,
        });
        if (res.status == 200) {
          mutate();
          toast.success("Амжилттай хадгаллаа", {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error(res.result, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
        toast.error(error, {
          position: toast.POSITION.TOP_RIGHT,
        });
      } finally {
        setLoading(false);
        setOpen(false);
      }
    }
  };
  const delInvoice = async (params) => {
    const body = {
      invoice_id: id,
      username: session?.user?.username,
    };

    try {
      setOpen(true);
      const r = await fetch("/api/finance/invoice/create", {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + session?.user?.token,
        },
        body: JSON.stringify(body),
      });

      const res = await r.json();

      if (r.status === 200) {
        mutate();
        toast.success("Амжилттай устгалаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpen(false);
      handleCloses();
    }
  };
  console.log("asdasdasdads", valueCredit);
  return (
    <>
      <ToastContainer />
      <IconButton
        aria-label="back"
        size="meduim"
        onClick={() => router.push(`/finance`)}
      >
        <ArrowBackIcon fontSize="inherit" />
        <Typography color="text.primary" variant="subtitle2">
          Back
        </Typography>
      </IconButton>

      <Formik
        initialValues={{
          invoiceId: infoInvoice?.result[0]?.invoice_id ?? "",
          invoiceNo:
            infoInvoice?.result[0]?.invoice_number ??
            dayjs().format("YYMMDDHHMMms").toString(),
          invoiceType: infoInvoice?.result[0]?.invoice_type ?? "",
          amount: infoInvoice?.result[0]?.amount ?? "",
          shipping_quantity_mt:
            infoInvoice?.result[0]?.shipping_quantity_mt ?? "",
          invoicing_unit: infoInvoice?.result[0]?.invoicing_unit ?? "",
          invoiceTypeName: infoInvoice?.result[0]?.invoice_type_name ?? "",
          invoiceState: infoInvoice?.result[0]?.invoice_state ?? "Provisional",
          bank: infoInvoice?.result[0]?.bank_id ?? "",
          account: infoInvoice?.result[0]?.account_id ?? "",
          company: infoInvoice?.result[0]?.company_id ?? "",
          counterParty: infoInvoice?.result[0]?.counterparty_id ?? "",
          counterPartyContact:
            infoInvoice?.result[0]?.counterparty_contact ?? "",
          creditorInvoiceNumber:
            infoInvoice?.result[0]?.creditor_invoice_number ?? "",
          invoiceDate:
            infoInvoice?.result[0]?.invoice_date ??
            dayjs().format("YYYY-MM-DD"),
          dueDate:
            infoInvoice?.result[0]?.invoice_due_date ??
            dayjs().format("YYYY-MM-DD"),
          invoiceAmount: infoInvoice?.result[0]?.invoice_amount ?? "0",
          currency: infoInvoice?.result[0]?.currency ?? "MNT",
          contactMail: infoInvoice?.result[0]?.company_contact ?? "",
          invoiceDesc: infoInvoice?.result[0]?.invoice_description ?? "",
          comments: infoInvoice?.result[0]?.invoice_comments ?? "",
          trade_id: infoInvoice?.result[0]?.trade_id ?? "",
          voyage_parcel_id: infoInvoice?.result[0]?.voyage_parcel_id ?? "",
          invoice_owner_id: infoInvoice?.result[0]?.invoice_owner_id ?? "",
        }}
        validationSchema={yup.object({
          trade_id: yup.string().required("Please select Trade"),
          voyage_parcel_id: yup.string().required("Please select parcel"),
          invoiceType: yup.string().required("Please select invoice type!"),
          company: yup.string().required("Please select company!"),
          counterParty: yup.string().required("Please select counterparty"),
          invoiceState: yup.string().required("Please select state!"),
          invoiceDate: yup
            .date()
            .required("Please choose invoice date")
            .typeError("Please choose invoice date"),
          dueDate: yup
            .date()
            .required("Please choose due date")
            .typeError("Please choose due date"),
          invoiceAmount: yup
            .number()

            .typeError("Please insert number!")
            .required("Please insert amount!"),
        })}
        onSubmit={async (values) => {
          saveInvoice(values);
        }}
        enableReinitialize={false}
      >
        {(formik) => (
          <form onSubmit={formik.handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Card>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      m: 1,
                    }}
                  >
                    <Typography variant="h5" sx={{ p: 1 }}>
                      Invoice
                    </Typography>
                    <Stack direction="row" spacing={1}>
                      {session &&
                      session?.user?.menuitems.filter(
                        (f) => f.menu_name === "Finance"
                      )[0].permission === "Edit" ? (
                        <>
                          <Button
                            variant="outlined"
                            size="small"
                            onClick={handleClickOpen}
                            color="error"
                          >
                            Delete
                          </Button>
                          <Button
                            variant="contained"
                            type="submit"
                            color="dark"
                            size="small"
                          >
                            Save
                          </Button>
                          {/* <Button
                            variant="contained"
                            size="small"
                            onClick={async () => {
                              submitInvoice();
                            }}
                          >
                            Submit
                          </Button> */}
                        </>
                      ) : null}
                    </Stack>
                  </Box>
                  <Box sx={{ p: 1 }}>
                    {invoice_id === "new" ? (
                      <Stack direction="row" spacing={1}>
                        <FormControl fullWidth>
                          <InputLabel size="small" sx={{ color: "#0B626B" }}>
                            Trade
                          </InputLabel>
                          <Select
                            name="trade_id"
                            size="small"
                            label="Trade"
                            value={formik.values.trade_id}
                            onChange={(e) => {
                              formik.setValues({
                                ...formik.values,
                                trade_id: e.target.value,
                              });
                            }}
                          >
                            {trades?.result.length > 0 &&
                              trades?.result.map((item) => (
                                <MenuItem
                                  value={item.trade_id}
                                  key={item.trade_id}
                                >
                                  {item.trade_name + "[" + item.trade_id + "]"}
                                </MenuItem>
                              ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.trade_id &&
                              Boolean(formik.errors.trade_id)
                            }
                          >
                            {formik.touched.trade_id && formik.errors.trade_id}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <InputLabel size="small" sx={{ color: "#0B626B" }}>
                            Parcel
                          </InputLabel>
                          <Select
                            name="voyage_parcel_id"
                            size="small"
                            label="Parcel"
                            value={formik.values.voyage_parcel_id}
                            onChange={(e) => {
                              const selectedParcelId = e.target.value;
                              const allparcels = trades?.result.flatMap(
                                (parcels) => parcels?.parcels || []
                              );

                              const selectedParcel = allparcels.find(
                                (item) =>
                                  item.voyage_parcel_id === selectedParcelId
                              );

                              if (selectedParcel) {
                                formik.setValues({
                                  ...formik.values,
                                  voyage_parcel_id:
                                    selectedParcel.voyage_parcel_id,
                                  invoice_owner_id:
                                    selectedParcel.operator_user_id,
                                  counterParty: selectedParcel.counterparty_id,
                                  amount: selectedParcel.amount,
                                  shipping_quantity_mt:
                                    selectedParcel.shipping_quantity_mt,
                                  invoicing_unit: selectedParcel.invoicing_unit,
                                });
                              }
                            }}
                          >
                            {trades?.result.filter(
                              (f) => f.trade_id === formik.values.trade_id
                            ).length > 0 &&
                              trades?.result
                                .filter(
                                  (f) => f.trade_id === formik.values.trade_id
                                )[0]
                                .parcels.map((item) => (
                                  <MenuItem
                                    value={item.voyage_parcel_id}
                                    key={item.voyage_parcel_id}
                                  >
                                    {item.shipping_quantity_mt +
                                      " " +
                                      item.invoicing_unit +
                                      "-" +
                                      item.product_name +
                                      " " +
                                      item.grade_name +
                                      " -<" +
                                      item.trade_code +
                                      "> -" +
                                      item.counterparty_name +
                                      "-" +
                                      dayjs(item.transfer_date).format(
                                        "YYYY-MM-DD"
                                      ) +
                                      " [" +
                                      item.parcel_number +
                                      " ]"}
                                  </MenuItem>
                                ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.voyage_parcel_id &&
                              Boolean(formik.errors.voyage_parcel_id)
                            }
                          >
                            {formik.touched.voyage_parcel_id &&
                              formik.errors.voyage_parcel_id}
                          </FormHelperText>
                        </FormControl>
                      </Stack>
                    ) : (
                      <Stack direction="row" spacing={1}>
                        <FormControl fullWidth>
                          <InputLabel size="small" sx={{ color: "#0B626B" }}>
                            Trade
                          </InputLabel>
                          <Select
                            readOnly
                            name="trade_id"
                            size="small"
                            label="Trade"
                            value={formik.values.trade_id}
                            onChange={(e) => {
                              formik.setValues({
                                ...formik.values,
                                trade_id: e.target.value,
                              });
                            }}
                          >
                            {trades?.result.length > 0 &&
                              trades?.result.map((item) => (
                                <MenuItem
                                  value={item.trade_id}
                                  key={item.trade_id}
                                >
                                  {item.trade_name + "[" + item.trade_id + "]"}
                                </MenuItem>
                              ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.trade_id &&
                              Boolean(formik.errors.trade_id)
                            }
                          >
                            {formik.touched.trade_id && formik.errors.trade_id}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <InputLabel size="small" sx={{ color: "#0B626B" }}>
                            Parcel
                          </InputLabel>
                          <Select
                            readOnly
                            name="voyage_parcel_id"
                            size="small"
                            label="Parcel"
                            value={formik.values.voyage_parcel_id}
                            onChange={(e) => {
                              const selectedParcelId = e.target.value;
                              const allparcels = trades?.result.flatMap(
                                (parcels) => parcels?.parcels || []
                              );

                              const selectedParcel = allparcels.find(
                                (item) =>
                                  item.voyage_parcel_id === selectedParcelId
                              );

                              if (selectedParcel) {
                                formik.setValues({
                                  ...formik.values,
                                  voyage_parcel_id:
                                    selectedParcel.voyage_parcel_id,
                                  invoice_owner_id:
                                    selectedParcel.operator_user_id,
                                  counterParty: selectedParcel.counterparty_id,
                                  amount: selectedParcel.amount,
                                  shipping_quantity_mt:
                                    selectedParcel.shipping_quantity_mt,
                                  invoicing_unit: selectedParcel.invoicing_unit,
                                });
                              }
                            }}
                          >
                            {trades?.result.filter(
                              (f) => f.trade_id === formik.values.trade_id
                            ).length > 0 &&
                              trades?.result
                                .filter(
                                  (f) => f.trade_id === formik.values.trade_id
                                )[0]
                                .parcels.map((item) => (
                                  <MenuItem
                                    value={item.voyage_parcel_id}
                                    key={item.voyage_parcel_id}
                                  >
                                    {item.shipping_quantity_mt +
                                      " " +
                                      item.invoicing_unit +
                                      "-" +
                                      item.product_name +
                                      " " +
                                      item.grade_name +
                                      " -<" +
                                      item.trade_code +
                                      "> -" +
                                      item.counterparty_name +
                                      "-" +
                                      dayjs(item.transfer_date).format(
                                        "YYYY-MM-DD"
                                      ) +
                                      " [" +
                                      item.parcel_number +
                                      " ]"}
                                  </MenuItem>
                                ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.voyage_parcel_id &&
                              Boolean(formik.errors.voyage_parcel_id)
                            }
                          >
                            {formik.touched.voyage_parcel_id &&
                              formik.errors.voyage_parcel_id}
                          </FormHelperText>
                        </FormControl>
                      </Stack>
                    )}
                  </Box>
                </Card>
              </Grid>
              <Grid item xs={12}>
                <Card>
                  <Box sx={{ ml: 1, mr: 1 }}>
                    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                      <AntTabs
                        value={value}
                        onChange={handleTabChange}
                        aria-label="invoice_tab"
                      >
                        <AntTab label="Invoice" {...a11yProps(0)} />
                        <AntTab label="Comments" {...a11yProps(1)} />
                        <AntTab label="Attachments" {...a11yProps(2)} />
                      </AntTabs>
                    </Box>
                    <CustomTabPanel value={value} index={0}>
                      <Card>
                        <Box
                          sx={{
                            m: 1,
                          }}
                        >
                          <Stack direction="column" spacing={1}>
                            <Stack direction="row" spacing={1}>
                              <FormControl fullWidth>
                                <TextField
                                  name="invoiceNo"
                                  label="Invoice No"
                                  variant="outlined"
                                  readOnly={true}
                                  size="small"
                                  value={formik.values.invoiceNo}
                                  onChange={formik.handleChange}
                                />
                              </FormControl>
                              <FormControl fullWidth>
                                <InputLabel
                                  id="invoiceTypeName-label"
                                  size="small"
                                  sx={{ color: " #0B626B" }}
                                >
                                  Invoice Type
                                </InputLabel>
                                <Select
                                  labelId="invoiceTypeNamey-label"
                                  name="invoiceType"
                                  size="small"
                                  label="Invoice Type"
                                  value={formik.values.invoiceType}
                                  onChange={formik.handleChange}
                                >
                                  {lookup?.result.length >= 0 &&
                                    lookup?.result
                                      .filter(
                                        (f) => f.lookup_type === "Invoice_type"
                                      )
                                      .map((item) => (
                                        <MenuItem
                                          value={item.lookup_code}
                                          key={item.lookup_code}
                                        >
                                          {item.name}
                                        </MenuItem>
                                      ))}
                                </Select>
                                <FormHelperText
                                  error={
                                    formik.touched.invoiceType &&
                                    Boolean(formik.errors.invoiceType)
                                  }
                                >
                                  {formik.touched.invoiceType &&
                                    formik.errors.invoiceType}
                                </FormHelperText>
                              </FormControl>
                              <FormControl fullWidth>
                                <RadioGroup
                                  row
                                  aria-labelledby="invoice-state-label"
                                  name="invoiceState"
                                  value={formik.values.invoiceState}
                                  sx={{ mb: -3 }}
                                  onChange={async (e) => {
                                    formik.handleChange(e);
                                  }}
                                >
                                  <FormControlLabel
                                    value="Provisional"
                                    control={<Radio />}
                                    label="Provisional"
                                  />
                                  <FormControlLabel
                                    value="Final"
                                    control={<Radio />}
                                    label="Final"
                                  />
                                </RadioGroup>
                                <FormHelperText
                                  error={
                                    formik.touched.invoiceState &&
                                    Boolean(formik.errors.invoiceState)
                                  }
                                >
                                  {formik.touched.invoiceState &&
                                    formik.errors.invoiceState}
                                </FormHelperText>
                              </FormControl>
                              <FormControl fullWidth></FormControl>
                            </Stack>
                            <Stack direction="row" spacing={1}>
                              <FormControl fullWidth>
                                <InputLabel
                                  id="counterparty-label"
                                  size="small"
                                  sx={{ color: " #0B626B" }}
                                >
                                  Counterparty
                                </InputLabel>
                                <Select
                                  labelId="counterparty-label"
                                  name="counterParty"
                                  size="small"
                                  label="Counterparty"
                                  value={formik.values.counterParty}
                                  onChange={(e) => {
                                    formik.setFieldValue(
                                      "counterParty",
                                      e.target.value
                                    );
                                    setCounterpartyId(e.target.value);
                                  }}
                                >
                                  {counterpartys?.result.length >= 0 &&
                                    counterpartys?.result.map((item) => (
                                      <MenuItem
                                        value={item.legal_entity_id}
                                        key={item.legal_entity_id}
                                      >
                                        {item.name}
                                      </MenuItem>
                                    ))}
                                </Select>
                                <FormHelperText
                                  error={
                                    formik.touched.counterParty &&
                                    Boolean(formik.errors.counterParty)
                                  }
                                >
                                  {formik.touched.counterParty &&
                                    formik.errors.counterParty}
                                </FormHelperText>
                              </FormControl>
                              <FormControl fullWidth>
                                <TextField
                                  name="counterPartyContact"
                                  label="Counterparty email"
                                  type="email"
                                  variant="outlined"
                                  size="small"
                                  value={formik.values.counterPartyContact}
                                  onChange={formik.handleChange}
                                />
                              </FormControl>
                            </Stack>
                            {formik.values.invoiceType === "SI" ? (
                              <>
                                <Stack direction="row" spacing={1}>
                                  <FormControl fullWidth>
                                    <InputLabel
                                      id="bank-label"
                                      size="small"
                                      sx={{ color: " #0B626B" }}
                                    >
                                      Bank
                                    </InputLabel>
                                    <Select
                                      labelId="bank-label"
                                      name="bank"
                                      value={formik.values.bank}
                                      onChange={(e) => {
                                        formik.setFieldValue(
                                          "bank",
                                          e.target.value
                                        );
                                        setBankId(e.target.value);
                                      }}
                                      // onChange={formik.handleChange}
                                      size="small"
                                      label="Bank"
                                    >
                                      {banks?.result.length >= 0 &&
                                        banks?.result.map((item) => (
                                          <MenuItem
                                            value={item.bank_id}
                                            key={item.bank_id}
                                          >
                                            {item.bank_name}
                                          </MenuItem>
                                        ))}
                                    </Select>
                                    <FormHelperText
                                      error={
                                        formik.touched.bank &&
                                        Boolean(formik.errors.bank)
                                      }
                                    >
                                      {formik.touched.bank &&
                                        formik.errors.bank}
                                    </FormHelperText>
                                  </FormControl>
                                  <FormControl fullWidth>
                                    <InputLabel
                                      id="account-label"
                                      size="small"
                                      sx={{ color: " #0B626B" }}
                                    >
                                      Account
                                    </InputLabel>
                                    <Select
                                      labelId="account-label"
                                      name="account"
                                      size="small"
                                      label="Account"
                                      value={formik.values.account}
                                      onChange={formik.handleChange}
                                    >
                                      {accounts?.result.length >= 0 &&
                                        accounts?.result.map((item) => (
                                          <MenuItem
                                            value={item.account_id}
                                            key={item.account_id}
                                          >
                                            {item.account_name +
                                              " " +
                                              item.account_no}
                                          </MenuItem>
                                        ))}
                                    </Select>
                                    <FormHelperText
                                      error={
                                        formik.touched.account &&
                                        Boolean(formik.errors.account)
                                      }
                                    >
                                      {formik.touched.account &&
                                        formik.errors.account}
                                    </FormHelperText>
                                  </FormControl>
                                </Stack>
                              </>
                            ) : null}

                            <Stack direction="row" spacing={1}>
                              <FormControl fullWidth>
                                <InputLabel
                                  id="company-label"
                                  size="small"
                                  sx={{ color: " #0B626B" }}
                                >
                                  Company
                                </InputLabel>
                                <Select
                                  labelId="company-label"
                                  name="company"
                                  size="small"
                                  label="Company"
                                  value={formik.values.company}
                                  onChange={formik.handleChange}
                                >
                                  {companys?.result.length >= 0 &&
                                    companys?.result.map((item) => (
                                      <MenuItem
                                        value={item.org_id}
                                        key={item.org_id}
                                      >
                                        {item.org_name}
                                      </MenuItem>
                                    ))}
                                </Select>
                                <FormHelperText
                                  error={
                                    formik.touched.company &&
                                    Boolean(formik.errors.company)
                                  }
                                >
                                  {formik.touched.company &&
                                    formik.errors.company}
                                </FormHelperText>
                              </FormControl>
                              <FormControl fullWidth>
                                <TextField
                                  name="contactMail"
                                  label="Company email"
                                  variant="outlined"
                                  type="email"
                                  size="small"
                                  value={formik.values.contactMail}
                                  onChange={formik.handleChange}
                                />
                              </FormControl>
                            </Stack>

                            <Stack direction="row" spacing={1}>
                              <FormControl fullWidth>
                                <LocalizationProvider
                                  dateAdapter={AdapterDayjs}
                                >
                                  <DatePicker
                                    format="YYYY-MM-DD"
                                    name="invoiceDate"
                                    slotProps={{ textField: { size: "small" } }}
                                    label="Invoice date"
                                    value={dayjs(formik.values.invoiceDate)}
                                    onChange={(newValue) => {
                                      formik.setFieldValue(
                                        "invoiceDate",
                                        dayjs(newValue).format("YYYY-MM-DD")
                                      );
                                    }}
                                  />
                                </LocalizationProvider>
                                <FormHelperText
                                  error={
                                    formik.touched.invoiceDate &&
                                    Boolean(formik.errors.invoiceDate)
                                  }
                                >
                                  {formik.touched.invoiceDate &&
                                    formik.errors.invoiceDate}
                                </FormHelperText>
                              </FormControl>
                              <FormControl fullWidth>
                                <LocalizationProvider
                                  dateAdapter={AdapterDayjs}
                                >
                                  <DatePicker
                                    format="YYYY-MM-DD"
                                    name="dueDate"
                                    slotProps={{ textField: { size: "small" } }}
                                    label="Due date"
                                    value={dayjs(formik.values.dueDate)}
                                    onChange={(newValue) => {
                                      formik.setFieldValue(
                                        "dueDate",
                                        dayjs(newValue).format("YYYY-MM-DD")
                                      );
                                    }}
                                  />
                                </LocalizationProvider>
                                <FormHelperText
                                  error={
                                    formik.touched.dueDate &&
                                    Boolean(formik.errors.dueDate)
                                  }
                                >
                                  {formik.touched.dueDate &&
                                    formik.errors.dueDate}
                                </FormHelperText>
                              </FormControl>
                              <FormControl fullWidth>
                                <TextField
                                  name="invoiceAmount"
                                  label="Amount"
                                  variant="outlined"
                                  readOnly
                                  size="small"
                                  value={numberWithCommas(valueDebit)}
                                />
                                <FormHelperText
                                  error={
                                    formik.touched.invoiceAmount &&
                                    Boolean(formik.errors.invoiceAmount)
                                  }
                                >
                                  {formik.touched.invoiceAmount &&
                                    formik.errors.invoiceAmount}
                                </FormHelperText>
                              </FormControl>

                              <FormControl fullWidth>
                                <InputLabel
                                  id="currency-label"
                                  size="small"
                                  sx={{ color: " #0B626B" }}
                                >
                                  Currency
                                </InputLabel>
                                <Select
                                  labelId="currency-label"
                                  name="currency"
                                  size="small"
                                  label="Currency"
                                  disabled
                                  value={formik.values.currency}
                                >
                                  <MenuItem value={"MNT"} key={"MNT"}>
                                    MNT
                                  </MenuItem>
                                </Select>
                              </FormControl>
                            </Stack>
                            <FormControl>
                              <TextField
                                name="invoiceDesc"
                                label="Invoice description"
                                variant="outlined"
                                size="small"
                                value={formik.values.invoiceDesc}
                                onChange={formik.handleChange}
                              />
                            </FormControl>
                          </Stack>
                        </Box>
                      </Card>
                      <Card sx={{ mt: 1 }}>
                        <Box
                          sx={{
                            direction: "row",
                            justifyContent: "flex-start",
                            alignItems: "center",
                            m: 1,
                            display: "flex",
                          }}
                        >
                          <Typography variant="h5">Debits</Typography>
                          <Typography sx={{ ml: 15 }}>
                            <strong>Total Value:</strong>
                          </Typography>
                          <Typography color={"black"}>
                            <strong>
                              {valueDebit ? numberWithCommas(valueDebit) : 0}
                            </strong>
                          </Typography>
                        </Box>
                        <React.Fragment>
                          <DebitsDatagrid
                            params={params}
                            dataRealChange={dataRealChange}
                            rows={rowsDebitData}
                            setRows={setRowsDebitData}
                          />
                        </React.Fragment>
                      </Card>
                      <Card sx={{ mt: 1 }}>
                        <Box
                          sx={{
                            direction: "row",
                            justifyContent: "flex-start",
                            alignItems: "center",
                            m: 1,
                            display: "flex",
                          }}
                        >
                          <Typography variant="h5">Credits</Typography>
                          <Typography sx={{ ml: 15 }}>
                            <strong>Total Value:</strong>
                          </Typography>
                          <Typography color={"black"}>
                            <strong>
                              {valueCredit ? numberWithCommas(valueCredit) : 0}
                            </strong>
                          </Typography>
                        </Box>
                        <React.Fragment>
                          <CreditDatagrid
                            params={params}
                            counterParty={formik.values.counterParty}
                            dataRealChange={dataRealChangeCredit}
                            rows={rowsCreditData}
                            invoiceType={formik.values.invoiceType}
                            setRows={dataRealChangeCredit}
                            invoiceAmount={
                              infoInvoice?.result[0].invoice_amount
                            }
                          />
                        </React.Fragment>
                      </Card>
                    </CustomTabPanel>
                    <CustomTabPanel value={value} index={1}>
                      <FormControl fullWidth>
                        <TextField
                          name="comments"
                          multiline
                          rows={35}
                          placeholder="Please comment you..."
                          variant="outlined"
                          value={formik.values.comments}
                          onChange={formik.handleChange}
                        />
                      </FormControl>
                    </CustomTabPanel>
                    <CustomTabPanel value={value} index={2}>
                      <Card>
                        <ToastContainer />

                        <Box
                          display={"flex"}
                          justifyContent={"flex-end"}
                          sx={{ mr: 1, mt: 1 }}
                        >
                          <Button
                            startIcon={<AddSharpIcon />}
                            variant="text"
                            onClick={() => {
                              formikAttachment.setValues({
                                id: null,
                                attachmentname: "",
                                attachmenttype: "",
                                description: "",
                                attachmenturl: "",
                                filename: "",
                                method: "POST",
                              });
                              setMethodattachment("POST");

                              handleAttachmentOpen();
                            }}
                          >
                            Add
                          </Button>
                        </Box>
                        <DataGrid
                          getRowId={(row) => row.id}
                          rows={rowsA ? rowsA : []}
                          columns={colAttachment}
                          pageSizeOptions={[50]}
                          slots={{
                            noRowsOverlay: CustomNoRowsOverlay,
                          }}
                          sx={{
                            backgroundColor: "#FFFFFF",
                            "& .MuiDataGrid-cell:hover": {
                              color: "primary.main",
                            },
                            "& .MuiDataGrid-cell": {
                              borderTop: 1,
                              borderTopColor: "#E9ECF0",
                            },
                          }}
                        />
                      </Card>
                    </CustomTabPanel>
                  </Box>
                </Card>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
      <Modal open={openAttachment}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            border: "1px solid #000",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <form onSubmit={formikAttachment.handleSubmit}>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography variant="h5" gutterBottom>
                Attachment
              </Typography>
              <IconButton
                sx={{ display: "flex", mt: -1 }}
                aria-label="close"
                onClick={handleAttachmentClose}
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Divider sx={{ mb: 1 }} />
            <Stack direction="column" spacing={1}>
              <FormControl>
                <TextField
                  name="attachmentname"
                  fullWidth
                  variant="outlined"
                  size="small"
                  label="Name"
                  value={formikAttachment.values.attachmentname}
                  onChange={formikAttachment.handleChange}
                />
                <FormHelperText
                  error={
                    formikAttachment.touched.attachmentname &&
                    Boolean(formikAttachment.errors.attachmentname)
                  }
                >
                  {formikAttachment.touched.attachmentname &&
                    formikAttachment.errors.attachmentname}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="description"
                  label="Description"
                  multiline
                  rows={15}
                  variant="outlined"
                  value={formikAttachment.values.description}
                  onChange={formikAttachment.handleChange}
                />
                <FormHelperText
                  error={
                    formikAttachment.touched.description &&
                    Boolean(formikAttachment.errors.description)
                  }
                >
                  {formikAttachment.touched.description &&
                    formikAttachment.errors.description}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="filename"
                  label="Filename"
                  variant="outlined"
                  size="small"
                  disabled
                  value={formikAttachment.values.filename}
                  onChange={formikAttachment.handleChange}
                />
                <FormHelperText
                  error={
                    formikAttachment.touched.filename &&
                    Boolean(formikAttachment.errors.filename)
                  }
                >
                  {formikAttachment.touched.filename &&
                    formikAttachment.errors.filename}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Input type="file" name="file" onChange={uploadToClient} />

            <Divider sx={{ mt: 1 }} />
            <Box
              sx={{
                display: "flex",
                mb: 1,
                mt: 1,
                justifyContent: "space-between",
              }}
            >
              <Button variant="outlined" onClick={handleAttachmentClose}>
                Close
              </Button>

              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            </Box>
          </form>
        </Box>
      </Modal>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
      <Modal open={opens} onClose={handleCloses}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Delete invoice
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                {params.invoice_id} are you sure delete!
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                delInvoice();
                router.push(`/finance`);
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              Yes
            </Button>
            <Button onClick={handleCloses} variant="outlined">
              No
            </Button>
          </Box>
        </Box>
      </Modal>

      <React.Fragment>
        <PurchaseReport
          loading={drawerLoading}
          setDrawerLoading={setDrawerLoading}
          setToggleReport1={setToggleReport1}
          toggleReport1={toggleReport1}
          // invoiceType={invoiceType}
        />
        <InvoiceReport
          loading={drawerLoading}
          setDrawerLoading={setDrawerLoading}
          setToggleReport={setToggleReport}
          toggleReport={toggleReport}
          // invoiceType={invoiceType}
        />
      </React.Fragment>
    </>
  );
}
