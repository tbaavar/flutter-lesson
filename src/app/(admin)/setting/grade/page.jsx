"use client";
import * as React from "react";
import { useFormik } from "formik";
import {
  Stack,
  Box,
  Button,
  IconButton,
  Card,
  TextField,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  Tooltip,
  Grid,
  InputBase,
  Divider,
  FormHelperText,
} from "@mui/material";
import { DataGrid, GridActionsCellItem, GridToolbarContainer } from "@mui/x-data-grid";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import GradingIcon from "@mui/icons-material/Grading";
import CloseIcon from "@mui/icons-material/Close";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};
export default function GradePage() {
  const { data: session } = useSession();
  const [openGrade, setOpenGrade] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleCloseGrade = () => {
    setOpenGrade(!openGrade);
  };
  const [grade, setGrade] = React.useState({
    method: "",
    grade_id: "",
    sequence: "",
    product_name: "",
    grade_name: "",
    grade_description: "",
  });
  const handleChangeGrade = (event) => {
    setGrade((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };
  const handleClickOpenGrade = () => {
    setOpenGrade(true);
  };
  const [filterTextGrade, setFilterTextGrade] = React.useState("");
  const handleFilterChangeGrade = (e) => {
    setFilterTextGrade(e.target.value);
  };

  const { data: products } = useSWR(
    [`/api/lookup/product`, session?.user?.token],
    fetcher
  );
  const {
    data: grades,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/grade`, session?.user?.token], fetcher);
  const columsGrade = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product name"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "grade_name",
      renderHeader: () => <strong>{"Grade name"}</strong>,
      width: 200,
      editable: false,
      sortable: false,
    },
    {
      field: "sequence",
      renderHeader: () => <strong>{"Sequence"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "grade_description",
      renderHeader: () => <strong>{"Grade Description"}</strong>,
      width: 200,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;

          setGrade({
            grade_id: currentRow.grade_id,
            product_name: currentRow.product_id,
            sequence: currentRow.sequence,
            grade_name: currentRow.grade_name,
            grade_description: currentRow.grade_description,
            method: "PUT",
          });
          handleClickOpenGrade();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setGrade({
            grade_id: currentRow.grade_id,
            product_name: currentRow.product_id,
            sequence: currentRow.sequence,
            grade_name: currentRow.grade_name,
            grade_description: currentRow.grade_description,
            method: "DELETE",
          });
          handleClickOpenGrade();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const formikGrade = useFormik({
    initialValues: grade,
    validationSchema: yup.object({
      method: yup.string(),
      product_name: yup.string().required("Please insert product name!"),
      grade_name: yup.string().required("Please insert grade name!"),
      grade_description: yup
        .string()
        .required("Please insert grade description!"),
      sequence: yup.string().required("Please insert sequence!"),
    }),
    onSubmit: async () => {
      saveGrade();
   
    },
    enableReinitialize: true,
  });

  const saveGrade = async () => {
    const body = {
      grade_id: grade.method === "POST" ? undefined : grade.grade_id,
      product_id: grade.product_name,
      grade_name: grade.grade_name,
      grade_description: grade.grade_description,
      sequence: grade.sequence,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/grade`,
        grade.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    finally{
      setLoading(false);
      setOpen(false);
      handleCloseGrade();
    }
  };

  const rowsGrade = grades?.result?.map((row, i) => ({
    id: i + 1,
    grade_id: row.grade_id,
    product_id: row.product_id,
    product_name: row.products?.product_name,
    grade_name: row.grade_name,
    sequence: row.sequence,
    grade_description: row.grade_description,
  }));

  const filteredRowsGrade = rowsGrade?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextGrade.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue
          .toLowerCase()
          .includes(filterTextGrade.toLowerCase());
      }
      return false;
    })
  );
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <ToastContainer />
      <Card sx={{ p: 2,ml:-3,mt:-3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder=" Search Grade"
              inputProps={{ "aria-label": "Search grade" }}
              value={filterTextGrade}
              onChange={handleFilterChangeGrade}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Grade">
              <IconButton aria-label="add">
                <GradingIcon
                  onClick={() => {
                    setGrade({
                      method: "POST",
                      grade_id: "",
                      product_name: "",
                      product_name: "",
                      grade_name: "",
                      sequence: "",
                      grade_description: "",
                    });

                    handleClickOpenGrade();
                  }}
                />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>

        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsGrade}
          columns={columsGrade}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
            // 
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openGrade}
          onClose={handleCloseGrade}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {grade.method === "DELETE" || grade.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6" component="h3">
                    {grade.method === "DELETE" ? "Grade delete?" : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{grade.grade_name}</AlertTitle>
                      {grade.method === "DELETE"
                        ? "Are you sure delete?"
                        : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseGrade} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      grade.method === "DELETE" ? saveGrade() : resetPassword();
                      handleCloseGrade();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikGrade.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Grade
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseGrade}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    sx={{
                      width: 400,
                      maxWidth: "100%",
                    }}
                    autoComplete="off"
                  >
                    <FormControl size="small" fullWidth margin="dense">
                      <InputLabel
                        id="productname"
                        error={
                          formikGrade.touched.product_name &&
                          Boolean(formikGrade.errors.product_name)
                        }
                      >
                        Product name
                      </InputLabel>
                      <Select
                        defaultValue=""
                        labelId="product_name"
                        id="product_name"
                        name="product_name"
                        label="product_name"
                        onChange={handleChangeGrade}
                        value={formikGrade.values.product_name}
                        onBlur={formikGrade.handleBlur}
                        error={
                          formikGrade.touched.product_name &&
                          Boolean(formikGrade.errors.product_name)
                        }
                      >
                        {products?.result.length > 0 &&
                          products?.result.map((product) => (
                            <MenuItem
                              value={product.product_id}
                              key={product.product_id}
                            >
                              {product.product_name}
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText
                        error={
                          formikGrade.touched.product_name &&
                          Boolean(formikGrade.errors.product_name)
                        }
                      >
                        {formikGrade.touched.product_name &&
                          formikGrade.errors.product_name}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="grade_name"
                        name="grade_name"
                        margin="dense"
                        label="Grade name"
                        size="small"
                        variant="outlined"
                        value={formikGrade.values.grade_name}
                        onChange={handleChangeGrade}
                      />
                      <FormHelperText
                        error={
                          formikGrade.touched.grade_name &&
                          Boolean(formikGrade.errors.grade_name)
                        }
                      >
                        {formikGrade.touched.grade_name &&
                          formikGrade.errors.grade_name}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        margin="dense"
                        id="sequence"
                        size="small"
                        label="Sequence"
                        name="sequence"
                        variant="outlined"
                        onChange={handleChangeGrade}
                        value={formikGrade.values.sequence}
                      />
                      <FormHelperText
                        error={
                          formikGrade.touched.sequence &&
                          Boolean(formikGrade.errors.sequence)
                        }
                      >
                        {formikGrade.touched.sequence &&
                          formikGrade.errors.sequence}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="grade_description"
                        name="grade_description"
                        margin="dense"
                        label="Grade Description"
                        size="small"
                        variant="outlined"
                        value={formikGrade.values.grade_description}
                        onChange={handleChangeGrade}
                      />
                      <FormHelperText
                        error={
                          formikGrade.touched.grade_description &&
                          Boolean(formikGrade.errors.grade_description)
                        }
                      >
                        {formikGrade.touched.grade_description &&
                          formikGrade.errors.grade_description}
                      </FormHelperText>
                    </FormControl>
                  </Stack>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseGrade}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
