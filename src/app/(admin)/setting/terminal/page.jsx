"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { useFormik } from "formik";
import PropaneIcon from "@mui/icons-material/Propane";
import CloseIcon from "@mui/icons-material/Close";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Alert,
  AlertTitle,
  FormControlLabel,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
} from "@mui/material";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer,
  GridToolbarExport,
} from "@mui/x-data-grid";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs, { Dayjs } from "dayjs";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import Checkbox from "@mui/material/Checkbox";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import AddIcon from "@mui/icons-material/Add";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import { GridRowModes, GridRowEditStopReasons } from "@mui/x-data-grid";

const label = { inputProps: { "aria-label": "Checkbox demo" } };
const style1 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 1200,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};

export default function TerminalPage() {
  const { data: session } = useSession();
  const [openTerminal, setOpenTerminal] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [tank, setTank] = React.useState([]);
  const [delTerminal, setDelTerminal] = React.useState();
  const [deleteItem, setDeleteItem] = React.useState();
  const [deleteItem1, setDeleteItem1] = React.useState();
  const handleClose1 = () => {
    setOpen1(false);
  };
  const handleCloseTerminal = () => {
    setOpenTerminal(!openTerminal);
  };
  const [rows, setRows] = React.useState("");
  const [rowModesModel, setRowModesModel] = React.useState({});

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
  };
  const handleDeleteClick = (params) => () => {
    setOpen1(true);
    setDeleteItem1(params);
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const handleClickOpenTerminal = (currentRow) => {
    setDeleteItem(currentRow);
    setOpenTerminal(true);
  };

  const [filterTextTerminal, setFilterTextTerminal] = React.useState("");
  const handleFilterChangeTerminal = (e) => {
    setFilterTextTerminal(e.target.value);
  };

  const { data: tankstatus } = useSWR(
    [`/api/lookup/Tank_status`, session?.user?.token],
    fetcher
  );

  const { data: tanktype } = useSWR(
    [`/api/lookup/Tank_type`, session?.user?.token],
    fetcher
  );

  const { data: terminalstatus } = useSWR(
    [`/api/lookup/Terminal_status`, session?.user?.token],
    fetcher
  );
  const { data: company } = useSWR(
    [` /api/lookup/company`, session?.user?.token],
    fetcher
  );

  const { data: activeport } = useSWR(
    ["/api/lookup/port", session?.user?.token],
    fetcher
  );

  const {
    data: terminals,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/terminal`, session?.user?.token], fetcher);

  const columns = [
    {
      field: "tank_name",
      headerName: "Tank name",
      type: "text",
      width: 180,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "tank_short_name",
      headerName: "Tank short name",
      width: 200,
      editable: true,
    },
    {
      field: "capacity",
      headerName: "Capacity",
      type: "number",
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "sequence",
      headerName: "Sequence",
      type: "number",
      width: 150,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "tank_type",
      type: "singleSelect",
      headerName: "Tank type",
      valueOptions: tanktype?.result,
      getOptionLabel: (value) => value.name,
      getOptionValue: (value) => value.lookup_code,
      editable: true,
      width: 150,
    },
    {
      field: "tank_status",
      type: "singleSelect",
      headerName: "Tank status",
      valueOptions: tankstatus?.result,
      getOptionLabel: (value) => value.name,
      getOptionValue: (value) => value.lookup_code,
      editable: true,
      width: 150,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      width: 100,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModel[params.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(params.id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(params.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  const formikTerminal = useFormik({
    initialValues: {
      method: "",
      org_id: "",
      terminal_id: null,
      terminal_name: "",
      terminal_status: "",
      terminal_short_name: "",
      capacity: "",
      rent_flag: "N",
      rent_start_date: null,
      rent_end_date: null,
      address: "",
      port_flag: "N",
      sequence: "",
      port: null,
    },
    validationSchema: yup.object({
      method: yup.string(),
      terminal_short_name: yup
        .string()
        .required("Please insert Terminal short name!"),
      terminal_name: yup.string().required("Please insert Terminal name!"),
      terminal_status: yup.string().required("Please select Terminal status!"),
      capacity: yup.string().required("Please insert capacity!"),
      sequence: yup.string().required("Please insert sequence!"),
      address: yup.string().required("Please insert address!"),
    }),
    onSubmit: async (values) => {
      try {
        saveTerminal(values);
      } catch (error) {
        console.error("Error saving terminal:", error);
      }
    },
    enableReinitialize: true,
  });

  const columnsTerminal = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "org_name",
      renderHeader: () => <strong>{"Company"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "terminal_name",
      renderHeader: () => <strong>{"Terminal name"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "terminal_short_name",
      renderHeader: () => <strong>{"Terminal short name"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "address",
      renderHeader: () => <strong>{"Address"}</strong>,
      width: 250,
      editable: false,
      sortable: false,
    },

    {
      field: "capacity",
      renderHeader: () => <strong>{"Capacity"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    
    {
      field: "port_name",
      renderHeader: () => <strong>{"Port"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "terminal_status_name",
      renderHeader: () => <strong>{"Terminal status"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
 
          formikTerminal.setValues({
            org_id: currentRow.org_id,
            terminal_id: currentRow.terminal_id,
            terminal_name: currentRow.terminal_name,
            terminal_short_name: currentRow.terminal_short_name,
            terminal_status: currentRow.terminal_status,
            capacity: currentRow.capacity,
            rent_flag: currentRow.rent_flag,
            rent_start_date: currentRow.rent_start_date
              ? currentRow.rent_start_date
              : undefined,
            rent_end_date:
              currentRow.rent_flag == "N"
                ? undefined
                : currentRow.rent_end_date,
            address: currentRow.address,
            port_flag: currentRow.port_flag,
            port: currentRow.port_id,
            sequence: currentRow.sequence,
            method: "PUT",
          });
          fetch(`/api/lookup/terminal/tank/${currentRow.terminal_id}`, {
            headers: {
              Authorization: `Bearer ${session?.user?.token}`,
            },
          })
            .then((res) => res.json())
            .then((d) =>
              setRows(
                d?.result?.map((row, i) => ({
                  id: i + 1,
                  tank_id: row.tank_id,
                  tank_name: row.tank_name,
                  tank_short_name: row.tank_short_name,
                  capacity: row.capacity,
                  sequence: row.sequence,
                  tank_type: row.tank_type,
                  tank_status: row.tank_status,
                  type: "EDIT",
                }))
              )
            )
            .catch((err) => console.log(err));
          handleClickOpenTerminal();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          formikTerminal.setValues({
            org_id: currentRow.org_id,
            terminal_id: currentRow.terminal_id,
            terminal_name: currentRow.terminal_name,
            terminal_short_name: currentRow.terminal_short_name,
            terminal_status: currentRow.terminal_status,
            capacity: currentRow.capacity,
            rent_flag: currentRow.rent_flag,
            rent_start_date: currentRow.rent_start_date,
            rent_end_date: currentRow.rent_end_date,
            address: currentRow.address,
            port_flag: currentRow.port_flag,
            port: currentRow.port_id,
            sequence: currentRow.sequence,
            method: "DELETE",
          });
          setDelTerminal(currentRow.terminal_id);
          handleClickOpenTerminal(currentRow);
        };

        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const deleteData = async (id) => {
    const body = {
      username: session?.user?.username,
      tank_id: deleteItem1.row.tank_id,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/lookup/terminal/tank`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай устгалаа";
        setRows(rows.filter((row) => row.id !== deleteItem1.row.id));
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const deleteTerminal = async (params) => {
    const body = {
      terminal_id: delTerminal,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/lookup/terminal`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        mutate();
        let message = null;
        message = "Амжилттай устгалаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const saveTerminal = async (data) => {
   
    const body = {
      terminal_id: data.method === "POST" ? undefined : data.terminal_id,
      org_id: data.org_id,
      terminal_name: data.terminal_name,
      terminal_status: data.terminal_status,
      rent_flag: data.rent_flag,
      port_flag: data.port_flag,
      terminal_short_name: data.terminal_short_name,
      rent_start_date:
        data.rent_flag === "N" ? undefined : data.rent_start_date,
      rent_end_date: data.rent_flag === "N" ? undefined : data.rent_end_date,
      address: data.address,
      capacity: data.capacity,
      sequence: data.sequence,
      port_id: data.port_flag === "N" ? undefined : data.port,
      tanks: rows,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/terminal`,
        data.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = "Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        throw new Error("Failed to save terminal");
      }
    } catch (error) {
      toast.error(error.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleCloseTerminal();
    }
  };

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;
    const handleClick = () => {
      let id = null;
      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            type: "ADD",
            id: id,
            tank_name: "name",
            tank_short_name: "name",
            isNew: true,
            capacity: 0,
            sequence: 0,
            tank_type: "",
            tank_status: "",
          },
        ]);
      } else {
        id = rows?.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            type: "ADD",
            id: id,
            tank_name: "name",
            tank_short_name: "name",
            isNew: true,
            capacity: 0,
            sequence: 0,
            tank_type: "",
            tank_status: "",
          },
        ]);
      }
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    return (
      <GridToolbarContainer
        style={{ display: "flex", justifyContent: "flex-end" }}
      >
        <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
          Add
        </Button>
      </GridToolbarContainer>
    );
  }

  const rowsTerminal = terminals?.result?.map((row, i) => ({
    id: i + 1,
    org_id: row.org_id,
    org_name: row.org_name,
    terminal_id: row.terminal_id,
    port_name: row.port_name,
    terminal_status_name: row.terminal_status_name,
    terminal_name: row.terminal_name,
    terminal_short_name: row.terminal_short_name,
    terminal_status: row.terminal_status,
    capacity: row.capacity,
    rent_flag: row.rent_flag,
    rent_start_date: row.rent_start_date,
    rent_end_date: row.rent_end_date,
    address: row.address,
    port_flag: row.port_flag,
    port_id: row.port_id,
    sequence: row.sequence,
  }));

  const filteredRowsTerminal = rowsTerminal?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextTerminal.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue
          .toLowerCase()
          .includes(filterTextTerminal.toLowerCase());
      }
      return false;
    })
  );

  if (tank?.result?.length > 0) {
    const tankRows = tank?.result?.map((row, i) => ({
      id: i + 1,
      tank_id: row.tank_id,
      tank_name: row.tank_name,
      tank_short_name: row.tank_short_name,
      capacity: row.capacity,
      sequence: row.sequence,
      tank_type: row.tank_type,
      tank_status: row.tank_status,
      type: "EDIT",
    }));
    setRows(tankRows);
  }

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder=" Search Terminal"
              inputProps={{ "aria-label": "Search terminal" }}
              value={filterTextTerminal}
              onChange={handleFilterChangeTerminal}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Terminal">
              <IconButton
                aria-label="add"
                onClick={() => {
                  formikTerminal.setValues({
                    method: "POST",
                    org_id: "",
                    terminal_id: "",
                    terminal_name: "",
                    terminal_status: "",
                    terminal_short_name: "",
                    capacity: "",
                    rent_flag: "N",
                    rent_start_date: null,
                    rent_end_date: null,
                    address: "",
                    port_flag: "N",
                    port: null,
                    sequence: "",
                  });
                  setRows("");

                  handleClickOpenTerminal();
                }}
              >
                <PropaneIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>

        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsTerminal}
          columns={columnsTerminal}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openTerminal}
          onClose={handleCloseTerminal}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style1}>
            {formikTerminal.values.method === "DELETE" ||
            formikTerminal.values.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6">
                    {formikTerminal.values.method === "DELETE"
                      ? "Terminal delete?"
                      : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{formikTerminal.terminal_name}</AlertTitle>
                      {formikTerminal.values.method === "DELETE"
                        ? "Are you sure delete?"
                        : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseTerminal} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      deleteTerminal();
                      handleCloseTerminal();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <Box sx={style}>
                  <form onSubmit={formikTerminal.handleSubmit}>
                    <Box
                      sx={{ display: "flex", justifyContent: "space-between" }}
                    >
                      <Typography variant="h5" gutterBottom>
                        Terminal
                      </Typography>
                      <IconButton
                        sx={{ display: "flex", mt: -1 }}
                        onClick={handleCloseTerminal}
                        aria-label="close"
                      >
                        <CloseIcon />
                      </IconButton>
                    </Box>
                    <Divider sx={{ mb: 1 }} />
                    <Stack
                      spacing={1}
                      container
                      direction="row"
                      justifyContent="space-between"
                      alignItems="center"
                      sx={{ display: "flex" }}
                    >
                      <FormControl size="small" fullWidth margin="dense">
                        <InputLabel
                          id="org_id"
                          error={
                            formikTerminal.touched.org_id &&
                            Boolean(formikTerminal.errors.org_id)
                          }
                        >
                          Company
                        </InputLabel>
                        <Select
                          defaultValue=""
                          labelId="org_id"
                          id="org_id"
                          name="org_id"
                          label="Company"
                          onChange={formikTerminal.handleChange}
                          value={formikTerminal.values.org_id}
                          onBlur={formikTerminal.handleBlur}
                          error={
                            formikTerminal.touched.org_id &&
                            Boolean(formikTerminal.errors.org_id)
                          }
                          sx={{ mb: -0.5 }}
                        >
                          {company?.result.length > 0 &&
                            company?.result.map((company) => (
                              <MenuItem
                                value={company.org_id}
                                key={company.org_id}
                              >
                                {company.org_name}
                              </MenuItem>
                            ))}
                        </Select>
                        <FormHelperText
                          error={
                            formikTerminal.touched.org_id &&
                            Boolean(formikTerminal.errors.org_id)
                          }
                        >
                          {formikTerminal.touched.org_id &&
                            formikTerminal.errors.org_id}
                        </FormHelperText>
                      </FormControl>
                      <FormControl fullWidth>
                        <TextField
                          id="terminal_name"
                          name="terminal_name"
                          margin="dense"
                          label="Terminal name"
                          size="small"
                          variant="outlined"
                          value={formikTerminal.values.terminal_name}
                          onChange={formikTerminal.handleChange}
                        />
                        <FormHelperText
                          error={
                            formikTerminal.touched.terminal_name &&
                            Boolean(formikTerminal.errors.terminal_name)
                          }
                        >
                          {formikTerminal.touched.terminal_name &&
                            formikTerminal.errors.terminal_name}
                        </FormHelperText>
                      </FormControl>
                      <FormControl fullWidth>
                        <TextField
                          id="terminal_short_name"
                          name="terminal_short_name"
                          margin="dense"
                          label="Terminal short name"
                          size="small"
                          variant="outlined"
                          value={formikTerminal.values.terminal_short_name}
                          onChange={formikTerminal.handleChange}
                        />
                        <FormHelperText
                          error={
                            formikTerminal.touched.terminal_short_name &&
                            Boolean(formikTerminal.errors.terminal_short_name)
                          }
                        >
                          {formikTerminal.touched.terminal_short_name &&
                            formikTerminal.errors.terminal_short_name}
                        </FormHelperText>
                      </FormControl>
                      <FormControl fullWidth>
                        <TextField
                          id="capacity"
                          name="capacity"
                          margin="dense"
                          label="Capacity"
                          size="small"
                          variant="outlined"
                          value={formikTerminal.values.capacity}
                          onChange={formikTerminal.handleChange}
                        />
                        <FormHelperText
                          error={
                            formikTerminal.touched.capacity &&
                            Boolean(formikTerminal.errors.capacity)
                          }
                        >
                          {formikTerminal.touched.capacity &&
                            formikTerminal.errors.capacity}
                        </FormHelperText>
                      </FormControl>
                      <FormControl fullWidth>
                        <InputLabel
                          size="small"
                          id="Terminal"
                          error={
                            formikTerminal.touched.terminal_name &&
                            Boolean(formikTerminal.errors.terminal_name)
                          }
                          sx={{ color: " #0B626B" }}
                        >
                          Terminal status
                        </InputLabel>
                        <Select
                          size="small"
                          labelId="terminal_status"
                          id="terminal_status"
                          name="terminal_status"
                          label="terminal_status"
                          onChange={formikTerminal.handleChange}
                          value={formikTerminal.values.terminal_status}
                          onBlur={formikTerminal.handleBlur}
                          error={
                            formikTerminal.touched.terminal_status &&
                            Boolean(formikTerminal.errors.terminal_status)
                          }
                          sx={{ mb: -0.5 }}
                        >
                          {terminalstatus?.result.length > 0 &&
                            terminalstatus?.result.map((terminalstatus) => (
                              <MenuItem
                                value={terminalstatus.lookup_code}
                                key={terminalstatus.lookup_code}
                              >
                                {terminalstatus.name}
                              </MenuItem>
                            ))}
                        </Select>
                        <FormHelperText
                          error={
                            formikTerminal.touched.terminal_status &&
                            Boolean(formikTerminal.errors.terminal_status)
                          }
                        >
                          {formikTerminal.touched.terminal_status &&
                            formikTerminal.errors.terminal_status}
                        </FormHelperText>
                      </FormControl>
                    </Stack>
                    <Stack
                      spacing={1}
                      container
                      direction="row"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <FormControl fullWidth>
                        <TextField
                          id="address"
                          name="address"
                          margin="dense"
                          label="Address"
                          size="small"
                          variant="outlined"
                          value={formikTerminal.values.address}
                          onChange={formikTerminal.handleChange}
                        />
                        <FormHelperText
                          error={
                            formikTerminal.touched.address &&
                            Boolean(formikTerminal.errors.address)
                          }
                        >
                          {formikTerminal.touched.address &&
                            formikTerminal.errors.address}
                        </FormHelperText>
                      </FormControl>
                      <FormControl fullWidth></FormControl>
                    </Stack>
                    <Stack
                      spacing={1}
                      container
                      direction="row"
                      justifyContent="space-between"
                      alignItems="center"
                      sx={{ display: "flex" }}
                    >
                      <FormControlLabel
                        control={
                          <Checkbox
                            size="small"
                            name="rent_flag"
                            checked={formikTerminal.values.rent_flag === "Y"}
                            onChange={(e) => {
                              const isChecked = e.target.checked;
                              formikTerminal.setFieldValue(
                                "rent_flag",
                                isChecked ? "Y" : "N"
                              );

                              if (!isChecked) {
                                formikTerminal.setFieldValue(
                                  "rent_start_date",
                                  dayjs()
                                );
                                formikTerminal.setFieldValue(
                                  "rent_end_date",
                                  dayjs()
                                );
                              }
                            }}
                          />
                        }
                        labelPlacement="start"
                        label="Rent"
                      />
                      <FormControl fullWidth>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DatePicker
                            name="rent_start_date"
                            id="rent_start_date"
                            label="Rent start date"
                            InputLabelProps={{
                              style: {
                                color: "#0B626B",
                              },
                            }}
                            slotProps={{
                              textField: {
                                size: "small",
                              },
                            }}
                            format="YYYY-MM-DD"
                            disabled={
                              !(formikTerminal.values.rent_flag === "Y")
                            }
                            value={dayjs(formikTerminal.values.rent_start_date)}
                            onChange={(newValue) => {
                              formikTerminal.setFieldValue(
                                "rent_start_date",
                                dayjs(newValue).format("YYYY-MM-DD")
                              );
                            }}
                          />

                          <FormHelperText
                            error={
                              formikTerminal.touched.rent_start_date &&
                              Boolean(formikTerminal.errors.rent_start_date)
                            }
                          >
                            {formikTerminal.touched.rent_start_date &&
                              formikTerminal.errors.rent_start_date}
                          </FormHelperText>
                        </LocalizationProvider>
                      </FormControl>
                      <FormControl fullWidth>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DatePicker
                            name="rent_end_date"
                            id="rent_end_date"
                            label="Rent end date"
                            InputLabelProps={{
                              style: {
                                color: "#0B626B",
                              },
                            }}
                            slotProps={{
                              textField: {
                                size: "small",
                              },
                            }}
                            format="YYYY-MM-DD"
                            disabled={
                              !(formikTerminal.values.rent_flag === "Y")
                            }
                            value={dayjs(formikTerminal.values.rent_end_date)}
                            onChange={(newValue) => {
                              formikTerminal.setFieldValue(
                                "rent_end_date",
                                dayjs(newValue).format("YYYY-MM-DD")
                              );
                            }}
                          />

                          <FormHelperText
                            error={
                              formikTerminal.touched.rent_end_date &&
                              Boolean(formikTerminal.errors.rent_end_date)
                            }
                          >
                            {formikTerminal.touched.rent_end_date &&
                              formikTerminal.errors.rent_end_date}
                          </FormHelperText>
                        </LocalizationProvider>
                      </FormControl>
                      <FormControlLabel
                        control={
                          <Checkbox
                            size="small"
                            name="port_flag"
                            checked={formikTerminal.values.port_flag === "Y"}
                            onChange={(e) => {
                              const isChecked = e.target.checked;
                              formikTerminal.setFieldValue(
                                "port_flag",
                                isChecked ? "Y" : "N"
                              );

                              if (!isChecked) {
                                formikTerminal.setFieldValue("port", "");
                              }
                            }}
                          />
                        }
                        labelPlacement="start"
                        label="Port"
                      />
                      <FormControl fullWidth>
                        <InputLabel
                          size="small"
                          id="port"
                          error={
                            formikTerminal.touched.port &&
                            Boolean(formikTerminal.errors.port)
                          }
                          sx={{ color: " #0B626B" }}
                        >
                          Port
                        </InputLabel>
                        <Select
                          sx={{ mb: -0.5 }}
                          id="port"
                          name="port"
                          label="Port"
                          size="small"
                          value={formikTerminal.values.port}
                          onChange={formikTerminal.handleChange}
                          disabled={formikTerminal.values.port_flag !== "Y"}
                        >
                          {activeport?.result.length > 0 &&
                            activeport?.result.map((item) => (
                              <MenuItem value={item.port_id} key={item.port_id}>
                                {item.port_name}
                              </MenuItem>
                            ))}
                        </Select>
                        <FormHelperText
                          error={
                            formikTerminal.touched.port &&
                            Boolean(formikTerminal.errors.port)
                          }
                        >
                          {formikTerminal.touched.port &&
                            formikTerminal.errors.port}
                        </FormHelperText>
                      </FormControl>
                      <FormControl fullWidth>
                        <TextField
                          id="sequence"
                          name="sequence"
                          margin="dense"
                          label="Sequence"
                          size="small"
                          variant="outlined"
                          value={formikTerminal.values.sequence}
                          onChange={formikTerminal.handleChange}
                        />
                        <FormHelperText
                          error={
                            formikTerminal.touched.sequence &&
                            Boolean(formikTerminal.errors.sequence)
                          }
                        >
                          {formikTerminal.touched.sequence &&
                            formikTerminal.errors.sequence}
                        </FormHelperText>
                      </FormControl>
                    </Stack>
                    <Box
                      sx={{
                        height: "30vh",
                        width: "60vw",
                        "& .actions": {
                          color: "text.secondary",
                        },
                        "& .textPrimary": {
                          color: "text.primary",
                        },
                      }}
                    >
                      <DataGrid
                        rows={rows ? rows : []}
                        columns={columns}
                        editMode="row"
                        rowModesModel={rowModesModel}
                        onRowModesModelChange={handleRowModesModelChange}
                        onRowEditStop={handleRowEditStop}
                        processRowUpdate={processRowUpdate}
                        slots={{
                          toolbar: EditToolbar,
                        }}
                        slotProps={{
                          toolbar: { setRows, setRowModesModel },
                        }}
                        sx={{
                          backgroundColor: "#FFFFFF",
                          "& .MuiDataGrid-cell:hover": {
                            color: "primary.main",
                          },
                          "& .MuiDataGrid-cell": {
                            borderTop: 1,
                            borderTopColor: "#E9ECF0",
                          },
                          maxWidth: 1170,
                          minWidth: 1100,
                        }}
                      />
                    </Box>
                    <Divider sx={{ mt: 1, mb: 1 }} />
                    <Box
                      sx={{ display: "flex", justifyContent: "space-between" }}
                    >
                      <Button variant="outlined" onClick={handleCloseTerminal}>
                        Close
                      </Button>
                      <Button variant="contained" type="submit">
                        Save
                      </Button>
                    </Box>
                    <Modal open={open1} onClose={handleClose1}>
                      <Box
                        sx={{
                          position: "absolute",
                          top: "50%",
                          left: "50%",
                          transform: "translate(-50%, -50%)",
                          bgcolor: "background.paper",
                          boxShadow: 24,
                          p: 4,
                        }}
                      >
                        <Box>
                          <Typography
                            id="modal-modal-title"
                            variant="h6"
                            component="h3"
                          >
                            Confirm
                          </Typography>
                        </Box>
                        <Stack
                          component="div"
                          sx={{
                            width: 400,
                            maxWidth: "100%",
                          }}
                          noValidate
                          autoComplete="off"
                        >
                          <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                            <Alert severity="error">
                              <AlertTitle>{""}</AlertTitle>
                              Are you sure you want to delete?
                            </Alert>
                          </Box>
                        </Stack>
                        <Box
                          sx={{
                            display: "flex",
                            marginBottom: 1,
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button
                            onClick={async () => {
                              handleClose1();
                              deleteData();
                            }}
                            variant="contained"
                            sx={{
                              marginLeft: "10px",
                              marginRight: "10px",
                            }}
                          >
                            YES
                          </Button>
                          <Button onClick={handleClose1} variant="outlined">
                            NO
                          </Button>
                        </Box>
                      </Box>
                    </Modal>
                    <Modal open={open}>
                      <Box
                        sx={{
                          position: "absolute",
                          top: "50%",
                          left: "50%",
                          minWidth: 600,
                          transform: "translate(-50%, -50%)",
                          bgcolor: "transparent",
                          overflowY: "auto",
                          overflowX: "auto",
                          boxShadow: 0,
                          p: 1,
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            m: 3,
                          }}
                        >
                          <CircularProgress sx={{ color: "#ffffff" }} />
                        </Box>
                      </Box>
                    </Modal>
                  </form>
                </Box>
              </>
            )}
          </Box>
        </Modal>
      </Card>
    </>
  );
}
