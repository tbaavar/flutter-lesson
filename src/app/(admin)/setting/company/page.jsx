"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import GradingIcon from "@mui/icons-material/Grading";
import CloseIcon from "@mui/icons-material/Close";
import { useFormik } from "formik";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  FormControl,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
  Input,
  Avatar,
  Checkbox,
} from "@mui/material";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer,
} from "@mui/x-data-grid";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import {
  AntTabs,
  AntTab,
  TradeTabs,
  tradeProps,
} from "@/components/CustomTabControl";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import AddIcon from "@mui/icons-material/Add";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import { GridRowModes, GridRowEditStopReasons } from "@mui/x-data-grid";
import DoneIcon from "@mui/icons-material/Done";
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
export default function CompanyPage() {
  const { data: session } = useSession();
  const [openCounterparty, setOpenCounterparty] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [openBank, setOpenBank] = React.useState(false);
  const [openAddress, setOpenAddress] = React.useState(false);
  const [openContact, setOpenContact] = React.useState(false);
  const [image, setImage] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [rowModesModel, setRowModesModel] = React.useState({});
  const [rowModesModelBank, setRowModesModelBank] = React.useState({});
  const [rowModesModelContact, setRowModesModelContact] = React.useState({});
  const [rows, setRows] = React.useState(null);
  const [bank, setBank] = React.useState(null);
  const [contact, setContact] = React.useState(null);
  const [deleteItem1, setDeleteItem1] = React.useState();
  const [deleteItemBank, setDeleteItemBank] = React.useState();
  const [deleteItemContact, setDeleteItemContact] = React.useState();
  const [counterparty, setCounterparty] = React.useState({
    method: "",
    register_number: "",
    org_id: "",
    name: "",
    name_eng: "",
    short_name: "",
    sequence: "",
    short_name_eng: "",
    logo_url: "",
    web: "",
    mail: "",
    phone: "",
    tax_number: "",
  });
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeCounterparty = (event) => {
    setCounterparty((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };
  const handleClickOpenCounterparty = () => {
    setOpenCounterparty(true);
  };
  const handleCloseCounterparty = () => {
    setOpenCounterparty(!openCounterparty);
  };
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const handleCloseAddress = () => {
    setOpenAddress(false);
  };
  const deleteDataAddress = async (id) => {
    const body = {
      username: session?.user?.username,
      org_address_id: deleteItem1,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/company/address`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай устгалаа";
        setRows(rows.filter((row) => row.id !== deleteItem1));
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  function EditToolbarAddress(props) {
    const { setRows, setRowModesModel } = props;
    const handleClick = () => {
      let id = null;
      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            type: "ADD",
            id: id,
            address_type: "",
            address_id: "",
            origin: "",
            isNew: true,
            address: "",
            city: "",
          },
        ]);
      } else {
        id = rows?.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            type: "ADD",
            id: id,
            address_type: "",
            address_id: "",
            origin: "",
            isNew: true,
            address: "",
            city: "",
          },
        ]);
      }
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    return (
      <GridToolbarContainer
        style={{ display: "flex", justifyContent: "flex-start" }}
      >
        <Button
          color="primary"
          // disabled={formikCounterparty.values.legal_entity_id === "" ? true : false}
          startIcon={<AddIcon />}
          onClick={handleClick}
        >
          Add
        </Button>
      </GridToolbarContainer>
    );
  }
  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };
  const saveData = async (dd) => {
    return saveAddress(dd.row);
  };
  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
  };
  const handleDeleteClick = (id) => () => {
    setOpenAddress(true);
    setDeleteItem1(id);
  };
  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };
  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };
  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };
  const columnAddress = [
    {
      field: "address_type",
      headerName: "Type",
      width: 200,
      editable: true,
    },
    {
      field: "origin",
      headerName: "Origin",
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "city",
      headerName: "City",
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "address",
      headerName: "Adress",
      editable: true,
      width: 200,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      width: 100,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModel[params.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<DoneIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(params.id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(params.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params.row.org_address_id)}
            color="inherit"
          />,
        ];
      },
    },
    {
      field: "save",
      headerName: "Save",
      editable: true,
      type: "text",
      align: "left",
      headerAlign: "left",
      width: 100,
      renderCell: (params) => (
        <GridActionsCellItem
          icon={<SaveIcon />}
          label="Save"
          sx={{
            color: "primary.main",
          }}
          onClick={() => saveData(params)}
        />
      ),
    },
  ];
  const { data: addresses } = useSWR(
    counterparty.org_id !== "new"
      ? [`/api/company/address/${counterparty.org_id}`, session?.user?.token]
      : null,
    fetcher
  );
  if (addresses?.result?.length > 0 && rows === null) {
    const rowsAdress = addresses?.result?.map((row, i) => ({
      id: i + 1,
      address_type: row.address_type,
      address_id: row.address_id,
      origin: row.origin,
      city: row.city,
      address: row.address,
      type: "EDIT",
    }));
    setRows(rowsAdress);
  }
  const saveAddress = async (row) => {
    const body = {
      org_id: counterparty.org_id,
      org_address_id: row.org_address_id ? row.org_address_id : null,
      address_type: row.address_type,
      origin: row.origin,
      city: row.city,
      address: row.address,
      username: session?.user?.username,
    };
    const method = row.org_address_id == null ? "POST" : "PUT";
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/company/address`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  const { data: banks } = useSWR(
    counterparty.org_id !== "new"
      ? [`/api/company/bank/${counterparty.org_id}`, session?.user?.token]
      : null,
    fetcher
  );
  if (banks?.result?.length > 0 && bank === null) {
    const rowsBank = banks?.result?.map((row, i) => ({
      id: i + 1,
      org_account_id: row.org_account_id,
      bank_id: row.bank_id,
      bank_address: row.bank_address,
      account_name: row.account_name,
      account_number: row.account_number,
      swift: row.swift,
      type: "EDIT",
    }));
    setBank(rowsBank);
  }

  const handleCloseBank = () => {
    setOpenBank(false);
  };
  const processRowUpdateBank = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setBank(bank.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };
  const handleRowEditStopBank = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const handleEditClickBank = (id) => () => {
    setRowModesModelBank({
      ...rowModesModelBank,
      [id]: { mode: GridRowModes.Edit },
    });
  };

  const saveBankData = async (dd) => {
    return saveBank(dd.row);
  };
  const handleSaveClickBank = (params) => () => {
    setRowModesModelBank({
      ...rowModesModelBank,
      [params.id]: { mode: GridRowModes.View },
    });
  };
  const handleDeleteClickBank = (id) => () => {
    setOpenBank(true);
    setDeleteItemBank(id);
  };
  const handleCancelClickBank = (id) => () => {
    setRowModesModelBank({
      ...rowModesModelBank,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
    const editedRow = bank.find((row) => row.id === id);
    if (editedRow.isNew) {
      setBank(bank.filter((row) => row.id !== id));
    }
  };
  function EditToolbarBank(props) {
    const { setBank, setRowModesModelBank } = props;
    const handleClick = () => {
      let id = null;
      if (bank === null) {
        id = 1;
        setBank((oldRows) => [
          {
            type: "ADD",
            id: id,
            bank_id: "nnn",
            bank_address: "nnn",
            isNew: true,
            account_name: "",
            account_number: "",
            swift: "",
          },
        ]);
      } else {
        id = bank?.length + 1;
        setBank((oldRows) => [
          ...oldRows,
          {
            type: "ADD",
            id: id,
            bank_id: "nnn",
            bank_address: "nnn",
            isNew: true,
            account_name: "",
            account_number: "",
            swift: "",
          },
        ]);
      }
      setRowModesModelBank((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    return (
      <GridToolbarContainer
        style={{ display: "flex", justifyContent: "flex-start" }}
      >
        <Button
          color="primary"
          // disabled={formikCounterparty.values.legal_entity_id === "" ? true : false}
          startIcon={<AddIcon />}
          onClick={handleClick}
        >
          Add
        </Button>
      </GridToolbarContainer>
    );
  }
  const { data: banktype } = useSWR(
    ["/api/lookup/bank", session?.user?.token],
    fetcher
  );
  const handleRowModesModelChangeBank = (newRowModesModelBank) => {
    setRowModesModelBank(newRowModesModelBank);
  };
  const columnBank = [
    {
      field: "bank_id",
      type: "singleSelect",
      headerName: "Bank",
      valueOptions: banktype?.result,
      getOptionLabel: (value) => value.bank_name,
      getOptionValue: (value) => value.bank_id,
      editable: true,
      width: 200,
    },
    {
      field: "bank_address",
      headerName: "Bank Address",
      type: "text",
      width: 150,
      editable: true,
    },
    {
      field: "account_name",
      headerName: "Account Name",
      width: 150,
      type: "text",
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "account_number",
      headerName: "Account №",
      width: 150,
      type: "number",
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "swift",
      headerName: "Swift",
      editable: true,
      type: "text",
      align: "left",
      headerAlign: "left",
      width: 150,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      align: "left",
      headerAlign: "left",
      width: 100,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModelBank[params.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClickBank(params)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClickBank(params.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClickBank(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClickBank(params.row.org_account_id)}
            color="inherit"
          />,
        ];
      },
    },
    {
      field: "save",
      headerName: "Save",
      editable: true,
      type: "text",
      align: "left",
      headerAlign: "left",
      width: 100,
      renderCell: (params) => (
        <GridActionsCellItem
          icon={<SaveIcon />}
          label="Save"
          sx={{
            color: "primary.main",
          }}
          onClick={() => saveBankData(params)}
        />
      ),
    },
  ];
  const deleteDataBank = async (id) => {
    const body = {
      username: session?.user?.username,
      org_account_id: deleteItemBank,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/company/bank`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай устгалаа";
        setBank(bank.filter((row) => row.id !== deleteItemBank));
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  const saveBank = async (row) => {
    const body = {
      org_id: counterparty.org_id,
      org_account_id: row.org_account_id ? row.org_account_id : null,
      bank_id: row.bank_id,
      bank_address: row.bank_address,
      account_name: row.account_name,
      account_number: `${row.account_number}`,
      swift: row.swift,
      username: session?.user?.username,
    };
    const method = row.org_account_id == null ? "POST" : "PUT";
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/company/bank`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const handleCloseContact = () => {
    setOpenContact(false);
  };
  const deleteDataContact = async (id) => {
    const body = {
      username: session?.user?.username,
      org_contact_id: deleteItemContact,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/company/contact`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай устгалаа";
        setContact(contact.filter((row) => row.id !== deleteItemContact));
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  const processRowUpdateContact = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setContact(contact.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };
  const handleRowEditStopContact = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const handleEditClickContact = (id) => () => {
    setRowModesModelContact({
      ...rowModesModelContact,
      [id]: { mode: GridRowModes.Edit },
    });
  };
  const saveContactData = async (dd) => {
    return saveContact(dd.row);
  };
  const handleSaveClickContact = (id) => () => {
    setRowModesModelContact({
      ...rowModesModelContact,
      [id]: { mode: GridRowModes.View },
    });
  };
  const handleDeleteClickContact = (id) => () => {
    setOpenContact(true);
    setDeleteItemContact(id);
  };
  const handleCancelClickContact = (id) => () => {
    setRowModesModelContact({
      ...rowModesModelContact,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
    const editedRow = contact.find((row) => row.id === id);
    if (editedRow.isNew) {
      setContact(contact.filter((row) => row.id !== id));
    }
  };
  function EditToolbarContact(props) {
    const { setContact, setRowModesModelContact } = props;
    const handleClick = () => {
      let id = null;
      if (contact === null) {
        id = 1;
        setContact((oldRows) => [
          {
            type: "ADD",
            id: id,
            lastname: "name",
            firstname: "name",
            isNew: true,
            position: "",
            phone: "",
            mail: "",
            web: "",
            contact_id: "",
          },
        ]);
      } else {
        id = contact?.length + 1;
        setContact((oldRows) => [
          ...oldRows,
          {
            type: "ADD",
            id: id,
            lastname: "name",
            firstname: "name",
            isNew: true,
            position: "",
            phone: "",
            mail: "",
            web: "",
            contact_id: "",
          },
        ]);
      }
      setRowModesModelContact((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    return (
      <GridToolbarContainer
        style={{ display: "flex", justifyContent: "flex-start" }}
      >
        <Button
          color="primary"
          // disabled={formikCounterparty.values.legal_entity_id === "" ? true : false}
          startIcon={<AddIcon />}
          onClick={handleClick}
        >
          Add
        </Button>
      </GridToolbarContainer>
    );
  }
  const handleRowModesModelChangeContact = (newRowModesModelContact) => {
    setRowModesModelContact(newRowModesModelContact);
  };
  const columnContact = [
    {
      field: "lastname",
      headerName: "Lastname",
      width: 125,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "firstname",
      headerName: "Firstname",
      width: 125,
      editable: true,
    },
    {
      field: "position",
      headerName: "Position",
      width: 150,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "phone",
      headerName: "Phone №",
      width: 150,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "mail",
      headerName: "E-Mail",
      editable: true,
      width: 150,
    },
    {
      field: "web",
      headerName: "Website",
      editable: true,
      width: 150,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      width: 75,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModelContact[params.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<DoneIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClickContact(params.id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClickContact(params.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClickContact(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClickContact(params.row.org_contact_id)}
            color="inherit"
          />,
        ];
      },
    },
    {
      field: "save",
      headerName: "Save",
      editable: true,
      type: "text",
      align: "left",
      headerAlign: "left",
      width: 75,
      renderCell: (params) => (
        <GridActionsCellItem
          icon={<SaveIcon />}
          label="Save"
          sx={{
            color: "primary.main",
          }}
          onClick={() => saveContactData(params)}
        />
      ),
    },
  ];
  const { data: contacts } = useSWR(
    counterparty.org_id !== "new"
      ? [`/api/company/contact/${counterparty.org_id}`, session?.user?.token]
      : null,
    fetcher,
    { refreshInterval: 1000 }
  );
  if (contacts?.result?.length > 0 && contact === null) {
    const rowsContact = contacts?.result?.map((row, i) => ({
      id: i + 1,
      contact_id: row.contact_id,
      lastname: row.lastname,
      firstname: row.firstname,
      position: row.position,
      phone: row.phone,
      mail: row.mail,
      web: row.web,
      type: "EDIT",
    }));
    setContact(rowsContact);
  }
  const saveContact = async (row) => {
    const body = {
      org_id: counterparty.org_id,
      org_contact_id: row.org_contact_id ? row.org_contact_id : null,
      lastname: row.lastname,
      firstname: row.firstname,
      position: row.position,
      phone: row.phone,
      mail: row.mail,
      web: row.web,
      username: session?.user?.username,
    };
    const method = row.org_contact_id == null ? "POST" : "PUT";
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/company/contact`,
        method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  const handleAvatarChange = async (event) => {
    event.preventDefault();
    if (!counterparty.org_id) {
      toast.error("Хадгалаад дараа нь зураг оруулна уу!", {
        position: toast.POSITION.TOP_RIGHT,
      });
      setOpenCounterparty(false);
    } else {
      const formData = new FormData();
      formData.append("file", event.target.files[0]);
      formData.append("bucket", "profile");
      formData.append("id", counterparty.org_id);

      try {
        setLoading(true);
        setOpen(true);
        const response = await fetch("/api/file/upload", {
          method: "POST",
          body: formData,
          headers: {
            Authorization: `Bearer ${session?.user?.token}`,
          },
        });
        const json = await response.json();
        const proURL = json.result.url;
        if (response.status === 200) {
          saveCounterparty(proURL);
          setCounterparty({ logo_url: proURL });

          let message = null;
          message = "Амжилттай хадгаллаа";
          toast.success(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error(response.result, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
        console.log(error);
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } finally {
        setLoading(false);
        setOpen(false);
      }
    }
  };

  const [filterTextCounterparty, setFilterTextCounterparty] =
    React.useState("");
  const handleFilterChangeCounterparty = (e) => {
    setFilterTextCounterparty(e.target.value);
  };
  const {
    data: counterpartys,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/company`, session?.user?.token], fetcher);

  const columsCounterparty = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "register_number",
      renderHeader: () => <strong>{"register number"}</strong>,
      width: 120,
      editable: false,
      sortable: false,
    },
    {
      field: "name",
      renderHeader: () => <strong>{"Name(Cyrillic)"}</strong>,
      width: 250,
      editable: false,
      sortable: false,
    },
    {
      field: "name_eng",
      renderHeader: () => <strong>{"Name(Latin)"}</strong>,
      width: 250,
      editable: false,
      sortable: false,
    },
    {
      field: "short_name",
      renderHeader: () => <strong>{"Short name(Cyrillic)"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "short_name_eng",
      renderHeader: () => <strong>{"Short name(Latin)"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "mail",
      renderHeader: () => <strong>{"E-Mail"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "phone",
      renderHeader: () => <strong>{"Phone"}</strong>,
      width: 120,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleCounterpartyEdit = () => {
          const currentRow = params.row;
          setCounterparty({
            org_id: currentRow.org_id,
            register_number: currentRow.register_number,
            name: currentRow.name,
            name_eng: currentRow.name_eng,
            short_name_eng: currentRow.short_name_eng,
            short_name: currentRow.short_name,
            web: currentRow.web,
            mail: currentRow.mail,
            phone: currentRow.phone,
            sequence: currentRow.sequence,
            logo_url: currentRow.logo_url,
            tax_number: currentRow.tax_number,
            method: "PUT",
          });
          setContact(null);
          setRows(null);
          setBank(null);
          handleClickOpenCounterparty();
        };

        const handleCounterpartyDelete = () => {
          const currentRow = params.row;
          setCounterparty({
            org_id: currentRow.org_id,
            register_number: currentRow.register_number,
            name: currentRow.name,
            sequence: currentRow.sequence,
            short_name: currentRow.short_name,
            address: currentRow.address,
            method: "DELETE",
          });
          handleClickOpenCounterparty();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleCounterpartyEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleCounterpartyDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const formikCounterparty = useFormik({
    initialValues: counterparty,
    validationSchema: yup.object({
      register_number: yup
        .number()
        .positive("Can't be negative number!")
        .required("Please insert Register!")
        .typeError("Insert only number(s)!"),
      tax_number: yup
        .number()
        .positive("Can't be negative number!")
        .required("Please insert VAT!")
        .typeError("Insert only number(s)!"),
      name: yup
        .string()
        .required("Please insert Name!")
        .matches(
          /^([А-Яа-яЭӨҮэөү0-9,.-\s]*)$/g,
          "Name can only contain Cyrillic letters."
        ),
      name_eng: yup
        .string()
        .required("Please insert Name!")
        .matches(
          /^([A-Za-z0-9.,-\s]*)$/gi,
          "Name can only contain Latin letters and Numbers."
        ),
      short_name: yup
        .string()
        .required("Please insert Short Name!")
        .matches(
          /^([А-Яа-яЭӨҮэөү0-9,.-\s]*)$/g,
          "Name can only contain Cyrillic letters and Numbers."
        ),
      short_name_eng: yup
        .string()
        .required("Please insert Name!")
        .matches(
          /^([A-Za-z0-9.,-\s]*)$/gi,
          "Name can only contain Latin letters and Numbers."
        ),
      phone: yup
        .number()
        .positive("A phone number can't start with a minus")
        .integer("A phone number can't include a decimal point")
        .min(8)
        .required("A phone number is required"),
      mail: yup
        .string()
        .email("Please insert email!")
        .required("Please insert mail!"),
      web: yup
        .string()
        .required("Please enter website")
        .url("Please insert web url"),
    }),
    onSubmit: async () => {
      saveCounterparty();
    },
    enableReinitialize: true,
  });
  const saveCounterparty = async (params) => {
    const body = {
      org_id: counterparty.method === "POST" ? undefined : counterparty.org_id,
      register_number: counterparty.register_number,
      tax_number: counterparty.tax_number,
      name: counterparty.name,
      name_eng: counterparty.name_eng,
      short_name: counterparty.short_name,
      short_name_eng: counterparty.short_name_eng,
      phone: counterparty.phone,
      web: counterparty.web,
      mail: counterparty.mail,
      sequence: counterparty.sequence,
      logo_url: params,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/company`,
        counterparty.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };
  const rowsCounterparty = counterpartys?.result?.map((row, i) => ({
    id: i + 1,
    org_id: row.org_id,
    register_number: row.register_number,
    name_eng: row.name_eng,
    short_name_eng: row.short_name_eng,
    mail: row.mail,
    sequence: row.sequence,
    phone: row.phone,
    tax_number: row.tax_number,
    name: row.name,
    short_name: row.short_name,
    web: row.web,
    logo_url: row.logo_url,
  }));
  const filteredRowsCounterparty = rowsCounterparty?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value
          .toLowerCase()
          .includes(filterTextCounterparty.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue
          .toLowerCase()
          .includes(filterTextCounterparty.toLowerCase());
      }
      return false;
    })
  );
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder=" Search Counterparty"
              inputProps={{ "aria-label": "Search Counterparty" }}
              value={filterTextCounterparty}
              onChange={handleFilterChangeCounterparty}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Company">
              <IconButton
                aria-label="add"
                onClick={() => {
                  setCounterparty({
                    method: "POST",
                    org_id: "",
                    register_number: "",
                    sequence: "",
                    name: "",
                    short_name: "",
                    address: "",
                  });
                  setContact(null);
                  setRows(null);
                  setBank(null);
                  handleClickOpenCounterparty();
                }}
              >
                <GradingIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>

        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsCounterparty}
          columns={columsCounterparty}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openCounterparty}
          onClose={handleCloseCounterparty}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",

              maxHeight: "100%",
              overflow: "auto",
              bgcolor: "background.paper",
              border: "1px solid #000",
              boxShadow: 0,
              p: 1,
            }}
          >
            {counterparty.method === "DELETE" ||
            counterparty.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6" component="h3">
                    {counterparty.method === "DELETE"
                      ? "Counterparty delete?"
                      : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{counterparty.name}</AlertTitle>
                      {counterparty.method === "DELETE"
                        ? "Are you sure delete?"
                        : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseCounterparty} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      counterparty.method === "DELETE"
                        ? saveCounterparty()
                        : resetPassword();
                      handleCloseCounterparty();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikCounterparty.handleSubmit}>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      overflow: "auto",
                    }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Company General Information
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseCounterparty}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    sx={{
                      maxWidth: "100%",
                    }}
                    direction={"row"}
                    spacing={1}
                    autoComplete="off"
                  >
                    <Stack direction={"row"} fullWidth>
                      <Avatar
                        variant="square"
                        label="220x220 hemjeetei zurag"
                        src={counterparty.logo_url}
                        sx={{ width: 220, height: 220 }}
                      />
                    </Stack>
                    <Stack direction={"column"} fullWidth>
                      <Stack
                        direction={"row"}
                        spacing={1}
                        sx={{ mt: 1 }}
                        fullWidth
                      >
                        <FormControl fullWidth>
                          <TextField
                            id="register_number"
                            name="register_number"
                            label="Register №"
                            size="small"
                            variant="outlined"
                            onBlur={formikCounterparty.handleBlur}
                            value={formikCounterparty.values.register_number}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.register_number &&
                              Boolean(formikCounterparty.errors.register_number)
                            }
                          >
                            {formikCounterparty.touched.register_number &&
                              formikCounterparty.errors.register_number}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="tax_number"
                            name="tax_number"
                            label="VAT №"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.tax_number}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.tax_number &&
                              Boolean(formikCounterparty.errors.tax_number)
                            }
                          >
                            {formikCounterparty.touched.tax_number &&
                              formikCounterparty.errors.tax_number}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="sequence"
                            name="sequence"
                            label="Sequence"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.sequence}
                            onChange={handleChangeCounterparty}
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.sequence &&
                              Boolean(formikCounterparty.errors.sequence)
                            }
                          >
                            {formikCounterparty.touched.sequence &&
                              formikCounterparty.errors.sequence}
                          </FormHelperText>
                        </FormControl>
                      </Stack>
                      <Stack
                        direction={"row"}
                        spacing={1}
                        sx={{ mt: 1 }}
                        fullWidth
                      >
                        <FormControl fullWidth>
                          <TextField
                            id="phone"
                            name="phone"
                            label="Phone №"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.phone}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.phone &&
                              Boolean(formikCounterparty.errors.phone)
                            }
                          >
                            {formikCounterparty.touched.phone &&
                              formikCounterparty.errors.phone}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="name_eng"
                            name="name_eng"
                            label="Name (Latin)"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.name_eng}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.name_eng &&
                              Boolean(formikCounterparty.errors.name_eng)
                            }
                          >
                            {formikCounterparty.touched.name_eng &&
                              formikCounterparty.errors.name_eng}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="name"
                            name="name"
                            label="Name (Cyrillic)"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.name}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.name &&
                              Boolean(formikCounterparty.errors.name)
                            }
                          >
                            {formikCounterparty.touched.name &&
                              formikCounterparty.errors.name}
                          </FormHelperText>
                        </FormControl>
                      </Stack>
                      <Stack
                        direction={"row"}
                        spacing={1}
                        sx={{ mt: 1 }}
                        fullWidth
                      >
                        <FormControl fullWidth>
                          <TextField
                            id="mail"
                            name="mail"
                            label="E-Mail"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.mail}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.mail &&
                              Boolean(formikCounterparty.errors.mail)
                            }
                          >
                            {formikCounterparty.touched.mail &&
                              formikCounterparty.errors.mail}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="short_name_eng"
                            name="short_name_eng"
                            label="Short Name (Latin)"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.short_name_eng}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.short_name_eng &&
                              Boolean(formikCounterparty.errors.short_name_eng)
                            }
                          >
                            {formikCounterparty.touched.short_name_eng &&
                              formikCounterparty.errors.short_name_eng}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="short_name"
                            name="short_name"
                            label="Short Name (Cyrillic)"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.short_name}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.short_name &&
                              Boolean(formikCounterparty.errors.short_name)
                            }
                          >
                            {formikCounterparty.touched.short_name &&
                              formikCounterparty.errors.short_name}
                          </FormHelperText>
                        </FormControl>
                      </Stack>

                      <Stack
                        direction={"row"}
                        spacing={1}
                        sx={{ mt: 1 }}
                        fullWidth
                      >
                        <FormControl fullWidth>
                          <TextField
                            id="web"
                            name="web"
                            label="Web Address"
                            size="small"
                            variant="outlined"
                            value={formikCounterparty.values.web}
                            onBlur={formikCounterparty.handleBlur}
                            onChange={handleChangeCounterparty}
                            required
                          />
                          <FormHelperText
                            error={
                              formikCounterparty.touched.web &&
                              Boolean(formikCounterparty.errors.web)
                            }
                          >
                            {formikCounterparty.touched.web &&
                              formikCounterparty.errors.web}
                          </FormHelperText>
                        </FormControl>
                      </Stack>
                    </Stack>
                  </Stack>
                  <FormControl fullWidth>
                    <Input
                      type="file"
                      name="file"
                      onChange={handleAvatarChange}
                    />
                  </FormControl>
                  <AntTabs
                    value={value}
                    onChange={handleChange}
                    aria-label="basic tabs example"
                  >
                    <AntTab label="Bank Accounts" {...a11yProps(0)} />
                    <AntTab label="Address " {...a11yProps(1)} />
                    <AntTab label="Contact Person" {...a11yProps(2)} />
                  </AntTabs>
                  <TradeTabs value={value} index={0}>
                    {" "}
                    <Box>
                      <Typography variant="h7">
                        <strong>Bank/Accounts</strong>
                      </Typography>
                      <DataGrid
                        getRowId={(row) => row.id}
                        rows={bank ? bank : []}
                        columns={columnBank}
                        editMode="row"
                        rowModesModel={rowModesModelBank}
                        onRowModesModelChange={handleRowModesModelChangeBank}
                        onRowEditStop={handleRowEditStopBank}
                        processRowUpdate={processRowUpdateBank}
                        slots={{
                          toolbar: EditToolbarBank,
                          noRowsOverlay: CustomNoRowsOverlay,
                        }}
                        slotProps={{
                          toolbar: { setBank, setRowModesModelBank },
                        }}
                        hideFooter
                        autoHeight={true}
                        sx={{
                          backgroundColor: "#FFFFFF",
                          "& .MuiDataGrid-cell:hover": {
                            color: "primary.main",
                          },
                          "& .MuiDataGrid-cell": {
                            borderTop: 1,
                            borderTopColor: "#E9ECF0",
                          },
                        }}
                      />
                    </Box>
                  </TradeTabs>
                  <TradeTabs value={value} index={1}>
                    {" "}
                    <Box>
                      <Typography variant="h7">
                        <strong> Address</strong>
                      </Typography>
                      <DataGrid
                        // getRowId={(row) => row.id}
                        rows={rows ? rows : []}
                        columns={columnAddress}
                        editMode="row"
                        rowModesModel={rowModesModel}
                        onRowModesModelChange={handleRowModesModelChange}
                        onRowEditStop={handleRowEditStop}
                        processRowUpdate={processRowUpdate}
                        slots={{
                          toolbar: EditToolbarAddress,
                          noRowsOverlay: CustomNoRowsOverlay,
                        }}
                        slotProps={{
                          toolbar: { setRows, setRowModesModel },
                        }}
                        hideFooter
                        autoHeight={true}
                        sx={{
                          backgroundColor: "#FFFFFF",
                          "& .MuiDataGrid-cell:hover": {
                            color: "primary.main",
                          },
                          "& .MuiDataGrid-cell": {
                            borderTop: 1,
                            borderTopColor: "#E9ECF0",
                          },
                        }}
                      />
                    </Box>
                  </TradeTabs>
                  <TradeTabs value={value} index={2}>
                    {" "}
                    <Box>
                      <Typography variant="h7">
                        <strong>Contact Person</strong>
                      </Typography>
                      <DataGrid
                        // getRowId={(row) => row.id}
                        rows={contact ? contact : []}
                        columns={columnContact}
                        editMode="row"
                        rowModesModel={rowModesModelContact}
                        onRowModesModelChange={handleRowModesModelChangeContact}
                        onRowEditStop={handleRowEditStopContact}
                        processRowUpdate={processRowUpdateContact}
                        slots={{
                          toolbar: EditToolbarContact,
                          noRowsOverlay: CustomNoRowsOverlay,
                        }}
                        slotProps={{
                          toolbar: { setContact, setRowModesModelContact },
                        }}
                        hideFooter
                        autoHeight={true}
                        sx={{
                          backgroundColor: "#FFFFFF",
                          "& .MuiDataGrid-cell:hover": {
                            color: "primary.main",
                          },
                          "& .MuiDataGrid-cell": {
                            borderTop: 1,
                            borderTopColor: "#E9ECF0",
                          },
                        }}
                      />
                    </Box>
                  </TradeTabs>

                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button
                      variant="outlined"
                      onClick={handleCloseCounterparty}
                    >
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
      <Modal open={openBank} onClose={handleCloseBank}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Confirm
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{""}</AlertTitle>
                Are you sure you want to delete?
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                handleCloseBank();
                deleteDataBank();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              YES
            </Button>
            <Button onClick={handleCloseBank} variant="outlined">
              NO
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal open={openContact} onClose={handleCloseContact}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Confirm
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{""}</AlertTitle>
                Are you sure you want to delete?
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                handleCloseContact();
                deleteDataContact();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              YES
            </Button>
            <Button onClick={handleCloseContact} variant="outlined">
              NO
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal open={openAddress} onClose={handleCloseAddress}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Confirm
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{""}</AlertTitle>
                Are you sure you want to delete?
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                handleCloseAddress();
                deleteDataAddress();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              YES
            </Button>
            <Button onClick={handleCloseAddress} variant="outlined">
              NO
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
}
