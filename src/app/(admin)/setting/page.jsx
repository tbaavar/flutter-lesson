"use client";
import * as React from "react";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box, Typography } from "@mui/material";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { styled } from "@mui/material/styles";
import { useSession } from "next-auth/react";
import UserPage from "./user/page";
import TerminalPage from "./terminal/page";
import ProductPage from "./product/page";
import LookupPage from "./lookup/page";
import CounterpartyPage from "./counterparty/page";
import BankPage from "./bank/page";
import PortPage from "./port/page";
import CostsPage from "./costs/page";
import CompanyPage from "./company/page"
import { AntTabs, AntTab, CustomTabPanel, a11yProps } from "@/components/CustomTabControl";

export default function SettingPage() {
  const { data: session } = useSession();

  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange1 = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <ToastContainer />
      <Box sx={{ width: "100%" }}>
        <AntTabs
          value={value}
          onChange={handleChange1}
          indicatorColor="secondary"
          textColor="inherit"
          aria-label="full width tabs example"
        >
          <AntTab label="User" {...a11yProps(0)} />
          <AntTab label="Lookup" {...a11yProps(1)} />
          <AntTab label="Port" {...a11yProps(2)} />
          <AntTab label="Terminal" {...a11yProps(3)} />
          <AntTab label="Product" {...a11yProps(4)} />
          <AntTab label="Bank" {...a11yProps(5)} />
          <AntTab label="Counterparty" {...a11yProps(6)} />
          <AntTab label="Cost" {...a11yProps(7)} />
          <AntTab label="Company" {...a11yProps(8)} />
        </AntTabs>
      </Box>
      <CustomTabPanel value={value} index={0} dir={theme.direction}>
        <UserPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={1} dir={theme.direction}>
        <LookupPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={2} dir={theme.direction}>
        <PortPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={3} dir={theme.direction}>
        <TerminalPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={4} dir={theme.direction}>
        <ProductPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={5} dir={theme.direction}>
        <BankPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={6} dir={theme.direction}>
        <CounterpartyPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={7} dir={theme.direction}>
        <CostsPage />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={8} dir={theme.direction}>
        <CompanyPage />
      </CustomTabPanel>
    </>
  );
}
