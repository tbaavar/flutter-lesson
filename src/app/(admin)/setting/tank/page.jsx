"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
} from "@mui/material";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer,
  GridToolbarExport,
} from "@mui/x-data-grid";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import OilBarrelOutlinedIcon from "@mui/icons-material/OilBarrelOutlined";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};
// function CustomToolbar() {
//   return (
//     <GridToolbarContainer>
//       <GridToolbarExport />
//     </GridToolbarContainer>
//   );
// }
export default function TankPage() {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [openTank, setOpenTank] = React.useState(false);
  const handleCloseTank = () => {
    setOpenTank(!openTank);
  };
  const [tank, setTank] = React.useState({
    method: "",
    tank_id: "",
    terminal: "",
    tank_name: "",
    tank_status: "",
    capacity_mt: "",
    capacity_cm: "",
    capacity_b: "",
  });
  const handleChangeTank = (event) => {
    setTank((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };
  const handleClickOpenTank = () => {
    setOpenTank(true);
  };
  const [filterTextTank, setFilterTextTank] = React.useState("");
  const handleFilterChangeTank = (e) => {
    setFilterTextTank(e.target.value);
  };
  const { data: terminals } = useSWR(
    [`/api/lookup/terminal`, session?.user?.token],
    fetcher
  );

  const { data: tankstatus } = useSWR(
    [`/api/lookup/Tank_status`, session?.user?.token],
    fetcher
  );

  const {
    data: tanks,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/tank/list`, session?.user?.token], fetcher);

  const columnsTank = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "terminal_name",
      renderHeader: () => <strong>{"Terminal"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "tank_name",
      renderHeader: () => <strong>{"Tank name"}</strong>,
      width: 110,
      editable: false,
      sortable: false,
    },
    {
      field: "tank_status",
      renderHeader: () => <strong>{"Tank status"}</strong>,
      width: 120,
      editable: false,
      sortable: false,
    },
    {
      field: "capacity_mt",
      renderHeader: () => <strong>{"MT"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "capacity_cm",
      renderHeader: () => <strong>{"CM"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "capacity_b",
      renderHeader: () => <strong>{"B"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },

    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          setTank({
            tank_id: currentRow.tank_id,
            terminal: currentRow.terminal_id,
            tank_name: currentRow.tank_name,
            tank_status: currentRow.tank_status,
            capacity_mt: currentRow.capacity_mt,
            capacity_cm: currentRow.capacity_cm,
            capacity_b: currentRow.capacity_b,
            method: "PUT",
          });
          handleClickOpenTank();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setTank({
            tank_id: currentRow.tank_id,
            terminal: currentRow.terminal_id,
            tank_name: currentRow.tank_name,
            tank_status: currentRow.tank_status,
            capacity_mt: currentRow.capacity_mt,
            capacity_cm: currentRow.capacity_cm,
            capacity_b: currentRow.capacity_b,
            method: "DELETE",
          });
          handleClickOpenTank();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];
  const formikTank = useFormik({
    initialValues: tank,
    validationSchema: yup.object({
      method: yup.string(),
      terminal: yup.string().required("Please select Terminal!"),
      tank_name: yup.string().required("Please insert tank name!"),
      tank_status: yup.string().required("Please select tank status!"),
      capacity_mt: yup.string().required("Please insert capacity mt"),
      capacity_cm: yup.string().required("Please insert capacity cm"),
      capacity_b: yup.string().required("Please insert capacity b"),
    }),
    onSubmit: async () => {
      saveTank();
    },
    enableReinitialize: true,
  });
  const saveTank = async () => {
    const body = {
      tank_id: tank.method === "POST" ? undefined : tank.tank_id,
      terminal_id: tank.terminal,
      tank_name: tank.tank_name,
      tank_status: tank.tank_status,
      capacity_mt: tank.capacity_mt,
      capacity_cm: tank.capacity_cm,
      capacity_b: tank.capacity_b,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/tank`,
        tank.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        mutate();
        message = "Амжилттай хадгаллаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleCloseTank();
    }
  };
  const rowsTank = tanks?.result?.map((row, i) => ({
    id: i + 1,
    tank_id: row.tank_id,
    terminal_id: row.terminal_id,
    terminal_name: row.storage_terminal.terminal_name,
    tank_name: row.tank_name,
    tank_status: row.tank_status,
    capacity_mt: row.capacity_mt,
    capacity_cm: row.capacity_cm,
    capacity_b: row.capacity_b,
  }));

  const filteredRowsTank = rowsTank?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextTank.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue.toLowerCase().includes(filterTextTank.toLowerCase());
      }
      return false;
    })
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder="Search Tank"
              inputProps={{ "aria-label": "Search tank" }}
              value={filterTextTank}
              onChange={handleFilterChangeTank}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Tank">
              <IconButton
                aria-label="add"
                onClick={() => {
                  setTank({
                    method: "POST",
                    tank_id: "",
                    terminal: "",
                    tank_name: "",
                    tank_status: "",
                    capacity_mt: "",
                    capacity_cm: "",
                    capacity_b: "",
                  });

                  handleClickOpenTank();
                }}
              >
                <OilBarrelOutlinedIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>

        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsTank}
          columns={columnsTank}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
            //
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openTank}
          onClose={handleCloseTank}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {tank.method === "DELETE" || tank.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6">
                    {tank.method === "DELETE" ? "Tank delete?" : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{tank.tank_name}</AlertTitle>
                      {tank.method === "DELETE" ? "Are you sure delete?" : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseTank} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      tank.method === "DELETE" ? saveTank() : resetPassword();
                      handleCloseTank();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikTank.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Tank
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseTank}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    sx={{
                      width: 400,
                      maxWidth: "100%",
                    }}
                    autoComplete="off"
                  >
                    <FormControl size="small" fullWidth margin="dense">
                      <InputLabel
                        id="Terminal"
                        error={
                          formikTank.touched.terminal &&
                          Boolean(formikTank.errors.terminal)
                        }
                      >
                        Terminal
                      </InputLabel>
                      <Select
                        defaultValue=""
                        labelId="terminal"
                        id="terminal"
                        name="terminal"
                        label="terminal"
                        onChange={handleChangeTank}
                        value={formikTank.values.terminal}
                        onBlur={formikTank.handleBlur}
                        error={
                          formikTank.touched.terminal &&
                          Boolean(formikTank.errors.terminal)
                        }
                      >
                        {terminals?.result.length > 0 &&
                          terminals?.result.map((terminal) => (
                            <MenuItem
                              value={terminal.terminal_id}
                              key={terminal.terminal_id}
                            >
                              {terminal.terminal_name}
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText
                        error={
                          formikTank.touched.terminal &&
                          Boolean(formikTank.errors.terminal)
                        }
                      >
                        {formikTank.touched.terminal &&
                          formikTank.errors.terminal}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="tank_name"
                        name="tank_name"
                        margin="dense"
                        label="Tank Name"
                        size="small"
                        variant="outlined"
                        value={formikTank.values.tank_name}
                        onChange={handleChangeTank}
                      />
                      <FormHelperText
                        error={
                          formikTank.touched.tank_name &&
                          Boolean(formikTank.errors.tank_name)
                        }
                      >
                        {formikTank.touched.tank_name &&
                          formikTank.errors.tank_name}
                      </FormHelperText>
                    </FormControl>
                    <FormControl size="small" fullWidth margin="dense">
                      <InputLabel
                        id="Terminal"
                        error={
                          formikTank.touched.tank_status &&
                          Boolean(formikTank.errors.tank_status)
                        }
                      >
                        Tank status
                      </InputLabel>
                      <Select
                        defaultValue=""
                        labelId="tank_status"
                        id="tank_status"
                        name="tank_status"
                        label="tank_status"
                        onChange={handleChangeTank}
                        value={formikTank.values.tank_status}
                        onBlur={formikTank.handleBlur}
                        error={
                          formikTank.touched.tank_status &&
                          Boolean(formikTank.errors.tank_status)
                        }
                      >
                        {tankstatus?.result.length > 0 &&
                          tankstatus?.result.map((tank_status) => (
                            <MenuItem
                              value={tank_status.lookup_code}
                              key={tank_status.lookup_code}
                            >
                              {tank_status.name}
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText
                        error={
                          formikTank.touched.tank_status &&
                          Boolean(formikTank.errors.tank_status)
                        }
                      >
                        {formikTank.touched.tank_status &&
                          formikTank.errors.tank_status}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        margin="dense"
                        id="capacity_mt"
                        size="small"
                        name="capacity_mt"
                        label="MT"
                        variant="outlined"
                        value={formikTank.values.capacity_mt}
                        onChange={handleChangeTank}
                      />
                      <FormHelperText
                        error={
                          formikTank.touched.capacity_mt &&
                          Boolean(formikTank.errors.capacity_mt)
                        }
                      >
                        {formikTank.touched.capacity_mt &&
                          formikTank.errors.capacity_mt}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        margin="dense"
                        id="capacity_cm"
                        size="small"
                        name="capacity_cm"
                        label="CM"
                        variant="outlined"
                        value={formikTank.values.capacity_cm}
                        onChange={handleChangeTank}
                      />
                      <FormHelperText
                        error={
                          formikTank.touched.capacity_cm &&
                          Boolean(formikTank.errors.capacity_cm)
                        }
                      >
                        {formikTank.touched.capacity_cm &&
                          formikTank.errors.capacity_cm}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        margin="dense"
                        id="capacity_b"
                        size="small"
                        name="capacity_b"
                        label="B"
                        variant="outlined"
                        value={formikTank.values.capacity_b}
                        onChange={handleChangeTank}
                      />
                      <FormHelperText
                        error={
                          formikTank.touched.capacity_b &&
                          Boolean(formikTank.errors.capacity_b)
                        }
                      >
                        {formikTank.touched.capacity_b &&
                          formikTank.errors.capacity_b}
                      </FormHelperText>
                    </FormControl>
                  </Stack>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseTank}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
