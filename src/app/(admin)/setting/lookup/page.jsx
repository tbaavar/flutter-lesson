"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  FormControl,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
} from "@mui/material";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";

import CreateNewFolderOutlinedIcon from "@mui/icons-material/CreateNewFolderOutlined";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};

export default function LookupPage() {
  const { data: session } = useSession();
  const [openlookup, setOpenlookup] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [selectedTradeId, setSelectedTradeId] = React.useState("");
  const [typename, setTypename] = React.useState("");
  const [isCellClicked, setIsCellClicked] = React.useState(false);
  const handleTradeIdCellClick = (params, event) => {
    const selectedTypename = params.row.lookup_type;
    setTypename(selectedTypename);
    setIsCellClicked(true);
  };
  const handleCloseLookup = () => {
    setOpenlookup(!openlookup);
  };

  const handleClickOpenLookup = () => {
    setOpenlookup(true);
  };

  const [filterTextLookup, setFilterTextLookup] = React.useState("");
  const handleFilterChangeLookup = (e) => {
    setFilterTextLookup(e.target.value);
  };

  const { data: type_list } = useSWR(
    [`/api/lookup/lookup_type`, session?.user?.token],
    fetcher
  );

  const {
    data: look,
    error,
    isLoading,
    mutate,
  } = useSWR(
    typename
      ? [`/api/lookup/${typename}`, session?.user?.token]
      : [`/api/lookup`, session?.user?.token],
    fetcher
  );
  const columnstype = [
    {
      field: "lookup_type",
      renderHeader: () => <strong>{"Type"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
  ];

  const columnsLookup = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },

    {
      field: "lookup_code",
      renderHeader: () => <strong>{"Code"}</strong>,
      width: 170,
      editable: false,
      sortable: false,
    },
    {
      field: "name",
      renderHeader: () => <strong>{"Name"}</strong>,
      width: 200,
      editable: false,
      sortable: false,
    },
    {
      field: "sequence",
      renderHeader: () => <strong>{"Sequence"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "Edit_Flag",
      renderHeader: () => <strong>{"Edit flag"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          formikLookup.setValues({
            lookup_code: currentRow.lookup_code,
            name: currentRow.name,
            lookup_type: currentRow.lookup_type,
            sequence: currentRow.sequence,
            description: currentRow.description,
            method: "PUT",
          });
          handleClickOpenLookup(typename);
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          formikLookup.setValues({
            lookup_code: currentRow.lookup_code,
            name: currentRow.name,
            lookup_type: typename,
            sequence: currentRow.sequence,
            description: currentRow.description,
            method: "DELETE",
          });
          handleClickOpenLookup(typename);
        };

        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              {params.row.Edit_Flag === "Y" ? (
                <GridActionsCellItem
                  icon={<EditIcon />}
                  label="Edit"
                  color="secondary"
                  onClick={handleUserEdit}
                />
              ) : null}
            </Tooltip>

            <Tooltip title="Delete">
              {params.row.Edit_Flag === "Y" ? (
                <GridActionsCellItem
                  icon={<DeleteIcon />}
                  label="Delete"
                  color="error"
                  onClick={handleUserDelete}
                />
              ) : null}
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const formikLookup = useFormik({
    initialValues: {
      lookup_code: "",
      name: "",
      lookup_type: "",
      sequence: "",
      description: "",
      Edit_Flag: "",
      username: "duulem@mail.com",
      method: null,
    },
    validationSchema: yup.object({
      method: yup.string(),
      lookup_code: yup.string().required("Please select lookup code!"),
      name: yup.string().required("Please insert name!"),
      description: yup.string().required("Please insert description!"),
      lookup_type: yup.string().required("Please insert lookup type!"),
      sequence: yup.string().required("Please insert sequence!"),
    }),
    onSubmit: async () => {
      saveLookup();
    },
    enableReinitialize: true,
  });
  const saveLookup = async () => {
    const body = {
      lookup_code: formikLookup.values.lookup_code,
      lookup_name: formikLookup.values.name,
      lookup_type: typename,
      description: formikLookup.values.description,
      sequence: formikLookup.values.sequence,
      username:
        formikLookup.values.method === "POST"
          ? undefined
          : formikLookup.values.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup`,
        formikLookup.values.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message ="Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleCloseLookup();
    }
  };

  const rowstype = type_list?.result?.map((row, i) => ({
    id: i + 1,
    lookup_type: row.lookup_type,
  }));

  const rowsLookup = look?.result?.map((row, i) => ({
    id: i + 1,
    lookup_type: row.lookup_type,
    lookup_code: row.lookup_code,
    name: row.name,
    sequence: row.sequence,
    description: row.description,
    Edit_Flag: row.Edit_Flag,
  }));
  const filteredRowsLookup = rowstype?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextLookup.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue
          .toLowerCase()
          .includes(filterTextLookup.toLowerCase());
      }
      return false;
    })
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <Card sx={{ p: 3, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder="Search Lookup"
              inputProps={{ "aria-label": "Search lookup" }}
              value={filterTextLookup}
              onChange={handleFilterChangeLookup}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Lookup">
              <IconButton
                aria-label="add"
                disabled={!isCellClicked}
                onClick={() => {
                  formikLookup.setValues({
                    lookup_code: "",
                    name: "",
                    lookup_type: typename,
                    sequence: "",
                    description: "",
                    method: "POST",
                  });
                  handleClickOpenLookup();
                }}
              >
                <CreateNewFolderOutlinedIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={1.5}>
            <DataGrid
              getRowId={(row) => row.id}
              rows={filteredRowsLookup ? filteredRowsLookup : []}
              columns={columnstype}
              initialState={{
                pagination: {
                  paginationModel: {
                    pageSize: 15,
                  },
                },
              }}
              autoHeight
              onRowClick={handleTradeIdCellClick}
              slots={{
                noRowsOverlay: CustomNoRowsOverlay,
              }}
              sx={{
                backgroundColor: "#FFFFFF",
                "& .MuiDataGrid-cell:hover": {
                  color: "primary.main",
                },
                "& .MuiDataGrid-cell": {
                  borderTop: 1,
                  borderTopColor: "#E9ECF0",
                },
              }}
              rowHeight={27}
              disableRowSelectionOnClick
            />
          </Grid>
          <Grid item xs={10.5}>
            <Typography sx={{ mb: 1.1 }}>
              Type :<strong>{typename ? typename : null}</strong>
            </Typography>
            <DataGrid
              getRowId={(row) => row.id}
              rows={rowsLookup ? rowsLookup : []}
              columns={columnsLookup}
              initialState={{
                pagination: {
                  paginationModel: {
                    pageSize: 15,
                  },
                },
              }}
              pageSizeOptions={[15, 30, 45]}
              autoHeight
              slots={{
                noRowsOverlay: CustomNoRowsOverlay,
              }}
              sx={{
                backgroundColor: "#FFFFFF",
                "& .MuiDataGrid-cell:hover": {
                  color: "primary.main",
                },
                "& .MuiDataGrid-cell": {
                  borderTop: 1,
                  borderTopColor: "#E9ECF0",
                },
              }}
              rowHeight={25}
              disableRowSelectionOnClick
            />
          </Grid>
        </Grid>

        <Modal
          open={openlookup}
          onClose={handleCloseLookup}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {formikLookup.values.method === "DELETE" ||
            formikLookup.values.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6">
                    {formikLookup.values.method === "DELETE"
                      ? "Lookup delete?"
                      : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{formikLookup.values.lookup_type}</AlertTitle>
                      {formikLookup.values.method === "DELETE"
                        ? "Are you sure delete?"
                        : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseLookup} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      formikLookup.values.method === "DELETE"
                        ? saveLookup()
                        : resetPassword();
                      handleCloseLookup();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikLookup.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Box sx={{ display: "flex" }}>
                      <Typography variant="h5" gutterBottom>
                        Lookup :
                      </Typography>
                      <Typography variant="h5" gutterBottom>
                        {" " + typename ? typename : null}
                      </Typography>
                    </Box>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseLookup}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />

                  <Stack
                    sx={{
                      width: 400,
                      maxWidth: "100%",
                    }}
                    autoComplete="off"
                  >
                    <FormControl>
                      <TextField
                        margin="dense"
                        id="lookup_code"
                        size="small"
                        name="lookup_code"
                        label="lookup code"
                        variant="outlined"
                        value={formikLookup.values.lookup_code}
                        disabled={
                          formikLookup.values.method === "PUT" ? true : false
                        }
                        onChange={formikLookup.handleChange}
                      />
                      <FormHelperText
                        error={
                          formikLookup.touched.lookup_code &&
                          Boolean(formikLookup.errors.lookup_code)
                        }
                      >
                        {formikLookup.touched.lookup_code &&
                          formikLookup.errors.lookup_code}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        size="small"
                        id="name"
                        label="Name"
                        name="name"
                        margin="dense"
                        variant="outlined"
                        value={formikLookup.values.name}
                        onChange={formikLookup.handleChange}
                      />
                      <FormHelperText
                        error={
                          formikLookup.touched.name &&
                          Boolean(formikLookup.errors.name)
                        }
                      >
                        {formikLookup.touched.name && formikLookup.errors.name}
                      </FormHelperText>
                    </FormControl>

                    <FormControl>
                      <TextField
                        margin="dense"
                        id="sequence"
                        size="small"
                        label="Sequence"
                        name="sequence"
                        variant="outlined"
                        onChange={formikLookup.handleChange}
                        value={formikLookup.values.sequence}
                      />
                      <FormHelperText
                        error={
                          formikLookup.touched.sequence &&
                          Boolean(formikLookup.errors.sequence)
                        }
                      >
                        {formikLookup.touched.sequence &&
                          formikLookup.errors.sequence}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="description"
                        name="description"
                        margin="dense"
                        label="Description"
                        size="small"
                        variant="outlined"
                        value={formikLookup.values.description}
                        onChange={formikLookup.handleChange}
                      />
                      <FormHelperText
                        error={
                          formikLookup.touched.description &&
                          Boolean(formikLookup.errors.description)
                        }
                      >
                        {formikLookup.touched.description &&
                          formikLookup.errors.description}
                      </FormHelperText>
                    </FormControl>
                  </Stack>

                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseLookup}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
