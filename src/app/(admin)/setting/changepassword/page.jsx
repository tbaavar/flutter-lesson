"use client";
import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { toast, ToastContainer } from "react-toastify";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { fetchData } from "@/lib/fetch";
import {
  IconButton,
  Box,
  Typography,
  OutlinedInput,
  FormControl,
  InputLabel,
  InputAdornment,
  Button,
  TextField,
  Divider,
  FormHelperText,
  Modal,
  CircularProgress,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useSession } from "next-auth/react";
import "react-toastify/dist/ReactToastify.css";

export default function ChangePasswordPage({
  modalClose,
  userMethod,
  user_id,
  username,
}) {
  const { data: session } = useSession();
  const [showNewPassword, setShowNewPassword] = React.useState(false);
  const [showRepeatPassword, setShowRepeatPassword] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const handleClickShowNewPassword = () => setShowNewPassword((show) => !show);
  const handleClickShowRepeatPassword = () =>
    setShowRepeatPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const formik = useFormik({
    initialValues: {
      currentpassword: "",
      newpassword: "",
      repeatpassword: "",
    },
    validationSchema: yup.object({
      newpassword: yup.string().required("Password is required"),
      repeatpassword: yup
        .string()
        .oneOf([yup.ref("newpassword"), null], "Passwords must match"),
    }),
    onSubmit: async (values) => {
      saveChangePassword(values);
    },
    enableReinitialize: true,
  });

  const saveChangePassword = async (data) => {
    const body = {
      user_id: userMethod === "user" ? user_id : session?.user?.user_id,
      newpassword: data.newpassword,
      username: userMethod === "user" ? username : session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const method = "PUT";
      const res = await fetchData(
        `/api/user/password/change`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Нууц үг амжилттай солигдлоо";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
      modalClose();
    }
  };
  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Typography variant="h5" gutterBottom>
            Change Password
          </Typography>
          <IconButton
            sx={{ display: "flex", mt: -1 }}
            aria-label="close"
            onClick={modalClose}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Divider sx={{ mb: 1 }} />
        <FormControl fullWidth>
         <Typography fontSize={"15px"}>Username: <strong>{userMethod === "user" ? username : session?.user?.username}</strong></Typography>
        </FormControl>
        <FormControl fullWidth sx={{ my: 1 }} variant="outlined">
          <InputLabel size="small">New Password</InputLabel>
          <OutlinedInput
            name="newpassword"
            size="small"
            value={formik.values.newpassword}
            type={showNewPassword ? "text" : "password"}
            onChange={formik.handleChange}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowNewPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {showNewPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="New Password"
          />
          <FormHelperText
            error={
              formik.touched.newpassword && Boolean(formik.errors.newpassword)
            }
          >
            {formik.touched.newpassword && formik.errors.newpassword}
          </FormHelperText>
        </FormControl>

        <FormControl fullWidth variant="outlined">
          <InputLabel size="small">Repeat Password</InputLabel>
          <OutlinedInput
            name="repeatpassword"
            size="small"
            value={formik.values.repeatpassword}
            type={showRepeatPassword ? "text" : "password"}
            onChange={formik.handleChange}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowRepeatPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {showRepeatPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Repeat Password"
          />
          <FormHelperText
            error={
              formik.touched.repeatpassword &&
              Boolean(formik.errors.repeatpassword)
            }
          >
            {formik.touched.repeatpassword && formik.errors.repeatpassword}
          </FormHelperText>
        </FormControl>

        <Divider sx={{ mt: 1 }} />
        <Box
          sx={{
            display: "flex",
            mb: 1,
            mt: 1,
            justifyContent: "space-between",
          }}
        >
          <Button variant="outlined" onClick={() => modalClose()}>
            Close
          </Button>
          <Button variant="contained" color="dark" type="submit">
            Save
          </Button>
        </Box>
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
