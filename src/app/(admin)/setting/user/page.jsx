"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import LockResetIcon from "@mui/icons-material/LockReset";
import SyncLockIcon from "@mui/icons-material/SyncLock";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
import PersonAddAlt1OutlinedIcon from "@mui/icons-material/PersonAddAlt1Outlined";
import {
  Stack,
  Box,
  Button,
  RadioGroup,
  Card,
  FormControlLabel,
  Radio,
  TextField,
  Checkbox,
  InputLabel,
  MenuItem,
  FormControl,
  ListItemText,
  Select,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  OutlinedInput,
  InputAdornment,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
  Autocomplete,
} from "@mui/material";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import ChangePasswordPage from "../changepassword/page";

function generatePassword(length) {
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()";
  let password = "";
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    password += characters[randomIndex];
  }
  return password;
}
const stylepass = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 20,
  p: 4,
};
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 840,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};
const style1 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};

// const ITEM_HEIGHT = 48;
// const ITEM_PADDING_TOP = 1;
// const MenuProps = {
//   PaperProps: {
//     style: {
//       maxHeight: ITEM_HEIGHT * 7 + ITEM_PADDING_TOP,
//       width: 250,
//     },
//   },
// };
export default function UserPage() {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [opens, setOpens] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const [showPasswordrep, setShowPasswordrep] = React.useState(false);
  const [permissions, setPermissions] = React.useState([]);
  const [userId, setUserid] = React.useState("");
  const [userIdname, setUseridname] = React.useState("");
  const [item, setItem] = React.useState(false);
  const [openpass, setOpenpass] = React.useState(false);
  const [newPass, setNewPass] = React.useState("");
  const [openNewPass, setOpenNewPass] = React.useState(false);
  const modalOpen = () => setOpenpass(true);
  const modalClose = () => setOpenpass(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handlePassClose = () => setOpenNewPass(false);

  const userMethod = "user";
  const formik = useFormik({
    initialValues: {
      user_id: null,
      lastname: "",
      firstname: "",
      phone: "",
      position: "",
      mail: "",
      password: "",
      role: "",
      username: "",
      method: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
      lastname: yup.string().required("Please insert Name!"),
      firstname: yup.string().required("Please insert firstname!"),
      // password: yup.string().when("method", {
      //   is: "POST",
      //   then: yup
      //     .string()
      //     .required("Password is required")
      //     .min(5, "Password must be at least 5 characters")
      //     .max(100, "Password must be at most 100 characters"),
      //   otherwise: null,
      // }),
      // repeatpassword: yup
      //   .string()
      //   .oneOf([yup.ref("password"), null], "Passwords must match"),
      // mail: yup.string().required("Please insert mail!"),
      role: yup.string().required("Please select role!"),
      phone: yup
        .number()
        .positive("A phone number can't start with a minus")
        .integer("A phone number can't include a decimal point")
        .min(8),
    }),
    onSubmit: async (values) => {
      saveUser(values);
    },

    enableReinitialize: true,
  });

  const handleOrgsChange = (event) => {
    orgaFormik.setFieldValue("organizations", event.target.value);
  };
  const orgaFormik = useFormik({
    initialValues: {
      organizations: [],
    },
    validationSchema: yup.object({}).required("Please choose org"),
    enableReinitialize: true,
  });

  const handlePermissionChange = (index, newPermission) => {
    setPermissions((prevPermissions) => {
      const updatedPermissions = [...prevPermissions];
      updatedPermissions[index] = {
        ...updatedPermissions[index],
        permission: newPermission,
      };
      return updatedPermissions;
    });
  };

  const [filterText, setFilterText] = React.useState("");

  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleClickShowPasswordrep = () => setShowPasswordrep((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const { data: roles } = useSWR(
    [`/api/lookup/Role`, session?.user?.token],
    fetcher
  );
  const { data: organization } = useSWR(
    [`/api/organization`, session?.user?.token],
    fetcher
  );

  const {
    data: users,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/user`, session?.user?.token], fetcher);

  const columns = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 40,
      editable: false,
      sortable: false,
    },
    {
      field: "username",
      renderHeader: () => <strong>{"Username"}</strong>,
      width: 200,
      editable: false,
      sortable: false,
    },
    {
      field: "lastname",
      renderHeader: () => <strong>{"LastName"}</strong>,
      width: 120,
      editable: false,
      sortable: false,
    },
    {
      field: "firstname",
      renderHeader: () => <strong>{"Firstname"}</strong>,
      width: 120,
      editable: false,
      sortable: false,
    },
    {
      field: "position",
      renderHeader: () => <strong>{"Position"}</strong>,
      width: 250,
      editable: false,
      sortable: false,
    },
    {
      field: "phone",
      renderHeader: () => <strong>{"Phone"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "mail",
      renderHeader: () => <strong>{"Mail"}</strong>,
      width: 200,
      editable: false,
      sortable: false,
    },
    {
      field: "role",
      renderHeader: () => <strong>{"Role"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 240,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          formik.setValues({
            user_id: currentRow.user_id,
            lastname: currentRow.lastname,
            firstname: currentRow.firstname,
            mail: currentRow.mail,
            role: currentRow.role,
            phone: currentRow.phone,
            position: currentRow.position,
            username: currentRow.username,
            method: "PUT",
          });

          fetch(`/api/lookup/company/${currentRow.user_id}`, {
            headers: {
              Authorization: `Bearer ${session?.user?.token}`,
            },
          })
            .then((res) => res.json())
            .then((d) => {
              const orgItems = d?.result.map((item) => item.org_id);
              orgaFormik.setFieldValue("organizations", orgItems);
            })
            .catch((err) => console.log(err));

          fetch(`/api/lookup/permission/${currentRow.user_id}`, {
            headers: {
              Authorization: `Bearer ${session?.user?.token}`,
            },
          })
            .then((res) => res.json())
            .then((d) => {
              setPermissions(d?.result);
            })
            .catch((err) => console.log(err));

          handleOpen();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setUserid(currentRow.user_id);
          formik.setValues({
            user_id: currentRow.user_id,
            lastname: currentRow.lastname,
            firstname: currentRow.firstname,
            middlename: currentRow.middlename,
            mail: currentRow.mail,
            role: currentRow.role,
            username: currentRow.username,
            method: "DELETE",
          });
          handleOpen();
        };

        const handleUserReset = () => {
          const currentRow = params.row;
          setUserid(currentRow.user_id);
          setUseridname(currentRow.username);
          formik.setValues({
            user_id: currentRow.user_id,
            lastname: currentRow.lastname,
            firstname: currentRow.firstname,
            middlename: currentRow.middlename,
            mail: currentRow.mail,
            role: currentRow.role,
            username: currentRow.username,
            method: "PATCH",
          });
          handleOpen();
        };
        const handleUserResetpass = () => {
          const currentRow = params.row;
          setUserid(currentRow.user_id);
          setUseridname(currentRow.username);
          formik.setValues({
            user_id: currentRow.user_id,
            lastname: currentRow.lastname,
            firstname: currentRow.firstname,
            middlename: currentRow.middlename,
            mail: currentRow.mail,
            role: currentRow.role,
            username: currentRow.username,
            method: "PATCH",
          });
          modalOpen();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>
            <Tooltip title="Reset">
              <GridActionsCellItem
                icon={<LockResetIcon />}
                label="Reset"
                color="warning"
                onClick={handleUserReset}
              />
            </Tooltip>
            <Tooltip title="Change password">
              <GridActionsCellItem
                icon={<SyncLockIcon />}
                label="Resetpass"
                color="green"
                onClick={handleUserResetpass}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];
  const saveUser = async (data) => {
    const arr = [];

    {
      orgaFormik.values.organizations.map((item1) => {
        arr.push({ org_id: item1 });
      });
    }

    const body = {
      user_id: data.method === "POST" ? undefined : data.user_id,
      lastname: data.lastname,
      firstname: data.firstname,
      mail: data.mail,
      password: data.password,
      role: data.role,
      phone: data.phone,
      position: data.position,
      username: data.username,
      permission: permissions,
      orgs: arr,
      creation_by: session?.user.username,
    };

    try {
      setLoading(true);
      setOpens(true);
      const res = await fetchData(
        `/api/user`,
        data.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        mutate;
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpens(false);
      handleClose();
    }
  };

  const delUser = async () => {
    const body = {
      user_id: userId,
      username: session?.user.username,
    };
    try {
      setLoading(true);
      setOpens(true);
      const res = await fetchData(
        `/api/user`,
        "DELETE",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        mutate;
        toast.success("Success", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpens(false);
      handleClose();
    }
  };

  const resetPassword = async () => {
    const newPassword = generatePassword(8);
    setNewPass(newPassword);
    const body = {
      user_id: userId,
      username: session?.user?.username,
      password: newPassword,
    };

    try {
      const res = await fetchData(
        `/api/user/password/reset`,
        "PUT",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        console.log(res);
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Reset үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setOpenNewPass(true);
    }
  };

  const rows = users?.result?.map((row, i) => ({
    id: i + 1,
    user_id: row.user_id,
    lastname: row.lastname,
    firstname: row.firstname,
    position: row.position,
    phone: row.phone,
    mail: row.mail,
    role: row.role,
    username: row.username,
  }));

  const filteredRows = rows?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterText.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue.toLowerCase().includes(filterText.toLowerCase());
      }
      return false;
    })
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  const options1 =
    roles?.result?.length > 0 && roles?.result?.map((role) => role.lookup_code);
  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder="Search User"
              inputProps={{ "aria-label": "Search person" }}
              value={filterText}
              onChange={handleFilterChange}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Person">
              <IconButton
                aria-label="add"
                onClick={() => {
                  formik.setValues({
                    ...formik.values,
                    method: "POST",
                  });

                  orgaFormik.setValues({
                    organizations: [],
                  });
                  fetch(`/api/lookup/permission/0`, {
                    headers: {
                      Authorization: `Bearer ${session?.user?.token}`,
                    },
                  })
                    .then((res) => res.json())
                    .then((d) => {
                      setPermissions(d?.result);
                    })
                    .catch((err) => console.log(err));

                  handleOpen();
                }}
              >
                <PersonAddAlt1OutlinedIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>
        <DataGrid
          getRowId={(row) => row.user_id}
          rows={filteredRows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 50,
              },
            },
          }}
          rowHeight={25}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          disableRowSelectionOnClick
        />
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style1}>
            {formik.values.method === "DELETE" ||
            formik.values.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6">
                    {formik.values.method === "DELETE"
                      ? "User delete?"
                      : "Password reset"}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{formik.values.username}</AlertTitle>
                      {formik.values.method === "DELETE"
                        ? "Are you sure delete?"
                        : "Are you sure password reset?"}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleClose} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      formik.values.method === "DELETE"
                        ? delUser()
                        : resetPassword();
                      handleClose();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <Box sx={style}>
                  <form onSubmit={formik.handleSubmit}>
                    <Box
                      sx={{ display: "flex", justifyContent: "space-between" }}
                    >
                      <Typography variant="h5" gutterBottom>
                        User
                      </Typography>
                      <IconButton
                        sx={{ display: "flex", mt: -1 }}
                        onClick={handleClose}
                        aria-label="close"
                      >
                        <CloseIcon />
                      </IconButton>
                    </Box>
                    <Divider sx={{ mb: 1.4 }} />
                    <Box display={"flex"}>
                      <Box
                        component="fieldset"
                        alignItems={"top"}
                        sx={{
                          borderRadius: 1,
                          borderColor: "#e6e6e6",
                          width: "50vw",
                        }}
                      >
                        <legend>Profile</legend>
                        <Stack direction={"column"} spacing={1}>
                          <FormControl sx={{ width: 378 }}>
                            <InputLabel
                              size="small"
                              id="orgs"
                              // error={
                              //   orgaFormik.values.organizations &&
                              //   Boolean(orgaFormik.values.organizations)
                              // }
                            >
                              Orgs
                            </InputLabel>
                            <Select
                              id="orgs"
                              name="orgs"
                              multiple
                              size="small"
                              sx={{ backgroundColor: "white" }}
                              value={orgaFormik.values.organizations}
                              onChange={handleOrgsChange}
                              input={<OutlinedInput label="orgs" />}
                              SelectDisplayProps={item.name}
                              renderValue={(selected) => {
                                return selected
                                  .map(
                                    (id) =>
                                      organization?.result.find(
                                        (item) => item.org_id === id
                                      )?.name
                                  )
                                  .join(", ");
                              }}
                            >
                              {organization?.result?.map((item) => (
                                <MenuItem key={item.org_id} value={item.org_id}>
                                  <Checkbox
                                    checked={
                                      orgaFormik.values.organizations.indexOf(
                                        item.org_id
                                      ) > -1
                                    }
                                  />
                                  <ListItemText
                                    primary={item.name}
                                    style={{ color: "black" }}
                                  />
                                </MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          <FormControl fullWidth>
                            <TextField
                              size="small"
                              name="lastname"
                              label="Lastname"
                              variant="outlined"
                              value={formik.values.lastname}
                              onChange={formik.handleChange}
                            />
                            <FormHelperText
                              error={
                                formik.touched.lastname &&
                                Boolean(formik.errors.lastname)
                              }
                            >
                              {formik.touched.lastname &&
                                formik.errors.lastname}
                            </FormHelperText>
                          </FormControl>
                          <FormControl fullWidth>
                            <TextField
                              size="small"
                              label="Firstname"
                              name="firstname"
                              variant="outlined"
                              value={formik.values.firstname}
                              onChange={formik.handleChange}
                            />
                            <FormHelperText
                              error={
                                formik.touched.firstname &&
                                Boolean(formik.errors.firstname)
                              }
                            >
                              {formik.touched.firstname &&
                                formik.errors.firstname}
                            </FormHelperText>
                          </FormControl>
                          <FormControl fullWidth>
                            <TextField
                              size="small"
                              label="Mail"
                              name="mail"
                              variant="outlined"
                              onChange={formik.handleChange}
                              value={formik.values.mail}
                            />
                            <FormHelperText
                              error={
                                formik.touched.mail &&
                                Boolean(formik.errors.mail)
                              }
                            >
                              {formik.touched.mail && formik.errors.mail}
                            </FormHelperText>
                          </FormControl>
                          <Autocomplete
                            options={options1}
                            size="small"
                            onChange={(event, value) => {
                              formik.setFieldValue("role", value); // Update the 'role' field in Formik
                            }}
                            value={formik.values.role}
                            renderInput={(params) => (
                              <TextField {...params} label="Role" />
                            )}
                          />
                          <FormControl fullWidth>
                            <TextField
                              id="position"
                              name="position"
                              label="Position"
                              size="small"
                              variant="outlined"
                              value={formik.values.position}
                              onChange={formik.handleChange}
                            />
                            <FormHelperText
                              error={
                                formik.touched.position &&
                                Boolean(formik.errors.position)
                              }
                            >
                              {formik.touched.position &&
                                formik.errors.position}
                            </FormHelperText>
                          </FormControl>
                          <FormControl fullWidth>
                            <TextField
                              name="phone"
                              label="Phone"
                              size="small"
                              variant="outlined"
                              value={formik.values.phone}
                              onBlur={formik.handleBlur}
                              onChange={formik.handleChange}
                            />
                            <FormHelperText
                              error={
                                formik.touched.phone &&
                                Boolean(formik.errors.phone)
                              }
                            >
                              {formik.touched.phone && formik.errors.phone}
                            </FormHelperText>
                          </FormControl>
                          <FormControl fullWidth>
                            <TextField
                              size="small"
                              name="username"
                              label="Username"
                              variant="outlined"
                              value={formik.values.username}
                              disabled={
                                formik.values.method === "PUT" ? true : false
                              }
                              onChange={formik.handleChange}
                            />
                            <FormHelperText
                              error={
                                formik.touched.username &&
                                Boolean(formik.errors.username)
                              }
                            >
                              {formik.touched.username &&
                                formik.errors.username}
                            </FormHelperText>
                          </FormControl>
                          {formik.values.method === "POST" ? (
                            <FormControl
                              fullWidth
                              size="small"
                              variant="outlined"
                            >
                              <InputLabel
                                error={
                                  formik.touched.password &&
                                  Boolean(formik.errors.password)
                                }
                              >
                                Password
                              </InputLabel>
                              <OutlinedInput
                                type={showPassword ? "text" : "password"}
                                name="password"
                                onChange={formik.handleChange}
                                endAdornment={
                                  <InputAdornment position="end">
                                    <IconButton
                                      aria-label="toggle password visibility"
                                      onClick={handleClickShowPassword}
                                      onMouseDown={handleMouseDownPassword}
                                      edge="end"
                                    >
                                      {showPassword ? (
                                        <VisibilityOff />
                                      ) : (
                                        <Visibility />
                                      )}
                                    </IconButton>
                                  </InputAdornment>
                                }
                                label="password"
                                value={formik.values.password}
                                onBlur={formik.handleBlur}
                                error={
                                  formik.touched.password &&
                                  Boolean(formik.errors.password)
                                }
                              />
                              <FormHelperText
                                error={
                                  formik.touched.password &&
                                  Boolean(formik.errors.password)
                                }
                              >
                                {formik.touched.password &&
                                  formik.errors.password}
                              </FormHelperText>
                            </FormControl>
                          ) : (
                            <FormControl fullWidth></FormControl>
                          )}
                          {formik.values.method === "POST" ? (
                            <FormControl fullWidth variant="outlined">
                              <InputLabel size="small">
                                Repeat Password
                              </InputLabel>
                              <OutlinedInput
                                name="repeatpassword"
                                size="small"
                                value={formik.values.repeatpassword}
                                type={showPasswordrep ? "text" : "password"}
                                onChange={formik.handleChange}
                                endAdornment={
                                  <InputAdornment position="end">
                                    <IconButton
                                      aria-label="toggle password visibility"
                                      onClick={handleClickShowPasswordrep}
                                      onMouseDown={handleMouseDownPassword}
                                      edge="end"
                                    >
                                      {showPasswordrep ? (
                                        <VisibilityOff />
                                      ) : (
                                        <Visibility />
                                      )}
                                    </IconButton>
                                  </InputAdornment>
                                }
                                label="Repeat Password"
                              />
                              <FormHelperText
                                error={
                                  formik.touched.repeatpassword &&
                                  Boolean(formik.errors.repeatpassword)
                                }
                              >
                                {formik.touched.repeatpassword &&
                                  formik.errors.repeatpassword}
                              </FormHelperText>
                            </FormControl>
                          ) : (
                            <FormControl fullWidth></FormControl>
                          )}
                        </Stack>
                      </Box>
                      <Box
                        component="fieldset"
                        alignItems={"top"}
                        sx={{
                          borderRadius: 1,
                          borderColor: "#e6e6e6",
                          width: "50vw",
                        }}
                      >
                        <legend>Permission</legend>
                        {permissions?.length > 0 &&
                          permissions?.map((item, index) => (
                            <>
                              <Stack
                                spacing={1}
                                direction={"row"}
                                alignItems="center"
                                justifyContent="space-between"
                              >
                                <Typography variant="body1">
                                  {item.menu_name}
                                </Typography>
                                <RadioGroup
                                  name="permission"
                                  row
                                  value={item.permission}
                                  defaultValue={
                                    item.permission ? "View" : "None"
                                  }
                                  onChange={(e) =>
                                    handlePermissionChange(
                                      index,
                                      e.target.value
                                    )
                                  }
                                >
                                  <FormControl>
                                    <FormControlLabel
                                      control={<Radio />}
                                      value="Edit"
                                      label="Edit"
                                      size="small"
                                    />
                                  </FormControl>
                                  <FormControl>
                                    <FormControlLabel
                                      control={<Radio />}
                                      value="View"
                                      label="View"
                                      size="small"
                                    />
                                  </FormControl>
                                  <FormControl>
                                    <FormControlLabel
                                      control={<Radio />}
                                      value="None"
                                      label="None"
                                      size="small"
                                    />
                                  </FormControl>
                                </RadioGroup>
                              </Stack>
                              <Divider sx={{ mb: 1.4 }} />
                            </>
                          ))}
                      </Box>
                    </Box>
                    <Divider sx={{ mt: 1, mb: 1 }} />
                    <Box
                      sx={{ display: "flex", justifyContent: "space-between" }}
                    >
                      <Button variant="outlined" onClick={handleClose}>
                        Close
                      </Button>
                      <Button variant="contained" type="submit">
                        Save
                      </Button>
                    </Box>
                  </form>
                </Box>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal
        open={openpass}
        aria-labelledby="modal-changepassword-title"
        aria-describedby="modal-changepassword-description"
      >
        <Box sx={stylepass}>
          <ChangePasswordPage
            modalClose={modalClose}
            userMethod={userMethod}
            user_id={userId}
            username={userIdname}
          />
        </Box>
      </Modal>
      <Modal open={opens}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
      <Modal open={openNewPass}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            border: "1px solid #000",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography variant="h5" gutterBottom>
              New Password
            </Typography>
            <IconButton
              sx={{ display: "flex", mt: -1 }}
              aria-label="close"
              onClick={handlePassClose}
            >
              <CloseIcon />
            </IconButton>
          </Box>
          <Divider sx={{ mb: 1 }} />
          <FormControl fullWidth>
            <Typography variant="h6">
              Mail: <strong>{userIdname}</strong>
            </Typography>
            <Typography variant="h6">
              New Password: <strong>{newPass}</strong>
            </Typography>
          </FormControl>
          <Divider sx={{ mt: 1 }} />
          <Box
            sx={{
              display: "flex",
              mb: 1,
              mt: 1,
              justifyContent: "space-between",
            }}
          >
            <Button variant="outlined" onClick={handlePassClose}>
              Close
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
}
