"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import { useFormik } from "formik";
import AddIcon from "@mui/icons-material/Add";
import CloseIcon from "@mui/icons-material/Close";
import { GridRowModes, GridRowEditStopReasons } from "@mui/x-data-grid";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  FormControl,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
} from "@mui/material";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer
  
} from "@mui/x-data-grid";
import InventoryIcon from "@mui/icons-material/Inventory";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};
const style1 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 1200,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};

export default function ProductPage() {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [openProduct, setOpenProduct] = React.useState(false);
  const [tank, setTank] = React.useState([]);
  const [delTerminal, setDelTerminal] = React.useState();
  const [deleteItem, setDeleteItem] = React.useState();
  const [deleteItem1, setDeleteItem1] = React.useState();
  const [open1, setOpen1] = React.useState(false);
  const handleCloseProduct = () => {
    setOpenProduct(!openProduct);
  };
  const handleClose1 = () => {
    setOpen1(false);
  };
  const [product, setProduct] = React.useState({
    method: "",
    product_id: "",
    sequence: "",
    product_name: "",
    product_description: "",
  });
  const handleChangeProduct = (event) => {
    setProduct((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };
  const [rows, setRows] = React.useState("");
  const [rowModesModel, setRowModesModel] = React.useState({});

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
  };
  const handleDeleteClick = (id) => () => {
    setOpen1(true);
    setDeleteItem1(id);
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const handleClickOpenProduct = (currentRow) => {
    setDeleteItem(currentRow);
    setOpenProduct(true);
  };

  const [filterTextProduct, setFilterTextProduct] = React.useState("");
  const handleFilterChangeProduct = (e) => {
    setFilterTextProduct(e.target.value);
  };

  const {
    data: products,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/product`, session?.user?.token], fetcher);

  const columsProduct = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "sequence",
      renderHeader: () => <strong>{"Sequence"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "product_description",
      renderHeader: () => <strong>{"Product description"}</strong>,
      width: 200,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          setProduct({
            product_id: currentRow.product_id,
            product_name: currentRow.product_name,
            sequence: currentRow.sequence,
            product_description: currentRow.product_description,
            method: "PUT",
          });
          fetch(`/api/lookup/grade/${currentRow.product_id}`, {
            headers: {
              Authorization: `Bearer ${session?.user?.token}`,
            },
          })
            .then((res) => res.json())
            .then((d) =>
              setRows(
                d?.result?.map((row, i) => ({
                  id: i + 1,
                  grade_id: row.grade_id,
                  sequence: row.sequence,
                  grade_name: row.grade_name,
                  grade_description: row.grade_description,
                  type: "EDIT",
                }))
              )
            )
            .catch((err) => console.log(err));
          handleClickOpenProduct();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setProduct({
            product_id: currentRow.product_id,
            product_name: currentRow.product_name,
            sequence: currentRow.sequence,
            product_description: currentRow.product_description,
            method: "DELETE",
          });
          setDelTerminal(currentRow.product_id);
          handleClickOpenProduct(currentRow);
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const columnsTerminal = [
    {
      field: "grade_name",
      renderHeader: () => <strong>{"Grade name"}</strong>,
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "sequence",
      renderHeader: () => <strong>{"Sequence"}</strong>,
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "grade_description",
      renderHeader: () => <strong>{"Grade description"}</strong>,
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      width: 100,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModel[params.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(params.id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(params.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params.row.grade_id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  const formikProduct = useFormik({
    initialValues: product,
    validationSchema: yup.object({
      method: yup.string(),
      product_name: yup.string().required("Please insert product name!"),
      product_description: yup
        .string()
        .required("Please insert product description!"),
      sequence: yup.string().required("Please insert sequence!"),
    }),
    onSubmit: async () => {
      saveProduct();
    },
    enableReinitialize: true,
  });
  const deleteData = async (id) => {
    const body = {
      username: session?.user?.username,
      grade_id: deleteItem1,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/lookup/grade`,
        "DELETE",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = "Амжилттай устгалаа";
        setRows(rows.filter((row) => row.id !== deleteItem1));
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const deleteTerminal = async (params) => {
    const body = {
      product_id: delTerminal,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/lookup/product`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        mutate();
        let message = null;
        message = "Амжилттай устгалаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const saveProduct = async () => {
    const body = {
      product_id: product.method === "POST" ? undefined : product.product_id,
      product_name: product.product_name,
      product_description: product.product_description,
      sequence: product.sequence,
      grades: rows,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/product`,
        product.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа"
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleCloseProduct();
    }
  };

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;
    const handleClick = () => {
      let id = null;
      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            type: "ADD",
            id: id,
            isNew: true,
            sequence: 0,
            grade_name: "name",
            grade_description: "name",
          },
        ]);
      } else {
        id = rows?.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            type: "ADD",
            id: id,
            isNew: true,
            sequence: 0,
            grade_name: "name",
            grade_description: "name",
          },
        ]);
      }
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    return (
      <GridToolbarContainer
        style={{ display: "flex", justifyContent: "flex-end" }}
      >
        <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
          Add
        </Button>
      </GridToolbarContainer>
    );
  }

  if (tank?.result?.length > 0) {
    const tankRows = tank?.result?.map((row, i) => ({
      id: i + 1,
      grade_id: row.grade_id,
      sequence: row.sequence,
      grade_name: row.grade_name,
      grade_description: row.grade_description,
      type: "EDIT",
    }));
    setRows(tankRows);
  }

  const rowsProduct = products?.result?.map((row, i) => ({
    id: i + 1,
    product_id: row.product_id,
    product_name: row.product_name,
    sequence: row.sequence,
    product_description: row.product_description,
  }));

  const filteredRowsProduct = rowsProduct?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextProduct.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue
          .toLowerCase()
          .includes(filterTextProduct.toLowerCase());
      }
      return false;
    })
  );
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder=" Search Product"
              inputProps={{ "aria-label": "Search Product" }}
              value={filterTextProduct}
              onChange={handleFilterChangeProduct}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Product">
              <IconButton aria-label="add">
                <InventoryIcon
                  onClick={() => {
                    setProduct({
                      method: "POST",
                      product_id: "",
                      sequence: "",
                      product_name: "",
                      product_description: "",
                    });
                    setRows("");
                    handleClickOpenProduct();
                  }}
                />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>
        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsProduct}
          columns={columsProduct}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
            // 
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openProduct}
          onClose={handleCloseProduct}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {product.method === "DELETE" || product.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6" component="h3">
                    {product.method === "DELETE" ? "Product delete?" : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{product.product_name}</AlertTitle>
                      {product.method === "DELETE"
                        ? "Are you sure delete?"
                        : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseProduct} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      product.method === "DELETE"
                        ? deleteTerminal()
                        : resetPassword();
                      handleCloseProduct();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <Box sx={style1}>
                <form onSubmit={formikProduct.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Product
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseProduct}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    spacing={1}
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                  >
                    <FormControl fullWidth>
                      <TextField
                        id="product_name"
                        name="product_name"
                        margin="dense"
                        label="Product name"
                        size="small"
                        variant="outlined"
                        value={formikProduct.values.product_name}
                        onChange={handleChangeProduct}
                      />
                      <FormHelperText
                        error={
                          formikProduct.touched.product_name &&
                          Boolean(formikProduct.errors.product_name)
                        }
                      >
                        {formikProduct.touched.product_name &&
                          formikProduct.errors.product_name}
                      </FormHelperText>
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        margin="dense"
                        id="sequence"
                        size="small"
                        label="Sequence"
                        name="sequence"
                        variant="outlined"
                        onChange={handleChangeProduct}
                        value={formikProduct.values.sequence}
                      />
                      <FormHelperText
                        error={
                          formikProduct.touched.sequence &&
                          Boolean(formikProduct.errors.sequence)
                        }
                      >
                        {formikProduct.touched.sequence &&
                          formikProduct.errors.sequence}
                      </FormHelperText>
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        id="product_description"
                        name="product_description"
                        margin="dense"
                        label="Product description"
                        size="small"
                        variant="outlined"
                        value={formikProduct.values.product_description}
                        onChange={handleChangeProduct}
                      />
                      <FormHelperText
                        error={
                          formikProduct.touched.product_description &&
                          Boolean(formikProduct.errors.product_description)
                        }
                      >
                        {formikProduct.touched.product_description &&
                          formikProduct.errors.product_description}
                      </FormHelperText>
                    </FormControl>
                  </Stack>
                  <Box
                    sx={{
                      height: "30vh",
                      width: "60vw",
                      "& .actions": {
                        color: "text.secondary",
                      },
                      "& .textPrimary": {
                        color: "text.primary",
                      },
                    }}
                  >
                    <DataGrid
                      rows={rows ? rows : []}
                      columns={columnsTerminal}
                      editMode="row"
                      rowModesModel={rowModesModel}
                      onRowModesModelChange={handleRowModesModelChange}
                      onRowEditStop={handleRowEditStop}
                      processRowUpdate={processRowUpdate}
                      slots={{
                        toolbar: EditToolbar,
                      }}
                      slotProps={{
                        toolbar: { setRows, setRowModesModel },
                      }}
                      sx={{
                        backgroundColor: "#FFFFFF",
                        "& .MuiDataGrid-cell:hover": {
                          color: "primary.main",
                        },
                        "& .MuiDataGrid-cell": {
                          borderTop: 1,
                          borderTopColor: "#E9ECF0",
                        },
                        maxWidth: 1170,
                        minWidth: 1100,
                      }}
                    />
                  </Box>

                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseProduct}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                  <Modal open={open1} onClose={handleClose1}>
                    <Box
                      sx={{
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        bgcolor: "background.paper",
                        boxShadow: 24,
                        p: 4,
                      }}
                    >
                      <Box>
                        <Typography
                          id="modal-modal-title"
                          variant="h6"
                          component="h3"
                        >
                          Confirm
                        </Typography>
                      </Box>
                      <Stack
                        component="div"
                        sx={{
                          width: 400,
                          maxWidth: "100%",
                        }}
                        noValidate
                        autoComplete="off"
                      >
                        <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                          <Alert severity="error">
                            <AlertTitle>{""}</AlertTitle>
                            Are you sure you want to delete?
                          </Alert>
                        </Box>
                      </Stack>
                      <Box
                        sx={{
                          display: "flex",
                          marginBottom: 1,
                          justifyContent: "flex-end",
                        }}
                      >
                        <Button
                          onClick={async () => {
                            handleClose1();
                            deleteData();
                          }}
                          variant="contained"
                          sx={{
                            marginLeft: "10px",
                            marginRight: "10px",
                          }}
                        >
                          YES
                        </Button>
                        <Button onClick={handleClose1} variant="outlined">
                          NO
                        </Button>
                      </Box>
                    </Box>
                  </Modal>
                  <Modal open={open}>
                      <Box
                        sx={{
                          position: "absolute",
                          top: "50%",
                          left: "50%",
                          minWidth: 600,
                          transform: "translate(-50%, -50%)",
                          bgcolor: "transparent",
                          overflowY: "auto",
                          overflowX: "auto",
                          boxShadow: 0,
                          p: 1,
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            justifyContent: "center",
                            m: 3,
                          }}
                        >
                          <CircularProgress sx={{ color: "#ffffff" }} />
                        </Box>
                      </Box>
                    </Modal>
                </form>
              </Box>
            )}
          </Box>
        </Modal>
      </Card>
    </>
  );
}
