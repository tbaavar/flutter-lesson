"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  FormControl,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
} from "@mui/material";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import InventoryIcon from "@mui/icons-material/Inventory";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";

import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};

export default function BankPage() {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [openBank, setOpenBank] = React.useState(false);

  const handleCloseBank = () => {
    setOpenBank(!openBank);
  };

  const [bank, setBank] = React.useState({
    method: "",
    bank_id: "",
    bank_name: "",
    bank_code: "",
    swift_code: "",
  });

  const handleChangeBank = (event) => {
    setBank((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };

  const handleClickOpenBank = () => {
    setOpenBank(true);
  };

  const [filterTextBank, setFilterTextBank] = React.useState("");
  const handleFilterChangeBank = (e) => {
    setFilterTextBank(e.target.value);
  };

  const columsBank = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "bank_name",
      renderHeader: () => <strong>{"Bank name"}</strong>,
      width: 350,
      editable: false,
      sortable: false,
    },
    {
      field: "swift_code",
      renderHeader: () => <strong>{"Swift name"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "bank_code",
      renderHeader: () => <strong>{"Bank code"}</strong>,
      width: 100,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          setBank({
            bank_id: currentRow.bank_id,
            bank_name: currentRow.bank_name,
            swift_code: currentRow.swift_code,
            bank_code: currentRow.bank_code,
            method: "PUT",
          });
          handleClickOpenBank();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setBank({
            bank_id: currentRow.bank_id,
            bank_name: currentRow.bank_name,
            swift_code: currentRow.swift_code,
            bank_code: currentRow.bank_code,
            method: "DELETE",
          });
          handleClickOpenBank();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const {
    data: banks,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/bank`, session?.user?.token], fetcher);

  const formikBank = useFormik({
    initialValues: bank,
    validationSchema: yup.object({
      method: yup.string(),
      bank_name: yup.string().required("Please insert bank name!"),
      swift_code: yup.string().required("Please insert swift code!"),
      bank_code: yup.string().required("Please insert bank code!"),
    }),
    onSubmit: async () => {
      saveBank();
    },
    enableReinitialize: true,
  });

  const saveBank = async () => {
    const body = {
      bank_id: bank.method === "POST" ? undefined : bank.bank_id,
      bank_name: bank.bank_name,
      swift_code: bank.swift_code,
      bank_code: bank.bank_code,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/bank`,
        bank.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleCloseBank();
    }
  };

  const rowsBank = banks?.result?.map((row, i) => ({
    id: i + 1,
    bank_id: row.bank_id,
    bank_name: row.bank_name,
    swift_code: row.swift_code,
    bank_code: row.bank_code,
  }));

  const filteredRowsBank = rowsBank?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextBank.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue.toLowerCase().includes(filterTextBank.toLowerCase());
      }
      return false;
    })
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder=" Search Bank"
              inputProps={{ "aria-label": "Search Bank" }}
              value={filterTextBank}
              onChange={handleFilterChangeBank}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Bank">
              <IconButton aria-label="add">
                <InventoryIcon
                  onClick={() => {
                    setBank({
                      method: "POST",
                      bank_id: "",
                      swift_code: "",
                      bank_name: "",
                      bank_code: "",
                    });
                    handleClickOpenBank();
                  }}
                />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>
        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsBank}
          columns={columsBank}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openBank}
          onClose={handleCloseBank}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {bank.method === "DELETE" || bank.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6" component="h3">
                    {bank.method === "DELETE" ? "Bank delete?" : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{bank.bank_name}</AlertTitle>
                      {bank.method === "DELETE" ? "Are you sure delete?" : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseBank} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      bank.method === "DELETE" ? saveBank() : resetPassword();
                      handleCloseBank();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikBank.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Bank
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseBank}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    sx={{
                      width: 400,
                      maxWidth: "100%",
                    }}
                    autoComplete="off"
                  >
                    <FormControl>
                      <TextField
                        id="bank_name"
                        name="bank_name"
                        margin="dense"
                        label="bank name"
                        size="small"
                        variant="outlined"
                        value={formikBank.values.bank_name}
                        onChange={handleChangeBank}
                      />
                      <FormHelperText
                        error={
                          formikBank.touched.bank_name &&
                          Boolean(formikBank.errors.bank_name)
                        }
                      >
                        {formikBank.touched.bank_name &&
                          formikBank.errors.bank_name}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="swift_code"
                        name="swift_code"
                        margin="dense"
                        label="swift code"
                        size="small"
                        variant="outlined"
                        value={formikBank.values.swift_code}
                        onChange={handleChangeBank}
                      />
                      <FormHelperText
                        error={
                          formikBank.touched.swift_code &&
                          Boolean(formikBank.errors.swift_code)
                        }
                      >
                        {formikBank.touched.swift_code &&
                          formikBank.errors.swift_code}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="bank_code"
                        name="bank_code"
                        margin="dense"
                        label="bank code"
                        size="small"
                        variant="outlined"
                        value={formikBank.values.bank_code}
                        onChange={handleChangeBank}
                      />
                      <FormHelperText
                        error={
                          formikBank.touched.bank_code &&
                          Boolean(formikBank.errors.bank_code)
                        }
                      >
                        {formikBank.touched.bank_code &&
                          formikBank.errors.bank_code}
                      </FormHelperText>
                    </FormControl>
                  </Stack>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseBank}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
