"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import CloseIcon from "@mui/icons-material/Close";
import FileOpenIcon from "@mui/icons-material/FileOpen";
import { useFormik } from "formik";
import {
  Stack,
  Box,
  Button,
  MenuItem,
  Card,
  TextField,
  FormControl,
  InputLabel,
  Select,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  CircularProgress,
  IconButton,
  FormHelperText,
  Tooltip,
  Grid,
  InputBase,
  Divider,
} from "@mui/material";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer,
} from "@mui/x-data-grid";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};

export default function CostsPage() {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [openCost, setOpenCost] = React.useState(false);
  const handleCloseCost = () => {
    setOpenCost(!openCost);
  };
  const [cost, setCost] = React.useState({
    method: "",
    cost_code: "",
    cost_name: "",
    cost_type: "",
  });
  const handleChangeCost = (event) => {
    setCost((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };

  const handleClickOpenCost = () => {
    setOpenCost(true);
  };
  const [filterTextCost, setFilterTextCost] = React.useState("");
  const handleFilterChangeCost = (e) => {
    setFilterTextCost(e.target.value);
  };
  const { data: Costtype } = useSWR(
    [`/api/lookup/Cost_type`, session?.user?.token],
    fetcher
  );
  const {
    data: costs,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/cost`, session?.user?.token], fetcher);

  const columnsPort = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "cost_code",
      renderHeader: () => <strong>{"Cost code"}</strong>,
      width: 350,
      editable: false,
      sortable: false,
    },
    {
      field: "cost_name",
      renderHeader: () => <strong>{"Cost name"}</strong>,
      width: 350,
      editabe: false,
      sortable: false,
    },
    {
      field: "cost_type",
      renderHeader: () => <strong>{"Cost type"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          setCost({
            cost_code: currentRow.cost_code,
            cost_name: currentRow.cost_name,
            cost_type: currentRow.cost_type,

            method: "PUT",
          });
          handleClickOpenCost();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setCost({
            cost_code: currentRow.cost_code,
            cost_name: currentRow.cost_name,
            cost_type: currentRow.cost_type,

            method: "DELETE",
          });
          handleClickOpenCost();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];
  const formikCost = useFormik({
    initialValues: cost,
    validationSchema: yup.object({
      method: yup.string(),
      cost_code: yup.string().required("Please insert cost code!"),
      cost_name: yup.string().required("Please insert cost name!"),
      cost_type: yup.string().required("Please select cost type!"),
    }),
    onSubmit: async () => {
      saveCost();
    },
    enableReinitialize: true,
  });
  const saveCost = async () => {
    const body = {
      cost_code: cost.cost_code,
      cost_name: cost.cost_name,
      cost_type: cost.cost_type,
      username: session?.user?.username,
    };
 
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/cost`,
        cost.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        mutate();
        message = "Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleCloseCost();
    }
  };
  const rowsCost = costs?.result?.map((row, i) => ({
    id: i + 1,
    cost_code: row.cost_code,
    cost_name: row.cost_name,
    cost_type: row.cost_type,
  }));

  const filteredrowsCost = rowsCost?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextCost.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue.toLowerCase().includes(filterTextCost.toLowerCase());
      }
      return false;
    })
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder="Search Port"
              inputProps={{ "aria-label": "Search Port" }}
              value={filterTextCost}
              onChange={handleFilterChangeCost}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Port">
              <IconButton
                aria-label="add"
                onClick={() => {
                  setCost({
                    method: "POST",
                    cost_code: "",
                    cost_name: "",
                    cost_type: "",
                  });

                  handleClickOpenCost();
                }}
              >
                <FileOpenIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>

        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredrowsCost}
          columns={columnsPort}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
            //
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openCost}
          onClose={handleCloseCost}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {cost.method === "DELETE" || cost.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6">
                    {cost.method === "DELETE" ? "Cost delete?" : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{cost.cost_name}</AlertTitle>
                      {cost.method === "DELETE" ? "Are you sure delete?" : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleCloseCost} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      cost.method === "DELETE" ? saveCost() : resetPassword();
                      handleCloseCost();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikCost.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Cost
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseCost}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    sx={{
                      width: 400,
                      maxWidth: "100%",
                    }}
                    autoComplete="off"
                  >
                    <FormControl>
                      <TextField
                        id="cost_code"
                        name="cost_code"
                        margin="dense"
                        label="Cost code"
                        size="small"
                        variant="outlined"
                        disabled={
                          formikCost.values.method === "PUT" ? true : false
                        }
                        value={formikCost.values.cost_code}
                        onChange={handleChangeCost}
                      />
                      <FormHelperText
                        error={
                          formikCost.touched.cost_code &&
                          Boolean(formikCost.errors.cost_code)
                        }
                      >
                        {formikCost.touched.cost_code &&
                          formikCost.errors.cost_code}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="cost_name"
                        name="cost_name"
                        margin="dense"
                        label="Cost name"
                        size="small"
                        variant="outlined"
                        value={formikCost.values.cost_name}
                        onChange={handleChangeCost}
                      />
                      <FormHelperText
                        error={
                          formikCost.touched.cost_name &&
                          Boolean(formikCost.errors.cost_name)
                        }
                      >
                        {formikCost.touched.cost_name &&
                          formikCost.errors.cost_name}
                      </FormHelperText>
                    </FormControl>

                    <FormControl size="small" fullWidth margin="dense">
                      <InputLabel
                        id="cost_type"
                        error={
                          formikCost.touched.cost_type &&
                          Boolean(formikCost.errors.cost_type)
                        }
                      >
                        Cost type
                      </InputLabel>
                      <Select
                        defaultValue=""
                        labelId="cost_type"
                        id="cost_type"
                        name="cost_type"
                        label="cost_type"
                        onChange={handleChangeCost}
                        value={formikCost.values.cost_type}
                        onBlur={formikCost.handleBlur}
                        error={
                          formikCost.touched.cost_type &&
                          Boolean(formikCost.errors.cost_type)
                        }
                      >
                        {Costtype?.result.length > 0 &&
                          Costtype?.result.map((cost) => (
                            <MenuItem
                              value={cost.lookup_code}
                              key={cost.lookup_code}
                            >
                              {cost.name}
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText
                        error={
                          formikCost.touched.cost_type &&
                          Boolean(formikCost.errors.cost_type)
                        }
                      >
                        {formikCost.touched.cost_type &&
                          formikCost.errors.cost_type}
                      </FormHelperText>
                    </FormControl>
                  </Stack>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseCost}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
