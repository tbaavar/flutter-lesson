"use client";
import * as React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import CloseIcon from "@mui/icons-material/Close";
import FileOpenIcon from "@mui/icons-material/FileOpen";
import { useFormik } from "formik";
import {
  Stack,
  Box,
  Button,
  Card,
  TextField,
  FormControl,
  ListItemText,
  Alert,
  AlertTitle,
  Modal,
  Typography,
  OutlinedInput,
  InputLabel,
  CircularProgress,
  IconButton,
  FormHelperText,
  Select,
  Tooltip,
  Grid,
  MenuItem,
  InputBase,
  Checkbox,
  Divider,
} from "@mui/material";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer,
} from "@mui/x-data-grid";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import SearchIcon from "@mui/icons-material/Search";
import CustomNoRowsOverlay from "@/components/NoRecord";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};

export default function PortPage() {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [item, setItem] = React.useState(false);
  const [openPort, setOpenPort] = React.useState(false);
  const handleClosePort = () => {
    setOpenPort(!openPort);
  };
  const [port, setPort] = React.useState({
    method: "",
    port_id: "",
    port_name: "",
    description: "",
    sequence: "",
  });
  const handleChangePort = (event) => {
    setPort((prev) => {
      return {
        ...prev,
        [event.target.name]: event.target.value,
      };
    });
  };
  const handleClickOpenPort = () => {
    setOpenPort(true);
  };
  const [filterTextPort, setFilterTextPort] = React.useState("");
  const handleFilterChangePort = (e) => {
    setFilterTextPort(e.target.value);
  };
  const { data: organization } = useSWR(
    [`/api/organization`, session?.user?.token],
    fetcher
  );
  const {
    data: ports,
    error,
    isLoading,
    mutate,
  } = useSWR([`/api/lookup/port`, session?.user?.token], fetcher);

  const columnsPort = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      editable: false,
      sortable: false,
    },
    {
      field: "port_name",
      renderHeader: () => <strong>{"Port"}</strong>,
      width: 350,
      editable: false,
      sortable: false,
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 350,
      editabe: false,
      sortable: false,
    },
    {
      field: "sequence",
      renderHeader: () => <strong>{"Sequence"}</strong>,
      width: 150,
      editable: false,
      sortable: false,
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 200,
      sortable: false,
      renderCell: (params) => {
        const handleUserEdit = () => {
          const currentRow = params.row;
          setPort({
            port_id: currentRow.port_id,
            port_name: currentRow.port_name,
            description: currentRow.description,
            sequence: currentRow.sequence,
            method: "PUT",
          });
          fetch(`/api/lookup/port/${currentRow.port_id}`, {
            headers: {

              Authorization: `Bearer ${session?.user?.token}`,
            },
          })
            .then((res) => res.json())
            .then((d) => {
              const orgItems = d?.result.map((item) => item.org_id);
              orgaFormik.setFieldValue("organizations", orgItems);
            })
            .catch((err) => console.log(err));
          handleClickOpenPort();
        };

        const handleUserDelete = () => {
          const currentRow = params.row;
          setPort({
            port_id: currentRow.port_id,
            port_name: currentRow.port_name,
            description: currentRow.description,
            sequence: currentRow.sequence,
            method: "DELETE",
          });
          handleClickOpenPort();
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleUserEdit}
              />
            </Tooltip>

            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleUserDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];
  const formikPort = useFormik({
    initialValues: port,
    validationSchema: yup.object({
      method: yup.string(),
      port_name: yup.string().required("Please insert port name!"),
      description: yup.string().required("Please insert description !"),
      sequence: yup.string().required("Please insert sequence"),
    }),
    onSubmit: async () => {
      savePort();
    },
    enableReinitialize: true,
  });
  const handleOrgsChange = (event) => {
    orgaFormik.setFieldValue("organizations", event.target.value);
  };
  const orgaFormik = useFormik({
    initialValues: {
      organizations: [],
    },
    validationSchema: yup.object({}).required("Please choose org"),
    enableReinitialize: true,
  });
  const savePort = async () => {
    const arr = [];
    {
      orgaFormik.values.organizations.map((item1) => {
        arr.push({ org_id: item1 });
      });
    }
    const body = {
      port_id: port.method === "POST" ? undefined : port.port_id,
      port_name: port.port_name,
      description: port.description,
      sequence: port.sequence,
      orgs: arr,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/lookup/port`,
        port.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        mutate();
        message = "Амжилттай хадгаллаа";
        mutate();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Error", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Error", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleClosePort();
    }
  };
  const rowsPort = ports?.result?.map((row, i) => ({
    id: i + 1,
    port_id: row.port_id,
    port_name: row.port_name,
    description: row.description,
    sequence: row.sequence,
  }));

  const filteredRowsPort = rowsPort?.filter((row) =>
    Object.values(row).some((value) => {
      if (typeof value === "string") {
        return value.toLowerCase().includes(filterTextPort.toLowerCase());
      } else if (typeof value === "number") {
        const stringValue = String(value);
        return stringValue.toLowerCase().includes(filterTextPort.toLowerCase());
      }
      return false;
    })
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  return (
    <>
      <Card sx={{ p: 2, ml: -3, mt: -3 }}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <IconButton
              type="button"
              sx={{
                backgroundColor: "white",
                border: "none",
                borderRadius: "0.2rem 0 0 0.2rem",
                height: 30,
                mb: 0.2,
              }}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              sx={{
                flex: 1,
                backgroundColor: "white",
                border: "none",
                borderRadius: "0 0.2rem 0.2rem 0",
                width: 250,
                height: 30,
              }}
              placeholder="Search Port"
              inputProps={{ "aria-label": "Search Port" }}
              value={filterTextPort}
              onChange={handleFilterChangePort}
            />
          </Grid>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Tooltip title="Add Port">
              <IconButton
                aria-label="add"
                onClick={() => {
                  setPort({
                    method: "POST",
                    port_id: "",
                    port_name: "",
                    sequence: "",
                    description: "",
                  });
                  orgaFormik.setValues({
                    organizations: [],
                  });
                  handleClickOpenPort();
                }}
              >
                <FileOpenIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </Grid>

        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRowsPort}
          columns={columnsPort}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 45]}
          autoHeight
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
            //
          }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
          rowHeight={25}
          disableRowSelectionOnClick
        />
        <Modal
          open={openPort}
          onClose={handleClosePort}
          aria-labelledby="Add modal"
          aria-describedby="ADD modal description"
        >
          <Box sx={style}>
            {port.method === "DELETE" || port.method === "PATCH" ? (
              <>
                <Box>
                  <Typography variant="h6">
                    {port.method === "DELETE" ? "Port delete?" : null}
                  </Typography>
                </Box>
                <Stack
                  component="div"
                  sx={{
                    width: 400,
                    maxWidth: "100%",
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                    <Alert severity="error">
                      <AlertTitle>{port.port_name}</AlertTitle>
                      {port.method === "DELETE" ? "Are you sure delete?" : null}
                    </Alert>
                  </Box>
                </Stack>
                <Box
                  sx={{
                    display: "flex",
                    marginBottom: 1,
                    justifyContent: "flex-end",
                  }}
                >
                  <Button onClick={handleClosePort} variant="outlined">
                    no
                  </Button>
                  <Button
                    onClick={async () => {
                      port.method === "DELETE" ? savePort() : resetPassword();
                      handleClosePort();
                    }}
                    variant="contained"
                    sx={{
                      marginLeft: "10px",
                      marginRight: "10px",
                    }}
                  >
                    yes
                  </Button>
                </Box>
              </>
            ) : (
              <>
                <form onSubmit={formikPort.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Port
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleClosePort}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack
                    sx={{
                      width: 400,
                      maxWidth: "100%",
                    }}
                    autoComplete="off"
                  >
                    <FormControl fullWidth>
                      <InputLabel size="small" id="orgs">
                        Orgs
                      </InputLabel>
                      <Select
                        id="orgs"
                        name="orgs"
                        multiple
                        size="small"
                        sx={{ backgroundColor: "white" }}
                        value={orgaFormik.values.organizations}
                        onChange={handleOrgsChange}
                        input={<OutlinedInput label="orgs" />}
                        SelectDisplayProps={item.name}
                        renderValue={(selected) => {
                          return selected
                            .map(
                              (id) =>
                                organization?.result.find(
                                  (item) => item.org_id === id
                                )?.name
                            )
                            .join(", ");
                        }}
                      >
                        {organization?.result?.map((item) => (
                          <MenuItem key={item.org_id} value={item.org_id}>
                            <Checkbox
                              checked={
                                orgaFormik.values.organizations.indexOf(
                                  item.org_id
                                ) > -1
                              }
                            />
                            <ListItemText
                              primary={item.name}
                              style={{ color: "black" }}
                            />
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="port_name"
                        name="port_name"
                        margin="dense"
                        label="Port name"
                        size="small"
                        variant="outlined"
                        value={formikPort.values.port_name}
                        onChange={handleChangePort}
                      />
                      <FormHelperText
                        error={
                          formikPort.touched.port_name &&
                          Boolean(formikPort.errors.port_name)
                        }
                      >
                        {formikPort.touched.port_name &&
                          formikPort.errors.port_name}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="description"
                        name="description"
                        margin="dense"
                        label="Description"
                        size="small"
                        variant="outlined"
                        value={formikPort.values.description}
                        onChange={handleChangePort}
                      />
                      <FormHelperText
                        error={
                          formikPort.touched.description &&
                          Boolean(formikPort.errors.description)
                        }
                      >
                        {formikPort.touched.description &&
                          formikPort.errors.description}
                      </FormHelperText>
                    </FormControl>
                    <FormControl>
                      <TextField
                        id="sequence"
                        name="sequence"
                        margin="dense"
                        label="Sequence"
                        size="small"
                        variant="outlined"
                        value={formikPort.values.sequence}
                        onChange={handleChangePort}
                      />
                      <FormHelperText
                        error={
                          formikPort.touched.sequence &&
                          Boolean(formikPort.errors.sequence)
                        }
                      >
                        {formikPort.touched.sequence &&
                          formikPort.errors.sequence}
                      </FormHelperText>
                    </FormControl>
                  </Stack>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleClosePort}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </>
            )}
          </Box>
        </Modal>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
