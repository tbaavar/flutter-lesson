"use client";
import React, { useState } from "react";
import DealTransactionReport from "@/components/DealTransactionReport";
import { useSession } from "next-auth/react";

import {
  Grid,
  FormControl,
  Box,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import SalesReport from "@/components/SalesReportReport";
import CustomerAccount from "@/components/CustomerAccount";
import Recievable from "@/components/RecievableControlList";
import SummaryList from "@/components/SummaryList";

export default function ReportPage() {
  const { data: session, status } = useSession();
  const [value, setValue] = useState(10);

  const handleChange = (event) => {
    setValue(event.target.value);
  };
  return (
      <Grid container spacing={3} marginTop={2}>
        <Grid item xs={12} lg={12} bgcolor={"white"}>
            <Box sx={{ minwidth: 30, ml: 1}}>
              <FormControl>
                <InputLabel id="demo-simple-select-label">Report</InputLabel>
                <Select
                sx={{ height: "34px", width: "205px" }}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={value}
                  label="Report"
                  onChange={handleChange}
                >
                  <MenuItem value={10}>
                    Deal transaction report
                  </MenuItem>
                   <MenuItem value={20}>
                    Sales report
                  </MenuItem>
                  <MenuItem value={30}>Customer account</MenuItem> 
                  <MenuItem value={40}>Recievable List</MenuItem> 
                  <MenuItem value={50}>Recievable Summary</MenuItem> 
                </Select>
              </FormControl>
            </Box>
            <Box mt={3}>
              {value === 10 ? (
                <Grid container spacing={3} p={1}>
                  <Grid item xs>
                    <DealTransactionReport />
                  </Grid>
                </Grid>
              ) : null}
              {value === 20 ? (
                <Grid container spacing={3} p={1}>
                  <Grid item xs>
                    <SalesReport />
                  </Grid>
                </Grid>
              ) : null}
              {value === 30 ? (
                <Grid container spacing={3} p={1}>
                  <Grid item xs>
                    <CustomerAccount />
                  </Grid>
                </Grid>
              ) : null}
               {value === 40 ? (
                <Grid container spacing={3} p={1}>
                  <Grid item xs>
                    <Recievable />
                  </Grid>
                </Grid>
              ) : null}
              {value === 50 ? (
                <Grid container spacing={3} p={1}>
                  <Grid item xs>
                    <SummaryList />
                  </Grid>
                </Grid>
              ) : null}
            </Box>
        </Grid>
      </Grid>
  );
}
