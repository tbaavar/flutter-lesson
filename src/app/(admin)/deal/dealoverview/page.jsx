"use client";
import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { styled } from "@mui/material/styles";
import CircularProgress from "@mui/material/CircularProgress";
import MyAlert from "@/components/Myalert";
import CustomNoRowsOverlay from "@/components/NoRecord";
import HandshakeIcon from "@mui/icons-material/Handshake";
import {
  Box,
  Stack,
  TextField,
  FormControl,
  Grid,
  Typography,
} from "@mui/material";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Chip from "@mui/material/Chip";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { DataGrid } from "@mui/x-data-grid";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import useSWR from "swr";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";
import InsertDriveFileOutlinedIcon from "@mui/icons-material/InsertDriveFileOutlined";
import dayjs from "dayjs";
import { numberWithCommas } from "@/lib/numberWithCommas";
import { AntTabs, AntTab, CustomTabPanel, a11yProps } from "@/components/CustomTabControl";

const parcelcolumns = [
  {
    field: "parcel_number",
    renderHeader: () => <strong>{"Parcel/Trade"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.parcel_number}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {"T- " + params.row.trade_id} </strong>
        </Typography>
      </Box>
    ),
    width: 130,
  },
  {
    field: "counterparty_name",
    renderHeader: () => <strong>{"Counterparty"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.counterparty_name}</Typography>
        {params.row.trade_type === "Spot_Purchase" ||
        params.row.trade_type === "Term_Purchase" ? (
          <Typography variant="h7" color={"#0b626b"}>
            <strong>{params.row.trade_type_name}</strong>
          </Typography>
        ) : (
          <Typography variant="h7" color={"#e27671"}>
            <strong>{params.row.trade_type_name}</strong>
          </Typography>
        )}
      </Box>
    ),
    width: 180,
  },
  {
    field: "term_name",
    renderHeader: () => <strong>{"Terms"}</strong>,
    width: 150,
  },

  {
    field: "product_name",
    renderHeader: () => <strong>{"Product/Grade"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.product_name}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.grade_name} </strong>
        </Typography>
      </Box>
    ),
    width: 180,
  },

  {
    field: "vessel_name",
    renderHeader: () => <strong>{"Transportation"}</strong>,
    width: 100,
  },
  {
    field: "shipping_quantity_mt",
    renderHeader: () => <strong>{"Shipping quantity (MT)"}</strong>,
    renderCell: (params) => (
      <Typography
        variant="h7"
        color={params.row.est_quantity_flag === "N" ? "#0b626b" : "#e27671"}
      >
        <strong>{numberWithCommas(params.row.shipping_quantity_mt)}</strong>
      </Typography>
    ),
    width: 150,
  },
  {
    field: "shipping_quantity_cm",
    renderHeader: () => <strong>{"Shipping quantity (CM)"}</strong>,
    renderCell: (params) => (
      <Typography
        variant="h7"
        color={params.row.est_quantity_flag === "N" ? "#0b626b" : "#e27671"}
      >
        <strong>{numberWithCommas(params.row.shipping_quantity_cm)}</strong>
      </Typography>
    ),
    width: 150,
  },
  {
    field: "border_crossing_date",
    renderHeader: () => <strong>{"Border cross date"}</strong>,
    renderCell: (params) => {
      if (params.row.border_crossing_date_est_flag === "N") {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={dayjs(params.row.border_crossing_date).format("YYYY-MM-DD")}
          />
        );
      } else
        return (
          <Chip
            variant="filled"
            color="urgent"
            label={dayjs(params.row.border_crossing_date).format("YYYY-MM-DD")}
          />
        );
    },
    width: 130,
  },

  {
    field: "transfer_date",
    renderHeader: () => <strong>{"Transfer date"}</strong>,
    renderCell: (params) => {
      if (params.row.transfer_date_est_flag === "N") {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
          />
        );
      } else
        return (
          <Chip
            variant="filled"
            color="urgent"
            label={dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
          />
        );
    },
    width: 130,
  },
  {
    field: "supply_end_date",
    renderHeader: () => <strong>{"Supply end date"}</strong>,
    renderCell: (params) => {
      if (params.row.no_agreed_supply_date_flag === "N") {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={dayjs(params.row.supply_end_date).format("YYYY-MM-DD")}
          />
        );
      } else
        return (
          <Chip
            variant="filled"
            color="urgent"
            label={dayjs(params.row.supply_end_date).format("YYYY-MM-DD")}
          />
        );
    },
    width: 130,
  },

  {
    field: "operator_name",
    renderHeader: () => <strong>{"Operator"}</strong>,
    width: 130,
  },
  {
    field: "financer_name",
    renderHeader: () => <strong>{"Financer"}</strong>,
    width: 130,
  },
];
const storageColumns = [
  {
    field: "parcel_movement_id",
    renderHeader: () => <strong>{"Movement Id"}</strong>,
    width: 100,
  },

  {
    field: "transfer_type",
    renderHeader: () => <strong>{"Transfer"}</strong>,
    width: 110,
  },
  {
    field: "terminal_from_date",
    renderHeader: () => <strong>{"Operation Date"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {"From: " + dayjs(params.row.terminal_from_date).format("YYYY-MM-DD")}
        </Typography>
        <Typography variant="h7">
          {"To: " + dayjs(params.row.terminal_to_date).format("YYYY-MM-DD")}
        </Typography>
        <Typography variant="h7">
          {"TR: " +
            dayjs(params.row.transfer_completion_date).format("YYYY-MM-DD")}
        </Typography>
      </Box>
    ),
    width: 150,
  },
  {
    field: "terminal_name",
    renderHeader: () => <strong>{"Tank name"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.terminal_name}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.tank_name} </strong>
        </Typography>
      </Box>
    ),
    width: 180,
  },
  {
    field: "tank_product",
    renderHeader: () => <strong>{"Product"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.tank_product}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.tank_grade} </strong>
        </Typography>
      </Box>
    ),
    width: 180,
  },
  {
    field: "quantity",
    renderHeader: () => <strong>{"Quantity"}</strong>,

    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.quantity}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.tank_unit_name} </strong>
        </Typography>
      </Box>
    ),
    width: 180,
  },
  {
    field: "transfer_status",
    renderHeader: () => <strong>{"Status"}</strong>,
    width: 120,
  },
];
const invoiceColumns = [
  {
    field: "deal_id",
    renderHeader: () => <strong>{"Deal/Trade ID"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        {params.row.deal_id ? (
          <Typography variant="h7">{"D- " + params.row.deal_id}</Typography>
        ) : null}
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {"T- " + params.row.trade_id} </strong>
        </Typography>
      </Box>
    ),
    width: 120,
  },
  {
    field: "counterparty_name",
    renderHeader: () => <strong>{"Counterparty"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.counterparty_name}</Typography>
        {params.row.trade_type === "SP" || params.row.trade_type === "TP" ? (
          <Typography variant="h7" color={"#0b626b"}>
            <strong>{params.row.trade_type_name}</strong>
          </Typography>
        ) : (
          <Typography variant="h7" color={"#e27671"}>
            <strong>{params.row.trade_type_name}</strong>
          </Typography>
        )}
      </Box>
    ),
    width: 180,
  },
  {
    field: "credit_type_name",
    renderHeader: () => <strong>{"Credit type"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.credit_type_name}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong>{dayjs(params.row.invoice_date).format("YYYY-MM-DD")}</strong>
        </Typography>
      </Box>
    ),
    width: 120,
  },
  // {
  //   field: "transport_type",
  //   renderHeader: () => <strong>{"Transport type"}</strong>,
  //   width: 120,
  // },
  {
    field: "invoice_number",
    renderHeader: () => <strong>{"Invoice number"}</strong>,
    width: 120,
  },
  {
    field: "invoice_type_name",
    renderHeader: () => <strong>{"Invoice Type"}</strong>,
    alignProperty: "center",
    renderCell: (params) => (
      <Box
        sx={{
          flexDirection: "column",
          display: "flex",
          justifyContent: "center",
        }}
      >
        {" "}
        <Typography variant="h7">{params.row.invoice_state}</Typography>
        {params.row.invoice_type === "PO" ? (
          <Typography variant="h7" color={"#e27671"}>
            <strong>{params.row.invoice_type_name}</strong>
          </Typography>
        ) : (
          <Typography variant="h7" color={"#0b626b"}>
            <strong>{params.row.invoice_type_name}</strong>
          </Typography>
        )}
      </Box>
    ),
    width: 120,
  },
  {
    field: "invoice_due_date",
    renderHeader: () => <strong>{"Due date"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {dayjs(params.row.invoice_due_date).format("YYYY-MM-DD")}
        </Typography>
      </Box>
    ),
    width: 100,
  },
  {
    field: "latestday",
    renderHeader: () => <strong>{"Expired day"}</strong>,
    renderCell: (params) => {
      if (parseInt(params.row.latestday) < -1) {
        return (
          <Chip variant="filled" color="regular" label={params.row.latestday} />
        );
      }
      if (
        parseInt(params.row.latestday) >= 0 &&
        parseInt(params.row.latestday) <= 1
      ) {
        return (
          <Chip variant="filled" color="medium" label={params.row.latestday} />
        );
      }
      if (
        parseInt(params.row.latestday) <= 5 &&
        parseInt(params.row.latestday) > 1
      ) {
        return (
          <Chip variant="filled" color="high" label={params.row.latestday} />
        );
      } else
        return (
          <Chip variant="filled" color="urgent" label={params.row.latestday} />
        );
    },
    width: 90,
  },
  {
    field: "est_amount",
    renderHeader: () => <strong>{"Est amount"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.est_amount)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },
  {
    field: "invoice_amount",
    renderHeader: () => <strong>{"Invoice amount"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.invoice_amount)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },
  {
    field: "difference",
    renderHeader: () => <strong>{"Difference"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.difference)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },

  {
    field: "paid_amount",
    renderHeader: () => <strong>{"Paid amount"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.paid_amount)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },
  {
    field: "unpaid_amount",
    renderHeader: () => <strong>{"Unpaid amount"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.unpaid_amount)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },
  // {
  //   field: "invoice_state",
  //   renderHeader: () => <strong>{"Invoice state"}</strong>,
  //   width: 120,
  // },
];
const tradeColumns = [
  {
    field: "trade_id",
    renderHeader: () => <strong>{"Trade id"}</strong>,
    renderCell: (params) => {
      if (params.row.trade_status === "Verified") {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={"T-" + params.row.trade_id}
          />
        );
      } else if (params.row.trade_status === "Canceled") {
        return (
          <Chip
            variant="filled"
            color="urgent"
            label={"T-" + params.row.trade_id}
          />
        );
      } else {
        return (
          <Chip
            variant="filled"
            color="medium"
            label={"T-" + params.row.trade_id}
          />
        );
      }
    },
    width: 140,
  },
  {
    field: "country_party_name",
    renderHeader: () => <strong>{"Counterparty"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.country_party_name}</Typography>
        {params.row.trade_type === "Spot_Purchase" ||
        params.row.trade_type === "Term_Purchase" ? (
          <Typography variant="h7" color={"#0b626b"}>
            <strong>{params.row.trade_type_name}</strong>
          </Typography>
        ) : (
          <Typography variant="h7" color={"#e27671"}>
            <strong>{params.row.trade_type_name}</strong>
          </Typography>
        )}
      </Box>
    ),
    width: 180,
  },
  {
    field: "date_range_from",
    renderHeader: () => <strong>{"Operation Date"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">
          {"From: " + dayjs(params.row.date_range_from).format("YYYY-MM-DD")}
        </Typography>
        <Typography variant="h7">
          {"To: " + dayjs(params.row.date_range_to).format("YYYY-MM-DD")}
        </Typography>
        <Typography variant="h7">
          {"BL: " + dayjs(params.row.trade_date).format("YYYY-MM-DD")}
        </Typography>
      </Box>
    ),
    width: 150,
  },
  {
    field: "product_name",
    renderHeader: () => <strong>{"Product"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.product_name}</Typography>

        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.grade_name} </strong>
        </Typography>
      </Box>
    ),
    width: 190,
    headerAlign: "left",
  },

  {
    field: "quantity",
    renderHeader: () => <strong>{"Quantity (MT)"}</strong>,
    renderCell: (params) => (
      <Box sx={{ display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.quantity)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },
  {
    field: "quantity_cm",
    renderHeader: () => <strong>{"Quantity (CM)"}</strong>,
    renderCell: (params) => (
      <Box sx={{ display: "flex" }}>
        <Typography variant="h7">
          {numberWithCommas(params.row.quantity_cm)}
        </Typography>
      </Box>
    ),
    width: 120,
    align: "right",
  },
  {
    field: "trader_name",
    renderHeader: () => <strong>{"Trader"}</strong>,
    width: 120,
  },

  {
    field: "operator_name",
    renderHeader: () => <strong>{"Operator"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.operator_name}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.operation_flag} </strong>
        </Typography>
      </Box>
    ),
    width: 120,
  },

  {
    field: "finance_name",
    renderHeader: () => <strong>{"Finance"}</strong>,
    renderCell: (params) => (
      <Box sx={{ flexDirection: "column", display: "flex" }}>
        <Typography variant="h7">{params.row.finance_name}</Typography>
        <Typography sx={{ fontSize: "12px" }}>
          <strong> {params.row.finance_flag} </strong>
        </Typography>
      </Box>
    ),
    width: 120,
  },
];
const balanceColumns = [
  {
    field: "types",
    renderHeader: () => <strong>{"Name"}</strong>,
    width: 150,
  },
  {
    field: "plan_purchase",
    renderHeader: () => <strong>{"Plan purchase"}</strong>,
    renderCell: (params) => (
      <Box>
        <Typography variant="h7" color="red">
          {params.row.plan_purchase
            ? "(" +
              numberWithCommas(params.row.plan_purchase) +
              " " +
              params.row.plan_purchase_unit +
              ")"
            : null}
        </Typography>
      </Box>
    ),
    width: 250,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "plan_sales",
    renderHeader: () => <strong>{"Plan sale"}</strong>,
    renderCell: (params) => (
      <Box>
        <Typography variant="h7" color="green">
          {params.row.plan_sales
            ? numberWithCommas(params.row.plan_sales) +
              " " +
              params.row.plan_sales_unit
            : null}
        </Typography>
      </Box>
    ),
    width: 250,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "purchase_mt",
    renderHeader: () => <strong>{"Purchase (MT)"}</strong>,
    renderCell: (params) => (
      <Box>
        <Typography variant="h7" color="red">
          {params.row.purchase_mt
            ? "(" + numberWithCommas(params.row.purchase_mt) + ")"
            : null}
        </Typography>
      </Box>
    ),
    width: 150,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "purchase_cm",
    renderHeader: () => <strong>{"Purchase (CM)"}</strong>,
    renderCell: (params) => (
      <Box>
        <Typography variant="h7" color="red">
          {params.row.purchase_cm
            ? "(" + numberWithCommas(params.row.purchase_cm) + ")"
            : null}
        </Typography>
      </Box>
    ),
    width: 150,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "sales_mt",
    renderHeader: () => <strong>{"Sales (MT)"}</strong>,
    renderCell: (params) => (
      <Box>
        <Typography variant="h7" color="green">
          {params.row.sales_mt ? numberWithCommas(params.row.sales_mt) : null}
        </Typography>
      </Box>
    ),
    width: 150,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "sales_cm",
    renderHeader: () => <strong>{"Sales (CM)"}</strong>,
    renderCell: (params) => (
      <Box>
        <Typography variant="h7" color="green">
          {params.row.sales_cm ? numberWithCommas(params.row.sales_cm) : null}
        </Typography>
      </Box>
    ),
    width: 150,
    align: "right",
    headerAlign: "right",
  },
];
const costColumns = [
  {
    field: "types",
    renderHeader: () => <strong>{"Name"}</strong>,
    width: 200,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "plan_amount",
    renderHeader: () => <strong>{"Plan amount"}</strong>,
    renderCell: (params) => (
      <Box>
        {params.row.plan_amount !== "0" && params.row.plan_amount !== null ? (
          params.row.cost_type === "C" ? (
            <Typography variant="h7" color="red">
              {"(" +
                numberWithCommas(params.row.plan_amount) +
                " " +
                params.row.plan_currency +
                ")"}
            </Typography>
          ) : (
            <Typography variant="h7" color="green">
              {numberWithCommas(params.row.plan_amount) +
                " " +
                params.row.plan_currency}
            </Typography>
          )
        ) : null}
      </Box>
    ),
    width: 250,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "purchase",
    renderHeader: () => <strong>{"Purchase"}</strong>,
    renderCell: (params) => (
      <Box>
        {params.row.purchase !== "0" && params.row.purchase !== null ? (
          params.row.cost_type === "C" ? (
            <Typography variant="h7" color="red">
              {"(" +
                numberWithCommas(params.row.purchase) +
                " " +
                params.row.purchase_currency +
                ")"}
            </Typography>
          ) : (
            <Typography variant="h7" color="green">
              {numberWithCommas(params.row.purchase) +
                " " +
                params.row.purchase_currency}
            </Typography>
          )
        ) : null}
      </Box>
    ),
    width: 250,
    align: "right",
    headerAlign: "right",
  },
  {
    field: "sales",
    renderHeader: () => <strong>{"Sales"}</strong>,
    renderCell: (params) => (
      <Box>
        {params.row.sales !== "0" && params.row.sales !== null ? (
          params.row.cost_type === "C" ? (
            <Typography variant="h7" color="red">
              {"(" +
                numberWithCommas(params.row.sales) +
                " " +
                params.row.sales_currency +
                ")"}
            </Typography>
          ) : (
            <Typography variant="h7" color="green">
              {numberWithCommas(params.row.sales) +
                " " +
                params.row.sales_currency}
            </Typography>
          )
        ) : null}
      </Box>
    ),
    width: 250,
    align: "right",
    headerAlign: "right",
  },
];



export default function DealOverview() {
  const { data: session } = useSession();
  const [filterTextDeal, setFilterTextDeal] = React.useState("");
  const [filterTextParcel, setFilterTextParcel] = React.useState("");
  const [filterTextTrade, setFilterTextTrade] = React.useState("");
  const [filterTextInvoice, setFilterTextInvoice] = React.useState("");
  const [filterTextStorage, setFilterTextStorage] = React.useState("");
  const [filterId, setFilterId] = React.useState({
    id: "",
    name: "",
    type: "",
  });
  const [switchTrade, setSwitchTrade] = React.useState(false);
  const [deal, setDeal] = React.useState("");
  const [tradeIdForSummary, setTradeIdForSummary] = React.useState("");
  const [tradeName, setTradeName] = React.useState("");
  const [openIndex, setOpenIndex] = React.useState(null);
  const [selectedTradeIndex, setSelectedTradeIndex] = React.useState(false);

  const handleClick = (index, item) => {
    handleSwitchesFalse();
    setDeal({
      deal_id: item.deal_id,
      deal_number: item.deal_number,
      title: item.title,
      description: item.description,
      deal_date: item.deal_date,
      method: "PUT",
    });

    setFilterId({
      id: item.deal_id,
      name: item.title,
      type: "D",
    });
    setOpenIndex(openIndex === index ? null : index);
  };
  const handleTradeClick = (index) => {
    setSelectedTradeIndex(index);
    handleSwitchesTrue();
    setTradeIdForSummary(filteredDealRows[openIndex].trades[index].trade_id);
    setFilterId({
      id: filteredDealRows[openIndex].trades[index].trade_id,
      name:
        filteredDealRows[openIndex].trades[index].trade_code +
        " " +
        filteredDealRows[openIndex].trades[index].counterparty +
        " " +
        numberWithCommas(filteredDealRows[openIndex].trades[index].quantity) +
        " " +
        filteredDealRows[openIndex].trades[index].unit +
        " " +
        filteredDealRows[openIndex].trades[index].trade_date,
      type: "T",
    });
  };

  const [clickedList, setClickedList] = React.useState(null);

  const handleFirstListClick = () => {
    setClickedList("first");
  };

  const handleSecondListClick = () => {
    setClickedList("second");
  };

  const handleFilterTextChangeDeal = (event) => {
    setFilterTextDeal(event.target.value);
  };

  const handleFilterTextChangeParcel = (event) => {
    setFilterTextParcel(event.target.value);
  };

  const handleFilterTextChangeTrade = (event) => {
    setFilterTextTrade(event.target.value);
  };

  const handleFilterTextChangeInvoice = (event) => {
    setFilterTextInvoice(event.target.value);
  };

  const handleFilterTextChangeStorage = (event) => {
    setFilterTextStorage(event.target.value);
  };
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleSwitchesTrue = () => setSwitchTrade(true);
  const handleSwitchesFalse = () => setSwitchTrade(false);

  const { data: tradeRows } = useSWR(
    filterId.type && filterId.id
      ? [
          `/api/deal/overview/trades/${filterId.type}/${filterId.id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const { data: unlallocatedTrade } = useSWR(
    [`/api/deal/unlocatedtrade`, session?.user?.token],
    fetcher
  );
  const { data: parcelRows } = useSWR(
    filterId.type && filterId.id
      ? [
          `/api/deal/overview/parcels/${filterId.type}/${filterId.id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const { data: storageRows } = useSWR(
    filterId.type && filterId.id
      ? [
          `/api/deal/overview/storage/${filterId.type}/${filterId.id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const { data: balanceRows } = useSWR(
    filterId.type && filterId.id
      ? [
          `/api/deal/overview/balance/${filterId.type}/${filterId.id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const { data: costRows } = useSWR(
    filterId.type && filterId.id
      ? [
          `/api/deal/overview/cost/${filterId.type}/${filterId.id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const { data: invoiceRows } = useSWR(
    filterId.type && filterId.id
      ? [
          `/api/deal/overview/invoice/${filterId.type}/${filterId.id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const {
    data: deals,
    error,
    isLoading,
  } = useSWR(
    [`/api/deal/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const dealRows = deals?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const balancerowscost = balanceRows?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const costrowsbalance = costRows?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const parcelRowsForParcelTab = parcelRows?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const tradeRowsForParcelTab = tradeRows?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const invoiceRowsForParcelTab = invoiceRows?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const storageRowsForParcelTab = storageRows?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  function calculateColumnSum(rows, columnName) {
    let sum = 0;
    if (Array.isArray(rows)) {
      rows.forEach((row) => {
        const value = parseFloat(row[columnName]);
        if (!isNaN(value)) {
          sum += value;
        }
      });
    }
    return sum;
  }

  const totalIP = calculateColumnSum(
    costRows?.result.filter((f) => f.cost_type === "I"),
    "purchase"
  );
  const totalIS = calculateColumnSum(
    costRows?.result.filter((f) => f.cost_type === "I"),
    "sales"
  );
  const totalCP = calculateColumnSum(
    costRows?.result.filter((f) => f.cost_type === "C"),
    "purchase"
  );
  const totalCS = calculateColumnSum(
    costRows?.result.filter((f) => f.cost_type === "C"),
    "sales"
  );
  const totalplanP = calculateColumnSum(
    costRows?.result.filter((f) => f.cost_type === "I"),
    "plan_amount"
  );
  const totalplanCP = calculateColumnSum(
    costRows?.result.filter((f) => f.cost_type === "C"),
    "plan_amount"
  );

  const totalPurchaseMtQty = calculateColumnSum(
    balanceRows?.result.filter((f) => f.types === "Quantity"),
    "purchase_mt"
  );

  const totalSalesMtQty = calculateColumnSum(
    balanceRows?.result.filter((f) => f.types === "Quantity"),
    "sales_mt"
  );

  const totalPurchaseCmQty = calculateColumnSum(
    balanceRows?.result.filter((f) => f.types === "Quantity"),
    "purchase_cm"
  );

  const totalSalesCmQty = calculateColumnSum(
    balanceRows?.result.filter((f) => f.types === "Quantity"),
    "sales_cm"
  );

  const balanceplan = totalplanP - totalplanCP;
  const balanceMtQty = totalPurchaseMtQty - totalSalesMtQty;
  const balanceCmQty = totalPurchaseCmQty - totalSalesCmQty;
  const balance = totalIP + totalIS - (totalCP + totalCS);
  const difference =
    balanceplan <= 0 && balance <= 0
      ? balanceplan + balance
      : balanceplan <= 0 && balance >= 0
      ? balance + balanceplan
      : balanceplan >= 0 && balance <= 0
      ? balance + balanceplan
      : balanceplan >= 0 && balance >= 0
      ? balanceplan - balance
      : null;
  const filterRows = (rows, filterText) =>
    rows?.filter((row) => {
      const mainItemMatches = Object.values(row).some((value) => {
        if (typeof value === "number" || typeof value === "string") {
          const strValue = String(value).toLowerCase();
          return strValue.includes(String(filterText).toLowerCase());
        }
        return false;
      });

      const nestedItemMatches = row.trades?.some((deal) =>
        Object.values(deal).some((value) => {
          if (typeof value === "number" || typeof value === "string") {
            const strValue = String(value).toLowerCase();
            return strValue.includes(String(filterText).toLowerCase());
          }
          return false;
        })
      );

      return mainItemMatches || nestedItemMatches;
    });

  const filteredDealRows = filterRows(dealRows, filterTextDeal);
  const filteredParcelRows = filterRows(
    parcelRowsForParcelTab,
    filterTextParcel
  );
  const filteredTradeRows = filterRows(tradeRowsForParcelTab, filterTextTrade);
  const filteredInvoiceRows = filterRows(
    invoiceRowsForParcelTab,
    filterTextInvoice
  );
  const filteredStorageRows = filterRows(
    storageRowsForParcelTab,
    filterTextStorage
  );

  const rows = unlallocatedTrade?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const totalplan = calculateColumnSum(costrowsbalance, "plan_amount");
  const totalpurchase = calculateColumnSum(costrowsbalance, "purchase");
  const totalsales = calculateColumnSum(costrowsbalance, "sales");

  const tradetotal_mt = filteredTradeRows
    ? calculateColumnSum(filteredTradeRows, "quantity")
    : 0;

  const tradetotal_cm = filteredTradeRows
    ? calculateColumnSum(filteredTradeRows, "quantity_cm")
    : 0;
  const parceltotal_mt = filteredParcelRows
    ? calculateColumnSum(filteredParcelRows, "shipping_quantity_mt")
    : 0;

  const parceltotal_cm = filteredParcelRows
    ? calculateColumnSum(filteredParcelRows, "shipping_quantity_cm")
    : 0;
  const storagetotal = filteredStorageRows
    ? calculateColumnSum(filteredStorageRows, "quantity")
    : 0;
  const invoicetotal = filteredInvoiceRows
    ? calculateColumnSum(filteredInvoiceRows, "invoice_amount")
    : 0;
  const unpaidtotal = filteredInvoiceRows
    ? calculateColumnSum(filteredInvoiceRows, "unpaid_amount")
    : 0;
  return (
    <>
      <ToastContainer />
      <Grid container spacing={2}>
        <Grid item xs={2.5}>
          {/* <FormControl sx={{ minWidth: 250, mb: 1 }}> */}
          <FormControl fullWidth sx={{ p: 1 }}>
            <TextField
              variant="outlined"
              size="small"
              placeholder="Search..."
              value={filterTextDeal}
              onChange={handleFilterTextChangeDeal}
            />
          </FormControl>
          <Stack direction="row" spacing={1} marginTop={1}>
            <List
              sx={{
                maxHeight: "70vh",
                width: "100%",
                maxWidth: 360,
                bgcolor: "background.paper",
                overflow: "auto",
              }}
              component="nav"
              aria-labelledby="nested-list-subheader"
              subheader={null}
            >
              {filteredDealRows?.map((item, i) => (
                <div key={i}>
                  <ListItemButton
                    onClick={() => {
                      handleFirstListClick();
                      handleClick(i, item);
                    }}
                    sx={{
                      backgroundColor: openIndex === i ? "#EEEEEE" : "inherit",
                    }}
                  >
                    <ListItemIcon
                      sx={{ color: openIndex === i ? "orange" : "#0B626B" }}
                    >
                      <HandshakeIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary={"D-" + item.deal_id + " " + item.title}
                      sx={{ color: "black", textAlign: "left" }}
                    />
                    {item.trades &&
                      item.trades.length > 0 &&
                      (openIndex === i ? <ExpandLess /> : <ExpandMore />)}
                  </ListItemButton>
                  <Collapse in={openIndex === i} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      {item.trades?.map((deal, j) => (
                        <ListItemButton
                          key={j}
                          onClick={() => {
                            handleSecondListClick();
                            handleTradeClick(j);
                          }}
                          sx={{
                            pl: 4,
                            backgroundColor:
                              selectedTradeIndex === j ? "#EEEEEE" : "inherit",
                          }}
                        >
                          <ListItemIcon sx={{ color: "#0B626B" }}>
                            {selectedTradeIndex === j ? (
                              <InsertDriveFileIcon sx={{ color: "orange" }} />
                            ) : (
                              <InsertDriveFileOutlinedIcon />
                            )}
                          </ListItemIcon>
                          <ListItemText
                            primary={
                              "T-" +
                              deal.trade_id +
                              " " +
                              deal.trade_code +
                              " " +
                              deal.counterparty +
                              " " +
                              numberWithCommas(deal.quantity) +
                              " " +
                              deal.unit +
                              " " +
                              deal.trade_date
                            }
                            sx={{ color: "black", textAlign: "left" }}
                          />
                        </ListItemButton>
                      ))}
                    </List>
                  </Collapse>
                </div>
              ))}
            </List>
          </Stack>
        </Grid>
        <Grid item xs={9.5}>
          <Box sx={{ width: "100%" }}>
            <Box sx={{ bgcolor: "#fff" }}>
              <AntTabs
                value={value}
                onChange={handleChange}
                aria-label="ant example"
              >
                <AntTab label="Balance" {...a11yProps(0)} />
                <AntTab label="Trade" {...a11yProps(1)} />
                <AntTab label="Parcel" {...a11yProps(2)} />
                <AntTab label="Storage" {...a11yProps(3)} />
                <AntTab label="Invoice" {...a11yProps(4)} />
              </AntTabs>
            </Box>
            <CustomTabPanel value={value} index={0}>
              <Stack
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography>
                  Name
                  <strong>
                    {filterId
                      ? " " +
                        filterId.type +
                        "-" +
                        filterId.id +
                        " " +
                        filterId.name
                      : null}
                  </strong>
                </Typography>
                <Box sx={{ display: "flex", mt: 2 }}>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Quantity balance(MT):</strong>
                  </Typography>
                  <strong>
                    {balanceMtQty < 0 ? (
                      <Typography color={"red"}>
                        {balanceMtQty ? numberWithCommas(balanceMtQty) : 0}
                      </Typography>
                    ) : (
                      <Typography color={"green"}>
                        {balanceMtQty ? numberWithCommas(balanceMtQty) : 0}
                      </Typography>
                    )}
                  </strong>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Quantity balance(CM):</strong>
                  </Typography>
                  <strong>
                    {balanceCmQty < 0 ? (
                      <Typography color={"red"}>
                        {balanceCmQty ? numberWithCommas(balanceCmQty) : 0}
                      </Typography>
                    ) : (
                      <Typography color={"green"}>
                        {balanceCmQty ? numberWithCommas(balanceCmQty) : 0}
                      </Typography>
                    )}
                  </strong>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Plan balance:</strong>
                  </Typography>
                  <strong>
                    {balanceplan < 0 ? (
                      <Typography color={"red"}>
                        {balanceplan ? numberWithCommas(balanceplan) : 0}
                        {costRows?.result[1]?.plan_currency}
                      </Typography>
                    ) : (
                      <Typography color={"green"}>
                        {balanceplan ? numberWithCommas(balanceplan) : 0}
                        {costRows?.result[1]?.plan_currency}
                      </Typography>
                    )}
                  </strong>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Actual balance:</strong>
                  </Typography>
                  <strong>
                    {balance < 0 ? (
                      <Typography color={"red"}>
                        {balance ? numberWithCommas(balance) : 0}
                      </Typography>
                    ) : (
                      <Typography color={"green"}>
                        {balance ? numberWithCommas(balance) : 0}
                      </Typography>
                    )}
                  </strong>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Difference:</strong>
                  </Typography>
                  <strong>
                    {difference < 0 ? (
                      <Typography color={"red"}>
                        {difference ? numberWithCommas(difference) : 0}
                        {costRows?.result[1]?.plan_currency}
                      </Typography>
                    ) : (
                      <Typography color={"green"}>
                        {difference ? numberWithCommas(difference) : 0}
                        {costRows?.result[1]?.plan_currency}
                      </Typography>
                    )}
                  </strong>
                </Box>
              </Stack>
              <DataGrid
                rows={balancerowscost ? balancerowscost : []}
                columns={balanceColumns}
                style={{ background: "white" }}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: "80vw",
                  maxHeight: "22vh",
                  minHeight: "22vh",
                  overflow: "auto",
                }}
                rowHeight={25}
                hideFooter
              />
              <Stack
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
                sx={{ mt: 2 }}
              >
                <Typography>
                  <strong>Costs</strong>
                </Typography>
                <Box sx={{ display: "flex", mt: 2 }}>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Total plan amount:</strong>
                  </Typography>
                  <Typography color={"black"}>
                    {totalplan ? numberWithCommas(totalplan) : 0}
                  </Typography>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Total purchase:</strong>
                  </Typography>{" "}
                  <Typography color={"black"}>
                    {totalpurchase ? numberWithCommas(totalpurchase) : 0}
                  </Typography>
                  <Typography sx={{ ml: 5 }}>
                    <strong>Total sales:</strong>
                  </Typography>
                  <Typography color={"black"}>
                    {totalsales ? numberWithCommas(totalsales) : 0}
                  </Typography>
                </Box>
              </Stack>
              <DataGrid
                rows={costrowsbalance ? costrowsbalance : []}
                columns={costColumns}
                style={{ background: "white" }}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: "80vw",
                  maxHeight: "34.5vh",
                  minHeight: "34.5vh",
                  overflow: "auto",
                }}
                rowHeight={25}
                hideFooter
              />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={1}>
              {clickedList === "first" && (
                <Stack
                  container
                  direction="row"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <FormControl sx={{ minWidth: 250, mb: 1 }}>
                    <TextField
                      variant="outlined"
                      size="small"
                      placeholder="Search..."
                      value={filterTextTrade}
                      onChange={handleFilterTextChangeTrade}
                    />
                  </FormControl>
                  <Box>
                    <Stack direction="row" alignItems="center" spacing={1}>
                      <Typography>
                        Total (MT):{" "}
                        <strong>
                          {" "}
                          {tradetotal_mt
                            ? numberWithCommas(tradetotal_mt)
                            : 0}{" "}
                        </strong>
                      </Typography>
                      <Typography>
                        Total (CM):
                        <strong>
                          {" "}
                          {tradetotal_cm ? numberWithCommas(tradetotal_cm) : 0}
                        </strong>
                      </Typography>
                    </Stack>
                  </Box>
                </Stack>
              )}
              {switchTrade === false ? (
                <DataGrid
                  rows={filteredTradeRows ? filteredTradeRows : []}
                  columns={tradeColumns}
                  style={{ background: "white" }}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                    maxWidth: "90vw",
                    maxHeight: "64.4vh",
                    minHeight: "64.4vh",
                    overflow: "auto",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                />
              ) : (
                <Stack direction={"row"} spacing={20}>
                  <Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Trade id</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].trade_id
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>

                    <Box sx={{ mb: 1 }}>
                      <Typography>Company</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].company_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>

                    <Box sx={{ mb: 1 }}>
                      <Typography>Counterparty</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].country_party_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Trade type</Typography>

                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].trade_type
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>

                    <Box sx={{ mb: 1 }}>
                      <Typography>Product name</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].product_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Grade name</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].grade_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Quantity</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? numberWithCommas(
                                tradeRowsForParcelTab[0].quantity
                              )
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Units</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].units
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                  </Box>
                  <Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Date range from</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].date_range_from !== null
                              ? dayjs(
                                  tradeRowsForParcelTab[0].date_range_from
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Date range to</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].date_range_to !== null
                              ? dayjs(
                                  tradeRowsForParcelTab[0].date_range_to
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Trader</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].trader_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Trade status</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].trade_status
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Operator name</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].operator_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Operation</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].operation_flag
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Finance name</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].finance_name
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>

                    <Box sx={{ mb: 1 }}>
                      <Typography>Finance</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].finance_flag
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                  </Box>
                  <Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Trade date</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].trade_date !== null
                              ? dayjs(
                                  tradeRowsForParcelTab[0].trade_date
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Contract received date</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0]
                                .contract_received_date !== null
                              ? dayjs(
                                  tradeRowsForParcelTab[0]
                                    .contract_received_date
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Response sent date</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].response_sent_date !==
                              null
                              ? dayjs(
                                  tradeRowsForParcelTab[0].response_sent_date
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Contract drafted date</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].contract_drafted_date !==
                              null
                              ? dayjs(
                                  tradeRowsForParcelTab[0].contract_drafted_date
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Contract send date</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0].contact_sent_date !==
                              null
                              ? dayjs(
                                  tradeRowsForParcelTab[0].contact_sent_date
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography>Broker contract received date</Typography>
                      <Typography variant="subtitle1">
                        <strong>
                          {tradeRowsForParcelTab
                            ? tradeRowsForParcelTab[0]
                                .broker_contract_received_date !== null
                              ? dayjs(
                                  tradeRowsForParcelTab[0]
                                    .broker_contract_received_date
                                ).format("YYYY-MM-DD")
                              : "*****"
                            : "*****"}
                        </strong>
                      </Typography>
                    </Box>
                  </Box>
                </Stack>
              )}
            </CustomTabPanel>
            <CustomTabPanel value={value} index={2}>
              <Stack
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <FormControl sx={{ minWidth: 250, mb: 1 }}>
                  <TextField
                    variant="outlined"
                    size="small"
                    placeholder="Search..."
                    value={filterTextParcel}
                    onChange={handleFilterTextChangeParcel}
                  />
                </FormControl>
                <Box>
                  <Stack direction="row" alignItems="center" spacing={1}>
                    <Typography>
                      Total(MT):
                      <strong>
                        {parceltotal_mt ? numberWithCommas(parceltotal_mt) : 0}
                      </strong>
                    </Typography>
                    <Typography>
                      Total(CM):
                      <strong>
                        {parceltotal_cm ? numberWithCommas(parceltotal_cm) : 0}
                      </strong>
                    </Typography>
                  </Stack>
                </Box>
              </Stack>
              <DataGrid
                rows={filteredParcelRows ? filteredParcelRows : []}
                columns={parcelcolumns}
                style={{ background: "white" }}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: "80vw",
                  maxHeight: "60vh",
                  minHeight: "60vh",
                  overflow: "auto",
                }}
                getRowHeight={() => "auto"}
                pageSizeOptions={[50]}
              />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={3}>
              <Stack
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <FormControl sx={{ minWidth: 250, mb: 1 }}>
                  <TextField
                    variant="outlined"
                    size="small"
                    placeholder="Search..."
                    value={filterTextStorage}
                    onChange={handleFilterTextChangeStorage}
                  />
                </FormControl>
                <Box>
                  <Typography>
                    <strong>Total:</strong>
                    {storagetotal ? numberWithCommas(storagetotal) : 0}
                  </Typography>
                </Box>
              </Stack>

              <DataGrid
                rows={filteredStorageRows ? filteredStorageRows : []}
                columns={storageColumns}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                style={{ background: "white" }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: "80vw",
                  maxHeight: "60vh",
                  minHeight: "60vh",
                  overflow: "auto",
                }}
                getRowHeight={() => "auto"}
                pageSizeOptions={[50]}
              />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={4}>
              <Stack
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <FormControl sx={{ minWidth: 250, mb: 1 }}>
                  <TextField
                    variant="outlined"
                    size="small"
                    placeholder="Search..."
                    value={filterTextInvoice}
                    onChange={handleFilterTextChangeInvoice}
                  />
                </FormControl>
                <Stack spacing={2} direction={"row"}>
                  <Typography>
                    <strong>Invoice total:</strong>
                    {invoicetotal ? numberWithCommas(invoicetotal) : 0}
                  </Typography>
                  <Typography>
                    <strong>Unpaid total:</strong>
                    {unpaidtotal ? numberWithCommas(unpaidtotal) : 0}
                  </Typography>
                </Stack>
              </Stack>

              <DataGrid
                rows={filteredInvoiceRows ? filteredInvoiceRows : []}
                columns={invoiceColumns}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                style={{ background: "white" }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: "80vw",
                  maxHeight: "60vh",
                  minHeight: "60vh",
                  overflow: "auto",
                }}
                getRowHeight={() => "auto"}
                pageSizeOptions={[50]}
              />
            </CustomTabPanel>
          </Box>
        </Grid>
      </Grid>
    </>
  );
}
