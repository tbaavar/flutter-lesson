"use client";
import React from "react";
import "react-toastify/dist/ReactToastify.css";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import { styled } from "@mui/material/styles";
import CircularProgress from "@mui/material/CircularProgress";
import MyAlert from "@/components/Myalert";
import CustomNoRowsOverlay from "@/components/NoRecord";
import Divider from "@mui/material/Divider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import Modal from "@mui/material/Modal";
import dayjs from "dayjs";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import { fetchData } from "@/lib/fetch";
import { toast, ToastContainer } from "react-toastify";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import {
  Box,
  Button,
  Stack,
  Alert,
  AlertTitle,
  TextField,
  FormControl,
  Grid,
  Typography,
  InputLabel,
  Tabs,
  IconButton,
  Tab,
  MenuItem,
  Tooltip,
  Link,
  Select,
  FormHelperText,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import PropTypes from "prop-types";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import CloseIcon from "@mui/icons-material/Close";
import {
  DataGrid,
  GridActionsCellItem,
  GridToolbarContainer,
} from "@mui/x-data-grid";
import InsertLinkIcon from "@mui/icons-material/InsertLink";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import * as yup from "yup";
import useSWR from "swr";
import Dealcon from "./dealcon.jsx/page";
import { useFormik } from "formik";
import Deal from "./deal/page";
import DealOverview from "./dealoverview/page";
import EditIcon from "@mui/icons-material/Edit";
import LinkOffIcon from "@mui/icons-material/LinkOff";
import { GridRowModes, GridRowEditStopReasons } from "@mui/x-data-grid";
import CheckIcon from "@mui/icons-material/Check";
import { AntTabs, AntTab, CustomTabPanel, a11yProps } from "@/components/CustomTabControl";
/////////////////////////////////////////////////////////////////////////////////////////////

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};
const style2 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};
const style1 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 2,
};

/////////////////////////////////////////////////////////////////////////////////////////////
export default function DealPage() {
  const { data: session } = useSession();
  const [openDealcon, setOpenDealcon] = React.useState(false);
  const [filterText, setFilterText] = React.useState("");
  const [dealcon, setDealcon] = React.useState("");
  const [openDeal, setOpenDeal] = React.useState(false);
  const [deal, setDeal] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(0);
  const [value1, setValue1] = React.useState(0);
  const [invoiceNumber, setInvoiceNumber] = React.useState("");
  const [selectedTradeId, setSelectedTradeId] = React.useState("");
  const [tradeIdForDeal, setTradeIdForDeal] = React.useState("All");
  const [delDealplan, setDelDealplan] = React.useState("");
  const [deltradeplan, setDeltradeplan] = React.useState("");
  const [dealcell, setDealcell] = React.useState("");
  const [tradecell, setTradecell] = React.useState(null);
  const [tradeidget, setTradeidget] = React.useState(null);
  const [opendealplan, setOpenDealplan] = React.useState("");
  const [isButtonDisabled, setIsButtonDisabled] = React.useState(true);
  const [isButtonDisabled1, setIsButtonDisabled1] = React.useState(true);
  const [opentradeplan, setOpenTradeplan] = React.useState("");
  const [filterText1, setFilterText1] = React.useState("");
  const [tank, setTank] = React.useState([]);
  const [filterText2, setFilterText2] = React.useState("");
  const [open1, setOpen1] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [deleteItem1, setDeleteItem1] = React.useState();
  const [filterText3, setFilterText3] = React.useState("");
  const [dataCost, setDataCost] = React.useState("");
  const [dealtotle, setDealtotle] = React.useState(true);
  ////////////////////////////////////////////////////////////////////////////////////////////
  const handleClose1 = () => {
    setOpen1(false);
  };
  const handleClose2 = () => {
    setOpen2(false);
  };
  const handleCloseDealplan = () => {
    setOpenDealplan(!opendealplan);
  };
  const handleCloseTradeplan = () => {
    setOpenTradeplan(!opentradeplan);
  };
  const handleDeletetrade = () => {
    setOpen2(!open2);
  };
  const handleFilterChange1 = (e) => {
    setFilterText1(e.target.value);
  };
  const handleFilterChange2 = (e) => {
    setFilterText2(e.target.value);
  };
  const handleFilterChange3 = (e) => {
    setFilterText3(e.target.value);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChange1 = (event, newValue) => {
    setValue1(newValue);
  };
  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };
  ////////////////////////////////////////////////////////////////////////////////////
  const [rows, setRows] = React.useState("");
  const [rowModesModel, setRowModesModel] = React.useState({});
  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };
  const handleSaveClick = (params) => () => {
    setRowModesModel({
      ...rowModesModel,
      [params.id]: { mode: GridRowModes.View },
    });
  };
  const saveCostData = async (dd) => {
    return saveCost(dd.row);
  };
  const handleDeleteClick = (params) => () => {
    setDeleteItem1(params);
    setOpen1(true);
  };
  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };
  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };
  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };
  ///////////////////////////////////////////////////////////////////////////////////////////
  const handleDealplanOpen = () => setOpenDealplan(true);
  const handleTradeplanOpen = () => setOpenTradeplan(true);
  const handleDealOpen = () => setOpenDeal(true);
  const handleDealClose = () => setOpenDeal(false);
  const handleDealconOpen = () => setOpenDealcon(true);
  const handleDealconClose = () => setOpenDealcon(false);
  const handleCellClick = (params, event) => {
    setSelectedTradeId(params.row.deal_id);
  };
  const handleTradeIdCellClick = (params, event) => {
    setTradeIdForDeal(params.row.trade_id);
  };
  const handledealplan = (params, event) => {
    setDealcell(params.row.deal_plan_id);
    setDealtotle(params.row.title);
    setIsButtonDisabled(false);
    setIsButtonDisabled1(false);
    const currentRow = params.row;
    setTradecell(currentRow.deal_plan_id);

    fetch(`api/deal/trade/plan/cost/${currentRow.deal_plan_id}`, {
      headers: {
        Authorization: `Bearer ${session?.user?.token}`,
      },
    })
      .then((res) => res.json())
      .then((d) =>
        setRows(
          d?.result?.map((row, i) => ({
            id: i + 1,
            plan_cost_id: row.plan_cost_id,
            cost_code: row.cost_code,
            amount: row.amount,
            type: "EDIT",
          }))
        )
      )
      .catch((err) => console.log(err));
  };
  ///////////////////////////////////////////////////////////////////////////////////////////
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );
  const { data: unlallocatedTrade } = useSWR(
    selectedTradeId
      ? [`/api/deal/${selectedTradeId}`, session?.user?.token]
      : null,
    fetcher,
    {
      refreshInterval: 1000,
    }
  );
  const { data: trade } = useSWR(
    [`/api/deal/filter/${session?.user.user_id}`, session?.user?.token],
    fetcher,
    {
      refreshInterval: 1000,
    }
  );
  // const { data: status } = useSWR(
  //   [`/api/lookup/Deal_status`, session?.user?.token],
  //   fetcher,
  //   {
  //     refreshInterval: 1000,
  //   }
  // );
  const { data: deals } = useSWR(
    [
      `/api/deal/list/${session?.user.user_id}/${tradeIdForDeal}`,
      session?.user?.token,
    ],
    fetcher,
    {
      refreshInterval: 1000,
    }
  );
  const { data: plandealapi } = useSWR(
    [`/api/deal/plan`, session?.user?.token],
    fetcher,
    {
      refreshInterval: 1000,
    }
  );
  const { data: plantrade } = useSWR(
    dealcell
      ? [`/api/deal/trade/plan/${dealcell}`, session?.user?.token]
      : null,
    fetcher,
    {
      refreshInterval: 1000,
    }
  );
  const { data: costcode } = useSWR(
    [`/api/lookup/cost`, session?.user?.token],
    fetcher
  );
  // const { data: currency } = useSWR(
  //   [`/api/lookup/Currency`, session?.user?.token],
  //   fetcher
  // );
  const { data: product_id } = useSWR(
    [`/api/lookup/product_grade`, session?.user?.token],
    fetcher
  );
  // const { data: units } = useSWR(
  //   [`/api/lookup/Unit`, session?.user?.token],
  //   fetcher
  // );
  // const { data: trade_type } = useSWR(
  //   [`/api/lookup/Trade_type`, session?.user?.token],
  //   fetcher
  // );
  const {
    data: undealtrade,
    error,
    isLoading,
  } = useSWR(
    [`/api/deal/unlocatedtrade/${session?.user.user_id}`, session?.user?.token],
    fetcher,
    {
      refreshInterval: 1000,
    }
  );
  ///////////////////////////////////////////////////////////////////////////////////////////
  const formik = useFormik({
    initialValues: {
      method: "",
      deal_number: "",
      title: "",
      deal_date: "",
      org_id: "",
      description: "",
      deal_status: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
      title: yup.string().required("Please insert title!"),
      deal_date: yup.string().required("Please insert date!"),
      deal_status: yup.string().required("Please select status!"),
    }),
    onSubmit: async (values) => {
      try {
        saveTerminal(values);
      } catch (error) {
        console.error("Error saving Deal:", error);
      }
    },
    enableReinitialize: true,
  });
  const formiktrade = useFormik({
    initialValues: {
      method: "",
      trade_type: "",
      trade_begin_date: "",
      trade_end_date: "",
      product_id: "",
      counterparty_id: "",
      grade_id: "",
      units: "",
      quantity: "",
      price: "",
      amount: "",
      currency: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
      trade_type: yup.string().required("Please select select!"),
      trade_begin_date: yup.string().required("Please insert date!"),
      trade_end_date: yup.string().required("Please insert date!"),
      product_id: yup.string().required("Please select product!"),
      grade_id: yup.string().required("Please select grade!"),
      units: yup.string().required("Please select unit!"),
      quantity: yup.string().required("Please insert quantity!"),
      price: yup.string().required("Please insert price!"),
      currency: yup.string().required("Please select currency!"),
    }),
    onSubmit: async (values) => {
      try {
        saveTrade(values);
      } catch (error) {
        console.error("Error saving trade:", error);
      }
    },
    enableReinitialize: true,
  });
  const { data: counterparty_id } = useSWR(
    formiktrade.values.trade_type
      ? [
          `/api/lookup/counterparty/${formiktrade.values.trade_type}/${formik.values.org_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const saveTrade = async (data) => {
    const body = {
      plan_trade_id: data.method == "POST" ? undefined : data.plan_trade_id,
      deal_plan_id: data.deal_plan_id,
      trade_type: data.trade_type,
      trade_begin_date: data.trade_begin_date,
      trade_end_date: data.trade_end_date,
      counterparty_id: data.counterparty_id,
      product_id: data.product_id,
      grade_id: data.grade_id,
      units: data.units,
      quantity: data.quantity,
      price: data.price,
      amount: data.amount,
      currency: data.currency,
      username: session?.user?.username,
    };

    try {
      setLoading(true);

      const res = await fetchData(
        `/api/deal/trade/plan`,
        data.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = "Success";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        throw new Error("Failed to save deal");
      }
    } catch (error) {
      toast.error(error.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      handleCloseTradeplan();
    }
  };
  const saveTerminal = async (data) => {
    const body = {
      deal_plan_id: data.method == "POST" ? undefined : data.deal_plan_id,
      deal_number: data.deal_number,
      title: data.title,
      description: data.description,
      deal_date: data.deal_date,
      org_id: data.org_id,
      deal_status: data.deal_status,
      username: session?.user?.username,
    };

    try {
      setLoading(true);

      const res = await fetchData(
        `/api/deal/plan`,
        data.method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = "Success";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        throw new Error("Failed to save deal");
      }
    } catch (error) {
      toast.error(error.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);

      handleCloseDealplan();
    }
  };
  const saveCost = async (data) => {
    const body = {
      plan_cost_id: data.row.plan_cost_id,
      deal_plan_id: tradecell,
      cost_code: data.row.cost_code,
      amount: data.row.amount,
      username: session?.user?.username,
    };
    const method = data.row.plan_cost_id === "" ? "POST" : "PUT";
    try {
      setLoading(true);
      const res = await fetchData(
        `/api/deal/trade/plan/cost`,
        method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = "Success";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        throw new Error("Failed to save deal");
      }
    } catch (error) {
      toast.error(error.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
    }
  };
  const deleteData = async (id) => {
    const body = {
      username: session?.user?.username,
      plan_cost_id: deleteItem1.row.plan_cost_id,
    };

    try {
      setLoading(true);
      const res = await fetchData(
        `/api/deal/trade/plan/cost`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        let message = null;
        message = "Амжилттай устгалаа";
        setRows(rows.filter((row) => row.id !== deleteItem1.row.id));
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;
    const handleClick = () => {
      let id = null;
      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            type: "ADD",
            id: id,
            plan_cost_id: "",
            isNew: true,
            cost_code: "",
            amount: 0,
          },
        ]);
      } else {
        id = rows?.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            type: "ADD",
            id: id,
            plan_cost_id: "",
            isNew: true,
            cost_code: "",
            amount: 0,
          },
        ]);
      }

      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    return (
      <GridToolbarContainer
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        <Box display="flex">
          <Typography sx={{ mr: 6 }}>
            Balance:<strong>{dealtotle ? dealtotle : null}</strong>
          </Typography>
          <Typography>
            Total: <strong style={{ color: "green" }}>{numberWithCommas(total)}</strong>
          </Typography>
        </Box>
        <Button
          color="primary"
          disabled={isButtonDisabled1}
          startIcon={<AddIcon />}
          onClick={handleClick}
        >
          Add Plan Cost
        </Button>
      </GridToolbarContainer>
    );
  }
  const deleteTrade = async (params) => {
    const body = {
      plan_trade_id: deltradeplan,
      username: session?.user?.username,
    };
    try {
      setOpen2(true);
      setLoading(true);
      const res = await fetchData(
        `/api/deal/trade/plan`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        mutate();
        let message = null;
        message = "Амжилттай устгалаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen2(false);
      handleDeletetrade();
    }
  };
  const deleteTerminal = async (params) => {
    const body = {
      deal_plan_id: delDealplan,
      username: session?.user?.username,
    };
    try {
      setOpenDealplan(true);
      setLoading(true);
      const res = await fetchData(
        `/api/deal/plan`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        mutate();
        let message = null;
        message = "Амжилттай устгалаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpenDealplan(false);
      handleCloseDealplan();
    }
  };
  function numberWithCommas(x) {
    if (!x) return "";
    x = parseFloat(x).toFixed(3);
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x)) x = x.replace(pattern, "$1,$2");
    return x;
  }
  const dealcolumn = [
    {
      field: "deal_plan_id",
      renderHeader: () => <strong>{"Deal id"}</strong>,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          formik.setValues({
            deal_plan_id: currentRow.deal_plan_id,
            deal_number: currentRow.deal_number,
            title: currentRow.title,
            description: currentRow.description,
            org_id: currentRow.org_id,
            deal_date: currentRow.deal_date,
            deal_status: currentRow.deal_status,
            method: "PUT",
          });
          fetch(`api/deal/trade/plan/cost/${currentRow.plan_trade_id}`, {
            headers: {
              Authorization: `Bearer ${session?.user?.token}`,
            },
          })
            .then((res) => res.json())
            .then((d) =>
              setRows(
                d?.result?.map((row, i) => ({
                  id: i + 1,
                  plan_cost_id: row.plan_cost_id,
                  cost_code: row.cost_code,
                  amount: row.amount,
                  type: "EDIT",
                }))
              )
            )
            .catch((err) => console.log(err));
          handleDealplanOpen();
        };
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Link onClick={handleEdit} variant="h7">
              {"D-" + params.row.deal_plan_id}
            </Link>
          </Box>
        );
      },
      width: 110,
    },
    {
      field: "deal_number",
      renderHeader: () => <strong>{"Deal number"}</strong>,
      width: 150,
    },
    {
      field: "title",
      renderHeader: () => <strong>{"Title"}</strong>,
      width: 100,
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 200,
    },
    {
      field: "deal_date",
      renderHeader: () => <strong>{"Deal date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.deal_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "deal_status",
      renderHeader: () => <strong>{"Deal status"}</strong>,
      width: 100,
    },
    {
      field: "####",
      renderHeader: () => <strong>{""}</strong>,
      renderCell: (params) => {
        const handlePreview = () => {
          const currentRow = params.row;
          formik.setValues({
            deal_plan_id: currentRow.deal_plan_id,
            deal_number: currentRow.deal_number,
            title: currentRow.title,
            description: currentRow.description,
            deal_date: currentRow.deal_date,
            org_id: currentRow.org_id,
            deal_status: currentRow.deal_status,
            method: "DELETE",
          });
          setDelDealplan(currentRow.deal_plan_id);
          handleDealplanOpen(currentRow);
        };

        return (
          <Tooltip title="Delete">
            <GridActionsCellItem
              icon={<DeleteIcon />}
              label="Delete"
              color="error"
              onClick={handlePreview}
            />
          </Tooltip>
        );
      },
      width: 50,
    },
  ];
  const dealtradecolumn = [
    {
      field: "plan_trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          formiktrade.setValues({
            plan_trade_id: currentRow.plan_trade_id,
            trade_type: currentRow.trade_type,
            trade_begin_date: currentRow.trade_begin_date,
            trade_end_date: currentRow.trade_end_date,
            product_id: currentRow.product_id,
            counterparty_id: currentRow.counterparty_id,
            grade_id: currentRow.grade_id,
            units: currentRow.units,
            quantity: currentRow.quantity,
            price: currentRow.price,
            amount: currentRow.amount,
            currency: currentRow.currency,
            method: "PUT",
          });

          handleTradeplanOpen();
        };
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Link onClick={handleEdit} variant="h7">
              {"T-" + params.row.plan_trade_id}
            </Link>
          </Box>
        );
      },
      width: 110,
    },
    {
      field: "trade_type",
      renderHeader: () => <strong>{"Type"}</strong>,
      width: 130,
    },
    {
      field: "trade_begin_date",
      renderHeader: () => <strong>{"Begin date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.trade_begin_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "trade_end_date",
      renderHeader: () => <strong>{"End date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.trade_end_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>
        </Box>
      ),
      width: 90,
    },
    {
      field: "grade_name",
      renderHeader: () => <strong>{"Grade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.grade_name}</Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      width: 150,
    },
    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.quantity)}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "units",
      renderHeader: () => <strong>{"Unit"}</strong>,
      width: 60,
    },
    {
      field: "price",
      renderHeader: () => <strong>{"Price"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.price)}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "amount",
      renderHeader: () => <strong>{"Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.amount)}
          </Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "currency",
      renderHeader: () => <strong>{"Currency"}</strong>,
      width: 100,
    },
    {
      field: "####",
      renderHeader: () => <strong>{""}</strong>,
      renderCell: (params) => {
        const handlePreview = () => {
          const currentRow = params.row;
          formiktrade.setValues({
            deal_plan_id: currentRow.deal_plan_id,
            plan_trade_id: currentRow.plan_trade_id,
            trade_type: currentRow.trade_type,
            trade_begin_date: currentRow.trade_begin_date,
            trade_end_date: currentRow.trade_end_date,
            product_id: currentRow.product_id,
            counterparty_id: currentRow.counterparty_id,
            grade_id: currentRow.grade_id,
            units: currentRow.units,
            quantity: currentRow.quantity,
            price: currentRow.price,
            amount: currentRow.amount,
            currency: currentRow.currency,
            method: "DELETE",
          });
          setDeltradeplan(currentRow.plan_trade_id);
          handleDeletetrade(currentRow);
        };

        return (
          <Tooltip title="Delete">
            <GridActionsCellItem
              icon={<DeleteIcon />}
              label="Delete"
              color="error"
              onClick={handlePreview}
            />
          </Tooltip>
        );
      },
      width: 50,
    },
  ];
  const columns = [
    {
      field: "cost_code",
      renderHeader: () => <strong>{"Cost"}</strong>,
      type: "singleSelect",
      valueOptions: costcode?.result,
      getOptionLabel: (value) => value.cost_name,
      getOptionValue: (value) => value.cost_code,
      editable: true,
      width: 200,
    },
    {
      field: "amount",
      renderHeader: () => <strong>{"Amount"}</strong>,
      type: "number",
      width: 200,
      align: "left",
      headerAlign: "left",
      editable: true,
    },
    {
      field: "actions",
      type: "actions",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 100,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModel[params.id]?.mode === GridRowModes.Edit;
        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<CheckIcon style={{ color: "black" }} />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(params)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(params.id)}
              color="inherit"
            />,
          ];
        }
        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params)}
            color="inherit"
          />,
        ];
      },
    },
    {
      renderHeader: () => <strong>{"Save"}</strong>,
      width: 100,
      align: "left",
      headerAlign: "left",
      editable: true,
      renderCell: (params) => {
        return [
          <GridActionsCellItem
            icon={<SaveIcon />}
            label="Save"
            sx={{
              color: "primary.main",
            }}
            onClick={() => saveCost(params)}
          />,
        ];
      },
    },
  ];
  const dealdatacolumn = [
    {
      field: "deal_id",
      renderHeader: () => <strong>{"Deal id"}</strong>,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;

          setDeal({
            deal_id: currentRow.deal_id,
            deal_number: currentRow.deal_number,
            title: currentRow.title,
            description: currentRow.description,
            deal_date: currentRow.deal_date,
            deal_status: currentRow.deal_status,
            deal_plan_id: currentRow.deal_plan_id,
            org_id: currentRow.org_id,
            method: "PUT",
          });

          handleDealOpen();
        };
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Link onClick={handleEdit} variant="h7">
              {"D-" + params.row.deal_id}
            </Link>
          </Box>
        );
      },
      width: 110,
    },
    {
      field: "title",
      renderHeader: () => <strong>{"Title"}</strong>,
      width: 200,
    },
    {
      field: "deal_date",
      renderHeader: () => <strong>{"Deal date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.deal_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 110,
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 250,
    },
  ];
  const tradelistgrid = [
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade list"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.trade_id}</Typography>
        </Box>
      ),
      width: 100,
    },
  ];
  const unlallocatedDealTradecolumns = [
    {
      field: "#",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 40,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          setDealcon({
            deal_trade_id: currentRow.deal_trade_id,
            method: "DELETE",
          });

          handleDealconOpen();
        };
        if (
          session &&
          session?.user?.menuitems.filter((f) => f.menu_name === "Deal")[0]
            .permission === "Edit"
        ) {
          return (
            <GridActionsCellItem
              icon={<LinkOffIcon />}
              label="Edit"
              color="secondary"
              onClick={handleEdit}
            />
          );
        } else {
          return null;
        }
      },
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{"T-" + params.row.trade_id}</Typography>
          <Typography variant="h7">
            <strong>{dayjs(params.row.trade_date).format("YYYY-MM-DD")}</strong>
          </Typography>
        </Box>
      ),
      width: 110,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 180,
    },

    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.quantity} <strong> {params.row.units} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "country_party_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.country_party_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_type} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },
    // {
    //   field: "est_bl_date",
    //   renderHeader: () => <strong>{"EST BL"}</strong>,
    //   renderCell: (params) => (
    //     <Box sx={{ flexDirection: "column", display: "flex" }}>
    //       <Typography variant="h7">
    //         {dayjs(params.row.trade_date).format("YYYY-MM-DD")}
    //       </Typography>
    //       <Typography sx={{ fontSize: "12px" }}>
    //         <strong> {params.row.transportation_type} </strong>
    //       </Typography>
    //     </Box>
    //   ),
    //   width: 100,
    // },
  ];
  const tradecolumn = [
    {
      field: "#",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 40,

      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          setDealcon({
            trade_id: currentRow.trade_id,
            method: "POST",
          });

          handleDealconOpen();
        };
        if (
          session &&
          session?.user?.menuitems.filter((f) => f.menu_name === "Deal")[0]
            .permission === "Edit"
        ) {
          return (
            <GridActionsCellItem
              icon={<InsertLinkIcon />}
              label="Edit"
              color="secondary"
              onClick={handleEdit}
            />
          );
        } else {
          return null;
        }
      },
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{"T-" + params.row.trade_id}</Typography>
          <Typography variant="h7">
            <strong>{dayjs(params.row.trade_date).format("YYYY-MM-DD")}</strong>
          </Typography>
        </Box>
      ),
      width: 110,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 180,
    },

    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.quantity} <strong> {params.row.units} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "country_party_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.country_party_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_type} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },
  ];
  ///////////////////////////////////////////////////////////////////////////////////////////
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  const dealRows = deals?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const tradeRows = trade?.result
    ? trade.result.map((row, i) => ({
        id: i + 1,
        ...row,
      }))
    : [];
  const rowstrade = unlallocatedTrade?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const undeal = undealtrade?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const plandealrow = plandealapi?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  const plantraderow = plantrade?.result?.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  if (tank?.result?.length > 0) {
    const tankRows = tank?.result?.map((row, i) => ({
      id: i + 1,
      plan_cost_id: row.plan_cost_id,
      amount: row.amount,
      cost_code: row.cost_code,
      type: "EDIT",
    }));
    setRows(tankRows);
  }
  const filteredRows = tradeRows.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText.toLowerCase()))
    )
  );
  const filteredRows1 = rowstrade?.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText1.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText1.toLowerCase()))
    )
  );
  const filteredRows2 = undeal.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText2.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText2.toLowerCase()))
    )
  );
  const filteredRows3 = plandealrow?.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText3.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText3.toLowerCase()))
    )
  );
  function calculateColumnSum(rows, columnName) {
    let sum = 0;
    if (Array.isArray(rows)) {
      rows.forEach((row) => {
        const value = parseFloat(row[columnName]);
        if (!isNaN(value)) {
          sum += value;
        }
      });
    }
    return sum;
  }
  const total = calculateColumnSum(rows, "amount");

  ////////////////////////////////////////////////////////////////////////////////////////////
  return (
    <>
      <Box>
        <Box>
          <AntTabs value={value} onChange={handleChange} aria-label="deal tab">
            <AntTab label="Overview" {...a11yProps(0)} />
            <AntTab label="Deal" {...a11yProps(1)} />
            <AntTab label="Plan" {...a11yProps(2)} />
          </AntTabs>
        </Box>
        <Box sx={{ bgcolor: "#fff" }}>
          <CustomTabPanel value={value} index={0}>
            <DealOverview />
          </CustomTabPanel>
          <CustomTabPanel value={value} index={1}>
            <Grid container spacing={2}>
              <Grid item xs={1}>
                <FormControl fullWidth sx={{ mb: 1 }}>
                  <TextField
                    variant="outlined"
                    size="small"
                    placeholder="Search..."
                    value={filterText}
                    onChange={handleFilterChange}
                  />
                </FormControl>
                <DataGrid
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    minHeight: "63.5vh",
                    maxHeight: "63.5vh",
                    minWidth: "1vw",
                  }}
                  onRowClick={handleTradeIdCellClick}
                  getRowId={(row) => row.trade_id}
                  rows={filteredRows ? filteredRows : []}
                  columns={tradelistgrid}
                  autoHeight
                  rowHeight={25}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                  hideFooter
                />
              </Grid>
              <Grid item xs={6}>
                {session &&
                session?.user?.menuitems.filter(
                  (f) => f.menu_name === "Deal"
                )[0].permission === "Edit" ? (
                  <Stack
                    direction={"row"}
                    spacing={1}
                    justifyContent="flex-end"
                    sx={{ mb: 1.4 }}
                  >
                    <Button
                      variant="text"
                      size="small"
                      sx={{ justifyContent: "flex-end" }}
                      onClick={() => {
                        setDeal({
                          deal_number: "",
                          title: "",
                          description: "",
                          deal_date: dayjs().format("YYYY-MM-DD"),
                          method: "POST",
                        });

                        handleDealOpen();
                      }}
                      startIcon={<AddSharpIcon />}
                    >
                      DEAL
                    </Button>
                  </Stack>
                ) : null}
                <Box sx={{ display: "flex" }}>
                  <Divider orientation="vertical" flexItem sx={{ mr: 2 }} />
                  <DataGrid
                    sx={{
                      backgroundColor: "#FFFFFF",
                      "& .MuiDataGrid-cell:hover": {
                        color: "primary.main",
                      },
                      "& .MuiDataGrid-cell": {
                        borderTop: 1,
                        borderTopColor: "#E9ECF0",
                      },
                      minHeight: "63.5vh",
                      overflow: "auto",
                    }}
                    onRowClick={handleCellClick}
                    getRowId={(row) => row.deal_id}
                    rows={dealRows ? dealRows : []}
                    columns={dealdatacolumn}
                    autoHeight
                    getRowHeight={() => "auto"}
                    slots={{
                      noRowsOverlay: CustomNoRowsOverlay,
                    }}
                    pageSizeOptions={[50]}
                  />
                  <Divider orientation="vertical" flexItem sx={{ ml: 2 }} />
                </Box>
              </Grid>
              <Grid item xs={5}>
                <AntTabs
                  value={value1}
                  onChange={handleChange1}
                  aria-label="Connect tab"
                  sx={{
                    mt: -0.8,
                  }}
                >
                  <AntTab label="Allocated trades" {...a11yProps(0)} />
                  <AntTab label="Unallocated trades" {...a11yProps(1)} />
                </AntTabs>
                <CustomTabPanel value={value1} index={0}>
                  <Box
                    sx={{
                      mt: -3,
                      ml: -3,
                    }}
                  >
                    <DataGrid
                      sx={{
                        backgroundColor: "#FFFFFF",
                        "& .MuiDataGrid-cell:hover": {
                          color: "primary.main",
                        },
                        "& .MuiDataGrid-cell": {
                          borderTop: 1,
                          borderTopColor: "#E9ECF0",
                        },
                        maxHeight: "63.4vh",
                        minHeight: "63.4vh",
                        overflow: "auto",
                      }}
                      // onRowClick={handleTradeCellClick}
                      getRowId={(row) => row.trade_id}
                      rows={filteredRows1 ? filteredRows1 : []}
                      columns={unlallocatedDealTradecolumns}
                      autoHeight
                      rowHeight={"auto"}
                      slots={{
                        noRowsOverlay: CustomNoRowsOverlay,
                      }}
                      pageSizeOptions={[50]}
                    />
                  </Box>
                </CustomTabPanel>
                <CustomTabPanel value={value1} index={1}>
                  <Box
                    sx={{
                      mt: -3,
                      ml: -3,
                    }}
                  >
                    <FormControl sx={{ minWidth: 250, mt: 1, mb: 1 }}>
                      <TextField
                        variant="outlined"
                        size="small"
                        placeholder="Search..."
                        value={filterText2}
                        onChange={handleFilterChange2}
                      />
                    </FormControl>
                    <DataGrid
                      sx={{
                        backgroundColor: "#FFFFFF",
                        "& .MuiDataGrid-cell:hover": {
                          color: "primary.main",
                        },
                        "& .MuiDataGrid-cell": {
                          borderTop: 1,
                          borderTopColor: "#E9ECF0",
                        },

                        maxHeight: "58.1vh",
                        minHeight: "58.1vh",
                        overflow: "auto",
                      }}
                      getRowId={(row) => row.trade_id}
                      rows={filteredRows2 ? filteredRows2 : []}
                      columns={tradecolumn}
                      slots={{
                        noRowsOverlay: CustomNoRowsOverlay,
                      }}
                      pageSizeOptions={[50]}
                    />
                  </Box>
                </CustomTabPanel>
              </Grid>
            </Grid>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={2}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Stack
                  direction={"row"}
                  spacing={1}
                  justifyContent="space-between"
                  sx={{ mb: 1 }}
                >
                  <FormControl fullWidth sx={{ mb: 1, width: 250 }}>
                    <TextField
                      variant="outlined"
                      size="small"
                      placeholder="Search..."
                      value={filterText3}
                      onChange={handleFilterChange3}
                    />
                  </FormControl>
                  {session &&
                  session?.user?.menuitems.filter(
                    (f) => f.menu_name === "Deal"
                  )[0].permission === "Edit" ? (
                    <Button
                      variant="text"
                      size="small"
                      sx={{ justifyContent: "flex-end" }}
                      onClick={() => {
                        formik.setValues({
                          deal_number: dayjs()
                            .format("YYMMDDHHMMms")
                            .toString(),
                          title: "",
                          description: "",
                          deal_date: "",
                          deal_status: "",
                          org_id: "",
                          method: "POST",
                        });
                        handleDealplanOpen();
                      }}
                      startIcon={<AddSharpIcon />}
                    >
                      DEAL PLAN
                    </Button>
                  ) : null}
                </Stack>
                <DataGrid
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    minHeight: "60vh",
                    maxHeight: "60vh",
                  }}
                  onRowClick={handledealplan}
                  getRowId={(row) => row.deal_plan_id}
                  rows={filteredRows3 ? filteredRows3 : []}
                  columns={dealcolumn}
                  autoHeight
                  rowHeight={25}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                  hideFooter
                />
              </Grid>
              <Grid item xs={6}>
                <Stack
                  direction={"row"}
                  spacing={1}
                  justifyContent="space-between"
                  sx={{ mb: 1.4 }}
                >
                  <Typography>
                    Title:<strong>{dealtotle ? dealtotle : null}</strong>
                  </Typography>
                  <Button
                    variant="text"
                    size="small"
                    sx={{ justifyContent: "flex-end" }}
                    onClick={() => {
                      formiktrade.setValues({
                        deal_plan_id: dealcell,
                        trade_type: "",
                        trade_begin_date: "",
                        trade_end_date: "",
                        counterparty_id: "",
                        product_id: "",
                        grade_id: "",
                        units: "",
                        quantity: "",
                        price: "",
                        amount: "",
                        currency: "",
                        method: "POST",
                      });
                      handleTradeplanOpen();
                    }}
                    startIcon={<AddSharpIcon />}
                    disabled={isButtonDisabled}
                  >
                    Trade Plan
                  </Button>
                </Stack>
                <DataGrid
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    minHeight: "28vh",
                    maxHeight: "28vh",
                    maxWidth: "50vw",
                  }}
                  rows={plantraderow ? plantraderow : []}
                  columns={dealtradecolumn}
                  autoHeight
                  rowHeight={25}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                  hideFooter
                />
                <DataGrid
                  rows={rows ? rows : []}
                  columns={columns}
                  editMode="row"
                  rowModesModel={rowModesModel}
                  onRowModesModelChange={handleRowModesModelChange}
                  onRowEditStop={handleRowEditStop}
                  processRowUpdate={processRowUpdate}
                  slots={{
                    toolbar: EditToolbar,
                    noRowsOverlay: CustomNoRowsOverlay,
                    //
                  }}
                  slotProps={{
                    toolbar: { setRows, setRowModesModel },
                  }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    minHeight: "32vh",
                    maxHeight: "32vh",
                    maxWidth: "50vw",
                  }}
                />
              </Grid>
            </Grid>
          </CustomTabPanel>
        </Box>
      </Box>
      <Modal
        open={opendealplan}
        onClose={handleCloseDealplan}
        aria-labelledby="Add modal"
        aria-describedby="ADD modal description"
      >
        <Box sx={style1}>
          {formik.values.method === "DELETE" ||
          formik.values.method === "PATCH" ? (
            <>
              <Box>
                <Typography variant="h6">
                  {formik.values.method === "DELETE" ? "Deal delete?" : null}
                </Typography>
              </Box>
              <Stack
                component="div"
                sx={{
                  width: 400,
                  maxWidth: "100%",
                }}
                noValidate
                autoComplete="off"
              >
                <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                  <Alert severity="error">
                    <AlertTitle>{formik.deal_plan_id}</AlertTitle>
                    {formik.values.method === "DELETE"
                      ? "Are you sure delete?"
                      : null}
                  </Alert>
                </Box>
              </Stack>
              <Box
                sx={{
                  display: "flex",
                  marginBottom: 1,
                  justifyContent: "flex-end",
                }}
              >
                <Button onClick={handleCloseDealplan} variant="outlined">
                  no
                </Button>
                <Button
                  onClick={async () => {
                    formik.values.method === "DELETE"
                      ? deleteTerminal()
                      : resetPassword();
                    handleCloseDealplan();
                  }}
                  variant="contained"
                  sx={{
                    marginLeft: "10px",
                    marginRight: "10px",
                  }}
                >
                  yes
                </Button>
              </Box>
            </>
          ) : (
            <>
              <Box sx={style1}>
                <form onSubmit={formik.handleSubmit}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" gutterBottom>
                      Deal
                    </Typography>
                    <IconButton
                      sx={{ display: "flex", mt: -1 }}
                      onClick={handleCloseDealplan}
                      aria-label="close"
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>
                  <Divider sx={{ mb: 1 }} />
                  <Stack direction={"column"} spacing={1}>
                    <FormControl fullWidth>
                      <InputLabel size="small" sx={{ color: " #0B626B" }}>
                        Company
                      </InputLabel>
                      <Select
                        name="org_id"
                        value={formik.values.org_id}
                        onChange={formik.handleChange}
                        size="small"
                        label="Company"
                      >
                        {org?.result.length >= 0 &&
                          org?.result.map((item) => (
                            <MenuItem value={item.org_id} key={item.org_id}>
                              {item.org_name}
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText
                        error={
                          formik.touched.org_id && Boolean(formik.errors.org_id)
                        }
                      >
                        {formik.touched.org_id && formik.errors.org_id}
                      </FormHelperText>
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        id="deal_number"
                        name="deal_number"
                        size="small"
                        disabled
                        label="Deal number"
                        value={formik.values.deal_number}
                        onChange={formik.handleChange}
                      />
                      <FormHelperText
                        error={
                          formik.touched.deal_number &&
                          Boolean(formik.errors.deal_number)
                        }
                      >
                        {formik.touched.deal_number &&
                          formik.errors.deal_number}
                      </FormHelperText>
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        id="title"
                        name="title"
                        size="small"
                        label="Title"
                        value={formik.values.title}
                        onChange={formik.handleChange}
                      />
                      <FormHelperText
                        error={
                          formik.touched.title && Boolean(formik.errors.title)
                        }
                      >
                        {formik.touched.title && formik.errors.title}
                      </FormHelperText>
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        id="description"
                        name="description"
                        size="small"
                        label="Desciption"
                        multiline
                        rows={10}
                        value={formik.values.description}
                        onChange={formik.handleChange}
                      />
                    </FormControl>
                    <FormControl fullWidth sx={{ mt: 2 }}>
                      <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker
                          name="deal_date"
                          id="deal_date"
                          label="Date"
                          InputLabelProps={{
                            style: {
                              color: " #0B626B",
                            },
                          }}
                          slotProps={{
                            textField: {
                              size: "small",
                            },
                          }}
                          format="YYYY-MM-DD"
                          value={dayjs(formik.values.deal_date)}
                          onChange={(newValue) => {
                            formik.setFieldValue(
                              "deal_date",
                              dayjs(newValue).format("YYYY-MM-DD")
                            );
                          }}
                        />
                      </LocalizationProvider>
                    </FormControl>
                    <FormControl fullWidth>
                      <InputLabel size="small" sx={{ color: " #0B626B" }}>
                        Status
                      </InputLabel>
                      <Select
                        name="deal_status"
                        value={formik.values.deal_status}
                        onChange={formik.handleChange}
                        size="small"
                        label="Status"
                      >
                        {lookup?.result.length >= 0 &&
                          lookup?.result
                            .filter((f) => f.lookup_type === "Deal_status")
                            .map((item) => (
                              <MenuItem
                                value={item.lookup_code}
                                key={item.lookup_code}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                      </Select>
                      <FormHelperText
                        error={
                          formik.touched.deal_status &&
                          Boolean(formik.errors.deal_status)
                        }
                      >
                        {formik.touched.deal_status &&
                          formik.errors.deal_status}
                      </FormHelperText>
                    </FormControl>
                  </Stack>

                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Button variant="outlined" onClick={handleCloseDealplan}>
                      Close
                    </Button>
                    <Button variant="contained" type="submit">
                      Save
                    </Button>
                  </Box>
                </form>
              </Box>
            </>
          )}
        </Box>
      </Modal>
      <Modal
        open={opentradeplan}
        onClose={handleCloseTradeplan}
        aria-labelledby="Add modal"
        aria-describedby="ADD modal description"
      >
        <Box sx={style}>
          <form onSubmit={formiktrade.handleSubmit}>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography variant="h5" gutterBottom>
                Trade
              </Typography>
              <IconButton
                sx={{ display: "flex", mt: -1 }}
                onClick={handleCloseTradeplan}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Divider sx={{ mb: 1.5 }} />
            <Stack
              spacing={1}
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ display: "flex" }}
            >
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: "#0B626B" }}>
                  Trade type
                </InputLabel>

                <Select
                  size="small"
                  labelId="trade_type"
                  margin="dense"
                  label="Trade type"
                  name="trade_type"
                  onChange={formiktrade.handleChange}
                  value={formiktrade.values.trade_type}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Trade_type")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Counter Party
                </InputLabel>
                <Select
                  labelId="counterparty_id"
                  id="counterparty_id"
                  name="counterparty_id"
                  defaultValue={""}
                  value={formiktrade.values.counterparty_id}
                  onChange={formiktrade.handleChange}
                  size="small"
                  sx={{ mb: -0.5 }}
                  label="Counter Party"
                >
                  {counterparty_id?.result &&
                  counterparty_id.result.length > 0 ? (
                    counterparty_id.result.map((item) => (
                      <MenuItem
                        value={item.legal_entity_id}
                        key={item.legal_entity_id}
                      >
                        {item.name}
                      </MenuItem>
                    ))
                  ) : (
                    <MenuItem value="" disabled>
                      No counterparty available
                    </MenuItem>
                  )}
                </Select>
                <FormHelperText
                  error={
                    formiktrade.touched.counterparty_id &&
                    Boolean(formiktrade.errors.counterparty_id)
                  }
                >
                  {formiktrade.touched.counterparty_id &&
                    formiktrade.errors.counterparty_id}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack
              spacing={1}
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ display: "flex", mt: 1.5 }}
            >
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    value={dayjs(formiktrade.values.trade_begin_date)}
                    name="trade_begin_date"
                    slotProps={{ textField: { size: "small" } }}
                    label="Date range from"
                    onChange={(newValue) => {
                      formiktrade.setFieldValue(
                        "trade_begin_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    name="trade_end_date"
                    value={dayjs(formiktrade.values.trade_end_date)}
                    slotProps={{ textField: { size: "small" } }}
                    label="Date range to"
                    onChange={(newValue) => {
                      formiktrade.setFieldValue(
                        "trade_end_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
            </Stack>
            <Stack
              spacing={1}
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ display: "flex", mt: 1.5 }}
            >
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Product
                </InputLabel>
                <Select
                  id="product_id"
                  name="product_id"
                  label="Product"
                  size="small"
                  value={formiktrade.values.product_id}
                  onBlur={formiktrade.handleBlur}
                  onChange={formiktrade.handleChange}
                >
                  {product_id?.result.length >= 0 &&
                    product_id?.result.map((item) => (
                      <MenuItem value={item.product_id} key={item.product_id}>
                        {item.product_name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formiktrade.touched.product_id &&
                    Boolean(formiktrade.errors.product_id)
                  }
                >
                  {formiktrade.touched.product_id &&
                    formiktrade.errors.product_id}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Grade
                </InputLabel>
                <Select
                  name="grade_id"
                  id="grade_id"
                  label="Grade"
                  value={formiktrade.values.grade_id}
                  onBlur={formiktrade.handleBlur}
                  onChange={formiktrade.handleChange}
                  size="small"
                >
                  {product_id?.result.filter(
                    (f) => f.product_id === formiktrade.values.product_id
                  ).length > 0 &&
                    product_id?.result
                      .filter(
                        (f) => f.product_id === formiktrade.values.product_id
                      )[0]
                      .grades.map((item) => (
                        <MenuItem value={item.grade_id} key={item.grade_id}>
                          {item.grade_name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formiktrade.touched.grade_id &&
                    Boolean(formiktrade.errors.grade_id)
                  }
                >
                  {formiktrade.touched.grade_id && formiktrade.errors.grade_id}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack
              spacing={1}
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ display: "flex", mt: 1.5 }}
            >
              <FormControl fullWidth>
                <TextField
                  id="quantity"
                  name="quantity"
                  type="number"
                  value={parseFloat(formiktrade.values.quantity)}
                  onChange={(e) => {
                    formiktrade.setFieldValue(
                      "quantity",
                      e.target.value?e.target.value:0
                    );
                    // Calculate amount when quantity changes
                    const newAmount = (
                      parseFloat(formiktrade.values.price?formiktrade.values.price:0) *
                      parseFloat(e.target.value)
                    ).toFixed(3);
                    formiktrade.setFieldValue("amount", newAmount);
                  }}
                  InputLabelProps={{
                    style: { color: "#0B626B" },
                  }}
                  size="small"
                  label="Quantity"
                />
                <FormHelperText
                  error={
                    formiktrade.touched.quantity &&
                    Boolean(formiktrade.errors.quantity)
                  }
                >
                  {formiktrade.touched.quantity && formiktrade.errors.quantity}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: "#0B626B" }}>
                  Units
                </InputLabel>
                <Select
                  value={formiktrade.values.units}
                  name="units"
                  label="Unit"
                  onChange={formiktrade.handleChange}
                  size="small"
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Unit")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formiktrade.touched.units &&
                    Boolean(formiktrade.errors.units)
                  }
                >
                  {formiktrade.touched.units && formiktrade.errors.units}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack
              spacing={1}
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ display: "flex", mt: 1.5 }}
            >
              <FormControl fullWidth>
                <TextField
                  id="price"
                  type="number"
                  name="price"
                  value={parseFloat(formiktrade.values.price)}
                  onChange={(e) => {
                    formiktrade.setFieldValue(
                      "price",
                      e.target.value?e.target.value:0
                    );
                    // Calculate amount when price changes
                    const newAmount = (
                      parseFloat(e.target.value) *
                      parseFloat(formiktrade.values.quantity)
                    ).toFixed(3);
                    formiktrade.setFieldValue("amount", newAmount);
                  }}
                  InputLabelProps={{
                    style: { color: "#0B626B" },
                  }}
                  size="small"
                  label="Price"
                />
                <FormHelperText
                  error={
                    formiktrade.touched.price &&
                    Boolean(formiktrade.errors.price)
                  }
                >
                  {formiktrade.touched.price && formiktrade.errors.price}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: "#0B626B" }}>
                  Currency
                </InputLabel>
                <Select
                  labelId="currency"
                  id="currency"
                  name="currency"
                  label="Currency"
                  value={formiktrade.values.currency}
                  onBlur={formiktrade.handleBlur}
                  onChange={formiktrade.handleChange}
                  size="small"
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Currency")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formiktrade.touched.currency &&
                    Boolean(formiktrade.errors.currency)
                  }
                >
                  {formiktrade.touched.currency && formiktrade.errors.currency}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack
              spacing={1}
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ display: "flex", mt: 1.5 }}
            >
              <FormControl fullWidth>
                <TextField
                  id="amount"
                  name="amount"
                  size="small"
                  value={numberWithCommas(formiktrade.values.amount)}
                  InputLabelProps={{
                    style: { color: "#0B626B" },
                  }}
                  onBlur={formiktrade.handleBlur}
                  label="Amount"
                  InputProps={{
                    readOnly: true,
                  }}
                />
                <FormHelperText
                  error={
                    formiktrade.touched.amount &&
                    Boolean(formiktrade.errors.amount)
                  }
                >
                  {formiktrade.touched.amount && formiktrade.errors.amount}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Divider sx={{ mt: 1, mb: 1 }} />
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Button variant="outlined" onClick={handleCloseTradeplan}>
                Close
              </Button>
              <Button variant="contained" type="submit">
                Save
              </Button>
            </Box>
          </form>
        </Box>
      </Modal>
      <Modal open={open1} onClose={handleClose1}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 2,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Confirm
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{""}</AlertTitle>
                Are you sure you want to delete?
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                handleClose1();
                deleteData();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              YES
            </Button>
            <Button onClick={handleClose1} variant="outlined">
              NO
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal open={open2} onClose={handleClose2}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 2,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Confirm
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{""}</AlertTitle>
                Are you sure you want to delete?
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                handleClose2();
                deleteTrade();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              YES
            </Button>
            <Button onClick={handleClose2} variant="outlined">
              NO
            </Button>
          </Box>
        </Box>
      </Modal>
      <ToastContainer />
      <Deal deal={deal} openDeal={openDeal} handleDealClose={handleDealClose} />
      <Dealcon
        dealcon={dealcon}
        openDealcon={openDealcon}
        handleDealconClose={handleDealconClose}
      />
    </>
  );
}
