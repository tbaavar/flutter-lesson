"use client";
import * as React from "react";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import {
  Box,
  Button,
  Stack,
  Card,
  TextField,
  Tooltip,
  CircularProgress,
  Alert,
} from "@mui/material";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import EditIcon from "@mui/icons-material/Edit";
import useSWR from "swr";
import { useRouter } from "next/navigation";
import CustomNoRowsOverlay from "@/components/NoRecord";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import Grid from "@mui/material/Grid";
import MyAlert from "@/components/Myalert";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { fetchData } from "@/lib/fetch";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { numberWithCommas } from "@/lib/numberWithCommas";
function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function DealLog() {
  const router = useRouter();
  const { data: session } = useSession();
  const [value, setValue] = React.useState(0);
  const [tradeid, setTradeid] = React.useState(0);
  const [filterText, setFilterText] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [selectedTradeId, setSelectedTradeId] = React.useState("");
  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  const { data: delivery } = useSWR(
    selectedTradeId
      ? [`/api/trade/log/delivery/${selectedTradeId}`, session?.user?.token]
      : null,
    fetcher
  );

  const { data: law } = useSWR(
    selectedTradeId
      ? [`/api/trade/log/law/${selectedTradeId}`, session?.user?.token]
      : null,
    fetcher
  );

  const { data: Payment } = useSWR(
    selectedTradeId
      ? [`/api/trade/log/payment/${selectedTradeId}`, session?.user?.token]
      : null,
    fetcher
  );

  const { data: product } = useSWR(
    selectedTradeId
      ? [`/api/trade/log/product/${selectedTradeId}`, session?.user?.token]
      : null,
    fetcher
  );

  const { data: pricing } = useSWR(
    selectedTradeId
      ? [`/api/trade/log/pricing/${selectedTradeId}`, session?.user?.token]
      : null,
    fetcher
  );

  const {
    data: trades,
    error,
    isLoading,
  } = useSWR([`/api/trade`, session?.user?.token], fetcher);

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  const handleRowClick = (params) => {
    setTradeid(`${params.row.trade_id}`);
  };
  const handleCellClick = (params, event) => {
    setSelectedTradeId(params.row.trade_id);
  };
  const column = [
    // {
    //   field: "action",
    //   headerAlign: "left",
    //   renderHeader: () => <strong>{"ACTION"}</strong>,
    //   width: 60,
    //   renderCell: (params) => {
    //     const handleEdit = (e) => {
    //       router.push(`/trade/detail/${params.row.trade_id}`);
    //     };
    //     return (
    //       <Stack direction="row" spacing={2}>
    //         <Tooltip title="Edit">
    //           {session && session?.user?.role === "Trader" ? (
    //             <>
    //               <GridActionsCellItem
    //                 icon={<EditIcon />}
    //                 label="Edit"
    //                 color="secondary"
    //                 onClick={handleEdit}
    //               />
    //             </>
    //           ) : (
    //             <GridActionsCellItem
    //               icon={<RemoveRedEyeIcon />}
    //               label="Edit"
    //               color="secondary"
    //               onClick={handleEdit}
    //             />
    //           )}
    //         </Tooltip>
    //       </Stack>
    //     );
    //   },
    // },

    {
      field: "trade_id",
      renderHeader: () => <strong>{"TRADE ID"}</strong>,
      width: 95,

      headerAlign: "left",
    },
    {
      field: "company_id",
      renderHeader: () => <strong>{"COMPANY"}</strong>,
      width: 70,

      headerAlign: "left",
    },
    {
      field: "country_party_name",
      renderHeader: () => <strong>{"COUNTERPARTY"}</strong>,
      width: 250,

      headerAlign: "left",
    },
    {
      field: "trade_type",
      renderHeader: () => <strong>{"TRADE TYPE"}</strong>,
      width: 120,

      headerAlign: "left",
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"PRODUCT"}</strong>,
      width: 100,

      headerAlign: "left",
    },
    {
      field: "grade_name",
      renderHeader: () => <strong>{"GRADE"}</strong>,
      width: 180,

      headerAlign: "left",
    },

    {
      field: "quantity",
      renderHeader: () => <strong>{"QUANTITY"}</strong>,
      width: 100,

      headerAlign: "left",
      valueGetter: (params) => numberWithCommas(params.row.quantity),
    },
    {
      field: "units",
      renderHeader: () => <strong>{"UNITS"}</strong>,
      width: 50,

      headerAlign: "left",
    },
    {
      field: "date_range_from",
      renderHeader: () => <strong>{"DATE FROM"}</strong>,
      width: 120,

      headerAlign: "left",
    },
    {
      field: "date_range_to",
      renderHeader: () => <strong>{"DATE TO"}</strong>,
      width: 120,

      headerAlign: "left",
    },
    {
      field: "trader_name",
      renderHeader: () => <strong>{"TRADER"}</strong>,
      width: 80,

      headerAlign: "left",
    },
    {
      field: "trade_status",
      renderHeader: () => <strong>{"TRADE STATUS"}</strong>,
      width: 100,

      headerAlign: "left",
    },
    {
      field: "operation_flag",
      renderHeader: () => <strong>{"OPERATOR"}</strong>,
      width: 100,

      headerAlign: "left",
    },

    // {
    //   field: "operator_sign_off_flag",

    //   headerAlign: "left",
    //   renderHeader: () => <strong>{"ACTION"}</strong>,
    //   width: 60,
    //   renderCell: (params) => {
    //     const handleEdit = (e) => {
    //       const currentRow = params.row;
    //       const saveOperation = async () => {
    //         const body = {
    //           trade_id: currentRow.trade_id,

    //           operator_sign_off_flag:
    //             currentRow.operator_sign_off_flag !== "Y" ? "Y" : "N",
    //           username: session?.user?.username,
    //         };

    //         try {
    //           setLoading(true);
    //           const res = await fetchData(
    //             `/api/trade/operation`,
    //             "PUT",
    //             body,
    //             session?.user?.token
    //           );

    //           if (res.status === 200) {
    //             let message = null;
    //             body.operator_sign_off_flag === "Y"
    //               ? (message = "Operator Signed")
    //               : (message = "Operator Unsigned");

    //             toast.success(message, {
    //               position: toast.POSITION.TOP_RIGHT,
    //               className: "toast-message",
    //             });
    //             mutate();
    //           } else {
    //             toast.error("Алдаа гарлаа", {
    //               position: toast.POSITION.TOP_RIGHT,
    //             });
    //           }
    //         } catch (error) {
    //         } finally {
    //           setLoading(false);
    //         }
    //       };
    //       saveOperation();
    //     };
    //     return (
    //       <Stack direction="row" spacing={2}>
    //         <Tooltip title="Edit">
    //           <GridActionsCellItem
    //             icon={
    //               params.row.operator_sign_off_flag === "Y" ? (
    //                 <CloseIcon />
    //               ) : (
    //                 <DoneIcon />
    //               )
    //             }
    //             label="Edit"
    //             color="secondary"
    //             onClick={handleEdit}
    //           />
    //         </Tooltip>
    //       </Stack>
    //     );
    //   },
    // },
    {
      field: "finance_flag",
      renderHeader: () => <strong>{"FINANCE"}</strong>,
      width: 100,

      headerAlign: "left",
    },
    // {
    //   field: "finance_sign_off_flag",

    //   headerAlign: "left",
    //   renderHeader: () => <strong>{"ACTION"}</strong>,
    //   width: 60,
    //   renderCell: (params) => {
    //     const handleEdit = (e) => {
    //       const currentRow = params.row;

    //       const saveFinance = async () => {
    //         const body = {
    //           trade_id: currentRow.trade_id,

    //           finance_sign_off_flag:
    //             currentRow.finance_sign_off_flag !== "Y" ? "Y" : "N",
    //           username: session?.user?.username,
    //         };

    //         try {
    //           setLoading(true);
    //           const res = await fetchData(
    //             `/api/trade/finance`,
    //             "PUT",
    //             body,
    //             session?.user?.token
    //           );

    //           if (res.status === 200) {
    //             let message = null;
    //             body.finance_sign_off_flag === "Y"
    //               ? (message = "Financer Signed")
    //               : (message = "Financer Unsigned");
    //             mutate();
    //             toast.success(message, {
    //               position: toast.POSITION.TOP_RIGHT,
    //               className: "toast-message",
    //             });
    //           } else {
    //             toast.error("Алдаа гарлаа", {
    //               position: toast.POSITION.TOP_RIGHT,
    //             });
    //           }
    //         } catch (error) {
    //         } finally {
    //           setLoading(false);
    //         }
    //       };
    //       saveFinance();
    //     };
    //     return (
    //       <Stack direction="row" spacing={2}>
    //         <Tooltip title="Edit">
    //           <GridActionsCellItem
    //             icon={
    //               params.row.finance_sign_off_flag === "Y" ? (
    //                 <CloseIcon />
    //               ) : (
    //                 <DoneIcon />
    //               )
    //             }
    //             label="Edit"
    //             color="secondary"
    //             onClick={handleEdit}
    //           />
    //         </Tooltip>
    //       </Stack>
    //     );
    //   },
    // },
  ];

  const productcolumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_product_id",
      renderHeader: () => <strong>{"Log ID"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "trade_product_id",
      renderHeader: () => <strong>{"Product ID"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade ID"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "geographical_area",
      renderHeader: () => <strong>{"Geographical"}</strong>,
      width: 105,
      headerAlign: "left",
    },
    {
      field: "grade_id",
      renderHeader: () => <strong>{"Grade ID"}</strong>,
      width: 75,
      headerAlign: "left",
    },
    {
      field: "product_id",
      renderHeader: () => <strong>{"Product ID"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "country_origin",
      renderHeader: () => <strong>{"Country origin"}</strong>,
      width: 105,
      headerAlign: "left",
    },
    {
      field: "units",
      renderHeader: () => <strong>{"Units"}</strong>,
      width: 55,
      headerAlign: "left",
    },
    {
      field: "est_density",
      renderHeader: () => <strong>{"Est density"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "trade_treatment",
      renderHeader: () => <strong>{"Trade treatment"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity"}</strong>,
      width: 75,
      headerAlign: "left",
    },
    {
      field: "plus_minus",
      renderHeader: () => <strong>{"Plus minus"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "min_quantity",
      renderHeader: () => <strong>{"Min quantity"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "max_quantity",
      renderHeader: () => <strong>{"Max quantity"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "product_cover_type",
      renderHeader: () => <strong>{"Product cover type"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "product_cover_counterparty",
      renderHeader: () => <strong>{"Product cover counterparty"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "product_option_quantity",
      renderHeader: () => <strong>{"Product option quantity"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "intention",
      renderHeader: () => <strong>{"intention"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "ops_active_flag",
      renderHeader: () => <strong>{"Ops active"}</strong>,
      width: 85,
      headerAlign: "left",
    },
    {
      field: "storage_active_flag",
      renderHeader: () => <strong>{"Storage active"}</strong>,
      width: 105,
      headerAlign: "left",
    },
    {
      field: "finance_active_flag",
      renderHeader: () => <strong>{"Finance active"}</strong>,
      width: 105,
      headerAlign: "left",
    },
    {
      field: "consolidate_deal_flag",
      renderHeader: () => <strong>{"Consolidate deal"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "financing_behalf_flag",
      renderHeader: () => <strong>{"Financing behalf"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "est_bl_date",
      renderHeader: () => <strong>{"Est bl date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "fixed_flag",
      renderHeader: () => <strong>{"Fixed"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 75,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update"}</strong>,
      width: 200,
      headerAlign: "left",
    },
  ];

  const pricingcolumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_fixed_pricing_id",
      renderHeader: () => <strong>{"Log trade fixed pricing ID"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "trade_fixed_pricing_id",
      renderHeader: () => <strong>{"Trade fixed pricing ID"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade ID"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "amount",
      renderHeader: () => <strong>{"Amount"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "currency",
      renderHeader: () => <strong>{"Currency"}</strong>,
      width: 75,
      headerAlign: "left",
    },
    {
      field: "unit",
      renderHeader: () => <strong>{"Unit"}</strong>,
      width: 55,
      headerAlign: "left",
    },
    {
      field: "cent_flag",
      renderHeader: () => <strong>{"Cent flag"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "freight_cost",
      renderHeader: () => <strong>{"Freight cost"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "freight_currency",
      renderHeader: () => <strong>{"Frieght currency"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "freight_unit",
      renderHeader: () => <strong>{"Freight unit"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "freight_cent_flag",
      renderHeader: () => <strong>{"Freight cent flag"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 95,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
  ];

  const deliverycolumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_delivery_id",
      renderHeader: () => <strong>{"Log trade delivery ID"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "trade_delivery_id",
      renderHeader: () => <strong>{"Trade delivery ID"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade ID"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "load_port_id",
      renderHeader: () => <strong>{"Load port ID"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "discharge_port_id",
      renderHeader: () => <strong>{"Discharge port ID"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "intended_vessel_id",
      renderHeader: () => <strong>{"Intended vessel ID"}</strong>,
      width: 125,
      headerAlign: "left",
    },
    {
      field: "laytime",
      renderHeader: () => <strong>{"Laytime"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "nor",
      renderHeader: () => <strong>{"Nor"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "pro_rata_cargo_flag",
      renderHeader: () => <strong>{"Prorata cargo"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "dates_based_on",
      renderHeader: () => <strong>{"Dates based on"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "date_range_from",
      renderHeader: () => <strong>{"Date range from"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "date_range_to",
      renderHeader: () => <strong>{"Date range to"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "quantity_based_on",
      renderHeader: () => <strong>{"Quantity based on"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "narrowed_by",
      renderHeader: () => <strong>{"Narrowed by"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "narrowed_lay_day",
      renderHeader: () => <strong>{"Narrowed lay day"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "demurrage_flag",
      renderHeader: () => <strong>{"Demurrage"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "pro_rata_per_day_flag",
      renderHeader: () => <strong>{"Prorata per day"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "spot_demurrage_rate",
      renderHeader: () => <strong>{"Spot demurrage rate"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "spot_demurrage_rate_other",
      renderHeader: () => <strong>{"Spot demurrage rate other"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "spot_demurrage_rate_max",
      renderHeader: () => <strong>{"Stop demurrage rate max"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "time_demurrage_rate",
      renderHeader: () => <strong>{"Time demurrage rate"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "time_demurrage_rate_other",
      renderHeader: () => <strong>{"Time demurrage rate other"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "laytime_demurrage_rate_maxtime",
      renderHeader: () => <strong>{"Laytime demurrage rate maxtime"}</strong>,
      width: 210,
      headerAlign: "left",
    },
    {
      field: "notification_timebar",
      renderHeader: () => <strong>{"Notification timebar"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "notification_from_date",
      renderHeader: () => <strong>{"Notification from date"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "notification_other",
      renderHeader: () => <strong>{"Notification other"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "fulldoc_timebar",
      renderHeader: () => <strong>{"Fulldoc timebar"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "fulldoc_from_date",
      renderHeader: () => <strong>{"Fulldoc from date"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "fulldoc_other",
      renderHeader: () => <strong>{"Fulldoc other"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "pdc_to_apply",
      renderHeader: () => <strong>{"PDC to apply"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "claims_person_id",
      renderHeader: () => <strong>{"Claim person id"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "duty_importer_record",
      renderHeader: () => <strong>{"Duty importer record"}</strong>,
      width: 300,
      headerAlign: "left",
    },
    {
      field: "duty_exporter_record",
      renderHeader: () => <strong>{"Duty exporter record"}</strong>,
      width: 250,
      headerAlign: "left",
    },
    {
      field: "first_purchaser_flag",
      renderHeader: () => <strong>{"First purchaser"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "tax_withheld_flag",
      renderHeader: () => <strong>{"Tax withheld"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
  ];

  const lawcolumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_law_id",
      renderHeader: () => <strong>{"Log trade law ID"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "trade_law_id",
      renderHeader: () => <strong>{"Trade law ID"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade ID"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "notes",
      renderHeader: () => <strong>{"Notes"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "conflict",
      renderHeader: () => <strong>{"Conflict"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "law_id",
      renderHeader: () => <strong>{"Law ID"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "title_to_pass",
      renderHeader: () => <strong>{"Title to pass"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "title_to_pass_other",
      renderHeader: () => <strong>{"Title to pass other"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "risk_to_pass",
      renderHeader: () => <strong>{"Rist to pass"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "risk_to_pass_other",
      renderHeader: () => <strong>{"Rist to pass other"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "general_term_condition",
      renderHeader: () => <strong>{"General term condition"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "quality_test_location",
      renderHeader: () => <strong>{"Quality test location"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "quality_test_other",
      renderHeader: () => <strong>{"Quality test other"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "quantity_test_port_id",
      renderHeader: () => <strong>{"Quantity test port ID"}</strong>,
      width: 190,
      headerAlign: "left",
    },
    {
      field: "quantity_test_other",
      renderHeader: () => <strong>{"Quantity test other"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "q_q_timebar",
      renderHeader: () => <strong>{"QQ timebar"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "q_q_from_date",
      renderHeader: () => <strong>{"QQ from date"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "q_q_other",
      renderHeader: () => <strong>{"QQ other"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "claim_timebar",
      renderHeader: () => <strong>{"Claim timebar"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "claim_from_date",
      renderHeader: () => <strong>{"Claim from date"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "claim_other",
      renderHeader: () => <strong>{"Claim other"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "inspection_share_port",
      renderHeader: () => <strong>{"Inspection share port"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "inspection_share_disport",
      renderHeader: () => <strong>{"Inspection share disport"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "contract_received_date",
      renderHeader: () => <strong>{"Contract received date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "response_sent_date",
      renderHeader: () => <strong>{"Response sent date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "contract_drafted_date",
      renderHeader: () => <strong>{"Contract drafted date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "contact_sent_date",
      renderHeader: () => <strong>{"Contact sent date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "broker_contract_received_date",
      renderHeader: () => <strong>{"Broker contract received date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "contract_finish_flag",
      renderHeader: () => <strong>{"Contract finish"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "contract_date_comment",
      renderHeader: () => <strong>{"Contract date comment"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 60,
      headerAlign: "left",
    },

    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      width: 200,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
  ];

  const paymentcolumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_payment_id",
      renderHeader: () => <strong>{"Log trade payment ID"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "trade_payment_id",
      renderHeader: () => <strong>{"Trade payment ID"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade ID"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "prepaid_flag",
      renderHeader: () => <strong>{"Prepaid"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "credit_type",
      renderHeader: () => <strong>{"Credit type"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "settlement_currency",
      renderHeader: () => <strong>{"Settlement currency"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "risk_mngt_approved_flag",
      renderHeader: () => <strong>{"Rist mngt approved"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "not_agreed_credit_given_flag",
      renderHeader: () => <strong>{"Not agreed credit given"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "ger_excluded_flag",
      renderHeader: () => <strong>{"Ger excluded"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "day_1",
      renderHeader: () => <strong>{"Day 1"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "time_condition_1",
      renderHeader: () => <strong>{"Time condition 1"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "condition_1",
      renderHeader: () => <strong>{"Condition 1"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "condition_other_1",
      renderHeader: () => <strong>{"Condition other 1"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "day_2",
      renderHeader: () => <strong>{"Day 2"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "time_condition_2",
      renderHeader: () => <strong>{"Time condition 2"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "condition_2",
      renderHeader: () => <strong>{"Condition 2"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "condition_other_2",
      renderHeader: () => <strong>{"Condition other 2"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "intended_vessel",
      renderHeader: () => <strong>{"Intended vessel"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "intended_vessel_whichever",
      renderHeader: () => <strong>{"Intended vessel whichever"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "fixed_date",
      renderHeader: () => <strong>{"Fixed date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "day_count_method",
      renderHeader: () => <strong>{"Day count method"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "prefered_pricing_parcels",
      renderHeader: () => <strong>{"Prefered pricing parcels"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "same_as_pricing_flag",
      renderHeader: () => <strong>{"Same as pricing"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "if_saturday_calc_method",
      renderHeader: () => <strong>{"If saturday calc method"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "if_sunday_calc_method",
      renderHeader: () => <strong>{"If sunday calc method"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "if_public_holiday_mon_calc_meth",
      renderHeader: () => <strong>{"If public holiday mon clac meth"}</strong>,
      width: 190,
      headerAlign: "left",
    },
    {
      field: "if_public_holiday_other_calc_me",
      renderHeader: () => <strong>{"If public holiday other calc me"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "interest_rate_type",
      renderHeader: () => <strong>{"Interest rate type"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "margin",
      renderHeader: () => <strong>{"Margin"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "period",
      renderHeader: () => <strong>{"Period"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "bank_id",
      renderHeader: () => <strong>{"Bank ID"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "credit_spread_rate",
      renderHeader: () => <strong>{"Credit spread rate"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "to_be_invoiced_flag",
      renderHeader: () => <strong>{"To be invoiced"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "payment_documentation",
      renderHeader: () => <strong>{"Payment documentation"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "payment_note",
      renderHeader: () => <strong>{"Payment note"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      width: 200,
      headerAlign: "left",
    },
  ];

  const productRows =
    product?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
    
  const pricingRows =
    pricing?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const deliveryRows =
    delivery?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const lawRows =
    law?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const PaymentRows =
    Payment?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const rows = trades?.result.map((row, i) => ({
    id: i + 1,
    trade_id: row.trade_id,
    deal_id: row.deal_id,
    company_id: row.company_id,
    counterparty_id: row.counterparty_id,
    country_party_name: row.country_party_name,
    trade_type: row.trade_type,
    trade_date: row.trade_date,
    product_id: row.product_id,
    product_name: row.product_name,
    grade_id: row.grade_id,
    grade_name: row.grade_name,
    quantity: row.quantity,
    units: row.units,
    date_range_from: row.date_range_from,
    date_range_to: row.date_range_to,
    trade_name: row.trade_name,
    trader_name: row.trader_name,
    trade_status: row.trade_status,
    operation_flag: row.operation_flag,
    finance_flag: row.finance_flag,
    operator_sign_off_flag: row.operator_sign_off_flag,
    finance_sign_off_flag: row.finance_sign_off_flag,
  }));
  const filteredRows = rows.filter((row) =>
    Object.values(row).some(
      (value) =>
        typeof value === "string" &&
        value.toLowerCase().includes(filterText.toLowerCase())
    )
  );
  const handleNewClick = () => {
    router.push("/trade/detail/new");
  };
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <ToastContainer />
          <Box sx={{ mb: 1 }}>
            <Stack
              direction={"row"}
              spacing={1}
              justifyContent="space-between"
              alignItems={"center"}
            >
              <TextField
                variant="outlined"
                size="small"
                placeholder="Search..."
                value={filterText}
                onChange={handleFilterChange}
                sx={{ minWidth: "300px" }}
              />

              {/* {session && session?.user?.role === "Trader" ? (
                <>
                  <Button
                    variant="text"
                    size="small"
                    color="dark"
                    onClick={handleNewClick}
                    startIcon={<AddSharpIcon />}
                    sx={{ justifyContent: "flex-end" }}
                  >
                    Trade
                  </Button>
                </>
              ) : null} */}
            </Stack>
          </Box>
          <Box style={{ height: 300, width: "98%", overflow: "auto" }}>
            <DataGrid
              onRowClick={handleCellClick}
              rows={filteredRows}
              columns={column}
              style={{ background: "white" }}
              sx={{
                backgroundColor: "#FFFFFF",
                "& .MuiDataGrid-cell:hover": {
                  color: "primary.main",
                },
                "& .MuiDataGrid-cell": {
                  borderTop: 1,
                  borderTopColor: "#E9ECF0",
                },
              }}
              rowHeight={25}
              pageSizeOptions={[50]}
            />
          </Box>
        </Grid>
        <Grid item xs={12} sx={{ ml: -2 }}>
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="secondary"
            textColor="inherit"
            aria-label="Unallocated Tabs"
          >
            <Tab label="Product" {...a11yProps(0)} />
            <Tab label="Delivery" {...a11yProps(1)} />
            <Tab label="Law" {...a11yProps(2)} />
            <Tab label="Payment" {...a11yProps(3)} />
            <Tab label="Pricing" {...a11yProps(4)} />
            <Tab label="Memo" {...a11yProps(5)} />
          </Tabs>
          <CustomTabPanel value={value} index={0}>
            <Card sx={{ p: 1 }}>
              <DataGrid
                getRowId={(row) => row.id}
                rows={productRows.filter(
                  (row) => row.trade_id === selectedTradeId
                )}
                columns={productcolumn}
                style={{ background: "white" }}
                onRowClick={handleRowClick}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: 1700,
                  overflow: "auto",
                }}
                rowHeight={30}
                pageSizeOptions={[50]}
              />
            </Card>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={1}>
            <Card sx={{ p: 1 }}>
              <DataGrid
                getRowId={(row) => row.id}
                rows={deliveryRows.filter(
                  (row) => row.trade_id === selectedTradeId
                )}
                columns={deliverycolumn}
                style={{ background: "white" }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: 1700,
                  overflow: "auto",
                }}
                rowHeight={30}
                pageSizeOptions={[50]}
              />
            </Card>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={2}>
            <Card sx={{ p: 1 }}>
              <DataGrid
                getRowId={(row) => row.id}
                rows={lawRows.filter((row) => row.trade_id === selectedTradeId)}
                columns={lawcolumn}
                style={{ background: "white" }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: 1700,
                  overflow: "auto",
                }}
                rowHeight={30}
                pageSizeOptions={[50]}
              />
            </Card>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={3}>
            <Card sx={{ p: 1 }}>
              <DataGrid
                getRowId={(row) => row.id}
                rows={PaymentRows.filter(
                  (row) => row.trade_id === selectedTradeId
                )}
                columns={paymentcolumn}
                style={{ background: "white" }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: 1700,
                  overflow: "auto",
                }}
                rowHeight={30}
                pageSizeOptions={[50]}
              />
            </Card>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={4}>
            <Card sx={{ p: 1 }}>
              <DataGrid
                getRowId={(row) => row.id}
                rows={pricingRows.filter(
                  (row) => row.trade_id === selectedTradeId
                )}
                columns={pricingcolumn}
                style={{ background: "white" }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  borderRadius: 0,
                  maxWidth: 1700,
                  overflow: "auto",
                }}
                rowHeight={30}
                pageSizeOptions={[50]}
              />
            </Card>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={5}></CustomTabPanel>
        </Grid>
      </Grid>
    </>
  );
}
