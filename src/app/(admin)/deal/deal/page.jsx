"use client";
import React from "react";
import {
  Box,
  Button,
  Modal,
  Stack,
  Typography,
  IconButton,
  FormControl,
  Divider,
  TextField,
  Select,
  InputLabel,
  CircularProgress,
  MenuItem,
  FormHelperText,
} from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CloseIcon from "@mui/icons-material/Close";
import { useSession } from "next-auth/react";
import * as yup from "yup";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { useFormik } from "formik";
import { fetchData } from "@/lib/fetch";
import useSWR from "swr";
import MyAlert from "@/components/Myalert";
import { fetcher } from "@/lib/fetcher";

export default function Deal({ openDeal, handleDealClose, deal }) {
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const { data: session } = useSession();
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: dealplan } = useSWR(
    [`/api/deal/plan`, session?.user?.token],
    fetcher
  );

  const {
    data: status,
    error,
    isLoading,
  } = useSWR([`/api/lookup/Deal_status`, session?.user?.token], fetcher);

  const formik = useFormik({
    initialValues: {
      deal_id: deal.deal_id,
      deal_number: deal.deal_number,
      title: deal.title,
      org_id: deal.org_id,
      deal_status: deal.deal_status,
      description: deal.description,
      deal_plan_id: deal.deal_plan_id,
      deal_date: deal.deal_date,
    },
    validationSchema: yup.object({
      title: yup.string().required("Please insert title!"),
      deal_status: yup.string().required("Please select status!"),
      org_id: yup.string().required("Please select company!"),
    }),
    onSubmit: (values) => {
      saveDeal(values);
    },

    enableReinitialize: true,
  });

  const saveDeal = async (data) => {
    const body = {
      deal_id: deal.method === "POST" ? undefined : data.deal_id,
      deal_number: data.deal_number,
      title: data.title,
      description: data.description,
      deal_status: data.deal_status,
      deal_date: data.deal_date,
      deal_plan_id: data.deal_plan_id,
      org_id: data.org_id,
      username: session?.user?.username,
    };
  
    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        "/api/deal",
        deal.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleDealClose();
    }
  };

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <Modal open={openDeal}>
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              bgcolor: "background.paper",
              border: "1px solid #000",
              overflowY: "auto",
              overflowX: "auto",
              boxShadow: 0,
              p: 1,
              width: 400,
            }}
          >
            <Box>
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h5" gutterBottom>
                  Deal
                </Typography>
                <IconButton
                  sx={{ display: "flex", mt: -1 }}
                  onClick={handleDealClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Divider sx={{ mb: 1 }} />

              <Stack direction={"column"} spacing={1} sx={{ m: 1 }}>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Company
                  </InputLabel>
                  <Select
                    name="org_id"
                    value={formik.values.org_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="Company"
                  >
                    {org?.result.length >= 0 &&
                      org?.result.map((item) => (
                        <MenuItem value={item.org_id} key={item.org_id}>
                          {item.org_name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.org_id && Boolean(formik.errors.org_id)
                    }
                  >
                    {formik.touched.org_id && formik.errors.org_id}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    id="title"
                    name="title"
                    size="small"
                    label="Title"
                    value={formik.values.title}
                    InputLabelProps={{
                      style: {},
                    }}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText
                    error={formik.touched.title && Boolean(formik.errors.title)}
                  >
                    {formik.touched.title && formik.errors.title}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    id="description"
                    name="description"
                    size="small"
                    label="Desciption"
                    multiline
                    rows={10}
                    value={formik.values.description}
                    InputLabelProps={{
                      style: {},
                    }}
                    onChange={formik.handleChange}
                  />
                </FormControl>
                <FormControl fullWidth sx={{ mt: 2 }}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      name="deal_date"
                      id="deal_date"
                      label="Date"
                      InputLabelProps={{
                        style: {
                          color: " #0B626B",
                        },
                      }}
                      slotProps={{
                        textField: {
                          size: "small",
                        },
                      }}
                      format="YYYY-MM-DD"
                      value={dayjs(formik.values.deal_date)}
                      onChange={(newValue) => {
                        formik.setFieldValue(
                          "deal_date",
                          dayjs(newValue).format("YYYY-MM-DD")
                        );
                      }}
                    />
                  </LocalizationProvider>
                </FormControl>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Status
                  </InputLabel>
                  <Select
                    name="deal_status"
                    value={formik.values.deal_status}
                    onChange={formik.handleChange}
                    size="small"
                    label="Status"
                  >
                    {status?.result.length >= 0 &&
                      status?.result.map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.deal_status &&
                      Boolean(formik.errors.deal_status)
                    }
                  >
                    {formik.touched.deal_status && formik.errors.deal_status}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <InputLabel size="small">Deal plan</InputLabel>
                  <Select
                    name="deal_plan_id"
                    value={formik.values.deal_plan_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="deal_plan"
                  >
                    {dealplan?.result.length >= 0 &&
                      dealplan?.result.map((item) => (
                        <MenuItem
                          value={item.deal_plan_id}
                          key={item.deal_plan_id}
                        >
                          {item.title}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.deal_plan_id &&
                      Boolean(formik.errors.deal_plan_id)
                    }
                  >
                    {formik.touched.deal_plan_id && formik.errors.deal_plan_id}
                  </FormHelperText>
                </FormControl>
              </Stack>
            </Box>
            <Divider sx={{ mt: 1, mb: 1 }} />
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Deal")[0]
              .permission === "Edit" ? (
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Button onClick={handleDealClose} variant="outlined">
                  Cancel
                </Button>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </Box>
            ) : null}
          </Box>
        </form>
      </Modal>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
