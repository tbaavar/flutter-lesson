"use client";
import React from "react";
import {
  Box,
  Button,
  Modal,
  Typography,
  IconButton,
  FormControl,
  Divider,
  InputLabel,
  Select,
  MenuItem,
  CircularProgress,
  Alert,
} from "@mui/material";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CloseIcon from "@mui/icons-material/Close";
import useSWR from "swr";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import * as yup from "yup";
import { useFormik } from "formik";
import { fetchData } from "@/lib/fetch";

export default function Dealcon({ openDealcon, handleDealconClose, dealcon }) {
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const { data: session } = useSession();
  const { data: deal } = useSWR([`/api/deal`, session?.user?.token], fetcher);

  const formik = useFormik({
    initialValues: {
      deal_id: "",
      trade_id: dealcon.trade_id,
    },
    validationSchema: yup.object({}),
    onSubmit: (values) => {
      saveDealcon(values);
    },

    enableReinitialize: true,
  });

  const saveDealcon = async (data) => {
    const body = {
      deal_id: data.deal_id,
      trade_id: data.trade_id,
      username: session?.user?.username,
      deal_trade_id: dealcon.deal_trade_id,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        "/api/deal/trade",
        dealcon.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleDealconClose();
    }
  };

  return (
    <Box>
      {dealcon.method === "POST" ? (
        <Modal
          open={openDealcon}
          aria-labelledby="New Voyage Modal"
          aria-describedby="New Voyage Modal Component"
        >
          <form onSubmit={formik.handleSubmit}>
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                bgcolor: "background.paper",
                border: "1px solid #000",
                overflowY: "auto",
                overflowX: "auto",
                boxShadow: 0,
                p: 1,
                width: 400,
              }}
            >
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h5" gutterBottom>
                  Deal Connection T-{dealcon.trade_id}
                </Typography>
                <IconButton
                  sx={{ display: "flex", mt: -1 }}
                  onClick={handleDealconClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
              </Box>

              <Box>
                <FormControl fullWidth>
                  <InputLabel
                    sx={{ color: " #0B626B" }}
                    size="small"
                    id="intended_vessel"
                  >
                    Deal
                  </InputLabel>
                  <Select
                    labelId="deal_id"
                    id="deal_id"
                    name="deal_id"
                    size="small"
                    label="Deal"
                    value={formik.values.deal_id}
                    onChange={formik.handleChange}
                  >
                    {deal?.result.length > 0 &&
                      deal?.result.map((item) => (
                        <MenuItem value={item.deal_id} key={item.deal_id}>
                          {item.title}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Box>
              <Divider sx={{ mt: 1, mb: 1 }} />
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Button variant="outlined" onClick={handleDealconClose}>
                  Close
                </Button>
                <Button variant="contained" type="submit" color="dark">
                  Save
                </Button>
              </Box>
            </Box>
          </form>
        </Modal>
      ) : (
        <Modal
          open={openDealcon}
          aria-labelledby="New Voyage Modal"
          aria-describedby="New Voyage Modal Component"
        >
          <form onSubmit={formik.handleSubmit}>
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                bgcolor: "background.paper",
                border: "1px solid #000",
                overflowY: "auto",
                overflowX: "auto",
                boxShadow: 0,
                p: 1,
                width: 400,
              }}
            >
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h5" gutterBottom>
                  Deal Connection {dealcon.trade_id}
                </Typography>
                <IconButton
                  sx={{ display: "flex", mt: -1 }}
                  onClick={handleDealconClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
              </Box>

              <Box>
                <Alert severity="error">
                  Are sure to undeal?
                </Alert>
              </Box>
              <Divider sx={{ mt: 1, mb: 1 }} />
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Button variant="outlined" onClick={handleDealconClose}>
                  Close
                </Button>
                <Button variant="contained" type="submit" color="dark">
                  Save
                </Button>
              </Box>
            </Box>
          </form>
        </Modal>
      )}
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </Box>
  );
}
