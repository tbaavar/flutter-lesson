"use client";
import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import { MenuItem, Divider, Alert } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import FormControl from "@mui/material/FormControl";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import InputLabel from "@mui/material/InputLabel";
import { renderTimeViewClock } from "@mui/x-date-pickers/timeViewRenderers";
import Typography from "@mui/material/Typography";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import FormControlLabel from "@mui/material/FormControlLabel";
import { numberWithCommas } from "@/lib/numberWithCommas";
import IconButton from "@mui/material/IconButton";
import { fetchData } from "@/lib/fetch";
import FormHelperText from "@mui/material/FormHelperText";
import * as yup from "yup";
import { useFormik } from "formik";
import useSWR from "swr";
import dayjs from "dayjs";
import { Stack } from "@mui/material";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import CircularProgress from "@mui/material/CircularProgress";
import MyAlert from "@/components/Myalert";

const label = { inputProps: { "aria-label": "Checkbox demo" } };

export default function LossParcelPage({
  parcel,
  openLossParcel,
  handleLossParcelClose,
  refreshAllData
}) {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [opens, setOpens] = React.useState(false);
  const [voyageId, setVoyageId] = React.useState(parcel.voyage_id);

  const handleClickClose = () => {
    setOpen(false);
  };
  const port_types = "Loss ";

  const formik = useFormik({
    initialValues: parcel,
    validationSchema: yup.object({
      voyage_id: yup.string().required("Please select voyage!"),
      operator: yup.string().required("Please select operator!"),
      invoicing_unit: yup.string().required("Please select invoice unit!"),
    }),

    onSubmit: async (values) => {
      saveParcelData(values);
      handleLossParcelClose();
    },
    enableReinitialize: true,
  });

  const { data: operator_user_id } = useSWR(
    parcel.company_id
      ? [`/api/lookup/user/${parcel.company_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const { data: ports } = useSWR(
    ["/api/lookup/port", session?.user?.token],
    fetcher
  );

  const {
    data: voyage,
    error,
    isLoading,
  } = useSWR(["/api/operation/voyage", session?.user?.token], fetcher);

  const { data: invoicing_unit } = useSWR(
    ["/api/lookup/Unit", session?.user?.token],
    fetcher
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const saveParcelData = async (data) => {
    const trade_code =
      data.trade_type === "Spot_Sale"
        ? "S"
        : formik.values.trade_type === "Term_Sale"
        ? "S"
        : "P";

    const body = {
      voyage_parcel_id:
        data.method == "PUT" ? data.voyage_parcel_id : undefined,
      trade_id: data.trade_id,
      voyage_id: data.voyage_id,
      parcel_number:
        data.method == "PUT"
          ? data.parcel_number
          : "P-" + dayjs().format("YYMMDDHHMMms").toString(),
      country_origin: data.country_origin,
      load_port_id: data.load_port_id,
      discharge_port_id: data.discharge_port_id,
      lc_open_date: data.lc_open_date,
      vat_type: data.vat_type,
      transfer_state: data.state,
      outside_us_flag: "Y",
      bookout_flag: data.bookout_flag,
      vat_country: data.country_vatno,
      tax_code: data.tax_code,
      custom_cleared_us_flag: "Y",
      dates_based_on: data.date_based_on,
      no_agreed_supply_date_flag: data.no_agreed_supply_date_flag,
      transfer_date: data.est_bl_date,
      transfer_date_est_flag: data.transfer_date_est_flag,
      border_crossing_date: data.border_crossing_date,
      border_crossing_date_est_flag: data.border_crossing_date_est_flag,
      supply_start_date: data.supply_start_date,
      supply_end_date: data.supply_end_date,
      transfer_detail_date: data.transfer_detail_date,
      api_60f: data.api_60f,
      est_density_15c_air: data.est_density_15c_air,
      est_density_15c_vac: data.est_density_15c_vac,
      shipping_quantity_mt: data.shipping_quantity_mt,
      shipping_quantity_cm: data.shipping_quantity_cm,
      est_quantity_flag: data.est_quantity_flag,
      show_outturn_qty_flag: data.show_outturn_qty_flag,
      invoicing_unit: data.invoicing_unit,
      operator_user_id: data.operator_id,
      voyage_port_id: data.voyage_port,
      port_type: port_types,
      product_id: data.product_id,
      grade_id: data.grade_id,
      trade_code: trade_code,
      counterparty_id: data.counterparty_id,
      trade_type: data.trade_type,
      unallocated_quantity_mt:
        data.available_quantity_mt - data.shipping_quantity_mt,
      unallocated_quantity_cm:
        data.available_quantity_cm - data.shipping_quantity_cm,
      org_id: data.company_id,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpens(true);
      const res = await fetchData(
        `/api/operation/loss`,
        data.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        refreshAllData();
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpens(false);
      handleLossParcelClose();
    }
  };

  const saveUnlocatedParcelData = async () => {
    const body = {
      voyage_parcel_id: parcel.voyage_parcel_id,
      voyage_port_id: parcel.voyage_port,
      trade_id: parcel.trade_id,
      voyage_id: parcel.voyage_id,
      org_id: parcel.company_id,
      unallocated_quantity_mt: parcel.shipping_quantity_mt,
      unallocated_quantity_cm: parcel.shipping_quantity_cm,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpens(true);
      const res = await fetchData(
        `/api/operation/loss`,
        "DELETE",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        refreshAllData();
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpens(false);
      setOpen(false);
      handleLossParcelClose();
    }
  };

  return (
    <>
      <Modal
        open={openLossParcel}
        aria-labelledby="SpotSale Modal"
        aria-describedby="SpotSale Modal Component"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            maxWidth: "100%",
            maxHeight: "100%",
            overflow: "auto",
            bgcolor: "background.paper",
            border: "1px solid #000",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography variant="subtitle1" gutterBottom>
              Auto parcel {parcel.parcel_number}
            </Typography>

            <IconButton
              sx={{ display: "flex", mt: -1 }}
              onClick={() => {
                setValue(0);
                handleLossParcelClose();
              }}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Box>
          <Divider sx={{ mb: 1 }} />
          <form onSubmit={formik.handleSubmit}>
            <Stack direction={"row"} spacing={1} sx={{ mt: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Voyage
                </InputLabel>
                <Select
                  name="voyage_id"
                  value={formik.values.voyage_id}
                  onChange={(e) => {
                    formik.setFieldValue("voyage_id", e.target.value);
                    setVoyageId(e.target.value);
                  }}
                  disabled={true}
                  size="small"
                  label="Voyage"
                >
                  {voyage?.result.length >= 0 &&
                    voyage?.result.map((item) => (
                      <MenuItem value={item.voyage_id} key={item.voyage_id}>
                        {item.voyage_title}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.voyage_id && Boolean(formik.errors.voyage_id)
                  }
                >
                  {formik.touched.voyage_id && formik.errors.voyage_id}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack spacing={1}>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Trade detail</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      name="trade_term_name"
                      size="small"
                      disabled
                      label="Terms"
                      value={formik.values.trade_term_name}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      name="counterparty_name"
                      size="small"
                      disabled
                      value={formik.values.counterparty_name}
                      label="Counterparty"
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <InputLabel size="small" sx={{ color: " #0B626B" }}>
                      Operator
                    </InputLabel>
                    <Select
                      name="operator_id"
                      value={formik.values.operator_id}
                      onChange={formik.handleChange}
                      size="small"
                      label="Operator"
                    >
                      {operator_user_id?.result.length >= 0 &&
                        operator_user_id?.result.map((item) => (
                          <MenuItem
                            value={item.user_id.toString()}
                            key={item.user_id}
                          >
                            {item.firstname + " " + item.lastname}
                          </MenuItem>
                        ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.operator &&
                        Boolean(formik.errors.operator)
                      }
                    >
                      {formik.touched.operator && formik.errors.operator}
                    </FormHelperText>
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      name="country_origin_name"
                      size="small"
                      value={formik.values.country_origin_name}
                      label="Country of origin"
                      disabled
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      name="product"
                      size="small"
                      disabled
                      label="Product"
                      value={formik.values.product}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      name="grade"
                      size="small"
                      value={formik.values.grade}
                      label="Grade"
                      disabled
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      name="credit"
                      size="small"
                      disabled
                      label="Credit type"
                      value={formik.values.credit_type}
                    />
                  </FormControl>

                  <FormControl fullWidth>
                    <TextField
                      disabled
                      name="bank"
                      size="small"
                      label="Bank"
                      value={formik.values.bank}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      name="financer"
                      size="small"
                      disabled
                      label="Financer"
                      value={formik.values.financer}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <InputLabel size="small" id="load-port-label">
                      Load Port
                    </InputLabel>
                    <Select
                      labelId="load-port-label"
                      size="small"
                      label="Load Port"
                      name="load_port_id"
                      value={formik.values.load_port_id}
                      disabled
                      onChange={formik.handleChange}
                    >
                      {ports?.result.length > 0 &&
                        ports?.result.map((item) => (
                          <MenuItem value={item.port_id} key={item.port_id}>
                            {item.port_name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                  <FormControl fullWidth>
                    <InputLabel size="small" id="discharge-port-label">
                      Discharge Port
                    </InputLabel>
                    <Select
                      labelId="discharge-port-label"
                      size="small"
                      label="Discharge Port"
                      name="discharge_port_id"
                      value={formik.values.discharge_port_id}
                      onChange={formik.handleChange}
                      disabled
                    >
                      {ports?.result.length > 0 &&
                        ports?.result.map((item) => (
                          <MenuItem value={item.port_id} key={item.port_id}>
                            {item.port_name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      name="unit_name"
                      size="small"
                      disabled
                      label="Unit"
                      value={formik.values.unit_name}
                    />
                  </FormControl>
                </Stack>
              </Box>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Parcel Dates</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      name="date_based_on"
                      size="small"
                      disabled
                      label="Date based on"
                      value={formik.values.date_based_on}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        format="YYYY-MM-DD"
                        disabled={formik.values.transfer_date_est_flag === "N"}
                        value={dayjs(formik.values.est_bl_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "est_bl_date",
                            dayjs(newValue).format("YYYY-MM-DD")
                          );
                        }}
                        slotProps={{ textField: { size: "small" } }}
                        label="Bill of landing"
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="transfer_date_est_flag"
                          checked={formik.values.transfer_date_est_flag === "Y"}
                          onChange={(e) => {
                            formik.setFieldValue(
                              "transfer_date_est_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      name="laytime"
                      size="small"
                      type="number"
                      disabled
                      label="laytime"
                      value={formik.values.laytime}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        format="YYYY-MM-DD"
                        value={dayjs(formik.values.border_crossing_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "border_crossing_date",
                            dayjs(newValue).format("YYYY-MM-DD")
                          );
                        }}
                        slotProps={{ textField: { size: "small" } }}
                        label="Border crossing Date"
                        disabled={
                          formik.values.border_crossing_date_est_flag === "N"
                        }
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="border_crossing_date_est_flag"
                          checked={
                            formik.values.border_crossing_date_est_flag === "Y"
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "border_crossing_date_est_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <FormControlLabel
                      sx={{ mt: -1 }}
                      control={
                        <Checkbox
                          size="small"
                          name="no_agreed_supply_date_flag"
                          checked={
                            formik.values.no_agreed_supply_date_flag === "Y"
                              ? true
                              : false
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "no_agreed_supply_date_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="No Agreed Supply Date"
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DateTimePicker
                        label="Supply Start Date"
                        format="YYYY-MM-DD hh:mm:ss"
                        value={dayjs(formik.values.supply_start_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "supply_start_date",
                            dayjs(newValue).format("YYYY-MM-DD hh:mm:ss")
                          );
                        }}
                        viewRenderers={{
                          hours: renderTimeViewClock,
                          minutes: renderTimeViewClock,
                          seconds: renderTimeViewClock,
                        }}
                        slotProps={{
                          textField: {
                            size: "small",
                          },
                        }}
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth></FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth></FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DateTimePicker
                        label="Supply End Date"
                        format="YYYY-MM-DD hh:mm:ss"
                        value={dayjs(formik.values.supply_end_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "supply_end_date",
                            dayjs(newValue).format("YYYY-MM-DD hh:mm:ss")
                          );
                        }}
                        viewRenderers={{
                          hours: null,
                          minutes: null,
                          seconds: null,
                        }}
                        slotProps={{
                          textField: {
                            size: "small",
                          },
                        }}
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        format="YYYY-MM-DD"
                        value={dayjs(formik.values.transfer_detail_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "transfer_detail_date",
                            dayjs(newValue).format("YYYY-MM-DD")
                          );
                        }}
                        slotProps={{ textField: { size: "small" } }}
                        label="Transfer Details From"
                      />
                    </LocalizationProvider>
                  </FormControl>
                </Stack>
              </Box>

              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Required shipping Quantities</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="max_quantity_mt"
                      size="small"
                      label="Max Quantity(MT)"
                      value={parseFloat(formik.values.max_quantity_mt).toFixed(
                        3
                      )}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="allocated_quantity_mt"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Allocated Quantity(MT)"
                      value={parseFloat(
                        formik.values.allocated_quantity_mt
                      ).toFixed(3)}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="available_quantity_mt"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Max Available Quantity(MT)"
                      value={parseFloat(
                        formik.values.available_quantity_mt
                      ).toFixed(3)}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="max_quantity_cm"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Max Quantity(CM)"
                      value={parseFloat(formik.values.max_quantity_cm).toFixed(
                        3
                      )}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="allocated_quantity_cm"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Allocated Quantity(CM)"
                      value={parseFloat(
                        formik.values.allocated_quantity_cm
                      ).toFixed(3)}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="available_quantity_cm"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Max Available Quantity(CM)"
                      value={parseFloat(
                        formik.values.available_quantity_cm
                      ).toFixed(3)}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="shipping_quantity_mt"
                      size="small"
                      label="Shipping quantity(MT)"
                      InputLabelProps={{
                        style: { color: " #0B626B" },
                      }}
                      value={parseFloat(
                        formik.values.shipping_quantity_mt
                      ).toFixed(3)}
                      onBlur={formik.handleBlur}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "shipping_quantity_mt",
                          e.target.value
                        );
                        const total = (
                          formik.values.trade_density
                            ? e.target.value / formik.values.trade_density
                            : 0
                        ).toFixed(3);

                        formik.setFieldValue("shipping_quantity_cm", total);
                      }}
                    />
                    <FormHelperText
                      error={
                        formik.touched.shipping_quantity_mt &&
                        Boolean(formik.errors.shipping_quantity_mt)
                      }
                    >
                      {formik.touched.shipping_quantity_mt &&
                        formik.errors.shipping_quantity_mt}
                    </FormHelperText>
                  </FormControl>

                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="shipping_quantity_cm"
                      size="small"
                      label="Shipping quantity(CM)"
                      InputLabelProps={{
                        style: { color: " #0B626B" },
                      }}
                      value={numberWithCommas(
                        formik.values.shipping_quantity_cm
                      )}
                      onBlur={formik.handleBlur}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "shipping_quantity_cm",
                          e.target.value
                        );
                        const total = (
                          formik.values.trade_density
                            ? e.target.value * formik.values.trade_density
                            : 0
                        ).toFixed(3);

                        formik.setFieldValue(
                          "shipping_quantity_mt",
                          total
                        );
                      }}
                    />
                    <FormHelperText
                      error={
                        formik.touched.shipping_quantity_cm &&
                        Boolean(formik.errors.shipping_quantity_cm)
                      }
                    >
                      {formik.touched.shipping_quantity_cm &&
                        formik.errors.shipping_quantity_cm}
                    </FormHelperText>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControlLabel
                      control={
                        <Checkbox
                          disabled={
                            parcel.port_type === "STS_load" ||
                            parcel.port_type === "STS_disport"
                              ? true
                              : false
                          }
                          size="small"
                          name="est_quantity_flag"
                          checked={
                            formik.values.est_quantity_flag === "Y"
                              ? true
                              : false
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "est_quantity_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                      readOnly={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                    />
                  </FormControl>
                </Stack>
              </Box>
            </Stack>
            <Stack spacing={1}>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Invoicing Detail</legend>
                <Stack direction="column" spacing={1}>
                  <FormControl>
                    <InputLabel sx={{ color: " #0B626B" }} size="small">
                      Invoice Unit
                    </InputLabel>
                    <Select
                      size="small"
                      disabled={true}
                      label="Invoice Unit"
                      name="invoicing_unit"
                      value={formik.values.invoicing_unit}
                      onChange={formik.handleChange}
                    >
                      {invoicing_unit?.result.length > 0 &&
                        invoicing_unit?.result.map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.invoicing_unit &&
                        Boolean(formik.errors.invoicing_unit)
                      }
                    >
                      {formik.touched.invoicing_unit &&
                        formik.errors.invoicing_unit}
                    </FormHelperText>
                  </FormControl>
                </Stack>
              </Box>
            </Stack>
            <Divider sx={{ mt: 1, mb: 1 }} />
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Stack direction={"row"} spacing={1}>
                {parcel &&
                parcel.method === "PUT" &&
                session?.user?.menuitems.filter(
                  (f) => f.menu_name === "Operation"
                )[0].permission === "Edit" ? (
                  <>
                    <Button
                      variant="outlined"
                      color="error"
                      onClick={async () => {
                        setOpen(true);
                      }}
                    >
                      Unloss
                    </Button>
                  </>
                ) : null}
              </Stack>

              <Stack direction={"row"} spacing={1}>
                <Button variant="outlined" onClick={handleLossParcelClose}>
                  Close
                </Button>
                {parcel.method === "POST" &&
                session &&
                session?.user?.menuitems.filter(
                  (f) => f.menu_name === "Operation"
                )[0].permission === "Edit" ? (
                  <Button variant="contained" type="submit" color="dark">
                    Save
                  </Button>
                ) : null}
              </Stack>
            </Box>
          </form>
        </Box>
      </Modal>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Unloss
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">are you sure unloss!</Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                saveUnlocatedParcelData();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              Yes
            </Button>
            <Button onClick={handleClickClose} variant="outlined">
              No
            </Button>
          </Box>
        </Box>
      </Modal>

      <Modal open={opens}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
