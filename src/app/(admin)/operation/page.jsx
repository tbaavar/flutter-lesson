"use client";
import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import CircularProgress from "@mui/material/CircularProgress";
import MyAlert from "@/components/Myalert";
import CustomNoRowsOverlay from "@/components/NoRecord";
import exportToExcel from "@/components/ExcelExport";
import CustomTreeItem from "@/components/CustomTreeItem";
import PlusSquare from "@/components/PlusSquare";
import MinusSquare from "@/components/MinusSquare";
import dayjs from "dayjs";
import Chip from "@mui/material/Chip";
import GridViewIcon from "@mui/icons-material/GridView";
import ListAltOutlinedIcon from "@mui/icons-material/ListAltOutlined";
import SaveAltIcon from "@mui/icons-material/SaveAlt";
import { IconDatabase } from "@tabler/icons-react";
import { numberWithCommas } from "@/lib/numberWithCommas";
import {
  AntTabs,
  AntTab,
  TradeTabs,
  tradeProps,
} from "@/components/CustomTabControl";
import {
  Box,
  Button,
  Card,
  ButtonGroup,
  Stack,
  TextField,
  Paper,
  FormControl,
  Grid,
  Typography,
  Link,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { TreeView } from "@mui/x-tree-view";
import ShareLocationOutlinedIcon from "@mui/icons-material/ShareLocationOutlined";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import InsertLinkIcon from "@mui/icons-material/InsertLink";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import FolderRoundedIcon from "@mui/icons-material/FolderRounded";
import useSWR, { mutate } from "swr";
import VoyagePage from "./voyage/page";
import VoyageParcelPage from "./voyageparcel/page";
import LossParcelPage from "./lossparcel/page";
import Port from "./port/page";

export default function OperationPage() {
  const { data: session } = useSession();

  const [openVoyage, setVoyageOpen] = React.useState(false);
  const [openSpotSale, setSpotsaleOpen] = React.useState(false);
  const [openLossParcel, setLossParcel] = React.useState(false);
  const [filterText, setFilterText] = React.useState("");
  const [value1, setValue1] = React.useState(0);
  const [parcel, setParcel] = React.useState("");
  const [newVoyage, setNewVoyage] = React.useState({});
  const [openPort, setOpenPort] = React.useState(false);
  const [arr, setArr] = React.useState([]);
  const [port, setPort] = React.useState("");
  const [switches, setSwitches] = React.useState(true);
  const [iconColor, setIconColor] = React.useState("black");
  const [iconColor1, setIconColor1] = React.useState("#0B626B");
  const [displayText, setDisplayText] = React.useState("Voyage");
  const [filterText1, setFilterText1] = React.useState("");
  const [filterText2, setFilterText2] = React.useState("");
  const [filt, setFilt] = React.useState("All");

  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };
  const handleFilterChange1 = (e) => {
    setFilterText1(e.target.value);
  };
  const handleFilterChange2 = (e) => {
    setFilterText2(e.target.value);
  };
  const handlePortOpen = () => setOpenPort(true);
  const handlePortClose = () => setOpenPort(false);
  const handleVoyageOpen = () => setVoyageOpen(true);
  const handleVoyageClose = () => setVoyageOpen(false);
  const handleSpotSaleOpen = () => setSpotsaleOpen(true);
  const handleSpotSaleClose = () => setSpotsaleOpen(false);

  const handleLossParcelOpen = () => setLossParcel(true);
  const handleLossParcelClose = () => setLossParcel(false);

  const handleSwitchesFalse = () => {
    setIconColor1("gray"); // Set icon color to gray
    setIconColor("#0B626B");
    setSwitches(false);
    setDisplayText("Parcel");
  };

  const handleSwitchesTrue = () => {
    setIconColor1("#0B626B"); // Set icon color to blue
    setIconColor("gray");
    setSwitches(true);
    setDisplayText("Voyage");
  };
  const handleChange = (event, newValue) => {
    setValue1(newValue);
  };

  const handleChangeFilt = (event) => {
    setFilt(event.target.value);
  };

  const { data: unlallocatedTrade } = useSWR(
    [`/api/operation/parcel/${session?.user.user_id}`, session?.user?.token],
    fetcher,
    { refreshInterval: 1000 }
  );

  const { data: unlallocatedParcel } = useSWR(
    [
      `/api/operation/unlocatedparcel/${session?.user.user_id}`,
      session?.user?.token,
    ],
    fetcher,
    { refreshInterval: 1000 }
  );

  const { data: parcelgrid } = useSWR(
    [`/api/operation/list/${session?.user.user_id}`, session?.user?.token],
    fetcher,
    { refreshInterval: 1000 }
  );
  const {
    data: voyage,
    error,
    isLoading,
  } = useSWR(
    [`/api/operation/voyage//${session?.user.user_id}`, session?.user?.token],
    fetcher,
    {
      refreshInterval: 1000,
    }
  );

  const refreshAllData = () => {
    const userId = session?.user.user_id;

    // Mutate each key to trigger a revalidation
    mutate(`/api/operation/parcel/${userId}`);
    mutate(`/api/operation/unlocatedparcel/${userId}`);
    mutate(`/api/operation/list/${userId}`);
    mutate(`/api/operation/voyage/${userId}`);
  };

  const parcelcolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 50,
    },
    {
      field: "parcel_number",
      renderHeader: () => <strong>{"Parcel number"}</strong>,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          const rowID = currentRow.voyage_parcel_id;
          filteredRows?.map((item) => {
            item.ports.map((port) => {
              port.parcels.map((parcel) => {
                if (parcel.voyage_parcel_id == rowID) {
                  setParcel({
                    voyage_parcel_id: parcel.voyage_parcel_id,
                    trade_id: parcel.trade_id,
                    trade_type:
                      parcel.port_type === "STS_load"
                        ? "Spot_Sale"
                        : parcel.port_type === "STS_disport"
                        ? "Spot_Purchase"
                        : parcel.trade_type,
                    trade_type_name: parcel.trade_type_name,
                    trade_term: parcel.terms,
                    trade_term_name: parcel.term_name,
                    company_id: parcel.company_id,
                    company_name: parcel.company_name,
                    counterparty_id: parcel.counterparty_id,
                    counterparty_name: parcel.counterparty_name,
                    credit_type: parcel.credit_type,
                    bank_id: parcel.bank_id,
                    bank: parcel.bank,
                    financer_id: parcel.finance_user_id,
                    financer: parcel.financer_name,
                    operator_id: parcel.operator_user_id,
                    operator: parcel.operator_name,
                    product_id: parcel.product_id,
                    product: parcel.product_name,
                    grade_id: parcel.grade_id,
                    grade: parcel.grade_name,
                    country_origin: parcel.country_origin,
                    country_origin_name: parcel.country_origin_name,
                    load_port_id: parcel.load_port_id,
                    discharge_port_id: parcel.discharge_port_id,
                    date_based_on: parcel.dates_based_on,
                    laytime: parcel.laytime,
                    est_bl_date: parcel.transfer_date,
                    unit: parcel.units,
                    unit_name: parcel.unit_name,
                    max_quantity_mt: parcel.max_quantity_mt,
                    max_quantity_cm: parcel.max_quantity_cm,
                    allocated_quantity_mt: parcel.allocated_quantity_mt,
                    allocated_quantity_cm: parcel.allocated_quantity_cm,
                    available_quantity_mt: parcel.available_quantity_mt,
                    available_quantity_cm: parcel.available_quantity_cm,
                    lc_open_date: parcel.lc_open_date,
                    bookout_flag: parcel.bookout_flag,
                    vat_type: parcel.vat_type,
                    country_vatno: parcel.country_vatno,
                    tax_code: parcel.tax_code,
                    transfer_date_est_flag: parcel.transfer_date_est_flag,
                    border_crossing_date: parcel.border_crossing_date,
                    border_crossing_date_est_flag:
                      parcel.border_crossing_date_est_flag,
                    no_agreed_supply_date_flag:
                      parcel.no_agreed_supply_date_flag,
                    supply_start_date: parcel.supply_start_date,
                    supply_end_date: parcel.supply_end_date,
                    transfer_detail_date: parcel.transfer_detail_date,
                    trade_density: parcel.est_density,
                    actual_density: parcel.est_density,
                    api_60f: parcel.api_60f,
                    est_density_15c_air: parcel.est_density_15c_air,
                    est_density_15c_vac: parcel.est_density_15c_vac,
                    shipping_quantity: parcel.shipping_quantity_mt,
                    shipping_quantity_mt: parcel.shipping_quantity_mt,
                    shipping_quantity_cm: parcel.shipping_quantity_cm,
                    shipping_quantity_b: parcel.shipping_quantity_b,
                    est_quantity_flag: parcel.est_quantity_flag,
                    show_outturn_qty_flag: parcel.show_outturn_qty_flag,
                    invoicing_unit: parcel.invoicing_unit,
                    voyage_id: parcel.voyage_id,
                    voyage_port: parcel.voyage_port_id,
                    port_type: parcel.port_type,
                    method: "PUT",
                    state: "unlocated",
                    parcel_number: parcel.parcel_number,
                  });
                }
              });
            });
          });
          handleSpotSaleOpen();
        };
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Link onClick={handleEdit} variant="h7">
              <Typography variant="h7">{params.row.parcel_number}</Typography>
            </Link>
            <Typography sx={{ fontSize: "12px" }}>
              <strong> {"T- " + params.row.trade_id} </strong>
            </Typography>
          </Box>
        );
      },

      width: 150,
    },
    {
      field: "terms",
      renderHeader: () => <strong>{"Terms"}</strong>,
      width: 80,
    },

    {
      field: "shipping_quantity_mt",
      renderHeader: () => <strong>{"Quantity"}</strong>,
      width: 130,
      renderCell: (params) => {
        if (
          parseInt(params.row.shipping_quantity_mt) <= 5 &&
          parseInt(params.row.shipping_quantity_mt) > 1
        ) {
          return (
            <Chip
              variant="filled"
              color="high"
              label={
                numberWithCommas(params.row.shipping_quantity_mt) +
                " " +
                params.row.units
              }
            />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="regular"
              label={
                numberWithCommas(params.row.shipping_quantity_mt) +
                " " +
                params.row.units
              }
            />
          );
      },
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Latest day"}</strong>,
      width: 100,
      renderCell: (params) => {
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
    },

    {
      field: "transfer_date",
      renderHeader: () => <strong>{"Bill of lading"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.transfer_date).format("YYYY-MM-DD"),
      width: 130,
      renderCell: (params) => {
        if (
          parseInt(params.row.transfer_date) <= 5 &&
          parseInt(params.row.transfer_date) > 1
        ) {
          return (
            <Chip
              variant="filled"
              color="high"
              label={dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
            />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
            />
          );
      },
    },
    {
      field: "border_crossing_date",
      renderHeader: () => <strong>{"Border crossing"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.border_crossing_date).format("YYYY-MM-DD"),
      width: 130,
      renderCell: (params) => {
        if (
          parseInt(params.row.border_crossing_date) <= 5 &&
          parseInt(params.row.border_crossing_date) > 1
        ) {
          return (
            <Chip
              variant="filled"
              color="success"
              label={dayjs(params.row.border_crossing_date).format(
                "YYYY-MM-DD"
              )}
            />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="regular"
              label={dayjs(params.row.border_crossing_date).format(
                "YYYY-MM-DD"
              )}
            />
          );
      },
    },
    {
      header: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      flex: 1,
      renderCell: (params) => (
        <div>
          <Typography variant="h7">{params.row.product_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </div>
      ),
    },
  ];

  const unlallocatedTradecolumns = [
    {
      field: "#",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 70,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          setParcel({
            voyage_parcel_id: "",
            trade_id: currentRow.trade_id,
            trade_type: currentRow.trade_type,
            trade_type_name: currentRow.trade_type_name,
            trade_term: currentRow.terms,
            trade_term_name: currentRow.term_name,
            company_id: currentRow.company_id,
            company_name: currentRow.company_name,
            counterparty_id: currentRow.counterparty_id,
            counterparty_name: currentRow.counterparty_name,
            credit_type: currentRow.credit_type,
            bank_id: currentRow.bank_id,
            bank: currentRow.bank,
            financer_id: currentRow.finance_user_id,
            financer: currentRow.financer_name,
            operator_id: currentRow.operator_user_id,
            operator: currentRow.operator_name,
            product_id: currentRow.product_id,
            product: currentRow.product_name,
            grade_id: currentRow.grade_id,
            grade: currentRow.grade_name,
            country_origin: currentRow.country_origin,
            country_origin_name: currentRow.country_origin_name,
            load_port_id: currentRow.load_port_id,
            discharge_port_id: currentRow.discharge_port_id,
            date_based_on: currentRow.dates_based_on,
            laytime: currentRow.laytime,
            est_bl_date: currentRow.est_bl_date,
            unit: currentRow.units,
            unit_name: currentRow.unit_name,
            max_quantity_mt: currentRow.quantity_mt,
            allocated_quantity_mt: "0",
            available_quantity_mt: currentRow.quantity_mt,
            max_quantity_cm: currentRow.quantity_cm,
            allocated_quantity_cm: "0",
            available_quantity_cm: currentRow.quantity_cm,
            // shipping_quantity: "",
            lc_open_date: dayjs().format("YYYY-MM-DD"),
            bookout_flag: "N",
            vat_type: "",
            country_vatno: "",
            tax_code: "",
            transfer_date_est_flag: "Y",
            border_crossing_date: dayjs().format("YYYY-MM-DD"),
            border_crossing_date_est_flag: "Y",
            no_agreed_supply_date_flag: "N",
            supply_start_date: dayjs().format("YYYY-MM-DD hh:mm:ss"),
            supply_end_date: dayjs().format("YYYY-MM-DD hh:mm:ss"),
            transfer_detail_date: dayjs().format("YYYY-MM-DD"),
            trade_density: currentRow.est_density,
            actual_density: currentRow.est_density,
            api_60f: "",
            est_density_15c_air: "",
            est_density_15c_vac: "",
            shipping_quantity_mt: "0",
            shipping_quantity_cm: "0",
            est_quantity_flag: "Y",
            show_outturn_qty_flag: "N",
            invoicing_unit: currentRow.units,
            voyage_id: "",
            voyage_port: "",
            port_type: "",
            method: "POST",
            state: "new",
          });

          handleSpotSaleOpen();
        };
        return (
          <GridActionsCellItem
            icon={<InsertLinkIcon />}
            label="Edit"
            color="secondary"
            onClick={handleEdit}
          />
        );
      },
    },

    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade Id"}</strong>,
      renderCell: (params) => (
        <div>
          <Typography variant="h7">{"T- " + params.row.trade_id}</Typography>
        </div>
      ),
      width: 120,
    },
    {
      field: "trade_type_name",
      renderHeader: () => <strong>{"Trade type"}</strong>,
      renderCell: (params) => (
        <div>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}

          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.counterparty_name} </strong>
          </Typography>
        </div>
      ),
      width: 180,
    },

    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <div>
          <Typography variant="h7">{params.row.product_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </div>
      ),
      width: 200,
    },
    {
      field: "quantity_mt",
      renderHeader: () => <strong>{"Quantity(MT)"}</strong>,

      renderCell: (params) => (
        <div>
          <Typography variant="h7">
            {parseFloat(params.row.quantity_mt).toFixed(5)}
          </Typography>
        </div>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "quantity_cm",
      renderHeader: () => <strong>{"Quantity(CM)"}</strong>,

      renderCell: (params) => (
        <div>
          <Typography variant="h7">
            {parseFloat(params.row.quantity_cm).toFixed(5)}
          </Typography>
        </div>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "est_density",
      renderHeader: () => <strong>{"Density"}</strong>,

      renderCell: (params) => (
        <strong>
          <Typography variant="h7">{params.row.est_density}</Typography>
        </strong>
      ),
      width: 80,
    },
  ];

  const unlallocatedParcelColumns = [
    {
      field: "#",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 60,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;

          setParcel({
            voyage_parcel_id: "",
            trade_id: currentRow.trade_id,
            trade_type: currentRow.trade_type,
            trade_type_name: currentRow.trade_type_name,
            trade_term: currentRow.terms,
            trade_term_name: currentRow.term_name,
            company_id: currentRow.company_id,
            company_name: currentRow.company_name,
            counterparty_id: currentRow.counterparty_id,
            counterparty_name: currentRow.counterparty_name,
            credit_type: currentRow.credit_type,
            bank_id: currentRow.bank_id,
            bank: currentRow.bank,
            financer_id: currentRow.finance_user_id,
            financer: currentRow.financer_name,
            operator_id: currentRow.operator_user_id,
            operator: currentRow.operator_name,
            product_id: currentRow.product_id,
            product: currentRow.product_name,
            grade_id: currentRow.grade_id,
            grade: currentRow.grade_name,
            country_origin: currentRow.country_origin,
            country_origin_name: currentRow.country_origin_name,
            load_port_id: currentRow.load_port_id,
            discharge_port_id: currentRow.discharge_port_id,
            date_based_on: currentRow.dates_based_on,
            laytime: currentRow.laytime,
            est_bl_date: currentRow.est_bl_date,
            unit: currentRow.units,
            unit_name: currentRow.unit_name,
            max_quantity_mt: currentRow.max_quantity_mt,
            max_quantity_cm: currentRow.max_quantity_cm,
            allocated_quantity_mt: currentRow.allocated_quantity_mt,
            allocated_quantity_cm: currentRow.allocated_quantity_cm,
            available_quantity_mt: currentRow.available_quantity_mt,
            available_quantity_cm: currentRow.available_quantity_cm,
            shipping_quantity: "",
            lc_open_date: dayjs().format("YYYY-MM-DD"),
            bookout_flag: "N",
            vat_type: "",
            country_vatno: "",
            tax_code: "",
            transfer_date_est_flag: "Y",
            border_crossing_date: dayjs().format("YYYY-MM-DD"),
            border_crossing_date_est_flag: "Y",
            no_agreed_supply_date_flag: "N",
            supply_start_date: dayjs().format("YYYY-MM-DD hh:mm:ss"),
            supply_end_date: dayjs().format("YYYY-MM-DD hh:mm:ss"),
            transfer_detail_date: dayjs().format("YYYY-MM-DD"),
            trade_density: currentRow.est_density,
            actual_density: currentRow.est_density,
            api_60f: "",
            est_density_15c_air: "",
            est_density_15c_vac: "",
            shipping_quantity_mt: "0",
            shipping_quantity_cm: "0",
            shipping_quantity_b: "0",
            est_quantity_flag: "Y",
            show_outturn_qty_flag: "N",
            invoicing_unit: currentRow.units,
            voyage_id: currentRow.voyage_id,
            voyage_port: "",
            port_type: "",
            method: "POST",
            state: "unlocated",
          });

          handleSpotSaleOpen();
        };
        return (
          <GridActionsCellItem
            icon={<InsertLinkIcon />}
            label="Edit"
            color="secondary"
            onClick={handleEdit}
          />
        );
      },
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade Id"}</strong>,
      renderCell: (params) => (
        <div>
          <Typography variant="h7">{"T- " + params.row.trade_id}</Typography>
        </div>
      ),
      width: 110,
    },
    {
      field: "trade_type_name",
      renderHeader: () => <strong>{"Trade type"}</strong>,
      renderCell: (params) => (
        <div>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}

          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.counterparty_name} </strong>
          </Typography>
        </div>
      ),
      width: 180,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <div>
          <Typography variant="h7">{params.row.product_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </div>
      ),
      width: 200,
    },
    {
      field: "available_quantity_mt",
      renderHeader: () => <strong>{"Quantity(MT)"}</strong>,

      renderCell: (params) => (
        <div>
          <Typography variant="h7">
            {params.row.available_quantity_mt}
          </Typography>
        </div>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "available_quantity_cm",
      renderHeader: () => <strong>{"Quantity(CM)"}</strong>,

      renderCell: (params) => (
        <div>
          <Typography variant="h7">
            {params.row.available_quantity_cm}
          </Typography>
        </div>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "est_density",
      renderHeader: () => <strong>{"Density"}</strong>,

      renderCell: (params) => (
        <strong>
          <Typography variant="h7">{params.row.est_density}</Typography>
        </strong>
      ),
      width: 80,
    },
    {
      field: "loss",
      renderHeader: () => <strong>{"Loss"}</strong>,
      width: 20,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;

          setParcel({
            voyage_parcel_id: "",
            trade_id: currentRow.trade_id,
            trade_type: currentRow.trade_type,
            trade_type_name: currentRow.trade_type_name,
            trade_term: currentRow.terms,
            trade_term_name: currentRow.term_name,
            company_id: currentRow.company_id,
            company_name: currentRow.company_name,
            counterparty_id: currentRow.counterparty_id,
            counterparty_name: currentRow.counterparty_name,
            credit_type: currentRow.credit_type,
            bank_id: currentRow.bank_id,
            bank: currentRow.bank,
            financer_id: currentRow.finance_user_id,
            financer: currentRow.financer_name,
            operator_id: currentRow.operator_user_id,
            operator: currentRow.operator_name,
            product_id: currentRow.product_id,
            product: currentRow.product_name,
            grade_id: currentRow.grade_id,
            grade: currentRow.grade_name,
            country_origin: currentRow.country_origin,
            country_origin_name: currentRow.country_origin_name,
            load_port_id: currentRow.load_port_id,
            discharge_port_id: currentRow.discharge_port_id,
            date_based_on: currentRow.dates_based_on,
            laytime: currentRow.laytime,
            est_bl_date: currentRow.est_bl_date,
            unit: currentRow.units,
            unit_name: currentRow.unit_name,
            max_quantity_mt: currentRow.max_quantity_mt,
            max_quantity_cm: currentRow.max_quantity_cm,
            allocated_quantity_mt: currentRow.allocated_quantity_mt,
            allocated_quantity_cm: currentRow.allocated_quantity_cm,
            available_quantity_mt: currentRow.available_quantity_mt,
            available_quantity_cm: currentRow.available_quantity_cm,
            shipping_quantity: "",
            lc_open_date: dayjs().format("YYYY-MM-DD"),
            bookout_flag: "N",
            vat_type: "",
            country_vatno: "",
            tax_code: "",
            transfer_date_est_flag: "Y",
            border_crossing_date: dayjs().format("YYYY-MM-DD"),
            border_crossing_date_est_flag: "Y",
            no_agreed_supply_date_flag: "N",
            supply_start_date: dayjs().format("YYYY-MM-DD hh:mm:ss"),
            supply_end_date: dayjs().format("YYYY-MM-DD hh:mm:ss"),
            transfer_detail_date: dayjs().format("YYYY-MM-DD"),
            trade_density: currentRow.est_density,
            actual_density: currentRow.est_density,
            api_60f: "",
            est_density_15c_air: "",
            est_density_15c_vac: "",
            shipping_quantity_mt: currentRow.available_quantity_mt,
            shipping_quantity_cm: currentRow.available_quantity_cm,
            shipping_quantity_b: "",
            est_quantity_flag: "Y",
            show_outturn_qty_flag: "N",
            invoicing_unit: currentRow.units,
            voyage_id: currentRow.voyage_id,
            voyage_port: "",
            port_type: "",
            method: "POST",
            state: "loss",
          });

          handleLossParcelOpen();
        };
        return (
          <GridActionsCellItem
            icon={<SaveAltIcon />}
            label="Edit"
            color="error"
            onClick={handleEdit}
          />
        );
      },
    },
  ];

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  const parcelrows =
    parcelgrid?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];

  const voyageRows = voyage?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const filteredRows = voyageRows.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText.toLowerCase()))
    )
  );

  const filteredRowsList = parcelrows.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText.toLowerCase()))
    )
  );

  const rows = unlallocatedTrade?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const UnlocatedParcelrows = unlallocatedParcel?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const filteredRows1 = rows.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText1.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText1.toLowerCase()))
    )
  );

  const filteredRows2 = UnlocatedParcelrows.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText2.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText2.toLowerCase()))
    )
  );

  const handleExport = (params) => {
    const rows = parcelrows.map((item) => ({
      id: item.id,
      voyage_id: item.voyage_id,
      voyage_port_name: item.voyage_port_name,
      parcel_number: item.parcel_number,
      trade_id: "T-" + item.trade_id,
      trade_type_name: item.trade_type_name,
      company_name: item.company_name,
      counterparty_name: item.counterparty_name,
      term_name: item.term_name,
      vessel_name: item.vessel_name,
      credit_type: item.credit_type,
      product_name: item.product_name,
      grade_name: item.grade_name,
      est_density: item.est_density,
      est_density_15c_air: item.est_density_15c_air,
      est_density_15c_vac: item.est_density_15c_vac,
      api_60f: item.api_60f,
      shipping_quantity_mt: item.shipping_quantity_mt,
      est_quantity_flag: item.est_quantity_flag,
      unit_name: item.unit_name,
      transfer_date: item.transfer_date,
      transfer_date_est_flag: item.transfer_date_est_flag,
      border_crossing_date: item.border_crossing_date,
      border_crossing_date_est_flag: item.border_crossing_date_est_flag,
      supply_start_date: item.supply_start_date,
      supply_end_date: item.supply_end_date,
      transfer_detail_date: item.transfer_detail_date,
      operator_name: item.operator_name,
      financer_name: item.financer_name,
    }));
    const ParcelColumn = [
      {
        header: "#",
      },
      {
        header: "voyage_id",
      },
      {
        header: "voyage_port",
      },
      {
        header: "parcel_number",
      },
      {
        header: "trade_id",
      },
      {
        header: "trade_type_name",
      },
      {
        header: "company",
      },
      {
        header: "counterparty",
      },
      {
        header: "term",
      },
      {
        header: "transportation",
      },
      {
        header: "credit",
      },
      {
        header: "product",
      },
      {
        header: "grade",
      },
      {
        header: "est_density",
      },
      {
        header: "est_density_15c_air",
      },
      {
        header: "est_density_15c_vac",
      },
      {
        header: "api_60f",
      },
      {
        header: "shipping_quantity_mt",
      },
      {
        header: "est_quantity_flag",
      },
      {
        header: "unit_name",
      },
      {
        header: "transfer_date",
      },
      {
        header: "transfer_date_est_flag",
      },
      {
        header: "border_crossing_date",
      },
      {
        header: "border_crossing_date_est_flag",
      },
      {
        header: "supply_start_date",
      },
      {
        header: "supply_end_date",
      },
      {
        header: "transfer_detail_date",
      },
      {
        header: "operator_name",
      },
      {
        header: "financer_name",
      },
    ];
    exportToExcel(
      rows,
      ParcelColumn,
      `ParcelReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportList = (params) => {
    const rows1 = filteredRows1.map((item) => ({
      id: item.id,
      trade_id: "T-" + item.trade_id,
      trade_type_name: item.trade_type_name,
      company_name: item.company_name,
      counterparty_name: item.counterparty_name,
      term_name: item.term_name,
      operator_name: item.operator_name,
      financer_name: item.financer_name,
      country_origin_name: item.country_origin_name,
      vessel_name: item.vessel_name,
      dates_based_on: item.dates_based_on,
      date_range_from: item.date_range_from,
      date_range_to: item.date_range_to,
      est_bl_date: item.est_bl_date,
      credit_type: item.credit_type,
      product_name: item.product_name,
      grade_name: item.grade_name,
      est_density: item.est_density,
      quantity: item.quantity,
      unit_name: item.unit_name,
    }));
    const TradeColumn = [
      {
        header: "#",
      },
      {
        header: "trade_id",
      },
      {
        header: "trade_type",
      },
      {
        header: "company",
      },
      {
        header: "counterparty",
      },
      {
        header: "term",
      },
      {
        header: "operator",
      },
      {
        header: "financer",
      },
      {
        header: "country_origin",
      },
      {
        header: "transportation",
      },
      {
        header: "dates_based_on",
      },
      {
        header: "date_range_from",
      },
      {
        header: "date_range_to",
      },
      {
        header: "est_bl_date",
      },
      {
        header: "credit",
      },
      {
        header: "product",
      },
      {
        header: "grade",
      },
      {
        header: "est_density",
      },
      {
        header: "quantity",
      },
      {
        header: "unit",
      },
    ];

    exportToExcel(
      rows1,
      TradeColumn,
      `UnallocatedTradeReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  return (
    <>
      <ToastContainer />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Stack
            direction={"row"}
            spacing={1}
            justifyContent="space-between"
            alignContent={"center"}
            sx={{ mb: 1 }}
          >
            <FormControl fullWidth>
              <TextField
                variant="outlined"
                size="small"
                placeholder="Search..."
                value={filterText}
                onChange={handleFilterChange}
              />
            </FormControl>
            <FormControl fullWidth>
              <Select
                labelId="filt"
                name="filt"
                size="small"
                sx={{ maxWidth: 150 }}
                value={filt}
                onChange={handleChangeFilt}
              >
                <MenuItem value={"All"}>All</MenuItem>
                <MenuItem value={"Closed_voyage"}>Closed Voyage</MenuItem>
                <MenuItem value={"In_Voyage"}>In Voyage</MenuItem>
                <MenuItem value={"Pre_voyage"}>Pre Voyage</MenuItem>
              </Select>
            </FormControl>

            {session &&
            session?.user?.menuitems.filter(
              (f) => f.menu_name === "Operation"
            )[0].permission === "Edit" ? (
              <Box sx={{ justifyContent: "space-between", display: "flex" }}>
                <Button
                  variant="text"
                  size="small"
                  startIcon={<AddSharpIcon />}
                  onClick={() => {
                    setPort({
                      voyage_port_id: null,
                      voyage_id: "",
                      port_id: "",
                      port_name: "",
                      port_type: "Load_port",
                      agent_id: "",
                      superveyor_id: "",
                      superintendent_id: "",
                      method: "POST",
                    });
                    handlePortOpen();
                  }}
                >
                  Port
                </Button>
                <Button
                  variant="text"
                  size="small"
                  sx={{ justifyContent: "flex-end" }}
                  onClick={() => {
                    setNewVoyage({
                      voyage_id: null,
                      intended_vessel: "",
                      product_id: "",
                      voyage_title: "",
                      voyage_status: "",
                      charter_type: "",
                      charter_party_date: dayjs().format("YYYY-MM-DD"),
                      insurance_required_flag: "N",
                      insurance_externally_flag: "N",
                      insured_description: "",
                      insurance_status: "",
                      quantity_discrepancy_flag: "N",
                      quantity_discrepancy_comment: "",
                      voyage_attachment_id: "",
                      voyage_comment: "",
                      org_id: "",
                      method: "POST",
                    });
                    setArr([]);
                    handleVoyageOpen();
                  }}
                  startIcon={<AddSharpIcon />}
                >
                  Voyage
                </Button>
              </Box>
            ) : null}
          </Stack>
          <Card sx={{ mt: 1.5, borderRadius: 0 }}>
            <Stack
              container="true"
              direction="row"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography
                variant="h5"
                sx={{ justifyContent: "flex-start", m: 1 }}
              >
                {displayText}
              </Typography>
              <ButtonGroup variant="">
                <Button
                  style={{ width: "25px", color: iconColor }}
                  onClick={handleSwitchesFalse}
                >
                  <ListAltOutlinedIcon className="largeIcon" />
                </Button>
                <Button
                  style={{ width: "25px", color: iconColor1 }}
                  onClick={handleSwitchesTrue}
                >
                  <GridViewIcon />
                </Button>
              </ButtonGroup>
            </Stack>
            <Stack direction="row" spacing={1}>
              <div
                style={{ overflowX: "auto", maxWidth: "100%", height: "77vh" }}
              >
                <Paper
                  sx={{
                    minWidth: "90vw",
                    overflow: "hidden",
                    // m: 1,
                  }}
                >
                  {switches === true ? (
                    <TreeView
                      aria-label="customized"
                      defaultCollapseIcon={<MinusSquare />}
                      defaultexpandeditems={["1", "2", "3"]}
                      defaultExpandIcon={<PlusSquare />}
                      sx={{
                        overflowX: "hidden",
                        mt: 1,

                        minHeight: "90vh",
                      }}
                    >
                      {console.log("weqeqweqwewqeqweq", filteredRows)}
                      {filteredRows?.length > 0 &&
                        filteredRows?.filter((f) => f.voyage_status === (filt==="All"? f.voyage_status:filt) ).map((item, i) => (
                          <CustomTreeItem
                            type="button"
                            nodeIcon={
                              <FolderRoundedIcon sx={{ color: "#ffac33" }} />
                            }
                            onClick={() => {
                              setNewVoyage({
                                voyage_id: item.voyage_id,
                                intended_vessel: item.intended_vessel,
                                product_id: item.product_id,
                                voyage_title: item.voyage_title,
                                voyage_status: item.voyage_status,
                                charter_type: item.charter_type,
                                charter_party_date: item.charter_party_date,
                                voyage_attachment_id: item.voyage_attachment_id,
                                insurance_required_flag:
                                  item.incurance_required_flag,
                                insurance_externally_flag:
                                  item.insurance_externally_flag,
                                insured_description: item.insured_description,
                                insurance_status: item.insurance_status,
                                quantity_discrepancy_flag:
                                  item.quantity_discrepancy_flag,
                                quantity_discrepancy_comment:
                                  item.quantity_discrepancy_comment,
                                voyage_comment: item.voyage_comment,
                                org_id: item.org_id,

                                method: "PUT",
                              });
                              setArr(item.documents);

                              handleVoyageOpen();
                            }}
                            nodeId={`1+${item.voyage_id.toString()}`}
                            
                            label={
                              item.org_name +
                              " - " +
                              item.voyage_status +
                              " - " +
                              item.voyage_title
                            }
                            key={i}
                          >
                            {item.ports?.length > 0 &&
                              item.ports.map((port, j) => (
                                <CustomTreeItem
                                  type="button"
                                  nodeIcon={
                                    <ShareLocationOutlinedIcon
                                      sx={{ color: "#f44336" }}
                                    />
                                  }
                                  onClick={() => {
                                    setPort({
                                      voyage_port_id: port.voyage_port_id,
                                      voyage_id: item.voyage_id,
                                      port_id: port.port_id,
                                      port_name: port.port_name,
                                      port_type: port.port_type,
                                      agent_id: port.agent_id,
                                      superveyor_id: port.superveyor_id,
                                      superintendent_id: port.superintendent_id,
                                      method: "PUT",
                                    });

                                    port.port_type === "Load_port" ||
                                    port.port_type === "Discharge_port"
                                      ? handlePortOpen()
                                      : null;
                                  }}
                                  nodeId={`2+${port.voyage_port_id.toString()}`}
                                  label={port?.port_title}
                                  key={j}
                                >
                                  {port.parcels?.length > 0 &&
                                    port.parcels.map((parcel, k) => (
                                      <CustomTreeItem
                                        type="button"
                                        onClick={() => {
                                          setParcel({
                                            voyage_parcel_id:
                                              parcel.voyage_parcel_id,
                                            trade_id: parcel.trade_id,
                                            trade_type:
                                              parcel.port_type === "STS_load"
                                                ? "Spot_Sale"
                                                : parcel.port_type ===
                                                  "STS_disport"
                                                ? "Spot_Purchase"
                                                : parcel.trade_type,
                                            trade_type_name:
                                              parcel.trade_type_name,
                                            trade_term: parcel.terms,
                                            trade_term_name: parcel.term_name,
                                            company_id: parcel.company_id,
                                            company_name: parcel.company_name,
                                            counterparty_id:
                                              parcel.counterparty_id,
                                            counterparty_name:
                                              parcel.counterparty_name,
                                            credit_type: parcel.credit_type,
                                            bank_id: parcel.bank_id,
                                            bank: parcel.bank,
                                            financer_id: parcel.finance_user_id,
                                            financer: parcel.financer_name,
                                            operator_id:
                                              parcel.operator_user_id,
                                            operator: parcel.operator_name,
                                            product_id: parcel.product_id,
                                            product: parcel.product_name,
                                            grade_id: parcel.grade_id,
                                            grade: parcel.grade_name,
                                            country_origin:
                                              parcel.country_origin,
                                            country_origin_name:
                                              parcel.country_origin_name,
                                            load_port_id: parcel.load_port_id,
                                            discharge_port_id:
                                              parcel.discharge_port_id,
                                            date_based_on:
                                              parcel.dates_based_on,
                                            laytime: parcel.laytime,
                                            est_bl_date: parcel.transfer_date,
                                            unit: parcel.units,
                                            unit_name: parcel.unit_name,
                                            max_quantity_mt:
                                              parcel.max_quantity_mt,
                                            max_quantity_cm:
                                              parcel.max_quantity_cm,
                                            allocated_quantity_mt:
                                              parcel.allocated_quantity_mt,
                                            allocated_quantity_cm:
                                              parcel.allocated_quantity_cm,
                                            available_quantity_mt:
                                              parcel.available_quantity_mt,
                                            available_quantity_cm:
                                              parcel.available_quantity_cm,
                                            lc_open_date: parcel.lc_open_date,
                                            bookout_flag: parcel.bookout_flag,
                                            vat_type: parcel.vat_type,
                                            country_vatno: parcel.country_vatno,
                                            tax_code: parcel.tax_code,
                                            transfer_date_est_flag:
                                              parcel.transfer_date_est_flag,
                                            border_crossing_date:
                                              parcel.border_crossing_date,
                                            border_crossing_date_est_flag:
                                              parcel.border_crossing_date_est_flag,
                                            no_agreed_supply_date_flag:
                                              parcel.no_agreed_supply_date_flag,
                                            supply_start_date:
                                              parcel.supply_start_date,
                                            supply_end_date:
                                              parcel.supply_end_date,
                                            transfer_detail_date:
                                              parcel.transfer_detail_date,
                                            trade_density: parcel.est_density,
                                            actual_density: parcel.est_density,
                                            api_60f: parcel.api_60f,
                                            est_density_15c_air:
                                              parcel.est_density_15c_air,
                                            est_density_15c_vac:
                                              parcel.est_density_15c_vac,
                                            shipping_quantity:
                                              parcel.shipping_quantity_mt,
                                            shipping_quantity_mt:
                                              parcel.shipping_quantity_mt,
                                            shipping_quantity_cm:
                                              parcel.shipping_quantity_cm,
                                            shipping_quantity_b:
                                              parcel.shipping_quantity_b,
                                            est_quantity_flag:
                                              parcel.est_quantity_flag,
                                            show_outturn_qty_flag:
                                              parcel.show_outturn_qty_flag,
                                            invoicing_unit: parcel.units,
                                            voyage_id: parcel.voyage_id,
                                            voyage_port: parcel.voyage_port_id,
                                            port_type: parcel.port_type,
                                            method: "PUT",
                                            state: "unlocated",
                                            parcel_number: parcel.parcel_number,
                                          });
                                          parcel.port_type !== "Loss "
                                            ? handleSpotSaleOpen()
                                            : handleLossParcelOpen();
                                        }}
                                        nodeIcon={
                                          parcel.port_type !== "Loss " ? (
                                            <IconDatabase
                                              sx={{ color: "#f44336" }}
                                            />
                                          ) : (
                                            <SaveAltIcon
                                              sx={{ color: "dark" }}
                                            />
                                          )
                                        }
                                        flagQ={parcel.est_quantity_flag}
                                        flagTD={parcel.transfer_date_est_flag}
                                        flagBCD={
                                          parcel.border_crossing_date_est_flag
                                        }
                                        nodeId={`3+${parcel.voyage_parcel_id.toString()}`}
                                        label={` ${parcel.product_name?.toString()} ${parcel.grade_name?.toString()}-<${parcel.trade_code?.toString()}>-${parcel.counterparty_name?.toString()}-<${parcel.parcel_number?.toString()} / T-${parcel.trade_id.toString()}>- [BL-${dayjs(
                                          parcel.transfer_date
                                        ).format("YYYY-MM-DD")}, BC-${dayjs(
                                          parcel.border_crossing_date
                                        ).format("YYYY-MM-DD")}, Q-${
                                          parcel.invoicing_unit === "MT"
                                            ? parcel.shipping_quantity_mt.toString()
                                            : parcel.invoicing_unit === "CM"
                                            ? parcel.shipping_quantity_cm.toString()
                                            : ""
                                        } ${parcel.invoicing_unit}]`}
                                        key={k}
                                      />
                                    ))}
                                </CustomTreeItem>
                              ))}
                          </CustomTreeItem>
                        ))}
                    </TreeView>
                  ) : (
                    <>
                      <Stack
                        spacing={1}
                        direction={"row"}
                        sx={{ m: 1 }}
                        justifyContent={"space-between"}
                      >
                        <Button
                          variant="outlined"
                          color="success"
                          size="small"
                          onClick={handleExport}
                        >
                          Export
                        </Button>
                      </Stack>
                      <DataGrid
                        getRowId={parcelrows.parcel_number}
                        rows={filteredRowsList}
                        columns={parcelcolumns}
                        style={{ background: "white" }}
                        sx={{
                          backgroundColor: "#FFFFFF",
                          "& .MuiDataGrid-cell:hover": {
                            color: "primary.main",
                          },
                          "& .MuiDataGrid-cell": {
                            borderTop: 1,
                            borderTopColor: "#E9ECF0",
                          },
                          borderRadius: 0,
                          maxWidth: "90vw",
                          overflow: "auto",
                        }}
                        rowHeight={50}
                        slots={{
                          noRowsOverlay: CustomNoRowsOverlay,
                        }}
                        pageSizeOptions={[50]}
                      />
                    </>
                  )}
                </Paper>
              </div>
            </Stack>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Box>
            <AntTabs
              value={value1}
              onChange={handleChange}
              aria-label="ant example"
            >
              <AntTab label="Unallocated Trade" {...tradeProps(0)} />
              <AntTab label="Unallocated Parcel" {...tradeProps(1)} />
            </AntTabs>
          </Box>

          <TradeTabs value={value1} index={0}>
            <Card
              sx={{
                borderRadius: 0,
                mt: -3,
                ml: -3,
                height: "80vh",
              }}
            >
              <Stack
                direction={"row"}
                spacing={1}
                sx={{ m: 1, justifyContent: "space-between" }}
              >
                <FormControl sx={{ minWidth: 250 }}>
                  <TextField
                    variant="outlined"
                    size="small"
                    placeholder="Search..."
                    value={filterText1}
                    onChange={handleFilterChange1}
                  />
                </FormControl>
                <Button
                  variant="outlined"
                  color="success"
                  size="small"
                  onClick={handleExportList}
                >
                  Export
                </Button>
              </Stack>

              <DataGrid
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  overflow: "auto",
                }}
                getRowId={(row) => row.trade_id}
                rows={filteredRows1 ? filteredRows1 : []}
                columns={unlallocatedTradecolumns}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                pageSizeOptions={[50]}
                getRowHeight={() => "auto"}
                disableRowSelectionOnClick
              />
            </Card>
          </TradeTabs>
          <TradeTabs value={value1} index={1}>
            <Card
              sx={{
                borderRadius: 0,
                mt: -3,
                ml: -3,
                height: "80vh",
              }}
            >
              <FormControl sx={{ minWidth: 250, m: 1 }}>
                <TextField
                  variant="outlined"
                  size="small"
                  placeholder="Search..."
                  value={filterText2}
                  onChange={handleFilterChange2}
                />
              </FormControl>
              <DataGrid
                fullWidth
                getRowId={(row) => row.trade_id}
                rows={filteredRows2 ? filteredRows2 : []}
                columns={unlallocatedParcelColumns}
                disableColumnMenu={true}
                disableColumnSelector={true}
                disableColumnFilter={true}
                disableDensitySelector={true}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  overflow: "auto",
                }}
                getRowHeight={() => "auto"}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                pageSizeOptions={[50]}
                disableRowSelectionOnClick
              />
            </Card>
          </TradeTabs>
        </Grid>
      </Grid>

      <Port
        port={port}
        openPort={openPort}
        handlePortClose={handlePortClose}
        refreshAllData={refreshAllData}
      />
      <VoyagePage
        newVoyage={newVoyage}
        arrr={arr}
        setArrr={setArr}
        openVoyage={openVoyage}
        handleVoyageClose={handleVoyageClose}
        refreshAllData={refreshAllData}
      />
      <VoyageParcelPage
        parcel={parcel}
        openSpotSale={openSpotSale}
        handleSpotSaleClose={handleSpotSaleClose}
        refreshAllData={refreshAllData}
      />

      <LossParcelPage
        parcel={parcel}
        openLossParcel={openLossParcel}
        handleLossParcelClose={handleLossParcelClose}
        refreshAllData={refreshAllData}
      />
    </>
  );
}
