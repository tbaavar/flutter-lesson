"use client";
import React from "react";
import {
  Box,
  Button,
  Modal,
  Stack,
  Typography,
  IconButton,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  RadioGroup,
  Radio,
  FormHelperText,
  FormControlLabel,
  Divider,
  CircularProgress,
} from "@mui/material";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CloseIcon from "@mui/icons-material/Close";
import useSWR from "swr";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import * as yup from "yup";
import { useFormik } from "formik";
import { fetchData } from "@/lib/fetch";
import MyAlert from "@/components/Myalert";

export default function Port({ openPort, handlePortClose, port }) {
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [orgid, setOrgid] = React.useState();
  const { data: session } = useSession();

  const formik = useFormik({
    initialValues: {
      voyage_port_id: port.method === "POST" ? "" : port.voyage_port_id,
      port_id: port.method === "POST" ? "" : port.port_id,
      port_type: port.method === "POST" ? "Load_port" : port.port_type,
      agent_id: port.method === "POST" ? "" : port.agent_id,
      voyage_id: port.method === "POST" ? "" : port.voyage_id,
      superintendent_id: port.method === "POST" ? "" : port.superintendent_id,
      superveyor_id: port.method === "POST" ? "" : port.superveyor_id,
    },
    validationSchema: yup.object({
      voyage_id: yup.string().required("Please select voyage!"),
      port_id: yup.string().required("Please select port!"),
    }),
    onSubmit: (values) => {
      savePort(values);
    },

    enableReinitialize: true,
  });
  const { data: ports } = useSWR(
    orgid ? 
    [`/api/lookup/port/company/${orgid}` , session?.user?.token] : null ,
    fetcher
  );

  const {
    data: voyage,
    error,
    isLoading,
  } = useSWR(
    [`/api/operation/port/voyage/${session?.user?.user_id}`, session?.user?.token],
    fetcher,
      { refreshInterval: 1000 }
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const savePort = async (data) => {
    const body = {
      voyage_port_id:
        data.voyage_port_id === null ? undefined : data.voyage_port_id,
      voyage_id: data.voyage_id,
      port_id: data.port_id,
      port_type: data.port_type,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/operation/port`,
        port.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        refreshAllData();
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpen(false);
      handlePortClose();
    }
  };
  const delPort = async (port) => {
    const body = {
      voyage_port_id: port.voyage_port_id,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/operation/port`,
        "DELETE",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        refreshAllData();
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpen(false);
      handlePortClose();
    }
  };

  return (
    <>
      <Modal open={openPort}>
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              bgcolor: "background.paper",
              border: "1px solid #000",
              overflowY: "auto",
              overflowX: "auto",
              boxShadow: 0,
              p: 1,
              width: 400,
            }}
          >
            <Box>
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h5" gutterBottom>
                  Port
                </Typography>
                <IconButton
                  sx={{ display: "flex", mt: -1 }}
                  onClick={handlePortClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Divider sx={{ mb: 1 }} />
              <Stack direction={"column"} spacing={1} sx={{ m: 1 }}>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Voyage
                  </InputLabel>
                  <Select
                    name="voyage_id"
                    value={formik.values.voyage_id}
                    onChange={(e) => {
                      formik.handleChange;
                      formik.setFieldValue("voyage_id" , e.target.value)
                      const selVoyage = voyage?.result.filter(
                        (item) => item.voyage_id === e.target.value
                      );

                      setOrgid(selVoyage[0].org_id);
                    }}
                    size="small"
                    disabled={port.method === "PUT" ? true : false}
                    label="Voyage"
                  >
                    {voyage?.result.length >= 0 &&
                      voyage?.result.map((item) => (
                        <MenuItem value={item.voyage_id} key={item.voyage_id}>
                          {item.org_name +
                            " - " +
                            item.voyage_status +
                            " - " +
                            item.voyage_title}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.voyage_id &&
                      Boolean(formik.errors.voyage_id)
                    }
                  >
                    {formik.touched.voyage_id && formik.errors.voyage_id}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel sx={{ color: " #0B626B" }} size="small" id="port">
                    Port
                  </InputLabel>
                  <Select
                    labelId="port_id"
                    id="port_id"
                    name="port_id"
                    size="small"
                    label="Port"
                    value={formik.values.port_id}
                    onChange={formik.handleChange}
                  >
                    {ports?.result.length > 0 &&
                      ports?.result.map((item) => (
                        <MenuItem value={item.port_id} key={item.port_id}>
                          {item.port_name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.port_id && Boolean(formik.errors.port_id)
                    }
                  >
                    {formik.touched.port_id && formik.errors.port_id}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <RadioGroup
                    name="port_type"
                    row
                    defaultValue={"Load_port"}
                    value={formik.values.port_type}
                    onChange={formik.handleChange}
                  >
                    <FormControlLabel
                      control={<Radio />}
                      value="Load_port"
                      label="Load Port"
                      labelPlacement="end"
                      size="small"
                    />
                    <FormControlLabel
                      control={<Radio />}
                      value="Discharge_port"
                      label="Discharge Port"
                      labelPlacement="end"
                      size="small"
                    />
                  </RadioGroup>
                </FormControl>
              </Stack>
            </Box>
            <Divider sx={{ mt: 1, mb: 1 }} />
            {session &&
            session?.user?.menuitems.filter(
              (f) => f.menu_name === "Operation"
            )[0].permission === "Edit" ? (
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Stack direction={"row"} spacing={1}>
                  {port.voyage_port_id !== null ? (
                    <Button
                      variant="outlined"
                      color="error"
                      onClick={async () => {
                        delPort(port);
                      }}
                    >
                      Delete
                    </Button>
                  ) : null}
                </Stack>
                <Stack direction={"row"} spacing={1}>
                  <Button onClick={handlePortClose} variant="outlined">
                    Close
                  </Button>
                  <Button type="submit" variant="contained" color="dark">
                    Save
                  </Button>
                </Stack>
              </Box>
            ) : null}
          </Box>
        </form>
      </Modal>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
