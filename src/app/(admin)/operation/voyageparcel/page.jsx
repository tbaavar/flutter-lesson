"use client";
import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import { MenuItem, Divider, Alert, Autocomplete } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import FormControl from "@mui/material/FormControl";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import InputLabel from "@mui/material/InputLabel";
import { renderTimeViewClock } from "@mui/x-date-pickers/timeViewRenderers";
import Typography from "@mui/material/Typography";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import FormControlLabel from "@mui/material/FormControlLabel";
import IconButton from "@mui/material/IconButton";
import { fetchData } from "@/lib/fetch";
import FormHelperText from "@mui/material/FormHelperText";
import * as yup from "yup";
import { useFormik } from "formik";
import useSWR from "swr";
import dayjs from "dayjs";
import { Stack } from "@mui/material";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import { numberWithCommas } from "@/lib/numberWithCommas";
import CircularProgress from "@mui/material/CircularProgress";
import MyAlert from "@/components/Myalert";

const label = { inputProps: { "aria-label": "Checkbox demo" } };

export default function VoyageParcelPage({
  parcel,
  openSpotSale,
  handleSpotSaleClose,
  refreshAllData
}) {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [opens, setOpens] = React.useState(false);
  const [value, setValue] = React.useState(0);
  const [item, setItem] = React.useState("");
  const [voyageId, setVoyageId] = React.useState(parcel.voyage_id);

  const handleClickClose = () => {
    setOpens(false);
  };


  console.log("parcel",parcel);

  const port_types =
    parcel.trade_type === "Spot_Sale"
      ? "Discharge_port"
      : parcel.trade_type === "Term_Sale"
      ? "Discharge_port"
      : "Load_port";

  const formik = useFormik({
    initialValues: parcel,
    validationSchema: yup.object({
      voyage_id: yup.string().required("Please select voyage!"),
      voyage_port: yup.string().required("Please select voyage port!"),
      operator: yup.string().required("Please select operator!"),
      invoicing_unit: yup.string().required("Please select invoice unit!"),
      available_quantity_mt: yup.string(),
      shipping_quantity_mt: yup
        .number()
        .typeError("Must be a number!")
        .min(0, "Value can't be negative!")
        .max(
          yup.ref("available_quantity_mt"),
          "Value can't be higher than Maximum Available quantity"
        )
        .required("Please insert shipping quantity!"),
      available_quantity_cm: yup.string(),
      shipping_quantity_cm: yup
        .number()
        .typeError("Must be a number!")
        .min(0, "Value can't be negative!")
        .max(
          yup.ref("available_quantity_cm"),
          "Value can't be higher than Maximum Available quantity"
        )
        .required("Please insert shipping quantity!"),
    }),

    onSubmit: async (values) => {
      saveParcelData(values);
    },
    enableReinitialize: true,
  });

  const { data: user } = useSWR(
    parcel.company_id
      ? [`/api/lookup/user/${parcel.company_id}`, session?.user?.token]
      : null,
    fetcher
  );
  const { data: invoice_total } = useSWR(
    parcel.voyage_parcel_id
      ? [
          `/api/operation/invoice/${parcel.voyage_parcel_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const { data: ports } = useSWR(
    ["/api/lookup/port", session?.user?.token],
    fetcher
  );

  const {
    data: voyage,
    error,
    isLoading,
  } = useSWR(
    [`/api/operation/voyage/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );

  const { data: voyagePorts } = useSWR(
    () =>
      parcel.voyage_id
        ? [
            `/api/operation/port/${parcel.voyage_id}/${port_types}`,
            session?.user?.token,
          ]
        : [
            `/api/operation/port/${voyageId}/${port_types}`,
            session?.user?.token,
          ],
    fetcher
  );

  const { data: invoicing_unit } = useSWR(
    ["/api/lookup/Unit", session?.user?.token],
    fetcher
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const saveParcelData = async (data) => {
    const trade_code =
      data.trade_type === "Spot_Sale"
        ? "S"
        : formik.values.trade_type === "Term_Sale"
        ? "S"
        : "P";

    const body = {
      voyage_parcel_id:
        data.method == "PUT" ? data.voyage_parcel_id : undefined,
      trade_id: data.trade_id,
      voyage_id: data.voyage_id,
      parcel_number:
        data.method == "PUT"
          ? data.parcel_number
          : "P-" + dayjs().format("YYMMDDHHMMms").toString(),
      country_origin: data.country_origin,
      load_port_id: data.load_port_id,
      discharge_port_id: data.discharge_port_id,
      lc_open_date: data.lc_open_date,
      vat_type: data.vat_type,
      transfer_state: data.state,
      outside_us_flag: "Y",
      bookout_flag: data.bookout_flag,
      vat_country: data.country_vatno,
      tax_code: data.tax_code,
      custom_cleared_us_flag: "Y",
      dates_based_on: data.date_based_on,
      no_agreed_supply_date_flag: data.no_agreed_supply_date_flag,
      transfer_date: data.est_bl_date,
      transfer_date_est_flag: data.transfer_date_est_flag,
      border_crossing_date: data.border_crossing_date,
      border_crossing_date_est_flag: data.border_crossing_date_est_flag,
      supply_start_date: data.supply_start_date,
      supply_end_date: data.supply_end_date,
      transfer_detail_date: data.transfer_detail_date,
      api_60f: data.api_60f,
      est_density_15c_air: data.est_density_15c_air,
      est_density_15c_vac: data.est_density_15c_vac,
      shipping_quantity_mt: data.shipping_quantity_mt,
      shipping_quantity_cm: data.shipping_quantity_cm,
      est_quantity_flag: data.est_quantity_flag,
      show_outturn_qty_flag: data.show_outturn_qty_flag,
      invoicing_unit: data.invoicing_unit,
      operator_user_id: data.operator_id,
      voyage_port_id: data.voyage_port,
      port_type: port_types,
      product_id: data.product_id,
      grade_id: data.grade_id,
      trade_code: trade_code,
      counterparty_id: data.counterparty_id,
      trade_type: data.trade_type,
      unallocated_quantity_mt:
        data.available_quantity_mt - data.shipping_quantity_mt,
      unallocated_quantity_cm:
        data.available_quantity_cm - data.shipping_quantity_cm,
      org_id: data.company_id,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/operation/parcel`,
        data.method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        refreshAllData();
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
      handleSpotSaleClose();
    }
  };

  const saveUnlocatedParcelData = async () => {

    const body = {
      voyage_parcel_id: parcel.voyage_parcel_id,
      trade_id: parcel.trade_id,
      voyage_id: parcel.voyage_id,
      org_id: parcel.company_id,
      unallocated_quantity_mt: parcel.shipping_quantity_mt,
      unallocated_quantity_cm: parcel.shipping_quantity_cm,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/operation/parcel`,
        "DELETE",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
      setOpens(false);
      handleSpotSaleClose();
    }
  };
  const options =
    voyage?.result?.length > 0 &&
    voyage?.result?.map((option) => {
      return option;
    });

  return (
    <>
      <Modal
        open={openSpotSale}
        aria-labelledby="SpotSale Modal"
        aria-describedby="SpotSale Modal Component"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            maxWidth: "100%",
            maxHeight: "100%",
            overflow: "auto",
            bgcolor: "background.paper",
            border: "1px solid #000",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            {parcel.port_type === "STS_load" ||
            parcel.port_type === "STS_disport" ? (
              <Typography variant="subtitle1" gutterBottom>
                Auto parcel {parcel.parcel_number}
              </Typography>
            ) : (
              <Typography variant="subtitle1" gutterBottom>
                {parcel.trade_type_name} for Trade {parcel.trade_id} | Parcel{" "}
                {parcel.parcel_number}
              </Typography>
            )}
            <IconButton
              sx={{ display: "flex", mt: -1 }}
              onClick={() => {
                setValue(0);
                handleSpotSaleClose();
              }}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Box>
          <Divider sx={{ mb: 1 }} />
          <form onSubmit={formik.handleSubmit}>
            <FormControl fullWidth>
              {formik.values.method === "POST" &&
              formik.values.state === "new" ? (
                <>
                  <Autocomplete
                    fullWidth
                    size="small"
                    options={options}
                    getOptionLabel={(option) => option.voyage_title}
                    onChange={(event, value) => {
                      formik.setFieldValue(
                        "voyage_id",
                        value ? value.voyage_id : ""
                      ); // Update the 'role' field in Formik
                      value ? setVoyageId(value.voyage_id) : null;
                    }}
                    value={formik.values.voyage_title}
                    readOnly={
                      formik.values.state === "unlocated" ? true : false
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Voyage"
                        InputLabelProps={{ sx: { color: "#0B626B" } }}
                      />
                    )}
                  />
                  <FormHelperText
                    error={
                      formik.touched.voyage_id &&
                      Boolean(formik.errors.voyage_id)
                    }
                  >
                    {formik.touched.voyage_id && formik.errors.voyage_id}
                  </FormHelperText>
                </>
              ) : (
                <>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Voyage
                  </InputLabel>
                  <Select
                    disabled={
                      parcel.port_type === "STS_load" ||
                      parcel.port_type === "STS_disport"
                        ? true
                        : false
                    }
                    size="small"
                    label="Voyage"
                    name="voyage_port"
                    readOnly={true}
                    value={formik.values.voyage_id}
                  >
                    {voyage?.result?.length > 0 &&
                      voyage?.result?.map((item) => (
                        <MenuItem value={item.voyage_id} key={item.voyage_id}>
                          {item.voyage_title}
                        </MenuItem>
                      ))}
                  </Select>
                </>
              )}
            </FormControl>
            <Stack direction={"row"} spacing={1} sx={{ mt: 1 }}>
              {parcel.port_type !== "STS_load" ||
              parcel.port_type !== "STS_disport" ? (
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Voyage Ports
                  </InputLabel>
                  <Select
                    size="small"
                    label="Voyage Ports"
                    name="voyage_port"
                    readOnly={
                      formik.values.state === "unlocated" &&
                      formik.values.method === "PUT"
                        ? true
                        : false
                    }
                    value={formik.values.voyage_port}
                    onChange={formik.handleChange}
                  >
                    {voyagePorts?.result?.length > 0 &&
                      voyagePorts?.result?.map((item) => (
                        <MenuItem
                          value={item.voyage_port_id}
                          key={item.voyage_port_id}
                        >
                          {item.port_name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.voyage_port &&
                      Boolean(formik.errors.voyage_port)
                    }
                  >
                    {formik.touched.voyage_port && formik.errors.voyage_port}
                  </FormHelperText>
                </FormControl>
              ) : null}
            </Stack>
            <Stack spacing={1}>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Trade detail</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      name="trade_term_name"
                      size="small"
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Terms"
                      value={formik.values.trade_term_name}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="counterparty_name"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      value={formik.values.counterparty_name}
                      label="Counterparty"
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <InputLabel size="small" sx={{ color: " #0B626B" }}>
                      Operator
                    </InputLabel>
                    <Select
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="operator_id"
                      value={formik.values.operator_id}
                      onChange={formik.handleChange}
                      size="small"
                      label="Operator"
                    >
                      {user?.result.length >= 0 &&
                        user?.result
                          .filter((f) => f.role === "Trader")
                          .map((item) => (
                            <MenuItem value={item.user_id} key={item.user_id}>
                              {item.firstname + " " + item.lastname}
                            </MenuItem>
                          ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.operator &&
                        Boolean(formik.errors.operator)
                      }
                    >
                      {formik.touched.operator && formik.errors.operator}
                    </FormHelperText>
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="country_origin_name"
                      size="small"
                      value={formik.values.country_origin_name}
                      label="Country of origin"
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="product"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Product"
                      value={formik.values.product}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="grade"
                      size="small"
                      value={formik.values.grade}
                      label="Grade"
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="credit"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Credit type"
                      value={formik.values.credit_type}
                    />
                  </FormControl>

                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      InputProps={{
                        readOnly: true,
                      }}
                      name="bank"
                      size="small"
                      label="Bank"
                      value={formik.values.bank}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="financer"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Financer"
                      value={formik.values.financer}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <InputLabel size="small" id="load-port-label">
                      Load Port
                    </InputLabel>
                    <Select
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      labelId="load-port-label"
                      size="small"
                      label="Load Port"
                      name="load_port_id"
                      value={formik.values.load_port_id}
                      InputProps={{
                        readOnly: true,
                      }}
                      onChange={formik.handleChange}
                    >
                      {ports?.result.length > 0 &&
                        ports?.result.map((item) => (
                          <MenuItem value={item.port_id} key={item.port_id}>
                            {item.port_name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                  <FormControl fullWidth>
                    <InputLabel size="small" id="discharge-port-label">
                      Discharge Port
                    </InputLabel>
                    <Select
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      labelId="discharge-port-label"
                      size="small"
                      label="Discharge Port"
                      name="discharge_port_id"
                      value={formik.values.discharge_port_id}
                      onChange={formik.handleChange}
                      InputProps={{
                        readOnly: true,
                      }}
                    >
                      {ports?.result.length > 0 &&
                        ports?.result.map((item) => (
                          <MenuItem value={item.port_id} key={item.port_id}>
                            {item.port_name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="unit_name"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Unit"
                      value={formik.values.unit_name}
                    />
                  </FormControl>
                </Stack>
              </Box>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Parcel Dates</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="date_based_on"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Date based on"
                      value={formik.values.date_based_on}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        disabled={
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                        format="YYYY-MM-DD"
                        readOnly={formik.values.transfer_date_est_flag === "N"}
                        value={dayjs(formik.values.est_bl_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "est_bl_date",
                            dayjs(newValue).format("YYYY-MM-DD")
                          );
                        }}
                        slotProps={{ textField: { size: "small" } }}
                        label="Bill of landing"
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControlLabel
                      control={
                        <Checkbox
                          disabled={
                            parcel.port_type === "STS_load" ||
                            parcel.port_type === "STS_disport"
                              ? true
                              : false
                          }
                          size="small"
                          name="transfer_date_est_flag"
                          checked={formik.values.transfer_date_est_flag === "Y"}
                          onChange={(e) => {
                            formik.setFieldValue(
                              "transfer_date_est_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      type="number"
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="laytime"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="laytime"
                      value={formik.values.laytime}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        disabled={
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                        format="YYYY-MM-DD"
                        value={dayjs(formik.values.border_crossing_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "border_crossing_date",
                            dayjs(newValue).format("YYYY-MM-DD")
                          );
                        }}
                        slotProps={{ textField: { size: "small" } }}
                        label="Border crossing Date"
                        readOnly={
                          formik.values.border_crossing_date_est_flag === "N"
                        }
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControlLabel
                      control={
                        <Checkbox
                          disabled={
                            parcel.port_type === "STS_load" ||
                            parcel.port_type === "STS_disport"
                              ? true
                              : false
                          }
                          size="small"
                          name="border_crossing_date_est_flag"
                          checked={
                            formik.values.border_crossing_date_est_flag === "Y"
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "border_crossing_date_est_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                      readOnly={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <FormControlLabel
                      sx={{ mt: -1 }}
                      control={
                        <Checkbox
                          disabled={
                            parcel.port_type === "STS_load" ||
                            parcel.port_type === "STS_disport"
                              ? true
                              : false
                          }
                          size="small"
                          name="no_agreed_supply_date_flag"
                          checked={
                            formik.values.no_agreed_supply_date_flag === "Y"
                              ? true
                              : false
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "no_agreed_supply_date_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                          readOnly={
                            parcel.port_type === "STS_load" ||
                            parcel.port_type === "STS_disport"
                              ? true
                              : false
                          }
                        />
                      }
                      label="No Agreed Supply Date"
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DateTimePicker
                        disabled={
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                        label="Supply Start Date"
                        format="YYYY-MM-DD hh:mm:ss"
                        value={dayjs(formik.values.supply_start_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "supply_start_date",
                            dayjs(newValue).format("YYYY-MM-DD hh:mm:ss")
                          );
                        }}
                        viewRenderers={{
                          hours: renderTimeViewClock,
                          minutes: renderTimeViewClock,
                          seconds: renderTimeViewClock,
                        }}
                        slotProps={{
                          textField: {
                            size: "small",
                          },
                        }}
                        readOnly={
                          formik.values.no_agreed_supply_date_flag === "N" ||
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth></FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth></FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DateTimePicker
                        disabled={
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                        label="Supply End Date"
                        format="YYYY-MM-DD hh:mm:ss"
                        value={dayjs(formik.values.supply_end_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "supply_end_date",
                            dayjs(newValue).format("YYYY-MM-DD hh:mm:ss")
                          );
                        }}
                        viewRenderers={{
                          hours: null,
                          minutes: null,
                          seconds: null,
                        }}
                        slotProps={{
                          textField: {
                            size: "small",
                          },
                        }}
                        readOnly={
                          formik.values.no_agreed_supply_date_flag === "N" ||
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                      />
                    </LocalizationProvider>
                  </FormControl>
                  <FormControl fullWidth>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        disabled={
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                        format="YYYY-MM-DD"
                        value={dayjs(formik.values.transfer_detail_date)}
                        onChange={(newValue) => {
                          formik.setFieldValue(
                            "transfer_detail_date",
                            dayjs(newValue).format("YYYY-MM-DD")
                          );
                        }}
                        slotProps={{ textField: { size: "small" } }}
                        label="Transfer Details From"
                        readOnly={
                          parcel.port_type === "STS_load" ||
                          parcel.port_type === "STS_disport"
                            ? true
                            : false
                        }
                      />
                    </LocalizationProvider>
                  </FormControl>
                </Stack>
              </Box>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Density Detail</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="trade_density"
                      value={parseFloat(formik.values.trade_density).toFixed(3)}
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      onChange={formik.handleChange}
                      label="Trade Density(kg/L)"
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="api_60f"
                      size="small"
                      type="number"
                      label="API at 60F"
                      value={parseFloat(formik.values.api_60f)}
                      onChange={formik.handleChange}
                      readOnly={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      name="est_density_15c_air"
                      size="small"
                      type="number"
                      label="Est Density at 20°C (air)(kg/L)"
                      readOnly={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      value={parseFloat(formik.values.est_density_15c_air)}
                      onChange={formik.handleChange}
                    />
                  </FormControl>

                  <FormControl fullWidth>
                    <TextField
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      type="number"
                      name="est_density_15c_vac"
                      size="small"
                      readOnly={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                      label="Est Density at 20°C (vacuum)(kg/L)"
                      value={parseFloat(formik.values.est_density_15c_vac)}
                      onChange={formik.handleChange}
                    />
                  </FormControl>
                </Stack>
              </Box>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Required shipping Quantities</legend>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="max_quantity_mt"
                      type="number"
                      size="small"
                      label="Max Quantity(MT)"
                      value={formik.values.max_quantity_mt}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      type="number"
                      name="allocated_quantity_mt"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Allocated Quantity(MT)"
                      value={formik.values.allocated_quantity_mt}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      type="number"
                      name="available_quantity_mt"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Max Available Quantity(MT)"
                      value={formik.values.available_quantity_mt}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      type="number"
                      name="max_quantity_cm"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Max Quantity(CM)"
                      value={formik.values.max_quantity_cm}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="allocated_quantity_cm"
                      type="number"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Allocated Quantity(CM)"
                      value={formik.values.allocated_quantity_cm}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      disabled={true}
                      name="available_quantity_cm"
                      type="number"
                      size="small"
                      InputProps={{
                        readOnly: true,
                      }}
                      label="Max Available Quantity(CM)"
                      value={formik.values.available_quantity_cm}
                    />
                  </FormControl>
                </Stack>
                <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
                  <FormControl fullWidth>
                    <TextField
                      type="number"
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport" ||
                        formik.values.unit === "CM" ||
                        formik.values.est_quantity_flag === "N"
                          ? true
                          : false
                      }
                      name="shipping_quantity_mt"
                      size="small"
                      label="Shipping quantity(MT)"
                      InputLabelProps={{
                        style: { color: " #0B626B" },
                      }}
                      readOnly={
                        formik.values.est_quantity_flag === "N" ? true : false
                      }
                      value={formik.values.shipping_quantity_mt}
                      onBlur={formik.handleBlur}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "shipping_quantity_mt",
                          e.target.value
                        );
                        const total = (
                          formik.values.trade_density
                            ? e.target.value / formik.values.trade_density
                            : 0
                        ).toFixed(5);

                        formik.setFieldValue("shipping_quantity_cm", total);
                      }}
                    />
                    <FormHelperText
                      error={
                        formik.touched.shipping_quantity_mt &&
                        Boolean(formik.errors.shipping_quantity_mt)
                      }
                    >
                      {formik.touched.shipping_quantity_mt &&
                        formik.errors.shipping_quantity_mt}
                    </FormHelperText>
                  </FormControl>

                  <FormControl fullWidth>
                    <TextField
                      type="number"
                      disabled={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport" ||
                        formik.values.unit === "MT" ||
                        formik.values.est_quantity_flag === "N"
                          ? true
                          : false
                      }
                      name="shipping_quantity_cm"
                      size="small"
                      label="Shipping quantity(CM)"
                      InputLabelProps={{
                        style: { color: " #0B626B" },
                      }}
                      value={formik.values.shipping_quantity_cm}
                      onBlur={formik.handleBlur}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "shipping_quantity_cm",
                          e.target.value
                        );
                        const total = (
                          formik.values.trade_density
                            ? e.target.value * formik.values.trade_density
                            : 0
                        ).toFixed(5);

                        formik.setFieldValue("shipping_quantity_mt", total);
                      }}
                    />
                    <FormHelperText
                      error={
                        formik.touched.shipping_quantity_cm &&
                        Boolean(formik.errors.shipping_quantity_cm)
                      }
                    >
                      {formik.touched.shipping_quantity_cm &&
                        formik.errors.shipping_quantity_cm}
                    </FormHelperText>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControlLabel
                      control={
                        <Checkbox
                          disabled={
                            parcel.port_type === "STS_load" ||
                            parcel.port_type === "STS_disport"
                              ? true
                              : false
                          }
                          size="small"
                          name="est_quantity_flag"
                          checked={
                            formik.values.est_quantity_flag === "Y"
                              ? true
                              : false
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "est_quantity_flag",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                      readOnly={
                        parcel.port_type === "STS_load" ||
                        parcel.port_type === "STS_disport"
                          ? true
                          : false
                      }
                    />
                  </FormControl>
                </Stack>
              </Box>
            </Stack>
            <Stack spacing={1}>
              <Box
                component="fieldset"
                alignItems={"top"}
                sx={{ borderRadius: 1, borderColor: "#e6e6e6" }}
              >
                <legend>Invoicing Detail</legend>
                <Stack direction="column" spacing={1}>
                  <FormControl>
                    <InputLabel sx={{ color: " #0B626B" }} size="small">
                      Invoice Unit
                    </InputLabel>
                    <Select
                      disabled={true}
                      size="small"
                      label="Invoice Unit"
                      name="invoicing_unit"
                      value={formik.values.invoicing_unit}
                      onChange={formik.handleChange}
                    >
                      {invoicing_unit?.result.length > 0 &&
                        invoicing_unit?.result.map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.invoicing_unit &&
                        Boolean(formik.errors.invoicing_unit)
                      }
                    >
                      {formik.touched.invoicing_unit &&
                        formik.errors.invoicing_unit}
                    </FormHelperText>
                  </FormControl>
                </Stack>
              </Box>
            </Stack>

            <Divider sx={{ mt: 1, mb: 1 }} />
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Stack direction={"row"} spacing={1}>
                {parcel &&
                parcel.method === "PUT" &&
                parcel.port_type !== "STS_load" &&
                parcel.port_type !== "STS_disport" &&
                session?.user?.menuitems.filter(
                  (f) => f.menu_name === "Operation"
                )[0].permission === "Edit" ? (
                  <>
                    <Button
                      variant="outlined"
                      color="error"
                      onClick={async () => {
                        setOpens(true);
                      }}
                    >
                      Unallocated
                    </Button>
                  </>
                ) : null}
              </Stack>
              {session &&
              session?.user?.menuitems.filter(
                (f) => f.menu_name === "Operation"
              )[0].permission === "Edit" ? (
                <Stack direction={"row"} spacing={1}>
                  <Button variant="outlined" onClick={handleSpotSaleClose}>
                    Close
                  </Button>

                  <Button variant="contained" type="submit" color="dark">
                    Save
                  </Button>
                </Stack>
              ) : null}
            </Box>
          </form>
        </Box>
      </Modal>

      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
      <Modal open={opens}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Unallocate
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Typography variant="h7">
                You have <strong>{invoice_total?.result.total_invoice} </strong>
                invoices connected to this Parcel
              </Typography>

              <Alert severity="error">are you sure to Unallocate!</Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                saveUnlocatedParcelData();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              Yes
            </Button>
            <Button onClick={handleClickClose} variant="outlined">
              No
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
}
