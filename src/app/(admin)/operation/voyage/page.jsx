"use client";
import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import {
  Box,
  Button,
  Modal,
  Stack,
  Typography,
  IconButton,
  FormControl,
  InputLabel,
  Select,
  TextField,
  MenuItem,
  FormHelperText,
  Table,
  Input,
  Divider,
  CircularProgress,
  Tooltip,
} from "@mui/material";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import { useFormik } from "formik";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { styled } from "@mui/material/styles";
import { fetchData } from "@/lib/fetch";
import useSWR from "swr";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { AntTabs, AntTab, a11yProps } from "@/components/CustomTabControl";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#3b8188",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

function VoyageTabs(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="voyagepanel"
      hidden={value !== index}
      id={`Voyage${index}`}
      aria-labelledby={`Voyage${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ m: 2, height: 500, width: 460 }}>
          <Typography component={"span"}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default function VoyagePage({
  newVoyage,
  arrr,
  setArrr,
  openVoyage,
  handleVoyageClose,
  refreshAllData
}) {
  const { data: session } = useSession();
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [image, setImage] = React.useState();
  const [imageName, setImageName] = React.useState();
  const [loading, setLoading] = React.useState(false);
  const [uploadOpen, setUploadOpen] = React.useState(false);


  const formData = new FormData();
  const handleVoyageChange = (event, newValue) => {
    setValue(newValue);
  };
  const formik = useFormik({
    initialValues: newVoyage,
    validationSchema: yup.object({
      intended_vessel: yup.string().required("Please select inteded vessel!"),
      product_id: yup.string().required("Please select main product!"),
      org_id: yup.string().required("Please select Company!"),
      voyage_title: yup.string().required("Please insert voyage title!"),
      charter_type: yup.string().required("Please select charter type!"),
      voyage_status: yup.string().required("Please select voyage status!"),
      charter_party_date: yup
        .string()
        .required("Please select Charter party date!"),
    }),
    onSubmit: async (values) => {
      saveVoyageData(values);
    },

    enableReinitialize: true,
  });

  const attachmentFormik = useFormik({
    initialValues: {
      invoice_attachment_id: "",
      attachment_name: "",
      fileDescription: "",
      attachmentname: "",
      attachmenttype: "",
      description: "",
      filename: "",
      filetype: "",
      attachmenturl: "",
      sourcefile: "",
      method: null,
    },
    validationSchema: yup.object({
      filename: yup.string().required("Please choose File!"),
    }),
    onSubmit: async (values) => {
      uploadToServer(values);
      setUploadOpen(false);
    },
    enableReinitialize: true,
  });
  const uploadToClient = (event) => {
    if (event.target.files && event.target.files[0]) {
      const i = event.target.files[0];
      setImage(i);
      setImageName(i.name);

      attachmentFormik.setFieldValue("filename", i.name);
      attachmentFormik.setFieldValue("filelength", i.size);
      attachmentFormik.setFieldValue("filetype", i.type);
      attachmentFormik.setFieldValue("sourcefile", i);
    }
  };

  const uploadToServer = async () => {
    formData.append("bucket", "voyage");
    formData.append("attachment_type", attachmentFormik.values.filetype);
    formData.append("attachment_name", attachmentFormik.values.attachmentname);
    formData.append("filename", attachmentFormik.values.filename);
    formData.append("description", attachmentFormik.values.description);
    formData.append("filelength", attachmentFormik.values.filelength);
    formData.append("voyage_attachment_id", null);
    formData.append("sourcefile", attachmentFormik.values.sourcefile);

    // Ensure oldArray is initialized properly
    setArrr((oldArray = []) => [
      ...oldArray,
      {
        voyage_attachment_id: null,
        attachment_type: attachmentFormik.values.filetype,
        attachment_name: attachmentFormik.values.attachmentname,
        description: attachmentFormik.values.description,
        bucket: "voyage",
        sourcefile: attachmentFormik.values.sourcefile,
        filetype: attachmentFormik.values.filetype,
        filelength: attachmentFormik.values.filelength,
        filename: attachmentFormik.values.filename,
      },
    ]);
  };
  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );

  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: product_id } = useSWR(
    [`/api/lookup/product`, session?.user?.token],
    fetcher
  );

  const handleDelete = (row) => {
    const currentRow = params.row;
    deleteVoyageAtt(currentRow);
  };
  const handlePreview = (row) => {
    window.open(`${row.attachment_url}`, "_blank");
  };
  const deleteVoyageAtt = async (data) => {
    try {
      setOpen(true);
      const body = {
        invoice_id: invoice_id,
        username: session?.user?.username,
        bucket: "voyage",
        id: invoice_id,
        filename: data.filename,
        voyage_attachment_id: data.voyage_attachment_id,
      };

      const res = await fetchData(
        `/api/operation/voyage`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        toast.success("Амжилттай устгалаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Устгах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpen(false);
    }
  };
  const saveVoyageData = async (data) => {
    formData.append("org_id", data.org_id);
    formData.append("voyage_comment", data.voyage_comment);
    formData.append(
      "quantity_discrepancy_comment",
      data.quantity_discrepancy_comment
    );
    formData.append(
      "quantity_discrepancy_flag",
      data.quantity_discrepancy_flag
    );
    formData.append("insurance_status", data.insurance_status);
    formData.append("insured_description", data.insured_description);
    formData.append(
      "insurance_externally_flag",
      data.insurance_externally_flag
    );
    formData.append("insurance_required_flag", data.insurance_required_flag);
    formData.append("charter_party_date", data.charter_party_date);
    formData.append("charter_type", data.charter_type);
    formData.append("voyage_status", data.voyage_status);
    formData.append("voyage_title", data.voyage_title);
    formData.append("username", session?.user?.username);
    formData.append("intended_vessel", data.intended_vessel);
    formData.append(
      "voyage_id",
      data.method === "POST" ? undefined : data.voyage_id
    );
    formData.append("product_id", data.product_id);

    formData.append("bucket", "voyage");
    formData.append("attachment_type", attachmentFormik.values.filetype);
    formData.append("attachment_name", attachmentFormik.values.attachmentname);
    formData.append("filename", attachmentFormik.values.filename);
    formData.append("description", attachmentFormik.values.description);
    formData.append("filelength", attachmentFormik.values.filelength);
    formData.append(
      "voyage_attachment_id",
      data.method === "POST" ? undefined : arrr[0]?.voyage_attachment_id
    );
    formData.append("sourcefile", attachmentFormik.values?.sourcefile);

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetch(`/api/operation/voyage`, {
        method: data.method,
        headers: {
          Authorization: `Bearer ${session?.user?.token}`,
        },
        body: formData,
      });
      console.log("res",res);
      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        refreshAllData();
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      console.log("error",error);
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleVoyageClose();
    }
  };

  const delVoyageData = async (newVoyage) => {
    const body = {
      voyage_id: newVoyage.voyage_id,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const res = await fetchData(
        `/api/operation/voyage`,
        "DELETE",
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleVoyageClose();
    }
  };

  return (
    <>
      {loading === "true" ? (
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            height: "flex",
            width: 1100,
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            border: "1px solid #000",
            boxShadow: 0,
            p: 2,
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <Box>
          <Modal
            open={openVoyage}
            aria-labelledby="New Voyage Modal"
            aria-describedby="New Voyage Modal Component"
          >
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                bgcolor: "background.paper",
                border: "1px solid #000",
                overflowY: "auto",
                overflowX: "auto",
                boxShadow: 0,
                p: 1,
              }}
            >
              <Box>
                <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                  <Typography variant="h5" gutterBottom>
                    Voyage
                  </Typography>
                  <IconButton
                    sx={{ display: "flex", mt: -1 }}
                    onClick={handleVoyageClose}
                    aria-label="close"
                  >
                    <CloseIcon />
                  </IconButton>
                </Box>
                <Divider sx={{ mb: 1 }} />
                <form onSubmit={formik.handleSubmit}>
                  <AntTabs
                    value={value}
                    onChange={handleVoyageChange}
                    aria-label="New Voyage Tabs"
                  >
                    <AntTab label="Detail" {...a11yProps(0)} />
                    <AntTab label="Documents" {...a11yProps(1)} />
                  </AntTabs>
                  <VoyageTabs value={value} index={0}>
                    <Box>
                      <Stack direction="column" spacing={1}>
                        <FormControl fullWidth>
                          <InputLabel sx={{ color: " #0B626B" }} size="small">
                            Company
                          </InputLabel>
                          <Select
                            id="org_id"
                            name="org_id"
                            size="small"
                            label="Company"
                            value={formik.values.org_id}
                            onChange={formik.handleChange}
                          >
                            {org?.result.length > 0 &&
                              org?.result.map((item) => (
                                <MenuItem value={item.org_id} key={item.org_id}>
                                  {item.org_name}
                                </MenuItem>
                              ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.org_id &&
                              Boolean(formik.errors.org_id)
                            }
                          >
                            {formik.touched.org_id && formik.errors.org_id}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <InputLabel sx={{ color: " #0B626B" }} size="small">
                            Transportation
                          </InputLabel>
                          <Select
                            id="intended_vessel"
                            name="intended_vessel"
                            size="small"
                            label="Transportation"
                            value={formik.values.intended_vessel}
                            onChange={formik.handleChange}
                          >
                            {lookup?.result.length >= 0 &&
                              lookup?.result
                                .filter(
                                  (f) => f.lookup_type === "Intended_Vessel"
                                )
                                .map((item) => (
                                  <MenuItem
                                    value={item.lookup_code}
                                    key={item.lookup_code}
                                  >
                                    {item.name}
                                  </MenuItem>
                                ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.intended_vessel &&
                              Boolean(formik.errors.intended_vessel)
                            }
                          >
                            {formik.touched.intended_vessel &&
                              formik.errors.intended_vessel}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <InputLabel sx={{ color: " #0B626B" }} size="small">
                            Main product
                          </InputLabel>
                          <Select
                            labelId="product_id"
                            id="product_id"
                            size="small"
                            name="product_id"
                            label="Main product"
                            value={formik.values.product_id}
                            onChange={formik.handleChange}
                          >
                            {product_id?.result.length > 0 &&
                              product_id?.result.map((item) => (
                                <MenuItem
                                  value={item.product_id}
                                  key={item.product_id}
                                >
                                  {item.product_name}
                                </MenuItem>
                              ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.product_id &&
                              Boolean(formik.errors.product_id)
                            }
                          >
                            {formik.touched.product_id &&
                              formik.errors.product_id}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <TextField
                            id="voyage_title"
                            name="voyage_title"
                            size="small"
                            label="Voyage title override"
                            value={formik.values.voyage_title}
                            InputLabelProps={{
                              style: { color: " #0B626B" },
                            }}
                            onChange={formik.handleChange}
                          />
                          <FormHelperText
                            error={
                              formik.touched.voyage_title &&
                              Boolean(formik.errors.voyage_title)
                            }
                          >
                            {formik.touched.voyage_title &&
                              formik.errors.voyage_title}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <InputLabel sx={{ color: " #0B626B" }} size="small">
                            Voyage status
                          </InputLabel>
                          <Select
                            labelId="voyage_status"
                            id="voyage_status"
                            size="small"
                            label="Voyage status"
                            name="voyage_status"
                            value={formik.values.voyage_status}
                            onChange={formik.handleChange}
                          >
                            {lookup?.result.length >= 0 &&
                              lookup?.result
                                .filter(
                                  (f) => f.lookup_type === "Voyage_status"
                                )
                                .map((item) => (
                                  <MenuItem
                                    value={item.lookup_code}
                                    key={item.lookup_code}
                                  >
                                    {item.name}
                                  </MenuItem>
                                ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.voyage_status &&
                              Boolean(formik.errors.voyage_status)
                            }
                          >
                            {formik.touched.voyage_status &&
                              formik.errors.voyage_status}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth>
                          <InputLabel size="small">Charter type</InputLabel>
                          <Select
                            labelId="charter_type"
                            id="charter_type"
                            size="small"
                            label="Charter type"
                            name="charter_type"
                            value={formik.values.charter_type}
                            onChange={formik.handleChange}
                          >
                            {lookup?.result.length >= 0 &&
                              lookup?.result
                                .filter((f) => f.lookup_type === "Charter_Type")
                                .map((item) => (
                                  <MenuItem
                                    value={item.lookup_code}
                                    key={item.lookup_code}
                                  >
                                    {item.name}
                                  </MenuItem>
                                ))}
                          </Select>
                          <FormHelperText
                            error={
                              formik.touched.charter_type &&
                              Boolean(formik.errors.charter_type)
                            }
                          >
                            {formik.touched.charter_type &&
                              formik.errors.charter_type}
                          </FormHelperText>
                        </FormControl>
                        <FormControl fullWidth sx={{ mt: 2 }}>
                          <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DatePicker
                              name="charter_party_date"
                              id="charter_party_date"
                              label="Charter party date"
                              InputLabelProps={{
                                style: {
                                  color: "#0B626B",
                                },
                              }}
                              slotProps={{
                                textField: {
                                  size: "small",
                                },
                              }}
                              format="YYYY-MM-DD"
                              disabled={formik.values.method === "PUT"}
                              value={dayjs(formik.values.charter_party_date)}
                              onChange={(newValue) => {
                                formik.setFieldValue(
                                  "charter_party_date",
                                  dayjs(newValue).format("YYYY-MM-DD")
                                );
                              }}
                            />

                            <FormHelperText
                              error={
                                formik.touched.charter_party_date &&
                                Boolean(formik.errors.charter_party_date)
                              }
                            >
                              {formik.touched.charter_party_date &&
                                formik.errors.charter_party_date}
                            </FormHelperText>
                          </LocalizationProvider>
                        </FormControl>
                        <Stack direction="row" spacing={1}>
                          <FormControl fullWidth>
                            <TextField
                              name="voyage_comment"
                              multiline
                              label="Comment"
                              rows={9}
                              variant="outlined"
                              value={formik.values.voyage_comment}
                              onChange={formik.handleChange}
                            />
                          </FormControl>
                        </Stack>
                      </Stack>
                    </Box>
                  </VoyageTabs>
                  <VoyageTabs value={value} index={1}>
                    <Box>
                      <Stack
                        direction={"row"}
                        spacing={1}
                        sx={{ mb: 1 }}
                        justifyContent={"space-between"}
                      >
                        <div></div>
                        {/* <Input
                          type="file"
                          name="file"
                          onChange={uploadToClient}
                        /> */}
                        {loading ? (
                          <CircularProgress
                            sx={{ maxWidth: 30, maxHeight: 30 }}
                          />
                        ) : (
                          <Button
                            variant="text"
                            size="small"
                            onClick={() => {
                              // uploadToServer()
                              setUploadOpen(true);
                            }}
                            startIcon={<AddSharpIcon />}
                          >
                            Add
                          </Button>
                        )}
                      </Stack>

                      <TableContainer>
                        <Table size="small" aria-label="summary">
                          <TableHead>
                            <TableRow>
                              <StyledTableCell align="left" width={"20px"}>
                                ID
                              </StyledTableCell>
                              <StyledTableCell align="left" width={"20px"}>
                             Name
                              </StyledTableCell>

                              <StyledTableCell align="center" width={"110"}>
                              Description
                              </StyledTableCell>
                              <StyledTableCell align="center" width={"110"}>
                                Action
                              </StyledTableCell>
                            </TableRow>
                          </TableHead>

                          {loading === "true" ? (
                            <CircularProgress />
                          ) : (
                            <TableBody>
                              {arrr?.map((row, i) => {
                                return (
                                  <TableRow
                                    key={row.name}
                                    sx={{
                                      "&:last-child td, &:last-child th": {
                                        border: 0,
                                      },
                                    }}
                                  >
                                    <TableCell scope="row">{i + 1}</TableCell>
                                    <TableCell
                                      size="small"
                                      align="center"
                                      sx={{
                                        wordWrap: "break-word",
                                        maxWidth: 130,
                                      }}
                                    >
                                      {row.attachment_name}
                                    </TableCell>
                                    <TableCell
                                      size="small"
                                      align="center"
                                      sx={{
                                        wordWrap: "break-word",
                                        maxWidth: 130,
                                      }}
                                    >
                                      {row.description}
                                    </TableCell>
                                    <TableCell
                                      size="small"
                                      align="center"
                                      sx={{
                                        wordWrap: "break-word",
                                        maxWidth: 130,
                                      }}
                                    >
                                      <Tooltip title="Delete">
                                        <IconButton
                                          color="error"
                                          onClick={() => handleDelete(row)}
                                        >
                                          <DeleteIcon />
                                        </IconButton>
                                      </Tooltip>
                                      {row.attachment_url ? (
                                        <Tooltip title="Preview">
                                          <IconButton
                                            color="primary"
                                            onClick={() => handlePreview(row)}
                                          >
                                            <VisibilityIcon />
                                          </IconButton>
                                        </Tooltip>
                                      ) : null}
                                    </TableCell>
                                  </TableRow>
                                );
                              })}
                            </TableBody>
                          )}
                        </Table>
                      </TableContainer>
                    </Box>
                  </VoyageTabs>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Stack direction={"row"} spacing={1}>
                      {newVoyage.voyage_id !== null ? (
                        <Button
                          variant="outlined"
                          color="error"
                          onClick={async () => {
                            delVoyageData(newVoyage);
                          }}
                        >
                          Delete
                        </Button>
                      ) : null}
                    </Stack>
                    {session &&
                    session?.user?.menuitems.filter(
                      (f) => f.menu_name === "Operation"
                    )[0].permission === "Edit" ? (
                      <Stack direction={"row"} spacing={1}>
                        <Button variant="outlined" onClick={handleVoyageClose}>
                          Close
                        </Button>
                        <Button variant="contained" type="submit" color="dark">
                          Save
                        </Button>
                      </Stack>
                    ) : null}
                  </Box>
                </form>
              </Box>
            </Box>
          </Modal>
        </Box>
      )}
      <Modal open={uploadOpen}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            border: "1px solid #000",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <form onSubmit={attachmentFormik.handleSubmit}>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography variant="h5" gutterBottom>
                Attachment
              </Typography>
              <IconButton
                sx={{ display: "flex", mt: -1 }}
                aria-label="close"
                onClick={() => setUploadOpen(false)}
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Divider sx={{ mb: 1 }} />
            <Stack direction="column" spacing={1}>
              <FormControl>
                <TextField
                  name="attachmentname"
                  fullWidth
                  variant="outlined"
                  size="small"
                  label="Name"
                  value={attachmentFormik.values.attachmentname}
                  onChange={attachmentFormik.handleChange}
                />
                <FormHelperText
                  error={
                    attachmentFormik.touched.attachmentname &&
                    Boolean(attachmentFormik.errors.attachmentname)
                  }
                >
                  {attachmentFormik.touched.attachmentname &&
                    attachmentFormik.errors.attachmentname}
                </FormHelperText>
              </FormControl>

              <FormControl>
                <TextField
                  name="description"
                  label="Description"
                  multiline
                  rows={15}
                  variant="outlined"
                  value={attachmentFormik.values.description}
                  onChange={attachmentFormik.handleChange}
                />
                <FormHelperText
                  error={
                    attachmentFormik.touched.description &&
                    Boolean(attachmentFormik.errors.description)
                  }
                >
                  {attachmentFormik.touched.description &&
                    attachmentFormik.errors.description}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="filename"
                  label="Filename"
                  variant="outlined"
                  size="small"
                  disabled
                  value={attachmentFormik.values.filename}
                  onChange={attachmentFormik.handleChange}
                />
                <FormHelperText
                  error={
                    attachmentFormik.touched.filename &&
                    Boolean(attachmentFormik.errors.filename)
                  }
                >
                  {attachmentFormik.touched.filename &&
                    attachmentFormik.errors.filename}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Input type="file" name="file" onChange={uploadToClient} />

            <Divider sx={{ mt: 1 }} />
            <Box
              sx={{
                display: "flex",
                mb: 1,
                mt: 1,
                justifyContent: "space-between",
              }}
            >
              <Button variant="outlined" onClick={() => setUploadOpen(false)}>
                Close
              </Button>

              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            </Box>
          </form>
          {/* </>
          )} */}
        </Box>
      </Modal>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
