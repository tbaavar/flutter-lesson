"use client";
import React from "react";
import { useState } from "react";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import { useFormik } from "formik";
import * as yup from "yup";
import MyAlert from "@/components/Myalert";
import CustomNoRowsOverlay from "@/components/NoRecord";
import CloseIcon from "@mui/icons-material/Close";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { numberWithCommas } from "@/lib/numberWithCommas";
import {
  Box,
  Button,
  Modal,
  Stack,
  Input,
  Select,
  MenuItem,
  TextField,
  FormHelperText,
  FormControl,
  InputLabel,
  IconButton,
  Divider,
  Grid,
  CircularProgress,
  Typography,
  Tooltip,
  Drawer,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import {
  DataGrid,
  GridActionsCellItem,
} from "@mui/x-data-grid";
import { useSession } from "next-auth/react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { fetcher } from "@/lib/fetcher";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import dayjs from "dayjs";
import exportToExcel from "@/components/ExcelExport";

export default function ContractPage() {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [filterText, setFilterText] = React.useState("");
  const [opens, setOpens] = React.useState(false);
  const [tradeID, setTradeID] = React.useState("");
  const [trade_id, setTradeId] = React.useState("");
  const [open, setOpen] = useState(false);
  const [openAtt, setOpenAtt] = useState(false);
  const [image, setImage] = React.useState(null);
  const [dealID, setDealID] = React.useState("");
  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const { data: types } = useSWR(
    ["/api/lookup/Attachment_type", session?.user?.token],
    fetcher
  );
  const { data: attachments } = useSWR(
    tradeID ? [`/api/contract/${tradeID}`, session?.user?.token] : null,
    fetcher,
    { refreshInterval: 1000 }
  );
  const {
    data: trades,
    error,
    isLoading,
    mutate,
  } = useSWR(
    [`/api/trade/contract/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );

  const handleExportExcel = () => {
    const trade = [
      {
        header: "id",
      },
      {
        header: "trade_id",
      },
      {
        header: "deal_id",
      },
      {
        header: "country_party_name",
      },
      {
        header: "trade_type_name",
      },
      {
        header: "product_name",
      },
      {
        header: "grade_name",
      },
      {
        header: "quantity",
      },
      {
        header: "units",
      },
      {
        header: "contract_received_date",
      },
      {
        header: "contract_drafted_date",
      },
      {
        header: "contact_sent_date",
      },
      {
        header: "trader_name",
      },

      {
        header: "operator_name",
      },

      {
        header: "operation_flag",
      },

      {
        header: "finance_name",
      },

      {
        header: "finance_flag",
      },
    ];
    const rows =
      trades?.result?.map((row, i) => ({
        id: i + 1,
        trade_id: row.trade_id,
        deal_id: row.deal_id,
        country_party_name: row.country_party_name,
        trade_type_name: row.trade_type_name,
        parcel_number: row.parcel_number,
        counterparty_name: row.counterparty_name,
        trade_type_name: row.trade_type_name,
        product_name: row.product_name,
        grade_name: row.grade_name,
        quantity: row.quantity,
        units: row.units,
        contract_received_date: row.contract_received_date,
        contract_drafted_date: row.contract_drafted_date,
        contact_sent_date: row.contact_sent_date,
        trader_name: row.trader_name,
        operator_name: row.operator_name,
        operation_flag: row.operation_flag,
        finance_name: row.finance_name,
        finance_flag: row.finance_flag,
      })) || [];

    exportToExcel(
      rows,
      trade,
      `ContractReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const columns = [
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      width: 100,
      sortable: false,
      renderCell: (params) => {
        // const handleEdit = (e) => {
        //   const currentRow = params.row;
        //   formik.setValues({
        //     id: currentRow.trade_attachment_id,
        //     attachmentname: currentRow.attachment_name,
        //     attachmenttype: currentRow.attachment_type,
        //     description: currentRow.description,
        //     filename: currentRow.filename,
        //     attachmenturl: currentRow.attachment_url,
        //     method: "PUT",
        //   });

        //   handleClickOpen();
        // };

        const handleDelete = (e) => {
          const currentRow = params.row;
          formik.setValues({
            id: currentRow.trade_contract_id,
            attachmentname: currentRow.attachment_name,
            attachmenttype: currentRow.attachment_type,
            description: currentRow.description,
            filename: currentRow.filename,
            attachmenturl: currentRow.attachment_url,
            method: "DELETE",
          });
          deleteAttachment(currentRow);
        };
        const handlePreview = () => {
          // router.push(`${params.row.attachment_url}`)
          window.open(`${params.row.attachment_url}`, "_blank");
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleDelete}
              />
            </Tooltip>
            <Tooltip title="Preview">
              <GridActionsCellItem
                icon={<VisibilityIcon />}
                label="Preview"
                color="primary"
                onClick={handlePreview}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
    {
      field: "attachment_name",
      renderHeader: () => <strong>{"Attachment name"}</strong>,
      width: 150,
    },

    {
      field: "attachment_type",
      renderHeader: () => <strong>{"Attachment type"}</strong>,
      width: 150,
    },

    {
      field: "filename",
      renderHeader: () => <strong>{"Filename"}</strong>,
      width: 150,
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Attachment date"}</strong>,
      width: 150,
      valueGetter: (params) =>
        dayjs(params.row.creation_date).format("YYYY-MM-DD"),
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 350,
    },
  ];
  const formik = useFormik({
    initialValues: {
      attachmentname: "",
      attachmenttype: "",
      description: "",
      filename: "",
      attachmenturl: "",
      method: null,
      id: "",
      filetype: "",
      sourcefile: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
      attachmentname: yup.string().required("Please insert Name!"),
    }),
    onSubmit: async (values) => {
      saveAttachment(values);
    },
    enableReinitialize: true,
  });

  const saveAttachment = async (data) => {
    setOpens(true);
    const formData = new FormData();
    formData.append("id", tradeID);
    formData.append("bucket", "contract");
    formData.append("trade_id", tradeID);
    formData.append("attachment_type", data.filetype);
    formData.append("attachment_name", data.attachmentname);
    formData.append("filename", data.filename);
    formData.append("description", data.description);
    formData.append("username", session?.user?.username);
    formData.append("filelength", data.filelength);
    formData.append(
      "trade_contract_id",
      data.method === "POST" ? undefined : data.id
    );
    formData.append("sourcefile", data.file);
    try {
      const res = await fetch(`/api/contract`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${session?.user?.token}`,
        },
        body: formData,
      });
      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Хадгалах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpenAtt(false);
      setOpens(false);
    }
  };

  const deleteAttachment = async (data) => {
    try {
      setOpens(true);
      const body = {
        trade_id: tradeID,
        username: session?.user?.username,
        bucket: "contract",
        id: tradeID,
        filename: data.filename,
        trade_contract_id: data.trade_contract_id,
      };

      const res = await fetchData(
        `/api/contract`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        toast.success("Амжилттай устгалаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("Устгах үед алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setOpenAtt(false);
      setOpens(false);
    }
  };
  const tradeColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade/Deal"}</strong>,
      width: 110,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {"T-" + params.row.trade_id + "/"}
          </Typography>
          <Typography variant="h7">
            {params.row.deal_id ? "D-" + params.row.deal_id : null}
          </Typography>
        </Box>
      ),
      headerAlign: "left",
    },

    {
      field: "country_party_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.country_party_name}</Typography>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#0b626b"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#e27671"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}
        </Box>
      ),
      width: 170,
      headerAlign: "left",
    },

    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>

          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 190,
      headerAlign: "left",
    },

    {
      field: "quantity_mt",
      renderHeader: () => <strong>{"Quantity(MT)"}</strong>,
      renderCell: (params) => {
        if (params.row.quantity !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                <strong> {numberWithCommas(params.row.quantity)}</strong>
              </Typography>
            </Box>
          );
        } else return null;
      },
      width: 120,
      headerAlign: "left",
      align: "right",
    },
    {
      field: "quantity_cm",
      renderHeader: () => <strong>{"Quantity(CM)"}</strong>,
      renderCell: (params) => {
        if (params.row.quantity_cm !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                <strong> {numberWithCommas(params.row.quantity_cm)}</strong>
              </Typography>
            </Box>
          );
        } else return null;
      },
      width: 120,
      headerAlign: "left",
      align: "right",
    },
    {
      field: "unit",
      renderHeader: () => <strong>{"Unit"}</strong>,
      renderCell: (params) => {
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Typography variant="h7">{params.row.unit_name}</Typography>
          </Box>
        );
      },
      width: 120,
      headerAlign: "left",
    },
    {
      field: "contract_date",
      renderHeader: () => <strong>{"Contract date"}</strong>,
      width: 150,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.contract_received_date
              ? "Recieve: " +
                dayjs(params.row.contract_received_date).format("YYYY-MM-DD")
              : "Recieve: "}
          </Typography>
          <Typography variant="h7">
            {params.row.contract_drafted_date
              ? "Draft: " +
                dayjs(params.row.contract_drafted_date).format("YYYY-MM-DD")
              : "Draft: "}
          </Typography>
          <Typography variant="h7">
            {params.row.contact_sent_date
              ? "Sent: " +
                dayjs(params.row.contact_sent_date).format("YYYY-MM-DD")
              : "Sent: "}
          </Typography>
        </Box>
      ),
      headerAlign: "left",
    },

    {
      field: "trader_name",
      renderHeader: () => <strong>{"Trader"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "operation_flag",
      renderHeader: () => <strong>{"Operation"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
      <Typography variant="h7">{params.row.operator_name?params.row.operator_name:"***"}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.operation_flag} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "finance_name",
      renderHeader: () => <strong>{"Finance"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.finance_name ? params.row.finance_name : "***"}
          </Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.finance_flag} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "total",
      renderHeader: () => <strong>{"Document"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      flex: 1,
      sortable: false,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const currentRow = params.row;
          formik.setValues({
            id: currentRow.trade_attachment_id,
            attachmentname: currentRow.attachment_name,
            attachmenttype: currentRow.attachment_type,
            description: currentRow.description,
            filename: currentRow.filename,
            attachmenturl: currentRow.attachment_url,
            method: "PUT",
          });
          setDealID(currentRow.deal_id);
          setTradeID(currentRow.trade_id);
          handleClickOpen();
        };

        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Edit">
              <GridActionsCellItem
                icon={<EditIcon />}
                label="Edit"
                color="secondary"
                onClick={handleEdit}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];

  const uploadToClient = (event) => {
    if (event.target.files && event.target.files[0]) {
      const i = event.target.files[0];
      setImage(i);
      formik.setFieldValue("file", i);
      formik.setFieldValue("filename", i.name);
      formik.setFieldValue("filelength", i.size);
      formik.setFieldValue("filetype", i.type);
      formik.setFieldValue("sourcefile", event.target.value);
    }
  };

  const tradeRows = trades?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));

  const filteredRows = tradeRows?.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText.toLowerCase()))
    )
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  return (
    <>
      <ToastContainer />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Box>
            <Stack spacing={1} direction={"column"}>
              <Stack
                spacing={1}
                direction={"row"}
                justifyContent={"space-between"}
              >
                <TextField
                  variant="outlined"
                  size="small"
                  placeholder="Search..."
                  value={filterText}
                  onChange={handleFilterChange}
                  sx={{ minWidth: "300px" }}
                />
                <Button
                  variant="outlined"
                  size="small"
                  onClick={handleExportExcel}
                >
                  Export
                </Button>
              </Stack>

              <DataGrid
                sx={{
                  maxWidth: "100vw",
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                }}
                getRowId={(row) => row.trade_id}
                rows={filteredRows ? filteredRows : []}
                columns={tradeColumn}
                getRowHeight={() => "auto"}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                pageSizeOptions={[50]}
                disableRowSelectionOnClick
              />
            </Stack>
          </Box>
        </Grid>
      </Grid>
      <Modal open={openAtt}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            border: "1px solid #000",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <form onSubmit={formik.handleSubmit}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="h5" gutterBottom>
                Document
              </Typography>
              <IconButton
                sx={{ display: "flex", mt: -1 }}
                aria-label="close"
                onClick={() => setOpenAtt(false)}
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Divider sx={{ mb: 1 }} />
            <Stack direction="column" spacing={1}>
              <FormControl>
                <TextField
                  name="attachmentname"
                  fullWidth
                  variant="outlined"
                  size="small"
                  label="Name"
                  value={formik.values.attachmentname}
                  onChange={formik.handleChange}
                />
                <FormHelperText
                  error={
                    formik.touched.attachmentname &&
                    Boolean(formik.errors.attachmentname)
                  }
                >
                  {formik.touched.attachmentname &&
                    formik.errors.attachmentname}
                </FormHelperText>
              </FormControl>
              <FormControl size="small">
                <InputLabel id="attachmenttype">Type</InputLabel>
                <Select
                  name="attachmenttype"
                  label="Type"
                  value={formik.values.attachmenttype}
                  onChange={formik.handleChange}
                >
                  {types?.result.length > 0 &&
                    types?.result.map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.attachmenttype &&
                    Boolean(formik.errors.attachmenttype)
                  }
                >
                  {formik.touched.attachmenttype &&
                    formik.errors.attachmenttype}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="description"
                  label="Description"
                  multiline
                  rows={15}
                  variant="outlined"
                  value={formik.values.description}
                  onChange={formik.handleChange}
                />
                <FormHelperText
                  error={
                    formik.touched.description &&
                    Boolean(formik.errors.description)
                  }
                >
                  {formik.touched.description && formik.errors.description}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="filename"
                  label="Filename"
                  variant="outlined"
                  size="small"
                  disabled
                  value={formik.values.filename}
                  onChange={formik.handleChange}
                />
                <FormHelperText
                  error={
                    formik.touched.filename && Boolean(formik.errors.filename)
                  }
                >
                  {formik.touched.filename && formik.errors.filename}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Input type="file" name="file" onChange={uploadToClient} />

            <Divider sx={{ mt: 1 }} />
            <Box
              sx={{
                display: "flex",
                mb: 1,
                mt: 1,
                justifyContent: "space-between",
              }}
            >
              <Button variant="outlined" onClick={() => setOpenAtt(false)}>
                Close
              </Button>
              <Button variant="contained" color="dark" type="submit">
                Save
              </Button>
            </Box>
          </form>
        </Box>
      </Modal>
      <Modal open={opens}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
      <Drawer anchor="right" open={open} onClose={() => setOpen(false)}>
        <ToastContainer />
        <Box
          sx={{
            width: "38vw",
            padding: 3,
          }}
          role="presentation"
        >
          <Box
            sx={{
              // display: "flex",
              marginBottom: 1,
              // justifyContent: "space-between",
              mb: 1,
            }}
          >
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Document
            </Typography>
            <Typography
              id="modal-modal-title"
              variant="h7"
              component="h3"
              sx={{ color: "#0b626b" }}
            >
              T - {tradeID} / D - {dealID}
            </Typography>
            <Box
              sx={{
                mt: 1,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Button
                onClick={() => setOpen(false)}
                variant="text"
                startIcon={<ArrowBackIcon />}
              >
                Back
              </Button>
              <Button
                onClick={() => {
                  setOpenAtt(true);
                  formik.setValues({
                    id: null,
                    attachmentname: "",
                    attachmenttype: "",
                    description: "",
                    attachmenturl: "",
                    filename: "",
                    method: "POST",
                  });
                }}
                variant="text"
                startIcon={<AddSharpIcon />}
              >
                Add
              </Button>
            </Box>
          </Box>
          <Box sx={{ bgcolor: "white" }}>
            <DataGrid
              getRowId={(row) => row.trade_contract_id}
              rows={attachments?.result || []}
              columns={columns}
              fullWidth
              initialState={{
                pagination: {
                  paginationModel: {
                    pageSize: 5,
                  },
                },
              }}
              rowHeight={25}
              autoHeight
              slots={{
                noRowsOverlay: CustomNoRowsOverlay,
                // toolbar: CustomToolbar,
              }}
              sx={{
                backgroundColor: "#FFFFFF",
                m: 1,
                "& .MuiDataGrid-cell:hover": {
                  color: "primary.main",
                },
                "& .MuiDataGrid-cell": {
                  borderTop: 1,
                  borderTopColor: "#E9ECF0",
                },
              }}
              pageSizeOptions={[50]}
            />
          </Box>
        </Box>
      </Drawer>
    </>
  );
}
