"use client";
import * as React from "react";
import {
  DataGrid,
  GridToolbarContainer,
  GridToolbarExport,
} from "@mui/x-data-grid";
import {
  FormControl,
  CircularProgress,
  Select,
  MenuItem,
  Typography,
  InputLabel,
  Chip,
  Stack,
  Box,
  Card,
  Button,
} from "@mui/material";
import useSWR from "swr";
import useMediaQuery from '@mui/material/useMediaQuery';
import Indicator from "@/components/Indicator";
import MyAlert from "@/components/Myalert";
import { fetcher } from "@/lib/fetcher";
import { useSession } from "next-auth/react";
import CustomNoRowsOverlay from "@/components/NoRecord";
import { numberWithCommas } from "@/lib/numberWithCommas";
import dayjs from "dayjs";
import exportToExcel from "@/components/ExcelExport";

export default function Dashboard() {
  const { data: session } = useSession();
  const [item, setItem] = React.useState("1");

  const { data: trades } = useSWR(
    [`/api/calendar/trade/${session?.user?.user_id}`, session?.user?.token],
    fetcher
  );

  const { data: invoice } = useSWR(
    [`/api/calendar/invoice/${session?.user?.user_id}`, session?.user?.token],
    fetcher
  );

  const { data: singoff } = useSWR(
    [
      `/api/calendar/trade/signoff/${session?.user?.user_id}`,
      session?.user?.token,
    ],
    fetcher
  );

  const { data: debitor } = useSWR(
    [`/api/calendar/debitor/${session?.user?.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: onway } = useSWR(
    [`/api/calendar/onway/${session?.user?.user_id}`, session?.user?.token],
    fetcher
  );

  const { data: creditor } = useSWR(
    [`/api/calendar/creditor/${session?.user?.user_id}`, session?.user?.token],
    fetcher
  );

  const {
    data: parcel,
    error,
    isLoading,
  } = useSWR(
    [`/api/calendar/parcel/${session?.user?.user_id}`, session?.user?.token],
    fetcher
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );
  const handleExportParcel = () => {
    const parcelDate = [
      {
        header: "id",
      },
      {
        header: "parcel_number",
      },
      {
        header: "trade_id",
      },
      {
        header: "trade_info",
      },
      {
        header: "operation",
      },
      {
        header: "transfer_date",
      },
      {
        header: "transfer_detail_date",
      },
      {
        header: "border_crossing_date",
      },
      {
        header: "supply_start_date",
      },
      {
        header: "supply_end_date",
      },
      {
        header: "product_name",
      },
      {
        header: "grade_name",
      },
      {
        header: "shipping_quantity_mt",
      },
      {
        header: "units",
      },
      {
        header: "latestday",
      },
    ];
    exportToExcel(
      parcelrows,
      parcelDate,
      `ParcelReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportUnallocated = () => {
    const newAndUnallocatedTrades = [
      {
        header: "#",
      },
      {
        header: "deal_id",
      },
      {
        header: "trade_id",
      },
      {
        header: "company_name",
      },
      {
        header: "country_party_name",
      },
      {
        header: "trade_type_name",
      },
      {
        header: "date_range_from",
      },
      {
        header: "date_range_to",
      },
      {
        header: "transfer_date",
      },
      {
        header: "latestday",
      },
      {
        header: "vessel",
      },
      {
        header: "product_name",
      },
      {
        header: "grade_name",
      },
      {
        header: "price",
      },
      {
        header: "quantity",
      },
      {
        header: "allocated_quantity",
      },
      {
        header: "unallocated_quantity",
      },
      {
        header: "trader_name",
      },
      {
        header: "operator_name",
      },
      {
        header: "finance_name",
      },
      {
        header: "contract_name",
      },
    ];
    exportToExcel(
      unallocatedrows,
      newAndUnallocatedTrades,
      `UnallocatedTradeReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportInvoice = () => {
    const invoicing = [
      {
        header: "id",
      },
      {
        header: "trade_id",
      },
      {
        header: "trade_info",
      },
      {
        header: "counterparty_name",
      },
      {
        header: "credit_type_name",
      },
      {
        header: "transport_type",
      },
      {
        header: "invoice_number",
      },
      {
        header: "invoice_date",
      },
      {
        header: "invoice_type_name",
      },
      {
        header: "latestday",
      },
      {
        header: "invoice_due_date",
      },
      {
        header: "est_amount",
      },
      {
        header: "invoice_amount",
      },
      {
        header: "difference",
      },
      {
        header: "paid_amount",
      },
      {
        header: "unpaid_amount",
      },
      {
        header: "invoice_state",
      },
    ];
    exportToExcel(
      invoicerows,
      invoicing,
      `InvoiceReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportSignoff = () => {
    const notSignedTrades = [
      {
        header: "id",
      },
      {
        header: "trade_id",
      },
      {
        header: "deal_id",
      },
      {
        header: "company_name",
      },
      {
        header: "country_party_name",
      },
      {
        header: "product_name",
      },
      {
        header: "grade_name",
      },
      {
        header: "quantity",
      },
      {
        header: "price",
      },
      {
        header: "vessel",
      },
      {
        header: "operator_name",
      },
      {
        header: "trader_name",
      },
      {
        header: "finance_name",
      },
      {
        header: "contract_name",
      },
      {
        header: "date_range_from",
      },
      {
        header: "date_range_to",
      },
      {
        header: "transfer_date",
      },
      {
        header: "pricing_date",
      },
    ];
    exportToExcel(
      singoffrows,
      notSignedTrades,
      `SignOffReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportCredit = () => {
    const agedCreditors = [
      {
        header: "id",
      },
      {
        header: "invoice_number",
      },
      {
        header: "deal_id",
      },
      {
        header: "trade_id",
      },
      {
        header: "trade_info",
      },
      {
        header: "counterparty_name",
      },
      {
        header: "cost_name",
      },
      {
        header: "tran_date",
      },
      {
        header: "exp_pay_date",
      },
      {
        header: "latestday",
      },
      {
        header: "invoice_payment_status",
      },
      {
        header: "invoice_amount",
      },
      {
        header: "value",
      },
      {
        header: "description",
      },
    ];
    exportToExcel(
      creditorrows,
      agedCreditors,
      `CreditorsReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportDebit = () => {
    const agedDebtors = [
      {
        header: "id",
      },
      {
        header: "invoice_number",
      },
      {
        header: "deal_id",
      },
      {
        header: "trade_id",
      },
      {
        header: "trade_info",
      },
      {
        header: "counterparty_name",
      },
      {
        header: "cost_name",
      },
      {
        header: "tran_date",
      },
      {
        header: "exp_pay_date",
      },
      {
        header: "latestday",
      },
      {
        header: "invoice_payment_status",
      },
      {
        header: "invoice_amount",
      },
      {
        header: "value",
      },
      {
        header: "description",
      },
    ];

    exportToExcel(
      debitorrows,
      agedDebtors,
      `DebtorsReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const handleExportOnWay = () => {
    const onWay = [
      {
        header: "id",
      },
      {
        header: "terminal_name",
      },
      {
        header: "tank_name",
      },
      {
        header: "final_product_name",
      },
      {
        header: "final_grade_name",
      },
      {
        header: "final_balance_cm",
      },
      {
        header: "final_balance_mt",
      },
      {
        header: "final_density",
      },
    ];

    exportToExcel(
      onwayRows,
      onWay,
      `OnWayReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  const invoicecolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"id"}</strong>,
      width: 50,
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade ID"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.trade_id}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "trade_info",
      renderHeader: () => <strong>{"Trade Info"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.trade_info}</Typography>
        </Box>
      ),
      width: 250,
    },
    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.counterparty_name}</Typography>
        </Box>
      ),
      width: 180,
    },

    {
      field: "credit_type_name",
      renderHeader: () => <strong>{"Credit Type"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.credit_type_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "transport_type",
      renderHeader: () => <strong>{"Transportation"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.transport_type}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            <strong>
              {" "}
              {dayjs(params.row.invoice_date).format("YYYY-MM-DD")}
            </strong>
          </Typography>
          <Typography variant="h7">{params.row.invoice_number}</Typography>
        </Box>
      ),
      width: 150,
    },
    {
      field: "invoice_type_name",
      renderHeader: () => <strong>{"Invoice Type"}</strong>,
      renderCell: (params) => {
        if (params.row.invoice_type === "PO") {
          return (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.invoice_type_name}</strong>
            </Typography>
          );
        } else
          return (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.invoice_type_name}</strong>
            </Typography>
          );
      },
      width: 150,
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Expired day"}</strong>,

      renderCell: (params) => {
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
      width: 120,
    },
    {
      field: "invoice_due_date",
      renderHeader: () => <strong>{"Due Date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.invoice_due_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "est_amount",
      renderHeader: () => <strong>{"Est Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.est_amount)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "invoice_amount",
      renderHeader: () => <strong>{"Invoice Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.invoice_amount)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "difference",
      renderHeader: () => <strong>{"Difference"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.difference)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "paid_amount",
      renderHeader: () => <strong>{"Paid Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.paid_amount)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "unpaid_amount",
      renderHeader: () => <strong>{"Unpaid Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.unpaid_amount)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "invoice_state",
      renderHeader: () => <strong>{"Invoice State"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.invoice_state}</Typography>
        </Box>
      ),
      width: 130,
    },
  ];
  const onwayColumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"id"}</strong>,
      width: 50,
    },
    {
      field: "terminal_name",
      renderHeader: () => <strong>{"Terminal"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.terminal_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "tank_name",
      renderHeader: () => <strong>{"Tank"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.tank_name}</Typography>
        </Box>
      ),
      width: 250,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product/Grade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.final_product_name}</Typography>
          <Typography variant="h7">
            <strong>{params.row.final_grade_name}</strong>
          </Typography>
        </Box>
      ),
      width: 180,
    },

    {
      field: "final_balance_cm",
      renderHeader: () => <strong>{"Balance(CM)"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.final_balance_cm}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "final_balance_mt",
      renderHeader: () => <strong>{"Balance(MT)"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.final_balance_mt}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "final_density",
      renderHeader: () => <strong>{"Density"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.final_density}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "transfer_completion_date",
      renderHeader: () => <strong>{"Transfer last date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.transfer_completion_date.format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 150,
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Idle day"}</strong>,
      width: 200,
      renderCell: (params) => {
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
    },
  ];
  const parcelcolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"id"}</strong>,
      width: 50,
    },
    {
      field: "parcel_number",
      renderHeader: () => <strong>{"Parcel/Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.parcel_number}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_id} </strong>
          </Typography>
        </Box>
      ),
      width: 200,
    },
    {
      field: "trade_info",
      renderHeader: () => <strong>{"Trade Info"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.trade_info}</Typography>
        </Box>
      ),
      width: 320,
    },
    {
      field: "operation",
      renderHeader: () => <strong>{"Operation Dates"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {"BL: " + dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"TRN: " +
              dayjs(params.row.transfer_detail_date).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"NOR: " +
              dayjs(params.row.border_crossing_date).format("YYYY-MM-DD")}
          </Typography>

          <Typography variant="h7">
            {"COD START: " +
              dayjs(params.row.supply_start_date).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"COD END: " +
              dayjs(params.row.supply_end_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 180,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 240,
    },
    {
      field: "shipping_quantity_mt",
      renderHeader: () => <strong>{"Quantity(MT)"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">
            {numberWithCommas(params.row.shipping_quantity_mt)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "shipping_quantity_cm",
      renderHeader: () => <strong>{"Quantity(CM)"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">
            {numberWithCommas(params.row.shipping_quantity_cm)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Latest day"}</strong>,
      width: 200,
      renderCell: (params) => {
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
    },
  ];
  const unallocatedcolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 10,
    },
    {
      field: "deal_id",
      renderHeader: () => <strong>{"Deal/Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.deal_id!=="D- null"?params.row.deal_id:"***"}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_id} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "company_name",
      renderHeader: () => <strong>{"Company"}</strong>,
      width: 200,
    },
    {
      field: "country_party_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.country_party_name}</Typography>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}
        </Box>
      ),
      width: 200,
    },
    {
      field: "date_range_from",
      renderHeader: () => <strong>{"Operation Date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {"From: " + dayjs(params.row.date_range_from).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"To: " + dayjs(params.row.date_range_to).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"BL: " + dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 150,
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Expired day"}</strong>,
      width: 120,
      renderCell: (params) => {
        if (!params.row.latestday) {
          return null; // Return null if the latestday is empty
        }
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
    },
    {
      field: "vessel",
      renderHeader: () => <strong>{"Transportation"}</strong>,
      width: 120,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>

          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 180,
    },
    {
      field: "price",
      renderHeader: () => <strong>{"Total amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.price)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity/Unit"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">
            {numberWithCommas(params.row.quantity)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },

    {
      field: "allocated_quantity",
      renderHeader: () => <strong>{"Allocated"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">
            {numberWithCommas(params.row.allocated_quantity)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },

    {
      field: "unallocated_quantity",
      renderHeader: () => <strong>{"Unallocated"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">
            {numberWithCommas(params.row.unallocated_quantity)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },

    {
      field: "trader_name",
      renderHeader: () => <strong>{"Trader"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">{params.row.trader_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "operator_name",
      renderHeader: () => <strong>{"Operator"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">{params.row.operator_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "finance_name",
      renderHeader: () => <strong>{"Finance"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">{params.row.finance_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "contract_name",
      renderHeader: () => <strong>{"Contract Admin"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">{params.row.contract_name}</Typography>
        </Box>
      ),
      width: 120,
    },
  ];
  const singoffcolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 10,
    },
    {
      field: "deal_id",
      renderHeader: () => <strong>{"Deal/Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
               <Typography variant="h7">{params.row.deal_id!=="D- null"?params.row.deal_id:"***"}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_id} </strong>
          </Typography>
        </Box>
      ),
      width: 150,
    },

    {
      field: "company_name",
      renderHeader: () => <strong>{"Company Name"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.company_name}</Typography>
        </Box>
      ),
      width: 220,
    },
    {
      field: "country_party_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.country_party_name}</Typography>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}
        </Box>
      ),
      width: 200,
    },

    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>

          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 150,
    },

    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.quantity)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "price",
      renderHeader: () => <strong>{"Total amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.price)}
          </Typography>
        </Box>
      ),
      width: 120,
      align: "right",
    },
    {
      field: "vessel",
      renderHeader: () => <strong>{"Transportation"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.vessel}</Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "operator_name",
      renderHeader: () => <strong>{"Trader"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">{params.row.operator_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "trader_name",
      renderHeader: () => <strong>{"Operator"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.trader_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.operation_flag} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "finance_name",
      renderHeader: () => <strong>{"Finance"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.finance_name}</Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.finance_flag} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "contract_name",
      renderHeader: () => <strong>{"Contract Admin"}</strong>,
      renderCell: (params) => (
        <Box>
          <Typography variant="h7">{params.row.contract_name}</Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "date_range_from",
      renderHeader: () => <strong>{"Operation Date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {"FROM: " + dayjs(params.row.date_range_from).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"TO: " + dayjs(params.row.date_range_to).format("YYYY-MM-DD")}
          </Typography>
          <Typography variant="h7">
            {"BL: " + dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 150,
    },
    {
      field: "pricing_date",
      renderHeader: () => <strong>{"Fixed Date"}</strong>,
      renderCell: (params) => {
        if (params.row.pricing_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.pricing_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 150,
    },
  ];
  const debitorcolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 10,
    },
    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice number"}</strong>,
      width: 120,
    },
    {
      field: "deal_id",
      renderHeader: () => <strong>{"Deal/Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          {params.row.deal_id ? (
            <Typography variant="h7">{params.row.deal_id}</Typography>
          ) : null}
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_id} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "trade_info",
      renderHeader: () => <strong>{"Trade info"}</strong>,
      width: 250,
    },
    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      width: 180,
    },
    {
      field: "invoice_payment_status",
      renderHeader: () => <strong>{"Invoice status"}</strong>,
      width: 100,
    },
    {
      field: "invoice_amount",
      renderHeader: () => <strong>{"Invoice amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.invoice_amount)}
          </Typography>
        </Box>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "cost_name",
      renderHeader: () => <strong>{"Cost name"}</strong>,
      width: 140,
    },

    {
      field: "tran_date",
      renderHeader: () => <strong>{"Tran date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.tran_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "exp_pay_date",
      renderHeader: () => <strong>{"Exp date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.exp_pay_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Expired day"}</strong>,

      renderCell: (params) => {
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
      width: 120,
    },

    {
      field: "value",
      renderHeader: () => <strong>{"Value"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.value)}
          </Typography>
        </Box>
      ),
      width: 100,
      align: "right",
    },
    {
      field: "unpaid_amount",
      renderHeader: () => <strong>{"Unpaid Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.unpaid_amount)}
          </Typography>
        </Box>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 200,
    },
  ];
  const creditorcolumns = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 10,
    },
    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice number"}</strong>,
      width: 120,
    },
    {
      field: "deal_id",
      renderHeader: () => <strong>{"Deal/Trade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          {params.row.deal_id ? (
            <Typography variant="h7">{params.row.deal_id}</Typography>
          ) : null}
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.trade_id} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "trade_info",
      renderHeader: () => <strong>{"Trade info"}</strong>,
      width: 250,
    },
    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      width: 180,
    },
    {
      field: "invoice_payment_status",
      renderHeader: () => <strong>{"Invoice status"}</strong>,
      width: 100,
    },
    {
      field: "invoice_amount",
      renderHeader: () => <strong>{"Invoice amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.invoice_amount)}
          </Typography>
        </Box>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "cost_name",
      renderHeader: () => <strong>{"Cost name"}</strong>,
      width: 140,
    },

    {
      field: "tran_date",
      renderHeader: () => <strong>{"Tran date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.tran_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "exp_pay_date",
      renderHeader: () => <strong>{"Exp pay date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {dayjs(params.row.exp_pay_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
    },
    {
      field: "latestday",
      renderHeader: () => <strong>{"Expired day"}</strong>,

      renderCell: (params) => {
        if (parseInt(params.row.latestday) < -1) {
          return (
            <Chip
              variant="filled"
              color="regular"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) >= 0 &&
          parseInt(params.row.latestday) <= 1
        ) {
          return (
            <Chip
              variant="filled"
              color="medium"
              label={params.row.latestday}
            />
          );
        }
        if (
          parseInt(params.row.latestday) <= 5 &&
          parseInt(params.row.latestday) > 1
        ) {
          return (
            <Chip variant="filled" color="high" label={params.row.latestday} />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="urgent"
              label={params.row.latestday}
            />
          );
      },
      width: 120,
    },

    {
      field: "value",
      renderHeader: () => <strong>{"Value"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.value)}
          </Typography>
        </Box>
      ),
      width: 100,
      align: "right",
    },
    {
      field: "unpaid_amount",
      renderHeader: () => <strong>{"Unpaid Amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.unpaid_amount)}
          </Typography>
        </Box>
      ),
      width: 110,
      align: "right",
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 200,
    },
  ];
  const unallocatedrows =
    trades?.result?.map((row, i) => ({
      id: i + 1,
      deal_id: "D- " + row.deal_id,
      trade_id: "T- " + row.trade_id,
      company_name: row.company_name,
      country_party_name: row.country_party_name,
      trade_type_name: row.trade_type_name,
      date_range_from: row.date_range_from,
      date_range_to: row.date_range_to,
      transfer_date: row.transfer_date,
      latestday: row.latestday,
      vessel: row.vessel,
      product_name: row.product_name,
      grade_name: row.grade_name,
      price: row.price,
      quantity: row.quantity,
      allocated_quantity: row.allocated_quantity,
      unallocated_quantity: row.unallocated_quantity,
      trader_name: row.trader_name,
      operator_name: row.operator_name,
      finance_name: row.finance_name,
      contract_name: row.contract_name,
    })) || [];

  const parcelrows =
    parcel?.result?.map((row, i) => ({
      id: i + 1,
      parcel_number: row.parcel_number,
      trade_id: "T- " + row.trade_id,
      trade_info: row.trade_info,
      operation: row.operation,
      transfer_date: row.transfer_date,
      transfer_detail_date: row.transfer_detail_date,
      border_crossing_date: row.border_crossing_date,
      supply_start_date: row.supply_start_date,
      supply_end_date: row.supply_end_date,
      product_name: row.product_name,
      grade_name: row.grade_name,
      shipping_quantity_mt: row.shipping_quantity_mt,
      shipping_quantity_cm: row.shipping_quantity_cm,
      units: row.units,
      unit_name: row.unit_name,
      latestday: row.latestday,
    })) || [];

  const invoicerows =
    invoice?.result?.map((row, i) => ({
      id: i + 1,
      trade_id: "T- " + row.trade_id,
      trade_info: row.trade_info,
      counterparty_name: row.counterparty_name,
      credit_type_name: row.credit_type_name,
      transport_type: row.transport_type,
      invoice_number: row.invoice_number,
      invoice_date: row.invoice_date,
      invoice_type_name: row.invoice_type_name,
      latestday: row.latestday,
      invoice_due_date: row.invoice_due_date,
      est_amount: row.est_amount,
      invoice_amount: row.invoice_amount,
      difference: row.difference,
      paid_amount: row.paid_amount,
      unpaid_amount: row.unpaid_amount,
      invoice_state: row.invoice_state,
    })) || [];

  const singoffrows =
    singoff?.result?.map((row, i) => ({
      id: i + 1,
      trade_id: "T- " + row.trade_id,
      deal_id: "D- " + row.deal_id,
      company_name: row.company_name,
      country_party_name: row.country_party_name,
      product_name: row.product_name,
      grade_name: row.grade_name,
      quantity: row.quantity,
      price: row.price,
      vessel: row.vessel,
      operator_name: row.operator_name,
      trader_name: row.trader_name,
      finance_name: row.finance_name,
      contract_name: row.contract_name,
      date_range_from: row.date_range_from,
      date_range_to: row.date_range_to,
      transfer_date: row.transfer_date,
      pricing_date: row.pricing_date,
    })) || [];

  const debitorrows =
    debitor?.result?.map((row, i) => ({
      id: i + 1,
      invoice_number: row.invoice_number,
      deal_id: "D- " + row.deal_id,
      trade_id: "T- " + row.trade_id,
      trade_info: row.trade_info,
      counterparty_name: row.counterparty_name,
      cost_name: row.cost_name,
      tran_date: row.tran_date,
      exp_pay_date: row.exp_pay_date,
      latestday: row.latestday,
      invoice_payment_status: row.invoice_payment_status,
      invoice_amount: row.invoice_amount,
      value: row.value,
      description: row.description,
      unpaid_amount: row.unpaid_amount,
    })) || [];

  const creditorrows =
    creditor?.result?.map((row, i) => ({
      id: i + 1,
      invoice_number: row.invoice_number,
      deal_id: "D- " + row.deal_id,
      trade_id: "T- " + row.trade_id,
      trade_info: row.trade_info,
      counterparty_name: row.counterparty_name,
      cost_name: row.cost_name,
      tran_date: row.tran_date,
      exp_pay_date: row.exp_pay_date,
      latestday: row.latestday,
      invoice_payment_status: row.invoice_payment_status,
      invoice_amount: row.invoice_amount,
      value: row.value,
      description: row.description,
      unpaid_amount: row.unpaid_amount,
    })) || [];
  const onwayRows =
    onway?.result?.map((row, i) => ({
      id: i + 1,
      terminal_name: row.terminal_name,
      tank_name: row.tank_name,
      final_product_name: row.final_product_name,
      final_grade_name: row.final_grade_name,
      final_balance_cm: row.final_balance_cm,
      final_balance_mt: row.final_balance_mt,
      final_density: row.final_density,
    })) || [];

  return (
    <>
      {/* <Box>
        <Stack direction="row" spacing={2} sx={{ mb: 1 }}>
          <Indicator
            menu={"/operation"}
            title={"Voyage parcel"}
            total={"0"}
            urgent={"5"}
            high={"1"}
            medium={"2"}
            regular={"6"}
          />
          <Indicator
            menu={"/storage"}
            title={"Storage"}
            total={"2"}
            urgent={"5"}
            high={"1"}
            medium={"2"}
            regular={"6"}
          />
          <Indicator
            menu={"/finance"}
            title={"Invoices"}
            total={"0"}
            urgent={"5"}
            high={"1"}
            medium={"2"}
            regular={"6"}
          />
          <Indicator
            menu={"/trade"}
            title={"Trades"}
            total={"1"}
            urgent={"5"}
            high={"1"}
            medium={"2"}
            regular={"6"}
          />
          <Indicator
            menu={"/contract"}
            title={"Contracts"}
            total={"6"}
            urgent={"5"}
            high={"1"}
            medium={"2"}
            regular={"6"}
          />
        </Stack>
      </Box> */}
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <FormControl sx={{ mt: 1, mb: 1, minWidth: "20vh" }} size="small">
          <InputLabel id="choice-select-label">Task calendars</InputLabel>
          <Select
            labelId="choice-select-label"
            label="Task calendars"
            id="task_calendars"
            name="task_calendars"
            value={item}
            onChange={(e) => {
              setItem(e.target.value);
            }}
          >
            <MenuItem value={1}>New and unallocated trades</MenuItem>
            <MenuItem value={2}>Not Signed Trades</MenuItem>
            <MenuItem value={3}>Parcel date</MenuItem>
            <MenuItem value={4}>Invoicing</MenuItem>
            <MenuItem value={5}>Aged Creditors</MenuItem>
            <MenuItem value={6}>Aged Debtors</MenuItem>
            <MenuItem value={7}>On Way</MenuItem>
          </Select>
        </FormControl>
      </Stack>
      <Box sx={{ maxWidth: "93wh" }}>
        <Box>
          {item === 2 ? (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>
                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportSignoff}
                  >
                    Export
                  </Button>
                </Stack>
                <DataGrid
                  rows={singoffrows}
                  columns={singoffcolumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />{" "}
              </Stack>
            </Box>
          ) : item === 3 ? (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>
                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportParcel}
                  >
                    Export
                  </Button>
                </Stack>
                <DataGrid
                  rows={parcelrows}
                  columns={parcelcolumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />
              </Stack>
            </Box>
          ) : item === 4 ? (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>
                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportInvoice}
                  >
                    Export
                  </Button>
                </Stack>
                <DataGrid
                  rows={invoicerows}
                  columns={invoicecolumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />
              </Stack>
            </Box>
          ) : item === 5 ? (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>
                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportCredit}
                  >
                    Export
                  </Button>
                </Stack>
                <DataGrid
                  rows={creditorrows}
                  columns={creditorcolumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />
              </Stack>
            </Box>
          ) : item === 6 ? (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>
                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportDebit}
                  >
                    Export
                  </Button>
                </Stack>
                <DataGrid
                  rows={debitorrows}
                  columns={debitorcolumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />{" "}
              </Stack>
            </Box>
          ) : item === 7 ? (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>
                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportOnWay}
                  >
                    Export
                  </Button>
                </Stack>
                <DataGrid
                  rows={onwayRows}
                  columns={onwayColumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />
              </Stack>
            </Box>
          ) : (
            <Box>
              <Stack direction="column" spacing={1} sx={{ mb: 1 }}>
                <Stack
                  direction="row"
                  spacing={1}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Chip variant="filled" label="Urgent" color="urgent"></Chip>
                    <Chip variant="filled" label="High" color="high"></Chip>
                    <Chip variant="filled" label="Medium" color="medium"></Chip>
                    <Chip
                      variant="filled"
                      label="Regular"
                      color="regular"
                    ></Chip>
                  </Box>

                  <Button
                    variant="outlined"
                    size="small"
                    sx={{ mb: 1 }}
                    onClick={handleExportUnallocated}
                  >
                    Export
                  </Button>
                </Stack>

                <DataGrid
                  rows={unallocatedrows}
                  columns={unallocatedcolumns}
                  style={{ background: "white" }}
                  sx={{
                    backgroundColor: "#FFFFFF",
                    "& .MuiDataGrid-cell:hover": {
                      color: "primary.main",
                    },
                    "& .MuiDataGrid-cell": {
                      borderTop: 1,
                      borderTopColor: "#E9ECF0",
                    },
                    borderRadius: 0,
                      maxWidth: "92.5vw",
                    maxHeight: "65vh",
                    minHeight: "64vh",
                  }}
                  getRowHeight={() => "auto"}
                  pageSizeOptions={[50]}
                  slots={{
                    noRowsOverlay: CustomNoRowsOverlay,
                  }}
                />
              </Stack>
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
}
