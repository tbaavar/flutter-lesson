"use client";
import * as React from "react";
import {
  Box,
  CircularProgress,
  Typography,
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  Button,
  Select,
  MenuItem,
  TextField,
  Stack,
  Divider,
  IconButton,
  FormHelperText,
  Modal,
  Autocomplete,
} from "@mui/material";
import * as yup from "yup";
import { useFormik } from "formik";
import useSWR from "swr";
import { fetchData } from "@/lib/fetch";
import { toast } from "react-toastify";
import dayjs from "dayjs";
import CloseIcon from "@mui/icons-material/Close";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import { numberWithCommas } from "@/lib/numberWithCommas";

export default function Load({ rowData, handleModalClose, method }) {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [companyId, setCompanyId] = React.useState("");
  const [tankName, setTankName] = React.useState(
    rowData?.tank_name ? rowData.tank_name : ""
  );
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: voyage_title } = useSWR(
    [
      `/api/storage/voyage/${session?.user.user_id}/Discharge_port`,
      session?.user?.token,
    ],
    fetcher
  );
  const { data: tank } = useSWR(
    companyId
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${companyId}`,
          session?.user?.token,
        ]
      : rowData
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${rowData.org_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
console.log("dadawdawdawdawdawdaw",tank)
  const {
    data: status,
    error,
    isLoading,
  } = useSWR(["/api/lookup/Adjustment_status", session?.user?.token], fetcher);

  const formik = useFormik({
    validateOnMount: true,
    initialValues: {
      company_id: rowData?.org_id ?? "",
      parcel_movement_id: rowData?.parcel_movement_id ?? "",
      voyage_id: rowData?.voyage_id ?? "",
      voyage_parcel_id: rowData?.voyage_parcel_id ?? "",
      movement_parcel_id: rowData?.movement_parcel_id ?? "",
      product_id: rowData?.product_id ?? "",
      product_name: rowData?.product_name ?? "",
      grade_id: rowData?.grade_id ?? "",
      grade_name: rowData?.grade_name ?? "",
      country_origin: rowData?.country_origin ?? "",
      country_origin_name: rowData?.country_origin_name ?? "",
      unit: rowData?.unit ?? "",
      unit_name: rowData?.unit_name ?? "",
      counterparty_id: rowData?.counterparty_id ?? "",
      counterparty_name: rowData?.counterparty_name ?? "",
      est_density: rowData?.est_density ?? "",
      shipping_quantity_cm: rowData?.shipping_quantity_cm ?? "",
      shipping_quantity_mt: rowData?.shipping_quantity_mt ?? "",
      quantity_cm: rowData?.quantity_cm ?? "",
      quantity_mt: rowData?.quantity_mt ?? "",
      parcel_comment: rowData?.parcel_comment ?? "",
      transfer_status: rowData?.transfer_status ?? "",
      superintendent_id: rowData?.superintendent_id ?? "",
      superveyor_id: rowData?.superveyor_id ?? "",
      source_terminal_id: rowData?.source_terminal_id ?? "",
      source_tank_id: rowData?.source_tank_id ?? "",
      quantity: rowData?.quantity ?? "",
      clean_tank_flag: rowData?.clean_tank_flag ?? "N",
      third_party_stock_flag: rowData?.third_party_stock_flag ?? "N",
      transfer_completion_date:
        rowData?.transfer_completion_date ?? dayjs().format("YYYY-MM-DD"),
      transfer_completion_date_est_fl:
        rowData?.transfer_completion_date_est_fl ?? "N",
      terminal_from_date:
        rowData?.terminal_from_date ?? dayjs().format("YYYY-MM-DD"),
      terminal_to_date:
        rowData?.terminal_to_date ?? dayjs().format("YYYY-MM-DD"),
      tank_comment: rowData?.tank_comment ?? "",
      tank_product: rowData?.tank_product ?? "",
      tank_grade: rowData?.tank_grade ?? "",
      tank_balance_cm: rowData?.tank_balance_cm ?? "",
      tank_balance_mt: rowData?.tank_balance_mt ?? "",
      tank_capacity: rowData?.tank_capacity ?? "",
      tank_density: rowData?.tank_density ?? "",
      units_name: rowData?.units_name ?? "",
    },

    validationSchema: yup.object({
      company_id: yup.string().required("Please select Company!"),
      voyage_id: yup.string().required("Please select voyage!"),
      movement_parcel_id: yup.string().required("Please select parcel!"),
      transfer_status: yup.string().required("Please select status!"),
      source_terminal_id: yup.string().required("Please select terminal!"),
      source_tank_id: yup.string().required("Please select tank!"),
      est_density: yup
        .number()
        .typeError("Please insert a number!")
        .required("Please insert density!")
        .min(0, "Density can't be a negative!")
        .max(1, "Density can't be higher than 1!"),
      quantity: yup
        .number()
        .min(0, "Quantity can't be a negative number!")
        .max(
          yup.ref("tank_balance_mt"),
          "Quantity can't be higher than Tank Balance"
        )
        .required("Please insert quantity!"),
    }),
    onSubmit: async (values) => {
      saveLoadData(values);
    },

    enableReinitialize: true,
  });

  const saveLoadData = async (data) => {
    const body = {
      company_id: data.company_id,
      voyage_id: data.voyage_id,
      voyage_parcel_id: data.voyage_parcel_id,
      transfer_type: "Load",
      product_id: data.product_id,
      grade_id: data.grade_id,
      country_origin: data.country_origin,
      parcel_comment: data.parcel_comment,
      transfer_status: data.transfer_status,
      superintendent_id: data.superintendent_id,
      superveyor_id: data.superveyor_id,
      movement_id: data.movement_id,
      transfer_completion_date: data.transfer_completion_date,
      transfer_completion_date_est_fl: data.transfer_completion_date_est_fl,
      terminal_from_date: data.terminal_from_date,
      terminal_to_date: data.terminal_to_date,
      source_terminal_id: data.source_terminal_id,
      source_tank_id: data.source_tank_id,
      target_terminal_id: data.source_terminal_id,
      target_tank_id: data.source_tank_id,
      quantity_mt: data.quantity_mt,
      quantity_cm: data.quantity_cm,
      quantity_b: 0,
      tank_comment: data.tank_comment,
      clean_tank_flag: data.clean_tank_flag,
      third_party_stock_flag: data.third_party_stock_flag,
      transfer_grade_id: "",
      blend_flag: "N",
      movement_parcel_id: data.movement_parcel_id,
      counterparty_id: data.counterparty_id,
      final_product_id: data.product_id,
      final_grade_id: data.grade_id,
      circulation: null,
      final_density: data.est_density,
      blend_ratio: "",
      balance_mt:
        parseFloat(data.tank_balance_mt) - parseFloat(data.quantity_mt),
      balance_cm:
        parseFloat(data.tank_balance_cm) - parseFloat(data.quantity_cm),
      balance_b: 0,
      units: data.unit,
      trade_code: "S",
      adjustment_type: "Standard",
      username: session?.user?.username,
    };
    console.log("dwadawdaw", body);
    try {
      setLoading(true);
      setOpen(true);
      const method = rowData ? "PUT" : "POST";
      const res = await fetchData(
        `/api/storage`,
        method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleModalClose();
    }
  };

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const label = { inputProps: { "aria-label": "Checkbox demo" } };

  const options =
    voyage_title?.result?.length > 0 &&
    voyage_title?.result?.map((option) => {
      return option;
    });
  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5" gutterBottom>
          Load
        </Typography>
        <IconButton
          sx={{ display: "flex", mt: -1 }}
          onClick={handleModalClose}
          aria-label="close"
        >
          <CloseIcon />
        </IconButton>
      </Box>
      <Divider sx={{ mb: 1 }} />
      <form onSubmit={formik.handleSubmit}>
        <Box
          component="fieldset"
          sx={{
            m: 1,
            borderRadius: 1,
            borderColor: "#e6e6e6",
          }}
        >
          <legend> Voyage Details</legend>
          <Stack direction="column" spacing={1}>
            <Stack direction="row" spacing={1}>
              {method === "POST" ? (
                <FormControl fullWidth>
                  <Autocomplete
                    fullWidth
                    size="small"
                    options={options}
                    getOptionLabel={(option) => option.voyage_title}
                    onChange={(event, value) => {
                      formik.setFieldValue(
                        "voyage_id",
                        value ? value.voyage_id : ""
                      );
                      if (value) {
                        formik.setValues({
                          ...formik.values,
                          voyage_id: value.voyage_id,
                          voyage_title: value.voyage_title,
                        });
                      }
                    }}
                    value={formik.values.voyage_title}
                    disabled={rowData ? true : false}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Voyage"
                        InputLabelProps={{ sx: { color: "#0B626B" } }}
                      />
                    )}
                  />
                  <FormHelperText
                    error={
                      formik.touched.voyage_id &&
                      Boolean(formik.errors.voyage_id)
                    }
                  >
                    {formik.touched.voyage_id && formik.errors.voyage_id}
                  </FormHelperText>
                </FormControl>
              ) : (
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Voyage
                  </InputLabel>
                  <Select
                    value={formik.values.voyage_id}
                    name="voyage_id"
                    label="voyage"
                    size="small"
                    disabled={rowData ? true : false}
                  >
                    {voyage_title?.result.length >= 0 &&
                      voyage_title?.result.map((item) => (
                        <MenuItem value={item.voyage_id} key={item.voyage_id}>
                          {item.voyage_title}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              )}

              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Parcel
                </InputLabel>
                <Select
                  value={formik.values.movement_parcel_id}
                  name="movement_parcel_id"
                  label="Parcel"
                  onChange={(e) => {
                    const selectedItemId = e.target.value;
                    const allItems = voyage_title?.result;

                    const selParcel = allItems.filter((item) => {
                      return item.parcels.find(
                        (i) => i.voyage_parcel_id === selectedItemId
                      );
                    });
                    const selectedItem = selParcel[0].parcels.find(
                      (item) => item.voyage_parcel_id == selectedItemId
                    );

                    if (selectedItem) {
                      formik.setValues({
                        ...formik.values,
                        final_product_id: selectedItem.product_id,
                        final_grade_id: selectedItem.grade_id,
                        product_id: selectedItem.product_id,
                        product_name: selectedItem.product_name,
                        grade_id: selectedItem.grade_id,
                        grade_name: selectedItem.grade_name,
                        shipping_quantity_cm: selectedItem.shipping_quantity_cm,
                        shipping_quantity_mt: selectedItem.shipping_quantity_mt,
                        transfer_product_id: selectedItem.product_id,
                        transfer_grade_id: selectedItem.grade_id,
                        unit: selectedItem.units,
                        unit_name: selectedItem.unit_name,
                        est_density: selectedItem.est_density,
                        country_origin: selectedItem.country_origin,
                        country_origin_name: selectedItem.country_origin_name,
                        counterparty_name: selectedItem.counterparty_name,
                        movement_parcel_id: selectedItemId,
                        counterparty_id: selectedItem.counterparty_id,
                      });
                    }
                  }}
                  size="small"
                  disabled={rowData ? true : false}
                >
                  {voyage_title?.result.filter(
                    (f) => f.voyage_id === formik.values.voyage_id
                  ).length > 0 &&
                    voyage_title?.result
                      .filter((f) => f.voyage_id === formik.values.voyage_id)[0]
                      .parcels.map((item) => (
                        <MenuItem
                          value={item.voyage_parcel_id}
                          key={item.voyage_parcel_id}
                        >
                          {item.shipping_quantity_mt +
                            " " +
                            item.invoicing_unit +
                            "-" +
                            item.product_name +
                            " " +
                            item.grade_name +
                            " -<" +
                            item.trade_code +
                            "> -" +
                            item.counterparty_name +
                            "-" +
                            dayjs(item.transfer_date).format("YYYY-MM-DD") +
                            " [" +
                            item.parcel_number +
                            " ]"}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.movement_parcel_id &&
                    Boolean(formik.errors.movement_parcel_id)
                  }
                >
                  {formik.touched.movement_parcel_id &&
                    formik.errors.movement_parcel_id}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="product_name"
                  readOnly={true}
                  value={formik.values.product_name}
                  size="small"
                  label="Product"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="grade_name"
                  readOnly={true}
                  value={formik.values.grade_name}
                  size="small"
                  label="Grade"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="country_origin_name"
                  readOnly={true}
                  value={formik.values.country_origin_name}
                  size="small"
                  label="Country of Origin"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="counterparty_name"
                  readOnly={true}
                  value={formik.values.counterparty_name}
                  size="small"
                  label="Counterparty"
                />
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="unit_name"
                  readOnly={true}
                  value={formik.values.unit_name}
                  size="small"
                  label="Unit Measurement"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="shipping_quantity_cm"
                  readOnly={true}
                  value={numberWithCommas(formik.values.shipping_quantity_cm)}
                  size="small"
                  label="Cubic metric qty"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="shipping_quantity_mt"
                  readOnly={true}
                  value={numberWithCommas(formik.values.shipping_quantity_mt)}
                  size="small"
                  label="Metric tonnes qty"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="est_density"
                  value={numberWithCommas(formik.values.est_density)}
                  size="small"
                  label="Density"
                  onChange={(e) => {
                    formik.setFieldValue("est_density", e.target.value);
                    if (formik.values.unit_name === "Cubic Meters") {
                      formik.setFieldValue("quantity_cm", e.target.value);
                      formik.setFieldValue(
                        "quantity_mt",
                        e.target.value / formik.values.est_density
                      );
                    } else {
                      formik.setFieldValue("quantity_mt", e.target.value);
                      formik.setFieldValue(
                        "quantity_cm",
                        e.target.value * formik.values.est_density
                      );
                    }
                  }}
                  onBlur={formik.handleBlur}
                />
                <FormHelperText
                  error={
                    formik.touched.est_density &&
                    Boolean(formik.errors.est_density)
                  }
                >
                  {formik.touched.est_density && formik.errors.est_density}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  id="parcel_comment"
                  name="parcel_comment"
                  value={formik.values.parcel_comment}
                  onChange={formik.handleChange}
                  size="small"
                  label="Comment"
                />
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Status
                </InputLabel>
                <Select
                  value={formik.values.transfer_status}
                  name="transfer_status"
                  label="status"
                  onChange={formik.handleChange}
                  size="small"
                >
                  {status?.result.length >= 0 &&
                    status?.result.map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.transfer_status &&
                    Boolean(formik.errors.transfer_status)
                  }
                >
                  {formik.touched.transfer_status &&
                    formik.errors.transfer_status}
                </FormHelperText>
              </FormControl>
            </Stack>
          </Stack>
        </Box>

        <Box
          component="fieldset"
          sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
        >
          <legend>Source Tank</legend>
          <Stack direction="column" spacing={1}>
            <FormControl fullWidth size="small">
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Company
              </InputLabel>
              <Select
                labelId="company_id"
                id="company_id"
                name="company_id"
                value={formik.values.company_id}
                label="Company"
                onChange={(e) => {
                  formik.setFieldValue("company_id", e.target.value);
                  setCompanyId(e.target.value);
                }}
              >
                {org?.result.length >= 0 &&
                  org?.result.map((item) => (
                    <MenuItem value={item.org_id} key={item.org_id}>
                      {item.org_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.company_id && Boolean(formik.errors.company_id)
                }
              >
                {formik.touched.company_id && formik.errors.company_id}
              </FormHelperText>
            </FormControl>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Terminal
                </InputLabel>
                <Select
                  name="source_terminal_id"
                  size="small"
                  label="Terminal"
                  disabled={rowData ? true : false}
                  value={formik.values.source_terminal_id}
                  onChange={formik.handleChange}
                >
                  {tank?.result.length > 0 &&
                    tank?.result.map((item) => (
                      <MenuItem value={item.terminal_id} key={item.terminal_id}>
                        {item.terminal_name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.source_terminal_id &&
                    Boolean(formik.errors.source_terminal_id)
                  }
                >
                  {formik.touched.source_terminal_id &&
                    formik.errors.source_terminal_id}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Tank
                </InputLabel>
                <Select
                  name="source_tank_id"
                  size="small"
                  label="Tank"
                  disabled={rowData ? true : false}
                  value={formik.values.source_tank_id}
                  onChange={(e) => {
                    const selectedTankId = e.target.value;

                    const alltanks = tank?.result;
                    const selTank = alltanks.filter((item) => {
                      return item.tanks.find(
                        (i) => i.tank_id === selectedTankId
                      );
                    });
                    const selectedTank = selTank[0].tanks.find(
                      (item) => item.tank_id == selectedTankId
                    );
                    setTankName(selectedTank.tank_name);

                    if (selectedTank) {
                      formik.setValues({
                        ...formik.values,
                        source_tank_id: selectedTank.tank_id,
                        tank_product: selectedTank.product_name,
                        tank_grade: selectedTank.grade_name,
                        tank_capacity: selectedTank.capacity,
                        tank_balance_cm: selectedTank.balance_cm,
                        tank_balance_mt: selectedTank.balance_mt,
                        tank_density: selectedTank.denstity,
                        units_name: selectedTank.unit_name,
                      });
                    }
                    formik.setFieldValue("quantity", 0);
                  }}
                >
                  {tank?.result.filter(
                    (f) => f.terminal_id === formik.values.source_terminal_id
                  ).length > 0 &&
                    tank?.result
                      .filter(
                        (f) =>
                          f.terminal_id === formik.values.source_terminal_id
                      )[0]
                      .tanks.map((item) => (
                        <MenuItem value={item.tank_id} key={item.tank_id}>
                          {item.tank_name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.source_tank_id &&
                    Boolean(formik.errors.source_tank_id)
                  }
                >
                  {formik.touched.source_tank_id &&
                    formik.errors.source_tank_id}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="quantity"
                  type="number"
                  onBlur={formik.handleBlur}
                  disabled={
                    formik.values.clean_tank_flag === "Y" ? true : false
                  }
                  value={parseFloat(formik.values.quantity)}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "quantity",
                      e.target.value ? e.target.value : 0
                    );
                    if (formik.values.unit_name === "Cubic Meters") {
                      formik.setFieldValue("quantity_cm", e.target.value);
                      formik.setFieldValue(
                        "quantity_mt",
                        e.target.value / formik.values.est_density
                      );
                    } else {
                      formik.setFieldValue("quantity_mt", e.target.value);
                      formik.setFieldValue(
                        "quantity_cm",
                        e.target.value * formik.values.est_density
                      );
                    }
                  }}
                  InputLabelProps={{
                    style: { color: " #0B626B" },
                  }}
                  size="small"
                  label="Quantity"
                />
                <FormHelperText
                  error={
                    formik.touched.quantity && Boolean(formik.errors.quantity)
                  }
                >
                  {formik.touched.quantity && formik.errors.quantity}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <Stack direction="row" spacing={1}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        size="small"
                        name="clean_tank_flag"
                        checked={
                          formik.values.clean_tank_flag === "Y" ? true : false
                        }
                        onChange={(e) => {
                          formik.setFieldValue(
                            "clean_tank_flag",
                            e.target.checked ? "Y" : "N"
                          );
                          e.target.checked
                            ? formik.setFieldValue(
                                "quantity",
                                formik.values.quantity_mt
                              )
                            : formik.setFieldValue("quantity", "");
                        }}
                      />
                    }
                    label="Clean Tank"
                  />
                </Stack>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DateTimePicker
                    label="Transfer completion date"
                    value={dayjs(formik.values.transfer_completion_date)}
                    format="YYYY-MM-DD hh:mm A"
                    name="transfer_completion_date"
                    id="transfer_completion_date"
                    slotProps={{
                      textField: { size: "small" },
                    }}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "transfer_completion_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <Stack direction="row" spacing={1}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        size="small"
                        name="transfer_completion_date_est_fl"
                        checked={
                          formik.values.transfer_completion_date_est_fl === "Y"
                            ? true
                            : false
                        }
                        onChange={(e) => {
                          formik.setFieldValue(
                            "transfer_completion_date_est_fl",
                            e.target.checked ? "Y" : "N"
                          );
                        }}
                      />
                    }
                    label="Est"
                  />
                </Stack>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    name="terminal_from_date"
                    id="terminal_from_date"
                    value={dayjs(formik.values.terminal_from_date)}
                    onBlur={formik.handleBlur}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "terminal_from_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                    label="Terminal dates from"
                    slotProps={{
                      textField: { size: "small" },
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    name="terminal_to_date"
                    id="terminal_to_date"
                    value={dayjs(formik.values.terminal_to_date)}
                    onBlur={formik.handleBlur}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "terminal_to_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                    label="Terminal dates to"
                    slotProps={{
                      textField: { size: "small" },
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <TextField
                fullWidth
                id="tank_comment"
                name="tank_comment"
                value={formik.values.tank_comment}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                size="small"
                label="Tank Comment"
              />
            </Stack>
          </Stack>
        </Box>
        <Box
          component="fieldset"
          sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
        >
          <legend>{tankName}</legend>
          <Stack direction="column" spacing={1} sx={{ mt: 1 }}>
            <Stack direction="row" spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <TextField
                  name="tank_product"
                  readOnly={true}
                  value={formik.values.tank_product}
                  size="small"
                  label="Product"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="tank_grade"
                  readOnly={true}
                  value={formik.values.tank_grade}
                  size="small"
                  label="Grade"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_density"
                  value={parseFloat(formik.values.tank_density).toFixed(3)}
                  size="small"
                  type="number"
                  label="Density(kg/l)"
                />
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  type="number"
                  name="tank_balance_cm"
                  value={formik.values.tank_balance_cm}
                  size="small"
                  label="Balance (CM)"
                />
              </FormControl>

              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_balance_mt"
                  value={formik.values.tank_balance_mt}
                  size="small"
                  type="number"
                  label="Balance (MT)"
                />
              </FormControl>

              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_capacity"
                  value={formik.values.tank_capacity}
                  size="small"
                  type="number"
                  label="Capacity"
                />
              </FormControl>
            </Stack>
          </Stack>
        </Box>
        <Divider sx={{ mt: 1, mb: 1 }} />
        {session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Storage")[0]
          .permission === "Edit" ? (
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Button variant="outlined" onClick={handleModalClose}>
              Close
            </Button>
            <Button variant="contained" type="submit" color="dark">
              Save
            </Button>
          </Box>
        ) : null}
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
