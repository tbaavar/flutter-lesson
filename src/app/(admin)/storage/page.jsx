"use client";
import * as React from "react";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import CustomNoRowsOverlay from "@/components/NoRecord";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import { styled, alpha } from "@mui/material/styles";
import { tableCellClasses } from "@mui/material/TableCell";
import useSWR from "swr";
import {
  Box,
  AlertTitle,
  Card,
  Menu,
  MenuItem,
  Typography,
  Stack,
  Modal,
  Alert,
  Button,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  Checkbox,
  OutlinedInput,
  ListItemText,
  TableContainer,
  Paper,
  TableCell,
  Table,
  TableBody,
  TableHead,
  TableRow,
  FormControlLabel,
  Divider,
  ButtonGroup,
  FormHelperText,
  Chip,
  Link,
  Tooltip,
} from "@mui/material";
import { useSession } from "next-auth/react";
import dayjs from "dayjs";
import { fetcher } from "@/lib/fetcher";
import Load from "../storage/load/page";
import Discharge from "../storage/discharge/page";
import PumpOver from "../storage/pumpover/page";
import Loss from "../storage/loss/page";
import Gain from "../storage/gain/page";
import Grade from "../storage/grade/page";
import { toast } from "react-toastify";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { ToastContainer } from "react-toastify";
import { mutate } from "swr";
import * as yup from "yup";
import { fetchData } from "@/lib/fetch";
import { useFormik } from "formik";
import GridViewIcon from "@mui/icons-material/GridView";
import ListAltOutlinedIcon from "@mui/icons-material/ListAltOutlined";
import { numberWithCommas } from "@/lib/numberWithCommas";
import exportToExcel from "@/components/ExcelExport";
import DeleteIcon from "@mui/icons-material/Delete";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#FFFFFF",
    color: "#3b8188",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "left",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "left",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === "light"
        ? "rgb(55, 65, 81)"
        : theme.palette.grey[300],
    boxShadow:
      "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 1;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 7 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

export default function StoragePage() {
  const [loading, setLoading] = React.useState(false);

  const { data: session } = useSession();
  const [anchorEl, setAnchorEl] = React.useState();
  const [open, setOpen] = React.useState(false);
  const [opens, setOpens] = React.useState(false);
  const [item, setItem] = React.useState(false);
  const [rowData, setRowData] = React.useState("");
  const [method, setMethod] = React.useState("");
  const [itemRow, setItemRow] = React.useState([]);
  const [itemList, setItemList] = React.useState([]);
  const [itemExport, setItemExport] = React.useState([]);
  const [switches, setSwitches] = React.useState(true);
  const [iconColor, setIconColor] = React.useState("#0B626B");
  const [iconColor1, setIconColor1] = React.useState("black");
  const [companyId, setCompanyId] = React.useState("");

  const handleSwitchesFalse = () => {
    setIconColor1("gray"); // Set icon color to gray
    setIconColor("#0B626B");
    setSwitches(false);
  };

  const handleSwitchesTrue = () => {
    setIconColor1("#0B626B"); // Set icon color to blue
    setIconColor("gray");
    setSwitches(true);
  };
  const ddd = itemRow;
  const list = itemList;
  const openl = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleModalOpen = (item) => {
    setOpen(true);
    setItem(item);
    setAnchorEl(false);
  };
  const handleModalClose = () => setOpen(false);

  const columns = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 50,
    },
    {
      field: "parcel_movement_id",
      renderHeader: () => <strong>{"Movement Id"}</strong>,
      renderCell: (params) => {
        const handleEdit = (e) => {
          const rows = params.row;

          setRowData(rows);
          setMethod("PUT");
          handleModalOpen(rows.transfer_type);
        };
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Link onClick={handleEdit} variant="h7">
              {params.row.parcel_movement_id}
            </Link>
          </Box>
        );
      },
      width: 90,
    },
    {
      field: "transfer_completion_date",
      renderHeader: () => <strong>{"Transfer date"}</strong>,
      width: 130,
      valueGetter: (params) =>
        dayjs(params.row.transfer_completion_date).format("YYYY-MM-DD hh:mm"),
    },
    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      width: 150,
    },
    {
      field: "transfer_type",
      renderHeader: () => <strong>{"Transfer type"}</strong>,
      renderCell: (params) => {
        if (params.row.transfer_type === "Load") {
          return (
            <Typography variant="h7" color={"#E21D12"}>
              {params.row.transfer_type}
            </Typography>
          );
        } else if (params.row.transfer_type === "Pumpover") {
          return (
            <Typography variant="h7" color={"#4285F4"}>
              {params.row.transfer_type}
            </Typography>
          );
        } else if (params.row.transfer_type === "Loss") {
          return (
            <Typography variant="h7" color={"#E21D12"}>
              {params.row.transfer_type}
            </Typography>
          );
        } else if (params.row.transfer_type === "Gain") {
          return (
            <Typography variant="h7" color={"#0B626B"}>
              {params.row.transfer_type}
            </Typography>
          );
        } else
          return (
            <Typography variant="h7" color={"#0B626B"}>
              {params.row.transfer_type}
            </Typography>
          );
      },
      width: 150,
    },
    {
      field: "terminal_name",
      renderHeader: () => <strong>{"Terminal"}</strong>,
      width: 150,
    },
    {
      field: "tank_name",
      headerName: "Tank",
      renderHeader: () => <strong>{"Tank"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.tank_name}</Typography>
          <Typography variant="h7">
            {"Tank Density: " + "" + params.row.tank_density}
          </Typography>
        </Box>
      ),
      width: 150,
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product/Grade"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.tank_product}</Typography>
          <Typography variant="h7">
            <strong>{params.row.tank_grade}</strong>
          </Typography>
        </Box>
      ),
      width: 200,
    },
    {
      field: "quantity",
      renderHeader: () => <strong>{"Quantity(MT)"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.quantity)}
          </Typography>
        </Box>
      ),
      width: 120,
    },
    {
      field: "quantity_cm",
      renderHeader: () => <strong>{"Quantity(CM)"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.quantity_cm)}
          </Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "est_density",
      renderHeader: () => <strong>{"Density"}</strong>,
      width: 150,
    },
    {
      field: "tank_balance",
      renderHeader: () => <strong>{"Balance"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {numberWithCommas(params.row.tank_balance)}
          </Typography>
          <Typography variant="h7">
            {"Final Density: " + "" + params.row.final_density}
          </Typography>
        </Box>
      ),

      width: 150,
    },
    {
      field: "transfer_status",
      renderHeader: () => <strong>{"Transfer status"}</strong>,
      renderCell: (params) => {
        if (params.row.transfer_status === "Hold") {
          return (
            <Chip
              variant="filled"
              color="high"
              size="small"
              label={params.row.transfer_status}
              sx={{ fontSize: "11px" }}
            />
          );
        } else if (params.row.transfer_status === "Progress") {
          return (
            <Chip
              variant="filled"
              color="urgent"
              size="small"
              sx={{ fontSize: "11px" }}
              label={params.row.transfer_status}
            />
          );
        } else
          return (
            <Chip
              variant="filled"
              color="regular"
              sx={{ fontSize: "11px" }}
              size="small"
              label={params.row.transfer_status}
            />
          );
      },
      width: 150,
    },
    {
      field: "#",
      renderHeader: () => <strong>{""}</strong>,
      width: 50,
      renderCell: (params) => {
        const handleDelete = (e) => {
          const rows = params.row;
          setRowData(rows);
          setMethod("DELETE");
          handleModalOpen(rows.transfer_type);
        };
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleDelete}
              />
            </Tooltip>
          </Stack>
        );
      },
    },
  ];
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: tank } = useSWR(
    companyId
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${companyId}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const getColor = (type) => {
    switch (type) {
      case "Load":
        return "#fdedec";
      case "Pumpover":
        return "#fff4cb";
      case "Discharge":
        return "#ecf6f0";
      case "Loss":
        return "#fdedec";
      case "Gain":
        return "#ecf6f0";
      case "Y":
        return "#00BFFF";
      default:
        return "#ffffff";
    }
  };

  const formik = useFormik({
    initialValues: {
      company_id: "",
      terminal_name: ddd[0]?.terminal_name ?? "",
      terminal_id: "",
      tanks: [],
      begin_date: dayjs(dayjs(new Date(new Date().getFullYear(), 0, 1))).format(
        "YYYY-MM-DD"
      ),
      end_date: dayjs().format("YYYY-MM-DD"),
      suspend_flag: "",
    },
    validationSchema: yup.object({
      company_id: yup.string().required("Please select Company"),
      terminal_id: yup.string().required("Please select Terminal!"),
      tanks: yup
        .array()
        .min(1, "Please select Tank(s)!")
        .required("Please select Tank(s)!"),
    }),
    onSubmit: (values) => {
      PatchTanks(values);
      PatchList(values);
    },
    enableReinitialize: false,
  });

  const PatchTanks = async (data) => {
    const arr = [];

    {
      data?.tanks?.map((item1) => {
        arr.push({ tank_id: item1 });
      });
    }

    const body = {
      terminal_id: data.terminal_id,
      tanks: arr,
      begin_date: data.begin_date,
      end_date: data.end_date,
      suspend_flag: true,
    };
    try {
      setLoading(true);

      const r = await fetch(`/api/storage`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + session?.user?.token,
        },
        body: JSON.stringify(body),
      });

      const res = await r.json();
      setItemRow(res.result);
      if (r.status === 200) {
        toast.success("Хайлт Амжилттай", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хайлт Амжилтгүй", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const PatchList = async (data) => {
    const arr = [];

    {
      data?.tanks.map((item1) => {
        arr.push({ tank_id: item1 });
      });
    }

    const body = {
      terminal_id: data.terminal_id,
      tanks: arr,
      begin_date: data.begin_date,
      end_date: data.end_date,
      suspend_flag: true,
    };

    try {
      setLoading(true);

      const r = await fetch(`/api/storage/list`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + session?.user?.token,
        },
        body: JSON.stringify(body),
      });

      const res = await r.json();
      setItemList(
        res?.result?.map((row, i) => ({
          id: i + 1,
          ...row,
        })) || []
      );
      setItemExport(
        res?.result?.map((row, i) => ({
          id: i + 1,
          parcel_movement_id: row.parcel_movement_id,
          transfer_completion_date: row.transfer_completion_date,
          counterparty_name: row.counterparty_name,
          transfer_type: row.transfer_type,
          terminal_name: row.terminal_name,
          tank_name: row.tank_name,
          tank_density: row.tank_density,
          quantity: row.quantity,
          est_density: row.est_density,
          tank_unit_name: row.tank_unit_name,
          tank_balance: row.tank_balance,
          final_density: row.final_density,
          transfer_status: row.transfer_status,
        })) || []
      );
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const deleteList = async (data) => {
    const body = {
      parcel_movement_id: data.parcel_movement_id,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpens(true);
      const res = await fetchData(
        `/api/storage`,
        "DELETE",
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        toast.success("Амжилттай устгалаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpens(false);
      handleModalClose();
      
  
    }
  };

  const handleExportExcel = () => {
    const trade = [
      {
        header: "id",
      },
      {
        header: "parcel_movement_id",
      },
      {
        header: "transfer_completion_date",
      },
      {
        header: "counterparty_name",
      },
      {
        header: "transfer_type",
      },
      {
        header: "terminal_name",
      },
      {
        header: "tank_name",
      },
      {
        header: "tank_density",
      },
      {
        header: "quantity",
      },
      {
        header: "est_density",
      },
      {
        header: "tank_unit_name",
      },
      {
        header: "tank_balance",
      },
      {
        header: "final_density",
      },
      {
        header: "transfer_status",
      },
    ];

    exportToExcel(
      itemExport,
      trade,
      `StorageReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <ToastContainer />
        <Box
          sx={{
            display: "flex",
            m: 1,
          }}
        >
          <FormControl size="small" fullWidth sx={{ my: 1, mr: 1 }}>
            {" "}
            <InputLabel>Company</InputLabel>
            <Select
              labelId="company_id"
              id="company_id"
              name="company_id"
              value={formik.values.company_id}
              label="Company"
              onChange={(e) => {
                formik.setFieldValue("company_id", e.target.value);
                setCompanyId(e.target.value);
              }}
            >
              {org?.result.length >= 0 &&
                org?.result.map((item) => (
                  <MenuItem value={item.org_id} key={item.org_id}>
                    {item.org_name}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.company_id && Boolean(formik.errors.company_id)
              }
            >
              {formik.touched.company_id && formik.errors.company_id}
            </FormHelperText>
          </FormControl>
          <FormControl size="small" fullWidth sx={{ my: 1, mr: 1 }}>
            <InputLabel>Terminal</InputLabel>
            <Select
              labelId="terminal_id"
              id="terminal_id"
              name="terminal_id"
              value={formik.values.terminal_id}
              label="Terminal"
              onChange={(e) => {
                formik.setValues({
                  ...formik.values,
                  terminal_id: e.target.value,
                });
                const allTankIds = tank.result
                  .filter((f) => f.terminal_id === e.target.value)
                  .flatMap((item) => item.tanks.map((tank) => tank.tank_id));
                formik.setFieldValue("tanks", allTankIds);
              }}
            >
              {tank?.result.length > 0 &&
                tank?.result.map((item) => (
                  <MenuItem value={item.terminal_id} key={item.terminal_id}>
                    {item.terminal_name}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.terminal_id && Boolean(formik.errors.terminal_id)
              }
            >
              {formik.touched.terminal_id && formik.errors.terminal_id}
            </FormHelperText>
          </FormControl>
          {/* <FormControl sx={{ minWidth: 151, mt: 1 }}>
            <FormControlLabel
              control={
                <Checkbox
                  size="small"
                  name="suspend_flag"
                  checked={formik.values.suspend_flag === "Y" ? true : false}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "suspend_flag",
                      e.target.checked ? "Y" : "N"
                    );
                  }}
                />
              }
              label="Include Suspended"
            />
          </FormControl> */}
          <FormControl size="small" fullWidth sx={{ m: 1 }}>
            <InputLabel>Tank</InputLabel>
            <Select
              labelId="tanks"
              id="tanks"
              name="tanks"
              multiple
              sx={{ backgroundColor: "white" }}
              label="Tank(s)"
              value={formik.values.tanks}
              onChange={formik.handleChange}
              input={<OutlinedInput label="Tank" />}
              SelectDisplayProps={item.tank_name}
              renderValue={(selected) =>
                selected
                  .map(
                    (id) =>
                      tank?.result
                        .find((tankItem) =>
                          tankItem.tanks.find((tank) => tank.tank_id === id)
                        )
                        ?.tanks.find((tank) => tank.tank_id === id).tank_name
                  )
                  .join(", ")
              }
              MenuProps={MenuProps}
            >
              {tank?.result.filter(
                (f) => f.terminal_id === formik.values.terminal_id
              ).length > 0 &&
                tank?.result
                  .filter((f) => f.terminal_id === formik.values.terminal_id)[0]
                  ?.tanks.map((item) => (
                    <MenuItem value={item.tank_id} key={item.tank_id}>
                      <Checkbox
                        checked={formik.values.tanks.indexOf(item.tank_id) > -1}
                      />
                      <ListItemText
                        primary={item.tank_name}
                        style={{ color: "black" }}
                      />
                    </MenuItem>
                  ))}
            </Select>
            <FormHelperText
              error={formik.touched.tanks && Boolean(formik.errors.tanks)}
            >
              {formik.touched.tanks && formik.errors.tanks}
            </FormHelperText>
          </FormControl>
          <FormControl fullWidth sx={{ m: 1 }}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                format="YYYY-MM-DD"
                name="begin_date"
                value={dayjs(formik.values.begin_date)}
                maxDate={dayjs(formik.values.end_date)}
                onChange={(newValue) => {
                  formik.setFieldValue(
                    "begin_date",
                    dayjs(newValue).format("YYYY-MM-DD")
                  );
                }}
                label="Begin Date"
                slotProps={{
                  textField: { size: "small" },
                }}
              />
              <FormHelperText
                error={
                  formik.touched.begin_date && Boolean(formik.errors.begin_date)
                }
              >
                {formik.touched.begin_date && formik.errors.begin_date}
              </FormHelperText>
            </LocalizationProvider>
          </FormControl>
          <FormControl fullWidth sx={{ m: 1 }}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                format="YYYY-MM-DD"
                name="end_date"
                value={dayjs(formik.values.end_date)}
                maxDate={dayjs()}
                onChange={(newValue) => {
                  formik.setFieldValue(
                    "end_date",
                    dayjs(newValue).format("YYYY-MM-DD")
                  );
                }}
                label="End Date"
                slotProps={{
                  textField: { size: "small" },
                }}
              />
            </LocalizationProvider>
            <FormHelperText
              error={formik.touched.end_date && Boolean(formik.errors.end_date)}
            >
              {formik.touched.end_date && formik.errors.end_date}
            </FormHelperText>
          </FormControl>
          {loading === true ? (
            <Box sx={{ display: "flex", justifyContent: "center", m: 1 }}>
              <CircularProgress />
            </Box>
          ) : (
            <Button variant="outlined" type="submit" sx={{ m: 1 }} size="small">
              Search
            </Button>
          )}
        </Box>
        <Box sx={{ mb: 1, ml: 1 }}>
          <Stack
            direction={"row"}
            spacing={1}
            justifyContent="space-between"
            alignItems={"center"}
          >
            <Box>
              <Typography variant="h8">
                <strong>Terminal name:</strong> {ddd[0]?.terminal_name ?? ""}
              </Typography>
            </Box>
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Storage")[0]
              .permission === "Edit" ? (
              <Button
                id="demo-customized-button"
                aria-controls={openl ? "demo-customized-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={openl ? "true" : undefined}
                variant="text"
                disableElevation
                startIcon={<AddSharpIcon />}
                onClick={handleClick}
                endIcon={<KeyboardArrowDownIcon />}
              >
                New
              </Button>
            ) : null}
            <StyledMenu
              id="add-new-button"
              MenuListProps={{
                "aria-labelledby": "add-new-button",
              }}
              anchorEl={anchorEl}
              open={openl}
              onClose={handleClose}
            >
              <MenuItem
                onClick={() => {
                  handleModalOpen("Load");
                  setRowData();
                  setMethod("POST");
                }}
                disableRipple
              >
                LOAD
              </MenuItem>
              <MenuItem
                onClick={() => {
                  handleModalOpen("Discharge");
                  setRowData();
                  setMethod("POST");
                }}
                disableRipple
              >
                DISCHARGE
              </MenuItem>
              <MenuItem
                onClick={() => {
                  handleModalOpen("Pumpover");
                  setRowData();
                }}
                disableRipple
              >
                PUMPOVER
              </MenuItem>
              <MenuItem
                onClick={() => {
                  handleModalOpen("Loss");
                  setRowData();
                }}
              >
                ADJUSTMENT LOSS
              </MenuItem>
              <MenuItem
                onClick={() => {
                  handleModalOpen("Gain");
                  setRowData();
                }}
                disableRipple
              >
                ADJUSTMENT GAIN
              </MenuItem>
              <MenuItem
                onClick={() => {
                  handleModalOpen("Grade");
                  setRowData();
                }}
                disableRipple
              >
                ADJUSTMENT GRADE
              </MenuItem>
            </StyledMenu>
          </Stack>
        </Box>

        <Box sx={{ maxWidth: "92vw", overflow: "auto" }}>
          <Paper
            sx={{
              m: 1,
            }}
          >
            <Card sx={{ display: "flex", justifyContent: "space-between" }}>
              <FormControl sx={{ mt: 1, ml: 1, mb: 1 }}>
                <Button
                  variant="outlined"
                  size="small"
                  sx={{ maxWidth: 205 }}
                  onClick={handleExportExcel}
                >
                  Export
                </Button>
              </FormControl>

              <Box sx={{ mr: 1 }}>
                <ButtonGroup variant="">
                  <Button
                    style={{ width: "25px", color: iconColor }}
                    onClick={handleSwitchesTrue}
                  >
                    <ListAltOutlinedIcon className="largeIcon" />
                  </Button>
                  <Button
                    style={{ width: "25px", color: iconColor1 }}
                    onClick={handleSwitchesFalse}
                  >
                    <GridViewIcon />
                  </Button>
                </ButtonGroup>
              </Box>
            </Card>
            <Divider />
            {switches === false ? (
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell
                        padding="none"
                        sx={{
                          maxWidth: 150,
                          minWidth: 150,
                          border: 0,
                          borderRight: 1,
                          borderBottom: 1,
                          textAlign: "center",
                        }}
                      >
                        <strong>Date</strong>
                      </StyledTableCell>
                      <StyledTableCell
                        sx={{
                          border: 0,
                          borderBottom: 1,
                          padding: 0,
                          textAlign: "center",
                        }}
                      >
                        <strong>Moved</strong>
                      </StyledTableCell>

                      {ddd.map((item, i) => (
                        <TableCell padding="none" key={i}>
                          {item.tankuud.map((lol, j) => (
                            <StyledTableCell
                              key={j}
                              sx={{
                                borderLeft: 1,

                                maxWidth: 450,
                                minWidth: 450,
                                p: 0,
                                borderBottom: 1,
                                borderBlock: 0,

                                alignContent: "right",
                              }}
                            >
                              <StyledTableCell
                                key={j}
                                align="center"
                                padding="none"
                                sx={{
                                  maxWidth: 250,
                                  minWidth: 250,
                                  borderRight: 1,
                                  borderBottom: 1,
                                }}
                              >
                                <strong> {lol.tank}</strong>
                              </StyledTableCell>
                              <StyledTableCell
                                key={j}
                                align="center"
                                padding="none"
                                sx={{
                                  maxWidth: 100,
                                  minWidth: 100,
                                  borderRight: 1,
                                  borderBottom: 1,
                                }}
                              >
                                <strong>Quantity</strong>
                              </StyledTableCell>
                              <StyledTableCell
                                key={j}
                                align="center"
                                padding="normal"
                                sx={{
                                  maxWidth: 100,
                                  minWidth: 100,
                                  borderBottom: 1,
                                  borderRight: 1,
                                }}
                              >
                                <strong>Balance</strong>
                              </StyledTableCell>
                            </StyledTableCell>
                          ))}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  {ddd.length === 0 && (
                    <TableRow>
                      <TableCell size="medium" align="center">
                        <CustomNoRowsOverlay />
                      </TableCell>
                    </TableRow>
                  )}
                  {ddd.map((row, i) => (
                    <TableBody key={i} component={"tbody"}>
                      {row.details.map((item, l) => (
                        <TableRow key={l}>
                          <TableCell
                            key={l}
                            padding="none"
                            sx={{
                              borderBottom: 1,
                              borderColor: "#E9ECF0",
                              maxWidth: 150,
                              borderRight: 1,
                              minWidth: 150,
                            }}
                            align="center"
                          >
                            {dayjs(item.transfer_date).format(
                              "YYYY-MM-DD hh:mm"
                            )}
                          </TableCell>

                          <TableCell
                            key={l}
                            padding="none"
                            sx={{
                              borderBottom: 1,
                              borderLeft: 1,
                              backgroundColor: getColor(item.transfer_type),
                              borderColor: "#E9ECF0",
                              maxWidth: 60,
                              minWidth: 60,
                            }}
                            align="center"
                          >
                            {item.transfer_type}
                          </TableCell>

                          <TableCell
                            padding="none"
                            sx={{ borderBottom: 1, borderColor: "#E9ECF0" }}
                          >
                            {item.tanks.map((tank, k) => (
                              <TableCell
                                padding="none"
                                size="small"
                                variant="body"
                                align="center"
                                sx={{
                                  borderLeft: 1,
                                  borderRight: 1,
                                  borderColor: "#E9ECF0",
                                  maxWidth: 450,
                                  minWidth: 450,
                                }}
                                key={k}
                              >
                                <TableCell
                                  key={k}
                                  padding="none"
                                  align="center"
                                  variant="body"
                                  sx={{
                                    maxWidth: 250,
                                    minWidth: 250,
                                    backgroundColor: getColor(
                                      tank.details[0]?.blend_flag
                                    ),
                                    borderRight: 1,
                                  }}
                                >
                                  {tank.details[0]?.grade_id}
                                </TableCell>
                                <TableCell
                                  key={k}
                                  padding="none"
                                  align="center"
                                  variant="body"
                                  sx={{
                                    maxWidth: 100,
                                    minWidth: 100,
                                    borderRight: 1,
                                  }}
                                >
                                  {numberWithCommas(
                                    tank.details[0]?.quantity_mt
                                  )}
                                </TableCell>
                                <TableCell
                                  key={k}
                                  padding="checkbox"
                                  align="center"
                                  variant="body"
                                  sx={{ maxWidth: 100, minWidth: 100 }}
                                >
                                  {numberWithCommas(
                                    tank.details[0]?.balance_mt
                                  )}
                                </TableCell>
                              </TableCell>
                            ))}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  ))}
                  {/* <TableFooter className={classes.sticky}>HEllo</TableFooter> */}
                </Table>
              </TableContainer>
            ) : (
              <DataGrid
                getRowId={(row) => row.parcel_movement_id}
                rows={list}
                columns={columns}
                autoHeight
                pageSizeOptions={[50]}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                sx={{
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                }}
                rowHeight={() => "auto"}
              />
            )}
          </Paper>
        </Box>
        <Modal open={open}>
          {method === "DELETE" ? (
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                height: "flex",
                width: 1100,
                transform: "translate(-50%, -50%)",
                bgcolor: "background.paper",
                border: "1px solid #000",
                boxShadow: 0,
                p: 2,
              }}
            >
              <Box>
                <Typography id="delete-alert-title" variant="h6" component="h3">
                  Storage {rowData?.transfer_type} delete
                </Typography>
              </Box>
              <Stack fullWidth component="div" noValidate autoComplete="off">
                <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                  <Alert severity="error">
                    <AlertTitle>
                      {JSON.stringify(rowData?.parcel_movement_id)}{" "}
                      {JSON.stringify(rowData?.transfer_type)}
                    </AlertTitle>
                    are you sure delete!
                  </Alert>
                </Box>
              </Stack>
              <Box
                sx={{
                  display: "flex",
                  marginBottom: 1,
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  variant="contained"
                  sx={{
                    marginLeft: "10px",
                    marginRight: "10px",
                  }}
                  onClick={async () => {
                    deleteList(rowData);
                   
                  }}
                >
                  Yes
                </Button>
                <Button onClick={handleModalClose} variant="outlined">
                  No
                </Button>
              </Box>
            </Box>
          ) : (
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                minWidth: 1000,
                transform: "translate(-50%, -50%)",
                bgcolor: "background.paper",
                border: "1px solid #000",
                overflowY: "auto",
                overflowX: "auto",
                boxShadow: 0,
                p: 1,
              }}
            >
              {item === "Load" ? (
                <Load
                  rowData={rowData}
                  handleModalClose={handleModalClose}
                  method={method}
                  mutate={mutate}
                />
              ) : null}
              {item === "Discharge" ? (
                <Discharge
                  rowData={rowData}
                  method={method}
                  handleModalClose={handleModalClose}
                  mutate={mutate}
                />
              ) : null}
              {item === "Pumpover" ? (
                <PumpOver
                  rowData={rowData}
                  handleModalClose={handleModalClose}
                  mutate={mutate}
                />
              ) : null}
              {item === "Loss" ? (
                <Loss
                  rowData={rowData}
                  handleModalClose={handleModalClose}
                  mutate={mutate}
                />
              ) : null}
              {item === "Gain" ? (
                <Gain
                  rowData={rowData}
                  handleModalClose={handleModalClose}
                  mutate={mutate}
                />
              ) : null}
              {item === "Grade" ? (
                <Grade
                  rowData={rowData}
                  handleModalClose={handleModalClose}
                  mutate={mutate}
                />
              ) : null}
            </Box>
          )}
        </Modal>
        <Modal open={opens}>
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              minWidth: 600,
              transform: "translate(-50%, -50%)",
              bgcolor: "transparent",
              overflowY: "auto",
              overflowX: "auto",
              boxShadow: 0,
              p: 1,
            }}
          >
            <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
              <CircularProgress sx={{ color: "#ffffff" }} />
            </Box>
          </Box>
        </Modal>
      </form>
    </>
  );
}
