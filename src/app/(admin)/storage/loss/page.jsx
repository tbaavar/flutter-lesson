"use client";
import * as React from "react";
import {
  Box,
  CircularProgress,
  Typography,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  InputLabel,
  Button,
  Select,
  MenuItem,
  TextField,
  Divider,
  IconButton,
  Stack,
  Modal,
} from "@mui/material";
import * as yup from "yup";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { fetchData } from "@/lib/fetch";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import useSWR from "swr";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import CloseIcon from "@mui/icons-material/Close";
import MyAlert from "@/components/Myalert";
import { numberWithCommas } from "@/lib/numberWithCommas";

export default function Loss({ rowData, handleModalClose, mutate }) {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [companyId, setCompanyId] = React.useState("");
  const [tankName, setTankName] = React.useState(
    rowData?.tank_name ? rowData?.tank_name : ""
  );
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: tank } = useSWR(
    companyId
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${companyId}`,
          session?.user?.token,
        ]
      : rowData
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${rowData.org_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  console.log("qqqqqqq", tank);
  const {
    data: lookup,
    error,
    isLoading,
  } = useSWR([`/api/lookup`, session?.user?.token], fetcher);

  const formik = useFormik({
    initialValues: {
      company_id: rowData?.org_id ?? "",
      parcel_movement_details: rowData?.parcel_movement_details ?? "",
      source_tank_id: rowData?.source_tank_id ?? "",
      parcel_movement_id: rowData?.parcel_movement_id ?? "",
      movement_id: rowData?.movement_id ?? "",
      transfer_completion_date:
        rowData?.transfer_completion_date ??
        dayjs().format("YYYY-MM-DD hh:mm A"),
      transfer_completion_date_est_fl:
        rowData?.transfer_completion_date_est_fl ?? "",
      transfer_status: rowData?.transfer_status ?? "",
      parcel_comment: rowData?.parcel_comment ?? "",
      transfer_type: rowData?.transfer_type ?? "",
      quantity: rowData?.quantity ?? "",
      quantity_cm: rowData?.quantity_cm ?? "",
      quantity_mt: rowData?.quantity_mt ?? "",
      adjustment_type: rowData?.adjustment_type ?? "",
      units: rowData?.units ?? "CM",
      third_party_stock_flag: rowData?.third_party_stock_flag ?? "N",
      source_terminal_id: rowData?.source_terminal_id ?? "",
      balance: rowData?.balance ?? "",
      tank_product: rowData?.tank_product ?? "",
      tank_grade: rowData?.tank_grade ?? "",
      tank_balance_cm: rowData?.tank_balance_cm ?? "",
      tank_balance_mt: rowData?.tank_balance_mt ?? "",
      tank_capacity: rowData?.tank_capacity ?? "",
      tank_density: rowData?.tank_density ?? "",
      country_origin: rowData?.country_origin ?? "",
      tank_product_id: rowData?.tank_product_id ?? "",
      tank_grade_id: rowData?.tank_grade_id ?? "",
      tank_unit: rowData?.tank_unit ?? "",
    },
    validationSchema: yup.object({
      company_id: yup.string().required("Please select Company!"),
      source_terminal_id: yup.string().required("Please select Terminal!"),
      source_tank_id: yup.string().required("Please select Tank!"),
      transfer_status: yup.string().required("Please select Status!"),
      adjustment_type: yup.string().required("Please select Type!"),

      quantity: yup
        .number()
        .min(0, "Quantity can't be a negative!")
        .max(yup.ref("tank_balance"), "Quantity can't be higher than Balance!")
        .required("Please insert quantity!")
        .typeError("Please insert number!"),
      tank_density: yup
        .number()
        .typeError("Please insert a number!")
        .required("Please insert density!")
        .min(0, "Density can't be a negative!")
        .max(1, "Density can't be higher than 1!"),
    }),
    onSubmit: async (values) => {
      saveLossData(values);
    },
    enableReinitialize: true,
  });

  const saveLossData = async (data) => {
    const body = {
      company_id: data.company_id,
      target_company_id: "",
      parcel_movement_id: data.parcel_movement_id,
      voyage_id: data.voyage_id,
      voyage_parcel_id: data.voyage_parcel_id,
      transfer_type: "Loss",
      product_id: data.tank_product_id,
      grade_id: data.tank_grade_id,
      country_origin: data.country_origin,
      parcel_comment: data.parcel_comment,
      transfer_status: data.transfer_status,
      superintendent_id: "",
      superveyor_id: "",
      movement_id: data.movement_id,
      transfer_completion_date: data.transfer_completion_date,
      transfer_completion_date_est_fl: data.transfer_completion_date_est_fl,
      terminal_from_date: "2024-01-01",
      terminal_to_date: "2024-01-01",
      source_terminal_id: data.source_terminal_id,
      source_tank_id: data.source_tank_id,
      target_terminal_id: data.source_terminal_id,
      target_tank_id: data.source_tank_id,
      quantity_mt: data.quantity_mt,
      quantity_cm: data.quantity_cm,
      quantity_b: 0,
      tank_comment: "",
      clean_tank_flag: "",
      third_party_stock_flag: data.third_party_stock_flag,
      transfer_product_id: data.tank_product_id,
      transfer_grade_id: data.tank_grade_id,
      blend_flag: "N",
      final_product_id: data.tank_product_id,
      final_grade_id: data.tank_grade_id,
      circulation: null,
      final_density: "",
      blend_ratio: "",
      balance_mt: parseFloat(data.quantity_mt),
      balance_cm: parseFloat(data.quantity_cm),
      balance_b: 0,
      units: data.units,
      trade_code: "S",
      adjustment_type: data.adjustment_type,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const method = rowData ? "PUT" : "POST";

      const res = await fetchData(
        `/api/storage`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
      handleModalClose();
    }
  };

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  const label = { inputProps: { "aria-label": "Checkbox demo" } };
  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5" gutterBottom>
          Adjustment Loss
        </Typography>
        <IconButton
          sx={{ display: "flex", mt: -1 }}
          onClick={handleModalClose}
          aria-label="close"
        >
          <CloseIcon />
        </IconButton>
      </Box>
      <Divider sx={{ mb: 1 }} />
      <form onSubmit={formik.handleSubmit}>
        <Stack direction={"column"} spacing={1} sx={{ m: 1 }}>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth size="small">
              <InputLabel size="small" sx={{ color: " #0B626B", mt: 1 }}>
                Company
              </InputLabel>
              <Select
                labelId="company_id"
                id="company_id"
                name="company_id"
                value={formik.values.company_id}
                label="Company"
                sx={{ mt: 1 }}
                onChange={(e) => {
                  formik.setFieldValue("company_id", e.target.value);
                  setCompanyId(e.target.value);
                }}
              >
                {org?.result.length >= 0 &&
                  org?.result.map((item) => (
                    <MenuItem value={item.org_id} key={item.org_id}>
                      {item.org_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.company_id && Boolean(formik.errors.company_id)
                }
              >
                {formik.touched.company_id && formik.errors.company_id}
              </FormHelperText>
            </FormControl>
            <FormControl
              fullWidth
              sx={{ display: "flex", flexDirection: "row" }}
            >
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DateTimePicker
                  ampm
                  format="YYYY-MM-DD hh:mm A"
                  label="Adjustment Date"
                  name="transfer_completion_date"
                  id="transfer_completion_date"
                  size="small"
                  sx={{ mt: 1 }}
                  slotProps={{
                    textField: {
                      size: "small",
                    },
                  }}
                  value={dayjs(formik.values.transfer_completion_date)}
                  onChange={(newValue) => {
                    formik.setFieldValue(
                      "transfer_completion_date",
                      dayjs(newValue).format("YYYY-MM-DD hh:mm A")
                    );
                  }}
                />
              </LocalizationProvider>

              <FormControlLabel
                control={
                  <Checkbox
                    size="small"
                    name="transfer_completion_date_est_fl"
                    checked={
                      formik.values.transfer_completion_date_est_fl === "Y"
                        ? true
                        : false
                    }
                    onChange={(e) => {
                      formik.setFieldValue(
                        "transfer_completion_date_est_fl",
                        e.target.checked ? "Y" : "N"
                      );
                    }}
                  />
                }
                sx={{ ml: 1 }}
                label="Est"
                labelPlacement="end"
              />
            </FormControl>
          </Stack>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <InputLabel size="small">Terminal</InputLabel>
              <Select
                labelId="source_terminal_id"
                id="source_terminal_id"
                size="small"
                label="Terminal"
                name="source_terminal_id"
                value={formik.values.source_terminal_id}
                onChange={formik.handleChange}
              >
                {tank?.result.length > 0 &&
                  tank?.result.map((item) => (
                    <MenuItem value={item.terminal_id} key={item.terminal_id}>
                      {item.terminal_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.source_terminal_id &&
                  Boolean(formik.errors.source_terminal_id)
                }
              >
                {formik.touched.source_terminal_id &&
                  formik.errors.source_terminal_id}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Tank
              </InputLabel>
              <Select
                fullWidth
                name="source_tank_id"
                label="Tank"
                value={formik.values.source_tank_id}
                onChange={(e) => {
                  const selectedTankId = e.target.value;

                  const alltanks = tank?.result;
                  const selTank = alltanks.filter((item) => {
                    return item.tanks.find((i) => i.tank_id === selectedTankId);
                  });
                  const selectedTank = selTank[0].tanks.find(
                    (item) => item.tank_id == selectedTankId
                  );
                  setTankName(selectedTank.tank_name);

                  if (selectedTank) {
                    formik.setValues({
                      ...formik.values,
                      source_tank_id: selectedTank.tank_id,
                      tank_product: selectedTank.product_name,
                      tank_product_id: selectedTank.product_id,
                      tank_grade: selectedTank.grade_name,
                      tank_grade_id: selectedTank.grade_id,
                      tank_capacity: selectedTank.capacity,
                      tank_balance_cm: selectedTank.balance_cm,
                      tank_balance_mt: selectedTank.balance_mt,
                      tank_density: selectedTank.denstity,
                      country_origin: selectedTank.country_origin,
                      tank_unit: selectedTank.unit,
                    });
                  }
                  formik.setFieldValue("quantity", 0);
                }}
                size="small"
              >
                {tank?.result.filter(
                  (f) => f.terminal_id === formik.values.source_terminal_id
                ).length > 0 &&
                  tank?.result
                    .filter(
                      (f) => f.terminal_id === formik.values.source_terminal_id
                    )[0]
                    .tanks.map((item) => (
                      <MenuItem value={item.tank_id} key={item.tank_id}>
                        {item.tank_name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.source_tank_id &&
                  Boolean(formik.errors.source_tank_id)
                }
              >
                {formik.touched.source_tank_id && formik.errors.source_tank_id}
              </FormHelperText>
            </FormControl>
          </Stack>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Status
              </InputLabel>
              <Select
                value={formik.values.transfer_status}
                fullWidth
                onBlur={formik.handleBlur}
                name="transfer_status"
                label="status"
                onChange={formik.handleChange}
                size="small"
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Adjustment_status")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.transfer_status &&
                  Boolean(formik.errors.transfer_status)
                }
              >
                {formik.touched.transfer_status &&
                  formik.errors.transfer_status}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Type
              </InputLabel>
              <Select
                name="adjustment_type"
                label="Type"
                value={formik.values.adjustment_type}
                onChange={formik.handleChange}
                size="small"
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Adjustment_type")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.adjustment_type &&
                  Boolean(formik.errors.adjustment_type)
                }
              >
                {formik.touched.adjustment_type &&
                  formik.errors.adjustment_type}
              </FormHelperText>
            </FormControl>
          </Stack>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <TextField
                id="quantity"
                name="quantity"
                type="number"
                value={parseFloat(formik.values.quantity)}
                onChange={(e) => {
                  formik.setFieldValue(
                    "quantity",
                    e.target.value ? e.target.value : 0
                  );
                  if (formik.values.units === "CM") {
                    formik.setFieldValue("quantity_cm", e.target.value);
                    formik.setFieldValue(
                      "quantity_mt",
                      e.target.value / formik.values.est_density
                    );
                  } else {
                    formik.setFieldValue("quantity_mt", e.target.value);
                    formik.setFieldValue(
                      "quantity_cm",
                      e.target.value * formik.values.est_density
                    );
                  }
                }}
                size="small"
                label="Quantity"
              />{" "}
              <FormHelperText
                error={
                  formik.touched.quantity && Boolean(formik.errors.quantity)
                }
              >
                {formik.touched.quantity && formik.errors.quantity}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Unit
              </InputLabel>
              <Select
                fullWidth
                name="units"
                label="Unit"
                value={formik.values.units}
                onChange={formik.handleChange}
                size="small"
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Unit")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
            </FormControl>
          </Stack>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <TextField
                id="parcel_comment"
                name="parcel_comment"
                value={formik.values.parcel_comment}
                onChange={formik.handleChange}
                size="small"
                label="Comment"
              />
            </FormControl>
          </Stack>
        </Stack>
        <Box
          component="fieldset"
          sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
        >
          <legend>{tankName}</legend>
          <Stack direction={"column"} spacing={1}>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <TextField
                name="tank_product"
                readOnly={true}
                value={formik.values.tank_product}
                size="small"
                label="Product"
              />
            </FormControl>
            <FormControl fullWidth>
              <TextField
                name="tank_grade"
                readOnly={true}
                value={formik.values.tank_grade}
                size="small"
                label="Grade"
              />
            </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1}>
            
                <FormControl fullWidth>
                  <TextField
                    readOnly={true}
                    name="tank_balance_cm"
                    type="number"
                    value={parseFloat(formik.values.tank_balance_cm).toFixed(3)}
                    size="small"
                    label="Balance (CM)"
                  />
                </FormControl>
             
                <FormControl fullWidth>
                  <TextField
                    readOnly={true}
                    name="tank_balance_mt"
                    type="number"
                    value={parseFloat(formik.values.tank_balance_mt).toFixed(3)}
                    size="small"
                    label="Balance (MT)"
                  />
                </FormControl>
      

              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_capacity"
                  type="number"
                  value={parseFloat(formik.values.tank_capacity).toFixed(3)}
                  size="small"
                  label="Capacity"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  // readOnly={true}
                  name="tank_density"
                  type="number"
                  value={parseFloat(formik.values.tank_density).toFixed(3)}
                  onChange={formik.handleChange}
                  size="small"
                  label="Density(kg/l)"
                  onBlur={formik.handleBlur}
                />
                <FormHelperText
                  error={
                    formik.touched.tank_density &&
                    Boolean(formik.errors.tank_density)
                  }
                >
                  {formik.touched.tank_density && formik.errors.tank_density}
                </FormHelperText>
              </FormControl>
            </Stack>
          </Stack>
        </Box>

        <Divider sx={{ mt: 1, mb: 1 }} />
        {session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Storage")[0]
          .permission === "Edit" ? (
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Button variant="outlined" onClick={handleModalClose}>
              Close
            </Button>
            <Button variant="contained" type="submit" color="dark">
              Save
            </Button>
          </Box>
        ) : null}
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
