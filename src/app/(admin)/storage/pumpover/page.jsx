"use client";
import * as React from "react";
import {
  Box,
  Typography,
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  Button,
  Select,
  MenuItem,
  TextField,
  Divider,
  IconButton,
  Stack,
  CircularProgress,
  Modal,
  FormHelperText,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { fetchData } from "@/lib/fetch";
import { toast } from "react-toastify";
import * as yup from "yup";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import useSWR from "swr";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import { numberWithCommas } from "@/lib/numberWithCommas";

export default function PumpOver({ rowData, handleModalClose, mutate }) {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [companyId, setCompanyId] = React.useState("");
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: product_id } = useSWR(
    [`/api/lookup/product_grade`, session?.user?.token],
    fetcher
  );
  const { data: tank } = useSWR(
    companyId
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${companyId}`,
          session?.user?.token,
        ]
      : rowData
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${rowData.org_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  console.log("dmnaowidjaowidjoai",tank)
  console.log("weqeqweqweq", tank);
  const {
    data: status,
    error,
    isLoading,
  } = useSWR([`/api/lookup/Adjustment_status`, session?.user?.token], fetcher);

  const formik = useFormik({
    validateOnMount: true,
    initialValues: {
      company_id: rowData?.org_id ?? "",
      voyage_id: rowData?.voyage_id ?? "",
      unit: rowData?.units ?? "",
      parcel_comment: rowData?.parcel_comment ?? "",
      movement_parcel_id: rowData?.movement_parcel_id ?? "",
      transfer_status: rowData?.transfer_status ?? "",
      superveyor_id: rowData?.superveyor_id ?? "",
      tank_comment: rowData?.tank_comment ?? "",
      source_terminal_id: rowData?.source_terminal_id ?? "",
      source_tank_id: rowData?.source_tank_id ?? "",
      source_country_origin: rowData?.source_country_origin ?? "",
      source_product_id: rowData?.source_product_id ?? "",
      source_product: rowData?.source_product ?? "",
      source_grade_id: rowData?.source_grade_id ?? "",
      source_grade: rowData?.source_grade ?? "",
      source_balance_cm: rowData?.source_balance_cm ?? "",
      source_balance_mt: rowData?.source_balance_mt ?? "",
      source_unit: rowData?.source_unit ?? "",
      source_unit_name: rowData?.unit_name ?? "",
      source_density: rowData?.source_density ?? "",
      quantity: rowData?.quantity ?? "",
      quantity_cm: rowData?.quantity_cm ?? "",
      quantity_mt: rowData?.quantity_mt ?? "",
      transfer_completion_date:
        rowData?.transfer_completion_date ?? dayjs().format("YYYY-MM-DD"),
      transfer_completion_date_est_fl:
        rowData?.transfer_completion_date_est_fl ?? "N",
      target_terminal_id: rowData?.target_terminal_id ?? "",
      target_tank_id: rowData?.target_tank_id ?? "",
      target_product: rowData?.target_product ?? "",
      target_grade: rowData?.target_grade ?? "",
      target_balance_cm: rowData?.target_balance_cm ?? "",
      target_balance_mt: rowData?.target_balance_cm ?? "",
      target_unit_name: rowData?.target_unit_name ?? "",
      target_density: rowData?.target_density ?? "",
      clean_tank_flag: rowData?.clean_tank_flag ?? "N",
      third_party_stock_flag: rowData?.third_party_stock_flag ?? "N",
      parcel_movement_id: rowData?.parcel_movement_id ?? "",
      movement_id: rowData?.movement_id ?? "",
      blend_flag: rowData?.blend_flag ?? "N",
      final_density: rowData?.final_density ?? "",
      blend_ratio: rowData?.blend_ratio ?? "",
      circulation: rowData?.circulation ?? dayjs("2022-04-17T15:30"),
      final_product_id: rowData?.final_product_id ?? "",
      final_grade_id: rowData?.final_grade_id ?? "",
      possible: "",
    },
    validationSchema: yup.object({
      company_id: yup.string().required("Please select Company!"),
      source_terminal_id: yup.string().required("Please select Terminal!"),
      source_tank_id: yup.string().required("Please select Tank!"),
      target_terminal_id: yup.string().required("Please select Terminal!"),
      target_tank_id: yup.string().required("Please select Tank!"),
      transfer_status: yup.string().required("Please select Status!"),
      quantity: yup
        .number()
        .min(0, "Quantity can't be negative number!")
        .max(yup.ref("possible"), "Maximum possible quantity reached try lower")
        .required("Please insert quantity!")
        .typeError("Must be a Number!"),
    }),
    onSubmit: async (values) => {
      savePumpOver(values);
    },
    enableReinitialize: false,
  });

  const savePumpOver = async (data) => {
    const body = {
      company_id: data.company_id,
      parcel_movement_id: data.parcel_movement_id,
      voyage_id: data.voyage_id,
      transfer_type: "Pumpover",
      product_id: data.source_product_id,
      grade_id: data.source_grade_id,
      country_origin: data.source_country_origin,
      parcel_comment: data.parcel_comment,
      transfer_status: data.transfer_status,
      superintendent_id: 0,
      superveyor_id: data.superveyor_id,
      movement_id: data.movement_id,
      transfer_completion_date: data.transfer_completion_date,
      transfer_completion_date_est_fl: data.transfer_completion_date_est_fl,
      terminal_from_date: data.transfer_completion_date,
      terminal_to_date: data.transfer_completion_date,
      source_terminal_id: data.source_terminal_id,
      source_tank_id: data.source_tank_id,
      target_terminal_id: data.target_terminal_id,
      target_tank_id: data.target_tank_id,
      quantity_mt: data.quantity_mt,
      quantity_cm: data.quantity_cm,
      quantity_b: data.quantity,
      tank_comment: data.tank_comment,
      clean_tank_flag: data.clean_tank_flag,
      third_party_stock_flag: data.third_party_stock_flag,
      transfer_product_id: data.transfer_product_id,
      transfer_grade_id: data.transfer_grade_id,
      blend_flag: data.blend_flag,
      final_product_id: data.final_product_id,
      final_grade_id: data.final_grade_id,
      circulation: data.circulation,
      final_density: data.final_density,
      blend_ratio: data.blend_ratio,
      movement_parcel_id: data.movement_parcel_id,
      balance_mt:
        parseFloat(data.source_balance_mt) - parseFloat(data.quantity_mt),
      balance_cm:
        parseFloat(data.source_balance_cm) - parseFloat(data.quantity_cm),
      balance_b: 0,
      target_balance_cm:
        parseFloat(data.target_balance_cm) + parseFloat(data.quantity_cm),
      target_balance_mt:
        parseFloat(data.target_balance_mt) + parseFloat(data.quantity_mt),
      units: data.source_unit,
      trade_code: "P",
      adjustment_type: "Standard",
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const method = rowData ? "PUT" : "POST";
      const res = await fetchData(
        `/api/storage`,
        method,
        body,
        session?.user?.token
      );
      console.log("meth", method);
      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleModalClose();
    }
  };

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const label = { inputProps: { "aria-label": "Checkbox demo" } };
  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5" gutterBottom>
          Pumpover
        </Typography>
        <IconButton
          sx={{ display: "flex", mt: -1 }}
          onClick={handleModalClose}
          aria-label="close"
        >
          <CloseIcon />
        </IconButton>
      </Box>
      <Divider sx={{ mb: 1 }} />
      <form onSubmit={formik.handleSubmit}>
        <Box
          component="fieldset"
          sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
        >
          <legend>Source Tank</legend>
          <FormControl size="small" fullWidth sx={{ my: 1, mr: 1 }}>
            {" "}
            <InputLabel>Company</InputLabel>
            <Select
              labelId="company_id"
              id="company_id"
              name="company_id"
              value={formik.values.company_id}
              label="Company"
              onChange={(e) => {
                formik.setFieldValue("company_id", e.target.value);
                setCompanyId(e.target.value);
              }}
            >
              {org?.result.length >= 0 &&
                org?.result.map((item) => (
                  <MenuItem value={item.org_id} key={item.org_id}>
                    {item.org_name}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.company_id && Boolean(formik.errors.company_id)
              }
            >
              {formik.touched.company_id && formik.errors.company_id}
            </FormHelperText>
          </FormControl>
          <Stack direction="row" spacing={1}>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Terminal
              </InputLabel>
              <Select
                name="source_terminal_id"
                size="small"
                label="Terminal"
                value={formik.values.source_terminal_id}
                onChange={formik.handleChange}
              >
                {" "}
                {tank?.result.length > 0 &&
                  tank?.result.map((item) => (
                    <MenuItem value={item.terminal_id} key={item.terminal_id}>
                      {item.terminal_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.source_terminal_id &&
                  Boolean(formik.errors.source_terminal_id)
                }
              >
                {formik.touched.source_terminal_id &&
                  formik.errors.source_terminal_id}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Tank
              </InputLabel>
              <Select
                name="source_tank_id"
                size="small"
                label="Tank"
                value={formik.values.source_tank_id}
                onChange={(e) => {
                  const selectedTankId = e.target.value;
                  const alltanks = tank?.result;
                  const selTank = alltanks.filter((item) => {
                    return item.tanks.find((i) => i.tank_id === selectedTankId);
                  });
                  const selectedTank = selTank[0].tanks.find(
                    (item) => item.tank_id == selectedTankId
                  );

                  if (selectedTank) {
                    formik.setValues({
                      ...formik.values,
                      source_tank_id: selectedTank.tank_id,
                      source_product_id: selectedTank.product_id,
                      source_product: selectedTank.product_name,
                      source_grade_id: selectedTank.grade_id,
                      source_grade: selectedTank.grade_name,
                      source_balance_cm: selectedTank.balance_cm,
                      source_balance_mt: selectedTank.balance_mt,
                      source_unit: selectedTank.unit,
                      source_unit_name: selectedTank.unit_name,
                      source_country_origin: selectedTank.country_origin,
                      final_product_id: selectedTank.product_id,
                      final_grade_id: selectedTank.grade_id,
                      source_density: selectedTank.denstity,
                    });
                  }
                  formik.setFieldValue("quantity", 0);
                }}
              >
                {tank?.result.filter(
                  (f) => f.terminal_id === formik.values.source_terminal_id
                ).length > 0 &&
                  tank?.result
                    .filter(
                      (f) => f.terminal_id === formik.values.source_terminal_id
                    )[0]
                    .tanks.map((item) => (
                      <MenuItem value={item.tank_id} key={item.tank_id}>
                        {item.tank_name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.source_tank_id &&
                  Boolean(formik.errors.source_tank_id)
                }
              >
                {formik.touched.source_tank_id && formik.errors.source_tank_id}
              </FormHelperText>
            </FormControl>
          </Stack>
          <Stack direction="row" spacing={1} sx={{ mt: 1 }}>
            <FormControl fullWidth>
              <TextField
                name="source_balance_cm"
                readOnly={true}
                type="number"
                value={formik.values.source_balance_cm}
                size="small"
                label="Balance (CM)"
              />
            </FormControl>

            <FormControl fullWidth>
              <TextField
                name="source_balance_mt"
                readOnly={true}
                type="number"
                value={formik.values.source_balance_mt}
                size="small"
                label="Balance (MT)"
              />
            </FormControl>

            <FormControl fullWidth>
              <TextField
                name="source_unit_name"
                readOnly={true}
                value={formik.values.source_unit_name}
                size="small"
                label="Unit Measurement"
              />
            </FormControl>
            <FormControl fullWidth>
              <TextField
                name="source_product"
                readOnly={true}
                value={formik.values.source_product}
                size="small"
                label="Product"
              />
            </FormControl>
            <FormControl fullWidth>
              <TextField
                name="source_grade"
                readOnly={true}
                value={formik.values.source_grade}
                size="small"
                label="Grade"
              />
            </FormControl>
          </Stack>
          <Stack direction="row" spacing={1} sx={{ mt: 1 }}>
            <FormControl fullWidth>
              <TextField
                name="quantity"
                type="number"
                value={parseFloat(formik.values.quantity)}
                onBlur={formik.handleBlur}
                onChange={(e) => {
                  if (formik.values.source_unit_name === "Metric Tonnes") {
                    formik.setFieldValue("quantity_mt", e.target.value);
                    formik.setFieldValue(
                      "quantity_cm",
                      e.target.value * formik.values.est_density
                    );
                  } else {
                    formik.setFieldValue("quantity_cm", e.target.value);
                    formik.setFieldValue(
                      "quantity_mt",

                      e.target.value / formik.values.est_density
                    );
                  }
                  const ratio =
                    e.target.value && formik.values.target_balance
                      ? (
                          (e.target.value /
                            (parseFloat(e.target.value) +
                              parseFloat(formik.values.target_balance))) *
                          100
                        ).toFixed(3)
                      : 0;
                  const quantity = e.target.value;
                  const density =
                    e.target.value &&
                    formik.values.source_density &&
                    formik.values.target_balance &&
                    formik.values.target_density
                      ? (
                          (parseFloat(
                            e.target.value * formik.values.source_density
                          ) +
                            parseFloat(
                              formik.values.target_balance *
                                formik.values.target_density
                            )) /
                          (parseFloat(e.target.value) +
                            parseFloat(formik.values.target_balance))
                        ).toFixed(3)
                      : null;

                  formik.setFieldValue("blend_ratio", ratio);
                  formik.setFieldValue("final_density", density);
                  formik.setFieldValue("quantity", quantity ? quantity : 0);
                }}
                InputLabelProps={{
                  style: { color: " #0B626B" },
                }}
                size="small"
                label="Quantity"
              />
              <FormHelperText
                error={
                  formik.touched.quantity && Boolean(formik.errors.quantity)
                }
              >
                {formik.touched.quantity && formik.errors.quantity}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <TextField
                name="source_density"
                readOnly={true}
                type="number"
                value={parseFloat(formik.values.source_density).toFixed(3)}
                size="small"
                label="Density (Kg/L)"
              />
            </FormControl>
          </Stack>
          <Stack>
            <Stack direction="row" spacing={1} sx={{ mt: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Status
                </InputLabel>
                <Select
                  name="transfer_status"
                  value={formik.values.transfer_status}
                  onChange={formik.handleChange}
                  size="small"
                  label="voyage"
                >
                  {status?.result.length >= 0 &&
                    status?.result.map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.transfer_status &&
                    Boolean(formik.errors.transfer_status)
                  }
                >
                  {formik.touched.transfer_status &&
                    formik.errors.transfer_status}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <Stack direction="row" spacing={1}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DateTimePicker
                      label="Transfer completion date"
                      value={dayjs(formik.values.transfer_completion_date)}
                      format="YYYY-MM-DD hh:mm A"
                      name="transfer_completion_date"
                      id="transfer_completion_date"
                      slotProps={{
                        textField: { size: "small" },
                      }}
                      onChange={(newValue) => {
                        formik.setFieldValue(
                          "transfer_completion_date",
                          dayjs(newValue).format("YYYY-MM-DD hh:mm A")
                        );
                      }}
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          sx={{ maxWidth: 100 }}
                          size="small"
                          name="transfer_completion_date_est_fl"
                          checked={
                            formik.values.transfer_completion_date_est_fl ===
                            "Y"
                              ? true
                              : false
                          }
                          onChange={(e) => {
                            formik.setFieldValue(
                              "transfer_completion_date_est_fl",
                              e.target.checked ? "Y" : "N"
                            );
                          }}
                        />
                      }
                      label="Est"
                    />
                  </LocalizationProvider>
                </Stack>
              </FormControl>
            </Stack>
          </Stack>
          <Stack direction="row" spacing={1} sx={{ mt: 1 }}>
            <TextField
              fullWidth
              name="tank_comment"
              value={formik.values.tank_comment}
              onChange={formik.handleChange}
              size="small"
              label="Tank Comment"
            />
          </Stack>
        </Box>
        <Box
          component="fieldset"
          sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
        >
          <legend>Target Tank</legend>
          <Stack direction="column" spacing={1}>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <InputLabel
                  size="small"
                  id="target_terminal-label"
                  sx={{ color: " #0B626B" }}
                >
                  Terminal
                </InputLabel>
                <Select
                  labelId="target_terminal-label"
                  name="target_terminal_id"
                  value={formik.values.target_terminal_id}
                  onChange={formik.handleChange}
                  size="small"
                  label="Terminal"
                >
                  {" "}
                  {tank?.result.length > 0 &&
                    tank?.result.map((item) => (
                      <MenuItem value={item.terminal_id} key={item.terminal_id}>
                        {item.terminal_name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.target_terminal_id &&
                    Boolean(formik.errors.target_terminal_id)
                  }
                >
                  {formik.touched.target_terminal_id &&
                    formik.errors.target_terminal_id}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Tank
                </InputLabel>
                <Select
                  name="target_tank_id"
                  value={formik.values.target_tank_id}
                  onChange={(e) => {
                    const selectedTankId = e.target.value;

                    const alltanks = tank?.result;
                    const selTank = alltanks.filter((item) => {
                      return item.tanks.find(
                        (i) => i.tank_id === selectedTankId
                      );
                    });
                    const selectedTank = selTank[0].tanks.find(
                      (item) => item.tank_id == selectedTankId
                    );

                    if (selectedTank) {
                      formik.setValues({
                        ...formik.values,
                        target_tank_id: selectedTank.tank_id,
                        target_product: selectedTank.product_name,
                        target_grade: selectedTank.grade_name,
                        target_balance_cm: selectedTank.balance_cm,
                        target_balance_mt: selectedTank.balance_mt,
                        target_unit_name: selectedTank.unit_name,
                        target_density: selectedTank.denstity,
                        possible:
                          selectedTank.capacity - selectedTank.balance_mt,
                      });
                    }
                    formik.setFieldValue("quantity", 0);
                  }}
                  size="small"
                  label="Tank"
                >
                  {tank?.result.filter(
                    (f) => f.terminal_id === formik.values.target_terminal_id
                  ).length > 0 &&
                    tank?.result
                      .filter(
                        (f) =>
                          f.terminal_id === formik.values.target_terminal_id
                      )[0]
                      .tanks.map((item) => (
                        <MenuItem value={item.tank_id} key={item.tank_id}>
                          {item.tank_name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.target_tank_id &&
                    Boolean(formik.errors.target_tank_id)
                  }
                >
                  {formik.touched.target_tank_id &&
                    formik.errors.target_tank_id}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="target_balance_cm"
                  readOnly={true}
                  type="number"
                  value={parseFloat(formik.values.target_balance_cm).toFixed(3)}
                  size="small"
                  label="Balance (CM)"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="target_balance_mt"
                  readOnly={true}
                  type="number"
                  value={parseFloat(formik.values.target_balance_mt).toFixed(3)}
                  size="small"
                  label="Balance (MT)"
                />
              </FormControl>

              <FormControl fullWidth>
                <TextField
                  name="target_product"
                  readOnly={true}
                  value={formik.values.target_product}
                  size="small"
                  label="Product"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="target_grade"
                  readOnly={true}
                  value={formik.values.target_grade}
                  size="small"
                  label="Grade"
                />
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="target_unit_name"
                  readOnly={true}
                  value={formik.values.target_unit_name}
                  size="small"
                  label="Unit Measurement"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="target_density"
                  type="number"
                  readOnly={true}
                  value={parseFloat(formik.values.target_density).toFixed(3)}
                  size="small"
                  label="Density (Kg/L)"
                />
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="parcel_comment"
                  value={formik.values.parcel_comment}
                  onChange={formik.handleChange}
                  size="small"
                  label="Tank Comment"
                />
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl
                fullWidth
                sx={{ display: "flex", flexDirection: "row" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      size="small"
                      name="clean_tank_flag"
                      checked={
                        formik.values.clean_tank_flag === "Y" ? true : false
                      }
                      onChange={(e) => {
                        formik.setFieldValue(
                          "clean_tank_flag",
                          e.target.checked ? "Y" : "N"
                        );
                      }}
                    />
                  }
                  label="Clean Tank"
                />
              </FormControl>
              <FormControl fullWidth></FormControl>
              <FormControl fullWidth>
                <TextField
                  name="final_density"
                  readOnly={true}
                  type="number"
                  value={parseFloat(formik.values.final_density).toFixed(3)}
                  size="small"
                  label="Final Density (Kg/L)"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="blend_ratio"
                  readOnly={true}
                  type="number"
                  value={parseFloat(formik.values.blend_ratio).toFixed(3)}
                  size="small"
                  label="Blend Ratio"
                />
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={1}>
              <FormControl fullWidth>
                <FormControlLabel
                  sx={{ justifyContent: "initial" }}
                  control={
                    <Checkbox
                      size="small"
                      name="blend_flag"
                      checked={formik.values.blend_flag === "Y" ? true : false}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "blend_flag",
                          e.target.checked ? "Y" : "N"
                        );
                      }}
                    />
                  }
                  label="Blend"
                />
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small">Final Product</InputLabel>
                <Select
                  disabled={formik.values.blend_flag === "N"}
                  name="final_product_id"
                  size="small"
                  label="final product"
                  value={formik.values.final_product_id}
                  onChange={formik.handleChange}
                >
                  {product_id?.result.length >= 0 &&
                    product_id?.result.map((item) => (
                      <MenuItem value={item.product_id} key={item.product_id}>
                        {item.product_name}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small">Final Grade</InputLabel>
                <Select
                  disabled={formik.values.blend_flag === "N"}
                  name="final_grade_id"
                  size="small"
                  label="final grade"
                  value={formik.values.final_grade_id}
                  onChange={formik.handleChange}
                >
                  {product_id?.result.filter(
                    (f) => f.product_id === formik.values.final_product_id
                  ).length > 0 &&
                    product_id?.result
                      .filter(
                        (f) => f.product_id === formik.values.final_product_id
                      )[0]
                      .grades.map((item) => (
                        <MenuItem value={item.grade_id} key={item.grade_id}>
                          {item.grade_name}
                        </MenuItem>
                      ))}
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider size="small" dateAdapter={AdapterDayjs}>
                  <TimePicker
                    disabled={formik.values.blend_flag === "N"}
                    value={dayjs(formik.values.circulation)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "circulation",
                        dayjs(newValue).format("hh:mm A")
                      );
                    }}
                    slotProps={{
                      textField: { size: "small" },
                    }}
                    label="Circulation"
                  />
                </LocalizationProvider>
              </FormControl>
            </Stack>
          </Stack>
        </Box>
        <Divider sx={{ mt: 1, mb: 1 }} />
        {session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Storage")[0]
          .permission === "Edit" ? (
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Button variant="outlined" onClick={handleModalClose}>
              Close
            </Button>
            <Button variant="contained" type="submit" color="dark">
              Save
            </Button>
          </Box>
        ) : null}
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
