"use client";
import * as React from "react";
import {
  Box,
  CircularProgress,
  FormHelperText,
  Typography,
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  Button,
  Select,
  MenuItem,
  TextField,
  Divider,
  Stack,
  IconButton,
  Modal,
} from "@mui/material";
import * as yup from "yup";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { fetchData } from "@/lib/fetch";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import useSWR from "swr";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import CloseIcon from "@mui/icons-material/Close";
import MyAlert from "@/components/Myalert";
import { numberWithCommas } from "@/lib/numberWithCommas";

export default function Grade({ rowData, handleModalClose, mutate }) {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [tankName, setTankName] = React.useState(rowData?.tank_name);
  const [selectedTank, setSelectedTank] = React.useState("");
  const [companyId, setCompanyId] = React.useState("");
  
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: status } = useSWR(
    ["/api/lookup/Adjustment_status", session?.user?.token],
    fetcher
  );
  const { data: tank } = useSWR(
    companyId
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${companyId}`,
          session?.user?.token,
        ]
      : rowData
      ? [
          `/api/lookup/tank/${session?.user.user_id}/${rowData.org_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const {
    data: grades,
    error,
    isLoading,
  } = useSWR([`/api/lookup/product_grade`, session?.user?.token], fetcher);

  const filteredGrade = grades?.result.filter((grade) => {
    return grade.product_name === selectedTank.product_name;
  });
  const formik = useFormik({
    initialValues: {
      company_id:rowData?.org_id??"",
      parcel_movement_details: rowData?.parcel_movement_details ?? "",
      source_tank_id: rowData?.source_tank_id ?? "",
      parcel_movement_id: rowData?.parcel_movement_id ?? "",
      movement_id: rowData?.movement_id ?? "",
      transfer_completion_date:
        rowData?.transfer_completion_date ??
        dayjs().format("YYYY-MM-DD hh:mm A"),
      transfer_completion_date_est_fl:
        rowData?.transfer_completion_date_est_fl ?? "",
      transfer_status: rowData?.transfer_status ?? "",
      parcel_comment: rowData?.parcel_comment ?? "",
      transfer_type: rowData?.transfer_type ?? "",
      quantity: rowData?.quantity_mt ?? "",
      transfer_grade: rowData?.transfer_grade ?? "",
      units: rowData?.units ?? "CM",
      third_party_stock_flag: rowData?.third_party_stock_flag ?? "N",
      source_terminal_id: rowData?.source_terminal_id ?? "",
      balance: rowData?.balance ?? "",
      tank_product: rowData?.tank_product ?? "",
      tank_grade: rowData?.tank_grade ?? "",
      tank_balance: rowData?.tank_balance ?? "",
      tank_capacity: rowData?.tank_capacity ?? "",
      tank_density: rowData?.tank_density ?? "",
      country_origin: rowData?.country_origin ?? "",
      tank_product_id: rowData?.tank_product_id ?? "",
      tank_grade_id: rowData?.tank_grade_id ?? "",
    },
    validationSchema: yup.object({
      company_id:yup.string().required("Please select Company!"),
      source_terminal_id: yup.string().required("Please select Terminal!"),
      source_tank_id: yup.string().required("Please select Tank!"),
      transfer_status: yup.string().required("Please select Status!"),
      transfer_grade: yup.string().required("Please select Grade!"),
    }),
    onSubmit: async (values) => {
      saveGradeData(values);
    },

    enableReinitialize: true,
  });

  const saveGradeData = async (data) => {
    const body = {
      company_id:data.company_id,
      target_company_id:"",
      parcel_movement_id: data.parcel_movement_id,
      voyage_id: null,
      transfer_type: "Grade",
      product_id: data.tank_product_id,
      grade_id: data.transfer_grade,
      country_origin: "",
      parcel_comment: "",
      transfer_status: data.transfer_status,
      superintendent_id: "",
      superveyor_id: "",
      transfer_completion_date: data.transfer_completion_date,
      transfer_completion_date_est_fl: data.transfer_completion_date_est_fl,
      terminal_from_date: data.transfer_completion_date,
      terminal_to_date: data.transfer_completion_date,
      source_terminal_id: data.source_terminal_id,
      source_tank_id: data.source_tank_id,
      target_terminal_id: data.source_terminal_id,
      target_tank_id: data.source_tank_id,
      quantity_mt: parseFloat(data.quantity),
      quantity_cm: parseFloat(data.quantity),
      quantity_b: parseFloat(data.quantity),
      tank_comment: data.parcel_comment,
      clean_tank_flag: "N",
      third_party_stock_flag: "N",
      transfer_product_id: "",
      transfer_grade_id: "",
      blend_flag: "N",
      final_product_id: data.tank_product_id,
      final_grade_id: data.transfer_grade,
      circulation: null,
      final_density: "",
      blend_ratio: "",
      balance_mt: parseFloat(data.quantity),
      balance_cm: parseFloat(data.quantity),
      balance_b: parseFloat(data.quantity),
      units: data.units,
      trade_code: "S",
      adjustment_type: "Standart",
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const method = rowData ? "PUT" : "POST";

      const res = await fetchData(
        `/api/storage`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
      handleModalClose();
    }
  };

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );
  const label = { inputProps: { "aria-label": "Checkbox demo" } };
  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5" gutterBottom>
          Adjustment Grade
        </Typography>
        <IconButton
          sx={{ display: "flex", mt: -1 }}
          onClick={handleModalClose}
          aria-label="close"
        >
          <CloseIcon />
        </IconButton>
      </Box>
      <Divider sx={{ mb: 1 }} />
      <form onSubmit={formik.handleSubmit}>
        <Stack direction={"column"} spacing={1} sx={{ m: 1 }}>
          <Stack direction={"row"} spacing={1}>
          <FormControl fullWidth size="small" >
            <InputLabel size="small" sx={{ color: " #0B626B",mt:1 }}>Company</InputLabel>
            <Select
              labelId="company_id"
              id="company_id"
              name="company_id"
              value={formik.values.company_id}
              label="Company"
              sx={{mt:1}}
              onChange={(e) => {
                formik.setFieldValue("company_id", e.target.value);
                setCompanyId(e.target.value);
              }}
            >
              {org?.result.length >= 0 &&
                org?.result.map((item) => (
                  <MenuItem value={item.org_id} key={item.org_id}>
                    {item.org_name}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.company_id && Boolean(formik.errors.company_id)
              }
            >
              {formik.touched.company_id && formik.errors.company_id}
            </FormHelperText>
            </FormControl>
            <FormControl fullWidth sx={{display:"flex",flexDirection:"row"}}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DateTimePicker
                  ampm
                  format="YYYY-MM-DD hh:mm A"
                  label="Adjustment Date"
                  name="transfer_completion_date"
                  id="transfer_completion_date"
                  size="small"
                  sx={{ mt: 1 }}
                  slotProps={{
                    textField: {
                      size: "small",
                    },
                  }}
                  value={dayjs(formik.values.transfer_completion_date)}
                  onChange={(newValue) => {
                    formik.setFieldValue(
                      "transfer_completion_date",
                      dayjs(newValue).format("YYYY-MM-DD hh:mm A")
                    );
                  }}
                />
              </LocalizationProvider>
           
              <FormControlLabel
                control={
                  <Checkbox
                    size="small"
                    name="transfer_completion_date_est_fl"
                    checked={
                      formik.values.transfer_completion_date_est_fl === "Y"
                        ? true
                        : false
                    }
                    onChange={(e) => {
                      formik.setFieldValue(
                        "transfer_completion_date_est_fl",
                        e.target.checked ? "Y" : "N"
                      );
                    }}
                  />
                }
                sx={{ml:1}}
                label="Est"
                labelPlacement="end"
              />
            </FormControl>
         
          </Stack>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Terminal
              </InputLabel>
              <Select
                labelId="source_terminal_id"
                id="source_terminal_id"
                size="small"
                label="Terminal"
                name="source_terminal_id"
                value={formik.values.source_terminal_id}
                onChange={formik.handleChange}
              >
                {tank?.result.length > 0 &&
                  tank?.result.map((item) => (
                    <MenuItem value={item.terminal_id} key={item.terminal_id}>
                      {item.terminal_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.source_terminal_id &&
                  Boolean(formik.errors.source_terminal_id)
                }
              >
                {formik.touched.source_terminal_id &&
                  formik.errors.source_terminal_id}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Tank
              </InputLabel>
              <Select
                fullWidth
                name="source_tank_id"
                label="Tank"
                value={formik.values.source_tank_id}
                onChange={(e) => {
                  const selectedTankId = e.target.value;

                  const alltanks = tank?.result;
                  const selTank = alltanks.filter((item) => {
                    return item.tanks.find((i) => i.tank_id === selectedTankId);
                  });
                  const selectedTank = selTank[0].tanks.find(
                    (item) => item.tank_id == selectedTankId
                  );
                  setTankName(selectedTank.tank_name);
                  setSelectedTank(selectedTank);
                  if (selectedTank) {
                    formik.setValues({
                      ...formik.values,
                      source_tank_id: selectedTank.tank_id,
                      tank_product: selectedTank.product_name,
                      tank_product_id: selectedTank.product_id,
                      tank_grade: selectedTank.grade_name,
                      tank_grade_id: selectedTank.grade_id,
                      tank_capacity: selectedTank.capacity,
                      tank_balance: selectedTank.balance,
                      tank_density: selectedTank.denstity,
                      country_origin: selectedTank.country_origin,
                    });
                  }
                }}
                size="small"
              >
                {tank?.result.filter(
                  (f) => f.terminal_id === formik.values.source_terminal_id
                ).length > 0 &&
                  tank?.result
                    .filter(
                      (f) => f.terminal_id === formik.values.source_terminal_id
                    )[0]
                    .tanks.map((item) => (
                      <MenuItem value={item.tank_id} key={item.tank_id}>
                        {item.tank_name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.source_tank_id &&
                  Boolean(formik.errors.source_tank_id)
                }
              >
                {formik.touched.source_tank_id && formik.errors.source_tank_id}
              </FormHelperText>
            </FormControl>
           
          </Stack>
          <Stack direction={"row"} spacing={1}>
            
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Grade
              </InputLabel>
              <Select
                name="transfer_grade"
                label="Grade"
                value={formik.values.transfer_grade}
                onChange={formik.handleChange}
                size="small"
              >
                {filteredGrade[0]?.grades.length >= 0 &&
                  filteredGrade[0]?.grades.map((item) => (
                    <MenuItem value={item.grade_id} key={item.grade_id}>
                      {item.grade_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.transfer_grade &&
                  Boolean(formik.errors.transfer_grade)
                }
              >
                {formik.touched.transfer_grade && formik.errors.transfer_grade}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Status
              </InputLabel>
              <Select
                value={formik.values.transfer_status}
                fullWidth
                onBlur={formik.handleBlur}
                name="transfer_status"
                label="status"
                onChange={formik.handleChange}
                size="small"
              >
                {status?.result.length >= 0 &&
                  status?.result.map((item) => (
                    <MenuItem value={item.lookup_code} key={item.lookup_code}>
                      {item.name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.transfer_status &&
                  Boolean(formik.errors.transfer_status)
                }
              >
                {formik.touched.transfer_status &&
                  formik.errors.transfer_status}
              </FormHelperText>
            </FormControl>
          </Stack>
          <Stack direction={"row"} spacing={1}>
            <FormControl fullWidth>
              <TextField
                id="parcel_comment"
                name="parcel_comment"
                value={formik.values.parcel_comment}
                onChange={formik.handleChange}
                size="small"
                label="Comment"
              />
            </FormControl>
          </Stack>
        </Stack>
        <Box
          component="fieldset"
          sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
        >
          <legend>{tankName}</legend>
          <Stack direction={"column"} spacing={1}>
            <FormControl fullWidth>
              <TextField
                name="tank_product"
                readOnly={true}
                value={formik.values.tank_product}
                size="small"
                label="Product"
              />
            </FormControl>
            <FormControl fullWidth>
              <TextField
                name="tank_grade"
                readOnly={true}
                value={formik.values.tank_grade}
                size="small"
                label="Grade"
              />
            </FormControl>
            <Stack direction={"row"} spacing={1}>
              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_balance"
                  type="number"
                  value={parseFloat(formik.values.tank_balance).toFixed(3)}
                  size="small"
                  label="Balance"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_capacity"
                  type="number"
                  value={parseFloat(formik.values.tank_capacity).toFixed(3)}
                  size="small"
                  label="Capacity"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  readOnly={true}
                  name="tank_density"
                  type="number"
                  value={parseFloat(formik.values.tank_density).toFixed(3)}
                  size="small"
                  label="Density(kg/l)"
                />
              </FormControl>
            </Stack>
          </Stack>
        </Box>
        <Divider sx={{ mt: 1, mb: 1 }} />
        {session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Storage")[0]
          .permission === "Edit" ? (
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Button variant="outlined" onClick={handleModalClose}>
              Close
            </Button>
            <Button variant="contained" type="submit" color="dark">
              Save
            </Button>
          </Box>
        ) : null}
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
