"use client";
import * as React from "react";
import Card from "@mui/material/Card";
import {
  Box,
  Button,
  Stack,
  Modal,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Divider,
  TextField,
  Tooltip,
  CircularProgress,
  Input,
  FormHelperText,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { DataGrid, GridActionsCellItem } from "@mui/x-data-grid";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import useSWR from "swr";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import { useFormik } from "formik";
import { fetchData } from "@/lib/fetch";
import CustomNoRowsOverlay from "@/components/NoRecord";
import IconButton from "@mui/material/IconButton";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import dayjs from "dayjs";
import MyAlert from "@/components/Myalert";
import CloseIcon from "@mui/icons-material/Close";
import { useRouter } from "next/navigation";
import VisibilityIcon from '@mui/icons-material/Visibility';

export default function AttachmentPage({ trade_id }) {
  const { data: session } = useSession();
  const router = useRouter();
  const [open, setOpen] = React.useState(false);
  const [opens, setOpens] = React.useState(false);
  const [image, setImage] = React.useState(null);
  const [loading, setLoading] = React.useState(false);

  const columns = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "action",
      renderHeader: () => <strong>{"Action"}</strong>,
      // flex: 1,
      width: 100,
      sortable: false,
      renderCell: (params) => {
        const handleDelete = (e) => {
          const currentRow = params.row;
          deleteAttachment(currentRow);
        };
        const handlePreview = ()=>{
          // router.push(`${params.row.attachment_url}`)
          window.open( `${params.row.attachment_url}`, '_blank');
        }
        return (
          <Stack direction="row" spacing={2}>
            <Tooltip title="Delete">
              <GridActionsCellItem
                icon={<DeleteIcon />}
                label="Delete"
                color="error"
                onClick={handleDelete}
              />
            </Tooltip>
            <Tooltip title="Preview">
              <GridActionsCellItem
                icon={<VisibilityIcon />}
                label="Preview"
                color="primary"
                onClick={handlePreview}
              />
            </Tooltip>
           
          </Stack>
        );
      },
    },
    {
      field: "attachment_name",
      renderHeader: () => <strong>{"Attachment name"}</strong>,
      width: 150,
    },

    {
      field: "attachment_type",
      renderHeader: () => <strong>{"Attachment type"}</strong>,
      width: 150,
    },

    {
      field: "filename",
      renderHeader: () => <strong>{"Filename"}</strong>,
      width: 150,
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Attachment date"}</strong>,
      width: 150,
      valueGetter: (params) =>
        dayjs(params.row.creation_date).format("YYYY-MM-DD"),
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 350,
    },

  ];

  const formik = useFormik({
    initialValues: {
      attachmentname: "",
      attachmenttype: "",
      description: "",
      filename: "",
      filetype:'',
      attachmenturl: "",
      sourcefile: "",
      method: null,
    },
    validationSchema: yup.object({
      method: yup.string(),
      attachmentname: yup.string().required("Please insert Name!"),
      attachmenttype: yup.string().required("Please Select Type!"),
      description: yup.string().required("Please insert description!"),
    }),
    onSubmit: async (values) => {
      saveAttachment(values);
      handleClose();
    },
    enableReinitialize: true,
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const { data: types } = useSWR(
    ["/api/lookup/Attachment_type", session?.user?.token],
    fetcher
  );

  const {
    data: attachments,
    error,
    isLoading,
    mutate,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/attachment/${trade_id}`, session?.user?.token]
      : null,
    fetcher,
    { refreshInterval: 1000 }
  );
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );
    const deleteAttachment = async (data) => {
      try {
        setOpens(true);
        const body = {
          trade_id: trade_id,
          username: session?.user?.username,
          bucket: "trade",
          id: trade_id,
          filename: data.filename,
          trade_attachment_id: data.trade_attachment_id,
        };
  
        const res = await fetchData(
          `/api/trade/attachment`,
          "DELETE",
          body,
          session?.user?.token
        );
        if (res.status === 200) {
          let message = null;
          message = "Амжилттай устгалаа ";
          mutate();
          toast.success(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error(res.result, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
        console.log(`error`, error);
        toast.error("Устгах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } finally {
        setOpens(false);
      }
    };
  const saveAttachment = async (data) => {
    if (trade_id === "new") {
      toast.error("Trade үүсээгүй байна", {
        position: toast.POSITION.TOP_RIGHT,
      });
      handleClose();
    } else {
      const formData = new FormData();
      formData.append("id", trade_id);
      formData.append("bucket", "trade");
      formData.append("trade_id", trade_id);
      formData.append("attachment_type", data.filetype);
      formData.append("attachment_name", data.attachmentname);
      formData.append("filename", data.filename);
      formData.append("description", data.description);
      formData.append("username", session?.user?.username);
      formData.append("filelength", data.filelength);
      formData.append("trade_attachment_id", data.method  === "POST" ? undefined : data.id);
      formData.append("sourcefile", data.sourcefile);
      try {
        setLoading(true);
        setOpens(true);
        
          const body = {
            trade_id: trade_id,
            attachment_type: data.filetype,
            attachment_name: data.attachmentname,
            filename: data.filename,
            description: data.description,
            username: session?.user?.username,
            bucket: 'trade',
            id: trade_id,
            sourcefile: data.sourcefile,
            filetype: data.filetype,
            filelength: data.filelength,
            trade_attachment_id: data.method === "POST" ? undefined : data.id,
          };

          const res = await fetch(`/api/trade/attachment`, {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${session?.user?.token}`,
            },
            body: formData,
          });
          if (res.status === 200) {
            let message = null;
            message = "Амжилттай хадгаллаа";
            mutate(attachments);
            toast.success(message, {
              position: toast.POSITION.TOP_RIGHT,
              className: "toast-message",
            });
          } else {
            toast.error(res.result, {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
        // }
      } catch (error) {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } finally {
        setLoading(false);
        setOpens(false);
      }
    }
  };

  const uploadToClient = (event) => {
    if (event.target.files && event.target.files[0]) {
      const i = event.target.files[0];
      setImage(i); 
      formik.setFieldValue("filename", event.target.files[0].name);
      formik.setFieldValue("filelength", event.target.files[0].size);
      formik.setFieldValue("filetype", event.target.files[0].type);
      formik.setFieldValue("sourcefile", i);
    }
  };
  const attachmentsRows = attachments?.result.map((row, i) => ({
    id: i + 1,
    ...row,
  }));
  return (
    <Card>
      <ToastContainer />
      <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
      {session && session?.user?.menuitems.filter(f => f.menu_name === "Trade")[0].permission==="Edit"  ? (
        <Button
          variant="text"
          size="small"
          sx={{ justifyContent: "flex-end", m: 1 }}
          onClick={() => {
            formik.setValues({
              id: null,
              attachmentname: "",
              attachmenttype: "",
              description: "",
              attachmenturl: "",
              filename: "",
              method: "POST",
            });

            handleClickOpen();
          }}
          startIcon={<AddSharpIcon />}
        >
          add
        </Button>):null}
      </Box>
      <DataGrid
        getRowId={(row) => row.trade_attachment_id}
        rows={attachmentsRows}
        columns={columns}
        fullWidth
        initialState={{
          pagination: {
            paginationModel: {
              pageSize: 5,
            },
          },
        }}
        rowHeight={25}
        autoHeight
        slots={{
          noRowsOverlay: CustomNoRowsOverlay
        }}
        sx={{
          backgroundColor: "#FFFFFF",
          m: 1,
          "& .MuiDataGrid-cell:hover": {
            color: "primary.main",
          },
          "& .MuiDataGrid-cell": {
            borderTop: 1,
            borderTopColor: "#E9ECF0",
          },
        }}
        pageSizeOptions={[50]}
      />

      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            border: "1px solid #000",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          {/* {attachment.method === "DELETE" ? (
            <>
              <Box>
                <Typography variant="h6" component="h3">
                  Attachment delete
                </Typography>
              </Box>
              <Stack
                component="div"
                sx={{
                  width: 400,
                  maxWidth: "100%",
                }}
                noValidate
                autoComplete="off"
              >
                <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                  <Alert severity="error">
                    <AlertTitle>{attachment.attachmentname}</AlertTitle>
                    are you sure delete!
                  </Alert>
                </Box>
              </Stack>
              <Box
                sx={{
                  display: "flex",
                  marginBottom: 1,
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  onClick={async () => {
                    saveAttachment();
                    handleClose();
                  }}
                  variant="contained"
                  sx={{
                    marginLeft: "10px",
                    marginRight: "10px",
                  }}
                >
                  Yes
                </Button>
                <Button onClick={handleClose} variant="outlined">
                  No
                </Button>
              </Box>
            </>
          ) : (
            <> */}
          <form onSubmit={formik.handleSubmit}>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography variant="h5" gutterBottom>
                Attachment
              </Typography>
              <IconButton
                sx={{ display: "flex", mt: -1 }}
                aria-label="close"
                onClick={handleClose}
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Divider sx={{ mb: 1 }} />
            <Stack direction="column" spacing={1}>
              <FormControl>
                <TextField
                  name="attachmentname"
                  fullWidth
                  variant="outlined"
                  size="small"
                  label="Name"
                  value={formik.values.attachmentname}
                  onChange={formik.handleChange}
                />
                <FormHelperText
                  error={
                    formik.touched.attachmentname &&
                    Boolean(formik.errors.attachmentname)
                  }
                >
                  {formik.touched.attachmentname &&
                    formik.errors.attachmentname}
                </FormHelperText>
              </FormControl>
              <FormControl size="small">
                <InputLabel id="attachmenttype">Type</InputLabel>
                <Select
                  name="attachmenttype"
                  label="Type"
                  value={formik.values.attachmenttype}
                  onChange={formik.handleChange}
                >
                  {types?.result.length > 0 &&
                    types?.result.map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.attachmenttype &&
                    Boolean(formik.errors.attachmenttype)
                  }
                >
                  {formik.touched.attachmenttype &&
                    formik.errors.attachmenttype}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="description"
                  label="Description"
                  multiline
                  rows={15}
                  variant="outlined"
                  value={formik.values.description}
                  onChange={formik.handleChange}
                />
                <FormHelperText
                  error={
                    formik.touched.description &&
                    Boolean(formik.errors.description)
                  }
                >
                  {formik.touched.description && formik.errors.description}
                </FormHelperText>
              </FormControl>
              <FormControl>
                <TextField
                  name="filename"
                  label="Filename"
                  variant="outlined"
                  size="small"
                  disabled
                  value={formik.values.filename}
                  onChange={formik.handleChange}
                />
                <FormHelperText
                  error={
                    formik.touched.filename && Boolean(formik.errors.filename)
                  }
                >
                  {formik.touched.filename && formik.errors.filename}
                </FormHelperText>
              </FormControl>
            </Stack>
            <Input type="file" name="file" onChange={uploadToClient} />

            <Divider sx={{ mt: 1 }} />
            <Box
              sx={{
                display: "flex",
                mb: 1,
                mt: 1,
                justifyContent: "space-between",
              }}
            >
              <Button variant="outlined" onClick={() => handleClose()}>
                Close
              </Button>
           
                <>
                  <Button type="submit" variant="contained" color="dark">
                    Save
                  </Button>
                </>
           
            </Box>
          </form>
          {/* </>
          )} */}
        </Box>
      </Modal>
      <Modal open={opens}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </Card>
  );
}
