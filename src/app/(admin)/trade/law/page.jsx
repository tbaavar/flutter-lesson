"use client";

import * as React from "react";
import Card from "@mui/material/Card";
import {
  Box,
  CircularProgress,
  Stack,
  FormHelperText,
  Divider,
  Modal,
} from "@mui/material";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import { useStore } from "@/store/store";
import { toast } from "react-toastify";
import { fetchData } from "@/lib/fetch";
import useSWR from "swr";
import InputLabel from "@mui/material/InputLabel";
import * as yup from "yup";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

export default function LawPage({ trade_id }) {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const setMenu = useStore((state) => state.setMenu);

  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );

  const { data: quality_test_location } = useSWR(
    [`/api/lookup/port`, session?.user?.token],
    fetcher
  );


  const {
    data: general,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/law/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const formik = useFormik({
    initialValues: {
      notes: general?.result?.notes ?? "",
      conflict: general?.result?.conflict ?? "",
      law_id: general?.result?.law_id ?? "",
      title_to_pass: general?.result?.title_to_pass ?? "",
      title_to_pass_other: general?.result?.title_to_pass_other ?? "",
      risk_to_pass: general?.result?.risk_to_pass ?? "",
      risk_to_pass_other: general?.result?.risk_to_pass_other ?? "",
      general_term_condition: general?.result?.general_term_condition ?? "",
      quality_test_location: general?.result?.quality_test_location ?? "",
      quality_test_other: general?.result?.quality_test_other ?? "",
      quantity_test_port_id: general?.result?.quantity_test_port_id ?? "",
      quantity_test_other: general?.result?.quantity_test_other ?? "",
      q_q_timebar: general?.result?.q_q_timebar ?? "",
      q_q_from_date: general?.result?.q_q_from_date ?? "",
      q_q_other: general?.result?.q_q_other ?? "",
      claim_timebar: general?.result?.claim_timebar ?? "",
      claim_from_date: general?.result?.claim_from_date ?? "",
      claim_other: general?.result?.claim_other ?? "",
      inspection_share_port: general?.result?.inspection_share_port ?? "",
      inspection_share_disport: general?.result?.inspection_share_disport ?? "",
      contract_received_date:
        general?.result?.contract_received_date ?? dayjs().format("YYYY-MM-DD"),
      response_sent_date:
        general?.result?.response_sent_date ?? dayjs().format("YYYY-MM-DD"),
      contract_drafted_date:
        general?.result?.contract_drafted_date ?? dayjs().format("YYYY-MM-DD"),
      contact_sent_date:
        general?.result?.contact_sent_date ?? dayjs().format("YYYY-MM-DD"),
      broker_contract_received_date:
        general?.result?.broker_contract_received_date ??
        dayjs().format("YYYY-MM-DD"),
      contract_finish_flag: general?.result?.contract_finish_flag ?? "N",
      contract_date_comment: general?.result?.contract_date_comment ?? "",
    },
    validationSchema: yup.object({
      law_id: yup.string().required("Please select Law!"),
    
      quality_test_location: yup
        .string()
        .required("Please select Location of Quality Test!"),
      // q_q_timebar: yup.string().required("Plese select Q&Q Timebar!"),
      // q_q_from_date: yup.string().required("Please select From!"),
      // claim_timebar: yup
      //   .string()
      //   .required("Plese select Other Claims Timebar!"),
      // claim_from_date: yup.string().required("Please select From!"),
      // inspection_share_port: yup
      //   .number()
      //   .max(100, "хувь 100-аас илүү байж болохгүй!")
      //   .typeError("Please insert Number!")
      //   .required("Please fill LoadPort!"),
      // inspection_share_disport: yup
      //   .number()
      //   .max(100, "хувь 100-аас илүү байж болохгүй!")
      //   .typeError("Please insert Number!")
      //   .required("Please fill DisPort!"),
    }),
    onSubmit: (values) => {
      saveLawData(values);
    },

    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );
  const saveLawData = async (data) => {
    const body = {
      trade_id: trade_id,
      notes: data.notes,
      conflict: data.conflict,
      law_id: data.law_id,
      title_to_pass: data.title_to_pass,
      title_to_pass_other: data.title_to_pass_other,
      risk_to_pass: data.risk_to_pass,
      risk_to_pass_other: data.risk_to_pass_other,
      general_term_condition: data.general_term_condition,
      quality_test_location: data.quality_test_location,
      quality_test_other: data.quality_test_other,
      quantity_test_port_id: data.quantity_test_port_id,
      quantity_test_other: data.quantity_test_other,
      q_q_timebar: data.q_q_timebar,
      q_q_from_date: data.q_q_from_date,
      q_q_other: data.q_q_other,
      claim_timebar: data.claim_timebar,
      claim_from_date: data.claim_from_date,
      claim_other: data.claim_other,
      inspection_share_port: data.inspection_share_port,
      inspection_share_disport: data.inspection_share_disport,
      contract_received_date: data.contract_received_date,
      response_sent_date: data.response_sent_date,
      contract_drafted_date: data.contract_drafted_date,
      contact_sent_date: data.contact_sent_date,
      broker_contract_received_date: data.broker_contract_received_date,
      contract_finish_flag: data.contract_finish_flag,
      contract_date_comment: data.contract_date_comment,
      username: session?.user?.username,
      trade_law_id: general.result ? general.result.trade_law_id : undefined,
    };

    try {
      setLoading(true);
      setOpen(true);
      const method = general.result ? "PUT" : "POST";
      const res = await fetchData(
        `/api/trade/law`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
        setMenu(4);
      } else {
        toast.error("Илгээх үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error("алдаа гарлаа", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <Card>
          <Box sx={{ m: 1 }}>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}></Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Law
                </InputLabel>
                <Select
                  size="small"
                  name="law_id"
                  label="Law"
                  value={formik.values.law_id}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Law")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={formik.touched.law_id && Boolean(formik.errors.law_id)}
                >
                  {formik.touched.law_id && formik.errors.law_id}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small">Conflict</InputLabel>
                <Select
                  size="small"
                  name="conflict"
                  label="Conflict"
                  value={formik.values.conflict}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Law")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <TextField
                  id="notes"
                  name="notes"
                  size="small"
                  value={formik.values.notes}
                  onChange={formik.handleChange}
                  label="Notes"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Title to pass
                </InputLabel>
                <Select
                  size="small"
                  name="title_to_pass"
                  value={formik.values.title_to_pass}
                  onChange={formik.handleChange}
                  label="  Title to Pass"
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Law_title")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.title_to_pass &&
                    Boolean(formik.errors.title_to_pass)
                  }
                >
                  {formik.touched.title_to_pass && formik.errors.title_to_pass}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="title_to_pass_other"
                  size="small"
                  value={formik.values.title_to_pass_other}
                  onChange={formik.handleChange}
                  label="Title to pass other"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Risk to pass
                </InputLabel>
                <Select
                  size="small"
                  name="risk_to_pass"
                  value={formik.values.risk_to_pass}
                  onChange={formik.handleChange}
                  label=" Risk to pass"
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Law_title")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.risk_to_pass &&
                    Boolean(formik.errors.risk_to_pass)
                  }
                >
                  {formik.touched.risk_to_pass && formik.errors.risk_to_pass}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="risk_to_pass_other"
                  size="small"
                  value={formik.values.risk_to_pass_other}
                  onChange={formik.handleChange}
                  label="Risk to pass other"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <TextField
                  id="general_term_condition"
                  name="general_term_condition"
                  size="small"
                  multiline
                  rows={10}
                  value={formik.values.general_term_condition}
                  onChange={formik.handleChange}
                  label="General term condition"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: " #0B626B" }}>
                  Location of quality test
                </InputLabel>
                <Select
                  id="quality_test_location"
                  name="quality_test_location"
                  size="small"
                  label="Location of quality Test"
                  value={formik.values.quality_test_location}
                  onChange={formik.handleChange}
                >
                  {quality_test_location?.result.length >= 0 &&
                    quality_test_location?.result.map((item) => (
                      <MenuItem value={item.port_id} key={item.port_id}>
                        {item.port_name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.quality_test_location &&
                    Boolean(formik.errors.quality_test_location)
                  }
                >
                  {formik.touched.quality_test_location &&
                    formik.errors.quality_test_location}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  id="quality_test_other"
                  size="small"
                  name="quality_test_other"
                  value={formik.values.quality_test_other}
                  onChange={formik.handleChange}
                  label="Location of quality other"
                />
              </FormControl>
            </Stack>
            {/* <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <Stack direction={"row"} spacing={1}>
                  <FormControl fullWidth>
                    <InputLabel size="small" sx={{ color: " #0B626B" }}>
                      Quantity of quality timebar
                    </InputLabel>
                    <Select
                      size="small"
                      name="q_q_timebar"
                      label="Quantity of quality timebar"
                      value={formik.values.q_q_timebar}
                      onChange={formik.handleChange}
                    >
                      {lookup?.result.length >= 0 &&
                        lookup?.result
                          .filter(
                            (f) => f.lookup_type === "Notification_timebar"
                          )
                          .map((item) => (
                            <MenuItem
                              value={item.lookup_code}
                              key={item.lookup_code}
                            >
                              {item.name}
                            </MenuItem>
                          ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.q_q_timebar &&
                        Boolean(formik.errors.q_q_timebar)
                      }
                    >
                      {formik.touched.q_q_timebar && formik.errors.q_q_timebar}
                    </FormHelperText>
                  </FormControl>
                  <FormControl fullWidth>
                    <InputLabel size="small" sx={{ color: " #0B626B" }}>
                      Quantity of quality from
                    </InputLabel>
                    <Select
                      size="small"
                      name="q_q_from_date"
                      label="Quantity of quality from"
                      value={formik.values.q_q_from_date}
                      onChange={formik.handleChange}
                    >
                      {lookup?.result.length >= 0 &&
                        lookup?.result
                          .filter(
                            (f) => f.lookup_type === "Notification_timebar"
                          )
                          .map((item) => (
                            <MenuItem
                              value={item.lookup_code}
                              key={item.lookup_code}
                            >
                              {item.name}
                            </MenuItem>
                          ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.q_q_from_date &&
                        Boolean(formik.errors.q_q_from_date)
                      }
                    >
                      {formik.touched.q_q_from_date &&
                        formik.errors.q_q_from_date}
                    </FormHelperText>
                  </FormControl>
                </Stack>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  id="q_q_other"
                  name="q_q_other"
                  size="small"
                  value={formik.values.q_q_other}
                  onChange={formik.handleChange}
                  label="Quantity of quality other"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <Stack direction={"row"} spacing={1}>
                  <FormControl fullWidth>
                    <InputLabel size="small" sx={{ color: " #0B626B" }}>
                      Other claims timebar
                    </InputLabel>
                    <Select
                      size="small"
                      name="claim_timebar"
                      label="Other claims timebar"
                      value={formik.values.claim_timebar}
                      onChange={formik.handleChange}
                    >
                      {lookup?.result.length >= 0 &&
                        lookup?.result
                          .filter((f) => f.lookup_type === "Period")
                          .map((item) => (
                            <MenuItem
                              value={item.lookup_code}
                              key={item.lookup_code}
                            >
                              {item.name}
                            </MenuItem>
                          ))}
                    </Select>
                    <FormHelperText
                      error={
                        formik.touched.claim_timebar &&
                        Boolean(formik.errors.claim_timebar)
                      }
                    >
                      {formik.touched.claim_timebar &&
                        formik.errors.claim_timebar}
                    </FormHelperText>
                  </FormControl>
                  <FormControl fullWidth>
                    <FormControl>
                      <InputLabel size="small" sx={{ color: " #0B626B" }}>
                        Other claims from
                      </InputLabel>
                      <Select
                        size="small"
                        name="claim_from_date"
                        label="Other claims from"
                        value={formik.values.claim_from_date}
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                      >
                        {lookup?.result.length >= 0 &&
                          lookup?.result
                            .filter(
                              (f) =>
                                f.lookup_type === "Notification_calc_method"
                            )
                            .map((item) => (
                              <MenuItem
                                value={item.lookup_code}
                                key={item.lookup_code}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                      </Select>
                      <FormHelperText
                        error={
                          formik.touched.claim_from_date &&
                          Boolean(formik.errors.claim_from_date)
                        }
                      >
                        {formik.touched.claim_from_date &&
                          formik.errors.claim_from_date}
                      </FormHelperText>
                    </FormControl>
                  </FormControl>
                </Stack>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  name="claim_other"
                  size="small"
                  value={formik.values.claim_other}
                  onChange={formik.handleChange}
                  label="Other claims other"
                />
              </FormControl>
            </Stack> */}
          </Box>
          {/* <Box
            component="fieldset"
            alignItems={"top"}
            sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
          >
            <legend> Inspection share</legend>
            <Stack direction={"row"} spacing={1}>
              <FormControl fullWidth>
                <TextField
                  name="inspection_share_port"
                  size="small"
                  value={formik.values.inspection_share_port}
                  error={
                    formik.touched.inspection_share_port &&
                    Boolean(formik.errors.inspection_share_port)
                  }
                  helperText={
                    formik.touched.inspection_share_port &&
                    formik.errors.inspection_share_port
                  }
                  onChange={formik.handleChange}
                  label="Load Port(%)"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  id="inspection_share_disport"
                  name="inspection_share_disport"
                  size="small"
                  value={formik.values.inspection_share_disport}
                  error={
                    formik.touched.inspection_share_disport &&
                    Boolean(formik.errors.inspection_share_disport)
                  }
                  helperText={
                    formik.touched.inspection_share_disport &&
                    formik.errors.inspection_share_disport
                  }
                  onChange={formik.handleChange}
                  label="Disport(%)"
                />
              </FormControl>
            </Stack>
          </Box> */}
          <Box
            component="fieldset"
            alignItems={"top"}
            sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
          >
            <legend>Contract dates</legend>
            <Stack direction={"row"} spacing={1} sx={{ mb: 1 }}>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    slotProps={{ textField: { size: "small" } }}
                    label="Contract Received"
                    name="contract_received_date"
                    value={dayjs(formik.values.contract_received_date)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "contract_received_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    slotProps={{ textField: { size: "small" } }}
                    label="Response Sent"
                    name="response_sent_date"
                    value={dayjs(formik.values.response_sent_date)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "response_sent_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    slotProps={{ textField: { size: "small" } }}
                    label="Broker contract received date"
                    name="broker_contract_received_date"
                    value={dayjs(formik.values.broker_contract_received_date)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "broker_contract_received_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <FormControlLabel
                  control={
                    <Checkbox
                      size="small"
                      checked={
                        formik.values.contract_finish_flag === "Y"
                          ? true
                          : false
                      }
                      onChange={(e) => {
                        formik.setFieldValue(
                          "contract_finish_flag",
                          e.target.checked ? "Y" : "N"
                        );
                      }}
                    />
                  }
                  label="Contract finalised"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1}>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    slotProps={{ textField: { size: "small" } }}
                    label="Contract Drafted"
                    name="contract_drafted_date"
                    value={dayjs(formik.values.contract_drafted_date)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "contract_drafted_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    slotProps={{ textField: { size: "small" } }}
                    label="Contract Sent"
                    name="contact_sent_date"
                    value={dayjs(formik.values.contact_sent_date)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "contact_sent_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth></FormControl>
              <FormControl fullWidth></FormControl>
            </Stack>
          </Box>
          <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
            <FormControl fullWidth>
              <TextField
                name="contract_date_comment"
                value={formik.values.contract_date_comment}
                onChange={formik.handleChange}
                label="Comment"
              />
            </FormControl>
          </Stack>
          <Divider sx={{ m: 1 }} />
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              m: 1,
            }}
          >
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
              .permission === "Edit" ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
          </Box>
        </Card>
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
