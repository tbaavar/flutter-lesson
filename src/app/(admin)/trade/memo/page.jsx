"use client";
import * as React from "react";
import TextField from "@mui/material/TextField";
import {
  Box,
  Button,
  Card,
  CircularProgress,
  Divider,
  FormControl,
  Modal,
} from "@mui/material";
import useSWR from "swr";
import MyAlert from "@/components/Myalert";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import { useFormik } from "formik";
import { fetchData } from "@/lib/fetch";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";

import { Editor } from "primereact/editor";

export default function MemoPage({ trade_id }) {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const {
    data: memo,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/memo/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const formik = useFormik({
    initialValues: {
      memo: memo?.result?.memo ?? "",
    },
    validationSchema: yup.object({}),
    onSubmit: async (values) => {
      const message = values.memo || "";
      const cleanMessage = message.replace(/<[^>]*>/g, "");
      const body = {
        trade_memo_id: memo?.result ? memo.result.trade_memo_id : undefined,
        trade_id: trade_id,
        // memo: values.memo,
        memo: cleanMessage,
        username: session?.user?.mail,
      };

      try {
        setLoading(true);
        setOpen(true);
        const res = await fetchData(
          `/api/trade/memo`,
          memo.result ? "PUT" : "POST",
          body,
          session?.user?.token
        );

        if (res.status === 200) {
          let message = null;
          message = "Амжилттай хадгаллаа";

          toast.success(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error("Хадгалах үед алдаа гарлаа", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
      } finally {
        setLoading(false);
        setOpen(false);
      }
    },

    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <Editor
          id="memo"
          name="memo"
          value={formik.values.memo}
          onTextChange={(e) => {
            formik.setFieldValue("memo", e.htmlValue);
          }}
          style={{ height: "320px" }}
        />

        <Divider sx={{ m: 1 }} />
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            m: 1,
          }}
        >
        {session && session?.user?.menuitems.filter(f => f.menu_name === "Trade")[0].permission==="Edit"  ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
        </Box>
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
