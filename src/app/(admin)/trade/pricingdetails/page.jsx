"use client";
import * as React from "react";
import {
  Divider,
  Modal,
  Stack,
  Card,
  Box,
  CircularProgress,
  FormControl,
  Select,
  MenuItem,
  TextField,
  Button,
  InputLabel,
  FormHelperText,
  InputAdornment,
} from "@mui/material";
import useSWR from "swr";
import { toast } from "react-toastify";
import { fetchData } from "@/lib/fetch";
import * as yup from "yup";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

export default function PricingDetails({ trade_id }) {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );

  const { data: product } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/product/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const {
    data: general,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/pricingdetail/fixed/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const formik = useFormik({
    initialValues: {
      currency: general?.result?.freight_unit ?? "",
      unit: product?.result?.units ?? "",
      amount: general?.result?.amount ?? "",
      total_amount: general?.result?.total_amount ?? "",
      amount_other: general?.result?.amount_other ?? "",
      total_amount_other: general?.result?.total_amount_other ?? "",
      cent_flag: general?.result?.cent_flag ?? "N",
    },
    validationSchema: yup.object({
      amount: yup
        .number()
        .required("Please insert Amount!")
        .typeError("Please insert number!"),
    }),
    onSubmit: async (values) => {
      savePriceData(values);
    },

    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  const savePriceData = async (data) => {
    const body = {
      trade_id: trade_id,
      amount: data.amount,
      total_amount: data.total_amount,
      amount_other: data.amount_other,
      total_amount_other: data.total_amount_other,
      currency: "MNT",
      freight_unit: data.currency,
      cent_flag: data.cent_flag,
      unit: data.unit,
      trade_fixed_pricing_id: general.result
        ? general.result.trade_fixed_pricing_id
        : undefined,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const method = general.result ? "PUT" : "POST";
      console.log("PPPPPPP", body);
      const res = await fetchData(
        `/api/trade/pricingdetail/fixed`,
        method,
        body,
        session?.user?.token
      );
      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };
  return (
    <>
      <Card>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={{ m: 1 }}>
            <Stack direction={"column"} spacing={1} sx={{ mt: 2 }}>
              <Stack direction={"row"} spacing={1}>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Unit
                  </InputLabel>
                  <Select
                    labelId="unit"
                    name="unit"
                    label="Unit"
                    readOnly={true}
                    value={formik.values.unit}
                    onBlur={formik.handleBlur}
                    onChange={(e) => {
                      formik.setFieldValue("unit", e.target.value);
                    }}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Unit")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.per_unit && Boolean(formik.errors.per_unit)
                    }
                  >
                    {formik.touched.per_unit && formik.errors.per_unit}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    name="amount"
                    size="small"
                    value={parseFloat(formik.values.amount)}
                    onChange={(e) => {
                      formik.setFieldValue(
                        "amount",
                        e.target.value ? e.target.value : 0
                      );
                      formik.values.unit === "CM"
                        ? formik.setFieldValue(
                            "total_amount",
                            parseFloat(
                              e.target.value * product?.result?.quantity_cm
                            ).toFixed(2)
                          )
                        : formik.values.unit === "MT"
                        ? formik.setFieldValue(
                            "total_amount",

                            parseFloat(
                              e.target.value * product?.result.quantity
                            ).toFixed(2)
                          )
                        : null;
                    }}
                    InputLabelProps={{
                      style: { color: "#0B626B" },
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">₮</InputAdornment>
                      ),
                    }}
                    onBlur={formik.handleBlur}
                    label="Unit Price in MNT"
                  />
                  <FormHelperText
                    error={
                      formik.touched.amount && Boolean(formik.errors.amount)
                    }
                  >
                    {formik.touched.amount && formik.errors.amount}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    name="total_amount"
                    size="small"
                    disabled
                    value={parseFloat(formik.values.total_amount).toFixed(2)}
                    InputLabelProps={{
                      style: { color: "#0B626B" },
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">₮</InputAdornment>
                      ),
                    }}
                    onBlur={formik.handleBlur}
                    label="Total amount in MNT"
                  />
                  <FormHelperText
                    error={
                      formik.touched.total_amount &&
                      Boolean(formik.errors.total_amount)
                    }
                  >
                    {formik.touched.total_amount && formik.errors.total_amount}
                  </FormHelperText>
                </FormControl>
              </Stack>
              <Stack direction={"row"} spacing={1}>
                <FormControl fullWidth>
                  <InputLabel size="small">Currency</InputLabel>
                  <Select
                    labelId="currency"
                    name="currency"
                    label="Currency"
                    value={formik.values.currency}
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Currency")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.currency && Boolean(formik.errors.currency)
                    }
                  >
                    {formik.touched.currency && formik.errors.currency}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    name="amount_other"
                    size="small"
                    value={parseFloat(formik.values.amount_other)}
                    onChange={(e) => {
                      formik.setFieldValue(
                        "amount_other",
                        e.target.value ? e.target.value : 0
                      );

                      formik.values.unit === "CM"
                        ? formik.setFieldValue(
                            "total_amount_other",

                            parseFloat(
                              e.target.value * product?.result?.quantity_cm
                            ).toFixed(2)
                          )
                        : formik.values.unit === "MT"
                        ? formik.setFieldValue(
                            "total_amount_other",

                            parseFloat(
                              e.target.value * product?.result.quantity
                            ).toFixed(2)
                          )
                        : null;
                    }}
                    InputProps={{
                      endAdornment:
                        formik.values.currency === "USD " ? (
                          <InputAdornment position="end">$</InputAdornment>
                        ) : formik.values.currency === "RMB" ? (
                          <InputAdornment position="end">¥</InputAdornment>
                        ) : formik.values.currency === "MNT" ? (
                          <InputAdornment position="end">₮</InputAdornment>
                        ) : formik.values.currency === "RUB" ? (
                          <InputAdornment position="end">₽</InputAdornment>
                        ) : null,
                    }}
                    onBlur={formik.handleBlur}
                    error={formik.error}
                    label="Unit Price in Currency"
                  />
                </FormControl>

                <FormControl fullWidth>
                  <TextField
                    name="total_amount_other"
                    size="small"
                    value={formik.values.total_amount_other}
                    disabled
                    InputProps={{
                      endAdornment:
                        formik.values.currency === "USD " ? (
                          <InputAdornment position="end">$</InputAdornment>
                        ) : formik.values.currency === "RMB" ? (
                          <InputAdornment position="end">¥</InputAdornment>
                        ) : formik.values.currency === "MNT" ? (
                          <InputAdornment position="end">₮</InputAdornment>
                        ) : formik.values.currency === "RUB" ? (
                          <InputAdornment position="end">₽</InputAdornment>
                        ) : null,
                    }}
                    label="Total amount in Currency"
                  />
                </FormControl>
              </Stack>
            </Stack>
          </Box>
          <Divider sx={{ m: 1 }} />
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              m: 1,
            }}
          >
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
              .permission === "Edit" ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
          </Box>
        </form>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
