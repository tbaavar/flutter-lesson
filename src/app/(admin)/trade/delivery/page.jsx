"use client";
import * as React from "react";
import dayjs from "dayjs";
import {
  Box,
  CircularProgress,
  FormHelperText,
  Card,
  Typography,
  MenuItem,
  TextField,
  Button,
  FormControl,
  Select,
  Checkbox,
  InputLabel,
  Divider,
  Stack,
  Modal,
} from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import useSWR from "swr";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import MyAlert from "@/components/Myalert";
import { fetchData } from "@/lib/fetch";
import * as yup from "yup";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";

const label = { inputProps: { "aria-label": "Checkbox demo" } };

export default function DeliveryPage({ trade_id }) {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const { data: lookup } = useSWR(
    ["/api/lookup", session?.user?.token],
    fetcher
  );
  const { data: load_port_id } = useSWR(
    ["/api/lookup/port", session?.user?.token],
    fetcher
  );
  const { data: discharge_port_id } = useSWR(
    ["/api/lookup/port", session?.user?.token],
    fetcher
  );
  const {
    data: general,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/delivery/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const formik = useFormik({
    validateOnMount: true,
    initialValues: {
      load_port_id: general?.result?.load_port_id ?? "",
      discharge_port_id: general?.result?.discharge_port_id ?? "",
      intended_vessel_id: general?.result?.intended_vessel_id ?? "",
      laytime: general?.result?.laytime ?? "",
      nor: general?.result?.nor ?? "",
      pro_rata_cargo_flag: general?.result?.pro_rata_cargo_flag ?? "",
      dates_based_on: general?.result?.dates_based_on ?? "",
      date_range_from:
        general?.result?.date_range_from ?? dayjs().format("YYYY-MM-DD"),
      date_range_to:
        general?.result?.date_range_to ?? dayjs().format("YYYY-MM-DD"),
      quantity_based_on: general?.result?.quantity_based_on ?? "",
      narrowed_by: general?.result?.narrowed_by ?? "",
      narrowed_lay_day: general?.result?.narrowed_lay_day ?? "",
      demurrage_flag: general?.result?.demurrage_flag ?? "N",
      pro_rata_per_day_flag: general?.result?.pro_rata_per_day_flag ?? "",
      spot_demurrage_rate: general?.result?.spot_demurrage_rate ?? "",
      spot_demurrage_rate_other:
        general?.result?.spot_demurrage_rate_other ?? "",
      spot_demurrage_rate_max: general?.result?.spot_demurrage_rate_max ?? "",
      time_demurrage_rate: general?.result?.time_demurrage_rate ?? "",
      time_demurrage_rate_other:
        general?.result?.time_demurrage_rate_other ?? "",
      time_demurrage_rate_max: general?.result?.time_demurrage_rate_max ?? "",
      notification_timebar: general?.result?.notification_timebar ?? "",
      notification_from_date: general?.result?.notification_from_date ?? "",
      notification_other: general?.result?.notification_other ?? "",
      fulldoc_timebar: general?.result?.fulldoc_timebar ?? "",
      fulldoc_from_date: general?.result?.fulldoc_from_date ?? "",
      fulldoc_other: general?.result?.fulldoc_other ?? "",
      pdc_to_apply: general?.result?.pdc_to_apply ?? "",
      claims_person_id: general?.result?.claims_person_id ?? "",
      duty_importer_record: general?.result?.duty_importer_record ?? "",
      duty_exporter_record: general?.result?.duty_exporter_record ?? "",
    },

    validationSchema: yup.object({
      load_port_id: yup.string().required("Please select load port!"),

      discharge_port_id: yup.string().required("Please select discharge port!"),
      intended_vessel_id: yup
        .string()
        .required("Please select transportation!"),
      dates_based_on: yup.string().required("Please select dates based on!"),
      quantity_based_on: yup
        .string()
        .required("Please select quantity based on!"),
      laytime: yup
        .number()
        .required("Please select Lay time!")
        .typeError("Please insert number!"),
    }),
    onSubmit: async (values) => {
      saveDeliveryData(values);
    },

    enableReinitialize: true,
  });

  const saveDeliveryData = async (data) => {
    const body = {
      trade_id: trade_id,
      load_port_id: data.load_port_id,
      discharge_port_id: data.discharge_port_id,
      intended_vessel_id: data.intended_vessel_id,
      laytime: data.laytime,
      nor: data.nor,
      pro_rata_cargo_flag: data.pro_rata_cargo_flag,
      dates_based_on: data.dates_based_on,
      date_range_from: data.date_range_from,
      date_range_to: data.date_range_to,
      quantity_based_on: data.quantity_based_on,
      narrowed_by: data.narrowed_by,
      narrowed_lay_day: data.narrowed_lay_day,
      demurrage_flag: data.demurrage_flag,
      pro_rata_per_day_flag: data.pro_rata_per_day_flag,
      spot_demurrage_rate: data.spot_demurrage_rate,
      spot_demurrage_rate_other: data.spot_demurrage_rate_other,
      spot_demurrage_rate_max: data.spot_demurrage_rate_max,
      time_demurrage_rate: data.time_demurrage_rate,
      time_demurrage_rate_other: data.time_demurrage_rate_other,
      time_demurrage_rate_max: data.time_demurrage_rate_max,
      notification_timebar: data.notification_timebar,
      notification_from_date: data.notification_from_date,
      notification_other: data.notification_other,
      fulldoc_timebar: data.fulldoc_timebar,
      fulldoc_from_date: data.fulldoc_from_date,
      fulldoc_other: data.fulldoc_other,
      pdc_to_apply: data.pdc_to_apply,
      claims_person_id: data.claims_person_id,
      duty_importer_record: data.duty_importer_record,
      duty_exporter_record: data.duty_exporter_record,
      first_purchaser_flag: "N",
      tax_withheld_flag: "N",
      trade_delivery_id: general.result
        ? general.result.trade_delivery_id
        : undefined,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const method = general.result ? "PUT" : "POST";
      const res = await fetchData(
        `/api/trade/delivery`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <Card>
          <Stack direction={"row"} spacing={1} sx={{ m: 1 }}></Stack>

          <Stack direction={"row"} spacing={1} sx={{ mt: 1, ml: 1, mr: 1 }}>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Load Port
              </InputLabel>
              <Select
                id="load_port_id"
                name="load_port_id"
                label="Load Port"
                size="small"
                value={formik.values.load_port_id}
                onChange={formik.handleChange}
              >
                {load_port_id?.result.length > 0 &&
                  load_port_id?.result.map((item) => (
                    <MenuItem value={item.port_id} key={item.port_id}>
                      {item.port_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.load_port_id &&
                  Boolean(formik.errors.load_port_id)
                }
              >
                {formik.touched.load_port_id && formik.errors.load_port_id}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Discharge Port
              </InputLabel>
              <Select
                id="discharge_port_id"
                name="discharge_port_id"
                size="small"
                label="Discharge Port"
                value={formik.values.discharge_port_id}
                onChange={formik.handleChange}
              >
                {discharge_port_id?.result.length > 0 &&
                  discharge_port_id?.result.map((item) => (
                    <MenuItem value={item.port_id} key={item.port_id}>
                      {item.port_name}
                    </MenuItem>
                  ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.discharge_port_id &&
                  Boolean(formik.errors.discharge_port_id)
                }
              >
                {formik.touched.discharge_port_id &&
                  formik.errors.discharge_port_id}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Transportation
              </InputLabel>
              <Select
                name="intended_vessel_id"
                size="small"
                label="Transportation"
                value={formik.values.intended_vessel_id}
                onChange={formik.handleChange}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Intended_Vessel")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.intended_vessel_id &&
                  Boolean(formik.errors.intended_vessel_id)
                }
              >
                {formik.touched.intended_vessel_id &&
                  formik.errors.intended_vessel_id}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <TextField
                name="laytime"
                size="small"
                type="number"
                value={formik.values.laytime}
                InputLabelProps={{
                  style: { color: " #0B626B" },
                }}
                onChange={formik.handleChange}
                label="Lay Time"
              />
              <FormHelperText
                error={formik.touched.laytime && Boolean(formik.errors.laytime)}
              >
                {formik.touched.laytime && formik.errors.laytime}
              </FormHelperText>
            </FormControl>
          </Stack>
          <Stack direction={"row"} spacing={1} sx={{ mt: 1, ml: 1, mr: 1 }}>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Date Based On
              </InputLabel>
              <Select
                size="small"
                name="dates_based_on"
                label="Dates Based On"
                value={formik.values.dates_based_on}
                onChange={formik.handleChange}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Delivery_based_on")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.dates_based_on &&
                  Boolean(formik.errors.dates_based_on)
                }
              >
                {formik.touched.dates_based_on && formik.errors.dates_based_on}
              </FormHelperText>
            </FormControl>
            <FormControl fullWidth>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                  format="YYYY-MM-DD"
                  value={dayjs(formik.values.date_range_from)}
                  name="date_range_from"
                  slotProps={{ textField: { size: "small" } }}
                  label="Date range from"
                  onChange={(newValue) => {
                    formik.setFieldValue(
                      "date_range_from",
                      dayjs(newValue).format("YYYY-MM-DD")
                    );
                  }}
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl fullWidth>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                  format="YYYY-MM-DD"
                  name="date_range_to"
                  value={dayjs(formik.values.date_range_to)}
                  slotProps={{ textField: { size: "small" } }}
                  label="Date range to"
                  onChange={(newValue) => {
                    formik.setFieldValue(
                      "date_range_to",
                      dayjs(newValue).format("YYYY-MM-DD")
                    );
                  }}
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small" sx={{ color: " #0B626B" }}>
                Quantity to base pricing on
              </InputLabel>
              <Select
                size="small"
                name="quantity_based_on"
                label="Quantity to base pricing on"
                value={formik.values.quantity_based_on}
                onChange={formik.handleChange}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Quantity_base")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
              <FormHelperText
                error={
                  formik.touched.quantity_based_on &&
                  Boolean(formik.errors.quantity_based_on)
                }
              >
                {formik.touched.quantity_based_on &&
                  formik.errors.quantity_based_on}
              </FormHelperText>
            </FormControl>
          </Stack>
          <Box
            component="fieldset"
            alignItems={"top"}
            sx={{ ml: 1, mr: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
          >
            <legend>
              <Typography variant="subtitle1">
                <Checkbox
                  size="small"
                  checked={formik.values.demurrage_flag === "Y" ? true : false}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "demurrage_flag",
                      e.target.checked ? "Y" : "N"
                    );
                  }}
                />
                Demurrage
              </Typography>
            </legend>
            <Stack
              direction={"row"}
              spacing={1}
              sx={{ mb: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
            >
              <FormControl fullWidth>
                <InputLabel size="small">
                  Spot charter demmurage rate
                </InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="spot_demurrage_rate"
                  size="small"
                  label="Spot charter demmurage rate"
                  value={formik.values.spot_demurrage_rate}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Demurrage_rate")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.spot_demurrage_rate &&
                      Boolean(formik.errors.spot_demurrage_rate)
                    }
                  >
                    {formik.touched.spot_demurrage_rate &&
                      formik.errors.spot_demurrage_rate}
                  </FormHelperText>
                ) : null}
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="spot_demurrage_rate_other"
                  size="small"
                  value={formik.values.spot_demurrage_rate_other}
                  onChange={formik.handleChange}
                  label="Spot charter demmurage other"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="spot_demurrage_rate_max"
                  size="small"
                  value={formik.values.spot_demurrage_rate_max}
                  onChange={formik.handleChange}
                  label="Spot charter demmurage max"
                />
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.spot_demurrage_rate_max &&
                      Boolean(formik.errors.spot_demurrage_rate_max)
                    }
                  >
                    {formik.touched.spot_demurrage_rate_max &&
                      formik.errors.spot_demurrage_rate_max}
                  </FormHelperText>
                ) : null}
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ mb: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small">
                  Time charter demmurage rate
                </InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  size="small"
                  id="time_demurrage_rate"
                  name="time_demurrage_rate"
                  label="Time charter demmurage rate"
                  value={formik.values.time_demurrage_rate}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Demurrage_rate")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.time_demurrage_rate &&
                      Boolean(formik.errors.time_demurrage_rate)
                    }
                  >
                    {formik.touched.time_demurrage_rate &&
                      formik.errors.time_demurrage_rate}
                  </FormHelperText>
                ) : null}
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="time_demurrage_rate_other"
                  size="small"
                  value={formik.values.time_demurrage_rate_other}
                  onChange={formik.handleChange}
                  label="Time charter demmurage other"
                />
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="time_demurrage_rate_max"
                  size="small"
                  value={formik.values.time_demurrage_rate_max}
                  onChange={formik.handleChange}
                  label="Time charter demmurage max"
                />
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.time_demurrage_rate_max &&
                      Boolean(formik.errors.time_demurrage_rate_max)
                    }
                  >
                    {formik.touched.time_demurrage_rate_max &&
                      formik.errors.time_demurrage_rate_max}
                  </FormHelperText>
                ) : null}
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ mb: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small">Notification timebar</InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  size="small"
                  name="notification_timebar"
                  label="Notification timebar"
                  value={formik.values.notification_timebar}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Notification_timebar")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.notification_timebar &&
                      Boolean(formik.errors.notification_timebar)
                    }
                  >
                    {formik.touched.notification_timebar &&
                      formik.errors.notification_timebar}
                  </FormHelperText>
                ) : null}
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small">Notification from</InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="notification_from_date"
                  size="small"
                  label="Notification from"
                  value={formik.values.notification_from_date}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Demurrage_rate")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.notification_from_date &&
                      Boolean(formik.errors.notification_from_date)
                    }
                  >
                    {formik.touched.notification_from_date &&
                      formik.errors.notification_from_date}
                  </FormHelperText>
                ) : null}
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="notification_other"
                  size="small"
                  value={formik.values.notification_other}
                  onChange={formik.handleChange}
                  label="Notification other"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ mb: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small">Fully documented timebar</InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  name="fulldoc_timebar"
                  size="small"
                  label="Fully documented timebar"
                  value={formik.values.fulldoc_timebar}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Notification_timebar")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.fulldoc_timebar &&
                      Boolean(formik.errors.fulldoc_timebar)
                    }
                  >
                    {formik.touched.fulldoc_timebar &&
                      formik.errors.fulldoc_timebar}
                  </FormHelperText>
                ) : null}
              </FormControl>
              <FormControl fullWidth>
                <InputLabel size="small">Fully documented from</InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  size="small"
                  name="fulldoc_from_date"
                  label="Fully documented from"
                  value={formik.values.fulldoc_from_date}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Demurrage_rate")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                {formik.values.demurrage_flag === "Y" ? (
                  <FormHelperText
                    error={
                      formik.touched.fulldoc_from_date &&
                      Boolean(formik.errors.fulldoc_from_date)
                    }
                  >
                    {formik.touched.fulldoc_from_date &&
                      formik.errors.fulldoc_from_date}
                  </FormHelperText>
                ) : null}
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  size="small"
                  name="fulldoc_other"
                  value={formik.values.fulldoc_other}
                  onChange={formik.handleChange}
                  label="Fully documented other"
                />
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1} sx={{ mb: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small">PDC to Apply</InputLabel>
                <Select
                  disabled={formik.values.demurrage_flag === "Y" ? false : true}
                  size="small"
                  name="pdc_to_apply"
                  label="PDC to Apply"
                  value={formik.values.pdc_to_apply}
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Notification_timebar")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
              </FormControl>
              <FormControl fullWidth></FormControl>
              <FormControl fullWidth></FormControl>
            </Stack>
          </Box>
          <Divider sx={{ m: 1 }} />
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              m: 1,
            }}
          >
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
              .permission === "Edit" ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
          </Box>
        </Card>
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
