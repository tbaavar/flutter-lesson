"use client";
import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";

import {
  Box,
  Button,
  Stack,
  TextField,
  CircularProgress,
  MenuItem,
  IconButton,
  FormControl,
  Select,
  InputLabel,
  Link,
} from "@mui/material";
import AddSharpIcon from "@mui/icons-material/AddSharp";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import useSWR from "swr";
import { useRouter } from "next/navigation";
import { Clear as ClearIcon } from "@mui/icons-material";
import { numberWithCommas } from "@/lib/numberWithCommas";
import CustomNoRowsOverlay from "@/components/NoRecord";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { useTradeFilter } from "@/app/hooks/tradeSearchFilter";
import exportToExcel from "@/components/ExcelExport";

function FilteredComp() {
  const router = useRouter();
  const { data: session } = useSession();
  const { searchFilter, setSearchFilter } = useTradeFilter();

  const [value, setValue] = React.useState(0);
  // const [tradeid, setTradeid] = React.useState(0);
  const [filterText, setFilterText] = React.useState("");
  // const [loading, setLoading] = React.useState(false);
  const [selectedTradeId, setSelectedTradeId] = React.useState("");

  /////////////////////////////////////////////////////////////////////////////////

  const [startDate, setStartDate] = useState(
    dayjs(new Date(new Date().getFullYear(), 0, 1))
  );
  const [endDate, setEndDate] = useState(dayjs());
  // const [tradetype, setTradetype] = useState(null);
  // const [tradeStatuses, setTradeStatuses] = useState(null);
  // const [trader, setTrader] = useState(null);
  // const [counterParty, setCounterParty] = useState(null);
  // const [product1, setProduct1] = useState(null);
  1;
  const handleChangeFilter = (e) => {
    setSearchFilter((prev) => {
      return {
        ...prev,
        [e.target.name]: e.target.value,
      };
    });
  };
  const handleClearIT = () => {
    // setTradetype(null);
    setSearchFilter((prev) => ({ ...prev, trade_type: "" }));
  };
  const handleClearStatus = () => {
    // setTradeStatuses(null);
    setSearchFilter((prev) => ({ ...prev, trade_status: "" }));
  };

  const handleClearPS = () => {
    // setTrader(null);
    setSearchFilter((prev) => ({ ...prev, trader_user_id: "" }));
  };

  const handleClearCP = () => {
    // setCounterParty(null);
    setSearchFilter((prev) => ({ ...prev, counter_party: "" }));
  };

  const handleClearPSS = () => {
    // setProduct1(null);
    setSearchFilter((prev) => ({ ...prev, product_id: "" }));
  };

  /////////////////////////////////////////////////////////////////////////////////

  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };
  // const handleChange = (event, newValue) => {
  //   setValue(newValue);
  // };

  const { data: counterparty } = useSWR(
    [`/api/legal`, session?.user?.token],
    fetcher
  );

  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );

  const { data: trader_user_id } = useSWR(
    [`/api/user/Trader`, session?.user?.token],
    fetcher
  );

  const { data: product_id } = useSWR(
    [`/api/lookup/product_grade`, session?.user?.token],
    fetcher
  );

  const {
    data: trades,
    error,
    isLoading,
  } = useSWR(
    [
      `/api/trade/list/${session?.user?.user_id}/${startDate.format(
        "YYYY-MM-DD"
      )}/${endDate.format("YYYY-MM-DD")}`,
      session?.user?.token,
    ],
    fetcher
  );
  console.log("dwada", trades);
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  const handleCellClick = (params, event) => {
    setSelectedTradeId(params.row.trade_id);
  };

  const column = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 10,
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 110,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Link href={`/trade/detail/${params.row.trade_id}`} variant="h7">
            {"T-" + params.row.trade_id}
          </Link>
        </Box>
      ),
      headerAlign: "left",
    },
    {
      field: "company_name",
      renderHeader: () => <strong>{"Company"}</strong>,
      width: 100,
      headerAlign: "left",
      width: 140,
    },
    {
      field: "country_party_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.country_party_name}</Typography>
          {params.row.trade_type === "Spot_Purchase" ||
          params.row.trade_type === "Term_Purchase" ? (
            <Typography variant="h7" color={"#34A853"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          ) : (
            <Typography variant="h7" color={"#4285F4"}>
              <strong>{params.row.trade_type_name}</strong>
            </Typography>
          )}
        </Box>
      ),
      width: 170,
      headerAlign: "left",
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">{params.row.product_name}</Typography>

          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.grade_name} </strong>
          </Typography>
        </Box>
      ),
      width: 190,
      headerAlign: "left",
    },
    {
      field: "quantity_mt",
      renderHeader: () => <strong>{"Quantity(MT)"}</strong>,
      renderCell: (params) => {
        if (params.row.quantity !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                <strong> {numberWithCommas(params.row.quantity)}</strong>
              </Typography>
            </Box>
          );
        } else return null;
      },
      width: 120,
      headerAlign: "left",
      align: "right",
    },
    {
      field: "quantity_cm",
      renderHeader: () => <strong>{"Quantity(CM)"}</strong>,
      renderCell: (params) => {
        if (params.row.quantity_cm !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                <strong> {numberWithCommas(params.row.quantity_cm)}</strong>
              </Typography>
            </Box>
          );
        } else return null;
      },
      width: 120,
      headerAlign: "left",
      align: "right",
    },
    {
      field: "unit",
      renderHeader: () => <strong>{"Unit"}</strong>,
      renderCell: (params) => {
        return (
          <Box sx={{ flexDirection: "column", display: "flex" }}>
            <Typography variant="h7">{params.row.unit_name}</Typography>
          </Box>
        );
      },
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 80,

      headerAlign: "left",
    },
    {
      field: "date_range_from",
      renderHeader: () => <strong>{"Operation date"}</strong>,
      width: 150,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.date_range_from
              ? "FROM: " +
                dayjs(params.row.date_range_from).format("YYYY-MM-DD")
              : "FROM: "}
          </Typography>
          <Typography variant="h7">
            {params.row.date_range_to
              ? "TO: " + dayjs(params.row.date_range_to).format("YYYY-MM-DD")
              : "TO: "}
          </Typography>
          <Typography variant="h7">
            {params.row.est_bl_date
              ? "BL: " + dayjs(params.row.est_bl_date).format("YYYY-MM-DD")
              : "BL: "}
          </Typography>
        </Box>
      ),
      headerAlign: "left",
    },
    {
      field: "trader_name",
      renderHeader: () => <strong>{"Trader"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "operation_flag",
      renderHeader: () => <strong>{"Operation"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.operator_name ? params.row.operator_name : "***"}
          </Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.operation_flag} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "finance_name",
      renderHeader: () => <strong>{"Finance"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.finance_name ? params.row.finance_name : "***"}
          </Typography>
          <Typography sx={{ fontSize: "12px" }}>
            <strong> {params.row.finance_flag} </strong>
          </Typography>
        </Box>
      ),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 170,
      headerAlign: "left",
    },
  ];
  const handleStartDateChange = (newValue) => {
    setStartDate(newValue);
  };

  const handleEndDateChange = (newValue) => {
    setEndDate(newValue);
  };

  const rows = trades?.result.map((row, i) => ({
    id: i + 1,
    trade_id: row.trade_id,
    deal_id: row.deal_id,
    company_name: row.company_name,
    counterparty_id: row.counterparty_id,
    country_party_name: row.country_party_name,
    trade_type: row.trade_type,
    trade_type_name: row.trade_type_name,
    trade_date: row.trade_date,
    product_id: row.product_id,
    product_name: row.product_name,
    grade_id: row.grade_id,
    trader_user_id: row.trader_user_id,
    grade_name: row.grade_name,
    quantity: row.quantity,
    quantity_cm: row.quantity_cm,
    units: row.units,
    date_range_from: row.date_range_from,
    date_range_to: row.date_range_to,
    trader_name: row.trader_name,
    trade_status: row.trade_status,
    operation_flag: row.operation_flag,
    operator_name: row.operator_name,
    finance_name: row.finance_name,
    finance_flag: row.finance_flag,
    operator_sign_off_flag: row.operator_sign_off_flag,
    finance_sign_off_flag: row.finance_sign_off_flag,
    unit_name: row.unit_name,
    created_by: row.created_by,
  }));

  const filteredRows = rows.filter(
    (row) =>
      (searchFilter.trade_type === "" ||
        row.trade_type === searchFilter.trade_type) &&
      (searchFilter.trader_user_id === "" ||
        row.trader_user_id === searchFilter.trader_user_id) &&
      (searchFilter.counter_party === "" ||
        row.counterparty_id === searchFilter.counter_party) &&
      (searchFilter.product_id === "" ||
        row.product_id === searchFilter.product_id) &&
      (searchFilter.trade_status === "" ||
        row.trade_status === searchFilter.trade_status) &&
      Object.values(row).some(
        (value) =>
          (typeof value === "string" &&
            value.toLowerCase().includes(filterText.toLowerCase())) ||
          (typeof value === "number" &&
            value.toString().includes(filterText.toLowerCase()))
      )
  );

  const handleExportExcel = () => {
    const trade = [
      {
        header: "id",
      },
      {
        header: "trade_id",
      },
      {
        header: "company_name",
      },
      {
        header: "country_party_name",
      },
      {
        header: "trade_type_name",
      },
      {
        header: "product_name",
      },
      {
        header: "grade_name",
      },
      {
        header: "quantity",
      },
      {
        header: "units",
      },
      {
        header: "trade_status",
      },
      {
        header: "date_range_from",
      },
      {
        header: "date_range_to",
      },
      {
        header: "est_bl_date",
      },
      {
        header: "trader_name",
      },
      {
        header: "operation_flag",
      },
      {
        header: "operator_name",
      },
      {
        header: "finance_name",
      },
      {
        header: "finance_flag",
      },
      {
        header: "created_by",
      },
    ];
    const rows = trades?.result.map((row, i) => ({
      id: i + 1,
      trade_id: row.trade_id,
      company_name: row.company_name,
      country_party_name: row.country_party_name,
      trade_type_name: row.trade_type_name,
      product_name: row.product_name,
      grade_name: row.grade_name,
      quantity: row.quantity,
      units: row.units,
      trade_status: row.trade_status,
      date_range_from: row.date_range_from,
      date_range_to: row.date_range_to,
      est_bl_date: row.est_bl_date,
      trader_name: row.trader_name,
      operation_flag: row.operation_flag,
      operator_name: row.operator_name,
      finance_name: row.finance_name,
      finance_flag: row.finance_flag,
      created_by: row.created_by,
    }));

    exportToExcel(
      rows,
      trade,
      `TradeReport${dayjs(new Date()).format("YYYYMMDD")}`
    );
  };
  /////////////////////////////////////////////////////////////////////////////////

  const handleNewClick = () => {
    router.push("/trade/detail/new");
  };

  return (
    <>
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <Box sx={{ maxWidth: "90vw" }}>
          <ToastContainer />

          <Stack
            sx={{ mb: 2 }}
            direction={"row"}
            spacing={1}
            justifyContent="space-between"
            alignItems={"center"}
          >
            <FormControl fullWidth>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
              >
                <DatePicker
                  label="Start date"
                  format="YYYY-MM-DD"
                  value={startDate}
                  onChange={handleStartDateChange}
                  slotProps={{ textField: { size: "small" } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl fullWidth>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
              >
                <DatePicker
                  label="End date"
                  format="YYYY-MM-DD"
                  value={endDate}
                  onChange={handleEndDateChange}
                  slotProps={{ textField: { size: "small" } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small">Counterparty</InputLabel>

              <Select
                size="small"
                labelId="counter_party"
                margin="dense"
                label="Counterparty"
                name="counter_party"
                onChange={handleChangeFilter}
                value={searchFilter.counter_party || ""}
              >
                {counterparty?.result.length > 0 &&
                  counterparty?.result.map((item, i) => (
                    <MenuItem
                      value={item.legal_entity_id}
                      key={item.legal_entity_id}
                    >
                      {`${item.name}`}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
            <FormControl>
              {searchFilter.counter_party && (
                <IconButton
                  aria-label="clear counterParty selection"
                  onClick={handleClearCP}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small">Trade type</InputLabel>

              <Select
                size="small"
                labelId="trade_type"
                margin="dense"
                label="Trade type"
                name="trade_type"
                onChange={handleChangeFilter}
                value={searchFilter.trade_type || ""}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Trade_type")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
            </FormControl>
            <FormControl>
              {searchFilter.trade_type && (
                <IconButton
                  aria-label="clear Trade type"
                  onClick={handleClearIT}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small">Trader</InputLabel>

              <Select
                size="small"
                labelId="trader_user_id"
                margin="dense"
                label="Trader"
                name="trader_user_id"
                onChange={handleChangeFilter}
                value={searchFilter.trader_user_id || ""}
              >
                {trader_user_id?.result.length > 0 &&
                  trader_user_id?.result.map((item, i) => (
                    <MenuItem value={item.user_id} key={item.user_id}>
                      {item.firstname + " " + item.lastname}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
            <FormControl>
              {searchFilter.trader_user_id && (
                <IconButton
                  aria-label="clear Trader"
                  onClick={handleClearPS}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small">Product</InputLabel>

              <Select
                size="small"
                labelId="product_id"
                margin="dense"
                label="Product"
                name="product_id"
                onChange={handleChangeFilter}
                value={searchFilter.product_id || ""}
              >
                {product_id?.result.length > 0 &&
                  product_id?.result.map((item, i) => (
                    <MenuItem value={item.product_id} key={item.product_id}>
                      {`${item.product_name}`}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
            <FormControl>
              {searchFilter.product_id && (
                <IconButton
                  aria-label="clear Product"
                  onClick={handleClearPSS}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </FormControl>
            <FormControl fullWidth>
              <InputLabel size="small">Trade Status</InputLabel>

              <Select
                size="small"
                labelId="trade_status"
                margin="dense"
                label="Trade Status"
                name="trade_status"
                onChange={handleChangeFilter}
                value={searchFilter.trade_status || ""}
              >
                {lookup?.result.length >= 0 &&
                  lookup?.result
                    .filter((f) => f.lookup_type === "Trade_status")
                    .map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
              </Select>
            </FormControl>
            <FormControl>
              {searchFilter.trade_status && (
                <IconButton
                  aria-label="clear Trade status"
                  onClick={handleClearStatus}
                  size="small"
                >
                  <ClearIcon />
                </IconButton>
              )}
            </FormControl>
          </Stack>

          <Stack
            sx={{ mb: 1 }}
            direction={"row"}
            spacing={1}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Box>
              <TextField
                variant="outlined"
                size="small"
                placeholder="Search..."
                value={filterText}
                onChange={handleFilterChange}
                sx={{ minWidth: "300px" }}
              />
            </Box>
            <Box>
              {session &&
              session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
                .permission === "Edit" ? (
                <>
                  <Button
                    variant="text"
                    size="small"
                    color="dark"
                    onClick={handleNewClick}
                    startIcon={<AddSharpIcon />}
                    sx={{ justifyContent: "flex-end" }}
                  >
                    Trade
                  </Button>
                </>
              ) : null}
              <Button
                variant="outlined"
                size="small"
                sx={{ ml: 1, maxWidth: 205 }}
                onClick={handleExportExcel}
              >
                Export
              </Button>
            </Box>
          </Stack>

          <DataGrid
            onRowClick={handleCellClick}
            rows={filteredRows}
            columns={column}
            slots={{
              noRowsOverlay: CustomNoRowsOverlay,
            }}
            style={{ background: "white" }}
            sx={{
              maxWidth: "100vw",

              backgroundColor: "#FFFFFF",
              "& .MuiDataGrid-cell:hover": {
                color: "primary.main",
              },
              "& .MuiDataGrid-cell": {
                borderTop: 1,
                borderTopColor: "#E9ECF0",
              },
            }}
            getRowHeight={() => "auto"}
            pageSizeOptions={[20]}
          />
        </Box>
      </Box>
    </>
  );
}
function TradePage() {
  return <FilteredComp />;
}
export default TradePage;
