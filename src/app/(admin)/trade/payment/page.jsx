"use client";
import * as React from "react";
import {
  Card,
  CircularProgress,
  FormControlLabel,
  FormHelperText,
  FormControl,
  Checkbox,
  Select,
  MenuItem,
  Radio,
  TextField,
  InputLabel,
  Box,
  Button,
  RadioGroup,
  Divider,
  Stack,
  Modal,
} from "@mui/material";
import useSWR from "swr";
import * as yup from "yup";
import { useFormik } from "formik";
import { fetchData } from "@/lib/fetch";
import { toast } from "react-toastify";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs";
import { useSession } from "next-auth/react";
import MyAlert from "@/components/Myalert";
import { fetcher } from "@/lib/fetcher";

export default function Payment({ trade_id }) {
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const { data: session } = useSession();
  const { data: lookup } = useSWR(
    ["/api/lookup", session?.user?.token],
    fetcher
  );

  const {
    data: general,
    error,
    isLoading,
    mutate,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/payment/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const formik = useFormik({
    initialValues: {
      prepaid_flag: general?.result?.prepaid_flag ?? "N",
      credit_type: general?.result?.credit_type ?? "",
      settlement_currency: general?.result?.settlement_currency ?? "USD ",
      risk_mngt_approved_flag: general?.result?.risk_mngt_approved_flag ?? "N",
      not_agreed_credit_given_flag:
        general?.result?.not_agreed_credit_given_flag ?? "day1",
      ger_excluded_flag: general?.result?.ger_excluded_flag ?? "N",
      day_1_flag: general?.result?.day_1_flag ?? "N",
      day_1: general?.result?.day_1 ?? "",
      time_condition_1: general?.result?.time_condition_1 ?? "",
      condition_1: general?.result?.condition_1 ?? "",
      condition_other_1: general?.result?.condition_other_1 ?? "",
      intended_vessel: general?.result?.intended_vessel ?? "",
      intended_vessel_whichever:
        general?.result?.intended_vessel_whichever ?? "CM",
      fixed_date: general?.result?.fixed_date ?? dayjs().format("YYYY-MM-DD"),
      day_count_method: general?.result?.day_count_method ?? "Work",
      same_as_pricing_flag: general?.result?.same_as_pricing_flag ?? "N",
      if_saturday_calc_method: general?.result?.if_saturday_calc_method ?? "",
      if_sunday_calc_method: general?.result?.if_sunday_calc_method ?? "",
      if_public_holiday_mon_calc_meth:
        general?.result?.if_public_holiday_mon_calc_meth ?? "",
      if_public_holiday_other_calc_me:
        general?.result?.if_public_holiday_other_calc_me ?? "",
      interest_rate_type: general?.result?.interest_rate_type ?? "",
      margin: general?.result?.margin ?? "",
      period: general?.result?.period ?? "",
      to_be_invoiced_flag: general?.result?.to_be_invoiced_flag ?? "N",
      payment_documentation: general?.result?.payment_documentation ?? "",
      payment_note: general?.result?.payment_note ?? "",
    },
    validationSchema: yup.object({
      credit_type: yup.string().required("Please select credit type!"),
      settlement_currency: yup
        .string()
        .required("Please select settlement currency!"),
      day_1: yup.string().required("Please insert days!"),
      time_condition_1: yup.string().required("Please select when!"),
      condition_1: yup.string().required("Please select condition!"),
      intended_vessel: yup.string().required("Please select transportation!"),
    }),
    onSubmit: (values) => {
      savePaymentData(values);
    },

    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  const savePaymentData = async (data) => {



    const body = {
      trade_id: trade_id,
      prepaid_flag: data.prepaid_flag,
      credit_type: data.credit_type,
      settlement_currency: data.settlement_currency,
      risk_mngt_approved_flag: data.risk_mngt_approved_flag,
      not_agreed_credit_given_flag: data.not_agreed_credit_given_flag,
      ger_excluded_flag: data.ger_excluded_flag,
      day_1: String(data.day_1),
      time_condition_1: data.time_condition_1,
      condition_1: data.condition_1,
      condition_other_1: data.condition_other_1,
      day_2: "",
      time_condition_2: "",
      condition_2: "",
      condition_other_2: "",
      intended_vessel: data.intended_vessel,
      intended_vessel_whichever: data.intended_vessel_whichever,
      fixed_date: data.fixed_date,
      day_count_method: data.day_count_method,
      prefered_pricing_parcels: "",
      same_as_pricing_flag: data.same_as_pricing_flag,
      if_saturday_calc_method: data.if_saturday_calc_method,
      if_sunday_calc_method: data.if_sunday_calc_method,
      if_public_holiday_mon_calc_meth: data.if_public_holiday_mon_calc_meth,
      if_public_holiday_other_calc_me: data.if_public_holiday_other_calc_me,
      interest_rate_type: data.interest_rate_type,
      margin: data.margin,
      period: data.period,
      bank_id: "",
      credit_spread_rate: "",
      to_be_invoiced_flag: data.to_be_invoiced_flag,
      payment_documentation: data.payment_documentation,
      payment_note: data.payment_note,
      trade_payment_id: general.result
        ? general.result.trade_payment_id
        : undefined,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const method = general.result ? "PUT" : "POST";

      const res = await fetchData(
        `/api/trade/payment`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        mutate();
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <Card>
          <Stack direction={"column"} spacing={1} sx={{ m: 1 }}>
            <Box
              component="fieldset"
              alignItems={"top"}
              sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
            >
              <legend>Credit</legend>
              <Stack direction={"row"} spacing={1}>
                <FormControl fullWidth sx={{ maxWidth: 180 }}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        size="small"
                        name="prepaid_flag"
                        checked={
                          formik.values.prepaid_flag === "Y" ? true : false
                        }
                        onChange={(e) => {
                          formik.setFieldValue(
                            "prepaid_flag",
                            e.target.checked ? "Y" : "N"
                          );
                        }}
                      />
                    }
                    label="Prepaid (fully or partly)"
                  />
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Credit Type
                  </InputLabel>
                  <Select
                    value={formik.values.credit_type}
                    onBlur={formik.handleBlur}
                    name="credit_type"
                    size="small"
                    label="Credit Type"
                    onChange={formik.handleChange}
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Credit_Type")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.credit_type &&
                      Boolean(formik.errors.credit_type)
                    }
                  >
                    {formik.touched.credit_type && formik.errors.credit_type}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Settlement currency
                  </InputLabel>
                  <Select
                    value={formik.values.settlement_currency}
                    name="settlement_currency"
                    size="small"
                    label="Settlement currency"
                    onChange={formik.handleChange}
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Currency")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.settlement_currency &&
                      Boolean(formik.errors.settlement_currency)
                    }
                  >
                    {formik.touched.settlement_currency &&
                      formik.errors.settlement_currency}
                  </FormHelperText>
                </FormControl>
              </Stack>
            </Box>
            <Box
              component="fieldset"
              alignItems={"top"}
              sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
            >
              <legend>Payment Dates</legend>
              <Stack direction={"row"} spacing={1} sx={{ mb: 1 }}>
                <FormControl fullWidth>
                  <TextField
                    id="day_1"
                    name="day_1"
                    size="small"
                    type="number"
                    InputLabelProps={{
                      style: { color: "#0B626B" },
                    }}
                    value={formik.values.day_1}
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    label="Days"
                  />

                  <FormHelperText
                    error={formik.touched.day_1 && Boolean(formik.errors.day_1)}
                  >
                    {formik.touched.day_1 && formik.errors.day_1}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    When
                  </InputLabel>
                  <Select
                    value={formik.values.time_condition_1}
                    onBlur={formik.handleBlur}
                    name="time_condition_1"
                    label="When"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Time_Condition")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.time_condition_1 &&
                      Boolean(formik.errors.time_condition_1)
                    }
                  >
                    {formik.touched.time_condition_1 &&
                      formik.errors.time_condition_1}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Condition
                  </InputLabel>
                  <Select
                    value={formik.values.condition_1}
                    name="condition_1"
                    label="Condition"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Delivery_date_type")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.condition_1 &&
                      Boolean(formik.errors.condition_1)
                    }
                  >
                    {formik.touched.condition_1 &&
                      formik.errors.condition_1}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    id="condition_other_1"
                    name="condition_other_1"
                    size="small"
                    value={formik.values.condition_other_1}
                    onChange={formik.handleChange}
                    label="Other"
                  />
                </FormControl>
              </Stack>
              <Stack direction={"row"} spacing={1}>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Transportation
                  </InputLabel>
                  <Select
                    value={formik.values.intended_vessel}
                    name="intended_vessel"
                    label="Transportation"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Intended_Vessel")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.intended_vessel &&
                      Boolean(formik.errors.intended_vessel)
                    }
                  >
                    {formik.touched.intended_vessel &&
                      formik.errors.intended_vessel}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Transportation whichever
                  </InputLabel>
                  <Select
                    value={formik.values.intended_vessel_whichever}
                    name="intended_vessel_whichever"
                    onChange={formik.handleChange}
                    size="small"
                    label="Transportation whichever"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Unit")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
                <FormControl fullWidth>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      disabled={formik.values.condition_1 !== "FD"}
                      format="YYYY-MM-DD"
                      value={dayjs(formik.values.fixed_date)}
                      name="fixed_date"
                      slotProps={{ textField: { size: "small" } }}
                      label="Fixed date"
                      onChange={(newValue) => {
                        formik.setFieldValue(
                          "fixed_date",
                          dayjs(newValue).format("YYYY-MM-DD")
                        );
                      }}
                    />
                  </LocalizationProvider>
                </FormControl>
                <FormControl fullWidth>
                  <RadioGroup
                    row
                    defaultValue="work"
                    name="day_count_method"
                    value={formik.values.day_count_method}
                    onChange={formik.handleChange}
                  >
                    <FormControlLabel
                      control={<Radio />}
                      value="work"
                      label="Working Days"
                      labelPlacement="end"
                    />

                    <FormControlLabel
                      control={<Radio />}
                      value="calendar"
                      label="Calendar days"
                      labelPlacement="end"
                    />
                  </RadioGroup>
                </FormControl>
              </Stack>
            </Box>
            <Box
              component="fieldset"
              alignItems={"top"}
              sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
            >
              <legend>Treatment of Payment Due Date</legend>
              <Stack direction={"row"} spacing={1}>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    if Saturday
                  </InputLabel>
                  <Select
                    value={formik.values.if_saturday_calc_method}
                    onBlur={formik.handleBlur}
                    name="if_saturday_calc_method"
                    label="if Saturday"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Day_Calc_Method")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.if_saturday_calc_method &&
                      Boolean(formik.errors.if_saturday_calc_method)
                    }
                  >
                    {formik.touched.if_saturday_calc_method &&
                      formik.errors.if_saturday_calc_method}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    If Sunday
                  </InputLabel>
                  <Select
                    value={formik.values.if_sunday_calc_method}
                    onBlur={formik.handleBlur}
                    label="If Sunday"
                    name="if_sunday_calc_method"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Day_Calc_Method")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.if_sunday_calc_method &&
                      Boolean(formik.errors.if_sunday_calc_method)
                    }
                  >
                    {formik.touched.if_sunday_calc_method &&
                      formik.errors.if_sunday_calc_method}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    If public holiday Monday
                  </InputLabel>
                  <Select
                    value={formik.values.if_public_holiday_mon_calc_meth}
                    onBlur={formik.handleBlur}
                    name="if_public_holiday_mon_calc_meth"
                    onChange={formik.handleChange}
                    label="If public holiday Monday"
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Day_Calc_Method")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.if_public_holiday_mon_calc_meth &&
                      Boolean(formik.errors.if_public_holiday_mon_calc_meth)
                    }
                  >
                    {formik.touched.if_public_holiday_mon_calc_meth &&
                      formik.errors.if_public_holiday_mon_calc_meth}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    If public holiday other
                  </InputLabel>
                  <Select
                    value={formik.values.if_public_holiday_other_calc_me}
                    onBlur={formik.handleBlur}
                    name="if_public_holiday_other_calc_me"
                    label="If public holiday other"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Day_Calc_Method")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.if_public_holiday_other_calc_me &&
                      Boolean(formik.errors.if_public_holiday_other_calc_me)
                    }
                  >
                    {formik.touched.if_public_holiday_other_calc_me &&
                      formik.errors.if_public_holiday_other_calc_me}
                  </FormHelperText>
                </FormControl>
              </Stack>
            </Box>
            <Box
              component="fieldset"
              alignItems={"top"}
              sx={{ m: 1, borderRadius: 1, borderColor: "#e6e6e6" }}
            >
              <legend>Late Payment Interest</legend>
              <Stack direction={"row"} spacing={1}>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Rate
                  </InputLabel>
                  <Select
                    value={formik.values.interest_rate_type}
                    onBlur={formik.handleBlur}
                    name="interest_rate_type"
                    label="Rate"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Interest_Rate_Type")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.interest_rate_type &&
                      Boolean(formik.errors.interest_rate_type)
                    }
                  >
                    {formik.touched.interest_rate_type &&
                      formik.errors.interest_rate_type}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <TextField
                    id="margin"
                    name="margin"
                    size="small"
                    type="number"
                    InputLabelProps={{
                      style: { color: "#0B626B" },
                    }}
                    label="Margin(%)"
                    value={parseFloat(formik.values.margin)}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText
                    error={
                      formik.touched.margin && Boolean(formik.errors.margin)
                    }
                  >
                    {formik.touched.margin && formik.errors.margin}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel size="small" sx={{ color: "#0B626B" }}>
                    Period
                  </InputLabel>
                  <Select
                    value={formik.values.period}
                    onBlur={formik.handleBlur}
                    name="period"
                    label="Period"
                    onChange={formik.handleChange}
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Period")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.period && Boolean(formik.errors.period)
                    }
                  >
                    {formik.touched.period && formik.errors.period}
                  </FormHelperText>
                </FormControl>
                <FormControl fullWidth></FormControl>
              </Stack>
            </Box>
            <Stack direction={"column"} spacing={1} sx={{ m: 1 }}>
              <FormControl fullWidth>
                <InputLabel size="small" sx={{ color: "#0B626B" }}>
                  Payment documentation
                </InputLabel>
                <Select
                  value={formik.values.payment_documentation}
                  onBlur={formik.handleBlur}
                  label="Payment documentation"
                  size="small"
                  name="payment_documentation"
                  onChange={formik.handleChange}
                >
                  {lookup?.result.length >= 0 &&
                    lookup?.result
                      .filter((f) => f.lookup_type === "Payment_documentation")
                      .map((item) => (
                        <MenuItem
                          value={item.lookup_code}
                          key={item.lookup_code}
                        >
                          {item.name}
                        </MenuItem>
                      ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.payment_documentation &&
                    Boolean(formik.errors.payment_documentation)
                  }
                >
                  {formik.touched.payment_documentation &&
                    formik.errors.payment_documentation}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  id="payment_note"
                  label="Payment Notes"
                  name="payment_note"
                  rows={20}
                  value={formik.values.payment_note}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Stack>
          </Stack>
          <Divider sx={{ m: 1 }} />
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              m: 1,
            }}
          >
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
              .permission === "Edit" ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
          </Box>
        </Card>
      </form>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
