"use client";
import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import useSWR from "swr";
import { Box, Card, CircularProgress, Typography } from "@mui/material";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";
import { numberWithCommas } from "@/lib/numberWithCommas";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#3b8188",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const rows = [
  { field: "trade_id", name: "Trade ID" },
  { field: "trade_type", name: "Trade type" },
  { field: "trade_date", name: "Trade date" },
  { field: "counter_party", name: "Counterparty" },
  { field: "group_co", name: "Group Company" },
  { field: "terms", name: "Incoterm" },
  { field: "trade_origin", name: "Country of origin" },
  { field: "trader_name", name: "Trader" },
  { field: "contract_name", name: "Contract admin" },
  { field: "operator_name", name: "Operator" },
  { field: "finance_name", name: "Financer" },
  { field: "product_name", name: "Product" },
  { field: "grade_name", name: "Grade" },
  { field: "quantity", name: "Quantity" },
  { field: "pricing_type", name: "Pricing type" },
  { field: "est_price", name: "Est price" },
  { field: "est_amount", name: "Est amount" },
  { field: "quantity_to_base_pricing", name: "Quantity to Base Pricing On" },
  { field: "payable_in", name: "Payable in" },
  { field: "pricing_dates", name: "Pricing dates" },
  { field: "load_port", name: "Load port" },
  { field: "dischage_port", name: "Discharge port" },
  { field: "vessel", name: "Transportation type" },
  { field: "laytime", name: "Laytime" },
  { field: "generaltc", name: "General term condition" },
  { field: "dates_based_on", name: "Dates Based On" },
  { field: "date_range", name: "Date Range" },
  { field: "est_bl_date", name: "Est B/L" },
  { field: "payment", name: "Payment" },
  { field: "intention", name: "Intention" },
  { field: "memo", name: "Memo" },
];

export default function SummaryPage({ trade_id }) {
  const { data: session } = useSession();
  const {
    data: summary,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/summary/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  return (
    <Card sx={{ p: 1 }}>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 150 }} size="small" aria-label="summary">
          <TableHead>
            <TableRow>
              <StyledTableCell align="left" width={"150px"}>
                FIELD
              </StyledTableCell>
              <StyledTableCell align="left" width={"300px"}>
                CURRENT VALUE
              </StyledTableCell>
              <StyledTableCell align="left">PREVIOUS VALUE</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              const formatValue = (value) =>
                row.field === "est_price" || row.field === "est_amount"
                  ? `${numberWithCommas(value)} MNT`
                  : value;

              const currentVal = formatValue(summary?.result[0][row.field]);
              const prevVal = formatValue(summary?.result[1][row.field]);

              return (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ backgroundColor: "#e1e5e9" }}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{ borderRight: "1px solid grey" }}
                  >
                    <Typography
                      variant="body1"
                      color={
                        summary?.result[1]
                          ? currentVal !== prevVal
                            ? "#E21D12"
                            : null
                          : null
                      }
                    >
                      {trade_id !== "new" ? currentVal : null}
                    </Typography>
                  </TableCell>
                  <TableCell align="left">
                    <Typography variant="body1">{prevVal}</Typography>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Card>
  );
}