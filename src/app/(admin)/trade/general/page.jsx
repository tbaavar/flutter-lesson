"use client";
import React from "react";
import {
  MenuItem,
  Card,
  Button,
  Typography,
  Box,
  TextField,
  CircularProgress,
  FormHelperText,
  Stack,
  Select,
  FormControlLabel,
  Checkbox,
  InputLabel,
  FormControl,
  Modal,
} from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import useSWR from "swr";
import dayjs from "dayjs";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import MyAlert from "@/components/Myalert";

const General = ({ trade_id }) => {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const router = useRouter();
  const [open, setOpen] = React.useState(false);

  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );
  const { data: org } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );


  const {
    data: general,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );
  const formik = useFormik({
    initialValues: {
      trade_id: general?.result?.trade_id ?? "New",
      from_pluto_flag: general?.result?.from_pluto_flag ?? "N",
      deal_id: general?.result?.deal_id ?? "",
      company_id: general?.result?.company_id ?? "",
      counterparty_id: general?.result?.counterparty_id ?? "",
      trade_type: general?.result?.trade_type ?? "",
      trade_date: general?.result?.trade_date ?? dayjs().format("YYYY-MM-DD"),
      contract_type: general?.result?.contract_type ?? "",
      trade_origin: general?.result?.trade_origin ?? "",
      delivery_method: general?.result?.delivery_method ?? "",
      terms: general?.result?.terms ?? "",
      trade_status: general?.result?.trade_status ?? "New",
      trader_user_id: general?.result?.trader_user_id ?? "",
      operator_user_id: general?.result?.operator_user_id ?? "",
      finance_user_id: general?.result?.finance_user_id ?? "",
      contract_user_id: general?.result?.contract_user_id ?? "",
      marketing_trade_flag: general?.result?.marketing_trade_flag ?? "N",
      cancelled_flag: general?.result?.cancelled_flag ?? "N",
      verified_flag: general?.result?.verified_flag ?? "N",
      operator_sign_off_flag: general?.result?.operator_sign_off_flag ?? "N",
      finance_sign_off_flag: general?.result?.finance_sign_off_flag ?? "N",
    },
    validationSchema: yup.object({
      company_id: yup.string().required("Please select company!"),
      counterparty_id: yup.string().required("Please select counter party!"),
      trade_type: yup.string().required("Please select trade type"),
      trade_date: yup
        .date()
        .required("Огноо сонгоно уу")
        .typeError("Огноо сонгоно уу"),
      trade_origin: yup.string().required("Please select trade origin"),
      terms: yup.string().required("Please select terms"),
      trader_user_id: yup.string().required("Please select trader"),
      operator_user_id: yup.string().required("Please select operator"),
    }),
    onSubmit: async (values) => {
      saveTradeData(values);
    },

    enableReinitialize: true,
  });

  const { data: counterparty_id } = useSWR(
    formik.values.trade_type
      ? [
          `/api/lookup/counterparty/${formik.values.trade_type}/${formik.values.company_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const { data: user } = useSWR(
    formik.values.company_id
      ? [`/api/lookup/user/${formik.values.company_id}`, session?.user?.token]
      : null,
    fetcher
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const saveTradeData = async (data) => {
    const body = {
      deal_id: data.deal_id,
      company_id: data.company_id,
      counterparty_id: data.counterparty_id,
      trade_type: data.trade_type,
      trade_date: data.trade_date,
      contract_type: data.contract_type,
      trade_origin: data.trade_origin,
      delivery_method: data.delivery_method,
      terms: data.terms,
      trade_status: data.trade_status,
      trader_user_id: data.trader_user_id,
      operator_user_id: data.operator_user_id,
      finance_user_id: data.finance_user_id,
      contract_user_id: data.contract_user_id,
      operator_sign_off_flag: data.operator_sign_off_flag,
      finance_sign_off_flag: data.finance_sign_off_flag,
      marketing_trade_flag: data.marketing_trade_flag,
      cancelled_flag: data.cancelled_flag,
      verified_flag: data.verified_flag,
      from_pluto_flag: "Y",
      trade_id: trade_id !== "new" ? trade_id : undefined,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      setOpen(true);
      const method = trade_id === "new" ? "POST" : "PUT";
      const r = await fetch("/api/trade", {
        method: method,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + session?.user?.token,
        },
        body: JSON.stringify(body),
      });

      const res = await r.json();

      if (r.status === 200) {
        toast.success("Амжилттай хадгаллаа.", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });

        if (method === "POST") {
          router.push(`/trade/detail/${res.result.trade_id}`);
        }
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };

  return (
    <>
      <ToastContainer />
      <Card>
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              m: 1,
            }}
          >
            <Typography variant="h5" sx={{ ml: 1 }}>
              General information
            </Typography>
            {session &&
            session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
              .permission === "Edit" ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
          </Box>
          <Box display={"flex"} sx={{ m: 1 }}>
            <Box sx={{ width: "20vw", ml: 1, mt: 1 }}>
              <Stack direction="column" spacing={1}>
                <Stack direction="row" spacing={1}>
                  <FormControl fullWidth>
                    <TextField
                      name="trade_id"
                      id="trade_id"
                      label="Trade ID"
                      onChange={formik.handleChange}
                      disabled
                      size="small"
                      value={formik.values.trade_id}
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <InputLabel sx={{ color: " #0B626B" }} size="small">
                      Status
                    </InputLabel>
                    <Select
                      labelId="trade_status"
                      id="trade_status"
                      name="trade_status"
                      value={formik.values.trade_status}
                      onChange={formik.handleChange}
                      size="small"
                      label="Status"
                    >
                      {lookup?.result.length >= 0 &&
                        lookup?.result
                          .filter((f) => f.lookup_type === "Trade_status")
                          .map((item) => (
                            <MenuItem
                              value={item.lookup_code}
                              key={item.lookup_code}
                            >
                              {item.name}
                            </MenuItem>
                          ))}
                    </Select>
                  </FormControl>
                </Stack>
                <FormControl>
                  <InputLabel sx={{ color: " #0B626B" }} size="small">
                    Company
                  </InputLabel>
                  <Select
                    labelId="company_id"
                    id="company_id"
                    name="company_id"
                    value={formik.values.company_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="Company"
                  >
                    {org?.result.length >= 0 &&
                      org?.result.map((item) => (
                        <MenuItem value={item.org_id} key={item.org_id}>
                          {item.org_name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.company_id &&
                      Boolean(formik.errors.company_id)
                    }
                  >
                    {formik.touched.company_id && formik.errors.company_id}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Trade Type
                  </InputLabel>
                  <Select
                    labelId="trade_type"
                    id="trade_type"
                    name="trade_type"
                    value={formik.values.trade_type}
                    onChange={formik.handleChange}
                    size="small"
                    label="Trade Type"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Trade_type")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.trade_type &&
                      Boolean(formik.errors.trade_type)
                    }
                  >
                    {formik.touched.trade_type && formik.errors.trade_type}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Counter Party
                  </InputLabel>
                  <Select
                    labelId="counterparty_id"
                    id="counterparty_id"
                    name="counterparty_id"
                    defaultValue={""}
                    value={formik.values.counterparty_id || ""}
                    onChange={formik.handleChange}
                    size="small"
                    label="Counter Party"
                  >
                    {counterparty_id?.result &&
                    counterparty_id.result.length > 0 ? (
                      counterparty_id.result.map((item) => (
                        <MenuItem
                          value={item.legal_entity_id}
                          key={item.legal_entity_id}
                        >
                          {item.name}
                        </MenuItem>
                      ))
                    ) : (
                      <MenuItem value="" disabled>
                        No counterparty available
                      </MenuItem>
                    )}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.counterparty_id &&
                      Boolean(formik.errors.counterparty_id)
                    }
                  >
                    {formik.touched.counterparty_id &&
                      formik.errors.counterparty_id}
                  </FormHelperText>
                </FormControl>
              </Stack>
            </Box>
            <Box sx={{ width: "20vw", ml: 1, mt: 1 }}>
              <Stack direction="column" spacing={1}>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Trade Origin
                  </InputLabel>
                  <Select
                    labelId="trade_origin"
                    id="trade_origin"
                    name="trade_origin"
                    value={formik.values.trade_origin}
                    onChange={formik.handleChange}
                    size="small"
                    label="Trade Origin"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Trade_Origin")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.trade_origin &&
                      Boolean(formik.errors.trade_origin)
                    }
                  >
                    {formik.touched.trade_origin && formik.errors.trade_origin}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <InputLabel size="small">Delivery Method</InputLabel>
                  <Select
                    labelId="delivery_method"
                    id="delivery_method"
                    name="delivery_method"
                    value={formik.values.delivery_method}
                    onChange={formik.handleChange}
                    label="Delivery Method"
                    size="small"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Delivery_method")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Terms
                  </InputLabel>
                  <Select
                    labelId="terms"
                    id="terms"
                    name="terms"
                    value={formik.values.terms}
                    onChange={formik.handleChange}
                    size="small"
                    label="Terms"
                  >
                    {lookup?.result.length >= 0 &&
                      lookup?.result
                        .filter((f) => f.lookup_type === "Terms")
                        .map((item) => (
                          <MenuItem
                            value={item.lookup_code}
                            key={item.lookup_code}
                          >
                            {item.lookup_code + "-" + item.name}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={formik.touched.terms && Boolean(formik.errors.terms)}
                  >
                    {formik.touched.terms && formik.errors.terms}
                  </FormHelperText>
                </FormControl>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    name="trade_date"
                    id="trade_date"
                    label="Trade Date"
                    InputLabelProps={{
                      style: {
                        color: " #0B626B",
                      },
                    }}
                    format="YYYY-MM-DD"
                    slotProps={{
                      textField: {
                        size: "small",
                      },
                    }}
                    value={dayjs(formik.values.trade_date)}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "trade_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                  />

                  <FormHelperText
                    error={
                      formik.touched.trade_date &&
                      Boolean(formik.errors.trade_date)
                    }
                  >
                    {formik.touched.trade_date && formik.errors.trade_date}
                  </FormHelperText>
                </LocalizationProvider>
              </Stack>
            </Box>
            <Box
              component="fieldset"
              alignItems={"top"}
              sx={{
                ml: 1.5,
                borderRadius: 1,
                borderColor: "#e6e6e6",
                flexDirection: "column",
                display: "flex",
              }}
            >
              <legend>Sign off</legend>
              <FormControlLabel
                control={<Checkbox size="small" />}
                label="Operator"
                name="operator_sign_off_flag"
                id="operator_sign_off_flag"
                checked={
                  formik.values.operator_sign_off_flag === "Y" ? true : false
                }
                onChange={(e) => {
                  formik.setFieldValue(
                    "operator_sign_off_flag",
                    e.target.checked ? "Y" : "N"
                  );
                }}
              />
              <FormControlLabel
                control={<Checkbox size="small" />}
                label="Finance"
                name="finance_sign_off_flag"
                id="finance_sign_off_flag"
                checked={
                  formik.values.finance_sign_off_flag === "Y" ? true : false
                }
                onChange={(e) => {
                  formik.setFieldValue(
                    "finance_sign_off_flag",
                    e.target.checked ? "Y" : "N"
                  );
                }}
              />
            </Box>
            <Box sx={{ width: "20vw", ml: 1, mt: 1 }}>
              <Stack direction="column" spacing={1}>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Trader
                  </InputLabel>
                  <Select
                    id="trader_user_id"
                    name="trader_user_id"
                    value={formik.values.trader_user_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="Trader"
                  >
                    {user?.result?.length >= 0 &&
                      user?.result
                        .filter((f) => f.role === "Trader")
                        .map((item) => (
                          <MenuItem value={item.user_id} key={item.user_id}>
                            {item.firstname + " " + item.lastname}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.trader_user_id &&
                      Boolean(formik.errors.trader_user_id)
                    }
                  >
                    {formik.touched.trader_user_id &&
                      formik.errors.trader_user_id}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <InputLabel size="small" sx={{ color: " #0B626B" }}>
                    Operator
                  </InputLabel>
                  <Select
                    id="operator_user_id"
                    name="operator_user_id"
                    value={formik.values.operator_user_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="Operator"
                  >
                    {user?.result?.length >= 0 &&
                      user?.result
                        .filter((f) => f.role === "Trader")
                        .map((item) => (
                          <MenuItem value={item.user_id} key={item.user_id}>
                            {item.firstname + " " + item.lastname}
                          </MenuItem>
                        ))}
                  </Select>
                  <FormHelperText
                    error={
                      formik.touched.operator_user_id &&
                      Boolean(formik.errors.operator_user_id)
                    }
                  >
                    {formik.touched.operator_user_id &&
                      formik.errors.operator_user_id}
                  </FormHelperText>
                </FormControl>
                <FormControl>
                  <InputLabel size="small">Financer</InputLabel>
                  <Select
                    id="finance_user_id"
                    name="finance_user_id"
                    value={formik.values.finance_user_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="Financer"
                  >
                    {user?.result?.length >= 0 &&
                      user?.result
                        .filter((f) => f.role === "Financer")
                        .map((item) => (
                          <MenuItem value={item.user_id} key={item.user_id}>
                            {item.firstname + " " + item.lastname}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
                <FormControl>
                  <InputLabel size="small">Contract Admin</InputLabel>
                  <Select
                    id="contract_user_id"
                    name="contract_user_id"
                    value={formik.values.contract_user_id}
                    onChange={formik.handleChange}
                    size="small"
                    label="Contract Admin"
                  >
                    {user?.result?.length >= 0 &&
                      user?.result
                        .filter((f) => f.role === "Trader")
                        .map((item) => (
                          <MenuItem value={item.user_id} key={item.user_id}>
                            {item.firstname + " " + item.lastname}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
              </Stack>
            </Box>
          </Box>
        </form>
      </Card>

      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default General;
