"use client";
import * as React from "react";
import {
  Box,
  CircularProgress,
  Card,
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  Button,
  Select,
  MenuItem,
  TextField,
  FormHelperText,
  Divider,
  Stack,
  Modal,
} from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import useSWR from "swr";
import dayjs from "dayjs";
import { toast } from "react-toastify";
import MyAlert from "@/components/Myalert";
import { fetchData } from "@/lib/fetch";
import * as yup from "yup";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";

const  rounded=(number) =>{ Math.round(number * 100000) / 100000};

export default function ProductPage({ trade_id }) {
  const { data: session } = useSession();
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const { data: product_id } = useSWR(
    [`/api/lookup/product_grade`, session?.user?.token],
    fetcher
  );

  const { data: lookup } = useSWR(
    [`/api/lookup`, session?.user?.token],
    fetcher
  );
  const { data: general_details } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );
  const { data: counterparty_id } = useSWR(
    general_details?.result?.trade_type
      ? [
          `/api/lookup/counterparty/${general_details?.result?.trade_type}/${general_details?.result?.company_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const {
    data: general,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/product/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );
  console.log("pp",general)
  const formik = useFormik({
    initialValues: {
      ops_active_flag: general?.result?.ops_active_flag ?? "N",
      storage_active_flag: general?.result?.storage_active_flag ?? "N",
      finance_active_flag: general?.result?.finance_active_flag ?? "N",
      consolidate_deal_flag: general?.result?.consolidate_deal_flag ?? "N",
      financing_behalf_flag: general?.result?.financing_behalf_flag ?? "N",
      est_bl_date: general?.result?.est_bl_date ?? dayjs().format("YYYY-MM-DD"),
      product_id: general?.result?.product_id ?? "",
      country_origin: general?.result?.country_origin ?? "",
      grade_id: general?.result?.grade_id ?? "",
      units: general?.result?.units ?? "MT",
      est_density: general?.result?.est_density ?? "",
      trade_treatment: general?.result?.trade_treatment ?? "",
      quantityCh: general?.result?.quantityCh ?? "",
      quantity: general?.result?.quantity ?? "",
      quantity_cm: general?.result?.quantity_cm ?? "",
      plus_minus: general?.result?.plus_minus ?? "",
      minQuantityCh: general?.result?.minQuantityCh ?? "",
      min_quantity: general?.result?.min_quantity ?? "",
      max_quantity: general?.result?.max_quantity ?? "",
      product_cover_type: general?.result?.product_cover_type ?? "",
      product_cover_counterparty:
        general?.result?.product_cover_counterparty ?? "",
      product_option_quantity: general?.result?.product_option_quantity ?? "",
      intention: general?.result?.intention ?? "",
      fixed_flag: general?.result?.fixed_flag ?? "quantity",
    },
    validationSchema: yup.object({
      product_id: yup.string().required("Please select product!"),
      country_origin: yup.string().required("Please select country Of origin!"),
      est_bl_date: yup
        .date()
        .required(" Please select date")
        .typeError("Please select date"),

      grade_id: yup.string().required("Please select grade"),
      quantity: yup.number().typeError("Please insert number!"),
      est_density: yup
        .number()
        .typeError("Please insert number!")
        .required("Please insert Density.")
        .min(0.001, "Deansity can't be Negative or 0 .")
        .max(1, "Density can't be higher than 1."),
      min_quantity: yup.number().typeError("Please insert number!"),
      max_quantity: yup.number().typeError("Please insert number!"),

      units: yup.string().required("Please select unit"),
      product_cover_type: yup.string().required("Plese select products Is"),
      product_option_quantity: yup
        .string()
        .required("Please select option on quantity"),
    }),
    onSubmit: (values) => {
      saveProductData(values);
    },
    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  const saveProductData = async (data) => {
    const body = {
      geographical_area: "",
      grade_id: data.grade_id,
      product_id: data.product_id,
      fixed_flag: data.fixed_flag,
      country_origin: data.country_origin,
      units: data.units,
      est_density: data.est_density,
      trade_treatment: data.trade_treatment,
      quantity: data.quantity,
      quantity_cm: data.quantity_cm,
      plus_minus: String(data.plus_minus),
      min_quantity: data.min_quantity,
      max_quantity: data.max_quantity,
      product_cover_type: data.product_cover_type,
      product_cover_counterparty: data.product_cover_counterparty,
      product_option_quantity: data.product_option_quantity,
      intention: data.intention,
      ops_active_flag: data.ops_active_flag,
      storage_active_flag: data.storage_active_flag,
      finance_active_flag: data.finance_active_flag,
      consolidate_deal_flag: data.consolidate_deal_flag,
      financing_behalf_flag: data.financing_behalf_flag,
      est_bl_date: data.est_bl_date,
      trade_id: trade_id,
      trade_product_id: general.result
        ? general.result.trade_product_id
        : undefined,
      username: session?.user?.username,
    };
    try {
      setLoading(true);
      setOpen(true);
      const method = general.result ? "PUT" : "POST";
      const res = await fetchData(
        `/api/trade/product`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        toast.success("Амжилттай хадгаллаа", {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error(res.result, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
      setOpen(false);
    }
  };

  const label = { inputProps: { "aria-label": "Checkbox demo" } };
  return (
    <form onSubmit={formik.handleSubmit}>
      <Card>
        <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
          <FormControlLabel
            control={
              <Checkbox
                size="small"
                name="ops_active_flag"
                checked={formik.values.ops_active_flag === "Y" ? true : false}
                onChange={(e) => {
                  formik.setFieldValue(
                    "ops_active_flag",
                    e.target.checked ? "Y" : "N"
                  );
                }}
              />
            }
            label="Operation Active"
          />
          <FormControlLabel
            control={
              <Checkbox
                size="small"
                name="storage_active_flag"
                checked={
                  formik.values.storage_active_flag === "Y" ? true : false
                }
                onChange={(e) => {
                  formik.setFieldValue(
                    "storage_active_flag",
                    e.target.checked ? "Y" : "N"
                  );
                }}
              />
            }
            label="Storage Active"
          />
          <FormControlLabel
            control={
              <Checkbox
                size="small"
                name="finance_active_flag"
                checked={
                  formik.values.finance_active_flag === "Y" ? true : false
                }
                onChange={(e) => {
                  formik.setFieldValue(
                    "finance_active_flag",
                    e.target.checked ? "Y" : "N"
                  );
                }}
              />
            }
            label="Finance Active"
          />
          <FormControlLabel
            control={
              <Checkbox
                size="small"
                name="consolidate_deal_flag"
                checked={
                  formik.values.consolidate_deal_flag === "Y" ? true : false
                }
                disabled
                onChange={(e) => {
                  formik.setFieldValue(
                    "consolidate_deal_flag",
                    e.target.checked ? "Y" : "N"
                  );
                }}
              />
            }
            label="Consolidate Deals"
          />
        </Stack>
        <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
          <FormControl fullWidth>
            <InputLabel size="small" sx={{ color: " #0B626B" }}>
              Country of Origin
            </InputLabel>
            <Select
              value={formik.values.country_origin}
              name="country_origin"
              label="Country of Origin"
              onChange={formik.handleChange}
              size="small"
            >
              {lookup?.result.length >= 0 &&
                lookup?.result
                  .filter((f) => f.lookup_type === "Country_Origin")
                  .map((item) => (
                    <MenuItem value={item.lookup_code} key={item.lookup_code}>
                      {item.name}
                    </MenuItem>
                  ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.country_origin &&
                Boolean(formik.errors.country_origin)
              }
            >
              {formik.touched.country_origin && formik.errors.country_origin}
            </FormHelperText>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel size="small" sx={{ color: " #0B626B" }}>
              Product
            </InputLabel>
            <Select
              id="product_id"
              name="product_id"
              label="Product"
              size="small"
              value={formik.values.product_id}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            >
              {product_id?.result.length >= 0 &&
                product_id?.result.map((item) => (
                  <MenuItem value={item.product_id} key={item.product_id}>
                    {item.product_name}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.product_id && Boolean(formik.errors.product_id)
              }
            >
              {formik.touched.product_id && formik.errors.product_id}
            </FormHelperText>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel size="small" sx={{ color: " #0B626B" }}>
              Grade
            </InputLabel>
            <Select
              name="grade_id"
              id="grade_id"
              label="Grade"
              value={formik.values.grade_id}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              size="small"
            >
              {product_id?.result.filter(
                (f) => f.product_id === formik.values.product_id
              ).length > 0 &&
                product_id?.result
                  .filter((f) => f.product_id === formik.values.product_id)[0]
                  .grades.map((item) => (
                    <MenuItem value={item.grade_id} key={item.grade_id}>
                      {item.grade_name}
                    </MenuItem>
                  ))}
            </Select>
            <FormHelperText
              error={formik.touched.grade_id && Boolean(formik.errors.grade_id)}
            >
              {formik.touched.grade_id && formik.errors.grade_id}
            </FormHelperText>
          </FormControl>
        </Stack>
        <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
          <FormControl fullWidth>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                format="YYYY-MM-DD"
                name="est_bl_date"
                value={dayjs(formik.values.est_bl_date)}
                onChange={(newValue) => {
                  formik.setFieldValue(
                    "est_bl_date",
                    dayjs(newValue).format("YYYY-MM-DD")
                  );
                }}
                label="Estimated BL Date"
                slotProps={{
                  textField: { size: "small" },
                  color: "#0B626B",
                }}
              />
            </LocalizationProvider>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel size="small" sx={{ color: "#0B626B" }}>
              Unit
            </InputLabel>
            <Select
              value={formik.values.units}
              name="units"
              label="Unit"
              onChange={formik.handleChange}
              size="small"
            >
              {lookup?.result.length >= 0 &&
                lookup?.result
                  .filter((f) => f.lookup_type === "Unit")
                  .map((item) => (
                    <MenuItem value={item.lookup_code} key={item.lookup_code}>
                      {item.name}
                    </MenuItem>
                  ))}
            </Select>
            <FormHelperText
              error={formik.touched.units && Boolean(formik.errors.units)}
            >
              {formik.touched.units && formik.errors.units}
            </FormHelperText>
          </FormControl>
          <FormControl fullWidth>
            <TextField
              name="est_density"
              variant="outlined"
              sx={{ color: " #0B626B" }}
              label="Estimated Density"
              type="number"
              onChange={(e) => {
                formik.setFieldValue("est_density", e.target.value);
                formik.values.units === "MT"
                  ? formik.setFieldValue(
                      "quantity_cm",
                      formik.values.quantity / e.target.value
                    )
                  : formik.setFieldValue(
                      "quantity",
                      formik.values.quantity_cm * e.target.value
                    );
              }}
              size="small"
              value={formik.values.est_density}
            />
            <FormHelperText
              error={
                formik.touched.est_density && Boolean(formik.errors.est_density)
              }
            >
              {formik.touched.est_density && formik.errors.est_density}
            </FormHelperText>
          </FormControl>
        </Stack>
        <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
          <FormControl fullWidth>
            <TextField
              name="quantity"
              disabled={formik.values.units === "CM" ? true : false}
              label="Quantity MT"
              value={formik.values.quantity}
              type="number"
              size="small"
              onChange={(e) => {
                formik.setFieldValue("quantity", e.target.value);

                const total = formik.values.est_density
                  ? e.target.value / formik.values.est_density
                  : 0;

                formik.setFieldValue(
                  "quantity_cm",
                  total.toFixed(5)
                );
              }}
              InputLabelProps={{
                style: { color: "#0B626B" },
              }}
            />
            <FormHelperText
              error={formik.touched.quantity && Boolean(formik.errors.quantity)}
            >
              {formik.touched.quantity && formik.errors.quantity}
            </FormHelperText>
          </FormControl>
          <FormControl fullWidth>
            <TextField
              name="quantity_cm"
              disabled={formik.values.units === "MT"}
              value={formik.values.quantity_cm}
              type="number"
              onChange={(e) => {
                formik.setFieldValue("quantity_cm", e.target.value);

                const total = formik.values.est_density
                  ? e.target.value * formik.values.est_density
                  : 0;

                formik.setFieldValue("quantity", total.toFixed(5));
              }}
              InputLabelProps={{
                style: { color: "#0B626B" },
              }}
              size="small"
              label="Quantity CM"
            />
          </FormControl>
          <FormControl fullWidth>
            <Box display={"flex"}>
              <TextField
                id="plus_minus"
                name="plus_minus"
                type="number"
                value={formik.values.plus_minus}
                sx={{ color: "#11ABB0" }}
                onChange={formik.handleChange}
                size="small"
                label="+/-"
              />
              <FormHelperText
                error={
                  formik.touched.plus_minus && Boolean(formik.errors.plus_minus)
                }
              >
                {formik.touched.plus_minus && formik.errors.plus_minus}
              </FormHelperText>
              <FormControlLabel
                sx={{ mb: -1, ml: -3 }}
                control={
                  <Checkbox
                    size="small"
                    name="est"
                    checked={formik.values.fixed_flag === "Y"}
                    onChange={(e) => {
                      formik.setFieldValue(
                        "fixed_flag",
                        e.target.checked ? "Y" : "N"
                      );
                    }}
                  />
                }
                label="Est"
              />
            </Box>
          </FormControl>
        </Stack>
        <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
          <FormControl fullWidth>
            <InputLabel size="small" sx={{ color: " #0B626B" }}>
              Product is
            </InputLabel>
            <Select
              value={formik.values.product_cover_type}
              onBlur={formik.handleBlur}
              name="product_cover_type"
              label="Product is"
              onChange={formik.handleChange}
              size="small"
            >
              {lookup?.result.length >= 0 &&
                lookup?.result
                  .filter((f) => f.lookup_type === "Cover_Type")
                  .map((item) => (
                    <MenuItem value={item.lookup_code} key={item.lookup_code}>
                      {item.name}
                    </MenuItem>
                  ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.product_cover_type &&
                Boolean(formik.errors.product_cover_type)
              }
            >
              {formik.touched.product_cover_type &&
                formik.errors.product_cover_type}
            </FormHelperText>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel size="small">To/From Counterparty</InputLabel>
            <Select
              value={formik.values.product_cover_counterparty}
              name="product_cover_counterparty"
              label="To/From Counterparty"
              onChange={formik.handleChange}
              size="small"
            >
              {counterparty_id?.result.length >= 0 &&
                counterparty_id?.result.map((item) => (
                  <MenuItem
                    value={item.legal_entity_id}
                    key={item.legal_entity_id}
                  >
                    {item.name}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel size="small" sx={{ color: " #0B626B" }}>
              Option on Quantity
            </InputLabel>
            <Select
              value={formik.values.product_option_quantity}
              onBlur={formik.handleBlur}
              name="product_option_quantity"
              label="Option on Quantity"
              onChange={formik.handleChange}
              size="small"
            >
              {lookup?.result.length >= 0 &&
                lookup?.result
                  .filter((f) => f.lookup_type === "Responsible_Party")
                  .map((item) => (
                    <MenuItem value={item.lookup_code} key={item.lookup_code}>
                      {item.name}
                    </MenuItem>
                  ))}
            </Select>
            <FormHelperText
              error={
                formik.touched.product_option_quantity &&
                Boolean(formik.errors.product_option_quantity)
              }
            >
              {formik.touched.product_option_quantity &&
                formik.errors.product_option_quantity}
            </FormHelperText>
          </FormControl>
        </Stack>
        <Stack direction={"row"} spacing={1} sx={{ m: 1 }}>
          <TextField
            fullWidth
            id="intention"
            name="intention"
            value={formik.values.intention}
            onChange={formik.handleChange}
            label="Intention"
          />
        </Stack>
        <Divider sx={{ m: 1 }} />
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            m: 1,
          }}
        >
          {session &&
          session?.user?.menuitems.filter((f) => f.menu_name === "Trade")[0]
            .permission === "Edit" ? (
            <>
              <Button type="submit" variant="contained" color="dark">
                Save
              </Button>
            </>
          ) : null}
        </Box>
      </Card>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            minWidth: 600,
            transform: "translate(-50%, -50%)",
            bgcolor: "transparent",
            overflowY: "auto",
            overflowX: "auto",
            boxShadow: 0,
            p: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
            <CircularProgress sx={{ color: "#ffffff" }} />
          </Box>
        </Box>
      </Modal>
    </form>
  );
}
