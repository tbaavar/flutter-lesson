"use client";
import * as React from "react";
import { Box, CircularProgress, Stack, FormHelperText } from "@mui/material";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import MenuItem from "@mui/material/MenuItem";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import Radio from "@mui/material/Radio";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import FormControlLabel from "@mui/material/FormControlLabel";
import InputLabel from "@mui/material/InputLabel";
import RadioGroup from "@mui/material/RadioGroup";
import dayjs from "dayjs";
import MyAlert from "@/components/Myalert";
import { toast, ToastContainer } from "react-toastify";
import { fetchData } from "@/lib/fetch";
import { useStore } from "@/store/store";
import useSWR from "swr";
import { Divider } from "@mui/material";
import * as yup from "yup";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";

export default function PricingDates({ trade_id }) {
  const setMenu = useStore((state) => state.setMenu);
  const [loading, setLoading] = React.useState(false);
  const { data: session } = useSession();
  const { data: price_date_type } = useSWR(
    [`/api/lookup/Price_Date_Type`, session?.user?.token],
    fetcher
  );

  const {
    data: general,
    error,
    isLoading,
  } = useSWR(
    trade_id !== "new"
      ? [`/api/trade/pricingdate/${trade_id}`, session?.user?.token]
      : null,
    fetcher
  );

  const formik = useFormik({
    initialValues: {
      price_date_type: general?.result?.price_date_type ?? "",
      multi_period_flag: general?.result?.multi_period_flag ?? "N",
      date_trigger_flag: general?.result?.date_trigger_flag ?? "N",
      pricing_date_memo: general?.result?.pricing_date_memo ?? "",
      fixed_begin_date:
        general?.result?.fixed_begin_date ?? dayjs().format("YYYY-MM-DD"),
      fixed_end_date:
        general?.result?.fixed_end_date ?? dayjs().format("YYYY-MM-DD"),
      contiguous_flag: general?.result?.contiguous_flag ?? "con",
    },
    validationSchema: yup.object({
      price_date_type: yup.string().required("Please select Type!"),

      fixed_begin_date: yup
        .date()
        .required(" Please select Date")
        .typeError("Please select Date"),
      fixed_end_date: yup
        .date()
        .required(" Please select Date")
        .typeError("Please select Date"),
      contiguous_flag: yup.string().required("Please select Type!"),
    }),
    onSubmit: (values) => {
      saveDatesData(values);
    },
    enableReinitialize: true,
  });

  if (error) return <MyAlert error={error} />;
  if (isLoading || loading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 15 }}>
        <CircularProgress />
      </Box>
    );

  const saveDatesData = async (data) => {
    const body = {
      trade_id: trade_id,
      price_date_type: data.price_date_type,
      multi_period_flag: data.multi_period_flag,
      date_trigger_flag: data.date_trigger_flag,
      pricing_date_memo: data.pricing_date_memo,
      fixed_begin_date: data.fixed_begin_date,
      fixed_end_date: data.fixed_end_date,
      contiguous_flag: data.contiguous_flag,
      trade_pricing_dates_id: general.result
        ? general.result.trade_pricing_dates_id
        : undefined,
      username: session?.user?.username,
    };

    try {
      setLoading(true);
      const method = general.result ? "PUT" : "POST";
      const res = await fetchData(
        `/api/trade/pricingdate`,
        method,
        body,
        session?.user?.token
      );

      if (res.status === 200) {
        let message = null;
        message = "Амжилттай хадгаллаа";

        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
          className: "toast-message",
        });
      } else {
        toast.error("Хадгалах үед алдаа гарлаа", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <Card>
          <Stack direction={"column"} spacing={1} sx={{ ml: 1, mr: 1, mt: 1 }}>
            <Stack direction={"row"} spacing={1}>
              <FormControl fullWidth>
                <InputLabel size="small">Type</InputLabel>
                <Select
                  labelId="price_date_type"
                  size="small"
                  name="price_date_type"
                  label="Type"
                  value={formik.values.price_date_type}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                >
                  {price_date_type?.result.length > 0 &&
                    price_date_type?.result.map((item) => (
                      <MenuItem value={item.lookup_code} key={item.lookup_code}>
                        {item.name}
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText
                  error={
                    formik.touched.price_date_type &&
                    Boolean(formik.errors.price_date_type)
                  }
                >
                  {formik.touched.price_date_type &&
                    formik.errors.price_date_type}
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth>
                <Stack direction={"row"} spacing={1}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        size="small"
                        name="multi_period_flag"
                        checked={
                          formik.values.multi_period_flag === "Y" ? true : false
                        }
                        onChange={(e) => {
                          formik.setFieldValue(
                            "multi_period_flag",
                            e.target.checked ? "Y" : "N"
                          );
                        }}
                      />
                    }
                    label="Multi Period"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        size="small"
                        name="date_trigger_flag"
                        checked={
                          formik.values.date_trigger_flag === "Y" ? true : false
                        }
                        onChange={(e) => {
                          formik.setFieldValue(
                            "date_trigger_flag",
                            e.target.checked ? "Y" : "N"
                          );
                        }}
                      />
                    }
                    label="Date Trigger"
                    labelPlacement="end"
                  />
                </Stack>
              </FormControl>
            </Stack>
            <Stack direction={"row"} spacing={1}>
              <FormControl fullWidth>
                <TextField
                  multiline
                  rows={10}
                  size="small"
                  label="Pricing dates memo"
                  name="pricing_date_memo"
                  value={formik.values.pricing_date_memo}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Stack>
            <Stack
              direction="row"
              spacing={1}
              alignContent={"center"}
              alignItems={"center"}
            >
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    value={dayjs(formik.values.fixed_begin_date)}
                    onBlur={formik.handleBlur}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "fixed_begin_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                    slotProps={{ textField: { size: "small" } }}
                    label="Fixed from date"
                  />

                  <FormHelperText
                    error={
                      formik.touched.fixed_begin_date &&
                      Boolean(formik.errors.fixed_begin_date)
                    }
                  >
                    {formik.touched.fixed_begin_date &&
                      formik.errors.fixed_begin_date}
                  </FormHelperText>
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    format="YYYY-MM-DD"
                    value={dayjs(formik.values.fixed_end_date)}
                    onBlur={formik.handleBlur}
                    onChange={(newValue) => {
                      formik.setFieldValue(
                        "fixed_end_date",
                        dayjs(newValue).format("YYYY-MM-DD")
                      );
                    }}
                    slotProps={{ textField: { size: "small" } }}
                    label="Fixed to date"
                  />

                  <FormHelperText
                    error={
                      formik.touched.fixed_begin_date &&
                      Boolean(formik.errors.fixed_begin_date)
                    }
                  >
                    {formik.touched.fixed_begin_date &&
                      formik.errors.fixed_begin_date}
                  </FormHelperText>
                </LocalizationProvider>
              </FormControl>
              <FormControl fullWidth>
                <RadioGroup
                  row
                  value={formik.values.contiguous_flag}
                  onChange={formik.handleChange}
                  defaultValue="Contiguous"
                  name="contiguous_flag"
                >
                  <FormControlLabel
                    control={<Radio size="small" />}
                    label="Contiguous"
                    value="Contiguous"
                  />
                  <FormControlLabel
                    control={<Radio size="small" />}
                    label="Non-contiguous"
                    value="NonContiguous"
                  />
                </RadioGroup>
                <FormHelperText
                  error={
                    formik.touched.contiguous_flag &&
                    Boolean(formik.errors.contiguous_flag)
                  }
                >
                  {formik.touched.contiguous_flag &&
                    formik.errors.contiguous_flag}
                </FormHelperText>
              </FormControl>
            </Stack>
          </Stack>

          <Divider sx={{ m: 1 }} />
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              m: 1,
            }}
          >
           {session && session?.user?.menuitems.filter(f => f.menu_name === "Trade")[0].permission==="Edit"  ? (
              <>
                <Button type="submit" variant="contained" color="dark">
                  Save
                </Button>
              </>
            ) : null}
          </Box>
        </Card>
      </form>
    </>
  );
}
