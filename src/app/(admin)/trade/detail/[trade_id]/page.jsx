"use client";
import * as React from "react";
import {
  Card,
  Typography,
  Grid,
  Box,
  ToggleButtonGroup,
  IconButton,
} from "@mui/material";
import MuiToggleButton from "@mui/material/ToggleButton";
import { styled } from "@mui/material/styles";
import General from "../../general/page";
import ProductPage from "../../product/page";
import DeliveryPage from "../../delivery/page";
import Law from "../../law/page";
import Payment from "../../payment/page";
import PricingDetails from "../../pricingdetails/page";
import AttachmentPage from "../../attachments/page";
import MemoPage from "../../memo/page";
import SummaryPage from "../../summary/page";
import { useRouter } from "next/navigation";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import CircularProgress from "@mui/material/CircularProgress";

const ToggleButton = styled(MuiToggleButton)({
  "&.Mui-selected": {
    backgroundColor: "#ddd",
    "&:hover": {
      backgroundColor: "#ddd",
    },
  },
  border: 0,
  "&:hover": {
    backgroundColor: "#ddd",
  },
});

export default function NewTrade({ params }) {
  const router = useRouter();
  const { trade_id } = params;
  const [menu, setMenu] = React.useState(1);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const handleBackButtonClick = () => {
    setLoading(true); // Start loading before navigation
    router.push(`/trade`);
  };
  const [alignment, setAlignment] = React.useState("product");

  const handleChange = (e, newAlignment) => {
    setAlignment(newAlignment);
    setSuccess(true)
  };


  const buttonSx = {
    ...(success && {
      bgcolor: "dark",
      "&:hover": {
        bgcolor: "dark",
      },
    }),
  };
  return (
    <Grid container spacing={1}>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ m: 1, position: "relative" }}>
          <IconButton
            aria-label="back"
            size="medium"
            disabled={loading}
            sx={buttonSx}
            onClick={handleBackButtonClick}
          >
            <ArrowBackIcon />
            <Typography color="text.primary" variant="subtitle2">
              Back
            </Typography>
          </IconButton>
          {loading && (
            <CircularProgress
              size={25}
              sx={{
                color: "dark",
                position: "absolute",
                top: "50%",
                left: "calc(100% + 8px)", // Adjust distance from button
                marginTop: "-12px",
                marginLeft: "-12px",
              }}
            />
          )}
        </Box>
      </Box>
      <Grid item xs={12}>
        <General trade_id={trade_id} />
      </Grid>
      <Grid item xs={2}>
        <Card sx={{ display: trade_id !== "new" ? "block" : "none" }}>
          <ToggleButtonGroup
            value={alignment}
            exclusive
            onChange={handleChange}
            orientation="vertical"
            size="small"
            fullWidth
          >
            <ToggleButton
              value="product"
              variant="filled"
              size="small"
              onClick={() => setMenu(1)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Product</strong>
            </ToggleButton>
            <ToggleButton
              value="delivery"
              variant="filled"
              size="small"
              onClick={() => setMenu(2)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Delivery</strong>
            </ToggleButton>
            <ToggleButton
              value="law"
              variant="filled"
              size="small"
              onClick={() => setMenu(3)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Law</strong>
            </ToggleButton>
            <ToggleButton
              value="payment"
              variant="filled"
              size="small"
              onClick={() => setMenu(4)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Payment</strong>
            </ToggleButton>
            <ToggleButton
              value="pricing"
              variant="filled"
              size="small"
              onClick={() => setMenu(5)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Pricing</strong>
            </ToggleButton>
            <ToggleButton
              value="attachments"
              variant="filled"
              size="small"
              onClick={() => setMenu(7)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Attachments</strong>
            </ToggleButton>
            <ToggleButton
              value="memo"
              variant="filled"
              size="small"
              onClick={() => setMenu(8)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Memo</strong>
            </ToggleButton>
            <ToggleButton
              value="summary"
              variant="filled"
              size="small"
              onClick={() => setMenu(9)}
              style={{ justifyContent: "flex-start", textAlign: "left" }}
            >
              <strong>Summary</strong>
            </ToggleButton>
          </ToggleButtonGroup>
        </Card>
      </Grid>
      <Grid item xs={10}>
        <Card sx={{ display: trade_id !== "new" ? "block" : "none" }}>
          {menu === 1 ? <ProductPage trade_id={trade_id} /> : null}
          {menu === 2 ? <DeliveryPage trade_id={trade_id} /> : null}
          {menu === 3 ? <Law trade_id={trade_id} /> : null}
          {menu === 4 ? <Payment trade_id={trade_id} /> : null}
          {menu === 5 ? <PricingDetails trade_id={trade_id} /> : null}{" "}
          {menu === 7 ? <AttachmentPage trade_id={trade_id} /> : null}
          {menu === 8 ? <MemoPage trade_id={trade_id} /> : null}
          {menu === 9 ? <SummaryPage trade_id={trade_id} /> : null}
        </Card>
      </Grid>
    </Grid>
  );
}
