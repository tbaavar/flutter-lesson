"use client";
import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import {
  Box,
  Stack,
  Typography,
  Grid,
  IconButton,
  FormControl,
  InputBase,
  Select,
  InputLabel,
} from "@mui/material";
import dayjs from "dayjs";
import SearchIcon from "@mui/icons-material/Search";
import MenuItem from "@mui/material/MenuItem";
import { useSession } from "next-auth/react";
import useSWR from "swr";
import { fetcher } from "@/lib/fetcher";
import CustomNoRowsOverlay from "@/components/NoRecord";

export default function LogPage() {
  const { data: session } = useSession();
  const [filterTextLogs, setFilterTextLogs] = React.useState("");
  const [item, setItem] = React.useState(7);
  const [subItem, setSubItem] = React.useState(null);

  const handleMainSelectChange = (event) => {
    setItem(event.target.value);
    setSubItem(null);
  };

  const handleSubSelectChange = (event) => {
    setSubItem(event.target.value);
  };

  const { data: general } = useSWR(
    [`/api/trade/log/general`, session?.user?.token],
    fetcher
  );
  const { data: delivery } = useSWR(
    [`/api/trade/log/delivery`, session?.user?.token],
    fetcher
  );
  const { data: law } = useSWR(
    [`/api/trade/log/law`, session?.user?.token],
    fetcher
  );
  const { data: Payment } = useSWR(
    [`/api/trade/log/payment`, session?.user?.token],
    fetcher
  );
  const { data: product } = useSWR(
    [`/api/trade/log/product`, session?.user?.token],
    fetcher
  );
  const { data: pricing } = useSWR(
    [`/api/trade/log/pricing`, session?.user?.token],
    fetcher
  );
  const { data: storage } = useSWR(
    [`/api/storage/log`, session?.user?.token],
    fetcher
  );
  const { data: operation } = useSWR(
    [`/api/operation/log`, session?.user?.token],
    fetcher
  );
  const { data: invoice } = useSWR(
    [`/api/finance/log`, session?.user?.token],
    fetcher
  );
  const { data: access } = useSWR(
    [`/api/user/log/access`, session?.user?.token],
    fetcher
  );
  const { data: permission } = useSWR(
    [`/api/user/log/permission`, session?.user?.token],
    fetcher
  );
  const { data: costs } = useSWR(
    [`/api/finance/log/detail`, session?.user?.token],
    fetcher
  );

  const generalRows =
    general?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const productRows =
    product?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const pricingRows =
    pricing?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const deliveryRows =
    delivery?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const lawRows =
    law?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const PaymentRows =
    Payment?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const StorageRows =
    storage?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const OperationRows =
    operation?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const InvoiceRows =
    invoice?.result.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const AccessRows =
    access?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const PermissionRows =
    permission?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];
  const CostsRows =
    costs?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];

  const productColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "log_trade_product_id",
      renderHeader: () => <strong>{"Log trade product id"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "trade_product_id",
      renderHeader: () => <strong>{"Trade product id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "ops_active_flag",
      renderHeader: () => <strong>{"Ops active flag"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "finance_active_flag",
      renderHeader: () => <strong>{"Finance active flag"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "storage_active_flag",
      renderHeader: () => <strong>{"Storage active"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product name"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "grade_name",
      renderHeader: () => <strong>{"Grade name"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "country_origin",
      renderHeader: () => <strong>{"Country origin"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "units",
      renderHeader: () => <strong>{"Units"}</strong>,
      width: 80,
      headerAlign: "left",
    },

    {
      field: "units_name",
      renderHeader: () => <strong>{"Units name"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "est_density",
      renderHeader: () => <strong>{"Est density"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "quantity_mt",
      renderHeader: () => <strong>{"Quantity mt"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "quantity_cm",
      renderHeader: () => <strong>{"Quantity cm"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "plus_minus",
      renderHeader: () => <strong>{"Plus minus"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "est_bl_date",
      renderHeader: () => <strong>{"Est bl date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.est_bl_date).format("YYYY-MM-DD"),
      width: 100,
      headerAlign: "left",
    },
    {
      field: "product_cover_type",
      renderHeader: () => <strong>{"Product cover type"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "product_cover_counterparty",
      renderHeader: () => <strong>{"Product cover counterparty"}</strong>,
      width: 180,
      headerAlign: "left",
    },

    {
      field: "product_option_quantity",
      renderHeader: () => <strong>{"Product option quantity"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "intention",
      renderHeader: () => <strong>{"Intention"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Last change flag"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 60,
      headerAlign: "left",
    },

    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.creation_date).format("YYYY-MM-DD"),
      width: 100,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.last_update_date).format("YYYY-MM-DD"),
      width: 130,
      headerAlign: "left",
    },
  ];
  const pricingColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_fixed_pricing_id",
      renderHeader: () => <strong>{"Log trade fixed pricing id"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "trade_fixed_pricing_id",
      renderHeader: () => <strong>{"Trade fixed pricing id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "unit",
      renderHeader: () => <strong>{"Unit"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "unit_name",
      renderHeader: () => <strong>{"Unit name"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "amount",
      renderHeader: () => <strong>{"Amount"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "total_amount",
      renderHeader: () => <strong>{"Total amount"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "freight_currency",
      renderHeader: () => <strong>{"Freight currency"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "amount_other",
      renderHeader: () => <strong>{"Amount other"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "total_amount_other",
      renderHeader: () => <strong>{"Total amount other"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 130,
      headerAlign: "left",
    },

    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 90,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 160,
      headerAlign: "left",
    },

    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => {
        if (params.row.creation_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 100,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => {
        if (params.row.last_update_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 120,
      headerAlign: "left",
    },
  ];
  const deliveryColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_delivery_id",
      renderHeader: () => <strong>{"Log trade delivery id"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "trade_delivery_id",
      renderHeader: () => <strong>{"Trade delivery id"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "load_port_name",
      renderHeader: () => <strong>{"Load port name"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "discharge_port_name",
      renderHeader: () => <strong>{"Discharge port name"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "transportation",
      renderHeader: () => <strong>{"Transportation"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "laytime",
      renderHeader: () => <strong>{"Laytime"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "dates_based_on",
      renderHeader: () => <strong>{"Dates based on"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "date_range_from",
      renderHeader: () => <strong>{"Date range from"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.date_range_from).format("YYYY-MM-DD"),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "date_range_to",
      renderHeader: () => <strong>{"Date range to"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.date_range_to).format("YYYY-MM-DD"),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "quantity_based_on",
      renderHeader: () => <strong>{"Quantity based on"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "demurrage_flag",
      renderHeader: () => <strong>{"Demurrage flag"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "spot_demurrage_rate",
      renderHeader: () => <strong>{"Spot demurrage rate"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "spot_demurrage_rate_max",
      renderHeader: () => <strong>{"Spot demmurage rate max"}</strong>,
      width: 190,
      headerAlign: "left",
    },
    {
      field: "spot_demurrage_rate_other",
      renderHeader: () => <strong>{"Spot demurrage rate other"}</strong>,
      width: 210,
      headerAlign: "left",
    },

    {
      field: "time_demurrage_rate",
      renderHeader: () => <strong>{"Time demurrage rate"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "time_demurrage_rate_max",
      renderHeader: () => <strong>{"Time demurrage rate max"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "time_demurrage_rate_other",
      renderHeader: () => <strong>{"Time demurrage rate other"}</strong>,
      width: 180,
      headerAlign: "left",
    },

    {
      field: "notification_timebar",
      renderHeader: () => <strong>{"Notification timebar"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "notification_from_date",
      renderHeader: () => <strong>{"Notification from date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.notification_from_date).format("YYYY-MM-DD"),
      width: 150,
      headerAlign: "left",
    },
    {
      field: "notification_other",
      renderHeader: () => <strong>{"Notification other"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "fulldoc_timebar",
      renderHeader: () => <strong>{"Prorata timebar"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "fulldoc_from_date",
      renderHeader: () => <strong>{"Fulldoc from date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.fulldoc_from_date).format("YYYY-MM-DD"),
      width: 100,
      headerAlign: "left",
    },
    {
      field: "fulldoc_other",
      renderHeader: () => <strong>{"Fulldoc other"}</strong>,
      width: 100,
      headerAlign: "left",
    },

    {
      field: "pdc_to_apply",
      renderHeader: () => <strong>{"Pdc to apply"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 200,
      headerAlign: "left",
    },

    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.creation_date).format("YYYY-MM-DD"),
      width: 100,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.last_update_date).format("YYYY-MM-DD"),
      width: 120,
      headerAlign: "left",
    },
  ];
  const lawColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_law_id",
      renderHeader: () => <strong>{"Log trade law id"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "trade_law_id",
      renderHeader: () => <strong>{"Trade law id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "notes",
      renderHeader: () => <strong>{"Notes"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "conflict",
      renderHeader: () => <strong>{"Conflict"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "law_name",
      renderHeader: () => <strong>{"Law name"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "title_to_pass",
      renderHeader: () => <strong>{"Title to pass"}</strong>,
      width: 190,
      headerAlign: "left",
    },
    {
      field: "title_to_pass_other",
      renderHeader: () => <strong>{"Title to pass other"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "risk_to_pass",
      renderHeader: () => <strong>{"Rist to pass"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "risk_to_pass_other",
      renderHeader: () => <strong>{"Rist to pass other"}</strong>,
      width: 130,
      headerAlign: "left",
    },

    {
      field: "general_term_condition",
      renderHeader: () => <strong>{"General term condition"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "quality_test_location",
      renderHeader: () => <strong>{"Quality test location"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "quality_test_other",
      renderHeader: () => <strong>{"Quality test other"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "contact_sent_date",
      renderHeader: () => <strong>{"Contact sent date"}</strong>,
      renderCell: (params) => {
        if (params.row.contact_sent_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.contact_sent_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 130,
      headerAlign: "left",
    },
    {
      field: "contract_received_date",
      renderHeader: () => <strong>{"Contract received date"}</strong>,
      renderCell: (params) => {
        if (params.row.contract_received_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.contract_received_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 150,
      headerAlign: "left",
    },
    {
      field: "contract_drafted_date",
      renderHeader: () => <strong>{"Contract drafted date"}</strong>,
      renderCell: (params) => {
        if (params.row.contract_drafted_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.contract_drafted_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 150,
      headerAlign: "left",
    },
    {
      field: "response_sent_date",
      renderHeader: () => <strong>{"Response sent date"}</strong>,
      renderCell: (params) => {
        if (params.row.response_sent_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.response_sent_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 150,
      headerAlign: "left",
    },
    {
      field: "broker_contract_received_date",
      renderHeader: () => <strong>{"Broker contract received date"}</strong>,
      renderCell: (params) => {
        if (params.row.broker_contract_received_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.broker_contract_received_date).format(
                  "YYYY-MM-DD"
                )}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 190,
      headerAlign: "left",
    },
    {
      field: "contract_finish_flag",
      renderHeader: () => <strong>{"Contract finish flag"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "contract_date_comment",
      renderHeader: () => <strong>{"Contract date comment"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 80,
      headerAlign: "left",
    },

    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => {
        if (params.row.creation_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 100,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => {
        if (params.row.last_update_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 140,
      headerAlign: "left",
    },
  ];
  const paymentColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_payment_id",
      renderHeader: () => <strong>{"Log trade payment id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_payment_id",
      renderHeader: () => <strong>{"Trade payment id"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "prepaid_flag",
      renderHeader: () => <strong>{"Prepaid flag"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "credit_type",
      renderHeader: () => <strong>{"Credit type"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "settlement_currency",
      renderHeader: () => <strong>{"Settlement currency"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "day_1",
      renderHeader: () => <strong>{"Day 1"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "time_condition_1",
      renderHeader: () => <strong>{"Time condition 1"}</strong>,
      width: 130,
      headerAlign: "left",
    },

    {
      field: "condition_1",
      renderHeader: () => <strong>{"Condition 1"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "condition_other_1",
      renderHeader: () => <strong>{"Condition other 1"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "transportation",
      renderHeader: () => <strong>{"Transportation"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "transportation_whichever",
      renderHeader: () => <strong>{"Transportation wichever"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "day_count_method",
      renderHeader: () => <strong>{"Day count method"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "if_saturday_calc_method",
      renderHeader: () => <strong>{"If saturday calc"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "if_sunday_calc_method",
      renderHeader: () => <strong>{"If sunday calc"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "if_public_holiday_mon_calc_meth",
      renderHeader: () => <strong>{"If puclic holiday mon"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "if_public_holiday_other_calc_me",
      renderHeader: () => <strong>{"If puclic holiday other"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "interest_rate_type",
      renderHeader: () => <strong>{"Interest rate type"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "margin",
      renderHeader: () => <strong>{"Margin"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "period",
      renderHeader: () => <strong>{"Period"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "payment_documentation",
      renderHeader: () => <strong>{"Payment documentation"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "payment_note",
      renderHeader: () => <strong>{"Payment note"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 100,
      headerAlign: "left",
    },

    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 80,
      headerAlign: "left",
    },

    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => {
        if (params.row.creation_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 100,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => {
        if (params.row.last_update_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 120,
      headerAlign: "left",
    },
  ];
  const generalColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_trade_id",
      renderHeader: () => <strong>{"Log trade id"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "trade_status",
      renderHeader: () => <strong>{"Trade status"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "company_name",
      renderHeader: () => <strong>{"Company"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "trade_type_name",
      renderHeader: () => <strong>{"Trade type name"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "trade_date",
      renderHeader: () => <strong>{"Trade date"}</strong>,
      renderCell: (params) => {
        if (params.row.trade_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.trade_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 120,
      headerAlign: "left",
    },
    {
      field: "trade_origin_name",
      renderHeader: () => <strong>{"Trade origin"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "terms",
      renderHeader: () => <strong>{"Terms"}</strong>,
      width: 70,
      headerAlign: "left",
    },

    {
      field: "trade_term_name",
      renderHeader: () => <strong>{"Trade term name"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "deliver_method_name",
      renderHeader: () => <strong>{"Deliver"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "operator_sign_off_flag",
      renderHeader: () => <strong>{"Operator singoff"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "finance_sign_off_flag",
      renderHeader: () => <strong>{"Finance flag"}</strong>,
      width: 100,
      headerAlign: "left",
    },

    {
      field: "trader_name",
      renderHeader: () => <strong>{"Trader name"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "operator_name",
      renderHeader: () => <strong>{"Operator name"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "finance_name",
      renderHeader: () => <strong>{"Finance"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "contract_name",
      renderHeader: () => <strong>{"Contract"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Lastest change"}</strong>,
      width: 100,
      headerAlign: "left",
    },

    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => {
        if (params.row.creation_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 100,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 110,
      headerAlign: "left",
    },

    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => {
        if (params.row.last_update_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 120,
      headerAlign: "left",
    },
  ];
  const storageColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_parcel_movement_id",
      renderHeader: () => <strong>{"Log parcel movement id"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "parcel_movement_id",
      renderHeader: () => <strong>{"Parcel movement id"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "voyage_title",
      renderHeader: () => <strong>{"Voyage title"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "terminal_name",
      renderHeader: () => <strong>{"Terminal name"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "tank_name",
      renderHeader: () => <strong>{"Tank name"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "transfer_type",
      renderHeader: () => <strong>{"Transfer type"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product name"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "grade_name",
      renderHeader: () => <strong>{"Grade name"}</strong>,
      width: 200,
      headerAlign: "left",
    },

    {
      field: "outturn_qty_mt",
      renderHeader: () => <strong>{"QTY MT"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "outturn_qty_cm",
      renderHeader: () => <strong>{"QTY CM"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "country_origin",
      renderHeader: () => <strong>{"Country origin"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "parcel_comment",
      renderHeader: () => <strong>{"Parcel comment"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "transfer_status",
      renderHeader: () => <strong>{"Transfer status"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "adjustment_type",
      renderHeader: () => <strong>{"Adjustment type"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "transfer_completion_date",
      renderHeader: () => <strong>{"Transfer completion date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.transfer_completion_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
      headerAlign: "left",
    },
    {
      field: "transfer_completion_date_est_fl",
      renderHeader: () => <strong>{"Transfer date EST"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "terminal_from_date",
      renderHeader: () => <strong>{"Terminal from date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.terminal_from_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "terminal_to_date",
      renderHeader: () => <strong>{"Terminal to date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.terminal_to_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 110,
      headerAlign: "left",
    },
    {
      field: "quantity_mt",
      renderHeader: () => <strong>{"Quantity MT"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "quantity_cm",
      renderHeader: () => <strong>{"Quantity CM"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "clean_tank_flag",
      renderHeader: () => <strong>{"Clean tank flag"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "blend_flag",
      renderHeader: () => <strong>{"Blend flag"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "transfer_product_name",
      renderHeader: () => <strong>{"Tranfer product"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "transfer_grade_name",
      renderHeader: () => <strong>{"Transfer grade"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "circulation",
      renderHeader: () => <strong>{"Circulation"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.circulation).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 90,
      headerAlign: "left",
    },
    {
      field: "blend_ratio",
      renderHeader: () => <strong>{"Blend ratio"}</strong>,
      width: 80,
      headerAlign: "left",
    },
    {
      field: "final_product_name",
      renderHeader: () => <strong>{"Final product name"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "final_grade_name",
      renderHeader: () => <strong>{"Final grade name"}</strong>,
      width: 200,
      headerAlign: "left",
    },
    {
      field: "final_density",
      renderHeader: () => <strong>{"Final density"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "balance_mt",
      renderHeader: () => <strong>{"MT"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "balance_cm",
      renderHeader: () => <strong>{"CM"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "tank_comment",
      renderHeader: () => <strong>{"Tank comment"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Lastest change flag"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 60,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 90,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 110,
      headerAlign: "left",
    },
  ];
  const operationColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 20,
      headerAlign: "left",
    },
    {
      field: "log_voyage_parcel_id",
      renderHeader: () => <strong>{"Log voyage parcel id"}</strong>,
      width: 160,
      headerAlign: "left",
    },

    {
      field: "trade_id",
      renderHeader: () => <strong>{"Trade id"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "voyage_id",
      renderHeader: () => <strong>{"Voyage id"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "company_name",
      renderHeader: () => <strong>{"Company name"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "counterparty",
      renderHeader: () => <strong>{"Counterparty"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "port_type",
      renderHeader: () => <strong>{"Port type"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "voyage_port",
      renderHeader: () => <strong>{"Voyage port"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "voyage_title",
      renderHeader: () => <strong>{"Voyage title"}</strong>,
      width: 250,
      headerAlign: "left",
    },

    {
      field: "voyage_parcel_id",
      renderHeader: () => <strong>{"Voyage parcel id"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "parcel_number",
      renderHeader: () => <strong>{"Parcel number"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "load_port",
      renderHeader: () => <strong>{"Load port"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "discharge_port_id",
      renderHeader: () => <strong>{"Discharge port id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "dates_based_on",
      renderHeader: () => <strong>{"Dates based on"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "product_name",
      renderHeader: () => <strong>{"Product name"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "grade_name",
      renderHeader: () => <strong>{"Grade name"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "transfer_date",
      renderHeader: () => <strong>{"Transfer date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.transfer_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 90,
      headerAlign: "left",
    },
    {
      field: "transfer_date_est_flag",
      renderHeader: () => <strong>{"Transfer date est flag"}</strong>,
      width: 90,
      headerAlign: "left",
    },
    {
      field: "border_crossing_date",
      renderHeader: () => <strong>{"Border crossing date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.border_crossing_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 140,
      headerAlign: "left",
    },

    {
      field: "border_crossing_date_est_flag",
      renderHeader: () => <strong>{"Border crossing EST flag"}</strong>,
      width: 170,
      headerAlign: "left",
    },
    {
      field: "supply_start_date",
      renderHeader: () => <strong>{"Supply start date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.supply_start_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 120,
      headerAlign: "left",
    },
    {
      field: "supply_end_date",
      renderHeader: () => <strong>{"Supply end date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.supply_end_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 110,
      headerAlign: "left",
    },
    {
      field: "no_agreed_supply_date_flag",
      renderHeader: () => <strong>{"No agreed supply date"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "transfer_detail_date",
      renderHeader: () => <strong>{"Transfer detail date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.transfer_detail_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 130,
      headerAlign: "left",
    },
    {
      field: "api_60f",
      renderHeader: () => <strong>{"API"}</strong>,
      width: 40,
      headerAlign: "left",
    },
    {
      field: "est_density_15c_air",
      renderHeader: () => <strong>{"Est density air"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "est_density_15c_vac",
      renderHeader: () => <strong>{"Est density vac"}</strong>,
      width: 130,
      headerAlign: "left",
    },
    {
      field: "shipping_quantity_mt",
      renderHeader: () => <strong>{"Shipping quantity MT"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "shipping_quantity_cm",
      renderHeader: () => <strong>{"Shipping quantity CM"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "est_quantity_flag",
      renderHeader: () => <strong>{"Est quantity flag"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "invoicing_unit",
      renderHeader: () => <strong>{"Invoicing unit"}</strong>,
      width: 140,
      headerAlign: "left",
    },
    {
      field: "operator_name",
      renderHeader: () => <strong>{"Operator name"}</strong>,
      width: 110,
      headerAlign: "left",
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Lasest change flag"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 60,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 140,
      headerAlign: "left",
    },

    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 90,
      headerAlign: "left",
    },
    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 110,
      headerAlign: "left",
    },
  ];
  const InvoiceColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "log_invoice_id",
      renderHeader: () => <strong>{"Log invoice id"}</strong>,
      width: 100,
    },
    {
      field: "voyage_parcel_id",
      renderHeader: () => <strong>{"Voyage parcel id"}</strong>,
      width: 200,
    },
    {
      field: "parcel_number",
      renderHeader: () => <strong>{"Parcel number"}</strong>,
      width: 120,
    },
    {
      field: "invoice_id",
      renderHeader: () => <strong>{"Invoice id"}</strong>,
      width: 100,
    },
    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice number"}</strong>,
      width: 150,
    },
    {
      field: "company_id",
      renderHeader: () => <strong>{"Company id"}</strong>,
      width: 100,
    },
    {
      field: "company_name",
      renderHeader: () => <strong>{"Company name"}</strong>,
      width: 120,
    },
    {
      field: "company_contact",
      renderHeader: () => <strong>{"Company contact"}</strong>,
      width: 120,
    },
    {
      field: "counterparty_id",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 120,
    },

    {
      field: "counterparty_name",
      renderHeader: () => <strong>{"Bank id"}</strong>,
      width: 120,
    },
    {
      field: "counterparty_contact",
      renderHeader: () => <strong>{"Counterparty contact"}</strong>,
      width: 120,
    },
    {
      field: "invoice_owner",
      renderHeader: () => <strong>{"Invoice owner"}</strong>,
      width: 120,
    },

    {
      field: "invoice_type",
      renderHeader: () => <strong>{"Invoice type"}</strong>,
      width: 120,
    },
    {
      field: "invoice_state",
      renderHeader: () => <strong>{"Invoice state"}</strong>,
      width: 120,
    },
    {
      field: "invoice_date",
      renderHeader: () => <strong>{"Invoice date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.invoice_date).format("YYYY-MM-DD")}
          </Typography>
          {/* <Typography variant="h7">{params.row.invoice_number}</Typography> */}
        </Box>
      ),
      width: 120,
    },
    {
      field: "bank",
      renderHeader: () => <strong>{"Bank"}</strong>,
      width: 120,
    },
    {
      field: "account",
      renderHeader: () => <strong>{"Account"}</strong>,
      width: 120,
    },
    {
      field: "invoice_amount",
      renderHeader: () => <strong>{"Invoice amount"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography variant="h7">
            {params.row.invoice_amount + " " + params.row.currency}
          </Typography>
        </Box>
      ),

      width: 130,
    },
    {
      field: "currency",
      renderHeader: () => <strong>{"Currency"}</strong>,
      width: 120,
    },
    {
      field: "invoice_description",
      renderHeader: () => <strong>{"Invoice description"}</strong>,
      width: 120,
    },
    {
      field: "invoice_comments",
      renderHeader: () => <strong>{"Invoice comment"}</strong>,
      width: 120,
    },

    {
      field: "invoice_payment_status",
      renderHeader: () => <strong>{"Invoice payment status"}</strong>,
      width: 120,
    },
    {
      field: "invoice_status",
      renderHeader: () => <strong>{"Invoice status"}</strong>,
      width: 120,
    },
    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 120,
    },
    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 120,
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 170,
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 120,
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 120,
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 120,
    },
  ];
  const accessColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      headerAlign: "left",
    },

    {
      field: "user_login_id",
      renderHeader: () => <strong>{"User login id"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "username",
      renderHeader: () => <strong>{"Username"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 150,
      headerAlign: "left",
    },
  ];
  const permissionColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "log_user_permission_id",
      renderHeader: () => <strong>{"Log user permission id"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "user_permission_id",
      renderHeader: () => <strong>{"User permission id"}</strong>,
      width: 160,
      headerAlign: "left",
    },
    {
      field: "user_id",
      renderHeader: () => <strong>{"User id"}</strong>,
      width: 150,
      headerAlign: "left",
    },
    {
      field: "username",
      renderHeader: () => <strong>{"Username"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "menu_name",
      renderHeader: () => <strong>{"Menu name"}</strong>,
      width: 70,
      headerAlign: "left",
    },

    {
      field: "permission",
      renderHeader: () => <strong>{"Permission"}</strong>,
      width: 80,
      headerAlign: "left",
    },

    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Last change"}</strong>,
      width: 90,
      headerAlign: "left",
    },

    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 70,
      headerAlign: "left",
    },
    {
      field: "created_by",
      renderHeader: () => <strong>{"Creation by"}</strong>,
      width: 100,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => (
        <Box sx={{ flexDirection: "column", display: "flex" }}>
          <Typography sx={{ fontSize: "12px" }}>
            {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
          </Typography>
        </Box>
      ),
      width: 100,
      headerAlign: "left",
    },
  ];
  const costsColumn = [
    {
      field: "id",
      renderHeader: () => <strong>{"ID"}</strong>,
      width: 50,
      headerAlign: "left",
    },
    {
      field: "log_invoice_detail_id",
      renderHeader: () => <strong>{"Log invoice detail id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "invoice_detail_id",
      renderHeader: () => <strong>{"Invoice detail id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "invoice_id",
      renderHeader: () => <strong>{"Invoice id"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice number"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "balance_type",
      renderHeader: () => <strong>{"Balance type"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "cost_name",
      renderHeader: () => <strong>{"Cost name"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "value",
      renderHeader: () => <strong>{"Value"}</strong>,
      width: 120,
      headerAlign: "left",
    },
    {
      field: "tran_date",
      renderHeader: () => <strong>{"Train date"}</strong>,
      renderCell: (params) => {
        if (params.row.tran_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.tran_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 110,
      headerAlign: "left",
    },
    {
      field: "exp_pay_date",
      renderHeader: () => <strong>{"Exp pay date"}</strong>,
      renderCell: (params) => {
        if (params.row.exp_pay_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.exp_pay_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 110,
      headerAlign: "left",
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      width: 250,
      headerAlign: "left",
    },
    {
      field: "parent_invoice_detail_id",
      renderHeader: () => <strong>{"Parent invoice detail id"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "latest_change_flag",
      renderHeader: () => <strong>{"Latest change flag"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "status",
      renderHeader: () => <strong>{"Status"}</strong>,
      width: 120,
      headerAlign: "left",
    },

    {
      field: "created_by",
      renderHeader: () => <strong>{"Created by"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "creation_date",
      renderHeader: () => <strong>{"Creation date"}</strong>,
      renderCell: (params) => {
        if (params.row.creation_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.creation_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 110,
      headerAlign: "left",
    },

    {
      field: "last_updated_by",
      renderHeader: () => <strong>{"Last updated by"}</strong>,
      width: 180,
      headerAlign: "left",
    },
    {
      field: "last_update_date",
      renderHeader: () => <strong>{"Last update date"}</strong>,
      renderCell: (params) => {
        if (params.row.last_update_date !== null) {
          return (
            <Box sx={{ flexDirection: "column", display: "flex" }}>
              <Typography variant="h7">
                {dayjs(params.row.last_update_date).format("YYYY-MM-DD")}
              </Typography>
            </Box>
          );
        } else {
          return null;
        }
      },
      width: 110,
      headerAlign: "left",
    },
  ];

  const filterRows = (searchText, rows) => {
    if (!searchText) return rows;

    return rows.filter((row) => {
      return Object.values(row).some(
        (value) =>
          (typeof value === "string" &&
            value.toLowerCase().includes(searchText.toLowerCase())) ||
          (typeof value === "number" &&
            value.toString().includes(searchText.toLowerCase()))
      );
    });
  };
  const handleFilterChangeLogs = (e) => {
    setFilterTextLogs(e.target.value);
  };

  const filteredRows = filterRows(
    filterTextLogs,
    subItem === 1
      ? generalRows
      : subItem === 2
      ? productRows
      : subItem === 3
      ? deliveryRows
      : subItem === 4
      ? lawRows
      : subItem === 5
      ? PaymentRows
      : subItem === 6
      ? pricingRows
      : item === 7
      ? OperationRows
      : item === 8
      ? StorageRows
      : item === 9
      ? InvoiceRows
      : item === 10
      ? AccessRows
      : item === 11
      ? PermissionRows
      : item === 12
      ? CostsRows
      : []
  );

  return (
    <>
      <Grid container alignItems="center">
        <Grid item>
          <IconButton
            type="button"
            sx={{
              backgroundColor: "white",
              border: "none",
              borderRadius: "0.2rem 0 0 0.2rem",
              height: 30,
              mb: 0.2,
            }}
            aria-label="search"
          >
            <SearchIcon />
          </IconButton>
          <InputBase
            sx={{
              flex: 1,
              backgroundColor: "white",
              border: "none",
              borderRadius: "0 0.2rem 0.2rem 0",
              width: 250,
              height: 30,
            }}
            placeholder=" Search Logs"
            inputProps={{ "aria-label": "Search logs" }}
            value={filterTextLogs}
            onChange={handleFilterChangeLogs}
          />
        </Grid>

        <FormControl
          sx={{ ml: 5, mt: 1, mb: 1, minWidth: "20vh" }}
          size="small"
        >
          <InputLabel id="choice-select-label">Log type</InputLabel>
          <Select
            labelId="Log type"
            label="Log type"
            id="Log type"
            name="Log type"
            value={item}
            onChange={handleMainSelectChange}
          >
            <MenuItem value={0}>Trade log type</MenuItem>
            <MenuItem value={7}>Parcel</MenuItem>
            <MenuItem value={8}>Storage</MenuItem>
            <MenuItem value={9}>Invoice</MenuItem>
            <MenuItem value={10}>Access</MenuItem>
            <MenuItem value={11}>Permission</MenuItem>
            <MenuItem value={12}>Costs</MenuItem>
          </Select>
        </FormControl>

        {item === 0 && (
          <FormControl
            sx={{ ml: 3, mt: 1, mb: 1, minWidth: "20vh" }}
            size="small"
          >
            <InputLabel id="sub-choice-select-label">Trade log type</InputLabel>
            <Select
              labelId="Trade type log"
              label="Trade type log"
              id="Trade type log"
              name="Trade type log"
              value={subItem}
              onChange={handleSubSelectChange}
            >
              <MenuItem value={1}>General</MenuItem>
              <MenuItem value={2}>Product</MenuItem>
              <MenuItem value={3}>Delivery</MenuItem>
              <MenuItem value={4}>Law</MenuItem>
              <MenuItem value={5}>Payment</MenuItem>
              <MenuItem value={6}>Pricing</MenuItem>
            </Select>
          </FormControl>
        )}
      </Grid>

      <Stack
        width={"92vw"}
        height={"82vh"}
        direction="row"
        spacing={1}
        sx={{ mb: 1 }}
      >
        <DataGrid
          getRowId={(row) => row.id}
          rows={filteredRows}
          columns={
            subItem === 1
              ? generalColumn
              : subItem === 2
              ? productColumn
              : subItem === 3
              ? deliveryColumn
              : subItem === 4
              ? lawColumn
              : subItem === 5
              ? paymentColumn
              : subItem === 6
              ? pricingColumn
              : item === 7
              ? operationColumn
              : item === 8
              ? storageColumn
              : item === 9
              ? InvoiceColumn
              : item === 10
              ? accessColumn
              : item === 11
              ? permissionColumn
              : item === 12
              ? costsColumn
              : []
          }
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
          }}
          style={{ background: "white" }}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
            overflow: "auto",
            borderRadius: 0,
            maxWidth: "100vw",
            minHeight: "84vh",
          }}
          getRowHeight={() => "auto"}
          pageSizeOptions={[50]}
        />
      </Stack>
    </>
  );
}
