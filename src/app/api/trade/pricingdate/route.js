import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.trade_pricing_dates.create({
      data: {
        trade_id: +body.trade_id,
        price_date_type: body.price_date_type,
        multi_period_flag: body.multi_period_flag,
        date_trigger_flag: body.date_trigger_flag,
        pricing_date_memo: body.pricing_date_memo,
        fixed_begin_date: new Date(body.fixed_begin_date),
        fixed_end_date: new Date(body.fixed_end_date),
        contiguous_flag: body.contiguous_flag,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });
    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    await prisma.trade_pricing_dates.update({
      data: {
        price_date_type: body.price_date_type,
        multi_period_flag: body.multi_period_flag,
        date_trigger_flag: body.date_trigger_flag,
        pricing_date_memo: body.pricing_date_memo,
        fixed_begin_date: new Date(body.fixed_begin_date),
        fixed_end_date: new Date(body.fixed_end_date),
        contiguous_flag: body.contiguous_flag,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: {
        trade_pricing_dates_id: +body.trade_pricing_dates_id,
        status: "A",
      },
    });
    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
