import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_pricing_dates.findFirst({
      select: {
        trade_pricing_dates_id: true,
        price_date_type: true,
        multi_period_flag: true,
        date_trigger_flag: true,
        pricing_date_memo: true,
        fixed_begin_date: true,
        fixed_end_date: true,
        contiguous_flag: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
