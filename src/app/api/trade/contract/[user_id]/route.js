import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                select  
                    aa1.trade_id,
                    aa18.deal_id,
                    aa1.company_id,
                    aa8.name company_name,
                    aa1.counterparty_id,
                    aa6.name country_party_name,
                    aa1.trade_type,
                    aa16.name trade_type_name,
                    aa1.trade_date,
                    aa2.product_id,
                    aa3.product_name,
                    aa2.grade_id,
                    aa5.grade_name,                    
                    aa2.quantity,	
                    aa2.quantity_cm,	
										aa2.units,
                    aa20.name unit_name,
                    aa4.date_range_from date_range_from,
                    aa4.date_range_to date_range_to,										
                    aa2.est_bl_date,
                    aa1.trade_status,
                    COALESCE(aa6.name,'*') || '-' || COALESCE(aa1.trade_type,'*') || '-'||  COALESCE(aa3.product_name,'*') || ' [' || COALESCE(aa5.grade_name,'*') || '] - ['|| COALESCE(to_char(aa4.date_range_from,'Mon dd yyyy'),'*') || '-'|| COALESCE(to_char(aa4.date_range_to,'Mon dd yyyy'),'*') || ']-' || COALESCE(aa2.quantity,'0') || ' '||COALESCE(aa2.units,'0') as trade_name,
                    aa1.operator_sign_off_flag,
                    case when aa1.operator_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end operation_flag,
                    aa1.finance_sign_off_flag,
                    case when aa1.finance_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end finance_flag,
                    round(aa14.amount * aa2.quantity) || ' '|| aa14.currency price,
                    aa15.fixed_date pricing_date,
                    substring(aa7.lastname,1,1) || '.'|| aa7.firstname trader_name,
                    substring(aa13.lastname,1,1) || '.' || aa13.firstname contract_name,				
				            substring(aa11.lastname,1,1) || '.' || aa11.firstname operator_name,		
				            substring(aa12.lastname,1,1) || '.' || aa12.firstname finance_name,
                    aa17.contract_received_date,
                    aa17.response_sent_date,
                    aa17.contract_drafted_date,
                    aa17.contact_sent_date,
                    aa17.broker_contract_received_date,
                    aa17.contract_finish_flag,
                    COALESCE(aa19.total,0)  ::int total
                from trades aa1
                  left join trade_products aa2
                    on aa1.trade_id = aa2.trade_id
                    and aa2.status = 'A'
                  left  join products aa3
                    on aa2.product_id = aa3.product_id
                    and aa3.status = 'A'
                  left join trade_deliveries aa4
                    on aa1.trade_id = aa4.trade_id
                    and aa4.status = 'A'
                  left join product_grades aa5
                    on aa2.product_id = aa5.product_id
                    and aa2.grade_id = aa5.grade_id
                    and aa5.status = 'A'
                  left join legal_entity aa6
                    on aa1.counterparty_id = aa6.legal_entity_id
                    and aa6.status='A'
									left join bs_users aa7
										on aa1.trader_user_id = aa7.user_id
										and aa7.status = 'A'
                  left join bs_organizations aa8
				            on aa1.company_id = aa8.org_id
                    and aa8.status = 'A'
                  left join bs_users aa11
				            on aa1.operator_user_id = aa11.user_id		
				            and aa11.status = 'A'
			            left join bs_users aa12
				            on aa1.finance_user_id = aa12.user_id	
				            and aa12.status = 'A'
			            left join bs_users aa13
			              on aa1.contract_user_id = aa13.user_id	
				            and aa13.status = 'A'
                  left join trade_pricing_fixed	aa14
				            on aa1.trade_id = aa14.trade_id
                    and aa14.status ='A'
                  left join trade_payments aa15
                    on aa1.trade_id = aa15.trade_id
                    and aa15.status = 'A'
                  left join bs_lookups aa16
				            on aa1.trade_type = aa16.lookup_code
				            and aa16.lookup_type = 'Trade_type'
                    and aa16.status = 'A'
                  left join trade_laws aa17
							      on aa1.trade_id = aa17.trade_id
							      and aa17.status = 'A'
                  left join deal_trades aa18
							      on aa1.trade_id = aa18.trade_id
							      and aa18.status = 'A'	
                  left join (
                            select trade_id,count(trade_contract_id) total
                            from trade_contracts
                            where status = 'A'
                            group by trade_id) aa19
                    on aa1.trade_id = aa19.trade_id
                  left join bs_lookups aa20
				            on aa2.units = aa20.lookup_code
				            and aa20.lookup_type = 'Unit'
				            and aa20.status = 'A'
                where aa1.status = 'A' and aa1.company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
                order by aa1.trade_type,aa1.trade_date desc`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}