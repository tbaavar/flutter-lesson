import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
          select 		
				aa1.trade_id,
				'Current' as fields,
				aa19.name trade_type,
				to_char(aa1.trade_date,'YYYY-MM-DD') trade_date,
				aa18.name counter_party,
				aa17.name group_co,		
				aa20.name terms,		
				aa21.name trade_origin,
				aa1.delivery_method,				
				aa1.trade_status,
				aa16.firstname || ' ' || aa16.lastname trader_name,
				aa13.firstname || ' ' || aa13.lastname contract_name,				
				aa11.firstname || ' ' || aa11.lastname operator_name,		
				aa12.firstname || ' ' || aa12.lastname finance_name,
				aa9.product_id,
				aa14.product_name,		
				aa9.grade_id,
				aa15.grade_name,		
				aa9.quantity as quantity,
				'Fixed' pricing_type,		
				aa10.amount || ' MNT' as est_price,	
				aa10.total_amount || ' MNT' as est_amount,					
				aa26.name as quantity_to_base_pricing,
				'MNT'  as payable_in,
				to_char(aa3.fixed_date,'YYYY-MM-DD') as pricing_dates,
				aa22.port_name as load_port,
				aa23.port_name as dischage_port,
				aa24.name as vessel,
				aa4.laytime as laytime,
				aa4.nor as nor,
				aa4.pro_rata_cargo_flag as pro_rata_cargo_flag,
				aa5.general_term_condition as generaltc,
				aa4.first_purchaser_flag as first_purchaser_flag,
				aa4.tax_withheld_flag as tax_withheld_flag,
				aa4.dates_based_on,
				aa4.date_range_from || ' to ' ||aa4.date_range_to date_range,
				to_char(aa9.est_bl_date,'YYYY-MM-DD') est_bl_date,
				'' est_date,
				aa25.name payment,
				aa9.intention,
				aa2.memo
		from trades aa1	
			left join trade_memos aa2
				on aa1.trade_id = aa2.trade_id
				and aa2.status = 'A'
			left join trade_payments aa3
				on aa1.trade_id = aa3.trade_id
				and aa3.status = 'A'	
			left join trade_deliveries aa4
				on aa1.trade_id = aa4.trade_id
				and aa4.status = 'A'
			left join trade_laws aa5
				on aa1.trade_id = aa5.trade_id
				and aa5.status = 'A'			
			left join trade_products aa9
				on aa1.trade_id = aa9.trade_id
				and aa9.status = 'A'
			left join trade_pricing_fixed aa10
				on aa1.trade_id = aa10.trade_id
				and aa10.status = 'A'
			left join bs_users aa16
				on aa1.trader_user_id = aa16.user_id
				and aa16.status = 'A'
			left join bs_users aa11
				on aa1.operator_user_id = aa11.user_id		
				and aa11.status = 'A'
			left join bs_users aa12
				on aa1.finance_user_id = aa12.user_id	
				and aa12.status = 'A'
			left join bs_users aa13
				on aa1.contract_user_id = aa13.user_id	
				and aa13.status = 'A'
			left join products aa14
				on aa9.product_id = aa14.product_id
				and aa14.status = 'A'
			left join product_grades aa15
				on aa9.grade_id = aa15.grade_id
				and aa9.product_id = aa15.product_id
				and aa15.status = 'A'		
			left join bs_organizations aa17
				on aa1.company_id = aa17.org_id
				and aa17.status = 'A'
			left join legal_entity aa18
				on aa1.counterparty_id = aa18.legal_entity_id
				and aa18.status = 'A'	
			left join bs_lookups aa19
				on aa1.trade_type = aa19.lookup_code
				and aa19.lookup_type = 'Trade_type'
				and aa19.status = 'A'
			left join bs_lookups aa20
				on aa1.terms = aa20.lookup_code
				and aa20.lookup_type = 'Terms'
				and aa20.status = 'A'
			left join bs_lookups aa21
				on aa1.trade_origin = aa21.lookup_code
				and aa21.lookup_type = 'Trade_Origin'
				and aa21.status = 'A'
			left join ports aa22
				on aa4.load_port_id = aa22.port_id
				and aa22.status = 'A'
			left join ports aa23
				on aa4.discharge_port_id = aa23.port_id
				and aa23.status = 'A'
			left join bs_lookups aa24
				on aa4.intended_vessel_id = aa24.lookup_code
				and aa24.lookup_type = 'Intended_Vessel'
				and aa24.status = 'A'
			left join bs_lookups aa25
				on aa3.credit_type = aa25.lookup_code
				and aa25.lookup_type = 'Credit_Type'
				and aa25.status = 'A'
			left join bs_lookups aa26
				on aa4.quantity_based_on = aa26.lookup_code
				and aa26.lookup_type = 'Quantity_base'				
				and aa26.status = 'A'
		where aa1.trade_id = ${+params.trade_id}
			and aa1.status = 'A'
		union ALL
		select 		
				aa1.trade_id,
				'Preview' as fields,
				aa19.name trade_type,
				to_char(aa1.trade_date,'YYYY-MM-DD') trade_date,
				aa18.name counter_party,
				aa17.name group_co,		
				aa20.name terms,		
				aa21.name trade_origin,
				aa1.delivery_method,				
				aa1.trade_status,
				aa16.firstname || ' ' || aa16.lastname trader_name,
				aa13.firstname || ' ' || aa13.lastname contract_name,				
				aa11.firstname || ' ' || aa11.lastname operator_name,		
				aa12.firstname || ' ' || aa12.lastname finance_name,
				aa9.product_id,
				aa14.product_name,		
				aa9.grade_id,
				aa15.grade_name,		
				aa9.quantity as quantity,
				'Fixed' pricing_type,		
				aa10.amount || ' MNT' as est_price,		
				aa10.total_amount || ' MNT' as est_mount,		
				aa26.name as quantity_to_base_pricing,
				'MNT' as payable_in,								
				to_char(aa3.fixed_date,'YYYY-MM-DD') as pricing_dates,
				aa22.port_name as load_port,
				aa23.port_name as dischage_port,
				aa24.name as vessel,
				aa4.laytime as laytime,
				aa4.nor as nor,
				aa4.pro_rata_cargo_flag as pro_rata_cargo_flag,
				aa5.general_term_condition as generaltc,
				aa4.first_purchaser_flag as first_purchaser_flag,
				aa4.tax_withheld_flag as tax_withheld_flag,
				aa4.dates_based_on,
				aa4.date_range_from || ' to ' ||aa4.date_range_to date_range,
				to_char(aa9.est_bl_date,'YYYY-MM-DD') est_bl_date,
				'' est_date,				
				aa25.name payment,
				aa9.intention,
				aa2.memo
		from log_trades aa1	
			left join log_trade_memos aa2
				on aa1.trade_id = aa2.trade_id
				and aa2.latest_change_flag = 'Y'
				and aa2.status = 'A'
			left join log_trade_payments aa3
				on aa1.trade_id = aa3.trade_id
				and aa3.latest_change_flag = 'Y'
				and aa3.status = 'A'	
			left join log_trade_deliveries aa4
				on aa1.trade_id = aa4.trade_id
				and aa4.latest_change_flag = 'Y'
				and aa4.status = 'A'
			left join log_trade_laws aa5
				on aa1.trade_id = aa5.trade_id
				and aa5.latest_change_flag = 'Y'
				and aa5.status = 'A'			
			left join log_trade_products aa9
				on aa1.trade_id = aa9.trade_id
				and aa9.latest_change_flag = 'Y'
				and aa9.status = 'A'
			left join log_trade_pricing_fixed aa10
				on aa1.trade_id = aa10.trade_id
				and aa10.latest_change_flag = 'Y'
				and aa10.status = 'A'
			left join bs_users aa16
				on aa1.trader_user_id = aa16.user_id
				and aa16.status = 'A'
			left join bs_users aa11
				on aa1.operator_user_id = aa11.user_id		
				and aa11.status = 'A'
			left join bs_users aa12
				on aa1.finance_user_id = aa12.user_id	
				and aa12.status = 'A'
			left join bs_users aa13
			on aa1.contract_user_id = aa13.user_id	
				and aa13.status = 'A'
			left join products aa14
				on aa9.product_id = aa14.product_id
				and aa14.status = 'A'
			left join product_grades aa15
				on aa9.grade_id = aa15.grade_id
				and aa9.product_id = aa15.product_id
				and aa15.status = 'A'		
			left join bs_organizations aa17
				on aa1.company_id = aa17.org_id
				and aa17.status = 'A'
			left join legal_entity aa18
				on aa1.counterparty_id = aa18.legal_entity_id
				and aa18.status = 'A'	
			left join bs_lookups aa19
				on aa1.trade_type = aa19.lookup_code
				and aa19.lookup_type = 'Trade_type'
				and aa19.status = 'A'
			left join bs_lookups aa20
				on aa1.terms = aa20.lookup_code
				and aa20.lookup_type = 'Terms'
				and aa20.status = 'A'
			left join bs_lookups aa21
				on aa1.trade_origin = aa21.lookup_code
				and aa21.lookup_type = 'Trade_Origin'
				and aa21.status = 'A'
			left join ports aa22
				on aa4.load_port_id = aa22.port_id
				and aa22.status = 'A'
			left join ports aa23
				on aa4.discharge_port_id = aa23.port_id
				and aa23.status = 'A'
			left join bs_lookups aa24
				on aa4.intended_vessel_id = aa24.lookup_code
				and aa24.lookup_type = 'Intended_Vessel'
				and aa24.status = 'A'
			left join bs_lookups aa25
				on aa3.credit_type = aa25.lookup_code
				and aa25.lookup_type = 'Credit_Type'
			left join bs_lookups aa26
				on aa4.quantity_based_on = aa26.lookup_code
				and aa26.lookup_type = 'Quantity_base'
				and aa26.status = 'A'
		where aa1.trade_id = ${+params.trade_id}
			and aa1.latest_change_flag = 'Y'
			and aa1.status = 'A'`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
