import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
        select 	
          aa1.log_trade_fixed_pricing_id,
          aa1.trade_fixed_pricing_id,
          aa1.trade_id,
          aa1.unit,
          aa2.name unit_name,
          aa1.amount,
          aa1.total_amount,
          aa1.freight_currency,
          aa1.amount_other,
          aa1.total_amount_other,
          aa1.latest_change_flag,
          aa1.status,
          aa1.created_by,
          aa1.creation_date,
          aa1.last_updated_by,
          aa1.last_update_date
        from log_trade_pricing_fixed aa1
          left join bs_lookups aa2
            on aa1.unit = aa2.lookup_code
            and aa2.lookup_type = 'Unit'
            and aa2.status = 'A'
        order by aa1.trade_id asc,aa1.trade_fixed_pricing_id asc,aa1.last_update_date desc`;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
