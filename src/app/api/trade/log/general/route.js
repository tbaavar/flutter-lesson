import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
              select 
                aa1.log_trade_id,
                aa1.trade_id,
                aa1.trade_status,
                aa2.name company_name,
                aa3.name counterparty_name,
                aa4.name trade_type_name,
                aa1.trade_date,
                aa6.name trade_origin_name,
                aa1.terms,
                aa5.name trade_term_name,
                aa7.name deliver_method_name,
                case when aa1.operator_sign_off_flag='Y' then 'Checked' else 'UnChecked' end operator_sign_off_flag,
                case when aa1.finance_sign_off_flag='Y' then 'Checked' else 'UnChecked' end finance_sign_off_flag,
                substring(aa8.lastname,1,1) || '.'|| aa8.firstname trader_name,
                substring(aa9.lastname,1,1) || '.' || aa9.firstname operator_name,
                substring(aa10.lastname,1,1) || '.' || aa10.firstname finance_name,
                substring(aa11.lastname,1,1) || '.' || aa11.firstname contract_name,
                aa1.latest_change_flag,
                aa1.status,
                aa1.created_by,
                aa1.creation_date,
                aa1.last_updated_by,
                aa1.last_update_date
            from log_trades aa1
              left join bs_organizations aa2
                on aa1.company_id = aa2.org_id
                and aa2.status = 'A'
              left join legal_entity aa3
                on aa1.counterparty_id = aa3.legal_entity_id
                and aa3.status = 'A'
              left join bs_lookups aa4
                on aa1.trade_type = aa4.lookup_code
                and aa4.lookup_type = 'Trade_type'
              left join bs_lookups aa5
                on aa1.terms = aa5.lookup_code
                and aa5.lookup_type = 'Terms'
                and aa5.status = 'A'
              left join bs_lookups aa6
                on aa1.trade_origin = aa6.lookup_code
                and aa6.lookup_type = 'Trade_Origin'
                and aa6.status = 'A'
              left join bs_lookups aa7
                on aa1.delivery_method = aa7.lookup_code
                and aa7.lookup_type = 'Delivery_method'
                and aa7.status = 'A'
              left join bs_users aa8
                on aa1.trader_user_id = aa8.user_id
                and aa8.status = 'A'
              left join bs_users aa9
                on aa1.operator_user_id = aa9.user_id		
                and aa9.status = 'A'
              left join bs_users aa10
                on aa1.finance_user_id = aa10.user_id	
                and aa10.status = 'A'
              left join bs_users aa11
                on aa1.contract_user_id = aa11.user_id	
                and aa11.status = 'A'
            order by aa1.trade_id asc,last_update_date desc
          `;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
