import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {

    const response = await prisma.$queryRaw`
      select 	
        aa1.log_trade_payment_id,
        aa1.trade_payment_id,
        aa1.trade_id,
        case when aa1.prepaid_flag='Y' then 'Checked' else 'UnChecked' end prepaid_flag,
        aa2.name credit_type,
        aa3.name settlement_currency,
        aa1.day_1,
        aa1.time_condition_1,
        aa1.condition_1,
        aa1.condition_other_1,
        aa4.name transportation,
        aa5.name transportation_whichever,
        aa1.fixed_date,
        case when aa1.day_count_method = 'work' then 'Working Days'
              when aa1.day_count_method = 'calendar' then 'Calendar days'
              else null
      end day_count_method,
        aa6.name if_saturday_calc_method,
        aa7.name if_sunday_calc_method,
        aa8.name if_public_holiday_mon_calc_meth,
        aa9.name if_public_holiday_other_calc_me,
        aa10.name interest_rate_type,
        aa1.margin,
        aa11.name period,
        aa12.name payment_documentation,
        aa1.payment_note,
        aa1.latest_change_flag,
        aa1.status,
        aa1.created_by,
        aa1.creation_date,
        aa1.last_updated_by,
        aa1.last_update_date
      from log_trade_payments aa1
        left join bs_lookups aa2
            on aa1.credit_type = aa2.lookup_code
            and aa2.lookup_type = 'Credit_Type'
            and aa2.status = 'A'
        left join bs_lookups aa3
            on aa1.settlement_currency = aa3.lookup_code
            and aa3.lookup_type = 'Currency'
            and aa3.status = 'A'
          left join bs_lookups aa4
            on aa1.intended_vessel = aa4.lookup_code
            and aa4.lookup_type = 'Intended_Vessel'
            and aa4.status = 'A'
          left join bs_lookups aa5
            on aa1.intended_vessel_whichever = aa5.lookup_code
            and aa5.lookup_type = 'Unit'
            and aa5.status = 'A'
          left join bs_lookups aa6
            on aa1.if_saturday_calc_method= aa6.lookup_code
            and aa6.lookup_type = 'Day_Calc_Method'
            and aa6.status = 'A'
          left join bs_lookups aa7
            on aa1.if_sunday_calc_method= aa7.lookup_code
            and aa7.lookup_type = 'Day_Calc_Method'
            and aa7.status = 'A'
          left join bs_lookups aa8
            on aa1.if_public_holiday_mon_calc_meth = aa8.lookup_code
            and aa8.lookup_type = 'Day_Calc_Method'
            and aa8.status = 'A'
          left join bs_lookups aa9
            on aa1.if_public_holiday_other_calc_me = aa9.lookup_code
            and aa9.lookup_type = 'Day_Calc_Method'
            and aa9.status = 'A'
          left join bs_lookups aa10
            on aa1.interest_rate_type = aa10.lookup_code
            and aa10.lookup_type = 'Interest_Rate_Type'
            and aa10.status = 'A'
          left join bs_lookups aa11
            on aa1.period = aa11.lookup_code
            and aa11.lookup_type = 'Period'
            and aa11.status = 'A'
          left join bs_lookups aa12
            on aa1.period = aa12.lookup_code
            and aa12.lookup_type = 'Payment_documentation'
            and aa12.status = 'A'
      order by aa1.trade_id asc,aa1.trade_payment_id asc,aa1.last_update_date desc`;
    
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
