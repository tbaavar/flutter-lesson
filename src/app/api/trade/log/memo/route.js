import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {

    const response = await prisma.$queryRaw`
                select 	
                    aa1.log_trade_memo_id,
                    aa1.trade_memo_id,
                    aa1.trade_id,
                    aa1.memo,
                    aa1.latest_change_flag,
                    aa1.status,
                    aa1.created_by,
                    aa1.creation_date,
                    aa1.last_updated_by,
                    aa1.last_update_date
                from log_trade_memos aa1
                order by aa1.trade_id asc,aa1.trade_memo_id asc,aa1.last_update_date desc`;
                
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
