import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {

    const response = await prisma.$queryRaw`
            select 
              aa1.log_trade_law_id,
              aa1.trade_law_id,
              aa1.trade_id,
              aa1.notes,
              aa2.name conflict,
              aa3.name law_name,
              aa4.name title_to_pass,
              aa1.title_to_pass_other,
              aa5.name risk_to_pass,
              aa1.risk_to_pass_other,
              aa1.general_term_condition,
              aa1.quality_test_location,
              aa1.quality_test_other,
              aa1.contract_received_date,
              aa1.contact_sent_date,
              aa1.contract_drafted_date,
              aa1.response_sent_date,
              aa1.broker_contract_received_date,
              case when aa1.contract_finish_flag='Y' then 'Checked' else 'UnChecked' end contract_finish_flag,
              aa1.contract_date_comment,
              aa1.latest_change_flag,
              aa1.status,
              aa1.created_by,
              aa1.creation_date,
              aa1.last_updated_by,
              aa1.last_update_date
            from log_trade_laws aa1
              left join bs_lookups aa2
                  on aa1.conflict = aa2.lookup_code
                  and aa2.lookup_type = 'Law'
                  and aa2.status = 'A'
              left join bs_lookups aa3
                  on aa1.law_id = aa3.lookup_code
                  and aa3.lookup_type = 'Law'
                  and aa3.status = 'A'
              left join bs_lookups aa4
                  on aa1.title_to_pass = aa4.lookup_code
                  and aa4.lookup_type = 'Law_title'
                  and aa4.status = 'A'	
              left join bs_lookups aa5
                  on aa1.risk_to_pass = aa5.lookup_code
                  and aa5.lookup_type = 'Law_title'
                  and aa5.status = 'A'	
            order by aa1.trade_id asc,aa1.trade_law_id asc,aa1.last_update_date desc`;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
