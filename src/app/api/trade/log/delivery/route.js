import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {

    const response = await prisma.$queryRaw`
           select
              aa1.log_trade_delivery_id,
              aa1.trade_delivery_id,
              aa1.trade_id,
              aa2.port_name load_port_name,
              aa3.port_name discharge_port_name,
              aa4.name transportation,
              aa1.laytime,
              aa5.name dates_based_on,
              aa1.date_range_from,
              aa1.date_range_to,
              aa6.name quantity_based_on,
              aa1.demurrage_flag,
              aa7.name spot_demurrage_rate,
              aa1.spot_demurrage_rate_other,
              aa1.spot_demurrage_rate_max,
              aa8.name time_demurrage_rate,
              aa1.time_demurrage_rate_other,
              aa1.time_demurrage_rate_max,
              aa9.name notification_timebar,
              aa1.notification_from_date,
              aa1.notification_other,
              aa1.fulldoc_timebar,
              aa1.fulldoc_from_date,
              aa1.fulldoc_other,
              aa1.pdc_to_apply,
              aa1.latest_change_flag,
              aa1.status,
              aa1.created_by,
              aa1.creation_date,
              aa1.last_updated_by,
              aa1.last_update_date
          from log_trade_deliveries aa1
            left join ports aa2
              on aa1.load_port_id = aa2.port_id
              and aa2.status = 'A'
            left join ports aa3
              on aa1.discharge_port_id = aa3.port_id
              and aa3.status = 'A'
            left join bs_lookups aa4
              on aa1.intended_vessel_id = aa4.lookup_code
              and aa4.lookup_type = 'Intended_Vessel'
              and aa4.status = 'A'
            left join bs_lookups aa5
              on aa1.dates_based_on = aa5.lookup_code
              and aa5.lookup_type = 'Delivery_based_on'
              and aa5.status = 'A'
            left join bs_lookups aa6
              on aa1.quantity_based_on = aa6.lookup_code
              and aa6.lookup_type = 'Quantity_base'
              and aa6.status = 'A'
            left join bs_lookups aa7
              on aa1.spot_demurrage_rate = aa7.lookup_code
              and aa7.lookup_type = 'Demurrage_rate'
              and aa7.status = 'A'	
            left join bs_lookups aa8
              on aa1.time_demurrage_rate = aa8.lookup_code
              and aa8.lookup_type = 'Demurrage_rate'
              and aa8.status = 'A'		
            left join bs_lookups aa9
              on aa1.notification_timebar = aa9.lookup_code
              and aa9.lookup_type = 'Notification_timebar'
              and aa9.status = 'A'
          order by aa1.trade_id asc,aa1.trade_delivery_id asc,aa1.last_update_date desc     
          `;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
