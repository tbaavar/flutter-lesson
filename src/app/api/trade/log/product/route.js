import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {

    const response = await prisma.$queryRaw`
                select
                  aa1.log_trade_product_id,
                  aa1.trade_product_id,
                  aa1.trade_id,
								  case when aa1.ops_active_flag='Y' then 'Checked' else 'UnChecked' end ops_active_flag,
									case when aa1.finance_active_flag='Y' then 'Checked' else 'UnChecked' end finance_active_flag,
									case when aa1.storage_active_flag='Y' then 'Checked' else 'UnChecked' end storage_active_flag,
                  aa2.product_name,
                  aa3.grade_name,
                  aa4.name country_origin,
                  aa1.units,
                  aa5.name units_name,
									aa1.est_density,
                  aa1.quantity quantity_mt,
                  aa1.quantity_cm quantity_cm,
                  aa1.plus_minus,
                  aa1.est_bl_date,
                  aa6.name product_cover_type,
                  aa7.name product_cover_counterparty,
                  aa9.name product_option_quantity,
                  aa1.intention,
                  aa1.latest_change_flag,
                  aa1.status,
                  aa1.created_by,
                  aa1.creation_date,
                  aa1.last_updated_by,
                  aa1.last_update_date
                from log_trade_products aa1
                  left join products aa2
                    on aa1.product_id = aa2.product_id
                    and aa2.status = 'A'
                  left join product_grades aa3
                    on aa1.product_id = aa3.product_id
                    and aa1.grade_id = aa3.grade_id
                    and aa3.status = 'A'
                  left join bs_lookups aa4
                    on aa1.country_origin = aa4.lookup_code
                    and aa4.lookup_type = 'Country_Origin'
                    and aa4.status = 'A'
                  left join bs_lookups aa5
                    on aa1.units = aa5.lookup_code
                    and aa5.lookup_type = 'Unit'
                    and aa5.status = 'A'
                  left join bs_lookups aa6
                    on aa1.product_cover_type = aa6.lookup_code
                    and aa6.lookup_type = 'Cover_Type'
                    and aa6.status = 'A'
                  left join legal_entity aa7
                    on aa1.product_cover_counterparty = aa7.legal_entity_id
                    and aa7.status = 'A'
                  left join bs_lookups aa9
                    on aa1.product_option_quantity = aa9.lookup_code
                    and aa9.lookup_type = 'Responsible_Party'
                    and aa9.status = 'A'
                order by aa1.trade_id asc,aa1.trade_product_id asc,aa1.last_update_date desc
          `;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
