import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.$queryRaw`
                select  
                    aa1.trade_id,
                    aa18.deal_id,
                    aa1.company_id,
                    aa8.name company_name,
                    aa1.counterparty_id,
                    aa6.name country_party_name,
                    aa1.trade_type,
                    aa16.name trade_type_name,
                    aa1.trade_date,
                    aa2.product_id,
                    aa3.product_name,
                    aa2.grade_id,
                    aa5.grade_name,                    
                    aa2.quantity,	
										aa2.units,
                    aa4.date_range_from date_range_from,
                    aa4.date_range_to date_range_to,										
                    aa2.est_bl_date,
                    aa1.trade_status,
                    COALESCE(aa6.name,'*') || '-' || COALESCE(aa1.trade_type,'*') || '-'||  COALESCE(aa3.product_name,'*') || ' [' || COALESCE(aa5.grade_name,'*') || '] - ['|| COALESCE(to_char(aa4.date_range_from,'Mon dd yyyy'),'*') || '-'|| COALESCE(to_char(aa4.date_range_to,'Mon dd yyyy'),'*') || ']-' || COALESCE(aa2.quantity,'0') || ' '||COALESCE(aa2.units,'0') as trade_name,
                    aa1.operator_sign_off_flag,
                    case when aa1.operator_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end operation_flag,
                    aa1.finance_sign_off_flag,
                    case when aa1.finance_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end finance_flag,
                    round(aa14.amount * aa2.quantity) || ' '|| aa14.currency price,
                    aa15.fixed_date pricing_date,
                    substring(aa7.lastname,1,1) || '.'|| aa7.firstname trader_name,
                    substring(aa13.lastname,1,1) || '.' || aa13.firstname contract_name,				
				            substring(aa11.lastname,1,1) || '.' || aa11.firstname operator_name,		
				            substring(aa12.lastname,1,1) || '.' || aa12.firstname finance_name,
                    aa17.contract_received_date,
                    aa17.response_sent_date,
                    aa17.contract_drafted_date,
                    aa17.contact_sent_date,
                    aa17.broker_contract_received_date,
                    aa17.contract_finish_flag
                from trades aa1
                  left join trade_products aa2
                    on aa1.trade_id = aa2.trade_id
                    and aa2.status = 'A'
                  left  join products aa3
                    on aa2.product_id = aa3.product_id
                    and aa3.status = 'A'
                  left join trade_deliveries aa4
                    on aa1.trade_id = aa4.trade_id
                    and aa4.status = 'A'
                  left join product_grades aa5
                    on aa2.product_id = aa5.product_id
                    and aa2.grade_id = aa5.grade_id
                    and aa5.status = 'A'
                  left join legal_entity aa6
                    on aa1.counterparty_id = aa6.legal_entity_id
                    and aa6.status='A'
									left join bs_users aa7
										on aa1.trader_user_id = aa7.user_id
										and aa7.status = 'A'
                  left join bs_organizations aa8
				            on aa1.company_id = aa8.org_id
                    and aa8.status = 'A'
                  left join bs_users aa11
				            on aa1.operator_user_id = aa11.user_id		
				            and aa11.status = 'A'
			            left join bs_users aa12
				            on aa1.finance_user_id = aa12.user_id	
				            and aa12.status = 'A'
			            left join bs_users aa13
			              on aa1.contract_user_id = aa13.user_id	
				            and aa13.status = 'A'
                  left join (
                          select trade_id,sum(amount) amount,max(currency) currency,max(unit) per_unit
                          from trade_pricing_fixed						
                          where status = 'A'
                          group by trade_id
                              ) aa14
				            on aa1.trade_id = aa14.trade_id
                  left join trade_payments aa15
                    on aa1.trade_id = aa15.trade_id
                    and aa15.status = 'A'
                  left join bs_lookups aa16
				            on aa1.trade_type = aa16.lookup_code
				            and aa16.lookup_type = 'Trade_type'
                    and aa16.status = 'A'
                  left join trade_laws aa17
							      on aa1.trade_id = aa17.trade_id
							      and aa17.status = 'A'
                  left join deal_trades aa18
							      on aa1.trade_id = aa18.trade_id
							      and aa18.status = 'A'	
                where aa1.status = 'A'
                order by aa1.trade_type,aa1.trade_date desc`;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trades.create({
      data: {
        deal_id: +body.deal_id,
        company_id: +body.company_id,
        org_id: +body.org_id,
        counterparty_id: +body.counterparty_id,
        trade_type: body.trade_type,
        trade_date: new Date(body.trade_date),
        contract_type: body.contract_type,
        trade_origin: body.trade_origin,
        delivery_method: body.delivery_method,
        terms: body.terms,
        trader_user_id: +body.trader_user_id,
        operator_user_id: +body.operator_user_id,
        finance_user_id: +body.finance_user_id,
        contract_user_id: +body.contract_user_id,
        trade_status: body.trade_status,
        operator_sign_off_flag: body.operator_sign_off_flag,
        finance_sign_off_flag: body.finance_sign_off_flag,
        marketing_trade_flag: body.marketing_trade_flag,
        cancelled_flag: body.cancelled_flag,
        verified_flag: body.verified_flag,
        from_pluto_flag: body.from_pluto_flag,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    await prisma.log_trades.create({
      data: {
        trade_id: +old_value.trade_id,
        deal_id: +old_value.deal_id,
        company_id: +old_value.company_id,
        org_id: +old_value.org_id,
        counterparty_id: +old_value.counterparty_id,
        trade_type: old_value.trade_type,
        trade_date: new Date(old_value.trade_date),
        contract_type: old_value.contract_type,
        trade_origin: old_value.trade_origin,
        delivery_method: old_value.delivery_method,
        terms: old_value.terms,
        trader_user_id: +old_value.trader_user_id,
        operator_user_id: +old_value.operator_user_id,
        finance_user_id: +old_value.finance_user_id,
        contract_user_id: +old_value.contract_user_id,
        trade_status: old_value.trade_status,
        operator_sign_off_flag: old_value.operator_sign_off_flag,
        finance_sign_off_flag: old_value.finance_sign_off_flag,
        marketing_trade_flag: old_value.marketing_trade_flag,
        cancelled_flag: old_value.cancelled_flag,
        verified_flag: old_value.verified_flag,
        from_pluto_flag: old_value.from_pluto_flag,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date
      },
    });

    return tokenResponse(request, old_value);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trades.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });

    const old_log = await prisma.log_trades.findFirst({
      where: { trade_id: +body.trade_id, latest_change_flag: "Y", status: "A" },
    });

    if (old_log) {
      await prisma.log_trades.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_trade_id: +old_log.log_trade_id },
      });
    }

    await prisma.log_trades.create({
      data: {
        trade_id: +old_value.trade_id,
        deal_id: +old_value.deal_id,
        company_id: +old_value.company_id,
        org_id: +old_value.org_id,
        counterparty_id: +old_value.counterparty_id,
        trade_type: old_value.trade_type,
        trade_date: new Date(old_value.trade_date),
        contract_type: old_value.contract_type,
        trade_origin: old_value.trade_origin,
        delivery_method: old_value.delivery_method,
        terms: old_value.terms,
        trader_user_id: +old_value.trader_user_id,
        operator_user_id: +old_value.operator_user_id,
        finance_user_id: +old_value.finance_user_id,
        contract_user_id: +old_value.contract_user_id,
        trade_status: old_value.trade_status,
        operator_sign_off_flag: old_value.operator_sign_off_flag,
        finance_sign_off_flag: old_value.finance_sign_off_flag,
        marketing_trade_flag: old_value.marketing_trade_flag,
        cancelled_flag: old_value.cancelled_flag,
        verified_flag: old_value.verified_flag,
        from_pluto_flag: old_value.from_pluto_flag,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date
      },
    });

    // const to = await prisma.$queryRaw`
    //             select
    //                   aa2.mail to_mail
    //             from trades aa1
    //               left join bs_users aa2
    //                 on aa1.trader_user_id = aa2.user_id
    //                 and aa2.status = 'A'
    //             where aa1.trade_id = ${+body.trade_id}
    //               and aa1.status = 'A'
    //             union all
    //             select
    //                   aa3.mail to_mail
    //             from trades aa1
    //               left join bs_users aa3
    //                 on aa1.operator_user_id = aa3.user_id
    //                 and aa3.status = 'A'
    //             where aa1.trade_id = ${+body.trade_id}
    //                 and aa1.status = 'A'
    //             union all
    //             select
    //                   aa4.mail to_mail
    //             from trades aa1
    //               left join bs_users aa4
    //                 on aa1.finance_user_id = aa4.user_id
    //                 and aa4.status = 'A'
    //             where aa1.trade_id = ${+body.trade_id}
    //                 and aa1.status = 'A'
    //             union all
    //             select aa5.mail to_mail
    //             from trades aa1
    //               left join bs_users aa5
    //                 on aa1.contract_user_id = aa5.user_id
    //                 and aa5.status = 'A'
    //             where aa1.trade_id = ${+body.trade_id}
    //                 and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "General information өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    const response = await prisma.trades.update({
      data: {
        deal_id: +body.deal_id,
        company_id: +body.company_id,
        counterparty_id: +body.counterparty_id,
        trade_type: body.trade_type,
        trade_date: new Date(body.trade_date),
        contract_type: body.contract_type,
        trade_origin: body.trade_origin,
        delivery_method: body.delivery_method,
        terms: body.terms,
        trade_status: body.trade_status,
        trader_user_id: +body.trader_user_id,
        operator_user_id: +body.operator_user_id,
        finance_user_id: +body.finance_user_id,
        contract_user_id: +body.contract_user_id,
        operator_sign_off_flag: body.operator_sign_off_flag,
        finance_sign_off_flag: body.finance_sign_off_flag,
        trade_process_status: body.trade_process_status,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { trade_id: +body.trade_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
