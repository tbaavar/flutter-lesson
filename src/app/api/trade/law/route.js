import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();

  try {
    const isLaw = await prisma.trade_laws.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });

    if (!isLaw) {
      const old_value = await prisma.trade_laws.create({
        data: {
          trade_id: +body.trade_id,
          notes: body.notes,
          conflict: body.conflict,
          law_id: body.law_id,
          title_to_pass: body.title_to_pass,
          title_to_pass_other: body.title_to_pass_other,
          risk_to_pass: body.risk_to_pass,
          risk_to_pass_other: body.risk_to_pass_other,
          general_term_condition: body.general_term_condition,
          quality_test_location: +body.quality_test_location,
          quality_test_other: body.quality_test_other,
          quantity_test_port_id: body.quantity_test_port_id,
          quantity_test_other: body.quantity_test_other,
          q_q_timebar: body.q_q_timebar,
          q_q_from_date: body.q_q_from_date,
          q_q_other: body.q_q_other,
          claim_timebar: body.claim_timebar,
          claim_from_date: body.claim_from_date,
          claim_other: body.claim_other,
          inspection_share_port: +body.inspection_share_port,
          inspection_share_disport: +body.inspection_share_disport,
          contract_received_date: new Date(body.contract_received_date),
          response_sent_date: new Date(body.response_sent_date),
          contract_drafted_date: new Date(body.contract_drafted_date),
          contact_sent_date: new Date(body.contact_sent_date),
          broker_contract_received_date: new Date(
            body.broker_contract_received_date
          ),
          contract_finish_flag: body.contract_finish_flag,
          contract_date_comment: body.contract_date_comment,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      await prisma.log_trade_laws.create({
        data: {
          trade_law_id: +old_value.trade_law_id,
          trade_id: +old_value.trade_id,
          notes: old_value.notes,
          conflict: old_value.conflict,
          law_id: old_value.law_id,
          title_to_pass: old_value.title_to_pass,
          title_to_pass_other: old_value.title_to_pass_other,
          risk_to_pass: old_value.risk_to_pass,
          risk_to_pass_other: old_value.risk_to_pass_other,
          general_term_condition: old_value.general_term_condition,
          quality_test_location: +old_value.quality_test_location,
          quality_test_other: old_value.quality_test_other,
          quantity_test_port_id: old_value.quantity_test_port_id,
          quantity_test_other: old_value.quantity_test_other,
          q_q_timebar: old_value.q_q_timebar,
          q_q_from_date: old_value.q_q_from_date,
          q_q_other: old_value.q_q_other,
          claim_timebar: old_value.claim_timebar,
          claim_from_date: old_value.claim_from_date,
          claim_other: old_value.claim_other,
          inspection_share_port: +old_value.inspection_share_port,
          inspection_share_disport: +old_value.inspection_share_disport,
          contract_received_date: new Date(old_value.contract_received_date),
          response_sent_date: new Date(old_value.response_sent_date),
          contract_drafted_date: new Date(old_value.contract_drafted_date),
          contact_sent_date: new Date(old_value.contact_sent_date),
          broker_contract_received_date: new Date(
            old_value.broker_contract_received_date
          ),
          contract_finish_flag: old_value.contract_finish_flag,
          contract_date_comment: old_value.contract_date_comment,
          created_by: old_value.created_by,
          creation_date: old_value.creation_date,
          last_updated_by: old_value.last_updated_by,
          last_update_date: old_value.last_update_date,
        },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trade_laws.findFirst({
      where: { trade_law_id: +body.trade_law_id, status: "A" },
    });

    const old_log = await prisma.log_trade_laws.findFirst({
      where: {
        trade_law_id: +body.trade_law_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.trade_laws.update({
      data: {
        notes: body.notes,
        conflict: body.conflict,
        law_id: body.law_id,
        title_to_pass: body.title_to_pass,
        title_to_pass_other: body.title_to_pass_other,
        risk_to_pass: body.risk_to_pass,
        risk_to_pass_other: body.risk_to_pass_other,
        general_term_condition: body.general_term_condition,
        quality_test_location: body.quality_test_location,
        quality_test_other: body.quality_test_other,
        quantity_test_port_id: body.quantity_test_port_id,
        quantity_test_other: body.quantity_test_other,
        q_q_timebar: body.q_q_timebar,
        q_q_from_date: body.q_q_from_date,
        q_q_other: body.q_q_other,
        claim_timebar: body.claim_timebar,
        claim_from_date: body.claim_from_date,
        claim_other: body.claim_other,
        inspection_share_port: +body.inspection_share_port,
        inspection_share_disport: +body.inspection_share_disport,
        contract_received_date: new Date(body.contract_received_date),
        response_sent_date: new Date(body.response_sent_date),
        contract_drafted_date: new Date(body.contract_drafted_date),
        contact_sent_date: new Date(body.contact_sent_date),
        broker_contract_received_date: new Date(
          body.broker_contract_received_date
        ),
        contract_finish_flag: body.contract_finish_flag,
        contract_date_comment: body.contract_date_comment,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { trade_law_id: +body.trade_law_id, status: "A" },
    });

    if (old_log) {
      await prisma.log_trade_laws.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_trade_law_id: +old_log.log_trade_law_id },
      });
    }

    await prisma.log_trade_laws.create({
      data: {
        trade_law_id: +old_value.trade_law_id,
        trade_id: +old_value.trade_id,
        notes: old_value.notes,
        conflict: old_value.conflict,
        law_id: old_value.law_id,
        title_to_pass: old_value.title_to_pass,
        title_to_pass_other: old_value.title_to_pass_other,
        risk_to_pass: old_value.risk_to_pass,
        risk_to_pass_other: old_value.risk_to_pass_other,
        general_term_condition: old_value.general_term_condition,
        quality_test_location: +old_value.quality_test_location,
        quality_test_other: old_value.quality_test_other,
        quantity_test_port_id: old_value.quantity_test_port_id,
        quantity_test_other: old_value.quantity_test_other,
        q_q_timebar: old_value.q_q_timebar,
        q_q_from_date: old_value.q_q_from_date,
        q_q_other: old_value.q_q_other,
        claim_timebar: old_value.claim_timebar,
        claim_from_date: old_value.claim_from_date,
        claim_other: old_value.claim_other,
        inspection_share_port: +old_value.inspection_share_port,
        inspection_share_disport: +old_value.inspection_share_disport,
        contract_received_date: new Date(old_value.contract_received_date),
        response_sent_date: new Date(old_value.response_sent_date),
        contract_drafted_date: new Date(old_value.contract_drafted_date),
        contact_sent_date: new Date(old_value.contact_sent_date),
        broker_contract_received_date: new Date(
          old_value.broker_contract_received_date
        ),
        contract_finish_flag: old_value.contract_finish_flag,
        contract_date_comment: old_value.contract_date_comment,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Law өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
