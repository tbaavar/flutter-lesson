import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_laws.findFirst({
      select: {
        trade_law_id: true,
        notes: true,
        conflict: true,
        law_id: true,
        title_to_pass: true,
        title_to_pass_other: true,
        risk_to_pass: true,
        risk_to_pass_other: true,
        general_term_condition: true,
        quality_test_location: true,
        quality_test_other: true,
        quantity_test_port_id: true,
        quantity_test_other: true,
        q_q_timebar: true,
        q_q_from_date: true,
        q_q_other: true,
        claim_timebar: true,
        claim_from_date: true,
        claim_other: true,
        inspection_share_port: true,
        inspection_share_disport: true,
        contract_received_date: true,
        response_sent_date: true,
        contract_drafted_date: true,
        contact_sent_date: true,
        broker_contract_received_date: true,
        contract_finish_flag: true,
        contract_date_comment: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
