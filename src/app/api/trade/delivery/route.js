import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();

  try {
    const isDelivery = await prisma.trade_deliveries.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });

    if (!isDelivery) {
      const old_value = await prisma.trade_deliveries.create({
        data: {
          trade_id: +body.trade_id,
          load_port_id: +body.load_port_id,
          discharge_port_id: +body.discharge_port_id,
          intended_vessel_id: body.intended_vessel_id,
          laytime: +body.laytime,
          nor: +body.nor,
          pro_rata_cargo_flag: body.pro_rata_cargo_flag,
          dates_based_on: body.dates_based_on,
          date_range_from: new Date(body.date_range_from),
          date_range_to: new Date(body.date_range_to),
          quantity_based_on: body.quantity_based_on,
          narrowed_by: body.narrowed_by,
          narrowed_lay_day: +body.narrowed_lay_day,
          demurrage_flag: body.demurrage_flag,
          pro_rata_per_day_flag: body.pro_rata_per_day_flag,
          spot_demurrage_rate: body.spot_demurrage_rate,
          spot_demurrage_rate_other: body.spot_demurrage_rate_other,
          spot_demurrage_rate_max: +body.spot_demurrage_rate_max,
          time_demurrage_rate: body.time_demurrage_rate,
          time_demurrage_rate_other: body.time_demurrage_rate_other,
          time_demurrage_rate_max: +body.time_demurrage_rate_max,
          notification_timebar: body.notification_timebar,
          notification_from_date: body.notification_from_date,
          notification_other: body.notification_other,
          fulldoc_timebar: body.fulldoc_timebar,
          fulldoc_from_date: body.fulldoc_from_date,
          fulldoc_other: body.fulldoc_other,
          pdc_to_apply: body.pdc_to_apply,
          claims_person_id: +body.claims_person_id,
          duty_importer_record: body.duty_importer_record,
          duty_exporter_record: body.duty_exporter_record,
          first_purchaser_flag: body.first_purchaser_flag,
          tax_withheld_flag: body.tax_withheld_flag,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      await prisma.log_trade_deliveries.create({
        data: {
          trade_id: +old_value.trade_id,
          trade_delivery_id: +old_value.trade_delivery_id,
          load_port_id: +old_value.load_port_id,
          discharge_port_id: +old_value.discharge_port_id,
          intended_vessel_id: old_value.intended_vessel_id,
          laytime: +old_value.laytime,
          nor: +old_value.nor,
          pro_rata_cargo_flag: old_value.pro_rata_cargo_flag,
          dates_based_on: old_value.dates_based_on,
          date_range_from: new Date(old_value.date_range_from),
          date_range_to: new Date(old_value.date_range_to),
          quantity_based_on: old_value.quantity_based_on,
          narrowed_by: old_value.narrowed_by,
          narrowed_lay_day: +old_value.narrowed_lay_day,
          demurrage_flag: old_value.demurrage_flag,
          pro_rata_per_day_flag: old_value.pro_rata_per_day_flag,
          spot_demurrage_rate: old_value.spot_demurrage_rate,
          spot_demurrage_rate_other: old_value.spot_demurrage_rate_other,
          spot_demurrage_rate_max: +old_value.spot_demurrage_rate_max,
          time_demurrage_rate: old_value.time_demurrage_rate,
          time_demurrage_rate_other: old_value.time_demurrage_rate_other,
          time_demurrage_rate_max: +old_value.time_demurrage_rate_max,
          notification_timebar: old_value.notification_timebar,
          notification_from_date: old_value.notification_from_date,
          notification_other: old_value.notification_other,
          fulldoc_timebar: old_value.fulldoc_timebar,
          fulldoc_from_date: old_value.fulldoc_from_date,
          fulldoc_other: old_value.fulldoc_other,
          pdc_to_apply: old_value.pdc_to_apply,
          claims_person_id: +old_value.claims_person_id,
          duty_importer_record: old_value.duty_importer_record,
          duty_exporter_record: old_value.duty_exporter_record,
          first_purchaser_flag: old_value.first_purchaser_flag,
          tax_withheld_flag: old_value.tax_withheld_flag,
          created_by: old_value.created_by,
          creation_date: old_value.creation_date,
          last_updated_by: old_value.last_updated_by,
          last_update_date: old_value.last_update_date,
        },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trade_deliveries.findFirst({
      where: { trade_delivery_id: +body.trade_delivery_id, status: "A" },
    });

    const old_log = await prisma.log_trade_deliveries.findFirst({
      where: {
        trade_delivery_id: +body.trade_delivery_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.trade_deliveries.update({
      data: {
        load_port_id: +body.load_port_id,
        discharge_port_id: +body.discharge_port_id,
        intended_vessel_id: body.intended_vessel_id,
        laytime: +body.laytime,
        nor: +body.nor,
        pro_rata_cargo_flag: body.pro_rata_cargo_flag,
        dates_based_on: body.dates_based_on,
        date_range_from: new Date(body.date_range_from),
        date_range_to: new Date(body.date_range_to),
        quantity_based_on: body.quantity_based_on,
        narrowed_by: body.narrowed_by,
        narrowed_lay_day: +body.narrowed_lay_day,
        demurrage_flag: body.demurrage_flag,
        pro_rata_per_day_flag: body.pro_rata_per_day_flag,
        spot_demurrage_rate: body.spot_demurrage_rate,
        spot_demurrage_rate_other: body.spot_demurrage_rate_other,
        spot_demurrage_rate_max: +body.spot_demurrage_rate_max,
        time_demurrage_rate: body.time_demurrage_rate,
        time_demurrage_rate_other: body.time_demurrage_rate_other,
        time_demurrage_rate_max: +body.time_demurrage_rate_max,
        notification_timebar: body.notification_timebar,
        notification_from_date: body.notification_from_date,
        notification_other: body.notification_other,
        fulldoc_timebar: body.fulldoc_timebar,
        fulldoc_from_date: body.fulldoc_from_date,
        fulldoc_other: body.fulldoc_other,
        pdc_to_apply: body.pdc_to_apply,
        claims_person_id: +body.claims_person_id,
        duty_importer_record: body.duty_importer_record,
        duty_exporter_record: body.duty_exporter_record,
        first_purchaser_flag: body.first_purchaser_flag,
        tax_withheld_flag: body.tax_withheld_flag,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { trade_delivery_id: +body.trade_delivery_id, status: "A" },
    });

    if (old_log) {
      await prisma.log_trade_deliveries.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_trade_delivery_id: +old_log.log_trade_delivery_id },
      });
    }

    await prisma.log_trade_deliveries.create({
      data: {
        trade_delivery_id: +old_value.trade_delivery_id,
        trade_id: +old_value.trade_id,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        intended_vessel_id: old_value.intended_vessel_id,
        laytime: +old_value.laytime,
        nor: +old_value.nor,
        pro_rata_cargo_flag: old_value.pro_rata_cargo_flag,
        dates_based_on: old_value.dates_based_on,
        date_range_from: new Date(old_value.date_range_from),
        date_range_to: new Date(old_value.date_range_to),
        quantity_based_on: old_value.quantity_based_on,
        narrowed_by: old_value.narrowed_by,
        narrowed_lay_day: +old_value.narrowed_lay_day,
        demurrage_flag: old_value.demurrage_flag,
        pro_rata_per_day_flag: old_value.pro_rata_per_day_flag,
        spot_demurrage_rate: old_value.spot_demurrage_rate,
        spot_demurrage_rate_other: old_value.spot_demurrage_rate_other,
        spot_demurrage_rate_max: +old_value.spot_demurrage_rate_max,
        time_demurrage_rate: old_value.time_demurrage_rate,
        time_demurrage_rate_other: old_value.time_demurrage_rate_other,
        time_demurrage_rate_max: +old_value.time_demurrage_rate_max,
        notification_timebar: old_value.notification_timebar,
        notification_from_date: old_value.notification_from_date,
        notification_other: old_value.notification_other,
        fulldoc_timebar: old_value.fulldoc_timebar,
        fulldoc_from_date: old_value.fulldoc_from_date,
        fulldoc_other: old_value.fulldoc_other,
        pdc_to_apply: old_value.pdc_to_apply,
        claims_person_id: +old_value.claims_person_id,
        duty_importer_record: old_value.duty_importer_record,
        duty_exporter_record: old_value.duty_exporter_record,
        first_purchaser_flag: old_value.first_purchaser_flag,
        tax_withheld_flag: old_value.tax_withheld_flag,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Delivery өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
