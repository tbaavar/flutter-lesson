import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();

  try {
    const isPayment = await prisma.trade_payments.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });

    if (!isPayment) {
      const old_value = await prisma.trade_payments.create({
        data: {
          trade_id: +body.trade_id,
          prepaid_flag: body.prepaid_flag,
          credit_type: body.credit_type,
          settlement_currency: body.settlement_currency,
          risk_mngt_approved_flag: body.risk_mngt_approved_flag,
          not_agreed_credit_given_flag: body.not_agreed_credit_given_flag,
          ger_excluded_flag: body.ger_excluded_flag,
          day_1: body.day_1,
          time_condition_1: body.time_condition_1,
          condition_1: body.condition_1,
          condition_other_1: body.condition_other_1,
          day_2: body.day_2,
          time_condition_2: body.time_condition_2,
          condition_2: body.condition_2,
          condition_other_2: body.condition_other_2,
          intended_vessel: body.intended_vessel,
          intended_vessel_whichever: body.intended_vessel_whichever,
          fixed_date: new Date(body.fixed_date),
          day_count_method: body.day_count_method,
          prefered_pricing_parcels: +body.prefered_pricing_parcels,
          same_as_pricing_flag: body.same_as_pricing_flag,
          if_saturday_calc_method: body.if_saturday_calc_method,
          if_sunday_calc_method: body.if_sunday_calc_method,
          if_public_holiday_mon_calc_meth: body.if_public_holiday_mon_calc_meth,
          if_public_holiday_other_calc_me: body.if_public_holiday_other_calc_me,
          interest_rate_type: body.interest_rate_type,
          margin: +body.margin,
          period: body.period,
          bank_id: +body.bank_id,
          credit_spread_rate: +body.credit_spread_rate,
          to_be_invoiced_flag: body.to_be_invoiced_flag,
          payment_documentation: body.payment_documentation,
          payment_note: body.payment_note,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      await prisma.log_trade_payments.create({
        data: {
          trade_payment_id: +old_value.trade_payment_id,
          trade_id: +old_value.trade_id,
          prepaid_flag: old_value.prepaid_flag,
          credit_type: old_value.credit_type,
          settlement_currency: old_value.settlement_currency,
          risk_mngt_approved_flag: old_value.risk_mngt_approved_flag,
          not_agreed_credit_given_flag: old_value.not_agreed_credit_given_flag,
          ger_excluded_flag: old_value.ger_excluded_flag,
          day_1: old_value.day_1,
          time_condition_1: old_value.time_condition_1,
          condition_1: old_value.condition_1,
          condition_other_1: old_value.condition_other_1,
          day_2: old_value.day_2,
          time_condition_2: old_value.time_condition_2,
          condition_2: old_value.condition_2,
          condition_other_2: old_value.condition_other_2,
          intended_vessel: old_value.intended_vessel,
          intended_vessel_whichever: old_value.intended_vessel_whichever,
          fixed_date: new Date(old_value.fixed_date),
          day_count_method: old_value.day_count_method,
          prefered_pricing_parcels: +old_value.prefered_pricing_parcels,
          same_as_pricing_flag: old_value.same_as_pricing_flag,
          if_saturday_calc_method: old_value.if_saturday_calc_method,
          if_sunday_calc_method: old_value.if_sunday_calc_method,
          if_public_holiday_mon_calc_meth:
            old_value.if_public_holiday_mon_calc_meth,
          if_public_holiday_other_calc_me:
            old_value.if_public_holiday_other_calc_me,
          interest_rate_type: old_value.interest_rate_type,
          margin: +old_value.margin,
          period: old_value.period,
          bank_id: +old_value.bank_id,
          credit_spread_rate: +old_value.credit_spread_rate,
          to_be_invoiced_flag: old_value.to_be_invoiced_flag,
          payment_documentation: old_value.payment_documentation,
          payment_note: old_value.payment_note,
          created_by: old_value.created_by,
          creation_date: old_value.creation_date,
          last_updated_by: old_value.last_updated_by,
          last_update_date: old_value.last_update_date,
        },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trade_payments.findFirst({
      where: { trade_payment_id: +body.trade_payment_id, status: "A" },
    });

    const old_log = await prisma.log_trade_payments.findFirst({
      where: {
        trade_payment_id: +body.trade_payment_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.trade_payments.update({
      data: {
        prepaid_flag: body.prepaid_flag,
        credit_type: body.credit_type,
        settlement_currency: body.settlement_currency,
        risk_mngt_approved_flag: body.risk_mngt_approved_flag,
        not_agreed_credit_given_flag: body.not_agreed_credit_given_flag,
        ger_excluded_flag: body.ger_excluded_flag,
        day_1: body.day_1,
        time_condition_1: body.time_condition_1,
        condition_1: body.condition_1,
        condition_other_1: body.condition_other_1,
        day_2: body.day_2,
        time_condition_2: body.time_condition_2,
        condition_2: body.condition_2,
        condition_other_2: body.condition_other_2,
        intended_vessel: body.intended_vessel,
        intended_vessel_whichever: body.intended_vessel_whichever,
        fixed_date: body.fixed_date,
        day_count_method: body.day_count_method,
        prefered_pricing_parcels: +body.prefered_pricing_parcels,
        same_as_pricing_flag: body.same_as_pricing_flag,
        if_saturday_calc_method: body.if_saturday_calc_method,
        if_sunday_calc_method: body.if_sunday_calc_method,
        if_public_holiday_mon_calc_meth: body.if_public_holiday_mon_calc_meth,
        if_public_holiday_other_calc_me: body.if_public_holiday_other_calc_me,
        interest_rate_type: body.interest_rate_type,
        margin: +body.margin,
        period: body.period,
        bank_id: +body.bank_id,
        credit_spread_rate: +body.credit_spread_rate,
        to_be_invoiced_flag: body.to_be_invoiced_flag,
        payment_documentation: body.payment_documentation,
        payment_note: body.payment_note,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { trade_payment_id: +body.trade_payment_id, status: "A" },
    });

    if (old_log) {
      await prisma.log_trade_payments.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_trade_payment_id: +old_log.log_trade_payment_id },
      });
    }

    await prisma.log_trade_payments.create({
      data: {
        trade_payment_id: +old_value.trade_payment_id,
        trade_id: +old_value.trade_id,
        prepaid_flag: old_value.prepaid_flag,
        credit_type: old_value.credit_type,
        settlement_currency: old_value.settlement_currency,
        risk_mngt_approved_flag: old_value.risk_mngt_approved_flag,
        not_agreed_credit_given_flag: old_value.not_agreed_credit_given_flag,
        ger_excluded_flag: old_value.ger_excluded_flag,
        day_1: old_value.day_1,
        time_condition_1: old_value.time_condition_1,
        condition_1: old_value.condition_1,
        condition_other_1: old_value.condition_other_1,
        day_2: old_value.day_2,
        time_condition_2: old_value.time_condition_2,
        condition_2: old_value.condition_2,
        condition_other_2: old_value.condition_other_2,
        intended_vessel: old_value.intended_vessel,
        intended_vessel_whichever: old_value.intended_vessel_whichever,
        fixed_date: new Date(old_value.fixed_date),
        day_count_method: old_value.day_count_method,
        prefered_pricing_parcels: +old_value.prefered_pricing_parcels,
        same_as_pricing_flag: old_value.same_as_pricing_flag,
        if_saturday_calc_method: old_value.if_saturday_calc_method,
        if_sunday_calc_method: old_value.if_sunday_calc_method,
        if_public_holiday_mon_calc_meth:
          old_value.if_public_holiday_mon_calc_meth,
        if_public_holiday_other_calc_me:
          old_value.if_public_holiday_other_calc_me,
        interest_rate_type: old_value.interest_rate_type,
        margin: +old_value.margin,
        period: old_value.period,
        bank_id: +old_value.bank_id,
        credit_spread_rate: +old_value.credit_spread_rate,
        to_be_invoiced_flag: old_value.to_be_invoiced_flag,
        payment_documentation: old_value.payment_documentation,
        payment_note: old_value.payment_note,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Payment өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
