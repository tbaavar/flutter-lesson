import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_payments.findFirst({
      select: {
        trade_payment_id: true,
        prepaid_flag: true,
        credit_type: true,
        settlement_currency: true,
        risk_mngt_approved_flag: true,
        not_agreed_credit_given_flag: true,
        ger_excluded_flag: true,
        day_1: true,
        time_condition_1: true,
        condition_1: true,
        condition_other_1: true,
        day_2: true,
        time_condition_2: true,
        condition_2: true,
        condition_other_2: true,
        intended_vessel: true,
        intended_vessel_whichever: true,
        fixed_date: true,
        day_count_method: true,
        prefered_pricing_parcels: true,
        same_as_pricing_flag: true,
        if_saturday_calc_method: true,
        if_sunday_calc_method: true,
        if_public_holiday_mon_calc_meth: true,
        if_public_holiday_other_calc_me: true,
        interest_rate_type: true,
        margin: true,
        period: true,
        bank_id: true,
        credit_spread_rate: true,
        to_be_invoiced_flag: true,
        payment_documentation: true,
        payment_note: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
