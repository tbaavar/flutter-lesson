import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trades.findUnique({
      select: {
        trade_id: true,
        deal_id: true,
        company_id: true,
        counterparty_id: true,
        trade_type: true,
        trade_date: true,
        contract_type: true,
        trade_origin: true,
        delivery_method: true,
        terms: true,
        trader_user_id: true,
        operator_user_id: true,
        finance_user_id: true,
        contract_user_id: true,
        trade_status: true,
        operator_sign_off_flag: true,
        finance_sign_off_flag: true,
        marketing_trade_flag: true,
        cancelled_flag: true,
        verified_flag: true,
        from_pluto_flag: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
