import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();

  try {
    const isMemo = await prisma.trade_memos.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });
    if (!isMemo) {
      const old_value = await prisma.trade_memos.create({
        data: {
          trade_id: +body.trade_id,
          memo: body.memo,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      await prisma.log_trade_memos.create({
        data: {
          trade_memo_id: +old_value.trade_memo_id,
          trade_id: +old_value.trade_id,
          memo: old_value.memo,
          created_by: old_value.created_by,
          creation_date: old_value.creation_date,
          last_updated_by: old_value.last_updated_by,
          last_update_date: old_value.last_update_date,
        },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trade_memos.findFirst({
      where: { trade_memo_id: +body.trade_memo_id, status: "A" },
    });

    const old_log = await prisma.log_trade_memos.findFirst({
      where: {
        trade_memo_id: +body.trade_memo_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.trade_memos.update({
      data: {
        memo: body.memo,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { trade_memo_id: +body.trade_memo_id, status: "A" },
    });

    if (old_log) {
      await prisma.log_trade_memos.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_trade_memo_id: +old_log.log_trade_memo_id },
      });
    }

    await prisma.log_trade_memos.create({
      data: {
        trade_memo_id: +old_value.trade_memo_id,
        trade_id: +old_value.trade_id,
        memo: old_value.memo,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Memo өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
