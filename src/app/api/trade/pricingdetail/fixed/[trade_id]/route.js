import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_pricing_fixed.findFirst({
      select: {
        trade_fixed_pricing_id: true,
        amount: true,
        currency: true,
        unit: true,
        cent_flag: true,
        freight_cost: true,
        freight_currency: true,
        freight_unit: true,
        freight_cent_flag: true,
        amount_other: true,
        total_amount: true,
        total_amount_other: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
