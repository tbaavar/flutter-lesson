import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();

  try {
    const IsPrice = await prisma.trade_pricing_fixed.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });

    if (!IsPrice) {
      const old_value = await prisma.trade_pricing_fixed.create({
        data: {
          trade_id: +body.trade_id,
          unit: body.unit,
          amount: +body.amount,
          total_amount: +body.total_amount,
          currency: body.currency,
          amount_other: +body.amount_other,
          total_amount_other: +body.total_amount_other,
          cent_flag: body.cent_flag,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      await prisma.log_trade_pricing_fixed.create({
        data: {
          trade_fixed_pricing_id: +old_value.trade_fixed_pricing_id,
          trade_id: +old_value.trade_id,
          unit: old_value.unit,
          amount: +old_value.amount,
          total_amount: +old_value.total_amount,
          currency: old_value.currency,
          amount_other: +old_value.amount_other,
          total_amount_other: +body.total_amount_other,
          cent_flag: old_value.cent_flag,
          freight_cost: +old_value.freight_cost,
          freight_currency: old_value.freight_currency,
          freight_unit: old_value.freight_unit,
          freight_cent_flag: old_value.freight_cent_flag,
          created_by: old_value.created_by,
          creation_date: old_value.creation_date,
          last_updated_by: old_value.last_updated_by,
          last_update_date: old_value.last_update_date,
        },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    const old_value = await prisma.trade_pricing_fixed.findFirst({
      where: {
        trade_fixed_pricing_id: +body.trade_fixed_pricing_id,
        status: "A",
      },
    });

    const old_log = await prisma.log_trade_pricing_fixed.findFirst({
      where: {
        trade_fixed_pricing_id: +body.trade_fixed_pricing_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.trade_pricing_fixed.update({
      data: {
        unit: body.unit,
        amount: +body.amount,
        total_amount: +body.total_amount,
        currency: body.currency,
        amount_other: +body.amount_other,
        total_amount_other: +body.total_amount_other,
        cent_flag: body.cent_flag,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: {
        trade_fixed_pricing_id: +body.trade_fixed_pricing_id,
        status: "A",
      },
    });

    if (old_log) {
      await prisma.log_trade_pricing_fixed.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: {
          log_trade_fixed_pricing_id: +old_log.log_trade_fixed_pricing_id,
        },
      });
    }

    await prisma.log_trade_pricing_fixed.create({
      data: {
        trade_fixed_pricing_id: +old_value.trade_fixed_pricing_id,
        trade_id: +old_value.trade_id,
        unit: old_value.unit,
        amount: +old_value.amount,
        total_amount: +old_value.total_amount,
        currency: old_value.currency,
        amount_other: +old_value.amount_other,
        total_amount_other: +body.total_amount_other,
        cent_flag: old_value.cent_flag,
        freight_cost: +old_value.freight_cost,
        freight_currency: old_value.freight_currency,
        freight_unit: old_value.freight_unit,
        freight_cent_flag: old_value.freight_cent_flag,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Pricing өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    await prisma.trade_pricing_fixed.update({
      data: {
        status: "I",
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: {
        trade_fixed_pricing_id: +body.trade_fixed_pricing_id,
        status: "A",
      },
    });
    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
