import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import { Trykker } from "next/font/google";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_pricing.findFirst({
      select: {
        trade_pricing_id: true,
        pricing_type: true,
        buyer_seller_option: true,
        declaration_date: true,
        declaration_done_flag: true,
        freight_cost: true,
        freight_currency: true,
        freight_unit: true,
        freight_cent_flag: true,
        other_price_description: true,
        other_currency: true,
        other_est_price: true,
        other_est_unit: true,
        other_cent_flag: true,
        efp_price: true,
        efp_currency: true,
        efp_est_unit: true,
        efp_cent_flag: true,
        duty_amount: true,
        duty_currency: true,
        duty_unit: true,
        duty_cent_flag: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
