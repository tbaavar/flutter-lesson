import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.trade_pricing.create({
      data: {
        trade_id: +body.trade_id,
        pricing_type: body.pricing_type,
        buyer_seller_option: body.buyer_seller_option,
        declaration_date: new Date(body.declaration_date),
        declaration_done_flag: body.declaration_done_flag,
        freight_cost: +body.freight_cost,
        freight_currency: body.freight_currency,
        freight_unit: body.freight_unit,
        freight_cent_flag: body.freight_cent_flag,
        other_price_description: body.other_price_description,
        other_currency: body.other_currency,
        other_est_price: +body.other_est_price,
        other_est_unit: body.other_est_unit,
        other_cent_flag: body.other_cent_flag,
        efp_price: +body.efp_price,
        efp_currency: body.efp_currency,
        efp_est_unit: body.efp_est_unit,
        efp_cent_flag: body.efp_cent_flag,
        duty_amount: +body.duty_amount,
        duty_currency: body.duty_currency,
        duty_unit: body.duty_unit,
        duty_cent_flag: body.duty_cent_flag,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });
    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    await prisma.trade_pricing.update({
      data: {
        pricing_type: body.pricing_type,
        buyer_seller_option: body.buyer_seller_option,
        declaration_date: new Date(body.declaration_date),
        declaration_done_flag: body.declaration_done_flag,
        freight_cost: +body.freight_cost,
        freight_currency: body.freight_currency,
        freight_unit: body.freight_unit,
        freight_cent_flag: body.freight_cent_flag,
        other_price_description: body.other_price_description,
        other_currency: body.other_currency,
        other_est_price: +body.other_est_price,
        other_est_unit: body.other_est_unit,
        other_cent_flag: body.other_cent_flag,
        efp_price: +body.efp_price,
        efp_currency: body.efp_currency,
        efp_est_unit: body.efp_est_unit,
        efp_cent_flag: body.efp_cent_flag,
        duty_amount: +body.duty_amount,
        duty_currency: body.duty_currency,
        duty_unit: body.duty_unit,
        duty_cent_flag: body.duty_cent_flag,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: {
        trade_pricing_id: +body.trade_pricing_id,
        status: "A",
      },
    });
    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
