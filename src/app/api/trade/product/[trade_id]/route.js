import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import Decimal from "decimal.js";

export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_products.findFirst({
      where: { trade_id: +params.trade_id, status: "A" },
    });

  

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
