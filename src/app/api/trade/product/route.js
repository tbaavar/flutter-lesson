import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";

dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const body = await request.json();

  try {
    const isProduct = await prisma.trade_products.findFirst({
      where: { trade_id: +body.trade_id, status: "A" },
    });

    if (!isProduct) {
      const old_value = await prisma.trade_products.create({
        data: {
          geographical_area: body.geographical_area,
          trade_id: +body.trade_id,
          grade_id: +body.grade_id,
          product_id: +body.product_id,
          country_origin: body.country_origin,
          units: body.units,
          est_density: +body.est_density,
          trade_treatment: body.trade_treatment,
          quantity: +body.quantity,
          quantity_cm: +body.quantity_cm,
          plus_minus: body.plus_minus,
          min_quantity: +body.min_quantity,
          max_quantity: +body.max_quantity,
          product_cover_type: body.product_cover_type,
          product_cover_counterparty: +body.product_cover_counterparty,
          product_option_quantity: body.product_option_quantity,
          intention: body.intention,
          ops_active_flag: body.ops_active_flag,
          storage_active_flag: body.storage_active_flag,
          finance_active_flag: body.finance_active_flag,
          consolidate_deal_flag: body.consolidate_deal_flag,
          financing_behalf_flag: body.financing_behalf_flag,
          est_bl_date: new Date(body.est_bl_date),
          fixed_flag: body.fixed_flag,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      await prisma.log_trade_products.create({
        data: {
          trade_product_id: +old_value.trade_product_id,
          geographical_area: old_value.geographical_area,
          trade_id: +old_value.trade_id,
          grade_id: +old_value.grade_id,
          product_id: +old_value.product_id,
          country_origin: old_value.country_origin,
          units: old_value.units,
          est_density: +old_value.est_density,
          trade_treatment: old_value.trade_treatment,
          quantity: +old_value.quantity,
          quantity_cm: +old_value.quantity_cm,
          plus_minus: old_value.plus_minus,
          min_quantity: +old_value.min_quantity,
          max_quantity: +old_value.max_quantity,
          product_cover_type: old_value.product_cover_type,
          product_cover_counterparty: +old_value.product_cover_counterparty,
          product_option_quantity: old_value.product_option_quantity,
          intention: old_value.intention,
          ops_active_flag: old_value.ops_active_flag,
          storage_active_flag: old_value.storage_active_flag,
          finance_active_flag: old_value.finance_active_flag,
          consolidate_deal_flag: old_value.consolidate_deal_flag,
          financing_behalf_flag: old_value.financing_behalf_flag,
          est_bl_date: new Date(old_value.est_bl_date),
          fixed_flag: old_value.fixed_flag,
          created_by: old_value.created_by,
          creation_date: old_value.creation_date,
          last_updated_by: old_value.last_updated_by,
          last_update_date: old_value.last_update_date,
        },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.trade_products.findFirst({
      where: { trade_product_id: +body.trade_product_id, status: "A" },
    });

    const allocated = await prisma.$queryRaw`
    select 
          max(voyage_id) voyage_id,
          max(org_id) org_id,
          sum(shipping_quantity_mt) shipping_quantity_mt,
          sum(shipping_quantity_cm) shipping_quantity_cm,
          count(*)::int too
    from voyage_parcels
    where trade_id = ${+old_value.trade_id}
          and status = 'A'`;

    if (allocated && allocated[0].too>0) {
      const quantity_mt =
        body.quantity - parseFloat(allocated[0].shipping_quantity_mt);

      const quantity_cm =
        body.quantity_cm - parseFloat(allocated[0].shipping_quantity_cm);

      if (quantity_mt > -1 || quantity_cm > -1) {
        const unallocated = await prisma.voyage_unallocated_parcels.findFirst({
          where: {
            trade_id: +old_value.trade_id,
            status: "A",
          },
        });

        if (unallocated) {
          await prisma.voyage_unallocated_parcels.update({
            data: {
              unallocated_parcel_quantity_cm: +quantity_cm,
              unallocated_quantity_mt: +quantity_mt,
              unallocated_parcel_quantity_b: 0,
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: {
              unallocated_parcel_id: +unallocated.unallocated_parcel_id,
            },
          });
        } else {
          await prisma.voyage_unallocated_parcels.create({
            data: {
              trade_id: +old_value.trade_id,
              voyage_id: +allocated[0].voyage_id,
              org_id: +allocated[0].org_id,
              unallocated_parcel_quantity_cm: +quantity_cm,
              unallocated_quantity_mt: +quantity_mt,
              unallocated_parcel_quantity_b: 0,
              created_by: body.username,
              last_updated_by: body.username,
            },
          });
        }
      } else {
        return Response.json(
          {
            message: "Error",
            result: "Хуваарилалт хийгдсэн тоо хэмжээнээс бага байна",
          },
          { status: 203 }
        );
      }
    }

    const old_log = await prisma.log_trade_products.findFirst({
      where: {
        trade_product_id: +body.trade_product_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    if (old_log) {
      await prisma.log_trade_products.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_trade_product_id: +old_log.log_trade_product_id },
      });
    }

    await prisma.log_trade_products.create({
      data: {
        trade_product_id: +old_value.trade_product_id,
        geographical_area: old_value.geographical_area,
        trade_id: +old_value.trade_id,
        grade_id: +old_value.grade_id,
        product_id: +old_value.product_id,
        country_origin: old_value.country_origin,
        units: old_value.units,
        est_density: +old_value.est_density,
        trade_treatment: old_value.trade_treatment,
        quantity: +old_value.quantity,
        quantity_cm: +old_value.quantity_cm,
        plus_minus: old_value.plus_minus,
        min_quantity: +old_value.min_quantity,
        max_quantity: +old_value.max_quantity,
        product_cover_type: old_value.product_cover_type,
        product_cover_counterparty: +old_value.product_cover_counterparty,
        product_option_quantity: old_value.product_option_quantity,
        intention: old_value.intention,
        ops_active_flag: old_value.ops_active_flag,
        storage_active_flag: old_value.storage_active_flag,
        finance_active_flag: old_value.finance_active_flag,
        consolidate_deal_flag: old_value.consolidate_deal_flag,
        financing_behalf_flag: old_value.financing_behalf_flag,
        est_bl_date: new Date(old_value.est_bl_date),
        fixed_flag: old_value.fixed_flag,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    await prisma.trade_products.update({
      data: {
        geographical_area: body.geographical_area,
        grade_id: +body.grade_id,
        product_id: +body.product_id,
        country_origin: body.country_origin,
        units: body.units,
        est_density: +body.est_density,
        trade_treatment: body.trade_treatment,
        quantity: +body.quantity,
        quantity_cm: +body.quantity_cm,
        plus_minus: body.plus_minus,
        min_quantity: +body.min_quantity,
        max_quantity: +body.max_quantity,
        product_cover_type: body.product_cover_type,
        product_cover_counterparty: +body.product_cover_counterparty,
        product_option_quantity: body.product_option_quantity,
        intention: body.intention,
        ops_active_flag: body.ops_active_flag,
        storage_active_flag: body.storage_active_flag,
        finance_active_flag: body.finance_active_flag,
        consolidate_deal_flag: body.consolidate_deal_flag,
        financing_behalf_flag: body.financing_behalf_flag,
        est_bl_date: new Date(body.est_bl_date),
        fixed_flag: body.fixed_flag,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { trade_product_id: +body.trade_product_id, status: "A" },
    });

    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Product өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
