import { putObject, removeObject } from "@/lib/minioClient";
import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
import { BedOutlined } from "@mui/icons-material";
import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";
import * as Minio from "minio";

const minioClient = new Minio.Client({
  endPoint: process.env.MINIO_ENDPOINT,
  port: 9000,
  useSSL: false,
  accessKey: process.env.MINIO_ACCESSKEY,
  secretKey: process.env.MINIO_SECRET_KEY,
});
dotenv.config();

const { SENDGRID_API_KEY } = process.env;

sgMail.setApiKey(SENDGRID_API_KEY);

export async function POST(request) {
  const formData = await request.formData();
  try {
    const url = await putObject(
      formData.get('sourcefile'),
      formData.get("id"),
      formData.get("bucket"),
      formData.get("filename"),
      formData.get("attachment_type"),
      formData.get("filelength")
    );

    if (url.status === 200) {
      await prisma.trade_attachments.create({
        data: {
          // trade_id: +body.trade_id,
          // attachment_type: body.attachment_type,
          // attachment_name: body.attachment_name,
          // // attachment_url: body.attachment_url,
          // attachment_url: url.result.url,
          // description: body.description,
          // filename: body.filename,
          // created_by: body.username,
          // last_updated_by: body.username,
          trade_id: +formData.get("trade_id"),
          attachment_type: formData.get("attachment_type"),
          attachment_name: formData.get("attachment_name"),
          description: formData.get("description"),
          filename: formData.get("filename"),
          created_by: formData.get("username"),
          last_updated_by: formData.get("username"),
          attachment_url: url.result.url,
        },
      });

      return tokenResponse(request);
    } else
      return Response.json(
        {
          message: "Error", result: url.result,
        },
        {
          status: 500,
        }
      );
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.trade_attachments.update({
      data: {
        attachment_type: body.attachment_type,
        attachment_name: body.attachment_name,
        attachment_url: body.attachment_url,
        description: body.description,
        filename: body.filename,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { trade_attachment_id: +body.trade_attachment_id, status: "A" },
    });
    // const to = await prisma.$queryRaw`
    // select
    //       aa2.mail to_mail
    // from trades aa1
    //   left join bs_users aa2
    //     on aa1.trader_user_id = aa2.user_id
    //     and aa2.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //   and aa1.status = 'A'
    // union all
    // select
    //       aa3.mail to_mail
    // from trades aa1
    //   left join bs_users aa3
    //     on aa1.operator_user_id = aa3.user_id
    //     and aa3.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select
    //       aa4.mail to_mail
    // from trades aa1
    //   left join bs_users aa4
    //     on aa1.finance_user_id = aa4.user_id
    //     and aa4.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'
    // union all
    // select aa5.mail to_mail
    // from trades aa1
    //   left join bs_users aa5
    //     on aa1.contract_user_id = aa5.user_id
    //     and aa5.status = 'A'
    // where aa1.trade_id = ${+body.trade_id}
    //     and aa1.status = 'A'`;

    // const toEmails = to.map((result) => result.to_mail.trim()).filter(Boolean);
    // const emailPromises = toEmails.map(async (toEmail) => {
    //   const msg = {
    //     to: toEmail,
    //     from: "ndteam.llc@gmail.com",
    //     subject: "Өөрчлөлтийн мэдээлэл",
    //     text: "Attachments өөрчлөлт орлоо",
    //   };
    //   return sgMail.send(msg);
    // });
    // await Promise.all(emailPromises);

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    const del = await removeObject(body.bucket, body.id, body.filename);
    if (del.status === 200) {
      await prisma.trade_attachments.update({
        data: {
          status: "I",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { trade_attachment_id: +body.trade_attachment_id, status: "A" },
      });
      return tokenResponse(request);
    } else {
      return Response.json(
        {
          message: "Error",
          result: del.status,
        },
        {
          status: 500,
        }
      );
    }
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
