import { signJwtAccessToken } from "@/lib/jwt";
import prisma from "@/lib/prisma";
import * as bcrypt from "bcrypt";

export async function POST(request) {
  const body = await request.json();

  const user = await prisma.bs_users.findFirst({
    select: {
      user_id: true,
      username: true,
      password: true,
      role: true,
      lastname: true,
      firstname: true,
      mail: true,
      position: true,
      phone: true,
    },
    where: { username: body.username, status: "A" },
  });

  const menuitems = await prisma.$queryRaw`
                      select 
                            aa1.menu_id,
                            aa2.menu_name,
                            aa2.menu_url,
                            aa1.permission
                      from bs_users_permission aa1
                        inner join bs_menu aa2
                          on aa1.menu_id = aa2.menu_id
                          and aa2.status = 'A'
                          where aa1.user_id = ${+user.user_id} and aa1.status = 'A'
                            and aa1.permission !='None'
                          order by aa1.menu_id`;

  if (user && (await bcrypt.compare(body.password, user.password))) {
    const { password, ...userWithoutPass } = user;

    const { token, refreshToken } = await signJwtAccessToken(userWithoutPass);
    const result = {
      ...userWithoutPass,
      menuitems,
      token,
      refreshToken,
    };

    await prisma.users_logins.create({
      data: {
        user_id: +user.user_id,
        creation_date: new Date(),
      },
    });

    return Response.json(result);
  } else
    return Response.json(
      { message: "Error", result: "Incorrect username or password!" },
      { status: 500 }
    );
}
