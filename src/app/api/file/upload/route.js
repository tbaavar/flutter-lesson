// import { putObject, removeObject } from "@/lib/minioClient";

import minioClient from "@/lib/minioClient";

export async function POST(request) {
  const formData = await request.formData();
  try {
    const file = formData.get("file");
    const bucketName = formData.get("bucket");
    const destinationObject = `${formData.get("id")}/${file.name}`;

    const exists = await minioClient.bucketExists(bucketName);
    if (!exists) {
      await minioClient.makeBucket(bucketName, "s3");
    }

    const fileBuffer = await file.arrayBuffer();
    const buffer = Buffer.from(fileBuffer);

    // Set the object metadata
    const metaData = {
      "Content-Type": file.type,
    };

    const response = await minioClient.putObject(
      bucketName,
      destinationObject,
      buffer,
      metaData
    );

    const fullPath = {
      url: process.env.FILE_URL + bucketName + "/" + destinationObject,
    };

    return Response.json(
      {
        message: "Success",
        result: fullPath,
      },
      {
        status: 200,
      }
    );
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  console.log(body);
  const response = await removeObject(body.bucket, body.id, body.file);

  return Response.json(
    { message: response.message, result: response.result },
    { status: response.status }
  );
}
