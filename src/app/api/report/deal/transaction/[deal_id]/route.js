import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    			select		
					dd1.invoice_id,
					dd1.invoice_number,
					dd1.deal_id,
					dd1.deal_date,
					dd1.title,
					dd1.deal_status,
					dd1.deal_desc,
					dd1.company_id,
					dd1.company_name,
					dd1.invoice_owner_name,
					dd1.cost_code,
					dd1.cost_name,
					dd1.trade_id,
					dd1.parcel_number,
					dd1.counterparty_id,
					dd1.counterparty_name,
					dd1.description,
					dd1.bank_id,
					dd1.bank_name,
					dd1.tran_date,
					dd1.exp_pay_date,
					dd1.invoice_due_date,
					dd1.invoice_date,
					dd1.currency,
					dd1.invoice_type,
					dd1.value	gross_value,
					dd1.value	gross_value_usd	
			from (
					SELECT
							aa1.invoice_id,
							aa7.deal_id,
							aa8.deal_date,
							aa8.title,
							aa8.deal_status,
							aa8.description deal_desc,
							aa5.trade_id,				
							aa1.company_id,
							aa2.name company_name,
							aa6.trade_type,
							aa16.name trade_type_name,
							aa1.counterparty_id,
							aa3.name counterparty_name,
							aa1.invoice_number,
							aa1.invoice_type,
							aa1.invoice_state,
							aa4.name invoice_type_name,
							aa1.invoice_date,
							aa1.invoice_due_date,	
							aa1.currency,
							aa1.bank_id,
							aa19.bank_name,
							aa1.invoice_amount invoice_amount,
							aa1.invoice_payment_status,
							aa1.invoice_status,
							aa5.parcel_number,
							aa17.cost_code,
							aa17.value,
							aa18.cost_name cost_name,
							aa17.exp_pay_date,
							aa17.description,
							aa17.tran_date,
							aa20.lastname || ' '|| aa20.firstname invoice_owner_name
					FROM invoices aa1
						inner join bs_organizations aa2
							on aa1.company_id = aa2.org_id
							and aa2.status = 'A'
						inner join legal_entity aa3
							on aa1.counterparty_id = aa3.legal_entity_id
							and aa3.status = 'A'
						inner join bs_lookups aa4
							on aa1.invoice_type = aa4.lookup_code
							and aa4.lookup_type = 'Invoice_type'
							and aa4.status = 'A'
						inner join voyage_parcels aa5
							on aa1.voyage_parcel_id = aa5.voyage_parcel_id
							and aa5.status = 'A'
						inner join trades aa6
							on aa5.trade_id = aa6.trade_id
							and aa6.status = 'A'
						left join deal_trades aa7
							on aa5.trade_id  = aa7.trade_id
							and aa7.status = 'A' 
						left join deals aa8
							on aa7.deal_id = aa8.deal_id
							and aa8.status = 'A'
						left join bs_lookups aa16
							on aa6.trade_type = aa16.lookup_code
							and aa16.lookup_type = 'Trade_type'
							and aa16.status = 'A'
						left join invoice_details aa17
							on aa1.invoice_id = aa17.invoice_id
							and aa17.status = 'A'
						left join costs aa18
							on aa17.cost_code = aa18.cost_code
							and aa18.status = 'A'
						left join banks aa19
							on aa1.bank_id = aa19.bank_id
							and aa19.status = 'A'
						left join bs_users aa20
							on aa1.invoice_owner_id = aa20.user_id
							and aa20.status= 'A'
					) dd1
where dd1.deal_id = ${+params.deal_id} 
order by dd1.cost_code,dd1.tran_date`;
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
