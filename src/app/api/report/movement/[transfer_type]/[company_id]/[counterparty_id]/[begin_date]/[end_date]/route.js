import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
					select 
						aa1.parcel_movement_id,
						aa1.transfer_completion_date,
						aa3.name company_name,
						aa2.name counterparty_name,
						aa4.terminal_name,
						aa5.tank_name,
						aa1.parcel_comment,
						aa1.tank_comment,
						aa8.product_name,
						aa9.grade_name,
						aa1.quantity_mt,
						aa1.quantity_cm,
						aa1.final_density,
						aa1.units,
						aa7.amount
					from parcel_movements aa1
						left join legal_entity aa2
							on aa1.counterparty_id = aa2.legal_entity_id
							and aa2.status = 'A'
						left join bs_organizations aa3
							on aa1.org_id = aa3.org_id
							and aa3.status = 'A'
						left join storage_terminal aa4
							on aa1.source_terminal_id = aa4.terminal_id
							and aa4.status = 'A'
						left join storage_tank aa5
							on aa1.source_tank_id = aa5.tank_id
							and aa5.status = 'A'
						left join voyage_parcels aa6
							on aa1.movement_parcel_id = aa6.voyage_parcel_id
							and aa6.status = 'A'
						left join trade_pricing_fixed aa7
							on aa6.trade_id = aa7.trade_id
							and aa7.status = 'A'
						left join products aa8
                            on aa1.final_product_id = aa8.product_id
                            and aa8.status = 'A'
                        left join product_grades aa9
                                on aa1.final_product_id = aa9.product_id
                                and aa1.final_grade_id = aa9.grade_id
                                and aa9.status = 'A'
					where aa1.counterparty_id =  ${+params.counterparty_id}
						and aa1.org_id =  ${+params.company_id}
						and to_char(aa1.transfer_completion_date,'YYYY-MM-DD')>=${params.begin_date}
						and to_char(aa1.transfer_completion_date,'YYYY-MM-DD')<=${params.end_date}
						and aa1.transfer_type = ${params.transfer_type}
						and aa1.status = 'A'
				    order by aa1.transfer_completion_date`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
