import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    			select
                    aa1.trade_id,
                    aa1.trade_type,
                    aa4.name trade_type_name,
                    aa1.company_id,
                    aa5.name company_name,
                    aa1.counterparty_id,
                    aa6.name counterparty_name,
                    aa2.product_id,
                    aa7.product_name,
                    aa2.grade_id,
                    aa8.grade_name,
                    aa2.quantity,
                    aa2.units,
                    aa3.amount price,
                    aa9.shipping_quantity_mt dispatch_quantity,
                    aa9.invoice_amount ::int total_amount
                from (
                            select *
                            from trades
                            where company_id = ${+params.company_id}
                                and counterparty_id = ${+params.counterparty_id}
                                and to_char(trade_date,'YYYY-MM-DD')>=${
                                  params.begin_date
                                }
                                and to_char(trade_date,'YYYY-MM-DD')<=${
                                  params.end_date
                                }
                                and status = 'A'
                            ) aa1
                            inner join trade_products aa2
                                on aa1.trade_id = aa2.trade_id
                                and aa2.status = 'A'
                            inner join trade_pricing_fixed aa3
                                on aa1.trade_id = aa3.trade_id
                                and aa3.status = 'A'
                            left join bs_lookups aa4
                                on aa1.trade_type = aa4.lookup_code
                                and aa4.lookup_type = 'Trade_type'
                                and aa4.status = 'A'
                            left join bs_organizations aa5
                                on aa1.company_id = aa5.org_id
                                and aa5.status = 'A'
                            left join legal_entity aa6
                                on aa1.counterparty_id = aa6.legal_entity_id
                                and aa6.status = 'A'
                            left join products aa7
                                on aa2.product_id =aa7.product_id
                                and aa7.status = 'A'
                            left join product_grades aa8
                                on aa2.product_id = aa8.product_id
                                and aa2.grade_id = aa8.grade_id
                                and aa8.status = 'A'
                            left join (
                                        select bb1.trade_id,sum(bb1.shipping_quantity_mt) shipping_quantity_mt, sum(bb1.shipping_quantity_cm) shipping_quantity_cm,sum(bb2.invoice_amount) invoice_amount
                                        from voyage_parcels bb1
                                            inner join invoices bb2	
                                                on bb1.voyage_parcel_id = bb2.voyage_parcel_id
                                                and bb2.status = 'A'
                                        where bb1.port_type in ('Load_port','Discharge_port')
                                            and bb1.status = 'A'
                                            and bb1.est_quantity_flag = 'N'
                                        group by bb1.trade_id
                                        ) aa9
                                on aa1.trade_id = aa9.trade_id`;

    const detail = await prisma.$queryRaw`
                    select aa1.pay_date,aa1.value,aa3.trade_id
                    from invoice_payments aa1
                        inner join invoices aa2
                            on aa1.invoice_id = aa2.invoice_id
                            and aa2.status = 'A'
                        inner join voyage_parcels aa3
                            on aa2.voyage_parcel_id = aa3.voyage_parcel_id
                            and aa3.port_type in ('Load_port','Discharge_port')
                            and aa3.est_quantity_flag = 'N'
                            and aa3.status = 'A'
                    where aa1.status = 'A'
                    `;

    const arr = [];

    for (let x of response) {
      const payments = [];
      for (let z of detail) {
        if (x.trade_id === z.trade_id) {
          payments.push(z);
        }
      }
      arr.push({
        trade_id: x.trade_id,
        trade_type_name: x.trade_type_name,
        company_name: x.company_name,
        counterparty_name: x.counterparty_name,
        product_name: x.product_name,
        grade_name: x.grade_name,
        quantity: x.quantity,
        units: x.units,
        price: x.price,
        dispatch_quantity: x.dispatch_quantity,
        total_amount: x.total_amount,
        payments: payments,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
