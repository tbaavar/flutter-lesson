import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    let response = null;

    if (params.company_id === "All" && params.counterparty_id === "All") {
      response = await prisma.$queryRaw`
        select 
	dd1.company,
	dd1.counterparty
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) delivered_not_yet
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) delivered_0110
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) delivered_1120
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) delivered_2130
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) delivered_30up
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) not_delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) not_delivered_not_yet
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) not_delivered_0110
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) not_delivered_1120
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) not_delivered_2130
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) not_delivered_30up
from (
select 
            aa1.deal_id,
            aa2.trade_id,
            aa4.voyage_parcel_id,
            aa5.voyage_id,
            aa3_4.name trade_type,
            aa3_2.product_name product,
            aa3_3.grade_name grade,
            aa3_5.name company,
            aa3_6.name counterparty,
            case when aa4.est_quantity_flag = 'N' and aa4.transfer_date_est_flag ='N' then 'Delivered' else 'Not Delivered' end delivery_status,
            aa6.invoice_payment_status,
            aa6.invoice_id,
            aa6.invoice_number,
            aa3_8.day_1 || ' '|| aa3_8.day_count_method || ' Days ' || aa3_8.time_condition_1 as payment_terms,
            aa4.transfer_date payment_due_date,
            aa6.invoice_date,
            aa6.invoice_due_date,
            aa6_1.bank_name,
            aa6_2.name invoice_type,
            aa6.invoice_comments,
            aa6.invoice_description,
            case when aa4.est_quantity_flag = 'N' then 'Actual' else 'Not Actual' end quantity_actual_est,
            aa6.currency,
						aa3_9.total_amount_other value_usd,
            aa4.shipping_quantity_mt quantity,
            aa3_9.amount,
            aa4.shipping_quantity_mt* aa3_9.amount parcel_value,
            aa6.invoice_amount,
						COALESCE(aa7.paid_amount,0) paid_amount,
						aa6.invoice_amount - COALESCE(aa7.paid_amount,0) unpaid_amount,
						case when aa6.invoice_due_date>CURRENT_DATE then 'Not Yet'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=1 and (CURRENT_DATE-aa6.invoice_due_date)<=10 then 'Overdue 1-10'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=10 and (CURRENT_DATE-aa6.invoice_due_date)<=20 then 'Overdue 11-20'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=21 and (CURRENT_DATE-aa6.invoice_due_date)<=30 then 'Overdue 21-30'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=31 then 'Overdue 30 +'
								 when aa6.invoice_id is null then 'Not Invoiced'
						end as overduedays
    from deals aa1
        inner join deal_trades aa2
				    on aa1.deal_id = aa2.deal_id
            and aa2.status = 'A' 
        inner join trades aa3
            on aa2.trade_id = aa3.trade_id
            and aa3.status = 'A'
        inner join trade_products aa3_1
            on aa3.trade_id= aa3_1.trade_id
            and aa3_1.status = 'A'
        inner join products aa3_2
            on aa3_1.product_id = aa3_2.product_id
            and aa3_2.status = 'A'
        inner join product_grades aa3_3
            on aa3_1.product_id = aa3_3.product_id
            and aa3_1.grade_id = aa3_3.grade_id
            and aa3_3.status = 'A'
        inner join bs_lookups aa3_4
            on aa3.trade_type = aa3_4.lookup_code
            and aa3_4.lookup_type = 'Trade_type'
            and aa3_4.status = 'A'
        inner join bs_organizations aa3_5
            on aa3.company_id = aa3_5.org_id
            and aa3_5.status = 'A'
        inner join legal_entity aa3_6
            on aa3.counterparty_id = aa3_6.legal_entity_id
            and aa3_6.status = 'A'
        inner join bs_users aa3_7
            on aa3.trader_user_id = aa3_7.user_id
            and aa3_7.status = 'A'
        inner join trade_payments aa3_8
            on aa3.trade_id = aa3_8.trade_id
            and aa3_8.status = 'A'
        inner join trade_pricing_fixed aa3_9
            on aa3.trade_id = aa3_9.trade_id
            and aa3_9.status = 'A'
        inner join voyage_parcels aa4
            on aa2.trade_id = aa4.trade_id
            and aa4.status = 'A'
        left join bs_users aa4_1
            on aa4.operator_user_id = aa4_1.user_id
            and aa4_1.status = 'A'
        left join ports aa4_2
            on aa4.load_port_id = aa4_2.port_id
            and aa4_2.status = 'A'
        left join ports aa4_3
            on aa4.discharge_port_id = aa4_3.port_id
            and aa4_3.status = 'A'
        inner join voyages aa5
            on aa4.voyage_id = aa5.voyage_id
            and aa5.status = 'A'
        inner join bs_lookups aa5_1
            on aa5.intended_vessel = aa5_1.lookup_code
            and aa5_1.lookup_type = 'Intended_Vessel'
            and aa5_1.status = 'A'
        left join invoices aa6
            on aa4.voyage_parcel_id = aa6.voyage_parcel_id
            and aa6.status= 'A'
        left join banks aa6_1
            on aa6.bank_id = aa6_1.bank_id
            and aa6_1.status = 'A'
        left join bs_lookups aa6_2
            on aa6.invoice_type = aa6_2.lookup_code
            and aa6_2.lookup_type = 'Invoice_type'
            and aa6_2.status = 'A'
        left join (
                    select invoice_id,sum(value) ::numeric paid_amount
                    from invoice_details
                    where balance_type = 'Credit'
                        and status = 'A'
                    group by invoice_id
                ) aa7
            on aa6.invoice_id = aa7.invoice_id
			 where to_char(aa3.trade_date,'YYYY-MM-DD')>=${params.begin_date}
        and to_char(aa3.trade_date ,'YYYY-MM-DD')<=${params.end_date}
				) dd1
group by dd1.company,dd1.counterparty`;
    }
    else if (params.company_id !== "All" && params.counterparty_id === "All") {
        response = await prisma.$queryRaw`
       select 
	dd1.company,
	dd1.counterparty
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) delivered_not_yet
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) delivered_0110
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) notdelivered_1120
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) delivered_2130
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) delivered_30up
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) not_delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) not_delivered_not_yet
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) not_delivered_0110
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) not_delivered_1120
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) not_delivered_2130
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) not_delivered_30up
from (
select 
            aa1.deal_id,
            aa2.trade_id,
            aa4.voyage_parcel_id,
            aa5.voyage_id,
            aa3_4.name trade_type,
            aa3_2.product_name product,
            aa3_3.grade_name grade,
            aa3_5.name company,
            aa3_6.name counterparty,
            case when aa4.est_quantity_flag = 'N' and aa4.transfer_date_est_flag ='N' then 'Delivered' else 'Not Delivered' end delivery_status,
            aa6.invoice_payment_status,
            aa6.invoice_id,
            aa6.invoice_number,
            aa3_8.day_1 || ' '|| aa3_8.day_count_method || ' Days ' || aa3_8.time_condition_1 as payment_terms,
            aa4.transfer_date payment_due_date,
            aa6.invoice_date,
            aa6.invoice_due_date,
            aa6_1.bank_name,
            aa6_2.name invoice_type,
            aa6.invoice_comments,
            aa6.invoice_description,
            case when aa4.est_quantity_flag = 'N' then 'Actual' else 'Not Actual' end quantity_actual_est,
            aa6.currency,
						aa3_9.total_amount_other value_usd,
            aa4.shipping_quantity_mt quantity,
            aa3_9.amount,
            aa4.shipping_quantity_mt* aa3_9.amount parcel_value,
            aa6.invoice_amount,
						COALESCE(aa7.paid_amount,0) paid_amount,
						aa6.invoice_amount - COALESCE(aa7.paid_amount,0) unpaid_amount,
						case when aa6.invoice_due_date>CURRENT_DATE then 'Not Yet'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=1 and (CURRENT_DATE-aa6.invoice_due_date)<=10 then 'Overdue 1-10'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=10 and (CURRENT_DATE-aa6.invoice_due_date)<=20 then 'Overdue 11-20'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=21 and (CURRENT_DATE-aa6.invoice_due_date)<=30 then 'Overdue 21-30'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=31 then 'Overdue 30 +'
								 when aa6.invoice_id is null then 'Not Invoiced'
						end as overduedays
    from deals aa1
        inner join deal_trades aa2
				    on aa1.deal_id = aa2.deal_id
            and aa2.status = 'A' 
        inner join trades aa3
            on aa2.trade_id = aa3.trade_id
            and aa3.status = 'A'
        inner join trade_products aa3_1
            on aa3.trade_id= aa3_1.trade_id
            and aa3_1.status = 'A'
        inner join products aa3_2
            on aa3_1.product_id = aa3_2.product_id
            and aa3_2.status = 'A'
        inner join product_grades aa3_3
            on aa3_1.product_id = aa3_3.product_id
            and aa3_1.grade_id = aa3_3.grade_id
            and aa3_3.status = 'A'
        inner join bs_lookups aa3_4
            on aa3.trade_type = aa3_4.lookup_code
            and aa3_4.lookup_type = 'Trade_type'
            and aa3_4.status = 'A'
        inner join bs_organizations aa3_5
            on aa3.company_id = aa3_5.org_id
            and aa3_5.status = 'A'
        inner join legal_entity aa3_6
            on aa3.counterparty_id = aa3_6.legal_entity_id
            and aa3_6.status = 'A'
        inner join bs_users aa3_7
            on aa3.trader_user_id = aa3_7.user_id
            and aa3_7.status = 'A'
        inner join trade_payments aa3_8
            on aa3.trade_id = aa3_8.trade_id
            and aa3_8.status = 'A'
        inner join trade_pricing_fixed aa3_9
            on aa3.trade_id = aa3_9.trade_id
            and aa3_9.status = 'A'
        inner join voyage_parcels aa4
            on aa2.trade_id = aa4.trade_id
            and aa4.status = 'A'
        left join bs_users aa4_1
            on aa4.operator_user_id = aa4_1.user_id
            and aa4_1.status = 'A'
        left join ports aa4_2
            on aa4.load_port_id = aa4_2.port_id
            and aa4_2.status = 'A'
        left join ports aa4_3
            on aa4.discharge_port_id = aa4_3.port_id
            and aa4_3.status = 'A'
        inner join voyages aa5
            on aa4.voyage_id = aa5.voyage_id
            and aa5.status = 'A'
        inner join bs_lookups aa5_1
            on aa5.intended_vessel = aa5_1.lookup_code
            and aa5_1.lookup_type = 'Intended_Vessel'
            and aa5_1.status = 'A'
        left join invoices aa6
            on aa4.voyage_parcel_id = aa6.voyage_parcel_id
            and aa6.status= 'A'
        left join banks aa6_1
            on aa6.bank_id = aa6_1.bank_id
            and aa6_1.status = 'A'
        left join bs_lookups aa6_2
            on aa6.invoice_type = aa6_2.lookup_code
            and aa6_2.lookup_type = 'Invoice_type'
            and aa6_2.status = 'A'
        left join (
                    select invoice_id,sum(value) ::numeric paid_amount
                    from invoice_details
                    where balance_type = 'Credit'
                        and status = 'A'
                    group by invoice_id
                ) aa7
            on aa6.invoice_id = aa7.invoice_id
  where aa3.company_id = ${+params.company_id}
        and to_char(aa3.trade_date,'YYYY-MM-DD')>=${params.begin_date}
        and to_char(aa3.trade_date ,'YYYY-MM-DD')<=${params.end_date}
				) dd1
group by dd1.company,dd1.counterparty`;
    }
    else if (params.company_id === "All" && params.counterparty_id !== "All") {
        response = await prisma.$queryRaw`
        select 
	dd1.company,
	dd1.counterparty
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) delivered_not_yet
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) delivered_0110
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) notdelivered_1120
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) delivered_2130
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) delivered_30up
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) not_delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) not_delivered_not_yet
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) not_delivered_0110
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) not_delivered_1120
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) not_delivered_2130
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) not_delivered_30up
from (
select 
            aa1.deal_id,
            aa2.trade_id,
            aa4.voyage_parcel_id,
            aa5.voyage_id,
            aa3_4.name trade_type,
            aa3_2.product_name product,
            aa3_3.grade_name grade,
            aa3_5.name company,
            aa3_6.name counterparty,
            case when aa4.est_quantity_flag = 'N' and aa4.transfer_date_est_flag ='N' then 'Delivered' else 'Not Delivered' end delivery_status,
            aa6.invoice_payment_status,
            aa6.invoice_id,
            aa6.invoice_number,
            aa3_8.day_1 || ' '|| aa3_8.day_count_method || ' Days ' || aa3_8.time_condition_1 as payment_terms,
            aa4.transfer_date payment_due_date,
            aa6.invoice_date,
            aa6.invoice_due_date,
            aa6_1.bank_name,
            aa6_2.name invoice_type,
            aa6.invoice_comments,
            aa6.invoice_description,
            case when aa4.est_quantity_flag = 'N' then 'Actual' else 'Not Actual' end quantity_actual_est,
            aa6.currency,
						aa3_9.total_amount_other value_usd,
            aa4.shipping_quantity_mt quantity,
            aa3_9.amount,
            aa4.shipping_quantity_mt* aa3_9.amount parcel_value,
            aa6.invoice_amount,
						COALESCE(aa7.paid_amount,0) paid_amount,
						aa6.invoice_amount - COALESCE(aa7.paid_amount,0) unpaid_amount,
						case when aa6.invoice_due_date>CURRENT_DATE then 'Not Yet'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=1 and (CURRENT_DATE-aa6.invoice_due_date)<=10 then 'Overdue 1-10'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=10 and (CURRENT_DATE-aa6.invoice_due_date)<=20 then 'Overdue 11-20'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=21 and (CURRENT_DATE-aa6.invoice_due_date)<=30 then 'Overdue 21-30'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=31 then 'Overdue 30 +'
								 when aa6.invoice_id is null then 'Not Invoiced'
						end as overduedays
    from deals aa1
        inner join deal_trades aa2
				    on aa1.deal_id = aa2.deal_id
            and aa2.status = 'A' 
        inner join trades aa3
            on aa2.trade_id = aa3.trade_id
            and aa3.status = 'A'
        inner join trade_products aa3_1
            on aa3.trade_id= aa3_1.trade_id
            and aa3_1.status = 'A'
        inner join products aa3_2
            on aa3_1.product_id = aa3_2.product_id
            and aa3_2.status = 'A'
        inner join product_grades aa3_3
            on aa3_1.product_id = aa3_3.product_id
            and aa3_1.grade_id = aa3_3.grade_id
            and aa3_3.status = 'A'
        inner join bs_lookups aa3_4
            on aa3.trade_type = aa3_4.lookup_code
            and aa3_4.lookup_type = 'Trade_type'
            and aa3_4.status = 'A'
        inner join bs_organizations aa3_5
            on aa3.company_id = aa3_5.org_id
            and aa3_5.status = 'A'
        inner join legal_entity aa3_6
            on aa3.counterparty_id = aa3_6.legal_entity_id
            and aa3_6.status = 'A'
        inner join bs_users aa3_7
            on aa3.trader_user_id = aa3_7.user_id
            and aa3_7.status = 'A'
        inner join trade_payments aa3_8
            on aa3.trade_id = aa3_8.trade_id
            and aa3_8.status = 'A'
        inner join trade_pricing_fixed aa3_9
            on aa3.trade_id = aa3_9.trade_id
            and aa3_9.status = 'A'
        inner join voyage_parcels aa4
            on aa2.trade_id = aa4.trade_id
            and aa4.status = 'A'
        left join bs_users aa4_1
            on aa4.operator_user_id = aa4_1.user_id
            and aa4_1.status = 'A'
        left join ports aa4_2
            on aa4.load_port_id = aa4_2.port_id
            and aa4_2.status = 'A'
        left join ports aa4_3
            on aa4.discharge_port_id = aa4_3.port_id
            and aa4_3.status = 'A'
        inner join voyages aa5
            on aa4.voyage_id = aa5.voyage_id
            and aa5.status = 'A'
        inner join bs_lookups aa5_1
            on aa5.intended_vessel = aa5_1.lookup_code
            and aa5_1.lookup_type = 'Intended_Vessel'
            and aa5_1.status = 'A'
        left join invoices aa6
            on aa4.voyage_parcel_id = aa6.voyage_parcel_id
            and aa6.status= 'A'
        left join banks aa6_1
            on aa6.bank_id = aa6_1.bank_id
            and aa6_1.status = 'A'
        left join bs_lookups aa6_2
            on aa6.invoice_type = aa6_2.lookup_code
            and aa6_2.lookup_type = 'Invoice_type'
            and aa6_2.status = 'A'
        left join (
                    select invoice_id,sum(value) ::numeric paid_amount
                    from invoice_details
                    where balance_type = 'Credit'
                        and status = 'A'
                    group by invoice_id
                ) aa7
            on aa6.invoice_id = aa7.invoice_id
  where aa3.counterparty_id =  ${+params.counterparty_id}
        and to_char(aa3.trade_date,'YYYY-MM-DD')>=${params.begin_date}
        and to_char(aa3.trade_date ,'YYYY-MM-DD')<=${params.end_date}
				) dd1
group by dd1.company,dd1.counterparty`;
    }
    else if (params.company_id !== "All" && params.counterparty_id !== "All") {
        response = await prisma.$queryRaw`
        select 
	dd1.company,
	dd1.counterparty
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) delivered_not_yet
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) delivered_0110
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) notdelivered_1120
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) delivered_2130
	,sum(case when dd1.delivery_status = 'Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) delivered_30up
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Invoiced' then dd1.parcel_value end) not_delivered_notinvoice
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Not Yet' then dd1.unpaid_amount end) not_delivered_not_yet
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 1-10' then dd1.unpaid_amount end) not_delivered_0110
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 11-20' then dd1.unpaid_amount end) not_delivered_1120
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 21-30' then dd1.unpaid_amount end) not_delivered_2130
	,sum(case when dd1.delivery_status = 'Not Delivered' and dd1.overduedays = 'Overdue 30 +' then dd1.unpaid_amount end) not_delivered_30up
from (
select 
            aa1.deal_id,
            aa2.trade_id,
            aa4.voyage_parcel_id,
            aa5.voyage_id,
            aa3_4.name trade_type,
            aa3_2.product_name product,
            aa3_3.grade_name grade,
            aa3_5.name company,
            aa3_6.name counterparty,
            case when aa4.est_quantity_flag = 'N' and aa4.transfer_date_est_flag ='N' then 'Delivered' else 'Not Delivered' end delivery_status,
            aa6.invoice_payment_status,
            aa6.invoice_id,
            aa6.invoice_number,
            aa3_8.day_1 || ' '|| aa3_8.day_count_method || ' Days ' || aa3_8.time_condition_1 as payment_terms,
            aa4.transfer_date payment_due_date,
            aa6.invoice_date,
            aa6.invoice_due_date,
            aa6_1.bank_name,
            aa6_2.name invoice_type,
            aa6.invoice_comments,
            aa6.invoice_description,
            case when aa4.est_quantity_flag = 'N' then 'Actual' else 'Not Actual' end quantity_actual_est,
            aa6.currency,
						aa3_9.total_amount_other value_usd,
            aa4.shipping_quantity_mt quantity,
            aa3_9.amount,
            aa4.shipping_quantity_mt* aa3_9.amount parcel_value,
            aa6.invoice_amount,
						COALESCE(aa7.paid_amount,0) paid_amount,
						aa6.invoice_amount - COALESCE(aa7.paid_amount,0) unpaid_amount,
						case when aa6.invoice_due_date>CURRENT_DATE then 'Not Yet'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=1 and (CURRENT_DATE-aa6.invoice_due_date)<=10 then 'Overdue 1-10'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=10 and (CURRENT_DATE-aa6.invoice_due_date)<=20 then 'Overdue 11-20'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=21 and (CURRENT_DATE-aa6.invoice_due_date)<=30 then 'Overdue 21-30'
								 when aa6.invoice_due_date<=CURRENT_DATE and (CURRENT_DATE-aa6.invoice_due_date)>=31 then 'Overdue 30 +'
								 when aa6.invoice_id is null then 'Not Invoiced'
						end as overduedays
    from deals aa1
        inner join deal_trades aa2
				    on aa1.deal_id = aa2.deal_id
            and aa2.status = 'A' 
        inner join trades aa3
            on aa2.trade_id = aa3.trade_id
            and aa3.status = 'A'
        inner join trade_products aa3_1
            on aa3.trade_id= aa3_1.trade_id
            and aa3_1.status = 'A'
        inner join products aa3_2
            on aa3_1.product_id = aa3_2.product_id
            and aa3_2.status = 'A'
        inner join product_grades aa3_3
            on aa3_1.product_id = aa3_3.product_id
            and aa3_1.grade_id = aa3_3.grade_id
            and aa3_3.status = 'A'
        inner join bs_lookups aa3_4
            on aa3.trade_type = aa3_4.lookup_code
            and aa3_4.lookup_type = 'Trade_type'
            and aa3_4.status = 'A'
        inner join bs_organizations aa3_5
            on aa3.company_id = aa3_5.org_id
            and aa3_5.status = 'A'
        inner join legal_entity aa3_6
            on aa3.counterparty_id = aa3_6.legal_entity_id
            and aa3_6.status = 'A'
        inner join bs_users aa3_7
            on aa3.trader_user_id = aa3_7.user_id
            and aa3_7.status = 'A'
        inner join trade_payments aa3_8
            on aa3.trade_id = aa3_8.trade_id
            and aa3_8.status = 'A'
        inner join trade_pricing_fixed aa3_9
            on aa3.trade_id = aa3_9.trade_id
            and aa3_9.status = 'A'
        inner join voyage_parcels aa4
            on aa2.trade_id = aa4.trade_id
            and aa4.status = 'A'
        left join bs_users aa4_1
            on aa4.operator_user_id = aa4_1.user_id
            and aa4_1.status = 'A'
        left join ports aa4_2
            on aa4.load_port_id = aa4_2.port_id
            and aa4_2.status = 'A'
        left join ports aa4_3
            on aa4.discharge_port_id = aa4_3.port_id
            and aa4_3.status = 'A'
        inner join voyages aa5
            on aa4.voyage_id = aa5.voyage_id
            and aa5.status = 'A'
        inner join bs_lookups aa5_1
            on aa5.intended_vessel = aa5_1.lookup_code
            and aa5_1.lookup_type = 'Intended_Vessel'
            and aa5_1.status = 'A'
        left join invoices aa6
            on aa4.voyage_parcel_id = aa6.voyage_parcel_id
            and aa6.status= 'A'
        left join banks aa6_1
            on aa6.bank_id = aa6_1.bank_id
            and aa6_1.status = 'A'
        left join bs_lookups aa6_2
            on aa6.invoice_type = aa6_2.lookup_code
            and aa6_2.lookup_type = 'Invoice_type'
            and aa6_2.status = 'A'
        left join (
                    select invoice_id,sum(value) ::numeric paid_amount
                    from invoice_details
                    where balance_type = 'Credit'
                        and status = 'A'
                    group by invoice_id
                ) aa7
            on aa6.invoice_id = aa7.invoice_id
    where aa3.counterparty_id =  ${+params.counterparty_id}
        and aa3.company_id = ${+params.company_id}
        and to_char(aa3.trade_date,'YYYY-MM-DD')>=${params.begin_date}
        and to_char(aa3.trade_date ,'YYYY-MM-DD')<=${params.end_date}
				) dd1
group by dd1.company,dd1.counterparty`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
