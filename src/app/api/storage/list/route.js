import prisma from "@/lib/prisma";
import { Prisma, PrismaClient } from "@prisma/client";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function PATCH(request) {
  const body = await request.json();

  try {
    const sql1 = `select 
                      aa1.parcel_movement_id,                      
                      aa1.transfer_type,
                      aa1.voyage_id,
                      aa1.voyage_parcel_id,
                      aa9.product_id,
                      aa9.product_name,
                      aa9.grade_id,
                      aa9.grade_name,
                      aa9.country_origin,
                      aa9.country_origin_name,
                      aa9.unit,
                      aa9.unit_name,
                      aa9.shipping_quantity_mt,
                      aa9.shipping_quantity_cm,
                      aa9.est_density,
                      aa1.parcel_comment,
                      aa1.transfer_status,
                      aa1.superintendent_id,
                      aa1.superveyor_id,                    
                      aa1.transfer_completion_date,
                      aa1.transfer_completion_date_est_fl,
                      aa1.terminal_from_date,
                      aa1.terminal_to_date,
                      aa1.source_terminal_id,
                      aa4.terminal_name,
                      aa1.source_tank_id,
                      aa3.tank_name,
                      aa1.target_terminal_id,
                      aa1.target_tank_id,
                      aa1.quantity_mt quantity,
                      aa1.quantity_cm,
                      aa1.units tank_unit,
                      aa8.name tank_unit_name,
                      aa1.tank_comment,
                      aa1.clean_tank_flag,
                      aa1.third_party_stock_flag,								
                      aa5.product_name tank_product,				
                      aa6.grade_name tank_grade,										
                      aa2.final_balance_mt tank_balance_mt,
                      aa2.final_balance_cm tank_balance_cm,
                      aa3.capacity tank_capacity,
                      aa1.final_density tank_density,
                      aa1.adjustment_type,
                      aa1.transfer_grade_id,
                      aa1.blend_flag,
                      aa1.final_product_id,
                      aa1.final_grade_id,
                      aa1.circulation,				
                      aa1.blend_ratio,
                      aa1.balance_mt,
                      aa1.balance_cm,
                      aa1.counterparty_id,
                      aa10.name counterparty_name,
                      aa1.final_density,
                      aa1.movement_parcel_id,
                      aa9.voyage_title,
                      aa1.org_id
                  from parcel_movements aa1   
                  inner join parcel_movement_balance aa2
                    on aa1.source_terminal_id = aa2.terminal_id
                    and aa1.source_tank_id = aa2.tank_id
                    and aa2.status = 'A'           
                  inner join storage_tank aa3
                      on aa1.source_tank_id = aa3.tank_id
                      and aa3.status = 'A'
                  inner join storage_terminal aa4
                      on aa1.source_terminal_id = aa4.terminal_id
                      and aa4.status = 'A'
                  inner join products aa5
                    on aa1.product_id = aa5.product_id
                    and aa5.status = 'A'
                  inner join product_grades aa6
                    on aa1.product_id = aa6.product_id
                    and aa1.grade_id = aa6.grade_id
                    and aa6.status = 'A'
                  inner join bs_lookups aa7
                    on aa1.country_origin = aa7.lookup_code
                    and aa7.lookup_type = 'Country_Origin'
                    and aa7.status = 'A'
                  inner join bs_lookups aa8
                    on aa1.units = aa8.lookup_code
                    and aa8.lookup_type = 'Unit'
                      and aa8.status = 'A'
                  left join (
                            select 
                                aa1.voyage_id,
                                max(aa1.voyage_title)	voyage_title,								
                                max(aa2.product_id) product_id,
                                max(aa3.product_name) product_name,
                                max(aa2.grade_id) grade_id,
                                max(aa4.grade_name) grade_name,
                                max(aa2.country_origin) country_origin,
                                max(aa5.name) country_origin_name,
                                max(aa6.units) unit,
                                max(aa7.name) unit_name,
                                max(aa6.est_density) est_density,
                                sum(case when aa2.trade_code = 'P' then aa2.shipping_quantity_mt end) shipping_quantity_mt,
                                sum(case when aa2.trade_code = 'P' then aa2.shipping_quantity_mt end)*max(aa6.est_density) as shipping_quantity_cm
                              from voyages aa1
                                inner join voyage_parcels aa2
                                  on aa1.voyage_id = aa2.voyage_id
                                  and aa2.status = 'A'
                                inner join products aa3
                                  on aa2.product_id = aa3.product_id
                                  and aa3.status = 'A'
                                inner join product_grades aa4
                                  on aa2.product_id = aa4.product_id
                                  and aa2.grade_id = aa4.grade_id
                                  and aa4.status = 'A'
                                left join bs_lookups aa5
                                  on aa2.country_origin = aa5.lookup_code
                                  and aa5.lookup_type = 'Country_Origin'
                                  and aa5.status = 'A'
                                inner join trade_products aa6
                                  on aa2.trade_id = aa6.trade_id
                                  and aa6.status = 'A'
                                inner join bs_lookups aa7
                                  on aa6.units = aa7.lookup_code
                                  and aa7.lookup_type = 'Unit'
                                  and aa7.status = 'A'                            
                              where aa1.status= 'A'
                              group by aa1.voyage_id
                            ) aa9                            
                        on aa1.voyage_id = aa9.voyage_id
                      left join legal_entity aa10
                      on aa1.counterparty_id = aa10.legal_entity_id
                      and aa10.status = 'A'
                    where aa1.source_tank_id in (${body.tanks
                      .map((v) => `${v.tank_id}`)
                      .join(",")})
                            and aa1.source_terminal_id = ${+body.terminal_id}
                            and to_char(aa1.transfer_completion_date,'YYYY-MM-DD') >= '${
                              body.begin_date
                            }'
                            and to_char(aa1.transfer_completion_date,'YYYY-MM-DD') <= '${
                              body.end_date
                            }'
                            and aa1.status = 'A'
                    order by aa1.transfer_completion_date desc`;

    const transfer_dates = await prisma.$queryRaw(Prisma.raw(sql1));

    return tokenResponse(request, transfer_dates);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
