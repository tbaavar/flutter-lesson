import prisma from "@/lib/prisma";
import { Prisma, PrismaClient } from "@prisma/client";
import { tokenResponse } from "@/lib/response";
import dayjs from "dayjs";

export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.$queryRaw`
                  select 
                      aa1.parcel_movement_id,		
                      aa1.voyage_id,
                      aa1.transfer_type,
                      aa1.product_id,
                      aa1.grade_id,                    
                      aa1.country_origin,
                      aa1.parcel_comment,
                      aa1.transfer_status,                      
                      aa1.superintendent_id,
                      aa1.superveyor_id,                      
                      aa1.transfer_completion_date,
                      aa1.transfer_completion_date_est_fl,
                      aa1.terminal_from_date,
                      aa1.terminal_to_date,
                      aa1.source_terminal_id,
                      aa4.terminal_name,
                      aa1.source_tank_id,
                      aa3.tank_name,
                      aa1.target_terminal_id,
                      aa1.target_tank_id,
                      aa1.quantity_mt,
                      aa1.quantity_cm,                                          
                      aa1.tank_comment,
                      aa1.clean_tank_flag,
                      aa1.third_party_stock_flag,
                      aa1.transfer_grade_id,
                      aa1.blend_flag,
                      aa1.final_product_id,
                      aa1.final_grade_id,
                      aa1.circulation,
                      aa1.final_density,
                      aa1.blend_ratio,
                      aa1.balance_mt,
                      aa1.balance_cm,                      
                      aa1.units,
                      aa1.adjustment_type
                      aa1.counterparty_id
                  from parcel_movements aa1                  
                    left join storage_tank aa3
                      on aa1.source_tank_id = aa3.tank_id
                      and aa3.status = 'A'
                    left join storage_terminal aa4
                      on aa1.source_terminal_id = aa4.terminal_id
                      and aa4.status = 'A'
                  where aa1.status = 'A'`;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  let ports = null;
  let parcel = null;

  const dates_based_on =
    body.transfer_type === "Load" ? "Loading" : "Discharge";

  const port_type = body.transfer_type === "Load" ? "STS_load" : "STS_disport";

  try {
    const isBalance = await prisma.parcel_movement_balance.findFirst({
      where: {
        terminal_id: +body.source_terminal_id,
        tank_id: +body.source_tank_id,
        status: "A",
      },
    });

    if (isBalance && isBalance.transfer_status !== "Completed") {
      return Response.json(
        {
          message: "Error",
          result: "This tank processing waiting for completed",
        },
        { status: 203 }
      );
    }

    if (body.transfer_type === "Load" || body.transfer_type === "Discharge") {
      const isPort = await prisma.voyage_ports.findFirst({
        where: {
          port_type: port_type,
          voyage_id: +body.voyage_id,
          status: "A",
        },
      });

      if (!isPort) {
        ports = await prisma.voyage_ports.create({
          data: {
            voyage_id: +body.voyage_id,
            port_id: 1,
            port_type: port_type,
            agent_id: null,
            superveyor_id: +body.superveyor_id,
            superintendent_id: +body.superintendent_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        ports = isPort;
      }

      parcel = await prisma.voyage_parcels.create({
        data: {
          trade_id: 1,
          voyage_id: +body.voyage_id,
          parcel_number: "P-" + dayjs().format("YYMMDDHHMMms").toString(),
          country_origin: body.country_origin,
          load_port_id: 0,
          discharge_port_id: 0,
          lc_open_date: new Date(),
          vat_type: null,
          transfer_state: null,
          outside_us_flag: "N",
          bookout_flag: "N",
          vat_country: null,
          tax_code: null,
          custom_cleared_us_flag: "N",
          dates_based_on: dates_based_on,
          no_agreed_supply_date_flag: "N",
          transfer_date: new Date(body.transfer_completion_date),
          transfer_date_est_flag: body.transfer_completion_date_est_fl,
          border_crossing_date: new Date(body.transfer_completion_date),
          border_crossing_date_est_flag: body.transfer_completion_date_est_fl,
          supply_start_date: new Date(body.transfer_completion_date),
          supply_end_date: new Date(body.transfer_completion_date),
          transfer_detail_date: new Date(body.transfer_completion_date),
          api_60f: null,
          est_density_15c_air: null,
          est_density_15c_vac: null,
          shipping_quantity_mt: +body.quantity_mt,
          shipping_quantity_cm: +body.quantity_cm,
          shipping_quantity_b: +body.quantity_mt,
          est_quantity_flag: "N",
          show_outturn_qty_flag: "N",
          invoicing_unit: body.units,
          operator_user_id: null,
          voyage_port_id: +ports.voyage_port_id,
          port_type: port_type,
          product_id: +body.product_id,
          grade_id: +body.grade_id,
          trade_code: body.trade_code,
          counterparty_id: +body.counterparty_id,
          terminal_id: +body.source_terminal_id,
          org_id:+body.company_id,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });
    }

    const old_value = await prisma.parcel_movements.create({
      data: {
        voyage_id:
          body.transfer_type === "Load" ||
          body.transfer_type === "Discharge" ||
          body.transfer_type === "Pumpover"
            ? +body.voyage_id
            : 1,
        voyage_parcel_id:
          body.transfer_type === "Load" || body.transfer_type === "Discharge"
            ? +parcel.voyage_parcel_id
            : 1,
        transfer_type: body.transfer_type,
        product_id: +body.product_id,
        grade_id: +body.grade_id,
        outturn_qty_mt: 0,
        outturn_qty_cm: 0,
        partial_discharge_flag: "N",
        country_origin: body.country_origin,
        parcel_comment: body.parcel_comment,
        transfer_status: body.transfer_status,
        superintendent_id: +body.superintendent_id,
        superveyor_id: +body.superveyor_id,
        adjustment_type: body.adjustment_type,
        transfer_completion_date: new Date(body.transfer_completion_date),
        transfer_completion_date_est_fl: body.transfer_completion_date_est_fl,
        terminal_from_date: new Date(body.terminal_from_date),
        terminal_to_date: new Date(body.terminal_to_date),
        source_terminal_id: +body.source_terminal_id,
        source_tank_id: +body.source_tank_id,
        target_terminal_id: +body.target_terminal_id,
        target_tank_id: +body.target_tank_id,
        transfer_product_id: +body.product_id,
        transfer_grade_id: +body.grade_id,
        units: body.units,
        quantity_mt: +body.quantity_mt,
        quantity_cm: +body.quantity_cm,
        blend_flag: body.blend_flag,
        final_product_id: +body.final_product_id,
        final_grade_id: +body.final_grade_id,
        circulation: new Date(body.circulation),
        final_density: +body.final_density,
        blend_ratio: +body.blend_ratio,
        balance_mt: +body.balance_mt,
        balance_cm: +body.balance_cm,
        tank_comment: body.tank_comment,
        clean_tank_flag: body.clean_tank_flag,
        third_party_stock_flag: body.third_party_stock_flag,
        counterparty_id: +body.counterparty_id,
        movement_parcel_id: +body.movement_parcel_id,
        org_id:+body.company_id,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    await prisma.log_parcel_movements.create({
      data: {
        parcel_movement_id: +old_value.parcel_movement_id,
        voyage_id: +old_value.voyage_id,
        voyage_parcel_id: +old_value.voyage_parcel_id,
        transfer_type: old_value.transfer_type,
        product_id: +old_value.product_id,
        grade_id: +old_value.grade_id,
        outturn_qty_mt: +old_value.outturn_qty_mt,
        outturn_qty_cm: +old_value.outturn_qty_cm,
        partial_discharge_flag: old_value.partial_discharge_flag,
        country_origin: old_value.country_origin,
        parcel_comment: old_value.parcel_comment,
        transfer_status: old_value.transfer_status,
        superintendent_id: +old_value.superintendent_id,
        superveyor_id: +old_value.superveyor_id,
        adjustment_type: old_value.adjustment_type,
        transfer_completion_date: new Date(old_value.transfer_completion_date),
        transfer_completion_date_est_fl:
          old_value.transfer_completion_date_est_fl,
        terminal_from_date: new Date(old_value.terminal_from_date),
        terminal_to_date: new Date(old_value.terminal_to_date),
        source_terminal_id: +old_value.source_terminal_id,
        source_tank_id: +old_value.source_tank_id,
        target_terminal_id: +old_value.target_terminal_id,
        target_tank_id: +old_value.target_tank_id,
        transfer_product_id: +old_value.transfer_product_id,
        transfer_grade_id: +old_value.transfer_grade_id,
        units: old_value.units,
        quantity_mt: +old_value.quantity_mt,
        quantity_cm: +old_value.quantity_cm,
        blend_flag: old_value.blend_flag,
        final_product_id: +old_value.final_product_id,
        final_grade_id: +old_value.final_grade_id,
        circulation: new Date(old_value.circulation),
        final_density: +old_value.final_density,
        blend_ratio: +old_value.blend_ratio,
        balance_mt: +old_value.balance_mt,
        balance_cm: +old_value.balance_cm,
        tank_comment: old_value.tank_comment,
        clean_tank_flag: old_value.clean_tank_flag,
        third_party_stock_flag: old_value.third_party_stock_flag,
        counterparty_id: +old_value.counterparty_id,
        movement_parcel_id: +old_value.movement_parcel_id,
        org_id:+old_value.org_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    if (body.transfer_type === "Pumpover") {
      const old_value1 = await prisma.parcel_movements.create({
        data: {
          voyage_id:
            body.transfer_type === "Load" ||
            body.transfer_type === "Discharge" ||
            body.transfer_type === "Pumpover"
              ? +body.voyage_id
              : 1,
          voyage_parcel_id:
            body.transfer_type === "Load" || body.transfer_type === "Discharge"
              ? +parcel.voyage_parcel_id
              : 1,
          transfer_type: body.transfer_type,
          product_id: +body.product_id,
          grade_id: +body.grade_id,
          outturn_qty_mt: 0,
          outturn_qty_cm: 0,
          partial_discharge_flag: "N",
          country_origin: body.country_origin,
          parcel_comment: body.parcel_comment,
          transfer_status: body.transfer_status,
          superintendent_id: +body.superintendent_id,
          superveyor_id: +body.superveyor_id,
          transfer_completion_date: new Date(body.transfer_completion_date),
          transfer_completion_date_est_fl: body.transfer_completion_date_est_fl,
          terminal_from_date: new Date(body.terminal_from_date),
          terminal_to_date: new Date(body.terminal_to_date),
          source_terminal_id: +body.target_terminal_id,
          source_tank_id: +body.target_tank_id,
          target_terminal_id: null,
          target_tank_id: null,
          quantity_mt: +body.quantity_mt,
          quantity_cm: +body.quantity_cm,
          tank_comment: body.tank_comment,
          clean_tank_flag: body.clean_tank_flag,
          third_party_stock_flag: body.third_party_stock_flag,
          transfer_product_id: +body.transfer_product_id,
          transfer_grade_id: +body.transfer_grade_id,
          blend_flag: body.blend_flag,
          final_product_id: +body.final_product_id,
          final_grade_id: +body.final_grade_id,
          circulation: new Date(body.circulation),
          final_density: +body.final_density,
          blend_ratio: +body.blend_ratio,
          balance_mt: +body.target_balance_mt,
          balance_cm: +body.target_balance_cm,
          units: body.units,
          counterparty_id: null,
          adjustment_type: body.adjustment_type,
          movement_parcel_id: +body.movement_parcel_id,
          org_id:+body.company_id,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });

      const isBalance1 = await prisma.parcel_movement_balance.findFirst({
        where: {
          terminal_id: +body.target_terminal_id,
          tank_id: +body.target_tank_id,
          status: "A",
        },
      });

      if (!isBalance1) {
        await prisma.parcel_movement_balance.create({
          data: {
            transfer_status: body.transfer_status,
            transfer_type: body.transfer_type,
            balance_date: new Date(),
            terminal_id: +body.target_terminal_id,
            tank_id: +body.target_tank_id,
            country_origin: body.country_origin,
            final_product_id: +body.final_product_id,
            final_grade_id: +body.final_grade_id,
            units: body.units,
            final_blend_ratio: +body.blend_ratio,
            final_density: +body.final_density,
            final_balance_mt: +body.target_balance_mt,
            final_balance_cm: +body.target_balance_cm,
            created_by: body.username,
            last_updated_by: body.username,
 
          },
        });
      } else {
        await prisma.parcel_movement_balance.update({
          data: {
            transfer_status: body.transfer_status,
            transfer_type: body.transfer_type,
            balance_date: new Date(),
            terminal_id: +body.target_terminal_id,
            tank_id: +body.target_tank_id,
            country_origin: body.country_origin,
            final_product_id: +body.final_product_id,
            final_grade_id: +body.final_grade_id,
            units: body.units,
            final_blend_ratio: +body.blend_ratio,
            final_density: +body.final_density,
            final_balance_mt: +body.target_balance_mt,
            final_balance_cm: +body.target_balance_cm,
            last_update_date: new Date(),
            last_updated_by: body.username,
          },
          where: { balance_id: isBalance1.balance_id, status: "A" },
        });
      }

      await prisma.log_parcel_movements.create({
        data: {
          parcel_movement_id: +old_value1.parcel_movement_id,
          voyage_id: +old_value1.voyage_id,
          voyage_parcel_id: +old_value1.voyage_parcel_id,
          transfer_type: old_value1.transfer_type,
          product_id: +old_value1.product_id,
          grade_id: +old_value1.grade_id,
          outturn_qty_mt: +old_value1.outturn_qty_mt,
          outturn_qty_cm: +old_value1.outturn_qty_cm,
          partial_discharge_flag: old_value1.partial_discharge_flag,
          country_origin: old_value1.country_origin,
          parcel_comment: old_value1.parcel_comment,
          transfer_status: old_value1.transfer_status,
          superintendent_id: +old_value1.superintendent_id,
          superveyor_id: +old_value1.superveyor_id,
          adjustment_type: old_value1.adjustment_type,
          transfer_completion_date: new Date(
            old_value1.transfer_completion_date
          ),
          transfer_completion_date_est_fl:
            old_value1.transfer_completion_date_est_fl,
          terminal_from_date: new Date(old_value1.terminal_from_date),
          terminal_to_date: new Date(old_value1.terminal_to_date),
          source_terminal_id: +old_value1.source_terminal_id,
          source_tank_id: +old_value1.source_tank_id,
          target_terminal_id: +old_value1.target_terminal_id,
          target_tank_id: +old_value1.target_tank_id,
          transfer_product_id: +old_value1.transfer_product_id,
          transfer_grade_id: +old_value1.transfer_grade_id,
          units: old_value1.units,
          quantity_mt: +old_value1.quantity_mt,
          quantity_cm: +old_value1.quantity_cm,
          blend_flag: old_value1.blend_flag,
          final_product_id: +old_value1.final_product_id,
          final_grade_id: +old_value1.final_grade_id,
          circulation: new Date(old_value1.circulation),
          final_density: +old_value1.final_density,
          blend_ratio: +old_value1.blend_ratio,
          balance_mt: +old_value1.balance_mt,
          balance_cm: +old_value1.balance_cm,
          tank_comment: old_value1.tank_comment,
          clean_tank_flag: old_value1.clean_tank_flag,
          third_party_stock_flag: old_value1.third_party_stock_flag,
          counterparty_id: +old_value.counterparty_id,
          movement_parcel_id: +old_value.movement_parcel_id,
          org_id: +old_value.org_id,
          created_by: old_value1.created_by,
          creation_date: old_value1.creation_date,
          last_updated_by: old_value1.last_updated_by,
          last_update_date: old_value1.last_update_date,
        },
      });
    }

    if (!isBalance) {
      await prisma.parcel_movement_balance.create({
        data: {
          transfer_status: body.transfer_status,
          transfer_type: body.transfer_type,
          balance_date: new Date(),
          terminal_id: +body.source_terminal_id,
          tank_id: +body.source_tank_id,
          country_origin: body.country_origin,
          final_product_id: +body.final_product_id,
          final_grade_id: +body.final_grade_id,
          units: body.units,
          final_blend_ratio: +body.blend_ratio,
          final_density: +body.final_density,
          final_balance_mt: +body.balance_mt,
          final_balance_cm: +body.balance_cm,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });
    } else {
      await prisma.parcel_movement_balance.update({
        data: {
          transfer_status: body.transfer_status,
          transfer_type: body.transfer_type,
          balance_date: new Date(),
          terminal_id: +body.source_terminal_id,
          tank_id: +body.source_tank_id,
          country_origin: body.country_origin,
          final_product_id: +body.final_product_id,
          final_grade_id: +body.final_grade_id,
          units: body.units,
          final_blend_ratio: +body.blend_ratio,
          final_density: +body.final_density,
          final_balance_mt: +body.balance_mt,
          final_balance_cm: +body.balance_cm,
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { balance_id: isBalance.balance_id, status: "A" },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    if (body.transfer_type === "Load" || body.transfer_type === "Discharge") {
      const port_type =
        body.transfer_type === "Load" ? "STS_load" : "STS_disport";

      const isPort = await prisma.voyage_ports.findFirst({
        where: {
          port_type: port_type,
          voyage_id: +body.voyage_id,
          status: "A",
        },
      });

      let ports = null;

      if (!isPort) {
        ports = await prisma.voyage_ports.create({
          data: {
            voyage_id: +body.voyage_parcel_id,
            port_id: 1,
            port_type: port_type,
            agent_id: null,
            superveyor_id: +body.superveyor_id,
            superintendent_id: +body.superintendent_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        ports = isPort;
      }

      await prisma.voyage_parcels.update({
        data: {
          transfer_date: new Date(body.transfer_completion_date),
          transfer_date_est_flag: body.transfer_completion_date_est_fl,
          border_crossing_date: new Date(body.transfer_completion_date),
          border_crossing_date_est_flag: body.transfer_completion_date_est_fl,
          supply_start_date: new Date(body.transfer_completion_date),
          supply_end_date: new Date(body.transfer_completion_date),
          transfer_detail_date: new Date(body.transfer_completion_date),
          shipping_quantity_mt: +body.quantity_mt,
          shipping_quantity_cm: +body.quantity_cm,
          shipping_quantity_b: 0,
          counterparty_id: +body.counterparty_id,
          terminal_id: +body.source_terminal_id,
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
      });
    }

    const response = await prisma.parcel_movements.update({
      data: {
        parcel_comment: body.parcel_comment,
        transfer_status: body.transfer_status,
        superintendent_id: +body.superintendent_id,
        superveyor_id: +body.superveyor_id,
        transfer_completion_date: new Date(body.transfer_completion_date),
        transfer_completion_date_est_fl: body.transfer_completion_date_est_fl,
        terminal_from_date: new Date(body.terminal_from_date),
        terminal_to_date: new Date(body.terminal_to_date),
        source_terminal_id: +body.source_terminal_id,
        source_tank_id: +body.source_tank_id,
        target_terminal_id: +body.target_terminal_id,
        target_tank_id: +body.target_tank_id,
        quantity_mt: +body.quantity_mt,
        quantity_cm: +body.quantity_cm,
        tank_comment: body.tank_comment,
        clean_tank_flag: body.clean_tank_flag,
        third_party_stock_flag: body.third_party_stock_flag,
        transfer_product_id: +body.transfer_product_id,
        transfer_grade_id: +body.transfer_grade_id,
        blend_flag: body.blend_flag,
        final_product_id: +body.final_product_id,
        final_grade_id: +body.final_grade_id,
        circulation: new Date(body.circulation),
        final_density: +body.final_density,
        blend_ratio: +body.blend_ratio,
        balance_mt: +body.balance_mt,
        balance_cm: +body.balance_cm,
        units: body.units,
        adjustment_type: body.adjustment_type,
        counterparty_id: +body.counterparty_id,
        org_id:+body.company_id,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { parcel_movement_id: +body.parcel_movement_id, status: "A" },
    });

    const isBalance = await prisma.parcel_movement_balance.findFirst({
      where: {
        terminal_id: +body.source_terminal_id,
        tank_id: +body.source_tank_id,
        status: "A",
      },
    });

    if (!isBalance) {
      await prisma.parcel_movement_balance.create({
        data: {
          transfer_status: body.transfer_status,
          transfer_type: body.transfer_type,
          balance_date: new Date(),
          terminal_id: +body.source_terminal_id,
          tank_id: +body.source_tank_id,
          country_origin: body.country_origin,
          final_product_id: +body.final_product_id,
          final_grade_id: +body.final_grade_id,
          units: body.units,
          final_blend_ratio: +body.blend_ratio,
          final_density: +body.final_density,
          final_balance_mt: +body.balance_mt,
          final_balance_cm: +body.balance_cm,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });
    } else {
      await prisma.parcel_movement_balance.update({
        data: {
          transfer_status: body.transfer_status,
          transfer_type: body.transfer_type,
          balance_date: new Date(),
          terminal_id: +body.source_terminal_id,
          tank_id: +body.source_tank_id,
          country_origin: body.country_origin,
          final_product_id: +body.final_product_id,
          final_grade_id: +body.final_grade_id,
          units: body.units,
          final_blend_ratio: +body.blend_ratio,
          final_density: +body.final_density,
          final_balance_mt: +body.balance_mt,
          final_balance_cm: +body.balance_cm,
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { balance_id: isBalance.balance_id, status: "A" },
      });
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PATCH(request) {
  const body = await request.json();

  try {
    const arr = [];

    const terminal = await prisma.$queryRaw`select terminal_name
    from storage_terminal
    where terminal_id = ${+body.terminal_id}
      and status = 'A'`;

    const sql = `select tank_id,tank_name as Tank
    from storage_tank
    where tank_id IN (${body.tanks.map((v) => `${v.tank_id}`).join(",")})
        and terminal_id = ${+body.terminal_id}
        and status = 'A'
    order by tank_name`;

    const tanks = await prisma.$queryRaw(Prisma.raw(sql));

    const sql1 = `select aa2.transfer_completion_date transfer_date,aa2.transfer_type,aa2.grade,aa2.quantity_mt,aa2.balance_mt,aa2.source_tank_id,aa2.blend_flag,aa2.counterparty_id,aa2.final_density,aa2.org_id
		from (
				select generate_series('${body.begin_date}', '${
      body.end_date
    }', '1 day'::interval) as transfer_Date
				) aa1 
				inner join (
									select
                     bb1.parcel_movement_id,                     
                     bb1.transfer_type,
                     bb1.transfer_completion_date,
                     bb1.source_terminal_id,
                     bb1.source_tank_id,
                     bb1.product_id,
                     bb1.grade_id,
                     bb4.grade_name as grade,
                     bb1.units,
                     bb1.quantity_mt,
                     bb1.balance_mt,
                     bb1.blend_flag,
                     bb1.counterparty_id,
                     bb1.final_density,
                     bb1.org_id
                   from parcel_movements bb1                      
                      inner join product_grades bb4
                        on bb1.product_id = bb4.product_id
                        and bb1.grade_id = bb4.grade_id
                        and bb4.status = 'A'
                    where bb1.source_tank_id in (${body.tanks
                      .map((v) => `${v.tank_id}`)
                      .join(",")})
                       and bb1.source_terminal_id = ${+body.terminal_id}
                       and bb1.status = 'A'
                      ) aa2
				on to_char(aa1.transfer_Date,'YYYY-MM-DD') = to_char(aa2.transfer_completion_date,'YYYY-MM-DD')
		where to_char(aa1.transfer_Date,'YYYY-MM-DD') >= '${body.begin_date}'
		and to_char(aa1.transfer_Date,'YYYY-MM-DD') <= '${body.end_date}'
    order by aa1.transfer_Date desc`;

    const transfer_dates = await prisma.$queryRaw(Prisma.raw(sql1));

    for (let x of terminal) {
      const transfer = [];
      const trankuud = [];

      for (let a of tanks) {
        trankuud.push({
          tank_id: a.tank_id,
          tank: a.tank,
        });
      }

      for (let z of transfer_dates) {
        const tankdtl = [];
        for (let y of tanks) {
          const detail = [];

          for (let k of transfer_dates) {
            if (
              y.tank_id === k.source_tank_id &&
              z.transfer_date === k.transfer_date
            ) {
              detail.push({
                grade_id: k.grade,
                quantity_mt: k.quantity_mt,
                balance_mt: k.balance_mt,
                blend_flag: k.blend_flag,
                counterparty_id: k.counterparty_id,
                final_density: k.final_density,
              });
            }
          }

          tankdtl.push({
            tank_id: y.tank_id,
            tank: y.tank,
            details: detail,
          });
        }
        transfer.push({
          transfer_date: z.transfer_date,
          transfer_type: z.transfer_type,
          tanks: tankdtl,
        });
      }

      arr.push({
        terminal_name: x.terminal_name,
        tankuud: trankuud,
        details: transfer,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {

    const transfer = await prisma.parcel_movements.findFirst({     
      where: { parcel_movement_id: +body.parcel_movement_id, status: "A" },
    });

    const response = await prisma.parcel_movements.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { parcel_movement_id: +body.parcel_movement_id, status: "A" },
    });

    const isBalance = await prisma.parcel_movement_balance.findFirst({
      where: {
        terminal_id: +response.source_terminal_id,
        tank_id: +response.source_tank_id,
        status: "A",
      },
    });
    await prisma.parcel_movement_balance.update({
      data: {
        final_balance_mt: isBalance.final_balance_mt - response.quantity_mt,
        final_balance_cm: isBalance.final_balance_cm - response.quantity_cm,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { balance_id: isBalance.balance_id, status: "A" },
    });

    if (transfer.transfer_type==="Load" || transfer.transfer_type==="Discharge") {
    await prisma.voyage_parcels.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_parcel_id: +response.voyage_parcel_id, status: "A" },
    });
  }

    return tokenResponse(request);
  } catch (error) {
    console.log("error",error)
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
