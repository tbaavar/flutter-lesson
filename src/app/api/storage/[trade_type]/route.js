import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                select aa1.voyage_id,aa1.voyage_title
                from voyages aa1
                    inner join voyage_parcels aa2
                        on aa1.voyage_id = aa2.voyage_id
                        and aa2.status = 'A'
                    inner join trades aa3
                        on aa2.trade_id = aa3.trade_id
                        and aa3.status = 'A'
                where aa1.status= 'A' and aa3.trade_type=${params.trade_type}`;
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
