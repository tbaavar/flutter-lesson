import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
    select
      aa1.log_parcel_movement_id,
      aa1.parcel_movement_id,
      aa2.voyage_title,	
      aa9.terminal_name,
      aa10.tank_name,
      aa1.transfer_type,
      aa4.product_name,
      aa5.grade_name,
      aa1.outturn_qty_mt,
      aa1.outturn_qty_cm,
      aa11.name country_origin,
      aa1.parcel_comment,
      aa7.name transfer_status,
      aa8.name adjustment_type,
      aa1.transfer_completion_date,
      case when aa1.transfer_completion_date_est_fl='Y' then 'Checked' else 'UnChecked' end transfer_completion_date_est_fl,
      aa1.terminal_from_date,
      aa1.terminal_to_date,
      aa1.quantity_mt,
      aa1.quantity_cm,
      case when aa1.clean_tank_flag='Y' then 'Checked' else 'UnChecked' end clean_tank_flag,
      case when aa1.blend_flag='Y' then 'Checked' else 'UnChecked' end blend_flag,
      aa14.product_name transfer_product_name,
      aa15.grade_name transfer_grade_name,
      aa1.circulation,
      aa1.blend_ratio,
      aa12.product_name final_product_name,
      aa13.grade_name final_grade_name,
      aa1.final_density,
      aa1.balance_mt,
      aa1.balance_cm,
      aa1.tank_comment,
      aa1.latest_change_flag,
      aa1.status,
      aa1.created_by,
      aa1.creation_date,
      aa1.last_updated_by,
      aa1.last_update_date
    from log_parcel_movements aa1
      left join voyages aa2
        on aa1.voyage_id = aa2.voyage_id
        and aa2.status = 'A'
      left join bs_lookups aa3
        on aa1.transfer_type = aa3.lookup_code
        and aa3.lookup_type = 'Transfer_type'
        and aa3.status = 'A'
      left join products aa4
        on aa1.product_id = aa4.product_id
        and aa4.status = 'A'
      left join product_grades aa5
        on aa1.product_id = aa5.product_id
        and aa1.grade_id = aa5.grade_id
        and aa5.status = 'A'
      left join bs_lookups aa6
        on aa1.country_origin = aa6.lookup_code
        and aa6.lookup_type = 'Transfer_type'
        and aa6.status = 'A'
      left join bs_lookups aa7
        on aa1.transfer_status = aa7.lookup_code
        and aa7.lookup_type = 'Adjustment_status'
        and aa7.status = 'A'
      left join bs_lookups aa8
        on aa1.adjustment_type = aa8.lookup_code
        and aa8.lookup_type = 'Adjustment_type'
        and aa8.status = 'A'
      left join storage_terminal aa9
        on aa1.source_terminal_id = aa9.terminal_id
        and aa9.status = 'A'
      left join storage_tank aa10
        on aa1.source_terminal_id = aa10.terminal_id
        and aa1.source_tank_id = aa10.tank_id
        and aa9.status = 'A'
      left join bs_lookups aa11
        on aa1.country_origin = aa11.lookup_code
        and aa11.lookup_type = 'Country_Origin'
        and aa11.status = 'A'
      left join products aa12
        on aa1.final_product_id = aa12.product_id
        and aa12.status = 'A'
      left join product_grades aa13
        on aa1.final_product_id = aa13.product_id
        and aa1.final_grade_id = aa13.grade_id
        and aa13.status = 'A'	
      left join products aa14
        on aa1.transfer_product_id = aa14.product_id
        and aa14.status = 'A'
      left join product_grades aa15
        on aa1.transfer_product_id = aa15.product_id
        and aa1.transfer_grade_id = aa15.grade_id
        and aa13.status = 'A'	
    order by aa1.parcel_movement_id asc,aa1.last_update_date desc`;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
