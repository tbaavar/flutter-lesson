import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                select 
                    aa1.voyage_id,
                    max(aa1.voyage_title) voyage_title,
                    max(aa2.product_id) product_id,
                    max(aa3.product_name) product_name,
                    max(aa2.grade_id) grade_id,
                    max(aa4.grade_name) grade_name,
                    max(aa2.country_origin) country_origin,
                    max(aa5.name) country_origin_name,
                    max(aa6.units) unit,
                    max(aa7.name) unit_name,
                    max(aa6.est_density) est_density,
                    sum(case when aa2.trade_code = 'P' then aa2.shipping_quantity_mt end) quantity_mt,
                    sum(case when aa2.trade_code = 'P' then aa2.shipping_quantity_mt end)*max(aa6.est_density) as quantity_cm
                  from voyages aa1
                    inner join voyage_parcels aa2
                      on aa1.voyage_id = aa2.voyage_id
                      and aa2.status = 'A'
                    inner join products aa3
                      on aa2.product_id = aa3.product_id
                      and aa3.status = 'A'
                    inner join product_grades aa4
                      on aa2.product_id = aa4.product_id
                      and aa2.grade_id = aa4.grade_id
                      and aa4.status = 'A'
                    inner join bs_lookups aa5
                      on aa2.country_origin = aa5.lookup_code
                      and aa5.lookup_type = 'Country_Origin'
                      and aa5.status = 'A'
                    inner join trade_products aa6
                      on aa2.trade_id = aa6.trade_id
                      and aa6.status = 'A'
                    inner join bs_lookups aa7
                      on aa6.units = aa7.lookup_code
                      and aa7.lookup_type = 'Unit'
                      and aa7.status = 'A'
                  where aa1.status= 'A'
                    and  aa1.org_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
                  group by aa1.voyage_id`;

    const parcels = await prisma.$queryRaw`
                    select 
                        aa1.voyage_parcel_id,
                        aa1.trade_id,
                        aa1.parcel_number,
                        aa1.invoicing_unit,
                        aa1.transfer_date,
                        aa2.name counterparty_name,
                        aa1.product_id,
                        aa3.product_name,
                        aa1.grade_id,
                        aa4.grade_name,
                        round(aa1.shipping_quantity_mt - COALESCE(aa8.quantity_mt,0),3) as shipping_quantity_mt,
                        round(aa1.shipping_quantity_cm - COALESCE(aa8.quantity_cm,0),3)  as shipping_quantity_cm,
                        aa1.trade_code,
                        aa1.operator_user_id,
                        aa1.counterparty_id,
                        aa10.amount,
                        aa10.currency,
                        aa1.voyage_id,
                        aa1.country_origin,
                        aa5.name country_origin_name,
                        aa6.est_density,
                        aa6.units,
                        aa7.name unit_name
                    from voyage_parcels aa1
                        inner join legal_entity aa2
                            on aa1.counterparty_id = aa2.legal_entity_id
                            and aa2.status = 'A'
                        inner join products aa3
                            on aa1.product_id = aa3.product_id
                            and aa3.status = 'A'
                        inner join product_grades aa4
                            on aa1.grade_id = aa4.grade_id
                            and aa1.product_id = aa4.product_id
                            and aa4.status = 'A'
                        left join trade_pricing_fixed aa10
                            on aa1.trade_id = aa10.trade_id
                            and aa10.status = 'A'
                        left join bs_lookups aa5
                            on aa1.country_origin = aa5.lookup_code
                            and aa5.lookup_type = 'Country_Origin'
                            and aa5.status = 'A'
                        inner join trade_products aa6
                            on aa1.trade_id = aa6.trade_id
                            and aa6.status = 'A'
                        inner join bs_lookups aa7
                            on aa6.units = aa7.lookup_code
                              and aa7.lookup_type = 'Unit'
                              and aa7.status = 'A'
                        left join (
                              select voyage_id,movement_parcel_id,sum(quantity_mt) ::numeric as quantity_mt,sum(quantity_cm)::numeric as quantity_cm
                              from parcel_movements
                              where status = 'A'
                              group by voyage_id,movement_parcel_id
                                ) aa8
                            on aa1.voyage_id = aa8.voyage_id
                            and aa1.voyage_parcel_id = aa8.movement_parcel_id
                    where aa1.port_type =${params.port_type}
                          and aa1.status = 'A'`;

    const arr = [];

    for (let x of response) {
      const parcel = [];
      for (let b of parcels) {
        if (x.voyage_id === b.voyage_id) {
          parcel.push(b);
        }
      }


      arr.push({
        voyage_id: x.voyage_id,
        voyage_title: x.voyage_title,
        product_id:x.product_id,
        product_name:x.product_name,
        grade_id:x.grade_id,
        grade_name:x.grade_name,
        country_origin:x.country_origin,
        country_origin_name:x.country_origin_name,
        unit:x.unit,
        unit_name:x.unit_name,
        est_density:x.est_density,
        quantity_mt:x.quantity_mt,
        quantity_cm:x.quantity_cm,
        parcels: parcel,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
