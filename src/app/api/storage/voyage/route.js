import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                select 
                    aa1.voyage_id,
                    max(aa1.voyage_title) voyage_title,
                    max(aa2.product_id) product_id,
                    max(aa3.product_name) product_name,
                    max(aa2.grade_id) grade_id,
                    max(aa4.grade_name) grade_name,
                    max(aa2.country_origin) country_origin,
                    max(aa5.name) country_origin_name,
                    max(aa6.units) unit,
                    max(aa7.name) unit_name,
                    max(aa6.est_density) est_density,
                    sum(case when aa2.trade_code = 'P' then aa2.shipping_quantity_mt end) quantity_mt,
                    sum(case when aa2.trade_code = 'P' then aa2.shipping_quantity_mt end)*max(aa6.est_density) as quantity_cm
                  from voyages aa1
                    inner join voyage_parcels aa2
                      on aa1.voyage_id = aa2.voyage_id
                      and aa2.status = 'A'
                    inner join products aa3
                      on aa2.product_id = aa3.product_id
                      and aa3.status = 'A'
                    inner join product_grades aa4
                      on aa2.product_id = aa4.product_id
                      and aa2.grade_id = aa4.grade_id
                      and aa4.status = 'A'
                    inner join bs_lookups aa5
                      on aa2.country_origin = aa5.lookup_code
                      and aa5.lookup_type = 'Country_Origin'
                      and aa5.status = 'A'
                    inner join trade_products aa6
                      on aa2.trade_id = aa6.trade_id
                      and aa6.status = 'A'
                    inner join bs_lookups aa7
                      on aa6.units = aa7.lookup_code
                      and aa7.lookup_type = 'Unit'
                      and aa7.status = 'A'
                  where aa1.status= 'A'
                  group by aa1.voyage_id`;

    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
