import prisma from "@/lib/prisma";
import { Prisma, PrismaClient } from "@prisma/client";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`                    
				select 
					aa1.balance_id,
					aa1.terminal_id,
					aa2.terminal_name,
					aa1.tank_id,
					aa3.tank_name,
					aa1.final_product_id,
					aa4.product_name final_product_name,
					aa1.final_grade_id,
					aa5.grade_name final_grade_name,
					aa1.final_balance_cm,
					aa1.final_balance_mt,
					aa1.final_density,
					aa6.transfer_completion_date,
					CURRENT_DATE -  aa6.transfer_completion_date|| ' day' latestday
				from parcel_movement_balance aa1
					inner join storage_terminal aa2
						on aa1.terminal_id = aa2.terminal_id
						and aa2.status = 'A'
					inner join storage_tank aa3
						on aa1.tank_id = aa3.tank_id	
						and aa3.tank_type= 'OnWay'
						and aa3.status = 'A'
					inner join products aa4	
						on aa1.final_product_id = aa4.product_id
						and aa4.status = 'A'
					inner join product_grades aa5
						on aa1.final_product_id = aa5.product_id
						and aa1.final_grade_id = aa5.grade_id
						and aa5.status = 'A'
					left join (
							select source_terminal_id, source_tank_id,max(transfer_completion_date) transfer_completion_date
							from parcel_movements
							where status = 'A'
							group by source_terminal_id,source_tank_id
							) aa6
						on aa1.terminal_id = aa6.source_terminal_id
						and aa1.tank_id = aa6.source_tank_id
				where round(aa1.final_balance_mt)>0
					and round(aa1.final_balance_cm)>0
					and aa2.org_id in (select org_id from bs_org_users where user_id = ${+params.user_id})`;
        
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
