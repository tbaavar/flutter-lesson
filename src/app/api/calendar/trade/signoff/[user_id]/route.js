import prisma from "@/lib/prisma";
import { Prisma, PrismaClient } from "@prisma/client";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
		select  
				aa1.trade_id,			
				aa16.deal_id,
				aa12.name company_name,
				aa6.name country_party_name,
				aa10.name trade_type_name,
				aa1.trade_type trade_type,		
				aa3.product_name,			
				aa5.grade_name,
				aa4.date_range_from,
				aa4.date_range_to,		
				aa2.est_bl_date,
				aa4.intended_vessel_id,
				aa17.name vessel,
				round(aa11.amount * aa2.quantity) || ' '|| aa11.currency price,
				aa13.fixed_date pricing_date,
				aa2.quantity || ' '|| aa2.units quantity,
				COALESCE(aa8.allocated_quantity, 0)  || ' '|| aa2.units allocated_quantity,
				COALESCE(aa8.unallocated_quantity_mt, 0)   || ' '|| aa2.units unallocated_quantity,
				CURRENT_DATE -  aa2.est_bl_date || ' day' latestday,
				aa1.operator_sign_off_flag,
				case when aa1.operator_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end operation_flag,
				aa1.finance_sign_off_flag,
				case when aa1.finance_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end finance_flag,
				substring(aa7.lastname,1,1) || '.'|| aa7.firstname trader_name,
				substring(aa9.lastname,1,1) || '.' || aa9.firstname operator_name,
				substring(aa14.lastname,1,1) || '.' || aa14.firstname finance_name,
				substring(aa15.lastname,1,1) || '.' || aa15.firstname contract_name
		from trades aa1
		left join trade_products aa2
			on aa1.trade_id = aa2.trade_id
		left  join products aa3
			on aa2.product_id = aa3.product_id
			and aa3.status = 'A'
		left join trade_deliveries aa4
			on aa1.trade_id = aa4.trade_id
			and aa4.status = 'A'
		left join product_grades aa5
			on aa2.product_id = aa5.product_id
			and aa2.grade_id = aa5.grade_id
			and aa5.status = 'A'
		left join legal_entity aa6
			on aa1.counterparty_id = aa6.legal_entity_id
			and aa6.status='A'
		left join bs_users aa7
			on aa1.trader_user_id = aa7.user_id
			and aa7.status = 'A'
		left join (
				select bb1.trade_id,sum(bb1.allocated_quantity) allocated_quantity,sum(bb2.unallocated_quantity_mt) unallocated_quantity_mt
				from (
					select trade_id,voyage_id,sum(shipping_quantity_mt) allocated_quantity
					from voyage_parcels
					where status = 'A'
					group by  trade_id,voyage_id
					) bb1
					inner join voyage_unallocated_parcels bb2
						on bb1.voyage_id = bb2.voyage_id
						and bb1.trade_id = bb2.trade_id
						and bb2.status = 'A'
				group by bb1.trade_id
				) aa8
			on aa1.trade_id = aa8.trade_id
		left join bs_users aa9
			on aa1.operator_user_id = aa9.user_id		
			and aa9.status = 'A'
		left join bs_lookups aa10
			on aa1.trade_type = aa10.lookup_code
			and aa10.lookup_type = 'Trade_type'
			and aa10.status = 'A'
		left join (
				select trade_id,sum(amount) amount,max(currency) currency,max(unit) per_unit
				from trade_pricing_fixed						
				where status = 'A'
				group by trade_id
				) aa11
			on aa1.trade_id = aa11.trade_id
		left join bs_organizations aa12
			on aa1.company_id = aa12.org_id
			and aa12.status = 'A'
		left join trade_payments aa13
			on aa1.trade_id = aa13.trade_id
			and aa13.status = 'A'
		left join bs_users aa14
			on aa1.finance_user_id = aa14.user_id	
			and aa14.status = 'A'
		left join bs_users aa15
			on aa1.contract_user_id = aa15.user_id	
			and aa15.status = 'A'
		left join deal_trades aa16
			on aa1.trade_id = aa16.trade_id
			and aa16.status = 'A'
		left join bs_lookups aa17
			on aa4.intended_vessel_id = aa17.lookup_code
			and aa17.lookup_type = 'Intended_Vessel'
			and aa17.status = 'A'
where aa1.status = 'A' and (aa1.operator_sign_off_flag = 'N' or aa1.finance_sign_off_flag = 'N')
				and aa1.company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
order by aa1.trade_type,aa1.trade_date desc`;
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
