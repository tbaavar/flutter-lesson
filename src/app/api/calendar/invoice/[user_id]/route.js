import prisma from "@/lib/prisma";
import { Prisma, PrismaClient } from "@prisma/client";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`                    
select
		dd1.invoice_id,
		dd1.deal_id,
		dd1.trade_id,
		dd1.company_name || ' <'|| dd1.trade_type || '> ' || dd1.terms || '-' || dd1.product_name || '-' ||  quantity || ' ' || unit as trade_info,
		dd1.transport_type,
		dd1.counterparty_name,
		dd1.invoice_number,
		dd1.invoice_type,
		dd1.invoice_type_name,
		dd1.invoice_date,
		dd1.invoice_due_date,
		dd1.invoice_payment_status,
		dd1.invoice_state,
		round(dd1.est_amount) || ' ' || currency est_amount,
		dd1.invoice_amount,
		dd1.est_amount - dd1.invoice_amount || ' ' || dd1.currency difference,
		dd1.credit_type,
		dd1.credit_type_name,
		COALESCE(dd1.paid_amount,0) || ' ' || dd1.currency paid_amount,
		dd1.invoice_amount -  COALESCE(dd1.paid_amount,0) || ' ' || dd1.currency as unpaid_amount,
		CURRENT_DATE -  dd1.invoice_due_date|| ' day' latestday
from (
		SELECT
				aa1.invoice_id,
				aa7.deal_id,
				aa5.trade_id,				
				aa1.company_id,
				aa2.name company_name,
				case when aa6.trade_type in ('Spot_Sale') then 'SS'
						 when aa6.trade_type in ('Spot_Purchase') then 'SP'	
						 when aa6.trade_type in ('Term_Sale') then 'TS'
						 when aa6.trade_type in ('Term_Purchase') then 'TP' end trade_type,
				aa6.terms,
				aa5.shipping_quantity_mt quantity,
				aa13.product_name,
				aa1.counterparty_id,
				aa3.name counterparty_name,
				aa1.invoice_number,
				aa1.invoice_type,
				aa1.invoice_state,
				aa4.name invoice_type_name,
				aa1.invoice_date,
				aa1.invoice_due_date,
				aa8.amount,
				aa8.unit,
				aa9.credit_type,		
				aa1.currency,
				aa1.invoice_amount,
				aa12.paid_amount,
				aa1.invoice_payment_status,
				aa1.invoice_status,
				aa11.intended_vessel_id transport_type,
				aa5.shipping_quantity_mt*aa8.amount est_amount,
				aa14.name credit_type_name
		FROM invoices aa1
			inner join bs_organizations aa2
				on aa1.company_id = aa2.org_id
				and aa2.status = 'A'
			inner join legal_entity aa3
				on aa1.counterparty_id = aa3.legal_entity_id
				and aa3.status = 'A'
			inner join bs_lookups aa4
				on aa1.invoice_type = aa4.lookup_code
				and aa4.lookup_type = 'Invoice_type'
			inner join voyage_parcels aa5
				on aa1.voyage_parcel_id = aa5.voyage_parcel_id
				and aa5.status = 'A'
			inner join trades aa6
				on aa5.trade_id = aa6.trade_id
				and aa6.status = 'A'
			inner join deal_trades aa7
				on aa5.trade_id  = aa7.trade_id
				and aa7.status = 'A'
			inner join trade_pricing_fixed aa8
				on aa5.trade_id = aa8.trade_id
				and aa8.status = 'A'
			inner join trade_payments aa9
				on aa5.trade_id = aa9.trade_id
				and aa9.status = 'A'
			inner join trade_products aa10
				on aa5.trade_id = aa10.trade_id
				and aa10.status = 'A'
			inner join trade_deliveries aa11
				on aa5.trade_id = aa11.trade_id
				and aa11.status = 'A'
			left join (
						select invoice_id,sum(value) paid_amount
						from invoice_details
						where balance_type = 'Credit' 
									and status = 'A'
							group by invoice_id
						) aa12
				on aa1.invoice_id= aa12.invoice_id
			left join products aa13
				on aa10.product_id = aa13.product_id
				and aa13.status = 'A'
			left join bs_lookups aa14
				on aa9.credit_type = aa14.lookup_code
				and aa14.lookup_type = 'Credit_Type'
			left join bs_lookups aa15
				on aa11.intended_vessel_id = aa15.lookup_code
				and aa15.lookup_type = 'Intended_Vessel'
				and aa15.status = 'A'
		where aa1.company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
		) dd1
		`;
        
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
