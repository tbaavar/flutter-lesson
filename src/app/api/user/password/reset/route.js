import prisma from "@/lib/prisma";
import * as bcrypt from "bcrypt";
import { tokenResponse } from "@/lib/response";

export async function PUT(request) {
  const body = await request.json();
  console.log("body", body);
  try {
    await prisma.bs_users.update({
      data: {
        password: await bcrypt.hash(body.password, 10),
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { user_id: +body.user_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log("error", error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
