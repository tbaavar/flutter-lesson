import prisma from "@/lib/prisma";
import * as bcrypt from "bcrypt";
import { tokenResponse } from "@/lib/response";

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_users.update({
      data: {
        password: await bcrypt.hash(body.newpassword, 10),
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { user_id: +body.user_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
