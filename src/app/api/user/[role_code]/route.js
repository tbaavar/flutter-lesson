import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.bs_users.findMany({
      select: {
        user_id: true,
        lastname: true,
        firstname: true,
        mail: true,
      },
      where: { role: params.role_code, status: "A" },
    });

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
