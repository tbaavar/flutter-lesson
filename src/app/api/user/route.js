import prisma from "@/lib/prisma";
import * as bcrypt from "bcrypt";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.bs_users.findMany({
      select: {
        user_id: true,
        username: true,
        lastname: true,
        firstname: true,
        position: true,
        role: true,
        phone: true,
        mail: true,
      },
      where: { status: "A" },
    });

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    const user = await prisma.bs_users.findFirst({
      select: {
        user_id: true,
        username: true,
        password: true,
        role: true,
        lastname: true,
        firstname: true,
        mail: true,
        position: true,
        phone: true,
      },
      where: { username: body.username, status: "A" },
    });

    if (user) {
      return Response.json(
        { message: "Error", result: "Exist user check your username!" },
        { status: 500 }
      );
    }
    const response = await prisma.bs_users.create({
      data: {
        username: body.username,
        lastname: body.lastname,
        firstname: body.firstname,
        mail: body.mail,
        position: body.position,
        phone: body.phone,
        password: await bcrypt.hash(body.password, 10),
        role: body.role,
        created_by: body.creation_by,
        last_updated_by: body.creation_by,
      },
    });

    if (body.permission) {

      await prisma.$transaction(async (prisma) => {
        const promises = body.permission.map(async (i) => {
          const old_value = await prisma.bs_users_permission.create({
            data: {
              user_id: +response.user_id,
              menu_id: +i.menu_id,
              permission: i.permission,
              created_by: body.creation_by,
              last_updated_by: body.creation_by,
            },
          });
      
          await prisma.log_bs_users_permission.create({
            data: {
              user_permission_id: +old_value.user_permission_id,
              user_id: +old_value.user_id,
              menu_id: +old_value.menu_id,
              permission: old_value.permission,
              created_by: old_value.created_by,
              last_updated_by: old_value.last_updated_by,
            },
          });
        });
      
        // Execute all operations concurrently
        await Promise.all(promises);
      });
      // for (let i of body.permission) {
      //   const old_value = await prisma.bs_users_permission.create({
      //     data: {
      //       user_id: +response.user_id,
      //       menu_id: +i.menu_id,
      //       permission: i.permission,
      //       created_by: body.creation_by,
      //       last_updated_by: body.creation_by,
      //     },
      //   });

      //   await prisma.log_bs_users_permission.create({
      //     data: {
      //       user_permission_id: +old_value.user_permission_id,
      //       user_id: +old_value.user_id,
      //       menu_id: +old_value.menu_id,
      //       permission: old_value.permission,
      //       created_by: old_value.creation_by,
      //       last_updated_by: old_value.creation_by,
      //     },
      //   });
      // }
    }

    if (body.orgs) {

      await prisma.$transaction(async (prisma) => {
        const promises = body.orgs.map(async (i) => {
          return prisma.bs_org_users.create({
            data: {
              user_id: +response.user_id,
              org_id: +i.org_id,
              created_by: body.creation_by,
              last_updated_by: body.creation_by,
            },
          });
        });
      
        // Execute all operations concurrently
        await Promise.all(promises);
      });
      // for (let i of body.orgs) {
      //   await prisma.bs_org_users.create({
      //     data: {
      //       user_id: +response.user_id,
      //       org_id: +i.org_id,
      //       created_by: body.creation_by,
      //       last_updated_by: body.creation_by,
      //     },
      //   });
      // }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_users.update({
      data: {
        username: body.username,
        lastname: body.lastname,
        firstname: body.firstname,
        mail: body.mail,
        position: body.position,
        phone: body.phone,
        role: body.role,
        last_update_date: new Date(),
        last_updated_by: body.creation_by,
      },
      where: { user_id: +body.user_id, status: "A" },
    });

    if (body.permission) {
      for (let i of body.permission) {
        if (i.user_permission_id === null) {
          const old_value = await prisma.bs_users_permission.create({
            data: {
              user_id: +body.user_id,
              menu_id: +i.menu_id,
              permission: i.permission,
              created_by: body.creation_by,
              last_updated_by: body.creation_by,
            },
          });

          await prisma.log_bs_users_permission.create({
            data: {
              user_permission_id: +old_value.user_permission_id,
              user_id: +old_value.user_id,
              menu_id: +old_value.menu_id,
              permission: old_value.permission,
              created_by: old_value.creation_by,
              last_updated_by: old_value.creation_by,
            },
          });
        } else {
          const old_value = await prisma.bs_users_permission.findFirst({
            where: {
              user_id: +body.user_id,
              user_permission_id: i.user_permission_id,
              status: "A",
            },
          });

          const old_log = await prisma.log_bs_users_permission.findFirst({
            where: {
              user_id: +body.user_id,
              user_permission_id: i.user_permission_id,
              latest_change_flag: "Y",
              status: "A",
            },
          });

          if (old_log) {
            await prisma.log_bs_users_permission.update({
              data: {
                latest_change_flag: "N",
                last_update_date: new Date(),
                last_updated_by: body.username,
              },
              where: {
                log_user_permission_id: +old_log.log_user_permission_id,
              },
            });

            await prisma.log_bs_users_permission.create({
              data: {
                user_permission_id: +old_value.user_permission_id,
                user_id: +old_value.user_id,
                menu_id: +old_value.menu_id,
                permission: old_value.permission,
                created_by: old_value.creation_by,
                creation_Date: old_value.creation_Date,
                last_updated_by: old_value.creation_by,
                last_update_date: old_value.last_update_date,
              },
            });
          }
          await prisma.bs_users_permission.update({
            data: {
              permission: i.permission,
              last_update_date: new Date(),
              last_updated_by: body.creation_by,
            },
            where: { user_permission_id: +i.user_permission_id, status: "A" },
          });
        }
      }
    }

    if (body.orgs) {
      await prisma.$queryRaw`UPDATE bs_org_users set status = 'I', last_update_date =${new Date()}, last_updated_by = ${
        body.creation_by
      } where user_id =${+body.user_id} and status = 'A'`;

      for (let i of body.orgs) {
        await prisma.bs_org_users.create({
          data: {
            user_id: +body.user_id,
            org_id: +i.org_id,
            created_by: body.creation_by,
            last_updated_by: body.creation_by,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.bs_users.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { user_id: +body.user_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
