import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
    try {
          const response = await prisma.$queryRaw`
          select aa1.user_login_id,aa2.username,aa1.creation_date
          from users_logins aa1
            inner join bs_users aa2
              on aa1.user_id = aa2.user_id
          order by aa1.creation_date desc`;
  
      return tokenResponse(null, response);
    } catch (error) {
      console.log(error);
      return Response.json(
        { message: "Error", result: error.error_code },
        { status: 500 }
      );
    }
  }