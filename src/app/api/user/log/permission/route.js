import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
    try {
      const response = await prisma.$queryRaw`
        select 
          aa1.log_user_permission_id,
          aa1.user_permission_id,
          aa1.user_id,
          aa2.username,
          aa3.menu_name,
          aa4.name permission,
          aa1.latest_change_flag,
          aa1.status,
          aa1.created_by,
          aa1.creation_date,
          aa1.last_updated_by,
          aa1.last_update_date
        from log_bs_users_permission aa1
          inner join bs_users aa2
            on aa1.user_id = aa2.user_id
          inner join bs_menu aa3
            on aa1.menu_id = aa3.menu_id
            and aa3.status = 'A'
          left join bs_lookups aa4
            on aa1.permission = aa4.lookup_code
            and aa4.lookup_type = 'Permission'
            and aa4.status = 'A'
        order by aa1.user_id,aa1.last_update_date desc`;
  
      return tokenResponse(null, response);
    } catch (error) {
      console.log(error);
      return Response.json(
        { message: "Error", result: error.error_code },
        { status: 500 }
      );
    }
  }