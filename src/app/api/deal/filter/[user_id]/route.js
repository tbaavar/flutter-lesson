import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    select 'All' as trade_id
    union ALL
    select 'T-' || trade_id
    from trades
    where status = 'A'
        and company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})   `;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
