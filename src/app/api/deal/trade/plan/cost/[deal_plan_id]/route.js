import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.deal_plan_costs.findMany({
      select: {
        plan_cost_id: true,
        cost_code: true,
        amount: true,  
      },
      where: { deal_plan_id: +params.deal_plan_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
