import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();
  try {
    await prisma.deal_plan_costs.create({
      data: {
        deal_plan_id: body.deal_plan_id,
        cost_code: body.cost_code,
        amount: +body.amount,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    await prisma.deal_plan_costs.update({
      data: {
        cost_code: body.cost_code,
        amount: +body.amount,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { plan_cost_id: +body.plan_cost_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    await prisma.deal_plan_costs.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { plan_cost_id: +body.plan_cost_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
