import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();
  try {
    await prisma.deal_plan_trades.create({
      data: {
        deal_plan_id: body.deal_plan_id,
        trade_type: body.trade_type,
        counterparty_id: +body.counterparty_id,
        trade_begin_date: new Date(body.trade_begin_date),
        trade_end_date: new Date(body.trade_end_date),
        product_id: +body.product_id,
        grade_id: +body.grade_id,
        units: body.units,
        quantity: +body.quantity,
        price: +body.price,
        amount: +body.amount,
        currency: body.currency,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });
    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    await prisma.deal_plan_trades.update({
      data: {
        trade_type: body.trade_type,
        trade_begin_date: new Date(body.trade_begin_date),
        trade_end_date: new Date(body.trade_end_date),
        counterparty_id: +body.counterparty_id,
        product_id: +body.product_id,
        grade_id: +body.grade_id,
        units: body.unit,
        quantity: +body.quantity,
        price: +body.price,
        amount: +body.amount,
        currency: body.currency,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { plan_trade_id: +body.plan_trade_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    await prisma.deal_plan_trades.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { plan_trade_id: +body.plan_trade_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
