import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
select 
		aa1.plan_trade_id,
		aa1.trade_type,
		aa1.trade_begin_date,
		aa1.trade_end_date,
		aa1.counterparty_id,
		aa4.name counterparty_name,
		aa1.product_id,
		aa2.product_name,
		aa1.grade_id,
		aa3.grade_name,
		aa1.units,
	  aa1.quantity,
		aa1.price,
		aa1.amount,
		aa1.currency
from deal_plan_trades aa1
	inner join products aa2
		on aa1.product_id = aa2.product_id
		and aa1.status = 'A'
	inner join product_grades aa3
		on aa1.product_id = aa3.product_id
		and aa1.grade_id = aa3.grade_id
		and aa3.status = 'A'
	inner join legal_entity aa4	
		on aa1.counterparty_id = aa4.legal_entity_id
		and aa4.status = 'A'
where aa1.deal_plan_id= ${+params.deal_plan_id}
	and aa1.status = 'A'`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
