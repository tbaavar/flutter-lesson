import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();
  try {
    const response = await prisma.deal_trades.create({
      data: {
        deal_id: +body.deal_id,
        trade_id: +body.trade_id,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    await prisma.$queryRaw`UPDATE trade_products SET 
                          consolidate_deal_flag = 'Y', 
                          last_updated_by= ${body.username},
                          last_update_date=CURRENT_TIMESTAMP 
                          where  trade_id= ${+body.trade_id} and status='A'`;

    return tokenResponse(response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.deal_trades.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { deal_trade_id: +body.deal_trade_id, status: "A" },
    });

    await prisma.$queryRaw`UPDATE trade_products SET 
                            consolidate_deal_flag = 'N', 
                            last_updated_by= ${body.username},
                            last_update_date=CURRENT_TIMESTAMP 
                            where  trade_id= ${+old_value.trade_id} and status='A'`;

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
