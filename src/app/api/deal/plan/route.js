import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.deal_plans.findMany({
      select: {
        deal_plan_id: true,
        deal_number: true,
        title: true,
        description: true,
        deal_date: true,
        deal_status: true,
        org_id:true
      },
      where: { status: "A" },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();
  try {
    await prisma.deal_plans.create({
      data: {
        deal_number: body.deal_number,
        org_id: +body.org_id,
        title: body.title,
        description: body.description,
        deal_date: new Date(body.deal_date),
        deal_status: body.deal_status,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    await prisma.deal_plans.update({
      data: {
        title: body.title,
        description: body.description,
        deal_date: new Date(body.deal_date),
        deal_status: body.deal_status,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { deal_plan_id: +body.deal_plan_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    await prisma.deal_plans.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { deal_plan_id: +body.deal_plan_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
