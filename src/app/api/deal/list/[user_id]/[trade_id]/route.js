import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    let response = null;
    if (params.trade_id === "All") {
      response = await prisma.$queryRaw`
    
      select deal_id,deal_number,title,description,deal_date,deal_status,org_id,deal_plan_id
      from deals
      where org_id in (select org_id from bs_org_users where user_id = ${+params.user_id})   
          and status= 'A'`;
    } else {
      response = await prisma.$queryRaw`
        select  
            aa1.deal_id,
            aa1.deal_number,
            aa1.title,
            aa1.description,
            aa1.deal_date,
            aa1.deal_status,
            aa1.deal_plan_id
    from deals aa1
        inner join deal_trades aa2
            on aa1.deal_id = aa2.deal_id
            and aa2.status = 'A'	
    where aa2.trade_id = (replace(${params.trade_id},'T-','')) ::int
        and aa1.status = 'A'`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
