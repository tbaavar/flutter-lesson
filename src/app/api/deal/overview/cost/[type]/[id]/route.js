import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";


export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    let response = null;

    if (params.type === "D") {
      response = await prisma.$queryRaw`
							select
								kk1.deal_id,
								kk1.cost_code||'-'||kk2.cost_name as types,
								kk2.cost_type,
								kk1.plan_amount,
								kk1.plan_currency,
								kk1.total_purchase ::numeric purchase,
								kk1.total_purchase_currency purchase_currency,
								kk1.total_sale ::numeric sales,
								kk1.total_sale_currency sales_currency
							from (
								select
									COALESCE(dd1.deal_id,dd2.deal_id) deal_id,
									COALESCE(dd1.cost_code,dd2.cost_code) cost_code,
									dd1.amount plan_amount,
									dd1.currency plan_currency,
									dd2.total_purchase,
									dd2.total_purchase_currency,
									dd2.total_sale,
									dd2.total_sale_currency
								from (
									select
										cc1.deal_id,
										cc2.cost_code,
										cc2.amount,
										cc2.currency
									from deals cc1
										left join (		
													select aa1.deal_plan_id,aa2.cost_code,aa2.amount,aa2.currency
													from deal_plans aa1
														inner join deal_plan_costs aa2
															on aa1.deal_plan_id = aa2.deal_plan_id
															and aa2.status = 'A'
													where aa1.status = 'A'
													) cc2
											on cc1.deal_plan_id = cc2.deal_plan_id
									where cc1.deal_id = ${+params.id}
										and cc1.status = 'A'
									) dd1
								full outer join (
												select 
													bb1.deal_id,
													bb5.cost_code,
													sum(COALESCE(case when bb2.trade_type in ('Spot_Purchase','Term_Purchase') then bb5.value end,0)) total_purchase,
													max(case when bb2.trade_type in ('Spot_Purchase','Term_Purchase') then bb4.currency end) total_purchase_currency,
													sum(COALESCE(case when bb2.trade_type in ('Spot_Sale','Term_Sale') then bb5.value end,0)) total_sale,
													max(case when bb2.trade_type in ('Spot_Sale','Term_Sale') then bb4.currency end) total_sale_currency
												from deal_trades bb1
														inner join trades bb2
															on bb1.trade_id = bb2.trade_id
															and bb2.status = 'A'
														inner join voyage_parcels bb3
															on bb2.trade_id = bb3.trade_id
															and bb3.status = 'A'
														inner join invoices bb4
															on bb3.voyage_parcel_id = bb4.voyage_parcel_id
															and bb4.status = 'A'
														inner join invoice_details bb5
															on bb4.invoice_id = bb5.invoice_id
															and bb5.balance_type = 'Debit'
															and bb5.status = 'A'
													where bb1.deal_id = ${+params.id}
														and bb1.status = 'A'
												group by bb1.deal_id,bb5.cost_code
												) dd2
										on dd1.deal_id = dd2.deal_id
										and dd1.cost_code = dd2.cost_code
									) kk1
									inner join costs kk2
										on kk1.cost_code = kk2.cost_code
										and kk2.status = 'A'
							order by kk1.cost_code`;
    } else {
      response = await prisma.$queryRaw`							
							select
								cc1.deal_id,
								cc1.cost_code||'-'||cc1.cost_name as types,
								cc1.cost_type,
								null plan_amount,
								null plan_currency,
								cc1.total_purchase ::numeric as purchase,
								cc1.total_purchase_currency purchase_currency,
								cc1.total_sale ::numeric sales,
								cc1.total_sale_currency sales_currency
							from (
								select 
									aa1.deal_id,
									aa6.cost_code,
									max(aa7.cost_name) cost_name,
									max(aa7.cost_type) cost_type,
									sum(COALESCE(case when aa3.trade_type in ('Spot_Purchase','Term_Purchase') then aa6.value end,0)) total_purchase,
									max(case when aa3.trade_type in ('Spot_Purchase','Term_Purchase') then aa5.currency end) total_purchase_currency,
									sum(COALESCE(case when aa3.trade_type in ('Spot_Sale','Term_Sale') then aa6.value end,0)) total_sale,
									max(case when aa3.trade_type in ('Spot_Sale','Term_Sale') then aa5.currency end) total_sale_currency
								from deals aa1
									inner join deal_trades aa2
										on aa1.deal_id = aa2.deal_id
										and aa2.status = 'A'
									inner join trades aa3
										on aa2.trade_id = aa3.trade_id
										and aa3.status = 'A'
									inner join  voyage_parcels aa4
										on aa3.trade_id = aa4.trade_id
										and aa4.status = 'A'
									inner join invoices aa5
										on aa4.voyage_parcel_id = aa5.voyage_parcel_id
										and aa5.status = 'A'
									inner join invoice_details aa6
										on aa5.invoice_id = aa6.invoice_id
										and aa6.balance_type = 'Debit'
										and aa6.status = 'A'
									inner join costs aa7
										on aa6.cost_code = aa7.cost_code
										and aa7.status = 'A'
								where aa2.trade_id = ${+params.id} 
										and aa1.status = 'A'
							group by aa1.deal_id,aa6.cost_code	
									) cc1
							order by cc1.cost_code`;
    }
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
