import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    let response = null;
    if (params.type === "D") {
      response = await prisma.$queryRaw`
					select  
							aa1.trade_id,
							aa1.deal_id,
							aa1.company_id,
                            aa8.name company_name,
							aa1.counterparty_id,
							aa6.name country_party_name,
							aa10.name trade_type_name,
							aa1.trade_type trade_type,		                      
							aa1.trade_date,
							aa2.product_id,
							aa3.product_name,
							aa2.grade_id,
							aa5.grade_name,
							aa2.quantity,	
							aa2.quantity_cm,	
							aa2.units,
							to_char(aa4.date_range_from,'Mon dd,yyyy') date_range_from,
							to_char(aa4.date_range_to,'Mon dd,yyyy') date_range_to,
							substring(aa7.lastname,1,1) || '.'||aa7.firstname trader_name,
							aa1.trade_status,
							aa1.operator_sign_off_flag,
							case when aa1.operator_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end operation_flag,
							aa1.finance_sign_off_flag,
							case when aa1.finance_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end finance_flag,
							substring(aa9.lastname,1,1) || '.' || aa9.firstname operator_name,
                            substring(aa14.lastname,1,1) || '.' || aa14.firstname finance_name,
							aa11.contract_received_date,
							aa11.response_sent_date,
							aa11.contract_drafted_date,
							aa11.contact_sent_date,
							aa11.broker_contract_received_date,
							aa11.contract_finish_flag		
					from deals cc1
						inner join deal_trades cc2
							on cc1.deal_id = cc2.deal_id
							and cc2.status = 'A'	
						inner join trades aa1
							on cc2.trade_id = aa1.trade_id
							and aa1.status = 'A'
						left join trade_products aa2
							on aa1.trade_id = aa2.trade_id
							and aa2.status = 'A'
						left  join products aa3
							on aa2.product_id = aa3.product_id
							and aa3.status = 'A'
						left join trade_deliveries aa4
							on aa1.trade_id = aa4.trade_id
							and aa4.status = 'A'
						left join product_grades aa5
							on aa2.product_id = aa5.product_id
							and aa2.grade_id = aa5.grade_id
							and aa5.status = 'A'
						left join legal_entity aa6
							on aa1.counterparty_id = aa6.legal_entity_id
							and aa6.status='A'
						left join bs_users aa7
							on aa1.trader_user_id = aa7.user_id
							and aa7.status = 'A'
                        left join bs_organizations aa8
				            on aa1.company_id = aa8.org_id
                            and aa8.status = 'A'
						left join bs_users aa9
							on aa1.operator_user_id = aa9.user_id		
							and aa9.status = 'A'
                        left join bs_lookups aa10
				            on aa1.trade_type = aa10.lookup_code
				            and aa10.lookup_type = 'Trade_type'
                            and aa10.status = 'A'
						left join trade_laws aa11
							on aa1.trade_id = aa11.trade_id
							and aa11.status = 'A'
						left join bs_users aa14
							on aa1.finance_user_id = aa14.user_id	
							and aa14.status = 'A'                        
					where cc1.deal_id =  ${+params.id}
						and cc1.status = 'A'
					order by aa1.trade_type,aa1.trade_date desc`;
    } else {
      response = await prisma.$queryRaw`
					select  
							aa1.trade_id,
							aa1.deal_id,
							aa1.company_id,
                            aa8.name company_name,
							aa1.counterparty_id,
							aa6.name country_party_name,
							aa10.name trade_type,
							aa1.trade_date,
							aa2.product_id,
							aa3.product_name,
							aa2.grade_id,
							aa5.grade_name,
							aa2.quantity,
							aa2.quantity_cm,
							aa2.units,
							to_char(aa4.date_range_from,'Mon dd,yyyy') date_range_from,
							to_char(aa4.date_range_to,'Mon dd,yyyy') date_range_to,
							substring(aa7.lastname,1,1) || '.'||aa7.firstname trader_name,
							aa1.trade_status,							
							aa1.operator_sign_off_flag,
							case when aa1.operator_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end operation_flag,
							aa1.finance_sign_off_flag,
							case when aa1.finance_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end finance_flag,
							substring(aa9.lastname,1,1) || '.' || aa9.firstname operator_name,
                            substring(aa14.lastname,1,1) || '.' || aa14.firstname finance_name,
							aa11.contract_received_date,
							aa11.response_sent_date,
							aa11.contract_drafted_date,
							aa11.contact_sent_date,
							aa11.broker_contract_received_date,
							aa11.contract_finish_flag
					from deals cc1
						inner join deal_trades cc2
							on cc1.deal_id = cc2.deal_id
							and cc2.status = 'A'	
						inner join trades aa1
							on cc2.trade_id = aa1.trade_id
							and aa1.status = 'A'
						left join trade_products aa2
							on aa1.trade_id = aa2.trade_id
						left  join products aa3
							on aa2.product_id = aa3.product_id
							and aa3.status = 'A'
						left join trade_deliveries aa4
							on aa1.trade_id = aa4.trade_id
							and aa4.status = 'A'
						left join product_grades aa5
							on aa2.product_id = aa5.product_id
							and aa2.grade_id = aa5.grade_id
							and aa5.status = 'A'
						left join legal_entity aa6
							on aa1.counterparty_id = aa6.legal_entity_id
							and aa6.status='A'
						left join bs_users aa7
							on aa1.trader_user_id = aa7.user_id
							and aa7.status = 'A'
						left join bs_organizations aa8
				            on aa1.company_id = aa8.org_id
                            and aa8.status = 'A'
						left join bs_users aa9
							on aa1.operator_user_id = aa9.user_id		
							and aa9.status = 'A'
                        left join bs_lookups aa10
				            on aa1.trade_type = aa10.lookup_code
				            and aa10.lookup_type = 'Trade_type'
                            and aa10.status = 'A'
						left join trade_laws aa11
							on aa1.trade_id = aa11.trade_id
							and aa11.status = 'A'
						left join bs_users aa14
							on aa1.finance_user_id = aa14.user_id	
							and aa14.status = 'A'
					where cc2.trade_id =  ${+params.id}`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
