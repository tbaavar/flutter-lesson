import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    let response = null;
    if (params.type === "D") {
      response = await prisma.$queryRaw`
                    select 
                        aa1.parcel_movement_id,                        
                        aa1.transfer_type,
                        aa1.voyage_id,
                        aa1.voyage_parcel_id,			
                        aa1.parcel_comment,
                        aa1.transfer_status,
                        aa1.superintendent_id,
                        aa1.superveyor_id,                    
                        aa1.transfer_completion_date,
                        aa1.transfer_completion_date_est_fl,
                        aa1.terminal_from_date,
                        aa1.terminal_to_date,
                        aa1.source_terminal_id,
                        aa4.terminal_name,
                        aa1.source_tank_id,
                        aa3.tank_name,
                        aa1.target_terminal_id,
                        aa1.target_tank_id,
                        aa1.quantity_mt quantity,
                        aa1.units tank_unit,
                        aa8.name tank_unit_name,
                        aa1.tank_comment,
                        aa1.clean_tank_flag,
                        aa1.third_party_stock_flag,
                        aa5.product_name tank_product,
                        aa6.grade_name tank_grade,
                        aa1.balance_mt tank_balance,
                        aa3.capacity tank_capacity,
                        aa1.final_density tank_density,
                        aa1.adjustment_type,
                        aa1.transfer_grade_id,
                        aa1.blend_flag,
                        aa1.final_product_id,
                        aa1.final_grade_id,
                        aa1.circulation,				
                        aa1.blend_ratio,
                        aa1.balance_mt			
                    from parcel_movements aa1                    
                    inner join storage_tank aa3
                        on aa1.source_tank_id = aa3.tank_id
                        and aa3.status = 'A'
                    inner join storage_terminal aa4
                        on aa1.source_terminal_id = aa4.terminal_id
                        and aa4.status = 'A'
                    inner join products aa5
                        on aa1.product_id = aa5.product_id
                        and aa5.status = 'A'
                    inner join product_grades aa6
                        on aa1.product_id = aa6.product_id
                        and aa1.grade_id = aa6.grade_id
                        and aa6.status = 'A'
                    inner join bs_lookups aa7
                        on aa1.country_origin = aa7.lookup_code
                        and aa7.lookup_type = 'Country_Origin'
                        and aa7.status = 'A'
                    inner join bs_lookups aa8
                        on aa1.units = aa8.lookup_code
                        and aa8.lookup_type = 'Unit'
                        and aa8.status = 'A'	
                where aa1.voyage_id in (						
						select bb2.voyage_id
from deal_trades bb1
	inner join voyage_parcels bb2
		on bb1.trade_id = bb2.trade_id
		and bb2.status = 'A'
where bb1.deal_id =  ${+params.id}) and  aa1.status = 'A'`;
    } else {
      response = await prisma.$queryRaw`
                    select 
                        aa1.parcel_movement_id,                        
                        aa1.transfer_type,
                        aa1.voyage_id,
                        aa1.voyage_parcel_id,			
                        aa1.parcel_comment,
                        aa1.transfer_status,
                        aa1.superintendent_id,
                        aa1.superveyor_id,                    
                        aa1.transfer_completion_date,
                        aa1.transfer_completion_date_est_fl,
                        aa1.terminal_from_date,
                        aa1.terminal_to_date,
                        aa1.source_terminal_id,
                        aa4.terminal_name,
                        aa1.source_tank_id,
                        aa3.tank_name,
                        aa1.target_terminal_id,
                        aa1.target_tank_id,
                        aa1.quantity_mt quantity,
                        aa1.units tank_unit,
                        aa8.name tank_unit_name,
                        aa1.tank_comment,
                        aa1.clean_tank_flag,
                        aa1.third_party_stock_flag,
                        aa5.product_name tank_product,
                        aa6.grade_name tank_grade,
                        aa1.balance_mt tank_balance,
                        aa3.capacity tank_capacity,
                        aa1.final_density tank_density,
                        aa1.adjustment_type,
                        aa1.transfer_grade_id,
                        aa1.blend_flag,
                        aa1.final_product_id,
                        aa1.final_grade_id,
                        aa1.circulation,				
                        aa1.blend_ratio,
                        aa1.balance_mt			
                    from parcel_movements aa1                    
                    inner join storage_tank aa3
                        on aa1.source_tank_id = aa3.tank_id
                        and aa3.status = 'A'
                    inner join storage_terminal aa4
                        on aa1.source_terminal_id = aa4.terminal_id
                        and aa4.status = 'A'
                    inner join products aa5
                        on aa1.product_id = aa5.product_id
                        and aa5.status = 'A'
                    inner join product_grades aa6
                        on aa1.product_id = aa6.product_id
                        and aa1.grade_id = aa6.grade_id
                        and aa6.status = 'A'
                    inner join bs_lookups aa7
                        on aa1.country_origin = aa7.lookup_code
                        and aa7.lookup_type = 'Country_Origin'
                        and aa7.status = 'A'
                    inner join bs_lookups aa8
                        on aa1.units = aa8.lookup_code
                        and aa8.lookup_type = 'Unit'
                        and aa8.status = 'A'	
                where aa1.voyage_id in (						
						select bb2.voyage_id
from deal_trades bb1
	inner join voyage_parcels bb2
		on bb1.trade_id = bb2.trade_id
		and bb2.status = 'A'
where bb1.deal_id =  ${+params.id}) and  aa1.status = 'A'`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
