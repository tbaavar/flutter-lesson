import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    let response = null;
    if (params.type === "D") {
      response = await prisma.$queryRaw`
                select
                    cc1.voyage_parcel_id,
                    cc1.voyage_id,		
                    cc1.trade_id,
                    cc8.trade_type,
                    cc10.name trade_type_name,
                    cc1.counterparty_id,
                    case when cc1.port_type in ('STS_load','STS_disport') then cc5.terminal_name else cc4.name end counterparty_name,
                    cc8.terms,
                    cc8.terms ||'-'|| cc11.name term_name,
                    cc1.operator_user_id,
                    substring(cc7.lastname,1,1) || '.' || cc7.firstname operator_name,
                    cc8.finance_user_id,
                    substring(cc9.lastname,1,1) || '.' || cc9.firstname financer_name,
                    cc1.country_origin,
                    cc6.name country_origin_name,		
                    cc2.product_name,		
                    cc3.grade_name,		
                    cc14.intended_vessel_id vessel,
                    cc15.name vessel_name,
                    cc1.transfer_date,
                    cc13.units,
                    cc16.name unit_name,
                    cc1.parcel_number,		
                    cc1.no_agreed_supply_date_flag,
                    cc1.transfer_date_est_flag,
                    cc1.border_crossing_date,
                    cc1.border_crossing_date_est_flag,
                    cc1.supply_start_date,
                    cc1.supply_end_date,
                    cc1.transfer_detail_date,		
                    cc1.shipping_quantity_mt,
                    cc1.shipping_quantity_cm,			
                    cc1.est_quantity_flag,		
                    cc1.invoicing_unit,
                    cc1.trade_code
                from deals aa1
                    inner join deal_trades aa2
                        on aa1.deal_id = aa2.deal_id
                        and aa2.status = 'A'                        
                    inner join voyage_parcels cc1
                        on aa2.trade_id = cc1.trade_id
                        and cc1.status = 'A'                        
                    inner join products cc2
                        on cc1.product_id = cc2.product_id
                        and cc2.status = 'A'
                    inner join product_grades cc3
                        on cc1.grade_id = cc3.grade_id
                        and cc1.product_id = cc3.product_id
                        and cc3.status = 'A'
                    left join legal_entity cc4
                        on cc1.counterparty_id = cc4.legal_entity_id
                        and cc4.status = 'A'
                    left join storage_terminal cc5
                        on cc1.counterparty_id = cc5.terminal_id
                        and cc5.status = 'A'
                    left join bs_lookups cc6
                        on cc1.country_origin = cc6.lookup_code
                        and cc6.lookup_type = 'Country_Origin'
                        and cc6.status = 'A'
                    left join bs_users cc7
                        on cc1.operator_user_id = cc7.user_id		
                        and cc7.status = 'A'	
                    left join trades cc8
                        on cc1.trade_id = cc8.trade_id
                        and cc8.status = 'A'
                    left join bs_users cc9
                        on cc8.finance_user_id = cc9.user_id	
                        and cc9.status = 'A'
                    left join bs_lookups cc10
                        on cc8.trade_type = cc10.lookup_code
                        and cc10.lookup_type = 'Trade_type'
                        and cc10.status = 'A'
                    left join bs_lookups cc11
                        on cc8.terms = cc11.lookup_code
                        and cc11.lookup_type = 'Terms'
                        and cc11.status = 'A'
                    left join bs_organizations cc12
                        on cc8.company_id = cc12.org_id
                        and cc12.status = 'A'
                    left join trade_products cc13
                        on cc1.trade_id = cc13.trade_id
                        and cc13.status = 'A'     
                    left join trade_deliveries cc14
                        on cc1.trade_id = cc14.trade_id
                        and cc14.status = 'A'
                    left join bs_lookups cc15
                        on cc14.intended_vessel_id = cc15.lookup_code
                        and cc15.lookup_type = 'Intended_Vessel'
                        and cc15.status = 'A'
                    left join bs_lookups cc16
                        on cc13.units = cc16.lookup_code
                        and cc16.lookup_type = 'Unit'
                        and cc16.status = 'A'
                    left join trade_payments cc17
                        on cc1.trade_id = cc17.trade_id
                        and cc17.status = 'A'
                    left join bs_lookups cc18
                        on cc17.credit_type = cc18.lookup_code
                        and cc18.lookup_type = 'Credit_Type'
                        and cc18.status = 'A'
                    left join banks cc19
                        on cc17.bank_id = cc19.bank_id
                        and cc19.status = 'A'  
                where aa1.deal_id = ${+params.id}
                    and aa1.status = 'A'`;
    } else {
      response = await prisma.$queryRaw`
                select
                    cc1.voyage_parcel_id,
                    cc1.voyage_id,		
                    cc1.trade_id,
                    cc8.trade_type,
                    cc10.name trade_type_name,
                    cc1.counterparty_id,
                    case when cc1.port_type in ('STS_load','STS_disport') then cc5.terminal_name else cc4.name end counterparty_name,
                    cc8.terms,
                    cc8.terms ||'-'|| cc11.name term_name,
                    cc1.operator_user_id,
                    cc7.firstname || ' ' || cc7.lastname operator_name,
                    cc8.finance_user_id,
                    cc9.firstname || ' ' || cc9.lastname financer_name,
                    cc1.country_origin,
                    cc6.name country_origin_name,		
                    cc2.product_name,		
                    cc3.grade_name,		
                    cc14.intended_vessel_id vessel,
                    cc15.name vessel_name,
                    cc1.transfer_date,
                    cc13.units,
                    cc16.name unit_name,
                    cc1.parcel_number,		
                    cc1.no_agreed_supply_date_flag,
                    cc1.transfer_date_est_flag,
                    cc1.border_crossing_date,
                    cc1.border_crossing_date_est_flag,
                    cc1.supply_start_date,
                    cc1.supply_end_date,
                    cc1.transfer_detail_date,		
                    cc1.shipping_quantity_mt,		
                    cc1.est_quantity_flag,		
                    cc1.invoicing_unit,
                    cc1.trade_code
                from deals aa1
                    inner join deal_trades aa2
                        on aa1.deal_id = aa2.deal_id
                        and aa2.status = 'A'                        
                    inner join voyage_parcels cc1
                        on aa2.trade_id = cc1.trade_id
                        and cc1.status = 'A'
                    inner join products cc2
                        on cc1.product_id = cc2.product_id
                        and cc2.status = 'A'
                    inner join product_grades cc3
                        on cc1.grade_id = cc3.grade_id
                        and cc1.product_id = cc3.product_id
                        and cc3.status = 'A'
                    left join legal_entity cc4
                        on cc1.counterparty_id = cc4.legal_entity_id
                        and cc4.status = 'A'
                    left join storage_terminal cc5
                        on cc1.counterparty_id = cc5.terminal_id
                        and cc5.status = 'A'
                    left join bs_lookups cc6
                        on cc1.country_origin = cc6.lookup_code
                        and cc6.lookup_type = 'Country_Origin'
                        and cc6.status = 'A'
                    left join bs_users cc7
                        on cc1.operator_user_id = cc7.user_id		
                        and cc7.status = 'A'	
                    left join trades cc8
                        on cc1.trade_id = cc8.trade_id
                        and cc8.status = 'A'
                    left join bs_users cc9
                        on cc8.finance_user_id = cc9.user_id	
                        and cc9.status = 'A'
                    left join bs_lookups cc10
                        on cc8.trade_type = cc10.lookup_code
                        and cc10.lookup_type = 'Trade_type'
                        and cc10.status = 'A'
                    left join bs_lookups cc11
                        on cc8.terms = cc11.lookup_code
                        and cc11.lookup_type = 'Terms'
                        and cc11.status = 'A'
                    left join bs_organizations cc12
                        on cc8.company_id = cc12.org_id
                        and cc12.status = 'A'
                    left join trade_products cc13
                        on cc1.trade_id = cc13.trade_id
                        and cc13.status = 'A'     
                    left join trade_deliveries cc14
                        on cc1.trade_id = cc14.trade_id
                        and cc14.status = 'A'
                    left join bs_lookups cc15
                        on cc14.intended_vessel_id = cc15.lookup_code
                        and cc15.lookup_type = 'Intended_Vessel'
                        and cc15.status = 'A'
                    left join bs_lookups cc16
                        on cc13.units = cc16.lookup_code
                        and cc16.lookup_type = 'Unit'
                        and cc16.status = 'A'
                    left join trade_payments cc17
                        on cc1.trade_id = cc17.trade_id
                        and cc17.status = 'A'
                    left join bs_lookups cc18
                        on cc17.credit_type = cc18.lookup_code
                        and cc18.lookup_type = 'Credit_Type'
                        and cc18.status = 'A'
                    left join banks cc19
                        on cc17.bank_id = cc19.bank_id
                        and cc19.status = 'A'  
                where cc1.trade_id= ${+params.id}
                    and aa1.status = 'A'`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
