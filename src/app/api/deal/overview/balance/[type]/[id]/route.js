import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    let response = null;

    if (params.type === "D") {
      response = await prisma.$queryRaw`
							select 
								bb1.deal_id,	
								'Quantity' as types,
								bb2.purchase_mt,
								bb2.purchase_cm,
								bb2.purchase_unit,
								bb2.sales_mt,
								bb2.sales_cm,
								bb2.sales_unit,
								bb3.plan_purchase,
								bb3.plan_purchase_unit,
								bb3.plan_sales,
								bb3.plan_sales_unit
							from deals bb1
								inner join 
										(
										select 
												aa1.deal_id,
												sum(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa3.quantity end) ::numeric purchase_mt,
												sum(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa3.quantity_cm end) ::numeric purchase_cm,
												max(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa3.units end) purchase_unit,
												sum(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa3.quantity end) ::numeric  sales_mt,
												sum(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa3.quantity_cm end) ::numeric  sales_cm,
												max(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa3.units end) sales_unit
										from deal_trades aa1
											inner join trades aa2
												on aa1.trade_id = aa2.trade_id
												and aa2.status = 'A'
											inner join trade_products aa3
												on aa2.trade_id = aa3.trade_id
												and aa3.status = 'A'
										where aa1.status = 'A'
										group by aa1.deal_id
										) bb2
									on bb1.deal_id = bb2.deal_id
								left join(
										select aa1.deal_plan_id,
													sum(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa2.quantity end) ::numeric plan_purchase,
													max(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa2.units end) plan_purchase_unit,
													sum(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa2.quantity end) ::numeric plan_sales,
													max(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa2.units end) plan_sales_unit
										from deal_plans aa1
											inner join deal_plan_trades aa2
												on aa1.deal_plan_id = aa2.deal_plan_id
												and aa2.status = 'A'
										where aa1.status = 'A'
										group by aa1.deal_plan_id
										) bb3
									on bb1.deal_plan_id = bb3.deal_plan_id
							where bb1.deal_id = ${+params.id}
								and bb1.status = 'A'
							union all
							select 
								cc1.deal_id,
								'Value' as types,
								cc2.purchase purchase_mt,
								cc2.purchase purchase_cm,
								cc2.purchase_currency,
								cc2.sales sales_mt,
								cc2.sales sales_cm,
								cc2.sales_currency,
								cc3.plan_purchase,
								cc3.plan_purchase_currency,
								cc3.plan_sales,
								cc3.plan_sales_currency
							from deals cc1
								inner join 	(
											select 
												dd1.deal_id,
												sum(dd1.purchase) purchase,
												max(dd1.purchase_currency) purchase_currency,
												sum(dd1.sales)sales,
												max(dd1.sales_currency) sales_currency
											from (
												select 
													aa1.deal_id,
													aa2.trade_id,
													COALESCE(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa4.total_amount end,0) ::numeric as purchase,
													'MNT' purchase_currency,
													COALESCE(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa4.total_amount end,0) ::numeric  sales,
													'MNT'  sales_currency
												from deal_trades aa1
													inner join trades aa2
														on aa1.trade_id = aa2.trade_id
														and aa2.status = 'A'
													inner join trade_products aa3
														on aa2.trade_id = aa3.trade_id
														and aa3.status = 'A'
													inner join trade_pricing_fixed aa4
														on aa2.trade_id = aa4.trade_id
														and aa4.status = 'A'
												where aa1.status = 'A'
												) dd1
											group by dd1.deal_id
											) cc2
										on cc1.deal_id = cc2.deal_id
								left join (
										select aa1.deal_plan_id,
													sum(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa2.amount end)  plan_purchase,
													max(case when aa2.trade_type in ('Spot_Purchase','Term_Purchase') then aa2.currency end) plan_purchase_currency,
													sum(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa2.amount end) ::numeric plan_sales,
													max(case when aa2.trade_type in ('Spot_Sale','Term_Sale') then aa2.currency end) plan_sales_currency
										from deal_plans  aa1
											inner join deal_plan_trades aa2
												on aa1.deal_plan_id = aa2.deal_plan_id
												and aa2.status = 'A'
										where aa1.status = 'A'
										group by aa1.deal_plan_id
										) cc3
									on cc1.deal_plan_id = cc3.deal_plan_id
							where cc1.deal_id =  ${+params.id}
								and cc1.status = 'A'`;
    } else {
      response = await prisma.$queryRaw`
							select 
								aa1.deal_id,	
								'Quantity' as types,
								sum(case when aa3.trade_type in ('Spot_Purchase','Term_Purchase') then aa4.quantity end) ::numeric purchase_mt,
								sum(case when aa3.trade_type in ('Spot_Purchase','Term_Purchase') then aa4.quantity_cm end) ::numeric purchase_cm,
								max(case when aa3.trade_type in ('Spot_Purchase','Term_Purchase') then aa4.units end) purchase_unit,
								sum(case when aa3.trade_type in ('Spot_Sale','Term_Sale') then aa4.quantity end) ::numeric  sales_mt,
								sum(case when aa3.trade_type in ('Spot_Sale','Term_Sale') then aa4.quantity_cm end) ::numeric  sales_cm,
								max(case when aa3.trade_type in ('Spot_Sale','Term_Sale') then aa4.units end) sales_unit,
								null plan_purchase,
								null plan_purchase_unit,
								null plan_sales,
								null plan_sales_unit
							from deals aa1
								inner join deal_trades aa2
									on aa1.deal_id = aa2.deal_id
									and aa2.status = 'A'
								inner join trades aa3
									on aa2.trade_id = aa3.trade_id
									and aa3.status = 'A'
								inner join trade_products aa4
									on aa2.trade_id = aa4.trade_id
									and aa4.status = 'A'
							where aa2.trade_id =  ${+params.id}
								and aa1.status = 'A'
							group by aa1.deal_id
							union all
							select 
									bb1.deal_id,
									'Value' as types,
									sum(bb1.purchase) purchase_mt,
									sum(bb1.purchase) purchase_cm,
									max(bb1.purchase_currency) purchase_currency,
									sum(bb1.sales) sales_mt,
									sum(bb1.sales) sales_cm,
									max(bb1.sales_currency) sales_currency,
									null plan_purchase,
									null plan_purchase_currency,
									null plan_sales,
									null plan_sales_currency
							from (
									select 
											aa1.deal_id,
											aa2.trade_id,
											COALESCE(case when aa3.trade_type in ('Spot_Purchase','Term_Purchase') then aa5.total_amount end,0) ::numeric as purchase,
											'MNT' purchase_currency,
											COALESCE(case when aa3.trade_type in ('Spot_Sale','Term_Sale') then aa5.total_amount end,0) ::numeric  sales,
											'MNT'  sales_currency
									from deals aa1
									inner join deal_trades aa2
										on aa1.deal_id = aa2.deal_id
										and aa2.status = 'A'
									inner join trades aa3
										on aa2.trade_id = aa3.trade_id
										and aa3.status = 'A'
									inner join trade_products aa4
										on aa2.trade_id = aa4.trade_id
										and aa4.status = 'A'
									inner join trade_pricing_fixed aa5
										on aa2.trade_id = aa5.trade_id
										and aa5.status = 'A'
									where aa2.trade_id =   ${+params.id}
									and aa1.status = 'A'
								) bb1
							group by bb1.deal_id`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
