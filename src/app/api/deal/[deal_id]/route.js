import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    select  
		aa1_1.deal_trade_id,
		aa1.trade_id,
		aa1.deal_id,
		aa1.company_id,
		aa1.counterparty_id,
		aa6.name country_party_name,
		aa1.trade_type,
		aa1.trade_date,
		aa2.product_id,
		aa3.product_name,
		aa2.grade_id,
		aa5.grade_name,
		aa2.quantity,	
		aa2.units,
		aa1.trade_date,
		aa4.intended_vessel_id transportation_type,
		aa2.est_bl_date,		
		aa1.terms inco_term,
		aa7.firstname || ' ' || aa7.lastname trader_name,
		aa10.firstname || ' ' || aa10.lastname contract_name,				
		aa8.firstname || ' ' || aa8.lastname operator_name,		
		aa9.firstname || ' ' || aa9.lastname finance_name,
		to_char(aa4.date_range_from,'Mon dd,yyyy') date_range_from,
		to_char(aa4.date_range_to,'Mon dd,yyyy') date_range_to,
		substring(aa7.lastname,1,1) || '.'||aa7.firstname trader_name,
		aa1.trade_status,
		COALESCE(aa6.name,'*') || '-' || COALESCE(aa1.trade_type,'*') || '-'||  COALESCE(aa3.product_name,'*') || ' [' || COALESCE(aa5.grade_name,'*') || '] - ['|| COALESCE(to_char(aa4.date_range_from,'Mon dd yyyy'),'*') || '-'|| COALESCE(to_char(aa4.date_range_to,'Mon dd yyyy'),'*') || ']-' || COALESCE(aa2.quantity,'0') || ' '||COALESCE(aa2.units,'0') as trade_name,
		aa1.operator_sign_off_flag,
		case when aa1.operator_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end operation_flag,
		aa1.finance_sign_off_flag,
		case when aa1.finance_sign_off_flag = 'Y' then 'Sign off' else 'Not signed' end finance_flag
	from trades aa1
	inner join deal_trades aa1_1
		on aa1.trade_id = aa1_1.trade_id
		and aa1_1.status = 'A'
	left join trade_products aa2
		on aa1.trade_id = aa2.trade_id
		and aa2.status = 'A'
	left  join products aa3
		on aa2.product_id = aa3.product_id
		and aa3.status = 'A'
	left join trade_deliveries aa4
		on aa1.trade_id = aa4.trade_id
		and aa4.status = 'A'
	left join product_grades aa5
		on aa2.product_id = aa5.product_id
		and aa2.grade_id = aa5.grade_id
		and aa5.status = 'A'
	left join legal_entity aa6
		on aa1.counterparty_id = aa6.legal_entity_id
		and aa6.status='A'
	left join bs_users aa7
		on aa1.trader_user_id = aa7.user_id
		and aa7.status = 'A'	
	left join bs_users aa8
		on aa1.operator_user_id = aa8.user_id		
		and aa8.status = 'A'
	left join bs_users aa9
		on aa1.finance_user_id = aa9.user_id	
		and aa9.status = 'A'
	left join bs_users aa10
		on aa1.contract_user_id = aa10.user_id	
		and aa10.status = 'A'
where aa1_1.deal_id = ${+params.deal_id}
	and aa1.status = 'A'
order by aa1.trade_type`;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
