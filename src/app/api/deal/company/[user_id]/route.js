import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const deals =await prisma.$queryRaw`
    
    select deal_id,deal_number,title,description,deal_date,deal_status,org_id
    from deals
    where org_id in (select org_id from bs_org_users where user_id = ${+params.user_id})   
        and status= 'A'`
     

    const trade_deals = await prisma.$queryRaw`
                      select
                          aa1.trade_id,
                          aa7.deal_id,
                          aa7.deal_trade_id,
                          case when aa1.trade_type in ('Spot_Sale','Term_Sale') then  '<S>' else '<P>' end trade_code,
                          COALESCE ( aa4.quantity, '0' ) as quantity,
                          COALESCE ( aa4.units, '*' ) as unit,
                          case when aa1.trade_type in ('Spot_Sale','Term_Sale') then aa2.name else aa3.name end  as counterparty,
                          to_char(aa1.trade_date,'YYYY-MM-DD') as trade_date,
                      COALESCE ( aa4.quantity, '0' ) ::VARCHAR  || ' ' ||
                      COALESCE ( aa4.units, '*' ) ::VARCHAR  || ' ' ||
                      COALESCE ( aa5.product_name, '*' ) ::VARCHAR   ||'-[' ||
                      COALESCE ( aa1.trade_type,'*' ) ::VARCHAR   ||']-['||
                      case when aa1.trade_type in ('Spot_Sale','Term_Sale') then aa2.name else aa3.name end || ']' as trade_name
                  from trades aa1
                      left join legal_entity aa2
                          on aa1.counterparty_id = aa2.legal_entity_id
                          and aa2.status = 'A'
                      left join bs_organizations aa3
                          on aa1.company_id = aa3.org_id
                          and aa3.status = 'A'
                      left join trade_products aa4
                          on aa1.trade_id = aa4.trade_id
                          and aa4.status = 'A'
                      left join products aa5
                          on aa4.product_id = aa5.product_id
                          and aa5.status = 'A'
                      left join trade_deliveries aa6
                          on  aa1.trade_id = aa6.trade_id
                          and aa6.status = 'A'
                      inner join deal_trades aa7
                        on aa1.trade_id = aa7.trade_id
                        and aa7.status = 'A'
                  where aa1.status = 'A'`;

    const arr = [];

    for (let x of deals) {
      const item_trade = [];

      for (let d of trade_deals) {
        if (x.deal_id === d.deal_id) {
          item_trade.push({
            deal_trade_id: d.deal_trade_id,
            trade_id: d.trade_id,
            trade_name: d.trade_name,
            trade_code: d.trade_code,
            quantity: d.quantity,
            unit: d.unit,
            counterparty: d.counterparty,
            trade_date: d.trade_date,
          });
        }
      }

      arr.push({
        deal_id: x.deal_id,
        deal_number: x.deal_number,
        title: x.title,
        description: x.description,
        deal_date: x.deal_date,
        deal_status: x.deal_status,
        trades: item_trade,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
