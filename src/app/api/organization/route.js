import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";
export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.bs_organizations.findMany({
      select: {
        org_id: true,
        name: true,
      },
      where: { status: "A" },
      orderBy: {
        sequence: "asc",
      },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
