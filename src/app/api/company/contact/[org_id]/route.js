import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.bs_org_contact_persons.findMany({
      select: {
        org_contact_id:true,
        lastname:true,
        firstname: true,
        position:true,
        phone: true,
        mail:true,
        web:true
      },
      where: { org_id: +params.org_id, status: "A" },
      
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
