import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";


export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_contact_persons.create({
      data: {
        org_id: +body.org_id,
        lastname: body.lastname,
        firstname: body.firstname,
        position: body.position,
        phone: body.phone,
        mail:body.mail,
        web:body.web,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_contact_persons.update({
      data: {
        lastname: body.lastname,
        firstname: body.firstname,
        position: body.position,
        phone: body.phone,
        mail:body.mail,
        web:body.web,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { org_contact_id: +body.org_contact_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error)
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_contact_persons.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { org_contact_id: +body.org_contact_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
