import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.bs_organizations.findMany({
      select: {
        org_id: true,
        register_number: true,
        tax_number: true,
        name: true,
        name_eng: true,
        short_name:true,
        short_name_eng: true,
        logo_url: true,
        phone: true,
        mail: true,
        web: true,
        sequence:true
      },
      where: { status: "A" },
      orderBy: {
        sequence: "asc",
      },
    });

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.bs_organizations.create({
      data: {
        register_number: body.register_number,
        tax_number: body.tax_number,
        name: body.name,
        name_eng:body.name_eng,
        short_name: body.short_name,
        short_name_eng: body.short_name_eng,
        logo_url: body.logo_url,
        phone: body.phone,
        mail: body.mail,
        web: body.web,
        sequence:+body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_organizations.update({
      data: {
        register_number: body.register_number,
        tax_number: body.tax_number,
        name: body.name,
        name_eng:body.name_eng,
        short_name: body.short_name,
        short_name_eng: body.short_name_eng,
        logo_url: body.logo_url,
        phone: body.phone,
        mail: body.mail,
        web: body.web,
        sequence:+body.sequence,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { org_id: +body.org_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.bs_organizations.update({
      data: {
        status: "I",
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { org_id: +body.org_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
