import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.bs_org_addresses.findMany({
      select: {
        org_address_id: true,
        address_type: true,
        origin: true,
        city: true,
        address: true,
      },
      where: { org_id: +params.org_id, status: "A" },
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
