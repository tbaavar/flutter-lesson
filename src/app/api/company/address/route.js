import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_addresses.create({
      data: {
        org_id: +body.org_id,
        address_type: body.address_type,
        origin: body.origin,
        city: body.city,
        address: body.address,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_addresses.update({
      data: {
        address_type: body.address_type,
        origin: body.origin,
        city: body.city,
        address: body.address,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { org_address_id: +body.org_address_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_addresses.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { org_address_id: +body.org_address_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log("e", error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
