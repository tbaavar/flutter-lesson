import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.bs_org_bank_accounts.findMany({
      select: {
        org_account_id:true,
        bank_id:true,
        bank_address: true,
        account_name:true,
        account_number: true,
        swift:true,
       
      },
      where: { org_id: +params.org_id, status: "A" },
      
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
