import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_bank_accounts.create({
      data: {
        org_id: +body.org_id,
        bank_id: +body.bank_id,
        bank_address: body.bank_address,
        account_name: body.account_name,
        account_number: body.account_number,
        swift: body.swift,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_bank_accounts.update({
      data: {
        bank_id: +body.bank_id,
        bank_address: body.bank_address,
        account_name: body.account_name,
        account_number: body.account_number,
        swift: body.swift,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { org_account_id: +body.org_account_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.bs_org_bank_accounts.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { org_account_id: +body.org_account_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
