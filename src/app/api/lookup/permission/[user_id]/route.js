import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
          select aa1.menu_id,aa1.menu_name,aa2.user_permission_id,case when aa1.menu_id =1 then 'View' else COALESCE(aa2.permission,'None') end permission
          from bs_menu aa1
            left join bs_users_permission aa2
                on aa1.menu_id = aa2.menu_id
                and aa2.user_id = ${+params.user_id}
                and aa2.status = 'A'
          where aa1.status = 'A'`;


    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
