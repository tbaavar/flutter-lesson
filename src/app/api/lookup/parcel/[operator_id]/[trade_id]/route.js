import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                select 
                  aa1.voyage_parcel_id,aa1.split_parcel_quantity_mt || ' '|| aa3.units || '-' || aa2.trade_type || '-' ||aa4.name || '-' || to_char(aa1.transfer_date,'YYYY-MM-DD') || ' [' || aa1.trade_id || '/' || aa1.voyage_parcel_id || ']' voyage_parcel_name
                from voyage_parcels aa1
                  inner join trades aa2
                    on aa1.trade_id = aa2.trade_id
                    and aa2.status= 'A'
                  inner join trade_products aa3
                    on aa1.trade_id = aa3.trade_id
                    and aa3.status = 'A'
                  inner join legal_entity aa4
                    on aa2.counterparty_id = aa4.legal_entity_id
                    and aa4.status= 'A'
                where aa1.trade_id = ${+params.trade_id}
                  and aa1.operator_user_id = ${+params.operator_id}
                  and aa1.status = 'A'`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
