import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.storage_tank.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { tank_id: +body.tank_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
