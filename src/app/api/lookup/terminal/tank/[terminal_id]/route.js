import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.storage_tank.findMany({
      select: {
        tank_id: true,
        tank_name: true,
        tank_short_name: true,
        tank_type:true,
        tank_status: true,
        capacity: true,
        sequence: true,
      },
      where: { terminal_id: +params.terminal_id, status: "A" },
    });

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
