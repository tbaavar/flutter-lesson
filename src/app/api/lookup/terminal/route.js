import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
    select 
        aa1.terminal_id,
        aa1.terminal_name,
        aa1.terminal_short_name,
        aa1.capacity,
        aa1.rent_flag,
        aa1.rent_start_date,
        aa1.rent_end_date,
        aa1.port_flag,
        aa1.port_id,
        aa4.port_name,
        aa1.sequence,
        aa1.terminal_status,
        aa3.name terminal_status_name,
        aa1.org_id,
        aa1.address,
        aa2.name org_name
    from storage_terminal aa1
      inner join bs_organizations aa2
        on aa1.org_id = aa2.org_id
        and aa2.status = 'A'
      inner join bs_lookups aa3
        on  aa1.terminal_status = aa3.lookup_code
        and aa3.lookup_type = 'Terminal_status'
        and aa3.status = 'A'
      left join ports aa4
        on aa1.port_id = aa4.port_id
        and aa4.status = 'A'
    where aa1.status = 'A'`;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    const response = await prisma.storage_terminal.create({
      data: {
        org_id: body.org_id,
        terminal_name: body.terminal_name,
        terminal_short_name: body.terminal_short_name,
        capacity: +body.capacity,
        rent_flag: body.rent_flag,
        rent_start_date:
          body.rent_flag === "Y" ? new Date(body.rent_start_date) : null,
        rent_end_date:
          body.rent_flag === "Y" ? new Date(body.rent_end_date) : null,
        address: body.address,
        port_flag: body.port_flag,
        port_id: body.port_flag === "Y" ? +body.port_id : null,
        sequence: +body.sequence,
        terminal_status: body.terminal_status,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    if (body.tanks) {
      for (let i of body.tanks) {
        await prisma.storage_tank.create({
          data: {
            terminal_id: +response.terminal_id,
            tank_name: i.tank_name,
            tank_short_name: i.tank_short_name,
            tank_type: i.tank_type,
            tank_status: i.tank_status,
            capacity: +i.capacity,
            sequence: +i.sequence,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    const response = await prisma.storage_terminal.update({
      data: {
        org_id: body.org_id,
        terminal_name: body.terminal_name,
        terminal_short_name: body.terminal_short_name,
        capacity: +body.capacity,
        rent_flag: body.rent_flag,
        rent_start_date:
          body.rent_flag === "Y" ? new Date(body.rent_start_date) : null,
        rent_end_date:
          body.rent_flag === "Y" ? new Date(body.rent_end_date) : null,
        address: body.address,
        port_flag: body.port_flag,
        port_id: body.port_flag === "Y" ? +body.port_id : null,
        sequence: +body.sequence,
        terminal_status: body.terminal_status,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { terminal_id: +body.terminal_id, status: "A" },
    });

    if (body.tanks) {
      for (let i of body.tanks) {
        if (i.type === "ADD") {
          await prisma.storage_tank.create({
            data: {
              terminal_id: +response.terminal_id,
              tank_name: i.tank_name,
              tank_short_name: i.tank_short_name,
              tank_type: i.tank_type,
              tank_status: i.tank_status,
              capacity: +i.capacity,
              sequence: +i.sequence,
              created_by: body.username,
              last_updated_by: body.username,
            },
          });
        } else if (i.type === "EDIT") {
          await prisma.storage_tank.update({
            data: {
              terminal_id: +response.terminal_id,
              tank_name: i.tank_name,
              tank_short_name: i.tank_short_name,
              tank_type: i.tank_type,
              tank_status: i.tank_status,
              capacity: +i.capacity,
              sequence: +i.sequence,
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { tank_id: +i.tank_id, status: "A" },
          });
        } else if (i.type === "DEL") {
          await prisma.storage_tank.update({
            data: {
              status: "I",
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { tank_id: +i.tank_id, status: "A" },
          });
        }
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.storage_terminal.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { terminal_id: +body.terminal_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
