import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.product_grades.findMany({
      select: {
        grade_id: true,
        grade_name: true,
        grade_description: true,
        sequence: true,
      },
      where: { product_id: +params.product_id, status: "A" },
    });

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
