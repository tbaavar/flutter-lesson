import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.product_grades.findMany({
      select: {
        grade_id: true,
        product_id: true,
        products: { select: { product_name: true } },
        grade_name: true,
        grade_description: true,
        sequence: true,
      },
      where: { status: "A" },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}



export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.product_grades.create({
      data: {
        product_id: +body.product_id,
        grade_name: body.grade_name,
        grade_description: body.grade_description,
        sequence: +body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.product_grades.update({
      data: {
        product_id: +body.product_id,
        grade_name: body.grade_name,
        grade_description: body.grade_description,
        sequence: +body.sequence,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { grade_id: +body.grade_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.product_grades.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { grade_id: +body.grade_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
