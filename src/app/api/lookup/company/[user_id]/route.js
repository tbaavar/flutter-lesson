import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                    select aa1.org_id,aa2.name org_name
                    from bs_org_users aa1
                        inner join bs_organizations aa2
                            on aa1.org_id = aa2.org_id
                            and aa2.status = 'A'
                    where aa1.user_id = ${+params.user_id}
                        and aa1.status = 'A'
                    order by aa2.sequence`;


    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}