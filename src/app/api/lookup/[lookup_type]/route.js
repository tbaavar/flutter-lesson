import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.bs_lookups.findMany({
      select: {
        lookup_code: true,
        name: true,
        description: true,
        sequence: true,
        Edit_Flag: true,
        lookup_type: true,
      },
      where: { lookup_type: params.lookup_type, status: "A" },
      orderBy: {
        sequence: "asc",
      },
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
