import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    let response = null;
    if (
      params.trade_type === "Spot_Sale" ||
      params.trade_type === "Term_Sale"
    ) {


      response = await prisma.$queryRaw`
      select 
        aa1.legal_entity_id,aa1.name,aa1.short_name,aa1.register_number
       from legal_entity aa1
         inner join bs_org_counterparties aa2
           on aa1.legal_entity_id = aa2.legal_entity_id
           and aa2.status= 'A'
       where aa2.org_id = ${+params.company_id}
          and aa1.customer_flag = 'Y'
         and aa1.status = 'A'
      order by aa1.sequence`;


      // response = await prisma.legal_entity.findMany({
      //   select: {
      //     legal_entity_id: true,
      //     name: true,
      //     short_name: true,
      //     register_number: true,
      //   },
      //   where: { customer_flag: "Y",org_id, status: "A" },
      // });
    } else if (
      params.trade_type === "Spot_Purchase" ||
      params.trade_type === "Term_Purchase"
    ) {


      response = await prisma.$queryRaw`
      select 
        aa1.legal_entity_id,aa1.name,aa1.short_name,aa1.register_number
       from legal_entity aa1
         inner join bs_org_counterparties aa2
           on aa1.legal_entity_id = aa2.legal_entity_id
           and aa2.status= 'A'
       where aa2.org_id = ${+params.company_id}
          and aa1.supplier_flag = 'Y'
         and aa1.status = 'A'
        order by aa1.sequence`;


      // response = await prisma.legal_entity.findMany({
      //   select: {
      //     legal_entity_id: true,
      //     name: true,
      //     short_name: true,
      //     register_number: true,
      //   },
      //   where: { supplier_flag: "Y", status: "A" },
      // });
    }

    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
