import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    let response = null;

    if (params.type === "Load") {
      response = await prisma.$queryRaw`
        select 
          aa1.counterparty_id,max(aa2.name) counterparty_name
         from voyage_parcels aa1
           inner join legal_entity aa2
             on aa1.counterparty_id = aa2.legal_entity_id
             and aa2.status= 'A'
         where aa1.voyage_id = ${+params.voyage_id}
                 and aa1.port_type = 'Discharge_port'
           and aa1.status = 'A'
     group by aa1.counterparty_id
     order by max(aa2.sequence)`;
    }

    if (params.type === "Discharge") {
      response = await prisma.$queryRaw`
        select 
          aa1.counterparty_id,max(aa2.name) counterparty_name
         from voyage_parcels aa1
           inner join legal_entity aa2
             on aa1.counterparty_id = aa2.legal_entity_id
             and aa2.status= 'A'
         where aa1.voyage_id =  ${+params.voyage_id}
                 and aa1.port_type = 'Load_port'
           and aa1.status = 'A'
        group by aa1.counterparty_id
         order by max(aa2.sequence)`;
    }

    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
