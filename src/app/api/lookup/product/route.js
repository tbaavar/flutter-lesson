import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.products.findMany({
      select: {
        product_id: true,
        product_name: true,
        product_description: true,
        sequence: true,
      },
      where: { status: "A" },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    const response = await prisma.products.create({
      data: {
        product_name: body.product_name,
        product_description: body.product_description,
        sequence: +body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    if (body.grades) {
      for (let i of body.grades) {
        await prisma.product_grades.create({
          data: {
            product_id: +response.product_id,
            grade_name: i.grade_name,
            grade_description: i.grade_description,
            sequence: +i.sequence,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.products.update({
      data: {
        product_name: body.product_name,
        product_description: body.product_description,
        sequence: +body.sequence,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { product_id: +body.product_id, status: "A" },
    });

    if (body.grades) {
      for (let i of body.grades) {
        if (i.type === "ADD") {
          await prisma.product_grades.create({
            data: {
              product_id: +body.product_id,
              grade_name: i.grade_name,
              grade_description: i.grade_description,
              sequence: +i.sequence,
              created_by: body.username,
              last_updated_by: body.username,
            },
          });
        } else if (i.type === "EDIT") {
          await prisma.product_grades.update({
            data: {
              grade_name: i.grade_name,
              grade_description: i.grade_description,
              sequence: +i.sequence,
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { grade_id: +i.grade_id, status: "A" },
          });
        }
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.products.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { product_id: +body.product_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
