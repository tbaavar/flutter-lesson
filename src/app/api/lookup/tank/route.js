import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.storage_terminal.findMany({
      select: {
        terminal_id: true,
        terminal_name: true,
      },
      where: { status: "A" },
    });

    const balance = await prisma.$queryRaw`
                          select 
                                bb1.tank_id,
                                bb1.terminal_id,
                                bb1.tank_name,
                                bb1.capacity capacity,
                                bb2.country_origin,
                                bb2.product_id,
                                bb2.product_name,
                                bb2.grade_id,
                                bb2.grade_name,
                                bb2.unit,
                                bb2.unit_name,
                                case when bb2.balance is null then 0 else bb2.balance end balance,
                                case when bb2.denstity is null then 0 else bb2.denstity end denstity
                          from (
                                select *
                                from storage_tank
                                where status = 'A'
                                ) bb1
                                left join (
                                          SELECT 
                                              aa1.terminal_id,
                                              aa1.tank_id,
                                              aa1.final_product_id  product_id,
                                              aa3.product_name product_name,
                                              aa1.final_grade_id  grade_id,
                                              aa4.grade_name grade_name,
                                              aa1.units unit,
                                              aa5.name unit_name,
                                              aa1.final_balance_mt balance,
                                              aa1.country_origin country_origin,
                                              aa1.final_density denstity
                                          FROM parcel_movement_balance aa1                                           
                                            inner join products aa3
                                              on aa1.final_product_id = aa3.product_id
                                              and aa3.status = 'A'
                                            inner join product_grades aa4
                                              on aa1.final_product_id = aa4.product_id
                                              and aa1.final_grade_id = aa4.grade_id
                                              and aa4.status = 'A'
                                            inner join bs_lookups aa5
                                              on aa1.units = aa5.lookup_code
                                              and aa5.lookup_type = 'Unit'
                                              and aa5.status = 'A'
                                          where aa1.status = 'A'                                          
                                            ) bb2
                                on bb1.tank_id = bb2.tank_id
                                and bb1.terminal_id = bb2.terminal_id
                                order by bb1.sequence
                `;

    const arr = [];

    for (let x of response) {
      const item_tanks = [];
      for (let b of balance) {
        if (x.terminal_id === b.terminal_id) {
          item_tanks.push(b);
        }
      }

      arr.push({
        terminal_id: x.terminal_id,
        terminal_name: x.terminal_name,
        tanks: item_tanks,
      });
    }

    return tokenResponse(null, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.storage_tank.create({
      data: {
        terminal_id: +body.terminal_id,
        tank_name: body.tank_name,
        tank_short_name: body.tank_short_name,
        tank_status: body.tank_status,
        capacity: +body.capacity,
        sequence: +body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.storage_tank.update({
      data: {
        terminal_id: +body.terminal_id,
        tank_name: body.tank_name,
        tank_short_name: body.tank_short_name,
        tank_status: body.tank_status,
        capacity: +body.capacity,
        sequence: +body.sequence,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { tank_id: +body.tank_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.storage_tank.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { tank_id: +body.tank_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
