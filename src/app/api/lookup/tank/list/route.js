import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response1 = await prisma.storage_tank.findMany({
      select: {
        tank_id: true,
        terminal_id: true,
        tank_name: true,
        tank_status: true,
        capacity: true,
        storage_terminal: { select: { terminal_name: true } },
      },
      where: { status: "A" },
    });

    return tokenResponse(null, response1);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
