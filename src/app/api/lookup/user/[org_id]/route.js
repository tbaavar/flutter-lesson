import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                                select 
                                    aa2.user_id,
                                    aa2.lastname,
                                    aa2.firstname,
                                    aa2.role
                                from bs_org_users aa1 
                                    inner join bs_users aa2
                                        on aa1.user_id = aa2.user_id
                                        and aa2.status = 'A'
                                    where aa1.org_id =  ${+params.org_id}
                                    and aa1.status ='A'`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
