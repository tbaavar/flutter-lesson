import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
          select 
                aa1.invoice_id,
                aa1.invoice_number,
                aa1.invoice_date,
                aa1.invoice_due_date,
                aa1.invoice_amount ::numeric as invoice_amount,
                COALESCE(aa2.paid_amount,0) ::numeric as paid_amount,
                aa1.invoice_amount - COALESCE(aa2.paid_amount,0) as unpaid_amount
          from invoices aa1
            left join (
                    select invoice_id,sum(value) ::numeric as paid_amount
                    from invoice_details
                    where balance_type = 'Credit'
                      and status = 'A'
                    group by invoice_id
                    ) aa2
                    on aa1.invoice_id = aa2.invoice_id
          where aa1.company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
            and aa1.counterparty_id = ${+params.counter_party_id}
            and aa1.invoice_type = 'SI'
            and aa1.status = 'A'
            and aa1.invoice_id <> ${+params.invoice_id}
            and aa1.invoice_payment_status ='Unpaid'
            `;
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
