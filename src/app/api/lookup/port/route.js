import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.ports.findMany({
      select: {
        port_id: true,
        port_name: true,
        description: true,
        sequence: true,
      },
      where: { status: "A" },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    const response = await prisma.ports.create({
      data: {
        port_name: body.port_name,
        description: body.description,
        sequence: +body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    if (body.orgs) {
      for (let i of body.orgs) {
        await prisma.bs_org_ports.create({
          data: {
            port_id: +response.port_id,
            org_id: +i.org_id,
            created_by: body.creation_by,
            last_updated_by: body.creation_by,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.ports.update({
      data: {
        port_name: body.port_name,
        description: body.description,
        sequence: +body.sequence,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { port_id: +body.port_id, status: "A" },
    });

    if (body.orgs) {
      await prisma.$queryRaw`UPDATE bs_org_ports set status = 'I', last_update_date =${new Date()}, last_updated_by = ${
        body.username
      } where port_id =${+body.port_id} and status = 'A'`;

      for (let i of body.orgs) {
        await prisma.bs_org_ports.create({
          data: {
            port_id: +body.port_id,
            org_id: +i.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.ports.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { port_id: +body.port_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
