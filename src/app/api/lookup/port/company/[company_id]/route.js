import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                    select aa1.port_id,aa2.port_name
                    from bs_org_ports aa1
                        inner join ports aa2
                            on aa1.port_id = aa2.port_id
                            and aa2.status = 'A'
                    where aa1.org_id = ${+params.company_id}
                        and aa1.status = 'A'
                    order by aa2.sequence`;


    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}