import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.products.findMany({
      select: {
        product_id: true,
        product_name: true,
        sequence: true,
        grades: {
          select: {
            grade_id: true,
            grade_name: true,
          },
        },
      },
      where: { status: "A" },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
