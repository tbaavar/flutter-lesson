import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
    select  
        aa1.trade_id,       
        COALESCE(aa6.name,'*') || '-' || COALESCE(aa1.trade_type,'*') || '-'||  COALESCE(aa3.product_name,'*') || ' [' || COALESCE(aa5.grade_name,'*') || '] - ['|| COALESCE(to_char(aa4.date_range_from,'Mon dd yyyy'),'*') || '-'|| COALESCE(to_char(aa4.date_range_to,'Mon dd yyyy'),'*') || ']-' || COALESCE(aa2.quantity,'0') as trade_name
    from trades aa1
      left join trade_products aa2
        on aa1.trade_id = aa2.trade_id
        and aa2.status = 'A'
      left  join products aa3
        on aa2.product_id = aa3.product_id
        and aa3.status = 'A'
      left join trade_deliveries aa4
        on aa1.trade_id = aa4.trade_id
        and aa4.status = 'A'
      left join product_grades aa5
        on aa2.product_id = aa5.product_id
        and aa2.grade_id = aa5.grade_id
        and aa5.status = 'A'
      left join legal_entity aa6
        on aa1.counterparty_id = aa6.legal_entity_id
        and aa6.status='A'
    where aa1.status = 'A'
    order by aa1.trade_type,aa1.trade_date desc`;


    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
