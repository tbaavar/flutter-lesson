import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.bs_lookups.findMany({
      select: {
        lookup_type: true,
        lookup_code: true,
        name: true,
        description: true,
        sequence: true,
        Edit_Flag: true,
      },
      where: { status: "A" },
    });
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.bs_lookups.create({
      data: {
        lookup_code: body.lookup_code,
        name: body.lookup_name,
        lookup_type: body.lookup_type,
        description: body.description,
        sequence: +body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.bs_lookups.update({
      data: {
        name: body.lookup_name,
        description: body.description,
        sequence: +body.sequence,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { lookup_code: body.lookup_code, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.bs_lookups.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { lookup_code: body.lookup_code, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
