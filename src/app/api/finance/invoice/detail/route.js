import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.invoice_details.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { invoice_detail_id: +body.invoice_detail_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
