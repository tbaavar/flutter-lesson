import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
  
    const response = await prisma.$queryRaw`
    SELECT 
          aa1.invoice_detail_id,
          aa1.description,
          aa1.cost_code,
          aa2.cost_name,
          aa1.value,
          aa1.tran_date,
          aa1.exp_pay_date,
          aa1.parent_invoice_detail_id,
          case when  aa1.parent_invoice_detail_id is not null then 'Offset' else null end offset_flag
    FROM invoice_details aa1
      inner join costs aa2
        on aa1.cost_code = aa2.cost_code
        and aa2.status = 'A'
    where aa1.invoice_id = ${+params.invoice_id}
        and aa1.balance_type = ${params.balance_type}
        and aa1.status = 'A'`;


    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
