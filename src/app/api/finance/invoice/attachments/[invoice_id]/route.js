import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.invoice_attachments.findMany({
      select: {
        invoice_attachment_id: true,
        attachment_type: true,
        attachment_name: true,
        attachment_url: true,
        description: true,
        filename:true,
      },
      where: { invoice_id: +params.invoice_id, status: "A" }
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
