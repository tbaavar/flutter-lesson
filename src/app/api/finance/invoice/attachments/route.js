import { putObject, removeObject } from "@/lib/minioClient";
import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const formData = await request.formData();
  try {
    const url = await putObject(
      formData.get("sourcefile"),
      formData.get("id"),
      formData.get("bucket"),
      formData.get("filename"),
      formData.get("attachment_type"),
      formData.get("filelength")
    );
    if (url.status === 200) {
      await prisma.invoice_attachments.create({
        data: {
          invoice_id: +formData.get("invoice_id"),
          attachment_type: formData.get("attachment_type"),
          attachment_name: formData.get("attachment_name"),
          description: formData.get("description"),
          filename: formData.get("filename"),
          created_by: formData.get("username"),
          last_updated_by: formData.get("username"),
          attachment_url: url.result.url,
        },
      });

      return tokenResponse(request);
    } else
      return Response.json(
        { message: "Error", result: url.result },
        { status: 500 }
      );
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  try {
    await prisma.invoice_attachments.update({
      data: {
        attachment_type: body.attachment_type,
        attachment_name: body.attachment_name,
        attachment_url: body.attachment_url,
        description: body.description,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: {
        invoice_attachment_id: +body.invoice_attachment_id,
        status: "A",
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    const del = await removeObject(body.bucket, body.id, body.filename);
    if (del.status === 200) {
      await prisma.invoice_attachments.update({
        data: {
          status: "I",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: {
          invoice_attachment_id: +body.invoice_attachment_id,
          status: "A",
        },
      });

      return tokenResponse(request);
    } else
      return Response.json(
        { message: "Error", result: del.status },
        { status: 500 }
      );
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
