import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";


export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.$queryRaw`
                  SELECT 
                        aa1.invoice_id,
                        aa1.company_id,
                        aa2.name company_name,
                        aa1.counterparty_id,
                        aa3.name counterparty_name,
                        aa1.invoice_number,
                        aa1.invoice_due_date,
                        aa1.currency,
                        aa1.invoice_amount,
                        aa1.invoice_payment_status,
                        aa1.invoice_type,
                        aa4.name invoice_type_name,
                        aa1.invoice_status
                  FROM "invoices" aa1
                    inner join bs_organizations aa2
                      on aa1.company_id = aa2.org_id
                      and aa2.status = 'A'
                    inner join legal_entity aa3
                      on aa1.counterparty_id = aa3.legal_entity_id
                      and aa3.status = 'A'
                    inner join bs_lookups aa4
                      on aa1.invoice_type = aa4.lookup_code
                      and aa4.lookup_type = 'Invoice_type'
                      and aa4.status = 'A'`;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
