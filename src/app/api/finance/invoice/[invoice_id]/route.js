import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    SELECT 
        aa1.invoice_id,
        aa1.company_id,        
        aa1.company_contact,
        aa1.counterparty_id,        
        aa1.counterparty_contact,
        aa1.invoice_owner_id,
        aa1.invoice_number,
        aa1.invoice_state,
        aa1.creditor_invoice_number,
        aa1.bank_id,
        aa1.account_id,
        aa1.invoice_date,
        aa1.invoice_due_date,
        aa1.currency,
        aa1.invoice_amount,
        aa1.invoice_payment_status,
        aa1.invoice_type,
        aa3.name invoice_type_name,
        aa1.invoice_status,
        aa1.invoice_description,
        aa1.invoice_comments,
        aa1.voyage_parcel_id,
        aa2.trade_id,
        aa10.amount,
        aa2.invoicing_unit,
        aa2.shipping_quantity_mt
    FROM invoices aa1
      inner join voyage_parcels aa2
        on aa1.voyage_parcel_id = aa2.voyage_parcel_id
        and aa2.status = 'A'
      inner join bs_lookups aa3
          on aa1.invoice_type = aa3.lookup_code
          and aa3.lookup_type = 'Invoice_type'
          and aa3.status = 'A'
      left join trade_pricing_fixed aa10
          on aa2.trade_id = aa10.trade_id
          and aa10.status = 'A'
    where aa1.invoice_id = ${+params.invoice_id} and aa1.status = 'A'`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
