import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function PUT(request) {
  const body = await request.json();
  try {
    const old_value = await prisma.invoices.findFirst({
      where: { invoice_id: +body.invoice_id, status: "A" },
    });

    const old_log = await prisma.log_invoices.findFirst({
      where: {
        invoice_id: +body.invoice_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    if (old_log) {
      await prisma.log_invoices.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_invoice_id: +old_log.log_invoice_id },
      });
    }

    await prisma.log_invoices.create({
      data: {
        invoice_id: +old_value.invoice_id,
        voyage_parcel_id: +old_value.voyage_parcel_id,
        company_id: old_value.company_id,
        company_contact: old_value.company_contact,
        counterparty_id: +old_value.counterparty_id,
        counterparty_contact: old_value.counterparty_contact,
        invoice_owner_id: +old_value.invoice_owner_id,
        invoice_number: old_value.invoice_number,
        invoice_state: old_value.invoice_state,
        creditor_invoice_number: old_value.creditor_invoice_number,
        invoice_type: old_value.invoice_type,
        invoice_date: new Date(old_value.invoice_date),
        invoice_due_date: new Date(old_value.invoice_due_date),
        bank_id: +old_value.bank_id,
        account_id: +old_value.account_id,
        invoice_amount: +old_value.invoice_amount,
        currency: old_value.currency,
        invoice_description: old_value.invoice_description,
        invoice_comments: old_value.invoice_comments,
        invoice_payment_status: old_value.invoice_payment_status,
        invoice_status: old_value.invoice_status,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    await prisma.invoices.update({
      data: {
        invoice_payment_status: body.payment_status,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { invoice_id: +body.invoice_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
