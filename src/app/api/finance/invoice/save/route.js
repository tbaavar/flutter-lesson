import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();
  let total = 0;
  try {
    const response = await prisma.invoices.create({
      data: {
        voyage_parcel_id: +body.voyage_parcel_id,
        company_id: +body.company_id,
        company_contact: body.company_contact,
        counterparty_id: +body.counterparty_id,
        counterparty_contact: body.counterparty_contact,
        invoice_owner_id: +body.invoice_owner_id,
        invoice_number: body.invoice_number,
        invoice_state: body.invoice_state,
        creditor_invoice_number: body.creditor_invoice_number,
        invoice_type: body.invoice_type,
        invoice_date: new Date(body.invoice_date),
        invoice_due_date: new Date(body.invoice_due_date),
        bank_id: +body.bank_id,
        account_id: +body.account_id,
        invoice_amount: +body.invoice_amount,
        currency: body.currency,
        invoice_description: body.invoice_description,
        invoice_comments: body.invoice_comments,
        invoice_payment_status: "Unpaid",
        invoice_status: "Saved",
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    if (body.detail) {

      await prisma.$transaction(async (prisma) => {
        const promises = body.detail.map(async (i) => {
          if (i.offset_inv_id === null) {
            return prisma.invoice_details.create({
              data: {
                invoice_id: +response.invoice_id,
                description: i.description,
                cost_code: i.cost_code,
                value: +i.value,
                tran_date: new Date(i.tran_date),
                exp_pay_date: new Date(i.exp_pay_date),
                balance_type: i.balance_type,
                parent_invoice_detail_id: null,
                created_by: body.username,
                last_updated_by: body.username,
              },
            });
          } else {
            const offsetA = await prisma.invoice_details.create({
              data: {
                invoice_id: +response.invoice_id,
                description: i.description,
                cost_code: i.cost_code,
                value: +i.value,
                tran_date: new Date(i.tran_date),
                exp_pay_date: new Date(i.exp_pay_date),
                balance_type: i.balance_type,
                parent_invoice_detail_id: null,
                created_by: body.username,
                last_updated_by: body.username,
              },
            });
      
            const offsetB = await prisma.invoice_details.create({
              data: {
                invoice_id: +i.offset_inv_id,
                description: i.description,
                cost_code: i.cost_code,
                value: +i.value,
                tran_date: new Date(i.tran_date),
                exp_pay_date: new Date(i.exp_pay_date),
                balance_type: i.balance_type,
                parent_invoice_detail_id: +offsetA.invoice_detail_id,
                created_by: body.username,
                last_updated_by: body.username,
              },
            });
      
            await prisma.invoice_details.update({
              data: {
                parent_invoice_detail_id: +offsetB.invoice_detail_id,
                last_update_date: new Date(),
                last_updated_by: body.username,
              },
              where: {
                invoice_detail_id: +offsetA.invoice_detail_id,
              },
            });
          }
      
          if (i.balance_type === "Credit") total += i.value;
        });
      
        // Execute all operations concurrently
        await Promise.all(promises);
      });
      // for (let i of body.detail) {
      //   if (i.offset_inv_id === null) {
      //     await prisma.invoice_details.create({
      //       data: {
      //         invoice_id: +response.invoice_id,
      //         description: i.description,
      //         cost_code: i.cost_code,
      //         value: +i.value,
      //         tran_date: new Date(i.tran_date),
      //         exp_pay_date: new Date(i.exp_pay_date),
      //         balance_type: i.balance_type,
      //         parent_invoice_detail_id: null,
      //         created_by: body.username,
      //         last_updated_by: body.username,
      //       },
      //     });
      //   } else {
      //     const offsetA = await prisma.invoice_details.create({
      //       data: {
      //         invoice_id: +response.invoice_id,
      //         description: i.description,
      //         cost_code: i.cost_code,
      //         value: +i.value,
      //         tran_date: new Date(i.tran_date),
      //         exp_pay_date: new Date(i.exp_pay_date),
      //         balance_type: i.balance_type,
      //         parent_invoice_detail_id: null,
      //         created_by: body.username,
      //         last_updated_by: body.username,
      //       },
      //     });

      //     const offsetB = await prisma.invoice_details.create({
      //       data: {
      //         invoice_id: +i.offset_inv_id,
      //         description: i.description,
      //         cost_code: i.cost_code,
      //         value: +i.value,
      //         tran_date: new Date(i.tran_date),
      //         exp_pay_date: new Date(i.exp_pay_date),
      //         balance_type: i.balance_type,
      //         parent_invoice_detail_id: +offsetA.invoice_detail_id,
      //         created_by: body.username,
      //         last_updated_by: body.username,
      //       },
      //     });

      //     await prisma.invoice_details.update({
      //       data: {
      //         parent_invoice_detail_id: +offsetB.invoice_detail_id,
      //         last_update_date: new Date(),
      //         last_updated_by: body.username,
      //       },
      //       where: {
      //         invoice_detail_id: +offsetA.invoice_detail_id,
      //       },
      //     });
      //   }
      //   if (i.balance_type === "Credit") total = total + i.value;
      // }

      if (body.invoice_amount <= total && total !== 0) {
        await prisma.invoices.update({
          data: {
            invoice_payment_status: "Paid",
            last_update_date: new Date(),
            last_updated_by: body.username,
          },
          where: { invoice_id: +response.invoice_id, status: "A" },
        });
      }
    }

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();
  let total = 0;
  try {
    const old_value = await prisma.invoices.findFirst({
      where: { invoice_id: +body.invoice_id, status: "A" },
    });

    const old_log = await prisma.log_invoices.findFirst({
      where: {
        invoice_id: +body.invoice_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    if (old_log) {
      await prisma.log_invoices.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_invoice_id: +old_log.log_invoice_id },
      });
    }

    await prisma.log_invoices.create({
      data: {
        invoice_id: +old_value.invoice_id,
        voyage_parcel_id: +old_value.voyage_parcel_id,
        company_id: +old_value.company_id,
        company_contact: old_value.company_contact,
        counterparty_id: +old_value.counterparty_id,
        counterparty_contact: old_value.counterparty_contact,
        invoice_owner_id: +old_value.invoice_owner_id,
        invoice_number: old_value.invoice_number,
        invoice_state: old_value.invoice_state,
        creditor_invoice_number: old_value.creditor_invoice_number,
        invoice_type: old_value.invoice_type,
        invoice_date: new Date(old_value.invoice_date),
        invoice_due_date: new Date(old_value.invoice_due_date),
        bank_id: +old_value.bank_id,
        account_id: +old_value.account_id,
        invoice_amount: +old_value.invoice_amount,
        currency: old_value.currency,
        invoice_description: old_value.invoice_description,
        invoice_comments: old_value.invoice_comments,
        invoice_payment_status: old_value.invoice_payment_status,
        invoice_status: old_value.invoice_status,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    await prisma.invoices.update({
      data: {
        company_id: +body.company_id,
        company_contact: body.company_contact,
        counterparty_id: +body.counterparty_id,
        counterparty_contact: body.counterparty_contact,
        invoice_owner_id: +body.invoice_owner_id,
        invoice_number: body.invoice_number,
        invoice_state: body.invoice_state,
        creditor_invoice_number: body.creditor_invoice_number,
        invoice_type: body.invoice_type,
        invoice_date: new Date(body.invoice_date),
        invoice_due_date: new Date(body.invoice_due_date),
        bank_id: +body.bank_id,
        account_id: +body.account_id,
        invoice_amount: +body.invoice_amount,
        currency: body.currency,
        invoice_description: body.invoice_description,
        invoice_comments: body.invoice_comments,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { invoice_id: +body.invoice_id, status: "A" },
    });

    if (body.detail) {
      for (let i of body.detail) {
        if (i.type === "DEL" || i.type === "EDIT") {
          const old_valueDetail = await prisma.invoice_details.findFirst({
            where: { invoice_detail_id: +i.invoice_detail_id, status: "A" },
          });

          const old_logDetail = await prisma.log_invoice_details.findFirst({
            where: {
              invoice_detail_id: +i.invoice_detail_id,
              latest_change_flag: "Y",
              status: "A",
            },
          });

          if (old_logDetail) {
            await prisma.log_invoice_details.update({
              data: {
                latest_change_flag: "N",
                last_update_date: new Date(),
                last_updated_by: body.username,
              },
              where: {
                log_invoice_detail_id: +old_logDetail.log_invoice_detail_id,
              },
            });
          }

          await prisma.log_invoice_details.create({
            data: {
              invoice_id: +old_valueDetail.invoice_id,
              invoice_detail_id: +old_valueDetail.invoice_detail_id,
              description: old_valueDetail.description,
              cost_code: old_valueDetail.cost_code,
              value: +old_valueDetail.value,
              tran_date: new Date(old_valueDetail.tran_date),
              exp_pay_date: new Date(old_valueDetail.exp_pay_date),
              balance_type: old_valueDetail.balance_type,
              parent_invoice_detail_id:
                +old_valueDetail.parent_invoice_detail_id,
              created_by: body.username,
              last_updated_by: body.username,
            },
          });
        }

        if (i.type === "ADD") {
          if (i.offset_inv_id === null) {
            await prisma.invoice_details.create({
              data: {
                invoice_id: +body.invoice_id,
                description: i.description,
                cost_code: i.cost_code,
                value: +i.value,
                tran_date: new Date(i.tran_date),
                exp_pay_date: new Date(i.exp_pay_date),
                balance_type: i.balance_type,
                parent_invoice_detail_id: null,
                created_by: body.username,
                last_updated_by: body.username,
              },
            });
          } else {
            const offsetA = await prisma.invoice_details.create({
              data: {
                invoice_id: +body.invoice_id,
                description: i.description,
                cost_code: i.cost_code,
                value: +i.value,
                tran_date: new Date(i.tran_date),
                exp_pay_date: new Date(i.exp_pay_date),
                balance_type: i.balance_type,
                parent_invoice_detail_id: null,
                created_by: body.username,
                last_updated_by: body.username,
              },
            });

            const offsetB = await prisma.invoice_details.create({
              data: {
                invoice_id: +i.offset_inv_id,
                description: i.description,
                cost_code: i.cost_code,
                value: +i.value,
                tran_date: new Date(i.tran_date),
                exp_pay_date: new Date(i.exp_pay_date),
                balance_type: i.balance_type,
                parent_invoice_detail_id: +offsetA.invoice_detail_id,
                created_by: body.username,
                last_updated_by: body.username,
              },
            });

            await prisma.invoice_details.update({
              data: {
                parent_invoice_detail_id: +offsetB.invoice_detail_id,
                last_update_date: new Date(),
                last_updated_by: body.username,
              },
              where: {
                invoice_detail_id: +offsetA.invoice_detail_id,
              },
            });
          }
        } else if (i.type === "EDIT") {
          await prisma.invoice_details.update({
            data: {
              description: i.description,
              cost_code: i.cost_code,
              value: +i.value,
              tran_date: new Date(i.tran_date),
              exp_pay_date: new Date(i.exp_pay_date),
              balance_type: i.balance_type,
              parent_invoice_detail_id: +i.parent_invoice_detail_id,
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { invoice_detail_id: i.invoice_detail_id, status: "A" },
          });
        } else if (i.type === "DEL") {
          await prisma.invoice_details.update({
            data: {
              status: "I",
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { invoice_detail_id: i.invoice_detail_id, status: "A" },
          });
        }

        if (
          i.balance_type === "Credit" &&
          (i.type === "EDIT" || i.type === "ADD")
        )
          total = total + i.value;

        if (body.invoice_amount <= total && total !== 0) {
          await prisma.invoices.update({
            data: {
              invoice_payment_status: "Paid",
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { invoice_id: +body.invoice_id, status: "A" },
          });
        }
        else 
        {
          await prisma.invoices.update({
            data: {
              invoice_payment_status: "Unpaid",
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: { invoice_id: +body.invoice_id, status: "A" },
          });
        }
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();
  try {
    await prisma.invoices.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { invoice_id: +body.invoice_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
