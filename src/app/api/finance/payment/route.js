import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();

  let total = 0;
  try {

    await prisma.$transaction(async (prisma) => {
      const promises = body.payment.map(async (i) => {
        if (i.payment_id) {
          const old_valueDetail = await prisma.invoice_payments.findFirst({
            where: { payment_id: +i.payment_id, status: "A" },
          });
    
          const old_logDetail = await prisma.log_invoice_payments.findFirst({
            where: {
              payment_id: +i.payment_id,
              latest_change_flag: "Y",
              status: "A",
            },
          });
    
          if (old_logDetail) {
            await prisma.log_invoice_payments.update({
              data: {
                latest_change_flag: "N",
                last_update_date: new Date(),
                last_updated_by: body.username,
              },
              where: {
                log_payment_id: +old_logDetail.log_payment_id,
              },
            });
    
            await prisma.log_invoice_payments.create({
              data: {
                invoice_id: +old_valueDetail.invoice_id,
                payment_id: +old_valueDetail.payment_id,
                pay_date: new Date(old_valueDetail.pay_date),
                value: +old_valueDetail.value,
                description: old_valueDetail.description,
                created_by: old_valueDetail.created_by,
                last_updated_by: old_valueDetail.last_updated_by,
              },
            });
          }
    
          await prisma.invoice_payments.update({
            data: {
              pay_date: new Date(i.pay_date),
              value: +i.value,
              description: i.description,
              last_update_date: new Date(),
              last_updated_by: i.last_updated_by,
            },
            where: { payment_id: +i.payment_id, status: "A" },
          });
        } else {
          const payment = await prisma.invoice_payments.create({
            data: {
              invoice_id: +body.invoice_id,
              pay_date: new Date(i.pay_date),
              value: +i.value,
              description: i.description,
              created_by: i.created_by,
              last_updated_by: i.last_updated_by,
            },
          });
    
          await prisma.log_invoice_payments.create({
            data: {
              invoice_id: +body.invoice_id,
              payment_id: payment.payment_id,
              pay_date: new Date(i.pay_date),
              value: +i.value,
              description: i.description,
              created_by: i.created_by,
              last_updated_by: i.last_updated_by,
            },
          });
        }
        total += i.value;
      });
    
      // Execute all operations concurrently
      await Promise.all(promises);
    });
    // for (let i of body.payment) {
    //   if (i.payment_id) {
    //     const old_valueDetail = await prisma.invoice_payments.findFirst({
    //       where: { payment_id: +i.payment_id, status: "A" },
    //     });

    //     const old_logDetail = await prisma.log_invoice_payments.findFirst({
    //       where: {
    //         payment_id: +i.payment_id,
    //         latest_change_flag: "Y",
    //         status: "A",
    //       },
    //     });

    //     if (old_logDetail) {
    //       await prisma.log_invoice_payments.update({
    //         data: {
    //           latest_change_flag: "N",
    //           last_update_date: new Date(),
    //           last_updated_by: body.username,
    //         },
    //         where: {
    //           log_payment_id: +old_logDetail.log_payment_id,
    //         },
    //       });

    //       await prisma.log_invoice_payments.create({
    //         data: {
    //           invoice_id: +old_valueDetail.invoice_id,
    //           payment_id: +old_valueDetail.payment_id,
    //           pay_date: new Date(old_valueDetail.pay_date),
    //           value: +old_valueDetail.value,
    //           description: old_valueDetail.description,
    //           created_by: old_valueDetail.created_by,
    //           last_updated_by: old_valueDetail.last_updated_by,
    //         },
    //       });
    //     }

    //     await prisma.invoice_payments.update({
    //       data: {
    //         pay_date: new Date(i.pay_date),
    //         value: +i.value,
    //         description: i.description,
    //         last_update_date: new Date(),
    //         last_updated_by: i.last_updated_by,
    //       },
    //       where: { payment_id: +i.payment_id, status: "A" },
    //     });
    //   } else {
    //     const payment = await prisma.invoice_payments.create({
    //       data: {
    //         invoice_id: +body.invoice_id,
    //         pay_date: new Date(i.pay_date),
    //         value: +i.value,
    //         description: i.description,
    //         created_by: i.created_by,
    //         last_updated_by: i.last_updated_by,
    //       },
    //     });
    //     await prisma.log_invoice_payments.create({
    //       data: {
    //         invoice_id: +i.invoice_id,
    //         payment_id: payment.payment_id,
    //         pay_date: new Date(i.pay_date),
    //         value: +i.value,
    //         description: i.description,
    //         created_by: i.created_by,
    //         last_updated_by: i.last_updated_by,
    //       },
    //     });
    //   }
    //   total = total + i.value;
    // }

    if (body.invoice_mount <= total && total !== 0) {
      await prisma.invoices.update({
        data: {
          invoice_payment_status: "Paid",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { invoice_id: +body.invoice_id, status: "A" },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.invoice_payments.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { payment_id: +body.payment_id, status: "A" },
    });

    await prisma.invoices.update({
      data: {
        invoice_payment_status: "Unpaid",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { invoice_id: +body.invoice_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
