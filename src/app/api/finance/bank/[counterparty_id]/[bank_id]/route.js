import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const accounts = await prisma.$queryRaw`
                            select le_account_id as account_id,account_number as account_no,bank_id,account_name
                            from le_bank_accounts 
                            where legal_entity_id = ${+params.counterparty_id} 
                                  and bank_id= ${+params.bank_id} and status = 'A'`;

    return tokenResponse(request, accounts);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
