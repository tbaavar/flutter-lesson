import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const banks = await prisma.$queryRaw`
                                select bank_id,bank_name
                                from banks
                                where status = 'A'`;

    const accounts = await prisma.$queryRaw`
                            select le_account_id as account_id,account_number as account_no,bank_id,account_name
                            from le_bank_accounts where legal_entity_id = ${+params.counterparty_id} and status = 'A'`;

    const arr = [];

    for (let x of banks) {
      const account = [];
      for (let b of accounts) {
        if (x.bank_id === b.bank_id) {
          account.push(b);
        }
      }

      arr.push({
        bank_id: x.bank_id,
        bank_name: x.bank_name,
        account: account,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
