import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const banks = await prisma.$queryRaw`
                        select bank_id,bank_name
                        from banks
                        where status = 'A'`;

    return tokenResponse(null, banks);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
