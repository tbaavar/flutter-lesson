import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";


export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const trades = await prisma.$queryRaw`
                                select  
                                    aa1.trade_id,       
                                    COALESCE(aa6.name,'*') || '-' || COALESCE(aa1.trade_type,'*') || '-'||  COALESCE(aa3.product_name,'*') || ' [' || COALESCE(aa5.grade_name,'*') || '] - ['|| COALESCE(to_char(aa4.date_range_from,'Mon dd yyyy'),'*') || '-'|| COALESCE(to_char(aa4.date_range_to,'Mon dd yyyy'),'*') || ']-' || COALESCE(aa2.quantity,'0') as trade_name
                                from trades aa1
                                left join trade_products aa2
                                    on aa1.trade_id = aa2.trade_id
                                    and aa2.status = 'A'
                                left  join products aa3
                                    on aa2.product_id = aa3.product_id
                                    and aa3.status = 'A'
                                left join trade_deliveries aa4
                                    on aa1.trade_id = aa4.trade_id
                                    and aa4.status = 'A'
                                left join product_grades aa5
                                    on aa2.product_id = aa5.product_id
                                    and aa2.grade_id = aa5.grade_id
                                    and aa5.status = 'A'
                                left join legal_entity aa6
                                    on aa1.counterparty_id = aa6.legal_entity_id
                                    and aa6.status='A'                                
                                where aa1.status = 'A' 
                                      and aa1.trade_id in (select trade_id from voyage_parcels where status = 'A')
                                      and aa2.finance_active_flag = 'Y'
                                      and aa1.company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
                                order by aa1.trade_type,aa1.trade_date desc`;

    const parcels = await prisma.$queryRaw`
                            select 
                                aa1.voyage_parcel_id,
                                aa1.trade_id,
                                aa1.parcel_number,
                                aa1.invoicing_unit,
                                aa1.transfer_date,
                                aa2.name counterparty_name,
                                aa3.product_name,
                                aa4.grade_name,
                                aa1.shipping_quantity_mt,
                                aa1.trade_code,
                                aa1.operator_user_id,
                                aa1.counterparty_id,
                                aa10.amount,
                                aa10.currency
                            from voyage_parcels aa1
                                inner join voyages aa1_1
                                    on aa1.voyage_id = aa1_1.voyage_id
                                    and aa1_1.status = 'A'
                                inner join legal_entity aa2
                                    on aa1.counterparty_id = aa2.legal_entity_id
                                    and aa2.status = 'A'
                                inner join products aa3
                                    on aa1.product_id = aa3.product_id
                                    and aa3.status = 'A'
                                inner join product_grades aa4
                                    on aa1.grade_id = aa4.grade_id
                                    and aa1.product_id = aa4.product_id
                                    and aa4.status = 'A'
                                left join trade_pricing_fixed	aa10
                                    on aa1.trade_id = aa10.trade_id
                                    and aa10.status = 'A'
                            where aa1.org_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
                                    and aa1.status = 'A'`;

    const arr = [];

    for (let x of trades) {
      const parcel = [];
      for (let b of parcels) {
        if (x.trade_id === b.trade_id) {
          parcel.push(b);
        }
      }

      arr.push({
        trade_id: x.trade_id,
        trade_name: x.trade_name,
        parcels: parcel,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
