import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
    select
	aa1.log_invoice_id,
	aa1.voyage_parcel_id,
	aa2.parcel_number,
	aa1.invoice_id,
	aa1.invoice_number,
	aa1.company_id,
	aa3.name company_name,
	aa1.company_contact,
	aa1.counterparty_id,
	aa4.name counterparty_name,
	aa1.counterparty_contact,
	substring(aa5.lastname,1,1) || '.' || aa5.firstname invoice_owner,
	aa6.name invoice_type,
	aa1.invoice_state,
	aa1.invoice_date,
	aa1.invoice_due_date,
	aa7.bank_name bank,
	aa8.account_name || ' ' || aa8.account_number as account,
	aa1.invoice_amount,
	aa9.name currency,
	aa1.invoice_description,
	aa1.invoice_comments,
	aa10.name invoice_payment_status,
	aa11.name invoice_status,
	aa1.latest_change_flag,
	aa1.status,
	aa1.created_by,
	aa1.creation_date,
	aa1.last_updated_by,
	aa1.last_update_date
from log_invoices aa1
	left join voyage_parcels aa2
		on aa1.voyage_parcel_id = aa2.voyage_parcel_id
		and aa2.status = 'A'
	left join bs_organizations aa3
		on aa1.company_id = aa3.org_id
		and aa3.status = 'A'
	left join legal_entity aa4
		on aa1.counterparty_id = aa4.legal_entity_id
		and aa4.status = 'A'
 left join bs_users aa5
		on aa1.invoice_owner_id = aa5.user_id		
		and aa5.status = 'A'
	left join bs_lookups aa6
		on aa1.invoice_type = aa6.lookup_code
		and aa6.lookup_type = 'Invoice_type'
		and aa6.status = 'A'
	left join banks aa7
		on aa1.bank_id = aa7.bank_id
		and aa7.status = 'A'
	left join le_bank_accounts aa8
		on aa1.account_id = aa8.le_account_id
		and aa8.status ='A'
	left join bs_lookups aa9
		on aa1.currency = aa9.lookup_code
		and aa9.lookup_type = 'Currency'
		and aa9.status = 'A'
	left join bs_lookups aa10
		on aa1.invoice_payment_status = aa10.lookup_code
		and aa10.lookup_type = 'Invoice_payment_status'
		and aa10.status = 'A'
	left join bs_lookups aa11
		on aa1.invoice_status = aa11.lookup_code
		and aa11.lookup_type = 'Invoice_status'
		and aa11.status = 'A'
order by aa1.voyage_parcel_id asc,aa1.invoice_id asc,aa1.last_update_date desc`;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
