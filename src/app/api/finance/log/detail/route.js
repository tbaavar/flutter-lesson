import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
    select 
        aa1.log_invoice_detail_id,
        aa1.invoice_detail_id,
        aa1.invoice_id,
        aa2.invoice_number,
        aa1.balance_type,
        aa3.cost_name,
        aa1.value,
        aa1.tran_date,
        aa1.exp_pay_date,
        aa1.description,
        aa1.parent_invoice_detail_id,
        aa1.latest_change_flag,
        aa1.status,
        aa1.created_by,
        aa1.creation_date,
        aa1.last_updated_by,
        aa1.last_update_date
    from log_invoice_details aa1
        inner join invoices aa2
                on aa1.invoice_id = aa2.invoice_id
                and aa2.status = 'A'
        inner join costs aa3
            on aa1.cost_code = aa3.cost_code
            and aa3.status = 'A'
    order by aa1.invoice_detail_id,aa1.last_update_date desc`;

    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
