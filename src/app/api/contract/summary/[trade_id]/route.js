import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
			select 
				aa1.deal_id,
				aa1.trade_id,
				aa1.trade_type,
				aa1.trade_date,
				aa18.name counter_party,
				aa17.name group_co,		
				aa1.terms,		
				aa1.trade_origin,
				aa16.firstname || ' ' || aa16.lastname trader_name,
				aa13.firstname || ' ' || aa13.lastname contract_name,
				aa1.delivery_method,				
				aa1.contract_type,
				aa1.trade_status,
				aa11.firstname || ' ' || aa11.lastname operator_name,		
				aa12.firstname || ' ' || aa12.lastname finance_name,
				aa9.product_id,
				aa14.product_name,		
				aa9.grade_id,
				aa15.grade_name,		
				aa9.quantity as quantity,
				aa6.pricing_type,				
				aa10.amount || ' ' || aa10.currency || ' ' || aa10.per_unit est_price,				
				aa4.quantity_based_on as quantity_to_base_pricing,				
				aa4.load_port_id as load_port,
				aa4.intended_vessel_id as vessel,
				aa4.laytime as laytime,
				aa4.nor as nor,
				aa4.pro_rata_cargo_flag as pro_rata_cargo_flag,
				aa5.general_term_condition as generaltc,
				aa4.first_purchaser_flag as first_purchaser_flag,
				aa4.tax_withheld_flag as tax_withheld_flag,
				aa4.dates_based_on,
				aa4.date_range_from || 'to' ||aa4.date_range_to	date_range,
				aa9.est_bl_date,					
				aa4.pdc_to_apply,
				aa3.day_1 || ' ' || time_condition_1 || ' '  || condition_1 payment,				
				aa5.contract_received_date,
				aa5.response_sent_date,
				aa5.contract_drafted_date,
				aa5.contact_sent_date,
				aa5.broker_contract_received_date,
				case when aa5.contract_finish_flag = 'Y' then 'Yes' else 'No' end contract_finish_flag,
				aa19.name conflict_name,
				aa20.name law_name
		from trades aa1		
			left join trade_payments aa3
				on aa1.trade_id = aa3.trade_id
				and aa3.status = 'A'	
			left join trade_deliveries aa4
				on aa1.trade_id = aa4.trade_id
				and aa4.status = 'A'
			left join trade_laws aa5
				on aa1.trade_id = aa5.trade_id
				and aa5.status = 'A'
			left join trade_pricing aa6
				on aa1.trade_id = aa6.trade_id
				and aa5.status = 'A'	
			left join trade_pricing_dates aa8
				on aa1.trade_id = aa8.trade_id
				and aa8.status = 'A'	
			left join trade_products aa9
				on aa1.trade_id = aa9.trade_id
				and aa9.status = 'A'	
			left join (
					select trade_id,sum(amount) amount,max(currency) currency,max(unit) per_unit
					from trade_pricing_fixed
					where status = 'A'
					group by trade_id
						) aa10
				on aa1.trade_id = aa10.trade_id
			left join bs_users aa11
				on aa1.operator_user_id = aa11.user_id		
				and aa11.status = 'A'
			left join bs_users aa12
				on aa1.finance_user_id = aa12.user_id	
				and aa12.status = 'A'
			left join bs_users aa13
			on aa1.contract_user_id = aa13.user_id	
				and aa13.status = 'A'
			left join products aa14
				on aa9.product_id = aa14.product_id
				and aa14.status = 'A'
			left join product_grades aa15
				on aa9.grade_id = aa15.grade_id
				and aa9.product_id = aa15.product_id
				and aa15.status = 'A'	
			left join bs_users aa16
				on aa1.trader_user_id = aa16.user_id
				and aa16.status = 'A'	
			left join bs_organizations aa17
				on aa1.company_id = aa17.org_id
				and aa17.status = 'A'
			left join legal_entity aa18
				on aa1.counterparty_id = aa18.legal_entity_id
				and aa18.status = 'A'	
			left join bs_lookups aa19
				on aa5.conflict = aa19.lookup_code
				and aa19.lookup_type = 'Law'
				and aa19.status = 'A'
			left join bs_lookups aa20
				on aa5.law_id = aa20.lookup_code
				and aa20.lookup_type = 'Law'
				and aa20.status = 'A'
		where aa1.trade_id =${+params.trade_id} 
			and aa1.status = 'A'`;

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
