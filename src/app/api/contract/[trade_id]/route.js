import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.trade_contracts.findMany({
      select: {
        trade_contract_id: true,
        attachment_type: true,
        attachment_name: true,
        filename: true,
        attachment_url: true,
        description: true,
        creation_date: true,
      },
      where: { trade_id: +params.trade_id, status: "A" },
    });

    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
