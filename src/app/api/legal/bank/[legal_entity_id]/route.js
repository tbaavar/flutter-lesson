import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.le_bank_accounts.findMany({
      select: {
        le_account_id:true,
        bank_id:true,
        bank_address: true,
        account_name:true,
        account_number: true,
        swift:true,
       
      },
      where: { legal_entity_id: +params.legal_entity_id, status: "A" },
      
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
