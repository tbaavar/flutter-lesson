import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.le_addresses.findMany({
      select: {
        address_id:true,
        address_type:true,
        origin: true,
        city:true,
        address: true,
       
      },
      where: { legal_entity_id: +params.legal_entity_id, status: "A" },
      
    });
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
