import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.legal_entity.findMany({
      select: {
        legal_entity_id: true,
        name: true,
        short_name: true,
        register_number: true,
        supplier_flag: true,
        customer_flag: true,
        name_eng: true,
        shortname_eng: true,
        web: true,
        mail: true,
        phone: true,
        logo: true,
        tax_number: true,
        sequence: true,
      },
      where: { status: "A" },
      orderBy: {
        sequence: "asc",
      },
    });

    const orgList = await prisma.$queryRaw`
    select 
        aa1.legal_entity_id,
        aa1.org_id,aa3.name org_name
    from bs_org_counterparties aa1
        inner join legal_entity aa2
            on aa1.legal_entity_id = aa2.legal_entity_id
            and aa2.status = 'A'
        inner join bs_organizations aa3
          on aa1.org_id = aa3.org_id
          and aa3.status = 'A'
    where  aa1.status = 'A'`;

    const arr = [];

    for (let x of response) {
      const orgs = [];
      for (let b of orgList) {
        if (x.legal_entity_id === b.legal_entity_id) {
          orgs.push(b);
        }
      }

      arr.push({
        legal_entity_id: x.legal_entity_id,
        name: x.name,
        short_name: x.short_name,
        register_number: x.register_number,
        supplier_flag: x.supplier_flag,
        customer_flag: x.customer_flag,
        name_eng: x.name_eng,
        shortname_eng: x.shortname_eng,
        web: x.web,
        mail: x.mail,
        phone: x.phone,
        logo: x.logo,
        tax_number: x.tax_number,
        sequence: x.sequence,
        orgs: orgs,
      });
    }

    return tokenResponse(null, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    const response = await prisma.legal_entity.create({
      data: {
        register_number: body.register_number,
        name: body.name,
        short_name: body.short_name,
        supplier_flag: body.supplier_flag,
        customer_flag: body.customer_flag,
        name_eng: body.name_eng,
        shortname_eng: body.shortname_eng,
        web: body.web,
        mail: body.mail,
        phone: body.phone,
        logo: body.logo,
        tax_number: body.tax_number,
        sequence: +body.sequence,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    if (body.orgs) {
      for (let i of body.orgs) {
        await prisma.bs_org_counterparties.create({
          data: {
            legal_entity_id: +response.legal_entity_id,
            org_id: +i.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.legal_entity.update({
      data: {
        register_number: body.register_number,
        name: body.name,
        short_name: body.short_name,
        supplier_flag: body.supplier_flag,
        customer_flag: body.customer_flag,
        name_eng: body.name_eng,
        shortname_eng: body.shortname_eng,
        web: body.web,
        mail: body.mail,
        phone: body.phone,
        logo: body.logo,
        tax_number: body.tax_number,
        sequence: +body.sequence,
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { legal_entity_id: +body.legal_entity_id, status: "A" },
    });

    if (body.orgs) {
      await prisma.$queryRaw`UPDATE bs_org_counterparties set status = 'I', last_update_date =${new Date()}, last_updated_by = ${
        body.username
      } where legal_entity_id =${+body.legal_entity_id} and status = 'A'`;

      for (let i of body.orgs) {
        await prisma.bs_org_counterparties.create({
          data: {
            legal_entity_id: +body.legal_entity_id,
            org_id: +i.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      }
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.legal_entity.update({
      data: {
        status: "I",
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { legal_entity_id: +body.legal_entity_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
