import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function DELETE(request) {
  const body = await request.json();

  try {
    await prisma.voyage_attachments.update({
      data: {
        status: "I",
        last_updated_by: body.username,
        last_update_date: new Date(),
      },
      where: { voyage_attachment_id: +body.voyage_attachment_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
