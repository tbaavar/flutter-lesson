import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const voyages = await prisma.$queryRaw`
                      select 
                        aa1.voyage_id,
                        aa1.intended_vessel,
                        aa1.product_id,
                        aa1.voyage_title,
                        aa1.voyage_status,
                        aa1.charter_type,
                        aa1.charter_party_date,
                        aa1.insurance_required_flag,
                        aa1.insurance_externally_flag,
                        aa1.insured_description,
                        aa1.insurance_status,
                        aa1.quantity_discrepancy_flag,
                        aa1.quantity_discrepancy_comment,
                        aa1.voyage_comment,
                        aa1.org_id,
                        aa2.name org_name
                      from voyages aa1
                            inner join bs_organizations aa2
                              on aa1.org_id = aa2.org_id
                              and aa2.status = 'A'
                      where aa1.org_id in (select org_id from bs_org_users where user_id = ${+params.user_id} and status = 'A') and aa1.status = 'A'
                        and aa1.voyage_status != 'Closed_voyage'
                      order by aa1.org_id`;

    const ports = await prisma.$queryRaw`
                      select 
                          aa1.voyage_port_id,
                          aa1.voyage_id,
                          aa1.port_id,
                          aa2.port_name,
                          aa1.port_type,
                          aa3.name port_type_name,
                          aa1.agent_id,
                          aa1.superveyor_id,
                          aa1.superintendent_id,
                          case when aa1.port_type in ('STS_load','STS_disport','Loss ') then aa3.name else aa3.name  || ' ' || aa2.port_name end port_title
                      from voyage_ports aa1
                        left join ports aa2
                          on aa1.port_id = aa2.port_id
                          and aa2.status = 'A'
                        inner join bs_lookups aa3
                          on aa1.port_type = aa3.lookup_code
                          and aa3.lookup_type = 'Port_type'
                        where aa1.status = 'A'
                      order by case when aa1.port_type = 'Load_port' then 1
                                    when aa1.port_type = 'Discharge_port' then 2
                                    when aa1.port_type = 'STS_load' then 3
                                    when aa1.port_type = 'STS_disport' then 4
                                end,aa1.creation_date desc
                                    `;

    const documents = await prisma.$queryRaw`
select 
    voyage_attachment_id,
    voyage_id,
    attachment_type,
    attachment_name,
    attachment_url,
    description
from voyage_attachments
where status = 'A'`;

    const parcels = await prisma.$queryRaw`
          select					
						cc1.voyage_parcel_id,
						cc1.voyage_id,
						cc1.voyage_port_id,
						cc1.trade_id,
						cc8.trade_type,
						cc10.name trade_type_name,		
						cc8.company_id,
						cc12.name company_name,
						cc1.counterparty_id,
						case when cc1.port_type in ('STS_load','STS_disport') then cc4.name  || '- [ST]'|| cc5.terminal_name else cc4.name end counterparty_name,
						cc8.terms,
						cc8.terms ||'-'|| cc11.name term_name,
						cc1.operator_user_id,
						cc7.firstname || ' ' || cc7.lastname operator_name,
						cc8.finance_user_id,
						cc9.firstname || ' ' || cc9.lastname financer_name,
						cc1.country_origin,
						cc6.name country_origin_name,
						cc1.product_id,
						cc2.product_name,
						cc1.grade_id,
						cc3.grade_name,		
						cc14.intended_vessel_id vessel,
						cc15.name vessel_name,
						cc1.load_port_id,
						cc1.discharge_port_id,		
						cc14.laytime as laytime,
						cc1.dates_based_on,
						cc14.date_range_from,
						cc14.date_range_to,
						cc1.transfer_date,
						cc13.units,
						cc16.name unit_name,		
						cc18.name credit_type,
						cc17.bank_id,
						cc19.bank_name bank,
						cc13.est_density ::numeric as est_density,			
            COALESCE(cc13.quantity,0)  ::varchar as max_quantity_mt,    
            COALESCE(cc13.quantity_cm,0) ::varchar as max_quantity_cm,    
						cc1.port_type,
						cc1.parcel_number,				
						cc1.lc_open_date,
						cc1.vat_type,
						cc1.transfer_state,
						cc1.outside_us_flag,
						cc1.bookout_flag,
						cc1.vat_country,
						cc1.tax_code,
						cc1.custom_cleared_us_flag,		
						cc1.no_agreed_supply_date_flag,		
						cc1.transfer_date_est_flag,
						cc1.border_crossing_date,
						cc1.border_crossing_date_est_flag,
						cc1.supply_start_date,
						cc1.supply_end_date,
						cc1.transfer_detail_date,
						cc1.api_60f,
						cc1.est_density_15c_air,
						cc1.est_density_15c_vac,
            COALESCE(cc1.shipping_quantity_mt,0) ::varchar as shipping_quantity_mt,
            COALESCE(cc1.shipping_quantity_cm,0) ::varchar as shipping_quantity_cm,
            COALESCE(cc1.shipping_quantity_b,0) ::varchar as shipping_quantity_b,
						cc1.est_quantity_flag,
						cc1.show_outturn_qty_flag,
						cc1.invoicing_unit,
						cc1.trade_code,
            COALESCE((cc20.allocated_qty_mt - cc1.shipping_quantity_mt),0) ::varchar as allocated_quantity_mt,
            COALESCE((cc20.allocated_qty_cm - cc1.shipping_quantity_cm),0) ::varchar as allocated_quantity_cm,
            COALESCE((COALESCE(cc13.quantity,0)-cc20.allocated_qty_mt + cc1.shipping_quantity_mt),0) ::varchar as available_quantity_mt,
            COALESCE((cc13.quantity_cm-cc20.allocated_qty_cm + cc1.shipping_quantity_cm),0) ::varchar as available_quantity_cm
        from voyage_parcels cc1
          inner join products cc2
            on cc1.product_id = cc2.product_id
            and cc2.status = 'A'
          inner join product_grades cc3
            on cc1.grade_id = cc3.grade_id
            and cc1.product_id = cc3.product_id
            and cc3.status = 'A'
          left join legal_entity cc4
            on cc1.counterparty_id = cc4.legal_entity_id
            and cc4.status = 'A'
          left join storage_terminal cc5
            on cc1.terminal_id = cc5.terminal_id
            and cc5.status = 'A'
          left join bs_lookups cc6
            on cc1.country_origin = cc6.lookup_code
            and cc6.lookup_type = 'Country_Origin'
            and cc6.status = 'A'
          left join bs_users cc7
            on cc1.operator_user_id = cc7.user_id		
            and cc7.status = 'A'	
          left join trades cc8
            on cc1.trade_id = cc8.trade_id
            and cc8.status = 'A'
          left join bs_users cc9
            on cc8.finance_user_id = cc9.user_id	
            and cc9.status = 'A'
          left join bs_lookups cc10
            on cc8.trade_type = cc10.lookup_code
            and cc10.lookup_type = 'Trade_type'
            and cc10.status = 'A'
          left join bs_lookups cc11
            on cc8.terms = cc11.lookup_code
            and cc11.lookup_type = 'Terms'
            and cc11.status = 'A'
        left join bs_organizations cc12
            on cc8.company_id = cc12.org_id
            and cc12.status = 'A'
          left join trade_products cc13
              on cc1.trade_id = cc13.trade_id
              and cc13.status = 'A'     
          left join trade_deliveries cc14
              on cc1.trade_id = cc14.trade_id
              and cc14.status = 'A'
          left join bs_lookups cc15
              on cc14.intended_vessel_id = cc15.lookup_code
              and cc15.lookup_type = 'Intended_Vessel'
              and cc15.status = 'A'
          left join bs_lookups cc16
              on cc13.units = cc16.lookup_code
              and cc16.lookup_type = 'Unit'
              and cc16.status = 'A'
          left join trade_payments cc17
              on cc1.trade_id = cc17.trade_id
              and cc17.status = 'A'
          left join bs_lookups cc18
              on cc17.credit_type = cc18.lookup_code
              and cc18.lookup_type = 'Credit_Type'
              and cc18.status = 'A'
          left join banks cc19
              on cc17.bank_id = cc19.bank_id
              and cc19.status = 'A'
          left join (
            select voyage_id,voyage_port_id,trade_id,sum(shipping_quantity_mt) allocated_qty_mt,sum(shipping_quantity_cm) allocated_qty_cm
                    from voyage_parcels 
                    where status = 'A'
                    group by voyage_id,voyage_port_id,trade_id
                    ) cc20
              on cc1.voyage_id = cc20.voyage_id
              and cc1.voyage_port_id = cc20.voyage_port_id
              and cc1.trade_id = cc20.trade_id
        where cc1.status = 'A'`;

    const arr = [];

    for (let x of voyages) {
      const item_documents = [];
      const item_ports = [];
      for (let z of ports) {
        const item_parcels = [];
        if (x.voyage_id === z.voyage_id) {
          for (let p of parcels) {
            if (z.voyage_port_id === p.voyage_port_id) {
              item_parcels.push({
                voyage_parcel_id: p.voyage_parcel_id,
                voyage_id: p.voyage_id,
                voyage_port_id: p.voyage_port_id,
                trade_id: p.trade_id,
                trade_type: p.trade_type,
                trade_type_name: p.trade_type_name,
                company_id: p.company_id,
                company_name: p.company_name,
                counterparty_id: p.counterparty_id,
                counterparty_name: p.counterparty_name,
                terms: p.terms,
                term_name: p.term_name,
                operator_user_id: p.operator_user_id,
                operator_name: p.operator_name,
                finance_user_id: p.finance_user_id,
                financer_name: p.financer_name,
                country_origin: p.country_origin,
                country_origin_name: p.country_origin_name,
                product_id: p.product_id,
                product_name: p.product_name,
                grade_id: p.grade_id,
                grade_name: p.grade_name,
                vessel: p.vessel,
                vessel_name: p.vessel_name,
                load_port_id: p.load_port_id,
                discharge_port_id: p.discharge_port_id,
                laytime: p.laytime,
                dates_based_on: p.dates_based_on,
                date_range_from: p.date_range_from,
                date_range_to: p.date_range_to,
                transfer_date: p.transfer_date,
                units: p.units,
                unit_name: p.unit_name,
                credit_type: p.credit_type,
                credit_type_name: p.credit_type_name,
                bank_id: p.bank_id,
                bank: p.bank,
                est_density: p.est_density,
                max_quantity_mt: p.max_quantity_mt,
                max_quantity_cm: p.max_quantity_cm,
                allocated_quantity_mt: p.allocated_quantity_mt,
                available_quantity_mt: p.available_quantity_mt,
                allocated_quantity_cm: p.allocated_quantity_cm,
                available_quantity_cm: p.available_quantity_cm,
                port_type: p.port_type,
                parcel_number: p.parcel_number,
                lc_open_date: p.lc_open_date,
                vat_type: p.vat_type,
                transfer_state: p.transfer_state,
                outside_us_flag: p.outside_us_flag,
                bookout_flag: p.bookout_flag,
                vat_country: p.vat_country,
                tax_code: p.tax_code,
                custom_cleared_us_flag: p.custom_cleared_us_flag,
                no_agreed_supply_date_flag: p.no_agreed_supply_date_flag,
                transfer_date_est_flag: p.transfer_date_est_flag,
                border_crossing_date: p.border_crossing_date,
                border_crossing_date_est_flag: p.border_crossing_date_est_flag,
                supply_start_date: p.supply_start_date,
                supply_end_date: p.supply_end_date,
                transfer_detail_date: p.transfer_detail_date,
                api_60f: p.api_60f,
                est_density_15c_air: p.est_density_15c_air,
                est_density_15c_vac: p.est_density_15c_vac,
                shipping_quantity_mt: p.shipping_quantity_mt,
                shipping_quantity_cm: p.shipping_quantity_cm,
                shipping_quantity_b: p.shipping_quantity_b,
                est_quantity_flag: p.est_quantity_flag,
                show_outturn_qty_flag: p.show_outturn_qty_flag,
                invoicing_unit: p.invoicing_unit,
                trade_code: p.trade_code,
              });
            }
          }
          item_ports.push({
            voyage_port_id: z.voyage_port_id,
            port_id: z.port_id,
            port_name: z.port_name,
            port_type: z.port_type,
            port_type_name: z.port_type_name,
            agent_id: z.agent_id,
            superveyor_id: z.superveyor_id,
            superintendent_id: z.superintendent_id,
            port_title: z.port_title,
            parcels: item_parcels,
          });
        }
      }

      for (let d of documents) {
        if (x.voyage_id === d.voyage_id) {
          item_documents.push({
            voyage_attachment_id: d.voyage_attachment_id,
            attachment_type: d.attachment_type,
            attachment_name: d.attachment_name,
            attachment_url: d.attachment_url,
            description: d.description,
          });
        }
      }

      arr.push({
        voyage_id: x.voyage_id,
        intended_vessel: x.intended_vessel,
        product_id: x.product_id,
        voyage_title: x.voyage_title,
        voyage_status: x.voyage_status,
        charter_type: x.charter_type,
        charter_party_date: x.charter_party_date,
        insurance_required_flag: x.insurance_required_flag,
        insurance_externally_flag: x.insurance_externally_flag,
        insured_description: x.insured_description,
        insurance_status: x.insurance_status,
        quantity_discrepancy_flag: x.quantity_discrepancy_flag,
        quantity_discrepancy_comment: x.quantity_discrepancy_comment,
        voyage_comment: x.voyage_comment,
        org_id: x.org_id,
        org_name: x.org_name,
        documents: item_documents,
        ports: item_ports,
      });
    }

    return tokenResponse(request, arr);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
