import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    select 
        aa1.voyage_port_id,        
        aa2.port_name        
    from voyage_ports aa1
      inner join ports aa2
        on aa1.port_id = aa2.port_id
        and aa2.status = 'A'
      where  aa1.voyage_id = ${+params.voyage_id}
        and aa1.port_type = ${params.port_type}
        and aa1.status = 'A'`;
    return tokenResponse(request, response);
  } catch (error) {
    return Response.json({ message: "Error", result: error }, { status: 500 });
  }
}
