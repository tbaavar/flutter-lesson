import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.voyage_ports.create({
      data: {
        voyage_id: +body.voyage_id,
        port_id: +body.port_id,
        port_type: body.port_type,
        agent_id: +body.agent_id,
        superveyor_id: +body.superveyor_id,
        superintendent_id: +body.superintendent_id,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    await prisma.voyage_ports.update({
      data: {
        voyage_id: +body.voyage_id,
        port_id: +body.port_id,
        port_type: body.port_type,
        agent_id: +body.agent_id,
        superveyor_id: +body.superveyor_id,
        superintendent_id: +body.superintendent_id,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_port_id: +body.voyage_port_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    const response = await prisma.$queryRaw`
    select voyage_parcel_id
               from voyage_parcels
               where voyage_port_id = ${+body.voyage_port_id}
                   and status = 'A'`;

    if (response.length > 0) {
      return Response.json(
        { message: "Error", result: "Parcel үүссэн байна устгах боломжгүй" },
        { status: 500 }
      );
    } else {
      await prisma.voyage_ports.update({
        data: {
          status: "I",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { voyage_port_id: +body.voyage_port_id, status: "A" },
      });
    }

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
