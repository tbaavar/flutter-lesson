import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {


    const response = await prisma.$queryRaw`
        select
          aa1.log_voyage_parcel_id,
          aa1.trade_id,
          aa1.voyage_id,
          aa6.name company_name,
          aa9.name counterparty,
          aa4.name port_type,
          aa3.port_name voyage_port,
          aa5.voyage_title,
          aa1.voyage_parcel_id,
          aa1.parcel_number,
          aa7.port_name load_port,
          aa8.port_name discharge_port,
          aa10.name dates_based_on,
          aa12.product_name,
          aa13.grade_name,
          aa1.transfer_date,
          case when aa1.transfer_date_est_flag='Y' then 'Checked' else 'UnChecked' end transfer_date_est_flag,
          aa1.border_crossing_date,
          case when aa1.border_crossing_date_est_flag='Y' then 'Checked' else 'UnChecked' end border_crossing_date_est_flag,
          aa1.supply_start_date,
          aa1.supply_end_date,
          case when aa1.no_agreed_supply_date_flag='Y' then 'Checked' else 'UnChecked' end no_agreed_supply_date_flag,
          aa1.transfer_detail_date,
          aa1.api_60f,
          aa1.est_density_15c_air,
          aa1.est_density_15c_vac,
          aa1.shipping_quantity_mt,
          aa1.shipping_quantity_cm,
          case when aa1.est_quantity_flag='Y' then 'Checked' else 'UnChecked' end est_quantity_flag,
          aa14.name invoicing_unit,
          substring(aa11.lastname,1,1) || '.' || aa11.firstname operator_name,
          aa1.latest_change_flag,
          aa1.status,
          aa1.created_by,
          aa1.creation_date,
          aa1.last_updated_by,
          aa1.last_update_date
        from log_voyage_parcels aa1
          left join voyage_ports aa2
            on aa1.voyage_port_id = aa2.voyage_port_id
            and aa2.status = 'A'
          left join ports aa3
            on aa2.port_id = aa3.port_id
            and aa3.status = 'A'
          left join bs_lookups aa4
            on aa2.port_type = aa4.lookup_code
            and aa4.lookup_type = 'Port_type'
            and aa4.status = 'A'
          left join voyages aa5
            on aa1.voyage_id = aa5.voyage_id
            and aa5.status = 'A'
          left join bs_organizations aa6
            on aa5.org_id = aa6.org_id
            and aa6.status = 'A'
          left join ports aa7
            on aa1.load_port_id = aa7.port_id
            and aa7.status = 'A'
          left join ports aa8
            on aa1.discharge_port_id = aa8.port_id
            and aa8.status = 'A'
          left join legal_entity aa9
            on aa1.counterparty_id = aa9.legal_entity_id
            and aa9.status = 'A'
          left join bs_lookups aa10
            on aa1.dates_based_on = aa10.lookup_code
            and aa10.lookup_type = 'Delivery_based_on'
            and aa10.status = 'A'
          left join bs_users aa11
            on aa1.operator_user_id = aa11.user_id		
            and aa11.status = 'A'
          left join products aa12
            on aa1.product_id = aa12.product_id
            and aa12.status = 'A'
          left join product_grades aa13
            on aa1.product_id = aa13.product_id
            and aa1.grade_id = aa13.grade_id
            and aa13.status = 'A'
          left join bs_lookups aa14
            on aa1.invoicing_unit = aa14.lookup_code
            and aa14.lookup_type = 'Unit'
            and aa14.status = 'A'
        order by aa1.trade_id asc,aa1.voyage_parcel_id asc,aa1.last_update_date desc`;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
