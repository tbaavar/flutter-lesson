import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
    select				
		cc1.voyage_parcel_id,
		cc1.voyage_id,
		cc1.voyage_port_id,
		cc1.trade_id,
		cc8.trade_type,
		cc10.name trade_type_name,		
		cc8.company_id,
		cc12.name company_name,
		cc1.counterparty_id,
		case when cc1.port_type in ('STS_load','STS_disport') then cc5.terminal_name else cc4.name end counterparty_name,
		cc8.terms,
		cc8.terms ||'-'|| cc11.name term_name,
		cc1.operator_user_id,
		cc7.firstname || ' ' || cc7.lastname operator_name,
		cc8.finance_user_id,
		cc9.firstname || ' ' || cc9.lastname financer_name,
		cc1.country_origin,
		cc6.name country_origin_name,
		cc1.product_id,
		cc2.product_name,
		cc1.grade_id,
		cc3.grade_name,		
		cc14.intended_vessel_id vessel,
	  	cc15.name vessel_name,
		cc1.load_port_id,
		cc1.discharge_port_id,		
	  	cc14.laytime as laytime,
		cc1.dates_based_on,
		cc14.date_range_from,
		cc14.date_range_to,
		cc1.transfer_date,
		cc13.units,
	  	cc16.name unit_name,		
		cc18.name credit_type,
	  	cc17.bank_id,
		cc19.bank_name bank,
		cc13.est_density,			
		cc13.quantity ::varchar as max_quantity_mt,    
		cc1.port_type,
		cc1.parcel_number,				
		cc1.lc_open_date,
		cc1.vat_type,
		cc1.transfer_state,
		cc1.outside_us_flag,
		cc1.bookout_flag,
		cc1.vat_country,
		cc1.tax_code,
		cc1.custom_cleared_us_flag,		
		cc1.no_agreed_supply_date_flag,		
		cc1.transfer_date_est_flag,
		cc1.border_crossing_date,
		cc1.border_crossing_date_est_flag,
		cc1.supply_start_date,
		cc1.supply_end_date,
		cc1.transfer_detail_date,
		cc1.api_60f,
		cc1.est_density_15c_air,
		cc1.est_density_15c_vac,
		cc1.shipping_quantity_mt ::varchar as shipping_quantity_mt,
		cc1.shipping_quantity_cm ::varchar as shipping_quantity_cm,
		cc1.shipping_quantity_b ::varchar as shipping_quantity_b,
		cc1.est_quantity_flag,
		cc1.show_outturn_qty_flag,
		cc1.invoicing_unit,
		cc1.trade_code,
        COALESCE((cc20.allocated_qty_mt - cc1.shipping_quantity_mt),0) ::varchar as allocated_quantity_mt,
		COALESCE((cc20.allocated_qty_cm - cc1.shipping_quantity_cm),0) ::varchar as allocated_quantity_cm,
		COALESCE((COALESCE(cc13.quantity,0)-cc20.allocated_qty_mt + cc1.shipping_quantity_mt),0) ::varchar as available_quantity_mt,
		COALESCE((cc13.quantity_cm-cc20.allocated_qty_cm + cc1.shipping_quantity_cm),0) ::varchar as available_quantity_cm,
        CURRENT_DATE -  cc1.transfer_date || ' day' latestday,
		cc22.port_name voyage_port_name,
		COALESCE(cc13.quantity_cm,0)::varchar as max_quantity_cm
from voyage_parcels cc1
    inner join voyages cc1_1
        on cc1.voyage_id = cc1_1.voyage_id
        and cc1_1.status = 'A' 
	inner join products cc2
		on cc1.product_id = cc2.product_id
		and cc2.status = 'A'
	inner join product_grades cc3
		on cc1.grade_id = cc3.grade_id
		and cc1.product_id = cc3.product_id
		and cc3.status = 'A'
	left join legal_entity cc4
		on cc1.counterparty_id = cc4.legal_entity_id
		and cc4.status = 'A'
	left join storage_terminal cc5
		on cc1.counterparty_id = cc5.terminal_id
		and cc5.status = 'A'
	left join bs_lookups cc6
		on cc1.country_origin = cc6.lookup_code
		and cc6.lookup_type = 'Country_Origin'
		and cc6.status = 'A'
	left join bs_users cc7
		on cc1.operator_user_id = cc7.user_id		
		and cc7.status = 'A'	
	left join trades cc8
		on cc1.trade_id = cc8.trade_id
		and cc8.status = 'A'
	left join bs_users cc9
		on cc8.finance_user_id = cc9.user_id	
		and cc9.status = 'A'
	left join bs_lookups cc10
		on cc8.trade_type = cc10.lookup_code
		and cc10.lookup_type = 'Trade_type'
		and cc10.status = 'A'
	left join bs_lookups cc11
		on cc8.terms = cc11.lookup_code
		and cc11.lookup_type = 'Terms'
		and cc11.status = 'A'
 	left join bs_organizations cc12
		on cc8.company_id = cc12.org_id
		and cc12.status = 'A'
	left join trade_products cc13
		on cc1.trade_id = cc13.trade_id
		and cc13.status = 'A'     
	left join trade_deliveries cc14
		on cc1.trade_id = cc14.trade_id
		and cc14.status = 'A'
	left join bs_lookups cc15
		on cc14.intended_vessel_id = cc15.lookup_code
		and cc15.lookup_type = 'Intended_Vessel'
		and cc15.status = 'A'
	left join bs_lookups cc16
		on cc13.units = cc16.lookup_code
		and cc16.lookup_type = 'Unit'
		and cc16.status = 'A'
	 left join trade_payments cc17
		on cc1.trade_id = cc17.trade_id
		and cc17.status = 'A'
	left join bs_lookups cc18
		on cc17.credit_type = cc18.lookup_code
		and cc18.lookup_type = 'Credit_Type'
		and cc18.status = 'A'
	left join banks cc19
		on cc17.bank_id = cc19.bank_id
		and cc19.status = 'A'
  	left join (
		select voyage_id,voyage_port_id,sum(shipping_quantity_mt) allocated_qty_mt,sum(shipping_quantity_cm) allocated_qty_cm
			from voyage_parcels 
			where status = 'A'
			group by voyage_id,voyage_port_id
				) cc20
		on cc1.voyage_id = cc20.voyage_id
		and cc1.voyage_port_id = cc20.voyage_port_id
	left join voyage_ports cc21
		on cc1.voyage_port_id = cc21.voyage_port_id
		and cc21.status = 'A'
	left join ports cc22
			on cc21.port_id = cc22.port_id
			and cc22.status = 'A'
where cc1.status = 'A'
        and cc1.port_type not in ('STS_load','STS_disport')
        and cc1_1.org_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
          `;
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
