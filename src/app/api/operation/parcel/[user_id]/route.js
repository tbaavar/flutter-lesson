import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
          select bb1.*
          from (            
                select                    
                      aa1.trade_id,
                      aa1.trade_type,
                      aa22.name trade_type_name,                      
											aa1.company_id,
                      aa17.name company_name,
                      aa1.counterparty_id,
                      aa18.name counterparty_name,
                      aa1.terms,		
                      aa1.terms ||'-'|| aa21.name term_name,
                      aa1.operator_user_id,
                      aa11.firstname || ' ' || aa11.lastname operator_name,		
                      aa1.finance_user_id,
                      aa12.firstname || ' ' || aa12.lastname financer_name,
											aa9.country_origin,
											aa23.name country_origin_name,
                      aa9.product_id,
                      aa14.product_name,
                      aa9.grade_id,
                      aa15.grade_name,		
                      aa9.quantity  ::varchar as quantity_mt,         
                      aa9.quantity_cm  ::varchar as quantity_cm,                 
											aa4.intended_vessel_id as vessel,
											aa24.name vessel_name,
                      aa4.load_port_id,
											aa4.discharge_port_id,
                      aa4.laytime as laytime,
                      aa4.dates_based_on,
                      aa4.date_range_from,
											aa4.date_range_to,
                      aa9.est_bl_date,
                      aa9.units,
                      aa25.name unit_name,
                      aa19.name credit_type,
											aa3.bank_id,
											aa20.bank_name bank,
                      aa9.est_density ::numeric as est_density
                  from trades aa1	                    
                    left join trade_payments aa3
                      on aa1.trade_id = aa3.trade_id
                      and aa3.status = 'A'	
                    left join trade_deliveries aa4
                      on aa1.trade_id = aa4.trade_id
                      and aa4.status = 'A'
                    left join trade_products aa9
                      on aa1.trade_id = aa9.trade_id
                      and aa9.status = 'A'                    
                    left join bs_users aa11
                      on aa1.operator_user_id = aa11.user_id		
                      and aa11.status = 'A'
                    left join bs_users aa12
                      on aa1.finance_user_id = aa12.user_id	
                      and aa12.status = 'A'                    
                    left join products aa14
                      on aa9.product_id = aa14.product_id
                      and aa14.status = 'A'
                    left join product_grades aa15
                      on aa9.grade_id = aa15.grade_id
                      and aa9.product_id = aa15.product_id
                      and aa15.status = 'A'		
                    left join bs_organizations aa17
                      on aa1.company_id = aa17.org_id
											and aa17.status = 'A'
                    left join legal_entity aa18
                      on aa1.counterparty_id = aa18.legal_entity_id
                      and aa18.status = 'A'                    
                    left join bs_lookups aa19
                      on aa3.credit_type = aa19.lookup_code
                      and aa19.lookup_type = 'Credit_Type'
											and aa19.status = 'A'
                    left join banks aa20
                      on aa3.bank_id = aa20.bank_id
                      and aa20.status = 'A'
                    left join bs_lookups aa21
                      on aa1.terms = aa21.lookup_code
                      and aa21.lookup_type = 'Terms'
											and aa21.status = 'A'
                    left join bs_lookups aa22
                      on aa1.trade_type = aa22.lookup_code
                      and aa22.lookup_type = 'Trade_type'
											and aa22.status = 'A'
										left join bs_lookups aa23
                      on aa9.country_origin = aa23.lookup_code
                      and aa23.lookup_type = 'Country_Origin'
											and aa23.status = 'A'
										left join bs_lookups aa24
                      on aa4.intended_vessel_id = aa24.lookup_code
                      and aa24.lookup_type = 'Intended_Vessel'
											and aa24.status = 'A'
                    left join bs_lookups aa25
                      on aa9.units = aa25.lookup_code
                      and aa25.lookup_type = 'Unit'
											and aa25.status = 'A'
                  where aa1.status = 'A' and aa9.ops_active_flag ='Y' and aa1.company_id in (select org_id from bs_org_users where user_id = ${+params.user_id})
                ) bb1
                left join voyage_parcels bb2
                  on bb1.trade_id = bb2.trade_id
                  and bb2.status = 'A'				
          where bb2.trade_id is null 
          `;
    return tokenResponse(request, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}