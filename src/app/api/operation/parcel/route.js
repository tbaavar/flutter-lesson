import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.$queryRaw`
          select bb1.*
          from (            
                select                    
                      aa1.trade_id,
                      aa1.trade_type,
                      aa22.name trade_type_name,                      
											aa1.company_id,
                      aa17.name company_name,
                      aa1.counterparty_id,
                      aa18.name counterparty_name,
                      aa1.terms,		
                      aa1.terms ||'-'|| aa21.name term_name,
                      aa1.operator_user_id,
                      aa11.firstname || ' ' || aa11.lastname operator_name,		
                      aa1.finance_user_id,
                      aa12.firstname || ' ' || aa12.lastname financer_name,
											aa9.country_origin,
											aa23.name country_origin_name,
                      aa9.product_id,
                      aa14.product_name,
                      aa9.grade_id,
                      aa15.grade_name,		
                      aa9.quantity ::int as quantity,                      
											aa4.intended_vessel_id as vessel,
											aa24.name vessel_name,
                      aa4.load_port_id,
											aa4.discharge_port_id,
                      aa4.laytime as laytime,
                      aa4.dates_based_on,
                      aa4.date_range_from,
											aa4.date_range_to,
                      aa9.est_bl_date,
                      aa9.units,
                      aa25.name unit_name,
                      aa19.name credit_type,
											aa3.bank_id,
											aa20.bank_name bank,
                      aa9.est_density ::numeric  as est_density
                  from trades aa1	                    
                    left join trade_payments aa3
                      on aa1.trade_id = aa3.trade_id
                      and aa3.status = 'A'	
                    left join trade_deliveries aa4
                      on aa1.trade_id = aa4.trade_id
                      and aa4.status = 'A'
                    left join trade_products aa9
                      on aa1.trade_id = aa9.trade_id
                      and aa9.status = 'A'                    
                    left join bs_users aa11
                      on aa1.operator_user_id = aa11.user_id		
                      and aa11.status = 'A'
                    left join bs_users aa12
                      on aa1.finance_user_id = aa12.user_id	
                      and aa12.status = 'A'                    
                    left join products aa14
                      on aa9.product_id = aa14.product_id
                      and aa14.status = 'A'
                    left join product_grades aa15
                      on aa9.grade_id = aa15.grade_id
                      and aa9.product_id = aa15.product_id
                      and aa15.status = 'A'		
                    left join bs_organizations aa17
                      on aa1.company_id = aa17.org_id
											and aa17.status = 'A'
                    left join legal_entity aa18
                      on aa1.counterparty_id = aa18.legal_entity_id
                      and aa18.status = 'A'                    
                    left join bs_lookups aa19
                      on aa3.credit_type = aa19.lookup_code
                      and aa19.lookup_type = 'Credit_Type'
											and aa19.status = 'A'
                    left join banks aa20
                      on aa3.bank_id = aa20.bank_id
                      and aa20.status = 'A'
                    left join bs_lookups aa21
                      on aa1.terms = aa21.lookup_code
                      and aa21.lookup_type = 'Terms'
											and aa21.status = 'A'
                    left join bs_lookups aa22
                      on aa1.trade_type = aa22.lookup_code
                      and aa22.lookup_type = 'Trade_type'
											and aa22.status = 'A'
										left join bs_lookups aa23
                      on aa9.country_origin = aa23.lookup_code
                      and aa23.lookup_type = 'Country_Origin'
											and aa23.status = 'A'
										left join bs_lookups aa24
                      on aa4.intended_vessel_id = aa24.lookup_code
                      and aa24.lookup_type = 'Intended_Vessel'
											and aa24.status = 'A'
                    left join bs_lookups aa25
                      on aa9.units = aa25.lookup_code
                      and aa25.lookup_type = 'Unit'
											and aa25.status = 'A'
                  where aa1.status = 'A' and aa9.ops_active_flag ='Y'
                ) bb1
                left join voyage_parcels bb2
                  on bb1.trade_id = bb2.trade_id
                  and bb2.status = 'A'				
          where bb2.trade_id is null
          `;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  if (body.trade_type === "Spot_Sale" || body.trade_type === "Term_Sale") {
    const purchase = await prisma.$queryRaw`
    select 
      aa1.trade_id,max(aa1.grade_id) grade_id
    from voyage_parcels aa1
      inner join trades aa2
        on aa1.trade_id = aa2.trade_id
        and aa2.status = 'A'    
    where aa2.trade_type in ('Term_Purchase','Spot_Purchase') 
          and voyage_id =  ${+body.voyage_id} and aa1.status = 'A'
    group by aa1.trade_id`;

    if (purchase.length === 0) {
      return Response.json(
        { message: "Error", result: "Purshase trade not found" },
        { status: 203 }
      );
    }
  }

  const oldValue = await prisma.$queryRaw`
        select 
          count(*) total,count(distinct case when aa1.grade_id = ${+body.grade_id}  then aa1.grade_id end) total_grade
        from voyage_parcels aa1
        inner join trades aa2
          on aa1.trade_id = aa2.trade_id
          and aa2.status = 'A'    
        where  voyage_id =  ${+body.voyage_id} 
        and aa1.status = 'A'`;

  if (oldValue.total !== 0 && oldValue.total_grade === 0) {
    return Response.json(
      { message: "Error", result: "Product not found" },
      { status: 203 }
    );
  }

  try {
    const old_value = await prisma.voyage_parcels.create({
      data: {
        trade_id: +body.trade_id,
        voyage_id: +body.voyage_id,
        parcel_number: body.parcel_number,
        country_origin: body.country_origin,
        load_port_id: +body.load_port_id,
        discharge_port_id: +body.discharge_port_id,
        lc_open_date: new Date(body.lc_open_date),
        vat_type: body.vat_type,
        transfer_state: body.transfer_state,
        outside_us_flag: body.outside_us_flag,
        bookout_flag: body.bookout_flag,
        vat_country: body.vat_country,
        tax_code: body.tax_code,
        custom_cleared_us_flag: body.custom_cleared_us_flag,
        dates_based_on: body.dates_based_on,
        no_agreed_supply_date_flag: body.no_agreed_supply_date_flag,
        transfer_date: new Date(body.transfer_date),
        transfer_date_est_flag: body.transfer_date_est_flag,
        border_crossing_date: new Date(body.border_crossing_date),
        border_crossing_date_est_flag: body.border_crossing_date_est_flag,
        supply_start_date: new Date(body.supply_start_date),
        supply_end_date: new Date(body.supply_end_date),
        transfer_detail_date: new Date(body.transfer_detail_date),
        api_60f: +body.api_60f,
        est_density_15c_air: +body.est_density_15c_air,
        est_density_15c_vac: +body.est_density_15c_vac,
        shipping_quantity_mt: +body.shipping_quantity_mt,
        shipping_quantity_cm: +body.shipping_quantity_cm,
        shipping_quantity_b: 0,
        est_quantity_flag: body.est_quantity_flag,
        show_outturn_qty_flag: body.show_outturn_qty_flag,
        invoicing_unit: body.invoicing_unit,
        operator_user_id: +body.operator_user_id,
        voyage_port_id: +body.voyage_port_id,
        port_type: body.port_type,
        product_id: body.product_id,
        grade_id: body.grade_id,
        trade_code: body.trade_code,
        counterparty_id: body.counterparty_id,
        org_id: +body.org_id,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });
    if (
      body.unallocated_quantity_mt > -1 ||
      body.unallocated_quantity_cm > -1
    ) {
      const isUnallocated = await prisma.voyage_unallocated_parcels.findFirst({
        where: {
          trade_id: +body.trade_id,
          voyage_id: +body.voyage_id,
          org_id: +body.org_id,
          status: "A",
        },
      });

      if (!isUnallocated) {
        await prisma.voyage_unallocated_parcels.create({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            org_id: +body.org_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        await prisma.voyage_unallocated_parcels.update({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            created_by: body.username,
            last_updated_by: body.username,
          },
          where: {
            unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
            status: "A",
          },
        });
      }
    }

    await prisma.log_voyage_parcels.create({
      data: {
        voyage_parcel_id: +old_value.voyage_parcel_id,
        org_id: +old_value.org_id,
        trade_id: +old_value.trade_id,
        voyage_id: +old_value.voyage_id,
        parcel_number: old_value.parcel_number,
        country_origin: old_value.country_origin,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        lc_open_date: new Date(old_value.lc_open_date),
        vat_type: old_value.vat_type,
        transfer_state: old_value.transfer_state,
        outside_us_flag: old_value.outside_us_flag,
        bookout_flag: old_value.bookout_flag,
        vat_country: old_value.vat_country,
        tax_code: old_value.tax_code,
        custom_cleared_us_flag: old_value.custom_cleared_us_flag,
        dates_based_on: old_value.dates_based_on,
        no_agreed_supply_date_flag: old_value.no_agreed_supply_date_flag,
        transfer_date: new Date(old_value.transfer_date),
        transfer_date_est_flag: old_value.transfer_date_est_flag,
        border_crossing_date: new Date(old_value.border_crossing_date),
        border_crossing_date_est_flag: old_value.border_crossing_date_est_flag,
        supply_start_date: new Date(old_value.supply_start_date),
        supply_end_date: new Date(old_value.supply_end_date),
        transfer_detail_date: new Date(old_value.transfer_detail_date),
        api_60f: +old_value.api_60f,
        est_density_15c_air: +old_value.est_density_15c_air,
        est_density_15c_vac: +old_value.est_density_15c_vac,
        shipping_quantity_mt: +old_value.shipping_quantity_mt,
        shipping_quantity_cm: +old_value.shipping_quantity_cm,
        shipping_quantity_b: +old_value.shipping_quantity_b,
        est_quantity_flag: old_value.est_quantity_flag,
        show_outturn_qty_flag: old_value.show_outturn_qty_flag,
        invoicing_unit: old_value.invoicing_unit,
        operator_user_id: +old_value.operator_user_id,
        voyage_port_id: +old_value.voyage_port_id,
        port_type: old_value.port_type,
        product_id: old_value.product_id,
        grade_id: old_value.grade_id,
        trade_code: old_value.trade_code,
        counterparty_id: old_value.counterparty_id,
        org_id: +old_value.org_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();

  try {
    const old_value = await prisma.voyage_parcels.findFirst({
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    const old_log = await prisma.log_voyage_parcels.findFirst({
      where: {
        voyage_parcel_id: +body.voyage_parcel_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.voyage_parcels.update({
      data: {
        operator_user_id: +body.operator_user_id,
        no_agreed_supply_date_flag: body.no_agreed_supply_date_flag,
        transfer_date: new Date(body.transfer_date),
        transfer_date_est_flag: body.transfer_date_est_flag,
        border_crossing_date: new Date(body.border_crossing_date),
        border_crossing_date_est_flag: body.border_crossing_date_est_flag,
        supply_start_date: new Date(body.supply_start_date),
        supply_end_date: new Date(body.supply_end_date),
        transfer_detail_date: new Date(body.transfer_detail_date),
        api_60f: +body.api_60f,
        est_density_15c_air: +body.est_density_15c_air,
        est_density_15c_vac: +body.est_density_15c_vac,
        shipping_quantity_mt: +body.shipping_quantity_mt,
        shipping_quantity_cm: +body.shipping_quantity_cm,
        shipping_quantity_b: 0,
        est_quantity_flag: body.est_quantity_flag,
        invoicing_unit: body.invoicing_unit,
        voyage_port_id: +body.voyage_port_id,
        port_type: body.port_type,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    if (
      body.unallocated_quantity_mt > -1 &&
      body.unallocated_quantity_cm > -1
    ) {
      const isUnallocated = await prisma.voyage_unallocated_parcels.findFirst({
        where: {
          trade_id: +body.trade_id,
          voyage_id: +body.voyage_id,
          org_id: +body.org_id,
          status: "A",
        },
      });

      if (!isUnallocated) {
        await prisma.voyage_unallocated_parcels.create({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            org_id: +body.org_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        await prisma.voyage_unallocated_parcels.update({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            created_by: body.username,
            last_updated_by: body.username,
          },
          where: {
            unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
            status: "A",
          },
        });
      }
    }

    if (old_log) {
      await prisma.log_voyage_parcels.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_voyage_parcel_id: +old_log.log_voyage_parcel_id },
      });
    }

    await prisma.log_voyage_parcels.create({
      data: {
        voyage_parcel_id: +old_value.voyage_parcel_id,
        trade_id: +old_value.trade_id,
        voyage_id: +old_value.voyage_id,
        parcel_number: old_value.parcel_number,
        country_origin: old_value.country_origin,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        lc_open_date: new Date(old_value.lc_open_date),
        vat_type: old_value.vat_type,
        transfer_state: old_value.transfer_state,
        outside_us_flag: old_value.outside_us_flag,
        bookout_flag: old_value.bookout_flag,
        vat_country: old_value.vat_country,
        tax_code: old_value.tax_code,
        custom_cleared_us_flag: old_value.custom_cleared_us_flag,
        dates_based_on: old_value.dates_based_on,
        no_agreed_supply_date_flag: old_value.no_agreed_supply_date_flag,
        transfer_date: new Date(old_value.transfer_date),
        transfer_date_est_flag: old_value.transfer_date_est_flag,
        border_crossing_date: new Date(old_value.border_crossing_date),
        border_crossing_date_est_flag: old_value.border_crossing_date_est_flag,
        supply_start_date: new Date(old_value.supply_start_date),
        supply_end_date: new Date(old_value.supply_end_date),
        transfer_detail_date: new Date(old_value.transfer_detail_date),
        api_60f: +old_value.api_60f,
        est_density_15c_air: +old_value.est_density_15c_air,
        est_density_15c_vac: +old_value.est_density_15c_vac,
        shipping_quantity_mt: +old_value.shipping_quantity_mt,
        shipping_quantity_cm: +old_value.shipping_quantity_cm,
        shipping_quantity_b: +old_value.shipping_quantity_b,
        est_quantity_flag: old_value.est_quantity_flag,
        show_outturn_qty_flag: old_value.show_outturn_qty_flag,
        invoicing_unit: old_value.invoicing_unit,
        operator_user_id: +old_value.operator_user_id,
        voyage_port_id: +old_value.voyage_port_id,
        port_type: old_value.port_type,
        product_id: old_value.product_id,
        grade_id: old_value.grade_id,
        trade_code: old_value.trade_code,
        counterparty_id: old_value.counterparty_id,
        org_id: +old_value.org_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    const old_value = await prisma.voyage_parcels.findFirst({
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    const old_log = await prisma.log_voyage_parcels.findFirst({
      where: {
        voyage_parcel_id: +body.voyage_parcel_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    await prisma.voyage_parcels.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    if (
      body.unallocated_quantity_mt > -1 &&
      body.unallocated_quantity_cm > -1
    ) {
      const isUnallocated = await prisma.voyage_unallocated_parcels.findFirst({
        where: {
          trade_id: +body.trade_id,
          voyage_id: +body.voyage_id,
          org_id: +body.org_id,
          status: "A",
        },
      });

      if (!isUnallocated) {
        await prisma.voyage_unallocated_parcels.create({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            org_id: +body.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        const parcel = await prisma.voyage_parcels.findMany({
          where: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            org_id: +body.org_id,
            status: "A",
          },
        });

        if (parcel.length === 0) {
          await prisma.voyage_unallocated_parcels.update({
            data: {
              status: "I",
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: {
              unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
              status: "A",
            },
          });
        } else {
          const latestqty_mt =
            isUnallocated.unallocated_quantity_mt +
            body.unallocated_quantity_mt;

          const latestqty_cm =
            isUnallocated.unallocated_parcel_quantity_cm +
            body.unallocated_quantity_cm;

          await prisma.voyage_unallocated_parcels.update({
            data: {
              trade_id: +body.trade_id,
              voyage_id: +body.voyage_id,
              unallocated_parcel_quantity_cm: latestqty_cm,
              unallocated_quantity_mt: latestqty_mt,
              unallocated_parcel_quantity_b: 0,
              last_update_date: new Date(),
              last_updated_by: body.username,
            },
            where: {
              unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
              status: "A",
            },
          });
        }
      }
    }

    if (old_log) {
      await prisma.log_voyage_parcels.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_voyage_parcel_id: +old_log.log_voyage_parcel_id },
      });
    }

    await prisma.log_voyage_parcels.create({
      data: {
        voyage_parcel_id: +old_value.voyage_parcel_id,
        trade_id: +old_value.trade_id,
        org_id: +old_value.org_id,
        voyage_id: +old_value.voyage_id,
        parcel_number: old_value.parcel_number,
        country_origin: old_value.country_origin,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        lc_open_date: new Date(old_value.lc_open_date),
        vat_type: old_value.vat_type,
        transfer_state: old_value.transfer_state,
        outside_us_flag: old_value.outside_us_flag,
        bookout_flag: old_value.bookout_flag,
        vat_country: old_value.vat_country,
        tax_code: old_value.tax_code,
        custom_cleared_us_flag: old_value.custom_cleared_us_flag,
        dates_based_on: old_value.dates_based_on,
        no_agreed_supply_date_flag: old_value.no_agreed_supply_date_flag,
        transfer_date: new Date(old_value.transfer_date),
        transfer_date_est_flag: old_value.transfer_date_est_flag,
        border_crossing_date: new Date(old_value.border_crossing_date),
        border_crossing_date_est_flag: old_value.border_crossing_date_est_flag,
        supply_start_date: new Date(old_value.supply_start_date),
        supply_end_date: new Date(old_value.supply_end_date),
        transfer_detail_date: new Date(old_value.transfer_detail_date),
        api_60f: +old_value.api_60f,
        est_density_15c_air: +old_value.est_density_15c_air,
        est_density_15c_vac: +old_value.est_density_15c_vac,
        shipping_quantity_mt: +old_value.shipping_quantity_mt,
        shipping_quantity_cm: +old_value.shipping_quantity_cm,
        shipping_quantity_b: +old_value.shipping_quantity_b,
        est_quantity_flag: old_value.est_quantity_flag,
        show_outturn_qty_flag: old_value.show_outturn_qty_flag,
        invoicing_unit: old_value.invoicing_unit,
        operator_user_id: +old_value.operator_user_id,
        voyage_port_id: +old_value.voyage_port_id,
        port_type: old_value.port_type,
        product_id: old_value.product_id,
        grade_id: old_value.grade_id,
        trade_code: old_value.trade_code,
        counterparty_id: old_value.counterparty_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    await prisma.$executeRaw`UPDATE invoices SET status='I',last_update_date=${new Date()},last_updated_by=${
      body.username
    }  WHERE voyage_parcel_id = ${+body.voyage_parcel_id}  and status = 'A'`;

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
