import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function GET() {
  try {
    const response = await prisma.$queryRaw`
            select 
                aa1.voyage_parcel_id,                
                aa1.trade_id,
                aa2.unallocated_parcel_id,
                aa2.unallocated_quantity_mt,
                aa2.unallocated_parcel_quantity_b,
                aa2.unallocated_parcel_quantity_cm
            from voyage_parcels aa1
              inner join voyage_unallocated_parcels aa2
                on aa1.voyage_parcel_id = aa2.voyage_parcel_id
                and aa2.status = 'A'
              inner join trades aa3
                on aa1.trade_id = aa3.trade_id
                and aa3.status = 'A'
            where aa1.status = 'A'
    `;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function POST(request) {
  const body = await request.json();

  try {
    await prisma.voyage_unallocated_parcels.create({
      data: {
        voyage_parcel_id: +body.voyage_parcel_id,
        unallocated_parcel_quantity_cm: +body.unallocated_parcel_quantity_cm,
        unallocated_quantity_mt: +body.unallocated_quantity_mt,
        unallocated_parcel_quantity_b: +body.unallocated_parcel_quantity_b,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });

    await prisma.voyage_parcels.update({
      data: {
        voyage_parcel_id: +body.voyage_parcel_id,
        split_parcel_quantity_cm: +body.unallocated_parcel_quantity_cm,
        split_parcel_quantity_mt: +body.unallocated_quantity_mt,
        split_parcel_quantity_b: +body.unallocated_parcel_quantity_b,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
