import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export async function POST(request) {
  const body = await request.json();
  let ports = null;

  try {
    const isPort = await prisma.voyage_ports.findFirst({
      where: {
        port_type: body.port_type,
        voyage_id: +body.voyage_id,
        status: "A",
      },
    });

    if (!isPort) {
      ports = await prisma.voyage_ports.create({
        data: {
          voyage_id: +body.voyage_id,
          port_id: 1,
          port_type: body.port_type,
          agent_id: null,
          superveyor_id: +body.superveyor_id,
          superintendent_id: +body.superintendent_id,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });
    } else {
      ports = isPort;
    }

    const old_value = await prisma.voyage_parcels.create({
      data: {
        trade_id: +body.trade_id,
        voyage_id: +body.voyage_id,
        parcel_number: body.parcel_number,
        country_origin: body.country_origin,
        load_port_id: +body.load_port_id,
        discharge_port_id: +body.discharge_port_id,
        lc_open_date: new Date(body.lc_open_date),
        vat_type: body.vat_type,
        transfer_state: body.transfer_state,
        outside_us_flag: body.outside_us_flag,
        bookout_flag: body.bookout_flag,
        vat_country: body.vat_country,
        tax_code: body.tax_code,
        custom_cleared_us_flag: body.custom_cleared_us_flag,
        dates_based_on: body.dates_based_on,
        no_agreed_supply_date_flag: body.no_agreed_supply_date_flag,
        transfer_date: new Date(body.transfer_date),
        transfer_date_est_flag: body.transfer_date_est_flag,
        border_crossing_date: new Date(body.border_crossing_date),
        border_crossing_date_est_flag: body.border_crossing_date_est_flag,
        supply_start_date: new Date(body.supply_start_date),
        supply_end_date: new Date(body.supply_end_date),
        transfer_detail_date: new Date(body.transfer_detail_date),
        api_60f: +body.api_60f,
        est_density_15c_air: +body.est_density_15c_air,
        est_density_15c_vac: +body.est_density_15c_vac,
        shipping_quantity_mt: +body.shipping_quantity_mt,
        shipping_quantity_cm: +body.shipping_quantity_cm,
        shipping_quantity_b: 0,
        est_quantity_flag: body.est_quantity_flag,
        show_outturn_qty_flag: body.show_outturn_qty_flag,
        invoicing_unit: body.invoicing_unit,
        operator_user_id: +body.operator_user_id,
        voyage_port_id: +ports.voyage_port_id,
        port_type: body.port_type,
        product_id: body.product_id,
        grade_id: body.grade_id,
        trade_code: body.trade_code,
        counterparty_id: body.counterparty_id,
        org_id: +body.org_id,
        created_by: body.username,
        last_updated_by: body.username,
      },
    });
    if (
      body.unallocated_quantity_mt > -1 ||
      body.unallocated_quantity_cm > -1
    ) {
      const isUnallocated = await prisma.voyage_unallocated_parcels.findFirst({
        where: {
          trade_id: +body.trade_id,
          voyage_id: +body.voyage_id,
          org_id: +body.org_id,
          status: "A",
        },
      });

      if (!isUnallocated) {
        await prisma.voyage_unallocated_parcels.create({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            org_id: +body.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        await prisma.voyage_unallocated_parcels.update({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            created_by: body.username,
            last_updated_by: body.username,
          },
          where: {
            unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
            status: "A",
          },
        });
      }
    }

    await prisma.log_voyage_parcels.create({
      data: {
        voyage_parcel_id: +old_value.voyage_parcel_id,
        trade_id: +old_value.trade_id,
        voyage_id: +old_value.voyage_id,
        parcel_number: old_value.parcel_number,
        country_origin: old_value.country_origin,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        lc_open_date: new Date(old_value.lc_open_date),
        vat_type: old_value.vat_type,
        transfer_state: old_value.transfer_state,
        outside_us_flag: old_value.outside_us_flag,
        bookout_flag: old_value.bookout_flag,
        vat_country: old_value.vat_country,
        tax_code: old_value.tax_code,
        custom_cleared_us_flag: old_value.custom_cleared_us_flag,
        dates_based_on: old_value.dates_based_on,
        no_agreed_supply_date_flag: old_value.no_agreed_supply_date_flag,
        transfer_date: new Date(old_value.transfer_date),
        transfer_date_est_flag: old_value.transfer_date_est_flag,
        border_crossing_date: new Date(old_value.border_crossing_date),
        border_crossing_date_est_flag: old_value.border_crossing_date_est_flag,
        supply_start_date: new Date(old_value.supply_start_date),
        supply_end_date: new Date(old_value.supply_end_date),
        transfer_detail_date: new Date(old_value.transfer_detail_date),
        api_60f: +old_value.api_60f,
        est_density_15c_air: +old_value.est_density_15c_air,
        est_density_15c_vac: +old_value.est_density_15c_vac,
        shipping_quantity_mt: +old_value.shipping_quantity_mt,
        shipping_quantity_cm: +old_value.shipping_quantity_cm,
        shipping_quantity_b: +old_value.shipping_quantity_b,
        est_quantity_flag: old_value.est_quantity_flag,
        show_outturn_qty_flag: old_value.show_outturn_qty_flag,
        invoicing_unit: old_value.invoicing_unit,
        operator_user_id: +old_value.operator_user_id,
        voyage_port_id: +old_value.voyage_port_id,
        port_type: old_value.port_type,
        product_id: old_value.product_id,
        grade_id: old_value.grade_id,
        trade_code: old_value.trade_code,
        counterparty_id: old_value.counterparty_id,
        org_id: +old_value.org_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function PUT(request) {
  const body = await request.json();

  let ports = null;

  try {
    const old_value = await prisma.voyage_parcels.findFirst({
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    const old_log = await prisma.log_voyage_parcels.findFirst({
      where: {
        voyage_parcel_id: +body.voyage_parcel_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });

    const isPort = await prisma.voyage_ports.findFirst({
      where: {
        port_type: body.port_type,
        voyage_id: +body.voyage_id,
        status: "A",
      },
    });

    if (!isPort) {
      ports = await prisma.voyage_ports.create({
        data: {
          voyage_id: +body.voyage_id,
          port_id: 1,
          port_type: body.port_type,
          agent_id: null,
          superveyor_id: +body.superveyor_id,
          superintendent_id: +body.superintendent_id,
          created_by: body.username,
          last_updated_by: body.username,
        },
      });
    } else {
      ports = isPort;
    }

    await prisma.voyage_parcels.update({
      data: {
        operator_user_id: +body.operator_user_id,
        no_agreed_supply_date_flag: body.no_agreed_supply_date_flag,
        transfer_date: new Date(body.transfer_date),
        transfer_date_est_flag: body.transfer_date_est_flag,
        border_crossing_date: new Date(body.border_crossing_date),
        border_crossing_date_est_flag: body.border_crossing_date_est_flag,
        supply_start_date: new Date(body.supply_start_date),
        supply_end_date: new Date(body.supply_end_date),
        transfer_detail_date: new Date(body.transfer_detail_date),
        api_60f: +body.api_60f,
        est_density_15c_air: +body.est_density_15c_air,
        est_density_15c_vac: +body.est_density_15c_vac,
        shipping_quantity_mt: +body.shipping_quantity_mt,
        shipping_quantity_cm: +body.shipping_quantity_cm,
        shipping_quantity_b: 0,
        est_quantity_flag: body.est_quantity_flag,
        invoicing_unit: body.invoicing_unit,
        voyage_port_id: +ports.voyage_port_id,
        port_type: body.port_type,
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    if (
      body.unallocated_quantity_mt > -1 ||
      body.unallocated_quantity_cm > -1
    ) {
      const isUnallocated = await prisma.voyage_unallocated_parcels.findFirst({
        where: {
          trade_id: +body.trade_id,
          voyage_id: +body.voyage_id,
          org_id: +body.org_id,
          status: "A",
        },
      });

      if (!isUnallocated) {
        await prisma.voyage_unallocated_parcels.create({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            org_id: +body.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        await prisma.voyage_unallocated_parcels.update({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            created_by: body.username,
            last_updated_by: body.username,
          },
          where: {
            unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
            status: "A",
          },
        });
      }
    }

    if (old_log) {
      await prisma.log_voyage_parcels.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_voyage_parcel_id: +old_log.log_voyage_parcel_id },
      });
    }

    await prisma.log_voyage_parcels.create({
      data: {
        voyage_parcel_id: +old_value.voyage_parcel_id,
        trade_id: +old_value.trade_id,
        voyage_id: +old_value.voyage_id,
        parcel_number: old_value.parcel_number,
        country_origin: old_value.country_origin,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        lc_open_date: new Date(old_value.lc_open_date),
        vat_type: old_value.vat_type,
        transfer_state: old_value.transfer_state,
        outside_us_flag: old_value.outside_us_flag,
        bookout_flag: old_value.bookout_flag,
        vat_country: old_value.vat_country,
        tax_code: old_value.tax_code,
        custom_cleared_us_flag: old_value.custom_cleared_us_flag,
        dates_based_on: old_value.dates_based_on,
        no_agreed_supply_date_flag: old_value.no_agreed_supply_date_flag,
        transfer_date: new Date(old_value.transfer_date),
        transfer_date_est_flag: old_value.transfer_date_est_flag,
        border_crossing_date: new Date(old_value.border_crossing_date),
        border_crossing_date_est_flag: old_value.border_crossing_date_est_flag,
        supply_start_date: new Date(old_value.supply_start_date),
        supply_end_date: new Date(old_value.supply_end_date),
        transfer_detail_date: new Date(old_value.transfer_detail_date),
        api_60f: +old_value.api_60f,
        est_density_15c_air: +old_value.est_density_15c_air,
        est_density_15c_vac: +old_value.est_density_15c_vac,
        shipping_quantity_mt: +old_value.shipping_quantity_mt,
        shipping_quantity_cm: +old_value.shipping_quantity_cm,
        shipping_quantity_b: +old_value.shipping_quantity_b,
        est_quantity_flag: old_value.est_quantity_flag,
        show_outturn_qty_flag: old_value.show_outturn_qty_flag,
        invoicing_unit: old_value.invoicing_unit,
        operator_user_id: +old_value.operator_user_id,
        voyage_port_id: +old_value.voyage_port_id,
        port_type: old_value.port_type,
        product_id: old_value.product_id,
        grade_id: old_value.grade_id,
        trade_code: old_value.trade_code,
        counterparty_id: old_value.counterparty_id,
        org_id: +old_value.org_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}

export async function DELETE(request) {
  const body = await request.json();

  try {
    const old_value = await prisma.voyage_parcels.findFirst({
      where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
    });

    const old_log = await prisma.log_voyage_parcels.findFirst({
      where: {
        voyage_parcel_id: +body.voyage_parcel_id,
        latest_change_flag: "Y",
        status: "A",
      },
    });
    if (old_value) {
      await prisma.voyage_parcels.update({
        data: {
          status: "I",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { voyage_parcel_id: +body.voyage_parcel_id, status: "A" },
      });
    }

    await prisma.voyage_ports.update({
      data: {
        status: "I",
        last_update_date: new Date(),
        last_updated_by: body.username,
      },
      where: { voyage_port_id: +body.voyage_port_id, status: "A" },
    });

    if (
      body.unallocated_quantity_mt > -1 ||
      body.unallocated_quantity_cm > -1
    ) {
      const isUnallocated = await prisma.voyage_unallocated_parcels.findFirst({
        where: {
          trade_id: +body.trade_id,
          voyage_id: +body.voyage_id,
          org_id: +body.org_id,
          status: "A",
        },
      });

      if (!isUnallocated) {
        await prisma.voyage_unallocated_parcels.create({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: +body.unallocated_quantity_cm,
            unallocated_quantity_mt: +body.unallocated_quantity_mt,
            unallocated_parcel_quantity_b: 0,
            org_id: +body.org_id,
            created_by: body.username,
            last_updated_by: body.username,
          },
        });
      } else {
        const latestqty_mt =
          isUnallocated.unallocated_quantity_mt + body.unallocated_quantity_mt;

        const latestqty_cm =
          isUnallocated.unallocated_parcel_quantity_cm +
          body.unallocated_quantity_cm;

        await prisma.voyage_unallocated_parcels.update({
          data: {
            trade_id: +body.trade_id,
            voyage_id: +body.voyage_id,
            unallocated_parcel_quantity_cm: latestqty_cm,
            unallocated_quantity_mt: latestqty_mt,
            unallocated_parcel_quantity_b: 0,
            created_by: body.username,
            last_updated_by: body.username,
          },
          where: {
            unallocated_parcel_id: isUnallocated.unallocated_parcel_id,
            status: "A",
          },
        });
      }
    }

    if (old_log) {
      await prisma.log_voyage_parcels.update({
        data: {
          latest_change_flag: "N",
          last_update_date: new Date(),
          last_updated_by: body.username,
        },
        where: { log_voyage_parcel_id: +old_log.log_voyage_parcel_id },
      });
    }

    await prisma.log_voyage_parcels.create({
      data: {
        voyage_parcel_id: +old_value.voyage_parcel_id,
        trade_id: +old_value.trade_id,
        voyage_id: +old_value.voyage_id,
        parcel_number: old_value.parcel_number,
        country_origin: old_value.country_origin,
        load_port_id: +old_value.load_port_id,
        discharge_port_id: +old_value.discharge_port_id,
        lc_open_date: new Date(old_value.lc_open_date),
        vat_type: old_value.vat_type,
        transfer_state: old_value.transfer_state,
        outside_us_flag: old_value.outside_us_flag,
        bookout_flag: old_value.bookout_flag,
        vat_country: old_value.vat_country,
        tax_code: old_value.tax_code,
        custom_cleared_us_flag: old_value.custom_cleared_us_flag,
        dates_based_on: old_value.dates_based_on,
        no_agreed_supply_date_flag: old_value.no_agreed_supply_date_flag,
        transfer_date: new Date(old_value.transfer_date),
        transfer_date_est_flag: old_value.transfer_date_est_flag,
        border_crossing_date: new Date(old_value.border_crossing_date),
        border_crossing_date_est_flag: old_value.border_crossing_date_est_flag,
        supply_start_date: new Date(old_value.supply_start_date),
        supply_end_date: new Date(old_value.supply_end_date),
        transfer_detail_date: new Date(old_value.transfer_detail_date),
        api_60f: +old_value.api_60f,
        est_density_15c_air: +old_value.est_density_15c_air,
        est_density_15c_vac: +old_value.est_density_15c_vac,
        shipping_quantity_mt: +old_value.shipping_quantity_mt,
        shipping_quantity_cm: +old_value.shipping_quantity_cm,
        shipping_quantity_b: +old_value.shipping_quantity_b,
        est_quantity_flag: old_value.est_quantity_flag,
        show_outturn_qty_flag: old_value.show_outturn_qty_flag,
        invoicing_unit: old_value.invoicing_unit,
        operator_user_id: +old_value.operator_user_id,
        voyage_port_id: +old_value.voyage_port_id,
        port_type: old_value.port_type,
        product_id: old_value.product_id,
        grade_id: old_value.grade_id,
        trade_code: old_value.trade_code,
        counterparty_id: old_value.counterparty_id,
        org_id: +old_value.org_id,
        created_by: old_value.created_by,
        creation_date: old_value.creation_date,
        last_updated_by: old_value.last_updated_by,
        last_update_date: old_value.last_update_date,
      },
    });

    return tokenResponse(request);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
