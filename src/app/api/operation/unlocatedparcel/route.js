import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;
export async function GET() {
  try {
    const response = await prisma.$queryRaw`
                    select  
                      aa26.voyage_id,                                        
                      aa1.trade_id,
                      aa1.trade_type,
                      aa22.name trade_type_name,
                      aa1.company_id,
                      aa17.name company_name,
                      aa1.counterparty_id,
                      aa18.name counterparty_name,
                      aa1.terms,		
                      aa1.terms ||'-'|| aa21.name term_name,
                      aa1.operator_user_id,
                      aa11.firstname || ' ' || aa11.lastname operator_name,		
                      aa1.finance_user_id,
                      aa12.firstname || ' ' || aa12.lastname financer_name,
                      aa9.country_origin,
                      aa23.name country_origin_name,
                      aa9.product_id,
                      aa14.product_name,
                      aa9.grade_id,
                      aa15.grade_name,					
                      aa4.intended_vessel_id as vessel,
                      aa24.name vessel_name,
                      aa4.load_port_id,
                      aa4.discharge_port_id,
                      aa4.laytime as laytime,
                      aa4.dates_based_on,
                      aa4.date_range_from,
                      aa4.date_range_to,
                      aa9.est_bl_date,
                      aa9.units,
                      aa25.name unit_name,
                      aa19.name credit_type,
                      aa3.bank_id,
                      aa20.bank_name bank,                      
                      aa9.est_density ::float4  as est_density,
                      aa9.quantity max_quantity,
                      aa26.allocated_quantity,
                      aa27.unallocated_quantity_mt as available_quantity
                  from trades aa1	                    
                    left join trade_payments aa3
                      on aa1.trade_id = aa3.trade_id
                      and aa3.status = 'A'	
                    left join trade_deliveries aa4
                      on aa1.trade_id = aa4.trade_id
                      and aa4.status = 'A'
                    left join trade_products aa9
                      on aa1.trade_id = aa9.trade_id
                      and aa9.status = 'A'                    
                    left join bs_users aa11
                      on aa1.operator_user_id = aa11.user_id		
                      and aa11.status = 'A'
                    left join bs_users aa12
                      on aa1.finance_user_id = aa12.user_id	
                      and aa12.status = 'A'                    
                    left join products aa14
                      on aa9.product_id = aa14.product_id
                      and aa14.status = 'A'
                    left join product_grades aa15
                      on aa9.grade_id = aa15.grade_id
                      and aa9.product_id = aa15.product_id
                      and aa15.status = 'A'		
                    left join bs_organizations aa17
                      on aa1.company_id = aa17.org_id
                      and aa17.status = 'A'
                    left join legal_entity aa18
                      on aa1.counterparty_id = aa18.legal_entity_id
                      and aa18.status = 'A'                    
                    left join bs_lookups aa19
                      on aa3.credit_type = aa19.lookup_code
                      and aa19.lookup_type = 'Credit_Type'
                      and aa19.status = 'A'
                    left join banks aa20
                      on aa3.bank_id = aa20.bank_id
                      and aa20.status = 'A'
                    left join bs_lookups aa21
                      on aa1.terms = aa21.lookup_code
                      and aa21.lookup_type = 'Terms'
                      and aa21.status = 'A'
                    left join bs_lookups aa22
                      on aa1.trade_type = aa22.lookup_code
                      and aa22.lookup_type = 'Trade_type'
                      and aa22.status = 'A'
                    left join bs_lookups aa23
                      on aa9.country_origin = aa23.lookup_code
                      and aa23.lookup_type = 'Country_Origin'
                      and aa23.status = 'A'
                    left join bs_lookups aa24
                      on aa4.intended_vessel_id = aa24.lookup_code
                      and aa24.lookup_type = 'Intended_Vessel'
                      and aa24.status = 'A'
                    left join bs_lookups aa25
                      on aa9.units = aa25.lookup_code
                      and aa25.lookup_type = 'Unit'
                      and aa25.status = 'A'
                    inner join (
                                select voyage_id,trade_id,sum(shipping_quantity_mt) allocated_quantity
                                from voyage_parcels
                                where status = 'A'
                                group by voyage_id,trade_id
                                ) aa26
                      on aa1.trade_id = aa26.trade_id			
                    inner join voyage_unallocated_parcels aa27
                      on aa26.voyage_id = aa27.voyage_id
                      and aa26.trade_id = aa27.trade_id
                      and aa27.status = 'A'
                  where aa27.unallocated_quantity_mt>0 and aa1.status = 'A'
          `;
    return tokenResponse(null, response);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
