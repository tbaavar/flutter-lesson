import prisma from "@/lib/prisma";
import { tokenResponse } from "@/lib/response";

export const revalidate = 0;

export async function GET(request, { params }) {
  try {
    const response = await prisma.$queryRaw`
                    select count(*) ::int as total_invoice
                    from invoices
                    where voyage_parcel_id = ${+params.voyage_parcel_id}
                        and status = 'A'`;



    return tokenResponse(request, response[0]);
  } catch (error) {
    console.log(error);
    return Response.json(
      { message: "Error", result: error.error_code },
      { status: 500 }
    );
  }
}
