import { create } from "zustand";
import { devtools } from "zustand/middleware"

let store = (set) => ({
  menu: 1,
  setMenu: (menu) => set((state) => ({ ...state, menu }))
})

store = devtools(store);

export const useStore = create(store);

