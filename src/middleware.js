import { NextResponse } from "next/server";
import { verifyJwt } from "@/lib/jwt";

export const config = {
  matcher: [
    "/api/calendar/:path*",
    "/api/company/:path*",
    "/api/contract/:path*",
    "/api/deal/:path*",
    "/api/finance/:path*",
    "/api/legal/:path*",
    "/api/lookup/:path*",
    "/api/operation/:path*",
    "/api/storage/:path*",
    "/api/trade/:path*",
    "/api/user/:path*",
    "/api/organization/:path*",
    "/api/report/:path*",
    "/api/file/:path*",
  ],
};

export default async function middleware(req) {
  const token = req.headers.get("authorization")?.split("Bearer ").at(1);
  const verifiedToken =
    token && (await verifyJwt(token).catch((err) => console.log(err)));
  if (!verifiedToken) {
    return NextResponse.json({ error: "unauthorized" }, { status: 401 });
  } else {
    return NextResponse.next();
  }
}
