import React from "react";
import Image from "next/image";
import { useRouter } from "next/navigation";
export default function Logo() {
  const router = useRouter();
  const handleNewClick = () => {
    router.push("/dashboard");
  };

  return (
    <div>
      <div
        style={{
          height: "100px",
          overflow: "hidden",
          display: "block",
          margin: "auto",
        }}
        href="/"
      >
        <Image
          src="/VINSON.svg"
          alt="logo"
          height={150}
          width={150}
          priority
          onClick={handleNewClick}
        />
      </div>
    </div>
  );
}
