import { useMediaQuery, Box, Drawer } from "@mui/material";
import SidebarItems from "@/components/SidebarItems";
import Image from "next/image";

import { useState } from "react";
const Sidebar = ({
  isMobileSidebarOpen,
  onSidebarClose,
  onSecSideBarClose,
  isSecSideBarOpen,
}) => {
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up("lg"));

  const sidebarWidth = "90px";

  if (isSecSideBarOpen) {
    return (
     
        <Box
          sx={{
            width: sidebarWidth,
            flexShrink: 0,
          }}
        >
          {/* ------------------------------------------- */}
          {/*Second Sidebar for desktop */}
          {/* ------------------------------------------- */}
          <Drawer
            anchor="left"
            onClose={onSecSideBarClose}
            variant="permanent"
            PaperProps={{
              sx: {
                width:sidebarWidth,
                mr:1,
                boxSizing: "border-box",
                border: "0",
                backgroundColor: "primary.main",
              },
            }}
          >
            {/* ------------------------------------------- */}
            {/* Second Sidebar Box */}
            {/* ------------------------------------------- */}
            <Box
              sx={{
                width:sidebarWidth,
                height: "100%",
              }}
              py={2}
            >
              {/* ------------------------------------------- */}
              {/* Logo */}
              {/* ------------------------------------------- */}
              <Box px={2}>
                <Image
                  src="/VINSON.png"
                  alt="logo"
                  height={50}
                  width={50}
                  priority
                />
              </Box>
              <Box>
                {/* ------------------------------------------- */}
                {/* Sidebar Items */}
                {/* ------------------------------------------- */}
                <Box>
                  <SidebarItems />
                </Box>
              </Box>
            </Box>
          </Drawer>
        </Box>
      
    );
   } 
  else if (lgUp) {
    return (
      <div
        style={{
          width: sidebarWidth,
        }}
      >
        <Box
          sx={{
            width:sidebarWidth,
            flexShrink: 0,
          }}
        >
          {/* ------------------------------------------- */}
          {/* Sidebar for desktop */}
          {/* ------------------------------------------- */}
          <Drawer
            anchor="left"
            variant="permanent"
            PaperProps={{
              sx: {
                mr:1,
                width: sidebarWidth,
                boxSizing: "border-box",

                backgroundColor: "primary.main",
              },
            }}
          >
            {/* ------------------------------------------- */}
            {/* Sidebar Box */}
            {/* ------------------------------------------- */}
            <Box
              sx={{
                height: "100%",
              }}
              py={2}
            >
              {/* ------------------------------------------- */}
              {/* Logo */}
              {/* ------------------------------------------- */}
              <Box px={2}>
                <Image
                  src="/VINSON.png"
                  alt="logo"
                  height={50}
                  width={50}
                  priority
                />
              </Box>
              <Box>
                {/* ------------------------------------------- */}
                {/* Sidebar Items */}
                {/* ------------------------------------------- */}
                <Box>
                  <SidebarItems />
                </Box>
              </Box>
            </Box>
          </Drawer>
        </Box>
      </div>
    );
  } 
  else
    return (
     
        
          <Drawer
            anchor="left"
            open={isMobileSidebarOpen}
            onClose={onSidebarClose}
            variant="temporary"
            PaperProps={{
              sx: {
                mr:1,
                width: sidebarWidth,
                backgroundColor: "primary.main",
              },
            }}
          >
            {/* ------------------------------------------- */}
            {/* Logo */}
            {/* ------------------------------------------- */}
            <Box px={2} py={2}>
              <Image
                src="/VINSON.png"
                alt="logo"
                height={50}
                width={50}
                priority
              />
            </Box>
            {/* ------------------------------------------- */}
            {/* Sidebar For Mobile */}
            {/* ------------------------------------------- */}
            <SidebarItems />
          </Drawer>
        
     
    );
};

export default Sidebar;
