import { fetcher } from "@/lib/fetcher";
import {
  Box,
  Button,
  Divider,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { useRef, useState } from "react";
import useSWR from "swr";
import { DownloadTableExcel } from "react-export-table-to-excel";
import * as yup from "yup";
import {
  ClearIcon,
  DatePicker,
  LocalizationProvider,
} from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { numberWithCommas } from "@/lib/numberWithCommas";
export default function Recievable() {
  const { data: session } = useSession();
  const [zuruvalue, setZuruvalue] = useState();
  const [startDate, setStartDate] = useState(
    dayjs(new Date(new Date().getFullYear(), 0, 1))
  );
  const [endDate, setEndDate] = useState(dayjs(new Date()));
  const formik = useFormik({
    initialValues: {
      company_id: "",
      counter_party: "",
      trade_id: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
    }),
    onSubmit: async (values) => {},

    enableReinitialize: true,
  });
  // const {
  //   data: data,
  //   error,
  //   isLoading,
  // } = useSWR(
  //   formik.values.counter_party
  //     ? [
  //         `/api/report/recievable/${formik.values.company_id}/${
  //           formik.values.counter_party
  //         }/${startDate.format("YYYY-MM-DD")}/${endDate.format("YYYY-MM-DD")}`,
  //         session?.user?.token,
  //       ]
  //     : null,
  //   fetcher
  // );
  const {
    data: data,
    error,
    isLoading,
  } = useSWR(
    formik.values.company_id && formik.values.counter_party
      ? [
          `/api/report/recievable/${formik.values.company_id}/${formik.values.counter_party}/${startDate.format("YYYY-MM-DD")}/${endDate.format("YYYY-MM-DD")}`,
          session?.user?.token,
        ]
      : formik.values.company_id
      ? [
          `/api/report/recievable/${formik.values.company_id}/All/${startDate.format("YYYY-MM-DD")}/${endDate.format("YYYY-MM-DD")}`,
          session?.user?.token,
        ]
      : [
        `/api/report/recievable/All/All/${startDate.format("YYYY-MM-DD")}/${endDate.format("YYYY-MM-DD")}`,
        session?.user?.token,
      ],
    fetcher
  );
  console.log(data);
  const { data: companyId } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: counterParty } = useSWR(
    [`api/legal`, session?.user?.token],
    fetcher
  );

  const handleClearCP = () => {
    formik.setFieldValue("counter_party", "");
  };
  const handleStartDateChange = (newValue) => {
    setStartDate(newValue);
  };

  const handleEndDateChange = (newValue) => {
    setEndDate(newValue);
  };

  const formatAsCurrency = (amount) => {
    const formatter = new Intl.NumberFormat("mn-MN", {
      style: "currency",
      currency: "MNT",
    });
    const formattedAmount = formatter.format(amount);
    return formattedAmount.replace("MNT", "₮").replace(/\.00$/, "");
  };

  // const numberWithCommas = (number) => {
  //   return number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  // };
  const tableRef = useRef(null);
  let totalDispatch = 0;
  let totalAmount = 0;
  let totalPayment = 0;
  let totalBalance = 0;

  return (
    <>
      <Stack>
        <Stack
          direction="row"
          spacing={1}
          display={"flex"}
          justifyContent={"space-between"}
        >
          <Stack
            direction={"row"}
            spacing={1}
            justifyContent={"flex-start"}
            alignItems={"center"}
          >
            <FormControl sx={{ width: "20%" }}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
                sx={{ ml: -20 }}
              >
                <DatePicker
                  label="Эхлэх огноо"
                  format="YYYY-MM-DD"
                  value={startDate}
                  onChange={handleStartDateChange}
                  slotProps={{ textField: { size: "small", minwidth: 200 } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl sx={{ ml: 5, width: "20%" }}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
              >
                <DatePicker
                  label="Дуусах огноо"
                  format="YYYY-MM-DD"
                  value={endDate}
                  onChange={handleEndDateChange}
                  slotProps={{ textField: { size: "small", minwidth: 200 } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Company</InputLabel>
              <Select
                sx={{ height: "34px", width: "200px" }}
                defaultValue=""
                labelId="company_id"
                margin="dense"
                label="Company"
                name="company_id"
                // onChange={formik.handleChange}
                onChange={(e) => {
                  formik.setFieldValue("company_id", e.target.value);
                }}
                value={formik.values.company_id}
              >
                {Array.isArray(companyId?.result) ? (
                  companyId?.result.map((item) => (
                    <MenuItem key={item.org_id} value={item.org_id}>
                      {item.org_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem value=""> </MenuItem>
                )}
              </Select>
            </FormControl>
            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Counterparty</InputLabel>
              <Select
                sx={{ height: "34px", width: "200px" }}
                defaultValue=""
                labelId="counter_party"
                margin="dense"
                label="Counterparty"
                name="counter_party"
                // onChange={formik.handleChange}
                onChange={(e) => {
                  formik.setFieldValue("counter_party", e.target.value);
                }}
                value={formik.values.counter_party}
              >
                {Array.isArray(counterParty?.result) ? (
                  counterParty?.result.map((item) => (
                    <MenuItem
                      key={item.legal_entity_id}
                      value={item.legal_entity_id}
                    >
                      {item.name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem value=""> </MenuItem>
                )}
              </Select>
            </FormControl>
            {/* <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Trade</InputLabel>
              <Stack>
                <Select
                  sx={{ height: "34px", width: "200px" }}
                  defaultValue=""
                  labelId="trade_id"
                  margin="dense"
                  label="Trade"
                  name="trade_id"
                  // onChange={formik.handleChange}
                  onChange={(e) => {
                    formik.setFieldValue("trade_id", e.target.value);
                  }}
                  value={formik.values.trade_id}
                >
                  {Array.isArray(dealIdData?.result) ? (
                    dealIdData?.result.map((item) => (
                      <MenuItem key={item.trade_id} value={item.trade_id}>
                        {item.title}
                      </MenuItem>
                    ))
                  ) : (
                    <MenuItem value=""> </MenuItem>
                  )}
                </Select>
                {formik.values.trade_id && (
                  <IconButton
                    aria-label="clear trade_id selection"
                    onClick={handleClearCP}
                    size="small"
                  >
                    <ClearIcon />
                  </IconButton>
                )}
              </Stack>
            </FormControl> */}
          </Stack>

          <DownloadTableExcel
            filename="Recievable Control"
            sheet="users"
            currentTableRef={tableRef.current}
          >
            <Button variant="outlined" color="success" sx={{ height: "34px" }}>
              EXCEL
            </Button>
          </DownloadTableExcel>
        </Stack>

        <Divider
          sx={{
            my: 2,
            mr: 2,
          }}
        />
        <TableContainer
          sx={{ maxHeight: "80vh", mb: 2, mr: 2, overflow: "auto" }}
        >
          <Table
            size="small"
            stickyHeader
            ref={tableRef}
            sx={{
              "& .MuiTableCell-root": {
                border: "0.5px solid #ddd",
              },
              overflow: "auto",
            }}
          >
            <TableHead sx={{ position: "sticky", zIndex: 100 }}>
              <TableRow>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Deal</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Trade
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Parcel ID</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Spot or Term
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Product</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "170px" }}>
                    Grade
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Group Company
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Counterparty
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Operator</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Trader</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>IncoTerm</Typography>
                </TableCell>

                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Voyage Vessel
                  </Typography>
                </TableCell>

                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Parcel Load Port
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "130px" }}>
                    Parcel discharge port
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Delivery Status
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Invoice status
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Invoice №
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Invoice Due Date
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Date Inv Entered
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    PaymentTerms
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "130px" }}>
                    Payment Due Date
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "200px" }}>
                    Financing Bank
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Finance person
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "100px" }}>
                    Trans type
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "120px" }}>
                    Due Date Comment
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "120px" }}>
                    Comment category
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "200px" }}>
                    TransactionBank
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "160px" }}>
                    Quantity Actual/Estimated
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Units</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Value(USD)</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Quantity</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Price</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "120px" }}>
                    Jupiter value (USD)
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "120px" }}>
                    Invoiced value(USD)
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Accrual (USD)
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Paid amount
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "90px" }}>
                    Over due day
                  </Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data?.result &&
                data.result.map((item, i) => {
                  const totalPrice = item.price * Number(item.quantity);
                  totalDispatch += Number(item.dispatch_quantity);
                  totalAmount += totalPrice;
                  const zuruvalue = item.jupitar_value - item.value_usd;

                  return (
                    <>
                      <TableRow key={i}>
                        <TableCell>
                          <Typography> {item.deal_id}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>T- {item.trade_id}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.voyage_parcel_id}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.trade_type}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.product}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.grade}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.company}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.counterparty}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.operator}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.trader}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.terms}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.voyage_transportation}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.parcel_load_port}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.parcel_discharge_port}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.delivery_status}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{item.invoice_payment_status}</Typography>
                        </TableCell>

                        <TableCell>
                          <Typography> {item.invoice_number}</Typography>
                        </TableCell>

                        <TableCell>
                          <Typography>
                            {item.invoice_due_date
                              ? dayjs(item.invoice_due_date).format(
                                  "YYYY-MM-DD"
                                )
                              : null}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {item.invoice_date
                              ? dayjs(item.invoice_date).format("YYYY-MM-DD")
                              : null}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.payment_terms}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {item.payment_due_date
                              ? dayjs(item.payment_due_date).format(
                                  "YYYY-MM-DD"
                                )
                              : null}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.bank_name}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.financer}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.invoice_type}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.invoice_comments}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.invoice_description}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.bank_name}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.quantity_actual_est}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.currency}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {numberWithCommas(item.value_usd)}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {numberWithCommas(item.quantity)}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {numberWithCommas(item.amount)}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {numberWithCommas(item.jupitar_value)}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {numberWithCommas(item.value_usd)}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>{numberWithCommas(zuruvalue)}</Typography>
                        </TableCell>
                        <TableCell>
                          <Typography>
                            {numberWithCommas(item.paid_amount)}
                          </Typography>
                        </TableCell>
                        <TableCell>
                          <Typography> {item.overduedays}</Typography>
                        </TableCell>
                      </TableRow>
                    </>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <Divider
          sx={{
            mb: 2,
            mr: 2,
          }}
        />
      </Stack>
    </>
  );
}
