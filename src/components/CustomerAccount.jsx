import { fetcher } from "@/lib/fetcher";
import {
  Box,
  Button,
  Divider,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { useRef, useState } from "react";
import useSWR from "swr";
import { DownloadTableExcel } from "react-export-table-to-excel";
import * as yup from "yup";
import {
  ClearIcon,
  DatePicker,
  LocalizationProvider,
} from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

export default function CustomerAccount() {
  const { data: session } = useSession();
  const [startDate, setStartDate] = useState(
    dayjs(new Date(new Date().getFullYear(), 0, 1))
  );
  const [endDate, setEndDate] = useState(dayjs(new Date()));
  const formik = useFormik({
    initialValues: {
      company_id: "",
      counter_party: "",
      trade_id: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
    }),
    onSubmit: async (values) => { },

    enableReinitialize: true,
  });
  const {
    data: data,
    error,
    isLoading,
  } = useSWR(
    formik.values.counter_party
      ? [
        `/api/report/customer/account/${formik.values.company_id}/${formik.values.counter_party
        }/${startDate.format("YYYY-MM-DD")}/${endDate.format("YYYY-MM-DD")}`,
        session?.user?.token,
      ]
      : null,
    fetcher
  );
  const { data: companyId } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: counterParty } = useSWR(
    [`api/legal`, session?.user?.token],
    fetcher
  );

  const handleClearCP = () => {
    formik.setFieldValue("counter_party", "");
  };
  const handleStartDateChange = (newValue) => {
    setStartDate(newValue);
  };

  const handleEndDateChange = (newValue) => {
    setEndDate(newValue);
  };

  const formatAsCurrency = (amount) => {
    const formatter = new Intl.NumberFormat("mn-MN", {
      style: "currency",
      currency: "MNT",
    });
    const formattedAmount = formatter.format(amount);
    return formattedAmount.replace("MNT", "₮").replace(/\.00$/, "");
  };

  const numberWithCommas = (number) => {
    return number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  const tableRef = useRef(null);
  let totalDispatch = 0;
  let totalAmount = 0;
  let totalPayment = 0;
  let totalBalance = 0;

  return (
    <>
      <Stack>
        <Stack
          direction="row"
          spacing={1}
          display={"flex"}
          justifyContent={"space-between"}
        >
          <Stack
            direction={"row"}
            spacing={1}
            justifyContent={"flex-start"}
            alignItems={"center"}
          >
            <FormControl sx={{ width: "20%" }}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
                sx={{ ml: -20 }}
              >
                <DatePicker
                  label="Эхлэх огноо"
                  format="YYYY-MM-DD"
                  value={startDate}
                  onChange={handleStartDateChange}
                  slotProps={{ textField: { size: "small", minwidth: 200 } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl sx={{ ml: 5, width: "20%" }}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
              >
                <DatePicker
                  label="Дуусах огноо"
                  format="YYYY-MM-DD"
                  value={endDate}
                  onChange={handleEndDateChange}
                  slotProps={{ textField: { size: "small", minwidth: 200 } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Company</InputLabel>
              <Select
                sx={{ height: "34px", width: "200px" }}
                defaultValue=""
                labelId="company_id"
                margin="dense"
                label="Company"
                name="company_id"
                // onChange={formik.handleChange}
                onChange={(e) => {
                  formik.setFieldValue("company_id", e.target.value);
                }}
                value={formik.values.company_id}
              >
                {Array.isArray(companyId?.result) ? (
                  companyId?.result.map((item) => (
                    <MenuItem key={item.org_id} value={item.org_id}>
                      {item.org_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem value=""> </MenuItem>
                )}
              </Select>
            </FormControl>
            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Counterparty</InputLabel>
              <Select
                sx={{ height: "34px", width: "200px" }}
                defaultValue=""
                labelId="counter_party"
                margin="dense"
                label="Counterparty"
                name="counter_party"
                // onChange={formik.handleChange}
                onChange={(e) => {
                  formik.setFieldValue("counter_party", e.target.value);
                }}
                value={formik.values.counter_party}
              >
                {Array.isArray(counterParty?.result) ? (
                  counterParty?.result.map((item) => (
                    <MenuItem
                      key={item.legal_entity_id}
                      value={item.legal_entity_id}
                    >
                      {item.name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem value=""> </MenuItem>
                )}
              </Select>
            </FormControl>
            {/* <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Trade</InputLabel>
              <Stack>
                <Select
                  sx={{ height: "34px", width: "200px" }}
                  defaultValue=""
                  labelId="trade_id"
                  margin="dense"
                  label="Trade"
                  name="trade_id"
                  // onChange={formik.handleChange}
                  onChange={(e) => {
                    formik.setFieldValue("trade_id", e.target.value);
                  }}
                  value={formik.values.trade_id}
                >
                  {Array.isArray(dealIdData?.result) ? (
                    dealIdData?.result.map((item) => (
                      <MenuItem key={item.trade_id} value={item.trade_id}>
                        {item.title}
                      </MenuItem>
                    ))
                  ) : (
                    <MenuItem value=""> </MenuItem>
                  )}
                </Select>
                {formik.values.trade_id && (
                  <IconButton
                    aria-label="clear trade_id selection"
                    onClick={handleClearCP}
                    size="small"
                  >
                    <ClearIcon />
                  </IconButton>
                )}
              </Stack>
            </FormControl> */}
          </Stack>

          <DownloadTableExcel
            filename="CustomerAccount"
            sheet="users"
            currentTableRef={tableRef.current}
          >
            <Button variant="outlined" color="success" sx={{ height: "34px" }}>
              EXCEL
            </Button>
          </DownloadTableExcel>
        </Stack>

        <Divider
          sx={{
            my: 2,
            mr: 2,
          }}
        />
        <TableContainer sx={{ maxHeight: "80vh", mb: 2, mr: 2 }}>
          <Table
            size="small"
            stickyHeader
            ref={tableRef}
            sx={{
              "& .MuiTableCell-root": {
                border: "0.5px solid #ddd",
              },
            }}
          >
            <TableHead sx={{ position: "sticky", zIndex: 100 }}>
              <TableRow>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Seller</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Buyer</Typography>
                </TableCell>

                {/* <TableCell colSpan={8}>Company name</TableCell> */}

                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Trade №</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Product</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Contract quantity
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Dispatch
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "130px" }}>
                    Price
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Total amount
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Payment date
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Payment amount
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Balance</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data?.result.map((item, i) => {
                const totalPrice = item.price * Number(item.quantity);
                totalDispatch += Number(item.dispatch_quantity);
                totalAmount += totalPrice;
                return (
                  <>
                    <TableRow key={i}>
                      <TableCell>
                        <Typography> {item.counterparty_name}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography>{item.company_name}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography>T- {item.trade_id}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography> {item.product_name} / {item.grade_name}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography> {item.quantity}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography> {item.dispatch_quantity}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography> {numberWithCommas(item.price)}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography>{numberWithCommas(totalPrice)}</Typography>
                      </TableCell>
                      <TableCell>
                        {item.payments.map((payment, j) => {
                          return (
                            <TableRow key={j}>
                             <Typography>{dayjs(payment.pay_date).format("YYYY-MM-DD")}</Typography>

                            </TableRow>
                          );
                        })}
                      </TableCell>
                      <TableCell>
                        {item.payments.map((payment, j) => {
                          totalPayment += payment.value

                          return (
                            <TableRow key={j}>
                               <Typography> {numberWithCommas(payment.value)}</Typography>



                            </TableRow>
                          );
                        })}
                      </TableCell>
                      <TableCell>
                        <Typography> {numberWithCommas(totalPrice - totalPayment)}</Typography>{" "}
                      </TableCell>
                    </TableRow>
                  </>
                );
              })}
              <TableRow>
                <TableCell colSpan={5}>Total amount:</TableCell>
                <TableCell colSpan={1}>
                  {numberWithCommas(totalDispatch)}
                </TableCell>
                <TableCell colSpan={1}></TableCell>

                <TableCell colSpan={1}>
                  {numberWithCommas(totalAmount)}
                </TableCell>
                <TableCell colSpan={1}></TableCell>

                <TableCell colSpan={1}>
                  {numberWithCommas(totalPayment)}
                </TableCell>
                <TableCell colSpan={1}>
                  {numberWithCommas(totalAmount - totalPayment)}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <Divider
          sx={{
            mb: 2,
            mr: 2,
          }}
        />
      </Stack>
    </>
  );
}
