import { alpha, styled } from "@mui/material/styles";
import React from "react";
import {
  TreeItem,
  treeItemClasses,
  useTreeItem,
} from "@mui/x-tree-view/TreeItem";
import clsx from "clsx";
import { Typography } from "@mui/material";

const StyledTreeItem = styled(TreeItem)(({ theme }) => ({
  [`& .${treeItemClasses.iconContainer}`]: {
    "& .close": {
      opacity: 0.3,
    },
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: 15,
    paddingLeft: 18,
    borderLeft: `1px dashed ${alpha(theme.palette.text.primary, 0.4)}`,
  },
}));

const CustomContent = React.forwardRef(function CustomContent(props, ref) {
  const {
    classes,
    className,
    label,
    nodeId,
    flagQ,
    flagTD,
    flagBCD,
    icon: iconProp,
    expansionIcon,
    displayIcon,
    nodeIcon,
    onClick,
  } = props;
  const {
    disabled,
    expanded,
    selected,
    focused,
    handleExpansion,
    handleSelection,
    preventSelection,
  } = useTreeItem(nodeId);
  const icon = iconProp || expansionIcon || displayIcon;

  const handleMouseDown = (event) => {
    preventSelection(event);
  };

  const handleExpansionClick = (event) => {
    handleExpansion(event);
  };

  const handleSelectionClick = (event) => {
    handleSelection(event);
  };
  const customStyles = {
    firstT: {
      color: flagQ == "N" ? "green" : "red",
    },
    lastD: {
      color: flagTD == "N" ? "green" : "red",
    },
    lastT: {
      color: flagBCD == "N" ? "green" : "red",
    },
  };
  const startBracket = label.indexOf("[");
  const endBracket = label.indexOf("]");

  let customLabel = "";
  if (nodeId.substring(0, 1) === "3") {
    const middleT = label.substring(0, startBracket); // From start to [
    const lastD = label.substring(startBracket + 1, label.indexOf(", ")); // From [BL-2024-06-18 to ,
    const lastT = label.substring(
      label.indexOf(", ") + 2,
      label.lastIndexOf(", ")
    ); // From BC-2024-06-18 to Q-200 CM
    const firstT = label.substring(label.lastIndexOf(", ") + 2, endBracket); // From Q-200 CM to ]


    customLabel = (
      <span>
        {/*bs */ <span className="middleT">{middleT}</span>}
        {/*bl*/ <span style={customStyles.lastD}> {lastD}</span>}
        {/*bc*/ <span style={customStyles.lastT}> {lastT}</span>}
        {/*q*/ <span style={customStyles.firstT}> {firstT}</span>}
      </span>
    );
  }
  return (
    <div
      className={clsx(className, classes.root, {
        [classes.expanded]: expanded,
        [classes.selected]: selected,
        [classes.focused]: focused,
        [classes.disabled]: disabled,
      })}
      onMouseDown={handleMouseDown}
      ref={ref}
    >
      <div onClick={handleExpansionClick} className={classes.iconContainer}>
        {icon}
      </div>

      <div className={classes.iconContainer}> {nodeIcon}</div>
      {nodeId.substring(0, 1) === "3" ? (
        <Typography
          onClick={onClick}
          component={"span"}
          className={classes.label}
        >
          {customLabel}
        </Typography>
      ) : (
        <Typography
          onClick={onClick}
          component={"span"}
          className={classes.label}
        >
          {label}
        </Typography>
      )}
    </div>
  );
});

const CustomTreeItem = React.forwardRef(function CustomTreeItem(props, ref) {
  const { flagQ, flagTD, flagBCD, nodeIcon } = props;
  return (
    <StyledTreeItem
      ContentComponent={(contentProps) => (
        <CustomContent
          {...contentProps}
          nodeIcon={nodeIcon}
          flagQ={flagQ}
          flagBCD={flagBCD}
          flagTD={flagTD}
        />
      )}
      {...props}
      ref={ref}
    />
  );
});

export default CustomTreeItem;
