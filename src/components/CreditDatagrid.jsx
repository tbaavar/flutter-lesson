"use client";
import * as React from "react";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import PriceChangeIcon from "@mui/icons-material/PriceChange";
import useSWR from "swr";
import MyAlert from "@/components/Myalert";
import CloseIcon from "@mui/icons-material/Close";
import { useState } from "react";
import CustomNoRowsOverlay from "@/components/NoRecord";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { numberWithCommas } from "@/lib/numberWithCommas";
import {
  Box,
  Button,
  CircularProgress,
  Typography,
  Stack,
  IconButton,
  Modal,
  Chip,
  TextField,
  Alert,
  Divider,
  AlertTitle,
} from "@mui/material";
import dayjs from "dayjs";
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
  GridRowEditStopReasons,
} from "@mui/x-data-grid";
import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import { fetchData } from "@/lib/fetch";
import { PanoramaFishEyeSharp } from "@mui/icons-material";
import { useEffect } from "react";

export default function CreditDatagrid({
  params,
  counterParty,
  dataRealChange,
  invoiceAmount,
  invoiceType,
  rows,
  setRows,
}) {
  const { data: session } = useSession();
  const { invoice_id } = params;
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [offset, setOffset] = React.useState(false);
  const [deleteItem, setDeleteItem] = React.useState(null);
  const [rowModesModel, setRowModesModel] = React.useState({});
  const [filterText, setFilterText] = useState("");
  const [paidAmount, setPaidAmount] = useState();
  const [offsetInvoiceId, setoffsetInvoiceId] = useState("");
  const [Myid, setMyId] = useState("");
  const [unpaidAmount, setUnpaidAmount] = useState();
  const [finalPaidAmount, setFinalPaidAmount] = useState();
  useEffect(() => {}, [rows]);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOffsetOpen = (id) => () => {
    setOffset(true);
    setMyId(id);
  };

  const handleOffsetClose = () => {
    setOffset(false);
  };

  const handleFilterChange = (e) => {
    setFilterText(e.target.value);
  };

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const { data: costs } = useSWR(
    [`/api/lookup/cost`, session?.user?.token],
    fetcher
  );

  const { data: invoice } = useSWR(
    counterParty
      ? [
          `/api/lookup/invoice/${session?.user.user_id}/${counterParty}/${invoice_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const {
    data: debits,
    error,
    isLoading,
    mutate,
  } = useSWR(
    invoice_id !== "new"
      ? [
          `/api/finance/invoice/detail/${invoice_id}/Credit`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  const rowInfoices =
    invoice?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    })) || [];

  const filteredRows = rowInfoices.filter((row) =>
    Object.values(row).some(
      (value) =>
        (typeof value === "string" &&
          value.toLowerCase().includes(filterText.toLowerCase())) ||
        (typeof value === "number" &&
          value.toString().includes(filterText.toLowerCase()))
    )
  );
  if (debits?.result?.length > 0 && rows === null) {
    const rowsDebit = debits?.result?.map((row, i) => ({
      id: i + 1,
      invoice_detail_id: row.invoice_detail_id,
      description: row.description,
      cost_code: row.cost_code,
      cost: row.cost_code,
      parent_invoice_detail_id: row.parent_invoice_detail_id,
      value: row.value,
      offset: row.offset_flag,
      offset_inv_id: null,
      exp_pay_date: dayjs(row.exp_pay_date).format("YYYY-MM-DD"),
      tran_date: dayjs(row.tran_date).format("YYYY-MM-DD"),
      type: "EDIT",
    }));
    setRows(rowsDebit);
  }

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;
    const handleClick = () => {
      let id = null;

      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            id: id,
            cost: "C0",
            value: 0,
            isNew: true,
            exp_pay_date: new Date(),
            tran_date: new Date(),
            description: "",
            type: "ADD",
          },
        ]);
      } else {
        id = rows.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            id: id,
            cost: "C0",
            value: 0,
            isNew: true,
            exp_pay_date: new Date(),
            tran_date: new Date(),
            description: "",
            type: "ADD",
          },
        ]);
      }
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    if (
      session &&
      session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
        .permission === "Edit"
    )
      return (
        <GridToolbarContainer>
          <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
            Add
          </Button>
        </GridToolbarContainer>
      );
  }
  const handlePaidAmount = (e) => {
    console.log("pp", e.target.value);
    setPaidAmount(e.target.value);
  };

  const handleEditClick = (id) => () => {
    setPaidAmount(id.row.value);
    setRowModesModel({
      ...rowModesModel,
      [id.row.id]: { mode: GridRowModes.Edit },
    });

    setMyId(id);
  };

  const handleSaveClick = (id) => {
    // setFinalPaidAmount(paidAmount);
    setRowModesModel({
      ...rowModesModel,
      [id.row.id]: { mode: GridRowModes.View },
    });
  };

  const handleDeleteClick = (id) => () => {
    setOpen(true);
    setDeleteItem(id);
  };
  const deleteData = async (id) => {
    if (deleteItem.row.invoice_detail_id) {
      const body = {
        username: session?.user?.username,
        invoice_detail_id: deleteItem.row.invoice_detail_id,
      };
      try {
        setLoading(true);
        const res = await fetchData(
          `/api/finance/invoice/detail`,
          "DELETE",
          body,
          session?.user?.token
        );

        if (res.status === 200) {
          mutate;
          setRows(rows.filter((row) => row.id !== deleteItem.row.id));
          toast.success("Амжилттай хадгаллаа", {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error("Хадгалах үед алдаа гарлаа", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    } else {
      setRows(rows.filter((row) => row.id !== deleteItem.row.id));
    }
  };
  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id.row.id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id.row.id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id.row.id));
    }
  };
  
  let updatedRow;
  const processRowUpdate = (newRow) => {
    if (offsetInvoiceId) {
      updatedRow = {
        ...newRow,
        isNew: false,
        value: finalPaidAmount,
        offset_inv_id: offsetInvoiceId,
      };
    } else {
      updatedRow = {
        ...newRow,
        isNew: false,
        offset_inv_id: "",
      };
    }

    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const handleRowClick = (params) => {
    setUnpaidAmount(params.row.unpaid_amount);
    setPaidAmount(params.row.unpaid_amount);
    setoffsetInvoiceId(params.row.invoice_id);
  };

  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "cost",
      renderHeader: () => <strong>{"Cost"}</strong>,
      type: "singleSelect",
      valueOptions: costs?.result,
      getOptionLabel: (value) => `${value.cost_code}-${value.cost_name}`,
      getOptionValue: (value) => value.cost_code,
      editable: true,
      width: 150,
    },

    {
      field: "value",
      type: "number",
      renderHeader: () => <strong>{"Amount"}</strong>,
      editable: true,
      width: 150,
    },
    {
      field: "tran_date",
      renderHeader: () => <strong>{"Tran date"}</strong>,
      valueGetter: (params) => {
        return new Date(params.row.tran_date);
      },
      type: "date",
      editable: true,
      width: 150,
    },
    {
      field: "exp_pay_date",
      renderHeader: () => <strong>{"Exp pay date"}</strong>,
      valueGetter: (params) => {
        return new Date(params.row.exp_pay_date);
      },
      type: "date",
      editable: true,
      width: 150,
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      editable: true,
      width: 250,
    },
    {
      field: "offset",
      renderHeader: () => <strong>{"Offset"}</strong>,
      // renderCell: (params) => {
      //   console.log(`params`, params)
      //   return (
      //     <Box sx={{ flexDirection: "column", display: "flex" }}>
      //       <Typography variant="h7">{params.row.offset ? params.row.offset : null + " - " + params.row.offset_inv_id}</Typography>
      //     </Box>
      //   )
      // },
      // editable: true,
      width: 250,
    },
    {
      field: "edit",
      renderHeader: () => <strong>{"Edit"}</strong>,
      type:
        session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
          .permission === "Edit"
          ? "actions"
          : "",
      headerName: "Actions",
      width: 120,
      cellClassName: "actions",
      getActions: (id) => {
        const isInEditMode =
          rowModesModel[id.row.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              key={id}
              icon={<PriceChangeIcon />}
              disabled={invoiceType === "SI" ? true : false}
              label="Offset"
              sx={{
                color: "primary.main",
              }}
              onClick={handleOffsetOpen(id)}
            />,
            <GridActionsCellItem
              key={id}
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              //  onClick={handleSaveClick(id)}
              onClick={() => {
                setFinalPaidAmount(paidAmount);
                handleSaveClick(id);

                console.log("paidAmount", paidAmount);
              }}
            />,
            <GridActionsCellItem
              key={id}
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        } else
          return [
            <GridActionsCellItem
              key={id}
              icon={<EditIcon />}
              label="Edit"
              className="textPrimary"
              onClick={handleEditClick(id)}
              color="inherit"
            />,
          ];
      },
    },
    {
      field: "delete",
      renderHeader: () => <strong>{"Delete"}</strong>,
      type:
        session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
          .permission === "Edit"
          ? "actions"
          : "",
      headerName: "Actions",
      width: 100,
      cellClassName: "actions",
      getActions: (id) => {
        return [
          <GridActionsCellItem
            key={id}
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  const infoiceCol = [
    {
      field: "id",
      renderHeader: () => <strong>{"Id"}</strong>,
      width: 30,
      headerAlign: "left",
    },
    {
      field: "invoice_id",
      renderHeader: () => <strong>{"Invoice Id"}</strong>,
      width: 110,
      headerAlign: "left",
    },

    {
      field: "invoice_number",
      renderHeader: () => <strong>{"Invoice number"}</strong>,
      width: 130,
      headerAlign: "left",
    },

    {
      field: "invoice_date",
      renderHeader: () => <strong>{"Invoice date"}</strong>,
      width: 110,
      valueGetter: (params) =>
        dayjs(params.row.invoice_date).format("YYYY-MM-DD"),
      headerAlign: "left",
    },

    {
      field: "invoice_due_date",
      renderHeader: () => <strong>{"Invoice due date"}</strong>,
      valueGetter: (params) =>
        dayjs(params.row.invoice_due_date).format("YYYY-MM-DD"),
      width: 110,
      headerAlign: "left",
    },
    {
      field: "invoice_amount",
      renderHeader: () => <strong>{"Invoice Amount"}</strong>,

      renderCell: (params) => {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={numberWithCommas(params.row.invoice_amount)}
          />
        );
      },
      width: 140,
      headerAlign: "left",
    },

    {
      field: "paid_amount",
      renderHeader: () => <strong>{"Paid amount"}</strong>,

      renderCell: (params) => {
        return (
          <Chip
            variant="filled"
            color="regular"
            label={numberWithCommas(params.row.paid_amount)}
          />
        );
      },
      width: 140,
      headerAlign: "left",
    },
    {
      field: "unpaid_amount",
      renderHeader: () => <strong>{"UnPaid amount"}</strong>,

      renderCell: (params) => {
        return (
          <Chip
            variant="filled"
            color="urgent"
            label={numberWithCommas(params.row.unpaid_amount)}
          />
        );
      },
      width: 140,
      headerAlign: "left",
    },
  ];

  return (
    <>
      <ToastContainer />
      <Box
        sx={{
          width: "100%",
          "& .actions": {
            color: "text.secondary",
          },
          "& .textPrimary": {
            color: "text.primary",
          },
        }}
      >
        <DataGrid
          // getRowId={(row) => row.id}
          rows={rows ? rows : []}
          columns={columns}
          editMode="row"
          rowModesModel={rowModesModel}
          onRowModesModelChange={handleRowModesModelChange}
          onRowEditStop={handleRowEditStop}
          processRowUpdate={processRowUpdate}
          slots={{
            toolbar: EditToolbar,
            noRowsOverlay: CustomNoRowsOverlay,
          }}
          slotProps={{
            toolbar: { setRows, setRowModesModel },
          }}
          hideFooter
          autoHeight={true}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
          }}
        />
        <Modal open={open} onClose={handleClose}>
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              bgcolor: "background.paper",
              boxShadow: 24,
              p: 4,
            }}
          >
            <Box>
              <Typography id="modal-modal-title" variant="h6" component="h3">
                Confirm
              </Typography>
            </Box>
            <Stack
              component="div"
              sx={{
                width: 400,
                maxWidth: "100%",
              }}
              noValidate
              autoComplete="off"
            >
              <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
                <Alert severity="error">
                  <AlertTitle>{""}</AlertTitle>
                  Are you sure you want to delete?
                </Alert>
              </Box>
            </Stack>
            <Box
              sx={{
                display: "flex",
                marginBottom: 1,
                justifyContent: "flex-end",
              }}
            >
              <Button
                onClick={async () => {
                  handleClose();
                  deleteData();
                }}
                variant="contained"
                sx={{
                  marginLeft: "10px",
                  marginRight: "10px",
                }}
              >
                Yes
              </Button>
              <Button onClick={handleClose} variant="outlined">
                No
              </Button>
            </Box>
          </Box>
        </Modal>

        <Modal open={offset} onClose={handleOffsetClose}>
          <>
            <ToastContainer />
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                minWidth: 800,
                transform: "translate(-50%, -50%)",
                bgcolor: "background.paper",
                border: "1px solid #000",
                overflowY: "auto",
                overflowX: "auto",
                boxShadow: 0,
                p: 1,
              }}
            >
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h5" gutterBottom>
                  Offset
                </Typography>
                <IconButton
                  sx={{ display: "flex", mt: -1 }}
                  onClick={handleOffsetClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Divider sx={{ mb: 1 }} />
              <Stack spacing={1} direction={"row"}>
                <TextField
                  variant="outlined"
                  size="small"
                  placeholder="Search..."
                  value={filterText}
                  onChange={handleFilterChange}
                  sx={{ minWidth: "300px" }}
                />

                <TextField
                  name="paidAmount"
                  label="Offset Amount"
                  value={paidAmount}
                  onChange={(e) => handlePaidAmount(e)}
                  size="small"
                />
              </Stack>
              <DataGrid
                getRowId={(row) => row.id}
                rows={filteredRows}
                onRowClick={handleRowClick}
                columns={infoiceCol}
                pageSizeOptions={[50]}
                slots={{
                  noRowsOverlay: CustomNoRowsOverlay,
                }}
                sx={{
                  maxWidth: "100vw",
                  backgroundColor: "#FFFFFF",
                  "& .MuiDataGrid-cell:hover": {
                    color: "primary.main",
                  },
                  "& .MuiDataGrid-cell": {
                    borderTop: 1,
                    borderTopColor: "#E9ECF0",
                  },
                  overflow: "auto",
                  maxHeight: "64.4vh",
                  minHeight: "64.4vh",
                }}
              />
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Button variant="outlined" onClick={handleOffsetClose}>
                  Close
                </Button>
                <Button
                  variant="contained"
                  color="dark"
                  onClick={() => {
                    // setRows(
                    //   rows.map((row) =>
                    //     row.id === Myid.row.id
                    //       ? {
                    //           ...Myid.row,
                    //           value: paidAmount,
                    //         }
                    //       : row
                    //   )invoiceAmount
                    // );

                    const checkAmount = rows.reduce(
                      (acc, row) => acc + row.value,
                      0
                    );
                    let remainingAmount = 0;
                    if (checkAmount > 0) {
                      remainingAmount = invoiceAmount - checkAmount;
                    } else {
                      remainingAmount = invoiceAmount;
                    }
                    // console.log(`invoiceAmount`, invoiceAmount);
                    // console.log(`checkAmount`, checkAmount);
                    // console.log(`remainingAmount`, remainingAmount);
                    // console.log(`paidAmount`, paidAmount);
                    // console.log(`unpaidAmount`, unpaidAmount);
                    if (
                      Number(remainingAmount) >= Number(paidAmount) &&
                      Number(paidAmount) <= Number(unpaidAmount)
                    ) {
                      setRowModesModel({
                        ...rowModesModel,
                        [Myid.row.id]: { mode: GridRowModes.Edit },
                      });
                      handleOffsetClose();
                    } else {
                      toast.error("Offset amount is not in range", {
                        position: toast.POSITION.TOP_RIGHT,
                      });
                    }
                  }}
                >
                  Choose
                </Button>
              </Box>
            </Box>
          </>
        </Modal>
      </Box>
    </>
  );
}
