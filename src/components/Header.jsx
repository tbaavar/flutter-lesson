import React from "react";
import {
  Box,
  AppBar,
  Toolbar,
  Stack,
  IconButton,
  Typography,
  useMediaQuery,
  Divider,
  Button,
} from "@mui/material";
import { IconMenu, IconHome2 } from "@tabler/icons-react";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";

// import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import Profile from "@/components/Profile";


const Header = ({ toggleMobileSidebar, toggleSecSideBar }) => {
 
  const AppBarStyled = styled(AppBar)(({ theme }) => ({
    boxShadow: "none",
    background: theme.palette.background.paper,
    justifyContent: "center",
    backdropFilter: "blur(4px)",
    [theme.breakpoints.up("lg")]: {
      minHeight: "1px",
    },
  }));
  const ToolbarStyled = styled(Toolbar)(({ theme }) => ({
    width: "100%",
    color: theme.palette.primary.main,
    border: "1px",
  }));

  return (
    <AppBarStyled  position="sticky" color="default">
      <ToolbarStyled>
        <Box alignItems="right" display={"flex"} >
         <IconButton
          id="mobile-icon"
          color="inherit"
          aria-label="menu"
          onClick={toggleMobileSidebar}
          sx={{mt:-0.3}}
        >
          <IconMenu width="20" height="20" />
        </IconButton>
          <Typography component="div" variant="h6">
            {typeof localStorage !== "undefined"
              ? localStorage.getItem("tradeMenu")
              : ""}
          </Typography>
        </Box>
        <Box flexGrow={1} />
        <Stack spacing={1} direction="row" alignItems="center">
          <Profile />
        </Stack>
      </ToolbarStyled>
    </AppBarStyled>
       
  );
};
Header.propTypes = {
  sx: PropTypes.object,
};
export default Header;
