import { Box, CircularProgress, Typography } from "@mui/material";
import React from "react";

export default function Loading() {
    return (
        <Box
            sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                height: "80vh",
            }}
        >
            <CircularProgress />
            <Typography sx={{ marginTop: 2, fontSize: 14 }}>
                Loading...
            </Typography>
        </Box>
    );
}
