"use client";
import React, { useState } from "react";
import {
  IconButton,
  Avatar,
  Box,
  Menu,
  Button,
  ListItemIcon,
  MenuList,
  Typography,
  Modal,
} from "@mui/material";
import { signOut } from "next-auth/react";
import MenuItem from "@mui/material/MenuItem";
import { Logout, Mail } from "@mui/icons-material";
import { useSession } from "next-auth/react";
import ChangePassword from "../app/(admin)/setting/changepassword/page";
import DownloadIcon from "@mui/icons-material/Download";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 20,

  p: 4,
};
const Profile = () => {
  const { data: session } = useSession();
  const [open, setOpen] = React.useState(false);
  const modalOpen = () => setOpen(true);
  const modalClose = () => setOpen(false);
  const [anchorEl2, setAnchorEl2] = useState(null);
  const handleClick2 = (event) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };

  return (
    <>
      <Box display={"flex"}>
        <Box
          sx={{
            display: {
              xs: "none",
              sm: "flex",
            },
            alignItems: "center",
          }}
        >
          <Typography color="textSecondary" fontWeight="400" sx={{ ml: 1 }}>
            {session?.user?.role}
          </Typography>
          <Typography
            fontWeight="400"
            variant="body2"
            sx={{
              ml: 1,
              mr: 1,
            }}
          >
            {`${session?.user?.lastname.substring(0, 1)}.${
              session?.user?.firstname
            }`}
          </Typography>
        </Box>

        <IconButton
          size="small"
          aria-label="show 11 new notifications"
          color="inherit"
          aria-controls="msgs-menu"
          aria-haspopup="true"
          sx={{
            ...(typeof anchorEl2 === "object" && {
              color: "primary.main",
            }),
          }}
          onClick={handleClick2}
        >
          <Avatar
            src="/avatar-alcides-antonio.png"
            alt="image"
            sx={{
              width: 35,
              height: 35,
            }}
          />
        </IconButton>
        <Menu
          id="msgs-menu"
          anchorEl={anchorEl2}
          keepMounted
          open={Boolean(anchorEl2)}
          onClose={handleClose2}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          sx={{
            "& .MuiMenu-paper": {
              width: "200px",
            },
          }}
        >
          <MenuList>
            <MenuItem onClick={modalOpen}>
              <ListItemIcon>
                <Mail sx={{ color: "#0B626B" }} />
              </ListItemIcon>
              <Typography variant="inherit" sx={{ color: "#0B626B" }}>
                Change Password
              </Typography>
            </MenuItem>
            <MenuItem
              component="a"
              href="/usermanual.pdf"
              download="usermanual.pdf"
            >
              <ListItemIcon>
                <DownloadIcon sx={{ color: "#0B626B" }} fontSize="small" />
              </ListItemIcon>
              <Typography sx={{ color: "#0B626B" }} variant="inherit">
                User manual
              </Typography>
            </MenuItem>

            <MenuItem onClick={() => signOut({ callbackUrl: "/" })}>
              <ListItemIcon>
                <Logout sx={{ color: "#0B626B" }} />
              </ListItemIcon>
              <Typography variant="inherit" sx={{ color: "#0B626B" }}>
                Log out
              </Typography>
            </MenuItem>
          </MenuList>
        </Menu>
      </Box>
      <Modal
        open={open}
        aria-labelledby="modal-changepassword-title"
        aria-describedby="modal-changepassword-description"
      >
        <Box sx={style}>
          <ChangePassword modalClose={modalClose} />
        </Box>
      </Modal>
    </>
  );
};

export default Profile;
