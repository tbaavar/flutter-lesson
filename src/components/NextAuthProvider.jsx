"use client";
import { SessionProvider } from "next-auth/react";

export function NextAuthProviders({ children, session }) {
  return <SessionProvider session={session}>{children}</SessionProvider>;
}
