import React from "react";
import { usePathname } from "next/navigation";
import { Box, List } from "@mui/material";
import NavItem from "@/components/NavItem";
import { useSession } from "next-auth/react";

const SidebarItems = ({ toggleMobileSidebar }) => {
  const { data: session } = useSession();
  const pathname = usePathname();
  const pathDirect = pathname;

  return (

    <Box >
      <List sx={{ pt: 0 }} component="div">
        {session && session?.user?.menuitems.map((item) => {
          return (
            <NavItem
              item={item}
              key={item.menu_id}
              pathDirect={pathDirect}
              onClick={toggleMobileSidebar}
            />
          );
        })}
      </List>
    </Box>
  );
};
export default SidebarItems;
