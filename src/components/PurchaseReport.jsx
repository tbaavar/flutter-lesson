import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Logo from "@/components/Logo";
import Loading from "./Loading";
import ReactToPrint from "react-to-print";
import { Box, Stack,Typography,Drawer,Button } from "@mui/material";
function createData(field, result) {
  return { field, result };
}
const rowss1 = [createData("PAYMENT ORDER-77774457(413866)")];
const rows2 = [createData("Paying Bank:", "result")];
const rows3 = [
  createData("Paying Bank:", "result"),
  createData("Paying Bank:", "result"),
  createData("Paying Bank:", "result"),
  createData("Paying Bank:", "result"),
];
const rows4 = [createData("Pag Bank:", "result")];
export default function PurchaseReport({toggleReport1,setToggleReport1,  loading,}) {
  return (
    <Drawer anchor="right" open={toggleReport1}>
    <Box
      sx={{
        width: {
          mobile: "100vw",
          tablet: "100vw",
          laptop: "70vw",
          desktop: "50vw",
        },
        padding: 3,
      }}
      role="presentation"
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "end",
          mb: 2,
        }}
      >
        <ReactToPrint
          content={() => ref.current}
          trigger={() => (
            <Button
              variant="contained"
              sx={{
                backgroundColor: "green",
                color: "white",
                mr: 2,
                ":hover": {
                  backgroundColor: "green",
                },
              }}
            >
              Print
            </Button>
          )}
          pageStyle={{
            margin: 10,
          }}
        />
        <Button
          variant="contained"
          sx={{
            backgroundColor: "grey",
            color: "white",
            ":hover": {
              backgroundColor: "grey",
            },
          }}
          onClick={() => setToggleReport1(false)}
        >
          Cancel
        </Button>
      </Box>
      {loading ? (
          <Loading />
        ) : (
    <Box>
      <Stack alignItems="center" spacing={3}>
        <Logo />
        <Typography variant="h4">PAYMENT ORDER-77774457(413866)</Typography>
      </Stack>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="a dense table">
          <TableBody>
            {rowss1.map((row) => (
              <TableRow
                key={row.field}
                sx={{ "&:last-child td, &:last-child th": { border: 2 } }}
              >
                <TableCell
                  component="th"
                  scope="row"
                  align="center"
                  style={{ width: "100px", height: "50px" }} // Apply styles as inline styles
                >
                  {row.field}
                </TableCell>
              </TableRow>
            ))}
            <Box>
              {/* <Stack alignItems="center" spacing={3}>
                <Logo />
                <Typography variant="h4">logo</Typography>
              </Stack>
              <TableContainer sx={{ mb: 1 }} component={Paper}>
                <Table size="small" aria-label="a dense table">
                  <TableBody>
                    {rowss1.map((row) => (
                      <TableRow
                        key={row.field}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 2 },
                        }}
                      >
                        <TableCell
                          component="th"
                          scope="row"
                          align="center"
                          sx={{ width: "200px", height: "50px" }}
                        >
                          <Typography
                            variant="body1"
                            style={{ fontSize: "22px" }}
                          >
                            {row.field}
                          </Typography>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer> */}
              <Typography sx={{ ml: 1 }} variant="h7">
                Raised By: Byambasuren Bayar on 10 Oct 2023
              </Typography>
              <TableContainer sx={{ mt: 3 }} component={Paper}>
                <Table size="small" aria-label="a dense table">
                  <TableBody>
                    {rows2.map((row) => (
                      <TableRow
                        key={row.field}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 1 },
                        }}
                      >
                        <TableCell
                          component="th"
                          scope="row"
                          align="left"
                          style={{ width: "100px", height: "20px" }} // Apply styles as inline styles
                        >
                          {row.field}
                        </TableCell>

                        <TableCell align="left">{row.result}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TableContainer sx={{ mt: 2 }} component={Paper}>
                <Table size="small" aria-label="a dense table">
                  <TableBody>
                    {rows3.map((row) => (
                      <TableRow key={row.field}>
                        <TableCell
                          component="th"
                          scope="row"
                          align="left"
                          sx={{
                            border: "1px solid black",
                            width: "100px",
                            height: "20px",
                          }}
                        >
                          {row.field}
                        </TableCell>
                        <TableCell
                          align="left"
                          sx={{ border: "1px solid black" }}
                        >
                          {row.result}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TableContainer sx={{ mb: 2 }} component={Paper}>
                <Table size="small" aria-label="a dense table">
                  <TableBody>
                    {rows4.map((row) => (
                      <TableRow key={row.field}>
                        <TableCell
                          component="th"
                          scope="row"
                          align="left"
                          sx={{
                            border: "1px solid black",

                            height: "10px",
                          }}
                        >
                          <Typography variant="h7">{row.field}</Typography>
                        </TableCell>
                        <TableCell
                          align="left"
                          sx={{ border: "1px solid black" }}
                        >
                          {row.result}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <Typography sx={{ ml: 1 }} variant="h7">
                Company: TRP, Trafigura Pte Limited
              </Typography>
            </Box>
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
     )}
    </Box>
    </Drawer>
  );
}
