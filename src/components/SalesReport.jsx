import {
  Box,
  Button,
  Drawer,
  Typography,
  Stack,
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Paper,
} from "@mui/material";
import React from "react";
import Loading from "./Loading";
import ReactToPrint from "react-to-print";
import Logo from "@/components/Logo";
function createData(field, result) {
  return { field, result };
}



const rowss = [
  createData("DATE", "10 October 2023"),
  createData("INVOICE NUMBER", "337941"),
  createData("EMAIL", "byambasuren.bayar@trafigura.com"),
];

const rowsss = [
  createData(
    "TO",
    "Shk (Sg) International Pte Ltd 6 YISHUN INDUSTRIAL STREET 1 HEX 04 03 NORTH VIEW BIZHUB..."
  ),
  createData("ATTIN", "jay@shksg.com.sg"),
];
const rowssss = [
  createData("DEAL ID/TRADE", "569608/3233779"),
  createData("vessel", "Railcar"),
  createData("B/L DATE", "15 September 2023"),
  createData("PRODUCT/GRADE", "GAs Oil/0.05% Sulphur"),
  createData("B/L QUANTITY", "541.973 Metric Tonnes"),
  createData("DELIVERY", "CPT/Border China/Mongolia"),
  createData("UNIT PRICE", "USD 1.476.0 PER METRIC TONNIE"),
  createData("TOTAL AMOUNT", "USD 799.899.01"),
  createData("PAYMENT TERMS", "5 Calendar Days After Invoicing (Pre payment)"),
  createData("DUE DATE", "31 October 2023"),
];

const rowsssss = [
  createData("TRAFIGURA PTE LIMITED VAT NUMBER", "GST 19-9601595-D"),
  createData("LOAD PORT", "Border China/Mongolia,Mongolia"),
  createData("DISCHARGE PORT", "Border China/Mongolia,Mongolia"),
  createData("TRAFIGURA PTE LIMITED TAX CODE", "SG99SI"),
  createData("BENEFICIARY", "Trafigura Pte Limited"),
  createData("ACCOUNT NUMBER", "Deutsche Bank AG"),
  createData("BENEFICIARY BANK SWIFT", "Deutsche Bank AG"),
  createData("FAVOAR OF", "Deutsche Bank AG"),
  createData("BENEFICIARY BANK SWIFT", "DEUTGB2LXXX"),
  createData("INTERMEDIARY BANK SWIFT", "DEUTGB2LXXX"),
  createData(
    "REFERENCE",
    "INVOICE 337941 (TO BE ENTERED IN THE PAYMENT DETAILS FIELD OF THE PAYMENT INSTRUCTION)"
  ),
];

const purshaseRows = [
  {
    field: "raised_by",
    name: "Raised By",
    value: "Byambasuren Bayar on 10 Oct 2023",
  },
  { field: "paying_bank", name: "Paying Bank", value: "" },
  {
    field: "to_customer",
    name: "To (Customer)",
    value: "Vs Oil LLC Ulaanbaatar (CP)",
  },
  {
    field: "currency",
    name: "Currency",
    value: "USD",
  },
  {
    field: "amount",
    name: "Amount",
    value: "8,621,334,20",
  },
  { field: "value_date", name: "Value Date", value: "11 Oct 2023" },
  {
    field: "beneficiary_bank",
    name: "Beneficiary Bank Charges to be paid by",
    value: "Vs Oil LLC Ulaanbaatar (CP)",
  },
  { field: "company", name: "Company", value: "TRP, Trafigura Pte Limited" },
  { field: "inv_Ref", name: "Inv Ref", value: "413865" },
  {
    field: "inv_date",
    name: "Inv Date",
    value: "10 Oct 2023",
  },
  {
    field: "deal_id",
    name: "Deal ID",
    value: "569359",
  },
  {
    field: "Creditor Ref",
    name: "creditor_ref",
    value: "23-64",
  },
  {
    field: "invoice_owner",
    name: "Invoice Owner",
    value: "Byambasuren Bayar",
  },
  {
    field: "cost_code_description",
    name: "Cost Code Description",
    value: "Physical Carge Purchase",
  },
  {
    field: "Value",
    name: "Value",
    value: "8,621,334,20",
  },
];

export default function InvoiceReport({
  loading,
  setDrawerLoading,
  toggleReport,
  setToggleReport,
  invoiceType,
}) {
  const ref = React.useRef();
  return (
    <Drawer anchor="right" open={toggleReport}>
      <Box
        sx={{
          width: {
            mobile: "100vw",
            tablet: "100vw",
            laptop: "70vw",
            desktop: "50vw",
          },
          padding: 3,
        }}
        role="presentation"
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "end",
            mb: 2,
          }}
        >
          <ReactToPrint
            content={() => ref.current}
            trigger={() => (
              <Button
                variant="contained"
                sx={{
                  backgroundColor: "green",
                  color: "white",
                  mr: 2,
                  ":hover": {
                    backgroundColor: "green",
                  },
                }}
              >
                Print
              </Button>
            )}
            pageStyle={{
              margin: 10,
            }}
          />
          <Button
            variant="contained"
            sx={{
              backgroundColor: "grey",
              color: "white",
              ":hover": {
                backgroundColor: "grey",
              },
            }}
            onClick={() => setToggleReport(false)}
          >
            Cancel
          </Button>
        </Box>
        {loading ? (
          <Loading />
        ) : (
          <Box>
            <Stack alignItems="center" spacing={3}>
              <Logo />
              <Typography variant="h4">FINAL INVOICE</Typography>
            </Stack>
            <TableContainer component={Paper}>
              <Table size="small" aria-label="a dense table">
                <TableBody>
                  {rowss.map((row) => (
                    <TableRow
                      key={row.field}
                      sx={{
                        "&:last-child td, &:last-child th": {
                          borderBottom: 1,
                          borderTop: 1,
                        },
                      }}
                    >
                      <TableCell component="th" scope="row">
                        {row.field}
                      </TableCell>
                      <TableCell align="left">{row.result}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>

                <TableBody>
                  {rowsss.map((row) => (
                    <TableRow
                      key={row.field}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.field}
                      </TableCell>
                      <TableCell align="left">{row.result}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Typography sx={{ ml: 1 }} variant="h7">
              INVOICE DESCRIPTION:
            </Typography>
            <TableContainer component={Paper}>
              <Table size="small" aria-label="a dense table">
                <TableBody>
                  {rowssss.map((row) => (
                    <TableRow
                      key={row.field}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.field}
                      </TableCell>
                      <TableCell align="left">{row.result}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Box sx={{ flexDirection: "column", display: "flex", ml: 1 }}>
              <Typography variant="h7">CODE:</Typography>
              <Typography variant="h7">
                PLEASE PAY US DOLLAR BY TELEGRAPHIC TRANSFER IN IMMEDIATELY Y
                AVAILABLE FUNDS USING THE FOLLOWING BANKING DETAILS:
              </Typography>
            </Box>
            <TableContainer component={Paper}>
              <Table size="small" aria-label="a dense table">
                <TableBody>
                  {rowsssss.map((row) => (
                    <TableRow key={row.field}>
                      <TableCell component="th" scope="row">
                        {row.field}
                      </TableCell>
                      <TableCell align="left">{row.result}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            
          </Box>
        )}
      </Box>
    </Drawer>
  );
}
