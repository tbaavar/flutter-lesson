import * as XLSX from "xlsx";

export default function exportToExcel(rows, ReportColumn, filename) {
  
  const worksheet = XLSX?.utils?.json_to_sheet(rows);

  const headers = ReportColumn.map((col) => col.header);

  XLSX.utils.sheet_add_aoa(worksheet, [headers], { origin: "A1" });

  ReportColumn.forEach((col, columnIndex) => {
    const columnSum = rows.reduce((total, row) => {
      const value = row[col.field];
      return isNaN(value) ? total : total + value;
    }, null);
    XLSX.utils.sheet_add_aoa(worksheet, [[columnSum]], {
      origin: { r: rows.length + 1, c: columnIndex },
    });
  });

  const workbook = XLSX?.utils?.book_new();

  XLSX?.utils?.book_append_sheet(workbook, worksheet, "Sheet1");

  XLSX?.writeFile(workbook, `${filename}.xlsx`);
}
