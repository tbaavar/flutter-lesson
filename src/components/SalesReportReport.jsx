import { fetcher } from "@/lib/fetcher";
import {
  Box,
  Button,
  Divider,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { useRef, useState } from "react";
import useSWR from "swr";
import { DownloadTableExcel } from "react-export-table-to-excel";
import * as yup from "yup";
import {
  ClearIcon,
  DatePicker,
  LocalizationProvider,
} from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

export default function SalesReport() {
  const { data: session } = useSession();
  const [startDate, setStartDate] = useState(
    dayjs(new Date(new Date().getFullYear(), 0, 1))
  );
  const [endDate, setEndDate] = useState(dayjs(new Date()));
  const formik = useFormik({
    initialValues: {
      company_id: "",
      counter_party: "",
      transfer_type: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
    }),
    onSubmit: async (values) => {},

    enableReinitialize: true,
  });
  const { data: companyId } = useSWR(
    [`/api/lookup/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const { data: counterParty } = useSWR(
    [`api/legal`, session?.user?.token],
    fetcher
  );
  const { data: transferType } = useSWR(
    [`api/lookup/Transfer_type`, session?.user?.token],
    fetcher
  );
  const {
    data: data,
    error,
    isLoading,
  } = useSWR(
    formik.values.counter_party
      ? [
          `/api/report/movement/${formik.values.transfer_type}/${
            formik.values.company_id
          }/${formik.values.counter_party}/${startDate.format(
            "YYYY-MM-DD"
          )}/${endDate.format("YYYY-MM-DD")}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );

  const handleStartDateChange = (newValue) => {
    setStartDate(newValue);
  };

  const handleEndDateChange = (newValue) => {
    setEndDate(newValue);
  };
  const formatAsCurrency = (amount) => {
    const formatter = new Intl.NumberFormat("mn-MN", {
      style: "currency",
      currency: "MNT",
    });
    const formattedAmount = formatter.format(amount);
    return formattedAmount.replace("MNT", "₮").replace(/\.00$/, "");
  };

  const numberWithCommas = (number) => {
    return number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const tableRef = useRef(null);
  let totalValue = 0;
  let totalLitr = 0;
  let totalKg = 0;
  let totalDollar = 0;
  return (
    <>
      <Stack>
        <Stack
          direction="row"
          spacing={1}
          display={"flex"}
          justifyContent={"space-between"}
        >
          <Stack
            direction={"row"}
            spacing={1}
            justifyContent={"flex-start"}
            alignItems={"center"}
          >
            <FormControl sx={{ width: "20%" }}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
                sx={{ ml: -20 }}
              >
                <DatePicker
                  label="Эхлэх огноо"
                  format="YYYY-MM-DD"
                  value={startDate}
                  onChange={handleStartDateChange}
                  slotProps={{ textField: { size: "small", minwidth: 200 } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl sx={{ ml: 5, width: "20%" }}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                firstDayOfWeek={0}
              >
                <DatePicker
                  label="Дуусах огноо"
                  format="YYYY-MM-DD"
                  value={endDate}
                  onChange={handleEndDateChange}
                  slotProps={{ textField: { size: "small", minwidth: 200 } }}
                  showDaysOutsideCurrentMonth
                  displayWeekNumber
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Company</InputLabel>
              <Select
                sx={{ height: "34px", width: "200px" }}
                defaultValue=""
                labelId="company_id"
                margin="dense"
                label="Company"
                name="company_id"
                // onChange={formik.handleChange}
                onChange={(e) => {
                  formik.setFieldValue("company_id", e.target.value);
                }}
                value={formik.values.company_id}
              >
                {Array.isArray(companyId?.result) ? (
                  companyId?.result.map((item) => (
                    <MenuItem key={item.org_id} value={item.org_id}>
                      {item.org_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem value=""> </MenuItem>
                )}
              </Select>
            </FormControl>
           

            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Counterparty</InputLabel>
                <Select
                  sx={{ height: "34px", width: "200px" }}
                  defaultValue=""
                  labelId="counter_party"
                  margin="dense"
                  label="Counterparty"
                  name="counter_party"
                  // onChange={formik.handleChange}
                  onChange={(e) => {
                    formik.setFieldValue("counter_party", e.target.value);
                  }}
                  value={formik.values.counter_party}
                >
                  {Array.isArray(counterParty?.result) ? (
                    counterParty?.result.map((item) => (
                      <MenuItem
                        key={item.legal_entity_id}
                        value={item.legal_entity_id}
                      >
                        {item.name}
                      </MenuItem>
                    ))
                  ) : (
                    <MenuItem value=""> </MenuItem>
                  )}
                </Select>
            </FormControl>
            <FormControl margin="normal">
              <InputLabel sx={{ mt: -0.8 }}>Transfer type</InputLabel>
              <Select
                sx={{ height: "34px", width: "200px" }}
                defaultValue=""
                labelId="transfer_type"
                margin="dense"
                label="Transfer"
                name="transfer_type"
                // onChange={formik.handleChange}
                onChange={(e) => {
                  formik.setFieldValue("transfer_type", e.target.value);
                }}
                value={formik.values.transfer_type}
              >
                {Array.isArray(transferType?.result) ? (
                  transferType?.result.map((item) => (
                    <MenuItem key={item.lookup_code} value={item.lookup_code}>
                      {item.name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem value=""> </MenuItem>
                )}
              </Select>
            </FormControl>
          </Stack>

          <DownloadTableExcel
            filename="SalesReport"
            sheet="users"
            currentTableRef={tableRef.current}
          >
            <Button variant="outlined" color="success" sx={{ height: "34px" }}>
            EXCEL
            </Button>
          </DownloadTableExcel>
        </Stack>

        <Divider
          sx={{
            my: 2,
            mr: 2,
          }}
        />
        <TableContainer sx={{ maxHeight: "80vh", mb: 2, mr: 2 }}>
          <Table
            size="small"
            stickyHeader
            ref={tableRef}
            sx={{
              "& .MuiTableCell-root": {
                border: "0.5px solid #ddd",
              },
            }}
          >
            <TableHead sx={{ position: "sticky", zIndex: 100 }}>
              <TableRow>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Огноо</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Нийлүүлэгч</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Хүлээн авагч
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Ачигдсан агуулах
                  </Typography>
                </TableCell>
                {/* <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>..</Typography>
                </TableCell> */}
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    ШТМ-ийн төрөл
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600", width: "130px" }}>
                    Т/х дугаар
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Ачуулсан литр
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Хувийн жин</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Ачсан кг</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Хэмжих нэгж
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>
                    Нэгжийн үнэ
                  </Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Нийт дүн</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data?.result.map((item, i) => {
                totalLitr += item.quantity_cm;
                totalKg += item.quantity_mt;
                if(item.units === "MT"){
                 totalDollar= item.amount * item.quantity_mt
                }else {
                  totalDollar= item.amount * item.quantity_cm
                }
                totalDollar += totalDollar;

                return (
                  <TableRow key={i}>
                    <TableCell>
                      <Typography>
                        {" "}
                        {dayjs(item.transfer_completion_date).format(
                          "YYYY-MM-DD"
                        )}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>{item.counterparty_name}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography> {item.company_name}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>
                        {item.terminal_name} / {item.tank_name}
                      </Typography>
                    </TableCell>
                    {/* <TableCell>
                      <Typography> {item.trade_id}</Typography>
                    </TableCell> */}
                    <TableCell>
                      <Typography>
                        {" "}
                        {item.product_name} / {item.grade_name}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography> {item.tank_comment}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography> {item.quantity_cm}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography> {item.final_density}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography> {item.quantity_mt}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>{item.units}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>{item.amount}</Typography>
                    </TableCell>
                    <TableCell>
                      {item.units === "MT" ? (
                        <Typography>
                          {" "}
                          {item.amount * item.quantity_mt}
                        </Typography>
                      ) : (
                        <Typography>
                          {" "}
                          {item.amount * item.quantity_cm}
                        </Typography>
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}

              <TableRow>
                <TableCell colSpan={1}></TableCell>
                <TableCell colSpan={5}>Нийт:</TableCell>
                <TableCell colSpan={1}>{numberWithCommas(totalLitr)}</TableCell>
                <TableCell colSpan={1}></TableCell>
                <TableCell colSpan={1}>{numberWithCommas(totalKg)}</TableCell>
                <TableCell colSpan={2}></TableCell>
                <TableCell colSpan={1}>
                  {numberWithCommas(totalDollar)}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <Divider
          sx={{
            mb: 2,
            mr: 2,
          }}
        />
      </Stack>
    </>
  );
}
