import React from "react";
import {
  ListItemIcon,
  Box,
  List,
  styled,
  ListItem,
  useTheme,
  ListItemText,
  ListItemButton,
} from "@mui/material";
import Link from "next/link";

import {
  IconHome2,
  IconReportMoney,
  IconBusinessplan,
  IconArchive,
  IconTransfer,
  IconWriting,
  IconSettings,
  IconCircleCheck,
  IconChartInfographic,
  IconHeartRateMonitor,
} from "@tabler/icons-react";

// const Menuitems = [
//   {
//     id: 1,
//     title: "Task",
//     icon: IconHome2,
//     href: "/dashboard",
//     role: ["edit"],
//     menu: "task"
//   },
//   {
//     id: uniqueId(),
//     title: "Trade",
//     icon: IconBusinessplan,
//     href: "/trade",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },
//   {
//     id: uniqueId(),
//     title: "Operation",
//     icon: IconTransfer,
//     href: "/operation",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },

//   {
//     id: uniqueId(),
//     title: "Storage",
//     icon: IconArchive,
//     href: "/storage",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },
//   {
//     id: uniqueId(),
//     title: "Finance",
//     icon: IconReportMoney,
//     href: "/finance",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },
//   {
//     id: uniqueId(),
//     title: "Contract",
//     icon: IconWriting,
//     href: "/contract",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },
//   {
//     id: uniqueId(),
//     title: "Deal",
//     icon: IconCircleCheck,
//     href: "/deal",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },
//   {
//     id: uniqueId(),
//     title: "Report",
//     icon: IconChartInfographic,
//     href: "/report",
//     role: ["Admin", "Operator", "Financer", "Trader", "ContractAdmin"],
//   },
//   {
//     id: uniqueId(),
//     title: "Setting",
//     icon: IconSettings,
//     href: "/setting",
//     role: ["Admin"],
//   },
// ];

const NavItem = ({ item, pathDirect, onClick }) => {
  const Icon =
    item.menu_name === "Task"
      ? IconHome2
      : item.menu_name === "Trade"
      ? IconBusinessplan
      : item.menu_name === "Operation"
      ? IconTransfer
      : item.menu_name === "Storage"
      ? IconArchive
      : item.menu_name === "Finance"
      ? IconReportMoney
      : item.menu_name === "Contract"
      ? IconWriting
      : item.menu_name === "Deal"
      ? IconCircleCheck
      : item.menu_name === "Report"
      ? IconChartInfographic
      : item.menu_name === "Setting"
      ? IconSettings
      : item.menu_name === "Logs"
      ? IconHeartRateMonitor
      : IconHome2;
  const theme = useTheme();
  const itemIcon = <Icon stroke={1.5} size="36px" />;

  return (
    <List>
      <ListItemButton
        component={Link}
        href={item.menu_url}
        disabled={item.disabled}
        selected={pathDirect === item.menu_url}
        onClick={() => {
          typeof localStorage !== undefined
            ? localStorage.setItem("tradeMenu", item.menu_name)
            : null;
          onClick;
        }}
        sx={{
          "&:hover": {
            color: "#ffffff",
            hoverOpacity: 0.02,
          },
          "&:selected": {
            color: "#ffffff",
            hoverOpacity: 0.95,
          },
        }}
      >
        <Box>
          <ListItemIcon>{itemIcon}</ListItemIcon>
          <ListItemText>{item.menu_name}</ListItemText>
        </Box>
      </ListItemButton>
    </List>
  );
};

export default NavItem;
