"use client";
import * as React from "react";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Typography,
  Stack,
  Modal,
  Alert,
  Divider,
  IconButton,
  AlertTitle,
  Grid,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import CustomNoRowsOverlay from "@/components/NoRecord";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
  GridRowEditStopReasons,
  GridToolbarExport 
} from "@mui/x-data-grid";
import { useSession } from "next-auth/react";
import useSWR from "swr";
import { fetcher } from "@/lib/fetcher";
import { fetchData } from "@/lib/fetch";
import dayjs from "dayjs";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  minWidth: "35vw",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  border: "1px solid #000",
  overflowY: "auto",
  overflowX: "auto",
  boxShadow: 0,
  p: 1,
};
function CustomToolbar() {
  return (
    <GridToolbarContainer>
      <GridToolbarExport />
    </GridToolbarContainer>
  );
}
export default function FinancePayment({
  invoiceId,
  // open,
  onClose,
  rows,
  setRows,
  setParams,
  dataRealChange,
  savePayment,
  invoiceTypo,
}) {
  const { data: session } = useSession();
  const [rowModesModel, setRowModesModel] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [deleteItem, setDeleteItem] = React.useState();
  const [info, setInfo] = React.useState();
  const handleOpen = (params) => {
    setDeleteItem(params);
    const qq =
      `Date: ` +
      dayjs(params.row.pay_date).format("YYYY-MM-DD") +
      ` Value: ` +
      params.row.value;
    setInfo(qq);
    setOpen(true);
  };
  const handleClose = () => setOpen(false);

  const { data: modalInvoices } = useSWR(
    [`/api/finance/payment/${invoiceId}`, session?.user?.token],
    fetcher
  );

  if (modalInvoices?.result?.length > 0 && rows === null) {
    const rowsInvcoice = modalInvoices?.result?.map((row, i) => ({
      id: i + 1,
      ...row,
    }));
    setRows(rowsInvcoice);
  }

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;

    const handleClick = () => {
      let id = null;
      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            id,
            pay_date: new Date(),
            value: "0",
            description: " ",
            isNew: true,
          },
        ]);
      } else {
        id = rows.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            id,
            pay_date: new Date(),
            value: "0",
            description: " ",
            isNew: true,
          },
        ]);
      }
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };

    return (
      <GridToolbarContainer>
        <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
          Add
        </Button>
      </GridToolbarContainer>
    );
  }

  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (params) => () => {
    dataRealChange(rows);
    setRowModesModel({
      ...rowModesModel,
      [params.row.id]: { mode: GridRowModes.View },
    });
  };

  const handleDeleteClick1 = (id) => () => {
    setRows(rows.filter((row) => row.id !== id));
  };

  const handleDeleteClick = () => () => {
    deleteData();
  };
  const deleteData = async (params) => {
    if (deleteItem.row.payment_id) {
      const body = {
        invoice_id: invoiceId,
        username: session?.user?.username,
        payment_id: deleteItem.row.payment_id,
      };
      try {
        setLoading(true);
        const res = await fetchData(
          `/api/finance/payment`,
          "DELETE",
          body,
          session?.user?.token
        );
        if (res.status === 200) {
   
          let message = null;
          message = "Амжилттай устгалаа";

          setRows(rows.filter((row) => row.id !== deleteItem.row.id));

          toast.success(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error("Устгах үед алдаа гарлаа", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    } else {
      setRows(rows.filter((row) => row.id !== deleteItem.row.id));
    }
  };
  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const columns = [
    { field: "id", headerName: "ID", width: 40 },
    {
      field: "pay_date",
      headerName: "Date",
      type: "date",
      width: 80,
      align: "left",
      headerAlign: "left",
      editable: true,
      valueGetter: (params) => new Date(params.row.pay_date),
    },
    {
      field: "value",
      headerName: "Value",
      align: "left",
      headerAlign: "left",
      type: "number",
      width: 180,
      editable: true,
    },
    {
      field: "description",
      headerName: "Description",
      type: "text",
      width: 200,
      editable: true,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      width: 100,
      cellClassName: "actions",
      getActions: (params) => {
        const isInEditMode =
          rowModesModel[params.row.id]?.mode === GridRowModes.Edit;
        // const currentRow = params.row;
        if (isInEditMode) {
          return [
            <GridActionsCellItem
              key={1}
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(params)}
              onClickCapture={() => {
                handleSaveClick(params);
                // savePayment(currentRow)
              }}
            />,
            <GridActionsCellItem
              key={2}
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(params.row.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            key={3}
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(params.row.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            key={4}
            icon={<DeleteIcon />}
            label="Delete"
            // onClick={handleDeleteClick(params)}
            onClick={() => handleOpen(params)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  return (
    <>
      <ToastContainer />
      <Box sx={style}>
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Typography variant="h5" gutterBottom>
            Payment
          </Typography>

          <IconButton
            sx={{ display: "flex", mt: -1 }}
            onClick={onClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
        </Box>

        <Divider sx={{ mb: 1 }} />
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          Invoice number :
          <Typography
            id="modal-modal-title"
            variant="subtitle1"
            fontWeight="bold"
          >
            {invoiceTypo.invoice_number}
          </Typography>
          Invoice amount :
          <Typography
            id="modal-modal-title"
            variant="subtitle1"
            fontWeight="bold"
          >
            {invoiceTypo.invoice_amount}
          </Typography>
          Unpaid amount :
          <Typography
            id="modal-modal-title"
            variant="subtitle1"
            fontWeight="bold"
          >
            {invoiceTypo.unpaid_amount}
          </Typography>
        </Grid>
        <DataGrid
          rows={rows ? rows : []}
          columns={columns}
          editMode="row"
          rowModesModel={rowModesModel}
          onRowModesModelChange={handleRowModesModelChange}
          onRowEditStop={handleRowEditStop}
          processRowUpdate={processRowUpdate}
          sx={{
            backgroundColor: "#FFFFFF",
            "& .MuiDataGrid-cell:hover": {
              color: "primary.main",
            },
            "& .MuiDataGrid-cell": {
              borderTop: 1,
              borderTopColor: "#E9ECF0",
            },
            borderRadius: 0,
            maxWidth: "90vw",
            minHeight: "20vh",
            maxHeight: "60vh",
            overflow: "auto",
          }}
          slots={{
            noRowsOverlay: CustomNoRowsOverlay,
            toolbar: EditToolbar,CustomToolbar,
          }}
          slotProps={{
            toolbar: { setRows, setRowModesModel },
          }}
          hideFooter
        />
        <Divider sx={{ my: 1 }} />
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Button onClick={onClose} variant="outlined">
            close
          </Button>
          <Button
            onClick={() => {
              savePayment();
            
            }}
            variant="contained"
          >
            Save
          </Button>
        </Box>
      </Box>
      <Modal open={open}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Delete payment
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{info}</AlertTitle>
                Are you sure delete!
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={() => {
                deleteData();
                handleClose();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              Yes
            </Button>
            <Button onClick={handleClose} variant="outlined">
              No
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
}
