"use client";
import * as React from "react";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import useSWR from "swr";
import MyAlert from "@/components/Myalert";
import CustomNoRowsOverlay from "@/components/NoRecord";
import {
  Box,
  Button,
  CircularProgress,
  Typography,
  Stack,
  Modal,
  Alert,
  AlertTitle,
} from "@mui/material";
import dayjs from "dayjs";
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
  GridRowEditStopReasons,
} from "@mui/x-data-grid";

import { useSession } from "next-auth/react";
import { fetcher } from "@/lib/fetcher";
import { fetchData } from "@/lib/fetch";


export default function DebitsDatagrid({
  params,
  dataRealChange,
  rows,
  setRows,
}) {
  const { data: session } = useSession();
  const { invoice_id } = params;
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [deleteItem, setDeleteItem] = React.useState(null);
  const [rowModesModel, setRowModesModel] = React.useState({});
  const handleClose = () => {
    setOpen(false);
  };
  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const { data: costs } = useSWR(
    [`/api/lookup/cost`, session?.user?.token],
    fetcher
  );

  const {
    data: depits,
    error,
    isLoading,
    mutate
  } = useSWR(
    invoice_id !== "new"
      ? [
          `/api/finance/invoice/detail/${invoice_id}/Debit`,
          session?.user?.token,
        ]
      : null,
    fetcher,
  );

  if (error) return <MyAlert error={error} />;
  if (isLoading)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", m: 3 }}>
        <CircularProgress />
      </Box>
    );

  if (depits?.result?.length > 0 && rows === null) {
    const rowsDebit = depits?.result?.map((row, i) => ({
      id: i + 1,
      invoice_detail_id: row.invoice_detail_id,
      description: row.description,
      cost_code: row.cost_code,
      cost: row.cost_code,
      value: row.value,
      exp_pay_date: dayjs(row.exp_pay_date).format("YYYY-MM-DD"),
      tran_date: dayjs(row.tran_date).format("YYYY-MM-DD"),
      type: "EDIT",
    }));
    setRows(rowsDebit);
  }

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;

    const handleClick = () => {

      let id = null;
      if (rows === null) {
        id = 1;
        setRows((oldRows) => [
          {
            id: id,
            cost: "C0",
            value: 0,
            isNew: true,
            exp_pay_date: new Date(),
            tran_date: new Date(),
            description: "",
            type: "ADD",
          },
        ]);
      } else {
        id = rows.length + 1;
        setRows((oldRows) => [
          ...oldRows,
          {
            id: id,
            cost: "C0",
            value: 0,
            isNew: true,
            exp_pay_date: new Date(),
            tran_date: new Date(),
            description: "",
            type: "ADD",
          },
        ]);
      }
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: "name" },
      }));
    };
    if (
      session &&
      session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
        .permission === "Edit"
    ) {
      return (
        <GridToolbarContainer>
          <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
            Add
          </Button>
        </GridToolbarContainer>
      );
    } else {
      return null;
    }
  }

  const handleEditClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id.row.id]: { mode: GridRowModes.Edit },
    });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id.row.id]: { mode: GridRowModes.View },
    });
    dataRealChange(rows);
  };

  const handleDeleteClick = (id) => () => {
    setOpen(true);
    setDeleteItem(id);
  };
  const deleteData = async (id) => {
    if (deleteItem.row.invoice_detail_id) {
      const body = {
        username: session?.user?.username,
        invoice_detail_id: deleteItem.row.invoice_detail_id,
      };
      try {
        setLoading(true);
        const res = await fetchData(
          `/api/finance/invoice/detail`,
          "DELETE",
          body,
          session?.user?.token
        );

        if (res.status === 200) {
          mutate
          setRows(rows.filter((row) => row.id !== deleteItem.row.id));
          toast.success("Амжилттай хадгаллаа", {
            position: toast.POSITION.TOP_RIGHT,
            className: "toast-message",
          });
        } else {
          toast.error("Хадгалах үед алдаа гарлаа", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    } else {
      setRows(rows.filter((row) => row.id !== deleteItem.row.id));
    }
  };
  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id.row.id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id.row.id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id.row.id));
    }
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "cost",
      renderHeader: () => <strong>{"Cost"}</strong>,
      type: "singleSelect",
      valueOptions: costs?.result,
      getOptionLabel: (value) => `${value.cost_code}-${value.cost_name}`,
      getOptionValue: (value) => value.cost_code,
      editable: true,
      width: 150,
    },

    {
      field: "value",
      type: "number",
      renderHeader: () => <strong>{"Amount"}</strong>,
      editable: true,
      width: 150,
    },

    {
      field: "tran_date",
      renderHeader: () => <strong>{"Tran date"}</strong>,
      valueGetter: (params) => {
        return new Date(params.row.tran_date);
      },
      type: "date",
      editable: true,
      width: 150,
    },
    {
      field: "exp_pay_date",
      renderHeader: () => <strong>{"Exp pay date"}</strong>,
      valueGetter: (params) => {
        return new Date(params.row.exp_pay_date);
      },
      type: "date",
      editable: true,
      width: 150,
    },
    {
      field: "description",
      renderHeader: () => <strong>{"Description"}</strong>,
      editable: true,
      width: 250,
    },
    {
      field: "edit",
      renderHeader: () => <strong>{"Edit"}</strong>,

      type:
        session &&
        session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
          .permission === "Edit"
          ? "actions"
          : "",
      headerName: "Actions",
      width: 80,
      cellClassName: "actions",
      getActions: (id) => {
        const isInEditMode =
          rowModesModel[id.row.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        } else {
          return [
            <GridActionsCellItem
              icon={<EditIcon />}
              label="Edit"
              className="textPrimary"
              onClick={handleEditClick(id)}
              color="inherit"
            />,
          ];
        }
      },
    },
    {
      field: "delete",
      renderHeader: () => <strong>{"Delete"}</strong>,
      type:
      session &&
      session?.user?.menuitems.filter((f) => f.menu_name === "Finance")[0]
        .permission === "Edit"
        ? "actions"
        : "",
      headerName: "Actions",
      width: 100,
      cellClassName: "actions",
      getActions: (id) => {
        return [
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
      }}
    >
      <DataGrid
        // getRowId={(row) => row.id}
        rows={rows ? rows : []}
        columns={columns}
        editMode="row"
        rowModesModel={rowModesModel}
        onRowModesModelChange={handleRowModesModelChange}
        onRowEditStop={handleRowEditStop}
        processRowUpdate={processRowUpdate}
        slots={{
          toolbar: EditToolbar,
          noRowsOverlay: CustomNoRowsOverlay,
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        hideFooter
        autoHeight={true}
        sx={{
          backgroundColor: "#FFFFFF",
          "& .MuiDataGrid-cell:hover": {
            color: "primary.main",
          },
          "& .MuiDataGrid-cell": {
            borderTop: 1,
            borderTopColor: "#E9ECF0",
          },
        }}
      />
      <Modal open={open} onClose={handleClose}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Box>
            <Typography id="modal-modal-title" variant="h6" component="h3">
              Confirm
            </Typography>
          </Box>
          <Stack
            component="div"
            sx={{
              width: 400,
              maxWidth: "100%",
            }}
            noValidate
            autoComplete="off"
          >
            <Box sx={{ mt: 2, mb: 2, fontSize: "0.8rem" }}>
              <Alert severity="error">
                <AlertTitle>{""}</AlertTitle>
                Are you sure you want to delete?
              </Alert>
            </Box>
          </Stack>
          <Box
            sx={{
              display: "flex",
              marginBottom: 1,
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={async () => {
                handleClose();
                deleteData();
              }}
              variant="contained"
              sx={{
                marginLeft: "10px",
                marginRight: "10px",
              }}
            >
              Yes
            </Button>
            <Button onClick={handleClose} variant="outlined">
              No
            </Button>
          </Box>
        </Box>
      </Modal>
    </Box>
  );
}
