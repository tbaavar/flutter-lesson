import { fetcher } from "@/lib/fetcher";
import {
  Box,
  Button,
  Divider,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { useRef } from "react";
import useSWR from "swr";
import { DownloadTableExcel } from "react-export-table-to-excel";
import * as yup from "yup";

export default function DealTransactionReport() {
  const { data: session } = useSession();

  const formik = useFormik({
    initialValues: {
      deal_id: "",
    },
    validationSchema: yup.object({
      method: yup.string(),
    }),
    onSubmit: async (values) => {},

    enableReinitialize: true,
  });
  const {
    data: dealData,
    error,
    isLoading,
  } = useSWR(
    formik.values.deal_id
      ? [
          `/api/report/deal/transaction/${formik.values.deal_id}`,
          session?.user?.token,
        ]
      : null,
    fetcher
  );
  const { data: dealIdData } = useSWR(
    [`api/deal/company/${session?.user.user_id}`, session?.user?.token],
    fetcher
  );
  const uniqueCostCodes = [
    ...new Set(dealData?.result.map((item) => item.cost_code)),
  ];
  //   const uniqueCostCodes = [...new Set(dealData?.result.map(item => ({
  //     cost_code: item.cost_code,
  //     gross_value: item.gross_value,
  //     gross_value_usd: item.gross_value_usd
  //   })))];
  let value = 0;
  const costCode = uniqueCostCodes.map((code) => {
    const matchingItem = dealData?.result.find(
      (item) => item.cost_code === code
    );

    return {
      cost_code: matchingItem.cost_code,
      gross_value: matchingItem.gross_value,
      gross_value_usd: matchingItem.gross_value_usd,
    };
  });
  const formatAsCurrency = (amount) => {
    const formatter = new Intl.NumberFormat("mn-MN", {
      style: "currency",
      currency: "MNT",
    });
    const formattedAmount = formatter.format(amount);
    return formattedAmount.replace("MNT", "₮").replace(/\.00$/, "");
  };

  const numberWithCommas = (number) => {
    return number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
  const tableRef = useRef(null);
  let totalValue = 0;

  return (
    <>
    <Stack >
        <Stack direction="row" spacing={1} display={'flex'} justifyContent={'space-between'} >
        <FormControl margin="normal">
          <InputLabel sx={{ mt: -0.8 }}>Deal</InputLabel>
          <Select
            sx={{ height: "38px", width: "200px" }}
            defaultValue=""
            labelId="deal_id"
            margin="dense"
            label="Deal"
            name="deal_id"
            // onChange={formik.handleChange}
            onChange={(e) => {
              formik.setFieldValue("deal_id", e.target.value);
            }}
            value={formik.values.deal_id}
          >
            {Array.isArray(dealIdData?.result) ? (
              dealIdData?.result.map((item) => (
                <MenuItem key={item.deal_id} value={item.deal_id}>
                  {item.title}
                </MenuItem>
              ))
            ) : (
              <MenuItem value=""> </MenuItem>
            )}
          </Select>
        </FormControl>
        <DownloadTableExcel
          filename="DealTransactionReport"
          sheet="users"
          currentTableRef={tableRef.current}
        >
          <Button variant="outlined" color="success" sx={{ height: "38px" }}>
            EXCEL
          </Button>
        </DownloadTableExcel>
      </Stack>


      <Divider
        sx={{
          my: 2,
          mr: 2,
        }}
      />
      <TableContainer sx={{ maxHeight: "80vh", mb: 2, mr: 2 }}>
        <Table
          size="small"
          stickyHeader
          ref={tableRef}
          sx={{
            "& .MuiTableCell-root": {
              border: "0.5px solid #ddd",
            },
          }}
        >
          <TableHead sx={{ position: "sticky", zIndex: 100 }}>
            <TableRow>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>№</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Inv Ref</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Inv Owner</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>
                  Sun Cost Code
                </Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>
                  Cost Description
                </Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Trade</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600", width: "130px" }}>
                  Parcel
                </Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>
                  Trans Counterparty
                </Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Description</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Bank</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Tran Date</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>
                  Cash Book Date
                </Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Value Date</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Updated By</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Trn Type</Typography>
              </TableCell>
              {/* <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Post Cls, Out</Typography>
                </TableCell>
                <TableCell rowSpan={1}>
                  <Typography sx={{ fontWeight: "600" }}>Rev?</Typography>
                </TableCell> */}
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>CCY</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>Gross Value</Typography>
              </TableCell>
              <TableCell rowSpan={1}>
                <Typography sx={{ fontWeight: "600" }}>
                  Gross Value USD
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {costCode.map((code, index) => {
              value = 0;
              return (
                <>
                  {dealData?.result.map((item, i) => {
                    const names = item.invoice_owner_name.split(" ");
                    const firstN = names[0].charAt(0);
                    const lastN = names.pop();
                    if (item.cost_code === code.cost_code) {
                      totalValue = totalValue + item.gross_value;

                      value = value + item.gross_value;
                      return (
                        <TableRow key={i}>
                          <TableCell>
                            <Typography>{i + 1}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.invoice_number}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography>
                              {firstN}.{lastN}
                            </Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.cost_code}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.cost_name}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.trade_id}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.parcel_number}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.counterparty_name}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.description}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.bank_name}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography>
                              {dayjs(item.tran_date).format("YYYY-MM-DD")}
                            </Typography>
                          </TableCell>
                          <TableCell>
                            <Typography>
                              {dayjs(item.exp_pay_date).format("YYYY-MM-DD")}
                            </Typography>
                          </TableCell>
                          <TableCell>
                            <Typography>
                              {" "}
                              {dayjs(item.invoice_due_date).format(
                                "YYYY-MM-DD"
                              )}
                            </Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.invoice_owner_name}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {item.invoice_type}</Typography>
                          </TableCell>
                          {/* <TableCell>
                        <Typography> {item.firstname}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography> {item.firstname}</Typography>
                      </TableCell> */}
                          <TableCell>
                            <Typography> {item.currency}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {numberWithCommas(item.gross_value)}</Typography>
                          </TableCell>
                          <TableCell>
                            <Typography> {numberWithCommas(item.gross_value_usd)}</Typography>
                          </TableCell>
                        </TableRow>
                      );
                    }

                    return null;
                  })}

                  <TableRow sx={{ backgroundColor: "#808080"}}>
                    <TableCell colSpan={15}></TableCell>
                    <TableCell colSpan={1}>
                      <Typography sx={{color: '#fff'}}>{code.cost_code}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography sx={{color: '#fff'}}>
                        {/* {code.gross_value + code.gross_value_usd} */}
                        {numberWithCommas(value)}
                      </Typography>
                    </TableCell>
                    <TableCell colSpan={1}></TableCell>
                  </TableRow>
                </>
              );
            })}
            <TableRow>
              <TableCell colSpan={15}></TableCell>
              <TableCell colSpan={1}>Total:</TableCell>
              <TableCell colSpan={1}>{numberWithCommas(totalValue)}</TableCell>
              <TableCell colSpan={1}></TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <Divider
        sx={{
          mb: 2,
          mr: 2,
        }}
      />
    </Stack>
      
    </>
  );
}
