import { Card, Box, Typography, Chip, IconButton } from "@mui/material";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import Stack from "@mui/material/Stack";
import React from "react";
import { useRouter } from "next/navigation";

export default function Indicator({
  menu,
  title,
  total,
  urgent,
  high,
  medium,
  regular,
}) {
  const router = useRouter();
  return (
    <Card variant="outlined" sx={{ width: 200, height: 90 }}>
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        sx={{ display: "flex" }}
      >
        <Typography sx={{ ml: 2, mt: 1, color: "#8993A0" }} variant="h5" component={'span'}>
          {title}
        </Typography>

        <IconButton
          onClick={() => {
            router.push(menu);
          }}
        >
          <OpenInNewIcon sx={{ mr: 1 }} />
        </IconButton>
      </Stack>
      <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
        <Typography sx={{ ml: 2 }} variant="h2" component={'span'}>
          {total}
        </Typography>
        <Chip variant="filled" color="urgent" label={urgent}></Chip>
        <Chip variant="filled" color="high" label={high}></Chip>
        <Chip variant="filled" color="medium" label={medium}></Chip>
        <Chip variant="filled" color="regular" label={regular}></Chip>
      </Box>
    </Card>
  );
}
